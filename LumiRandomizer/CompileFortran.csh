#! /bin/tcsh -f
#
#      shell to run simple program
#
   setenv PROGRAM `echo $1 | sed -e "s/\.f//"`
   echo $PROGRAM
if $2 == "c" then
    rm -f  $PROGRAM.exe
endif
if $2 != "r" then
#   setenv PROGRAM $1
   echo $OSTYPE
   if $OSTYPE == "hpux"  then
    setenv COMP "fort77 +ppu -g +U77 -C -K +O2 +E4"
    setenv MACHINE "HPUX"
   
   else if $OSTYPE == "linux"  then
#    setenv COMP "g77 -fno-automatic -fno-backslash -fbounds-check -C -g"
    setenv COMP "gfortran -fno-automatic -fno-backslash -fbounds-check -C -g -o $PROGRAM.exe"
    setenv MACHINE "linux"
   endif
#   echo $COMP
   $COMP $1
   #gmake
   set stat=${status}
   if ( ${stat} ) then
      echo ""
#      echo "FCASPLIT error. Exit"
      exit ${stat}
   endif
endif
$PROGRAM.exe
