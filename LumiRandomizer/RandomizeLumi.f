*     Original file courtesy of K. Moenig
*     
      implicit none
c     real x1(5,3000000)
c$$$  real x1(5,5000000)
c$$$  real ranv(5000000)
      INTEGER COLUMNS, i, j, n
      parameter( COLUMNS=10 )
      real x1(COLUMNS,7000000)
      real ranv(7000000)
      open (1,file='lumi.dat')
      n = 1
      do while (.true.)

         read (1,*,end=100,err=100) (x1(i,n),i=1,COLUMNS)
         n = n+1
      enddo
 100  continue
      n = n-1
      print*,n

      print*, (x1(i,1),i=1,COLUMNS)
      print*, (x1(i,n),i=1,COLUMNS)
      close(1)
      open (2,file='lumi2.dat')
      do while (n.gt.0)
c     call RANLUX(ranv,1)
c     j = n*rndm(n)+1
c     j = n*ranv(1)+1
         j = n*rand(0)+1
c     write (2,'(5f10.3)'), (x1(i,j),i=1,5)
c     only write the first two columns
c     write to stream 2 10 columns of g 20 characters with 10 digit precision
         write (2,'(10 g20.10)'), (x1(i,j),i=1,COLUMNS)
         do i=1,COLUMNS
            x1(i,j) = x1(i,n)
         enddo
         n = n-1
      enddo
      close (2)
*     
      end
*     
