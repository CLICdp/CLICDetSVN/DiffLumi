#include <iostream>
#include <iomanip>

#include <TF1.h>
#include <TF2.h>
#include <TMath.h>
#include "TCanvas.h"

#include "TH3D.h"
#include "TH2D.h"


#include "Integration.hh"

//Cosntants
const Double_t BetaLimit = 0.999;

//Our Final Goal
Double_t Convolution1D(Double_t* x, Double_t* p);
Double_t Convolution2D(Double_t* x, Double_t* p);

//Normal Functions
Double_t Peak(Double_t *x, Double_t* p);
Double_t BetaFunction(Double_t *x, Double_t* p);
Double_t BeamSpread(Double_t *x, Double_t* p);

//Convolutions between functions
Double_t BetaFunctionBeamSpread(Double_t *x, Double_t* p);
Double_t PeakBeamSpread(Double_t *x, Double_t* p);


//Normalisation Functions
inline Double_t getBeamSpreadNorm(double *a) {
  return 1./(a[2]/a[3]*(TMath::SinH(a[3]*(a[1]-a[4]))-TMath::SinH(a[3]*(a[0]-a[4])))+(a[1]-a[0]));
}

inline Double_t getBetaNorm(double *a, double x = 1.0) {
  //BetaIncomplete is Integral / BetaFunction, thus we have to multiply by beta!
  //Should be this order for the parameters!
  return 1./(TMath::Beta(1.+a[1], 1.+a[2])*TMath::BetaIncomplete(x, 1.+a[1], 1.+a[2]));
}

void ConvolutionInTwoDimensions();
void ConvolutionInOneDimensions();


int main ()
{

  ConvolutionInOneDimensions();
  ConvolutionInTwoDimensions();


  return 0;

}//Main

void ConvolutionInOneDimensions() {
  //  TF2 DiffLumi("Convo2D",Convolution2D, 0.,1.1, 0., 1.1, 30);


  //3 parameters for beta, 5 parameters for beamspread
  Double_t parameter[8] = {5.38449e-1, 2.05944e-1, -5.96254e-1, //with delta function
  			   0.995291-1, 1.00550-1, //Range for BeamSpread
  			   2.22607e-4/1.26940e-2, 1.08480e3, 1.00077-1, }; //bsparameter

  // Double_t parameter[8] = {0.0, 2.05944e-1, -5.96254e-1, //with delta function
  // 			   0.995291-1, 1.00550-1, //Range for BeamSpread
  // 			   2.22607e-4/1.26940e-2, 1.08480e3, 1.00077-1, }; //bsparameter


  TF1 BeamSpreadFunction("BeamSpreadFunction", BeamSpread, 0.9, 1.1, 5);
  BeamSpreadFunction.SetParameters(&(parameter[3]));
  std::cout << "BeamSpread Integral   " << Integrate(&BeamSpreadFunction, -.1, .1, 0.001)  << std::endl;


  TF1 BetaFunctionFunction("BetaFunctionFunction", BetaFunction, 0.0, 1.1, 5);
  BetaFunctionFunction.SetParameters(&(parameter[0]));
  std::cout << "BetaFunction Integral " << Integrate(&BetaFunctionFunction, 0.0, 1.1, 0.001)  << std::endl;


  TF1 DiffLumi("Convo1D",Convolution1D, 0.9, 1.01, 8);
  DiffLumi.SetParameters(parameter);


  TCanvas c1d("c1d","c1d");
  //  c1d.DrawFrame(0.9, 1.1, 0.0, 10);
  DiffLumi.SetNpx(10000);

  std::cout << "Drawing Function"  << std::endl;
  DiffLumi.Draw();
  std::cout << "...Done"  << std::endl;
  c1d.SetLogy();
  c1d.SaveAs("OneD.eps");


 //  return 0;

  std::cout << "Integrating"  << std::endl;
  std::cout << Integrate(&DiffLumi, 0.0, 1.1, 0.0001)  << std::endl;
  //  std::cout << Integrate(&DiffLumi, 0.9, 1.1, 0.001)  << std::endl;
  std::cout << "...Done"  << std::endl;

  return;

}//ConvolutionInOneDimensions


Double_t Convolution1D(Double_t* x, Double_t* p){

  //Distribute the parameters
  // Double_t betaPar[3], bsPar[5];
  // for (int i =0; i < 3;++i) {
  //   betaPar[i]=p[i];
  // }

  // for (int i =3; i < 8;++i) {
  //   bsPar[i]=p[i];
  // }

  return 0
    + BetaFunctionBeamSpread(x, p)
    + PeakBeamSpread(x, p)
    ;
}//Convolution2D


void ConvolutionInTwoDimensions() {

  Double_t parameter2d[19] = {
    0.25			, 0.25		, 0.25		,    //For the different parts
    5.38449e-1			, 2.05944e-1	, -5.96254e-1	,    //with delta function
    0.995291-1			, 1.00550-1	,                    //Range for BeamSpread
    2.22607e-4/1.26940e-2	, 1.08480e3	, 1.00077-1     ,    // bsparameter
    5.38449e-1			, 2.05944e-1	, -5.96254e-1	,    //with delta function
    0.995291-1			, 1.00550-1	,                    //Range for BeamSpread
    2.22607e-4/1.26940e-2	, 1.08480e3	, 1.00077-1     }; //bsparameter


  //  TF2 DiffLumi("Convo2D",Convolution2D, 0.,1.1, 0., 1.1, 19);
  //  TF2 DiffLumi("Convo2D",Convolution2D, 0.9, 1.1, 0.9, 1.1, 19);
  TF2 DiffLumi("Convo2D",Convolution2D, 0.9, 1.01, 0.9, 1.01, 19);
  DiffLumi.SetParameters(parameter2d);
  DiffLumi.SetNpx(200);
  DiffLumi.SetNpy(200);

  // //  DiffLumi.SetRange(1e-3,6e2);
  TCanvas c2d("c2d","c2d");
  c2d.SetRightMargin(0.2);
  c2d.SetLogz();
  DiffLumi.SetContour(50);
  DiffLumi.Draw("cont1z");


  c2d.SaveAs("TwoD.eps");



  return;
}//ConvolutionIntwoDimensions

Double_t Convolution2D(Double_t* x, Double_t* p){
  Double_t nbod = 1.- p[0] - p[1] - p[2];
  return 0
    + p[0] * PeakBeamSpread        ( &(x[0]), &(p[3])) * PeakBeamSpread        ( &(x[1]), &(p[11]))
    + p[1] * PeakBeamSpread        ( &(x[0]), &(p[3])) * BetaFunctionBeamSpread( &(x[1]), &(p[11]))
    + p[2] * BetaFunctionBeamSpread( &(x[0]), &(p[3])) * PeakBeamSpread        ( &(x[1]), &(p[11]))
    + nbod * BetaFunctionBeamSpread( &(x[0]), &(p[3])) * BetaFunctionBeamSpread( &(x[1]), &(p[11]))
    ;


}//Convolution2D



Double_t Peak(Double_t *x, Double_t* p)
{
  if( BetaLimit < x[0] && x[0] <= 1.0) {
    return p[0]/(1.0-BetaLimit);
  }
  return 0;
}//Peak

Double_t BeamSpread(Double_t *x, Double_t* p){
  if ( p[0] < x[0] && x[0] <= p[1]) {
    return getBeamSpreadNorm(p)*(p[2]*TMath::CosH(p[3]*(x[0]-p[4]))+1);
  }
  return 0;
}//BeamSpread


Double_t BetaFunction(Double_t *x, Double_t *p) {
  // std::cout << std::setw(12) << x[0]
  // 	    << std::setw(12) << p[0]
  // 	    << std::setw(12) << p[1]
  // 	    << std::setw(12) << p[2]
  // 	    << std::endl;
  if( 0 < x[0] && x[0] <= BetaLimit) {
    return getBetaNorm(p, BetaLimit) * (1.-p[0]) * pow(x[0],p[1]) * pow(1.-x[0],p[2]);
  }
  return 0;
}//BetaFunction




////////////////////////////////////////////////////////////
// Convolution of Peak With BeamSpread
////////////////////////////////////////////////////////////

//Convolution is Integral mInf p Inf f(tau)*g(x-tau) dtau
//We use g as the beamspread, and f as the other functions of x, right?
//Now we have to create a function out of the two we have, to give us the right result...

Double_t ConvolutePeakBeamSpread(Double_t* tau, Double_t *p) {
  Double_t XminusTau[1] = {p[8] - tau[0]};
  return BeamSpread(XminusTau, &(p[3])) * Peak(tau, &(p[0]));
}

Double_t PeakBeamSpread(Double_t *x, Double_t* p){
  //Now we have to Convolute these to functions
  Double_t convP[9];

  for (int i = 0; i < 8; ++i) {
    convP[i] = p[i];
  }


  convP[8] = x[0];

  TF1 ConvPeakBeamSpread("cpbs", ConvolutePeakBeamSpread, BetaLimit, 1.0, 9);
  ConvPeakBeamSpread.SetParameters(convP);

  return Integrate(&ConvPeakBeamSpread, BetaLimit, 1.0, 0.0001);

}



////////////////////////////////////////////////////////////
// Convolution of BetaFunction With BeamSpread
////////////////////////////////////////////////////////////



Double_t ConvoluteBetaFunctionBeamSpread(Double_t* tau, Double_t *p) {
  Double_t XminusTau[1] = {p[8] - tau[0]};
  return BeamSpread(tau, &(p[3])) * BetaFunction(XminusTau, &(p[0]));
}


Double_t BetaFunctionBeamSpread(Double_t *x, Double_t* p){

  Double_t convP[9];

  for (int i = 0; i < 8; ++i) {
    convP[i] = p[i];
  }
  convP[8] = x[0];

  //We can use BetaLimit because we integrate over tau, which is passed straight to the beamspread
  //which is only non zero between -0.005 and 0.005
  TF1 ConvBetaFunctionBeamSpread("cpbs", ConvoluteBetaFunctionBeamSpread, 0, BetaLimit, 9);
  ConvBetaFunctionBeamSpread.SetParameters(convP);

  return Integrate(&ConvBetaFunctionBeamSpread, p[3], p[4], 0.0001);

}
