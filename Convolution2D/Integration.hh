#ifndef INTEGRATION_HH
#define INTEGRATION_HH 1

#include <Math/WrappedTF1.h>
#include <Math/GSLIntegrator.h>


Double_t Integrate(TF1 *function, Double_t x1, Double_t x2, Double_t precision=0.00001){
	// Warp the function
	ROOT::Math::WrappedTF1 wconvo(*function);
 	// Create the Integrator
	ROOT::Math::GSLIntegrator ig2(ROOT::Math::IntegrationOneDim::kADAPTIVE);
 	// Set parameters of the integration
	ig2.SetFunction(wconvo);
	ig2.SetRelTolerance(precision);
 	return ig2.Integral(x1, x2);
}


#endif
