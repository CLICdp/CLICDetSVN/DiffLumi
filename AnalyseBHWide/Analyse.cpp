// $Id: $
// Include files 



// local
#include "Analyse.h"
#include "TLorentzVector.h"

#include <iostream>
//-----------------------------------------------------------------------------
// Implementation file for class : Analyse
//
// 2011-10-28 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Analyse::Analyse(  ) : m_h_cme(NULL),m_h_e1(NULL),m_h_e1_cor(NULL),m_h_e1_ratio(NULL),
                       m_h_e1_vs_e1_corr(NULL),
                       m_h_e2(NULL),
                       m_n_p(NULL),m_h_the1(NULL),m_h_the2(NULL),
                       m_h_max_ph_e(NULL),m_n_ph_e(NULL),m_h_deltathe(NULL),m_angle(NULL),
                       m_h_cm(NULL),m_h_cm2(NULL),m_h_cm3(NULL),
                       m_f(NULL){
  m_histos = false;
  
}
//=============================================================================
// Destructor
//=============================================================================
Analyse::~Analyse() {} 

//=============================================================================
 

//=========================================================================
//  
//=========================================================================
void Analyse::createHistos ( ) {
  m_h_cme = new TH1D("h_cme","",300,0,1);
  m_h_cme->SetXTitle("#frac{#sqrt{s'}}{#sqrt{s}}");
  
  m_h_e1 = new TH1D("h_e1","energy_{e2}",300,0,1550);
  m_h_e1->SetXTitle("e1");
  
  m_h_e1_cor = new TH1D("h_e1_cor","e1 corrected",300,0,1550);
  m_h_e1_cor->SetXTitle("e1 corrected");
  //m_h_e1_ratio = new TH1D("h_e1_ratio","ration e1cor/e1",300,0,1550);
  m_h_e1_ratio = new TH1D("h_e1_ratio","ratio e1cor/e1",300,0,0);
  m_h_e1_ratio->SetXTitle("#frac{e1 corrected}{e1}");
  m_h_e1_vs_e1_corr = new TH2D("m_h_e1_vs_e1_corr","e1 vs e1 corrected",300,0,1550,300,0,1550);
  m_h_e1_vs_e1_corr->SetXTitle("e1");
  m_h_e1_vs_e1_corr->SetYTitle("e1 corrected");
  m_h_e2 = new TH1D("h_e2","energy_{e2}",300,0,1550);
  m_h_e2->SetXTitle("e2");
  m_h_the1 = new TH1D("h_the1","#theta_{e1}",300,0,3.15);
  m_h_the1->SetXTitle("#theta_{e1}");
  m_h_the2 = new TH1D("h_the2","#theta_{e2}",300,0,3.15);
  m_h_the2->SetXTitle("#theta_{e2}");
  m_h_max_ph_e = new TH1D("max_ph_e","",300,0,1550);
  m_h_max_ph_e->SetXTitle("Max photon energy");
  
  m_n_p = new TH1D("n_p","n_p",11,-0.5,10.5);
  m_n_p->SetXTitle("Nb photon");
  
  m_n_ph_e = new TH2D("n_ph_e","",10,0.5,10.5,100,0,1550);
  m_n_ph_e->SetXTitle("Nb Photons");
  m_n_ph_e->SetYTitle("Max Photon energy");
 
  m_h_deltathe = new TH1D("h_deltathe","7<#theta_{photon}<173",300,0,0);
  m_h_deltathe->SetXTitle("#theta_{e1}-#theta_{photon}");

  m_angle = new TH1D("m_angle","Angle",300,0,0);
  m_angle->SetXTitle("Angle");

  m_h_cm = new TH2D("h_cm","cme vs cme corrected",300,0,3050,300,0,3050);
  m_h_cm->SetXTitle("CME");
  m_h_cm->SetXTitle("CME corrected");
  m_h_cm2 = new TH2D("h_cm2","cme vs effective sqrts",300,0,3050,300,0,1);
  m_h_cm3 = new TH2D("h_cm3","cme corr vs effective sqrts",300,0,3050,300,0,1);
  m_histos = true;
}
//=========================================================================
//  
//=========================================================================
void Analyse::loop (TChain*ch ) {

  Init(ch);
  if(!fChain) return;
  if (!m_histos) return;

  Long64_t nentries = fChain->GetEntries();
  for (Long64_t jentry=0; jentry<nentries;++jentry) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;

    fChain->GetEntry(jentry);
    double eb1r = eb1+deb1;
    double eb2r = eb2+deb2;

    if(eb1r < 0 || eb2r < 0) continue;

    TLorentzVector lve1(0,0,0,0);
    lve1.SetPxPyPzE(px1,py1,pz1,e1);
    TLorentzVector lve2(0,0,0,0);
    lve2.SetPxPyPzE(px2,py2,pz2,e2);
    TLorentzVector cm = lve1+lve2;
    
    Double_t th1 = lve1.Theta();
    m_h_the1->Fill(th1);
    Double_t th2 = lve2.Theta();
    m_h_the2->Fill(th2);
    Double_t eff_sqrts = TMath::Sqrt((TMath::Sin(th1)+TMath::Sin(th2)+TMath::Sin(th1+th2))/
                                     (TMath::Sin(th1)+TMath::Sin(th2)-TMath::Sin(th1+th2)));
    m_h_cme->Fill(eff_sqrts);
    m_h_e1->Fill(e1);
    m_h_e2->Fill(e2);
    double e1cor = e1;
    double e2cor = e2;
    m_n_p->Fill(nphotons);
    TLorentzVector max_ph(0,0,0,0);
    for (int i=0;i<nphotons;++i){
      TLorentzVector ph(0,0,0,0);
      ph.SetPxPyPzE(photdata[i][0],photdata[i][1],photdata[i][2],photdata[i][3]);
      if(ph.E()>max_ph.E() && ph.Theta()>(7*TMath::Pi()/180.) && ph.Theta()<((180-7)*TMath::Pi()/180.)){
        max_ph = ph;
      }
      double deltatheta=0;
      deltatheta = th1-ph.Theta();
      //if (th1<TMath::Pi()/2)
      //else
      //  deltatheta = ph.Theta()-th1;
      if (ph.Theta()>(7*TMath::Pi()/180.) && ph.Theta()<((180-7)*TMath::Pi()/180.)){

        m_h_deltathe->Fill(deltatheta);
        double angle = ph.Angle(lve1.Vect());
        m_angle->Fill(angle);
        if (TMath::Abs(angle)<0.2){
          e1cor+=ph.E();
        }
        angle = ph.Angle(lve2.Vect());
        if (TMath::Abs(angle)<0.2){
          e2cor+=ph.E();
        }
        
      }
    }
    if (nphotons && max_ph.E()){      
      m_h_max_ph_e->Fill(max_ph.E());
      m_n_ph_e->Fill(nphotons,max_ph.E());
    }
    m_h_e1_cor->Fill(e1cor);
    if (e1cor!=e1){
      m_h_e1_ratio->Fill(e1cor/e1);
    }
    m_h_e1_vs_e1_corr->Fill(e1,e1cor);


    TLorentzVector lve1corr(0,0,0,0);
    lve1corr.SetPxPyPzE(px1,py1,pz1,e1cor);
    TLorentzVector lve2corr(0,0,0,0);
    lve2corr.SetPxPyPzE(px1,py1,pz1,e2cor);
    TLorentzVector cmcor = lve1corr+lve2corr;
    
    m_h_cm->Fill(cm.E(),cmcor.E());
    m_h_cm2->Fill(cm.E(),eff_sqrts);
    m_h_cm3->Fill(cmcor.E(),eff_sqrts);

  }
  
  //m_h_e1_ratio->Divide(m_h_e1_cor,m_h_e1);
  
}


//=========================================================================
//  
//=========================================================================
void Analyse::saveHistos ( ) {
  m_f = new TFile("Analysis.root","RECREATE");
  m_h_cme->Write();
  m_h_e1->Write();
  m_h_e1_cor->Write();
  m_h_e1_ratio->Write();
  m_h_e1_vs_e1_corr->Write();
  m_h_e2->Write();
  m_n_p->Write();
  m_h_the1->Write();
  m_h_the2->Write();
  m_h_max_ph_e->Write();
  m_n_ph_e->Write();
  m_h_deltathe->Write();
  m_angle->Write();
  m_h_cm->Write();
  m_h_cm2->Write();
  m_h_cm3->Write();
  m_f->Close();
  
}
