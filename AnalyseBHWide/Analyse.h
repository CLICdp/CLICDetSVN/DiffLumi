// $Id: $
#ifndef ANALYSEBHWIDE_ANALYSE_H 
#define ANALYSEBHWIDE_ANALYSE_H 1

// Include files
#include "MCTuple.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
/** @class Analyse Analyse.h AnalyseBHWide/Analyse.h
 *  
 *
 *  @author Stephane Poss
 *  @date   2011-10-28
 */
class Analyse : public MCTuple {
public: 
  /// Standard constructor
  Analyse( ); 

  virtual ~Analyse( ); ///< Destructor

  void createHistos();
  
  void loop(TChain*ch);
  
  void saveHistos();
  

protected:

  TH1D*m_h_cme,*m_h_e1,*m_h_e1_cor,*m_h_e1_ratio;
  TH2D*m_h_e1_vs_e1_corr;
  TH1D*m_h_e2,*m_n_p;
  TH1D*m_h_the1,*m_h_the2,*m_h_max_ph_e;
  TH2D*m_n_ph_e;

  TH1D*m_h_deltathe,*m_angle;
  TH2D*m_h_cm,*m_h_cm2,*m_h_cm3;
  
  TFile*m_f;
  

private:
  bool m_histos;

};
#endif // ANALYSEBHWIDE_ANALYSE_H 
