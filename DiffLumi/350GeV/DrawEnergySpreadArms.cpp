#include "Utilities.h"
#include "ConvolutedFunctions.hh"
#include "DrawSpectra.hh"

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

using Utility::Integrate;
using Utility::DrawConfidenceInterval;

using namespace ConvolutedFunctions;

int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
  RootStyle::MyColor colors;
#endif

  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/Lumi350_2.root";

  double smin=-0.0046*1.1;
  double smax= 0.0054*1.1;

  // double smin = 1.1*getParameters()[2];
  // double smax = 1.1*getParameters()[3];

  HistoVector histograms( DrawSeparatedEnergySpread("PeakArms", fileWithELoss, smin, smax, 100000000, false));


  TF1 functions[2] = {getESFunction( getParameters() ), getESFunction( getParameters())};
  //fix upper beta dist parameter for tailFunction
  functions[1].SetParLimits(1, 0.0, 10.0);
  functions[1].SetParameter(1, 1.0);

  for(int i = 0; i <= 1; ++i) {
    functions[i].FixParameter( 2, -0.00467888 );
    functions[i].FixParameter( 3,  0.00549502 );
    functions[i].FixParameter( 4,  0.000136742);
  }

  std::vector<Color_t> lineColors;
  std::vector<Color_t> fillColors;
  lineColors.push_back(kRed-7);
  fillColors.push_back(kPink+2);

  lineColors.push_back(kBlue);
  fillColors.push_back(kCyan+1);

  TCanvas canv("canv","Energy Spread Peak and Arms");
  TGraphErrors* fitCL[2];
  TVirtualFitter::SetPrecision(0.001);
  for (int i = 0; i <= 1 ; ++i) {
    std::cout << "\n\n\n\n\n\n\n\n"  << std::endl;
    //    histograms[i].Fit( &functions[i] , "");
    // std::cout << "*******************************************************************************************"  << std::endl;
    //histograms[i].Fit( &functions[i] , "EV");
    // std::cout << "*******************************************************************************************"  << std::endl;
    histograms[i].Fit( &functions[i] , "WL");
    fitCL[i] = DrawConfidenceInterval(functions[i],0.99);
#ifdef USE_RootStyle
    Color_t color = colors.GetColor();
    RootStyle::SetAllColors( &histograms[i], color);
    functions[i].SetLineColor( lineColors.at(i) );
    fitCL[i]->SetLineColor( lineColors.at(i)  );
    fitCL[i]->SetFillColor( fillColors.at(i) );
    fitCL[i]->SetTitle("Fit + 99% C.L.");
#endif
  }//loop over histograms and fits


  histograms[0].Draw("hist");
  fitCL[0]->Draw("l3");

  histograms[1].Draw("hist,same");
  fitCL[1]->Draw("l3");

#ifdef USE_RootStyle
  {
    //RootStyle::AutoSetYRange (canv, 1.1);
    histograms[0].SetAxisRange(0,540, "y");
    TLegend* leg = RootStyle::BuildLegend(canv, 0.3, 0.9-4*0.0625, 0.6, 0.9);
    //leg->SetMargin(0.16);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.06);
  }
#endif

  functions[0].Draw("same");
  functions[1].Draw("same");


  canv.SaveAs("EnergySpreadPeakArms.eps");

  return 0;
}//Main
