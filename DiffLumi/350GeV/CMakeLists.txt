LINK_LIBRARIES( RootStyle )

ADD_EXECUTABLE( CreateLumiFileAbstract350  CreateLumiFileAbstract.cpp )
ADD_EXECUTABLE( FitReweighting350          FitReweighting.cpp         )
ADD_EXECUTABLE( FitBeamstrahlung350        FitBeamstrahlung.cpp       )
ADD_EXECUTABLE( DrawBDSSpread350           DrawBDSSpread.cpp          )
ADD_EXECUTABLE( DrawEnergySpreadArms350    DrawEnergySpreadArms.cpp   )
ADD_EXECUTABLE( DrwPkAm 		   DrwPkAm.cpp )
ADD_EXECUTABLE( MyFitBeamstrahlung350	   MyFitBeamstrahlung.cpp  ) 


TARGET_LINK_LIBRARIES( DrawBDSSpread350 Functions )
TARGET_LINK_LIBRARIES( FitBeamstrahlung350        Functions   )
TARGET_LINK_LIBRARIES( DrwPkAm           Functions   )
TARGET_LINK_LIBRARIES( MyFitBeamstrahlung350	Functions   )

TARGET_LINK_LIBRARIES( DrawEnergySpreadArms350    Functions   )

TARGET_LINK_LIBRARIES( FitReweighting350          Reweighting  Functions   )

TARGET_LINK_LIBRARIES( CreateLumiFileAbstract350  Reweighting Functions )
