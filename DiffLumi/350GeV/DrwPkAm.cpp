#include "Utilities.h"
#include "DrawSpectra.hh"
#include "ConvolutedFunctions.hh"

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>
#include <TString.h>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

TH1D MyDrawBDS(TString name, TString fileName, TString treeName="eleTree", double beamEnergy=175.0, double smin=0.99, double smax=0.99 , int entries=-1 , bool pk=true , bool isGP=false, bool scale=true);


int main (int ac, char **argv)
{

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  if(ac < 4) {
    std::cout << "Not enough input variables\n"
	      << "DrwPkAm BeamEnergy Rebin inputfile.root [inputfile2.root ...]"
	      << std::endl;
    return 1;
  }

  double nominalEnergy(175.0);

  nominalEnergy = std::atof(argv[1]);
  std::cout << "Nominal Beam Energy: " << nominalEnergy  << std::endl;
  if( nominalEnergy <= 0) {
    std::cout << "Error: Nominal Beam Energy " << nominalEnergy  << std::endl;
    exit(1);
  }

  int rebin(1);
  rebin = std::atoi(argv[2]);

  std::vector<TString> filenames;
  for (int i = 3; i < ac;++i) {
    filenames.push_back(TString(argv[i]));
  }
  
  
  std::vector<TH1D> histos;
  
  for (std::vector<TString>::iterator it = filenames.begin(); it != filenames.end(); ++it) {
    //TH1D hEnergy(DrawBDS(*it, *it, "MyTree", nominalEnergy, -0.006, 0.0065, 30000000, true));
    TH1D hEnergy(MyDrawBDS(*it, *it, "MyTree", nominalEnergy, -0.006, 0.007, 30000000, true));
    hEnergy.Rebin(rebin);
    hEnergy.Scale(1.0/double(rebin));
    histos.push_back(hEnergy);

    TH1D hEnergy2(MyDrawBDS("h2", *it, "MyTree", nominalEnergy, -0.006, 0.007, 30000000, false));
    hEnergy2.Rebin(rebin);
    hEnergy2.Scale(1.0/double(rebin));
    histos.push_back(hEnergy2);

  }

  TF1 esFunction( ConvolutedFunctions::getESFunction( ConvolutedFunctions::getParameters() ));
  esFunction.SetNpx(1000);

#ifdef USE_RootStyle
  RootStyle::PDFFile file;
  RootStyle::MyColor colors;
  TCanvas c("c","bds");
  bool first=true;
  for (std::vector<TH1D>::iterator it = histos.begin(); it != histos.end(); ++it) {
    c.cd();
    RootStyle::SetAllColors(&(*it), colors.GetColor() );
    std::cout << "Fitting (This can take quite some time if the binning is too fine)..." << it - histos.begin() << std::endl;
    (*it).Fit(&esFunction,"WL","same");
    (*it).SetTitle(Form("Beam Energy Spread at %i GeV", int(2*nominalEnergy)));

    if (first){
      (*it).Draw("");
      first = false;
    } else {
       (*it).Draw("same,hist");
    }

    TCanvas c2("c2","c2");
    c2.cd();
    (*it).Draw("hist");
    esFunction.Draw("same");
    (*it).SetAxisRange(0, 500,"Y");
    TString title = (*it).GetTitle();
    //title.ReplaceAll(".root","");
    std::cout << strlen(title.Data())  << std::endl;
    std::cout << title.Data()  << std::endl;
    RootStyle::BuildLegend(&c2, 0.2, 0.8, 0.9, 0.9);
    file.AddCanvas(&c2, title.Data());
  }
  file.CloseFile();
  RootStyle::BuildLegend(&c, 0.2, 0.8, 0.9, 0.9);
  c.SaveAs(Form("PeakArmHistograms_%i_Binning%i.eps", int(nominalEnergy), rebin));
#endif

  return 0;
}




TH1D MyDrawBDS(TString name, TString fileName, TString treeName, double beamEnergy, double smin, double smax, int entries, bool Pk, bool isGP, bool scale){

  TH1D histo(name, name, 1000,  smin, smax);
  histo.SetXTitle("x=#DeltaE/E_{Beam}");
  histo.SetYTitle("dN/dx");
  //  TH1D histo(name, name, 200,  -0.0046*1.1, 0.0054*1.1);
  //  TH1D histo(name, name, gBins,  -0.0046*1.1, 0.0054*1.1);
  //  TH1D histo(name, name, 1000, smin, smax);
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  TTree *tree = (TTree*)file->Get(treeName);
  if(not tree) {
    std::cerr << "Tree " << treeName << " not found in file " << fileName  << std::endl;
    exit(1);
  }

  double E1, E2;

  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);
 
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;

  std::cout << "Running over file: " << fileName  << " .... " << entries << std::endl;

  for (int i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);
    if(isGP) {
      E1 /= beamEnergy;
    }
        E1 += gRandom->Gaus(0.0, getSigma() );
    
    if(Pk){
      if ( E2 > 0.996 ) {
         histo.Fill(E1-1.0);
       }
    }
    else{
      if ( E2 < 0.996 ) {
         histo.Fill(E1-1.0);
       }
    }
    
  }
  std::cout << std::endl;
  file->Close();
  if(scale) histo.Scale(1./histo.Integral("width") );

  return histo;

}//DrawEnergySpread



/*

Now we want to fit the beam energy spread of the peak and of the arm separately:

* Find the separation between Peak and Arms region
* Draw the beam energy spread histogram for peak and arms separately
* Fit the two histograms separately

*/
