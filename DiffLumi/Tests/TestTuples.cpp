#include "TupleBHWide.hh" 
#include "Event.hh"
#include "Utilities.h"


#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>
#include <log4cplus/loggingmacros.h>
using namespace log4cplus;
using namespace log4cplus::helpers;

#include <vector>

//Logger log_1 =  Logger::getInstance(LOG4CPLUS_TEXT("test"));

int main(){
  tcout << LOG4CPLUS_TEXT("Entering main()...") << std::endl;
  //LogLog::getLogLog()->setInternalDebugging(true);
  Logger log_1 =  Logger::getInstance(LOG4CPLUS_TEXT("TestTuple"));
  ConfigureAndWatchThread configureThread(LOG4CPLUS_TEXT( Utility::GetWorkingDirectory()+  "/Logger/log4cplus.properties"), 5 * 1000);

  // TupleBHWide t1( Utility::GetWorkingDirectory() + "/RootFiles/BHWide_GP.root", 10000);
  // TupleBHWide t2( Utility::GetWorkingDirectory() + "/RootFiles/BHWide_MC_Smeared.root", 10000);
  // t1.SetBins(3,3,3);
  // t1.CreateEquiProbability( Utility::GetWorkingDirectory() + "/build/TestTupleBinning.root");


  TupleBHWide t1( Utility::GetWorkingDirectory() + "/RootFiles/BHWide_GP.root");
  TupleBHWide t2( Utility::GetWorkingDirectory() + "/RootFiles/BHWide_MC_Smeared.root");
  t1.LoadEquiProbability( Utility::GetWorkingDirectory() + "/build/TestTupleBinning.root");

  LOG4CPLUS_INFO(log_1, "\n"<<*(t1.GetBinning()) );
  //std::cout << *(t1.GetBinning())  << std::endl;
  std::vector<double> data(t1.GetBinning()->GetNBins(),0.0);
  
  //  TupleBHWide t2("../RootFiles/BHWide_MC_Smeared.root");
  t2.SetBinning(t1.GetBinning());

  LOG4CPLUS_INFO(log_1, "\n"<<*(t1.GetBinning()) );
  //std::cout << *(t2.GetBinning())  << std::endl;

  std::vector<Event> evt;
  std::vector<double> mc(t2.GetBinning()->GetNBins(),0.0);
  std::vector<double> weight(t2.GetBinning()->GetNBins(),0.0);
  //  t2.FillVectors(&mc,&evt,&weight);

  t1.FillVectors(&data);

  t2.FillVectors(&mc);
 
  LOG4CPLUS_INFO(log_1, "length data "  << data.size());
  LOG4CPLUS_INFO(log_1, "length mc "  << mc.size());
  LOG4CPLUS_INFO(log_1, "length weith "  << weight.size());
  LOG4CPLUS_INFO(log_1, "length evt "  << evt.size());
  return 0;

}
