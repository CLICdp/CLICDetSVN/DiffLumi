#include "StoreBinning.hh"

#include <iostream>
#include <cstdlib>


int main (int argc, char **argv ) {

  int nBins = 2;
  StoreBinning myBins(nBins,nBins,nBins);
  for (int i = 1; i <= nBins ;++i) {
    double x(1.0*double(i)/double(nBins));

    for (int j = 1; j <= nBins ;++j) {
      double y(1.0*double(j)/double(nBins));

      for (int k = 1; k <= nBins ;++k) {
	double z(1.0*double(k)/double(nBins));
	myBins.SetBinBoundaries(i-1, j-1, k-1, x, y, z);
	
      }//k
    }//j
  }//i
  

  //  myBins.setBinBoundaries(3, 0.6, 0.1, 0.6);

  std::cout << myBins  << std::endl;

  std::cout << "sorting now"  << std::endl;
  myBins.Sort();
  std::cout << myBins  << std::endl;

  std::cout << "sorting now"  << std::endl;
  myBins.Sort();
  std::cout << myBins  << std::endl;

  std::cout << "sorting now"  << std::endl;
  myBins.Sort();
  std::cout << myBins  << std::endl;


  double x, y, z;
  int globalBin, foundBin;
  if (argc < 5) {
    std::cerr << "Not enough parameters " << argc << std::endl;
    return 1;
  }
  x = std::atof(argv[1]);
  y = std::atof(argv[2]);
  z = std::atof(argv[3]);
  globalBin = std::atoi(argv[4]);
  foundBin = myBins.GetBinNumber(x, y, z);
  std::cout << std::setw(5) << x
	    << std::setw(5) << y
	    << std::setw(5) << z
	    << std::setw(5) << globalBin
	    << " Output " << foundBin
	    << std::endl;
  if ( foundBin == globalBin) {
    return 0;
  }
  return 1;
}
