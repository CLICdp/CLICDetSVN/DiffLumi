#include "Bin.hh"

#include <iostream>
#include <vector>
#include <algorithm>


int main (int argc, char **argv )
{

  if(argc < 6) {
    std::cerr <<  "Not enough parameters" << std::endl;
    return 1;
  }

  double x1 = std::atof(argv[1]);
  double y1 = std::atof(argv[2]);
  double z1 = std::atof(argv[3]);
  double x2 = std::atof(argv[4]);
  double y2 = std::atof(argv[5]);
  double z2 = std::atof(argv[6]);

  bool result(Bin(x1, y1, z1) < Bin(x2, y2, z2));

  std::cout << Bin(x1, y1, z1) 
	    << Bin(x2, y2, z2)
	    << " " << std::boolalpha << result << std::noboolalpha
	    << std::endl;

  std::cout.unsetf ( std::ios_base::boolalpha );

  if( result ) {
    return 0;
  } else {
    return 1;
  }

  // if( Bin(0.0, 0.0, 0.0) < Bin(0.0, 0.0, 1.0) ) {
  //   std::cout << "This Works: 1"  << std::endl;
  // }

  // if( Bin(1.0, 0.0, 0.0) < Bin(0.0, 0.0, 1.0) ) {
  //   std::cout << "This Works: 2"  << std::endl;
  // }

  // if( Bin(0.6, 0.6, 0.6) < Bin(1.0, 0.5, 0.5) ) {
  //   std::cout << "This Works: 3a"  << std::endl;
  // }


  // if( Bin(0.6, 0.6, 0.6) < Bin(1.0, 1.0, 1.0) ) {
  //   std::cout << "This Works: 3b"  << std::endl;
  // }


  // std::vector<Bin> bins;

  // bins.push_back(Bin(1.0, 1.0, 1.0));
  // bins.push_back(Bin(1.0, 0.5, 0.5));
  // bins.push_back(Bin(0.6, 0.6, 0.6));

  // std::sort(bins.begin(), bins.end());

  // for (std::vector<Bin>::iterator it = bins.begin(); it != bins.end(); ++it) {
  //   std::cout << *it  << std::endl;
  // }
 

}
