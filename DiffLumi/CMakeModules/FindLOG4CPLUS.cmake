# Try to ind Log4CPLus
# Once done this will define
# LOG4CPLUS_FOUND
# LOG4CPLUS_INCLUDE_DIRS
# LOG4CPLUS_LIBRARIES
# LOG4CPLUS_DEFINITIONS


MESSAGE ( STATUS "Looking for Log4CPlus" )

FIND_PATH ( LOG4CPLUS_INCLUDE_DIR log4cplus/logger.h
  HINTS 
  ${LOG4CPLUS_DIR}
  ${LOG4CPLUS_DIR}/include
  /usr/local/include 
  PATH_SUFFIXES include 
)

FIND_LIBRARY ( LOG4CPLUS_LIBRARY NAMES log4cplus liblog4cplus
  HINTS 
  ${LOG4CPLUS_DIR} 
  /usr/local/lib
  PATH_SUFFIXES
  lib
)

SET (LOG4CPLUS_INCLUDE_DIRS ${LOG4CPLUS_INCLUDE_DIR} )
SET (LOG4CPLUS_LIBRARIES ${LOG4CPLUS_LIBRARY} )

# MESSAGE ( STATUS ${LOG4CPLUS_LIBRARY} )
# MESSAGE ( STATUS ${LOG4CPLUS_DIR} )


INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LOG4CPLUS_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LOG4CPLUS  DEFAULT_MSG
                                  LOG4CPLUS_LIBRARY LOG4CPLUS_INCLUDE_DIR)

mark_as_advanced(LOG4CPLUS_INCLUDE_DIR LOG4CPLUS_LIBRARY)