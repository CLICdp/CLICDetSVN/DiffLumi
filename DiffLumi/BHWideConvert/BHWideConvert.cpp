#define BHWideConvert_cxx
#include "BHWideConvert.hh"
#include "MyTree.hh"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif


#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>


#include <cmath>


int BHWideConvert::Loop(std::string inputfile, std::string outputfile, Long64_t events)
{
  if (!fChain) return 1;

  //   In a ROOT session, you can do:
  //      Root > .L BHWideConvert.C
  //      Root > BHWideConvert t
  //      Root > t.GetEntry(12); // Fill t data members with entry number 12
  //      Root > t.Show();       // Show values of entry 12
  //      Root > t.Show(16);     // Read and show values of entry 16
  //      Root > t.Loop();       // Loop on all entries
  //

  //     This is the loop skeleton where:
  //    jentry is the global entry number in the chain
  //    ientry is the entry number in the current Tree
  //  Note that the argument to GetEntry must be:
  //    jentry for TChain::GetEntry
  //    ientry for TTree::GetEntry and TBranch::GetEntry
  //
  //       To read only selected branches, Insert statements like:
  // METHOD1:
  //    fChain->SetBranchStatus("*",0);  // disable all branches
  //    fChain->SetBranchStatus("branchname",1);  // activate branchname
  // METHOD2: replace line
  //    fChain->GetEntry(jentry);       //read all branches
  //by  b_branchname->GetEntry(ientry); //read only this branch
  //TFile* f0=TFile::Open("/home/sposs/Lumi_studies/NewDiffLumi/RootFiles/Lumi_GP_BHWeighted_Double_slives50_Random.root");
  TFile* f0=TFile::Open(TString(inputfile));
  TTree*temptree = NULL; 
  f0->GetObject("MyTree",temptree);
  if (temptree==NULL){
    std::cout << "Tree MyTree not found in "  << inputfile <<std::endl;
    return 1;
  } 
  MyTree*myt = new MyTree(temptree);

  TFile*f = new TFile(TString(outputfile),"RECREATE");
  TTree*t = new TTree("MyTree","MyTree");
  Double_t t_eb1,t_eb2,t_deb1,t_deb2, t_px1,t_py1,t_pz1,t_e1,t_px2,t_py2;
  Double_t t_deb1_g,t_deb2_g, t_E1,t_E2;
  Double_t t_pz2,t_e2;
  Double_t t_m_relproba;
  Int_t t_nphotons;
  Double_t t_photdata[800][4];
  t->Branch("eb1",&t_eb1,"eb1/D");
  t->Branch("eb2",&t_eb2,"eb2/D");
  t->Branch("deb1",&t_deb1,"deb1/D");
  t->Branch("deb2",&t_deb2,"deb2/D");
  t->Branch("deb1_g",&t_deb1_g,"deb1_g/D");
  t->Branch("deb2_g",&t_deb2_g,"deb2_g/D");
  t->Branch("E1",&t_E1,"E1/D");
  t->Branch("E2",&t_E2,"E2/D");

  t->Branch("px1",&t_px1,"px1/D");
  t->Branch("py1",&t_py1,"py1/D");
  t->Branch("pz1",&t_pz1,"pz1/D");
  t->Branch("e1",&t_e1,"e1/D");
  t->Branch("px2",&t_px2,"px2/D"); 
  t->Branch("py2",&t_py2,"py2/D");
  t->Branch("pz2",&t_pz2,"pz2/D");
  t->Branch("e2",&t_e2,"e2/D");
  t->Branch("nphotons",&t_nphotons,"nphotons/I");
  t->Branch("photdata",t_photdata,"photdata[nphotons][4]/D");
  t->Branch("RelativeProbability",&t_m_relproba,"RelativeProbability/D");

  if (fChain == 0) {
    std::cout << "Empty tree !"  << std::endl;
    return 1;
  }
  Long64_t nentries = fChain->GetEntries();
  
  if (events>0 && nentries>events){
    nentries = events;
  }

  std::cout << "Will convert "  <<  nentries << " events to the right format." << std::endl;
  Long64_t jentry1=0;
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(jentry, nentries);
#endif
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    myt->fChain->GetEntry(jentry);

    //do {
    //  myt->fChain->GetEntry(jentry1++);      
    //}while (myt->E1<0 || myt->E2<0);
    if ( fabs(eb1+deb1+deb1g - myt->E1) > 1e-6){
      std::cout << "Error, not synct evt: "  << jentry <<" e: " << eb1+deb1+deb1g <<" != "<< myt->E1<<std::endl;
      return 1;
    }

    // if (Cut(ientry) < 0) continue;
    t_eb1=myt->eb1;
    t_eb2=myt->eb2;
    t_E1=myt->E1;
    t_E2=myt->E2;
    t_deb1=myt->deb1;
    t_deb2=myt->deb2;
    t_deb1_g=myt->deb1_g;
    t_deb2_g=myt->deb2_g;
    t_px1 = (double)bpx2;
    t_py1 = (double)bpy2;
    t_pz1 = (double)bpz2;
    t_e1 = (double)bpe2;
    t_px2 = (double)bqx2;
    t_py2 = (double)bqy2;
    t_pz2 = (double)bqz2;
    t_e2 = (double)bqe2;
    t_nphotons = nphotons;
    t_photdata[0][0]=0;
    for (int i=0;i<nphotons;++i){
      for (int j=0;j<4;++j){
        t_photdata[i][j] = (double)photdata[i][j];
      }
    }
    t_m_relproba = (double)relproba;
    
    t->Fill();
  }
  std::cout << "  Done"  << std::endl;
  t->Write();
  f->Close();
  return 0;
}
