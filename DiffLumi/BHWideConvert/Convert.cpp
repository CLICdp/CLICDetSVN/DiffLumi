#include "MyTree.hh"
#include "BHWideConvert.hh"
#include <tclap/CmdLine.h>

int main(int argc, char* argv[]){

  std::string inputfile("");
  std::string outputfile("BHWide_merged.root");

  std::string inputBHWide("");
  Long64_t events(-1);

  try {  
    TCLAP::CmdLine cmd("Check the defaults.", ' ', "0.0");
    TCLAP::ValueArg<int> eventsArg("","events","Read only N events",false,events,"int",cmd);
    TCLAP::ValueArg<std::string> fileOutArg("","outputfile","File to use as output",false, outputfile, "string", cmd);
    TCLAP::ValueArg<std::string> fileInArg("","inputfile",
					   "Lumi root file to use as input for merging",
					   true, inputfile, "string", cmd);
    TCLAP::ValueArg<std::string> fileBHWideArg("","inputBHWide",
					       "File to use as BHWide input containing the h998 tree",true, 
					       inputBHWide, "string", cmd);
    
    cmd.parse( argc, argv );
    events = eventsArg.getValue();
    inputfile = fileInArg.getValue();
    outputfile = fileOutArg.getValue();
    inputBHWide = fileBHWideArg.getValue();
  }
  catch (TCLAP::ArgException &e){  // catch any exceptions 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return 1;
  }

  BHWideConvert bhwidec(inputBHWide);
  
  return bhwidec.Loop(inputfile, outputfile, events);
}
