      PROGRAM MAIN
*     ************ 
!---------------------------------------------------------------!
! Simple demo of the program BHWIDE for wide-angle Bhabha       !
!---------------------------------------------------------------!
      IMPLICIT REAL*8 (A-H,O-Z) 
! Histograms in blank common   
      COMMON / cglib / b(50000)
      parameter (npawc=2000000)
      real hh(npawc)
      common /pawc/ hh

      COMMON nEvents, filename, roots

! Read some command line parameters
      character*200 filename, temp
      integer nEvents, num_args
      real*8 roots

! Initialization of histogramming package --
! Here we use double precision HBOOK-like histogramming/plotting
! package GLIBK written by S. Jadach, (1990),  unpublished.
! General Output for everybody including Glibk
      nout = 16
      OPEN(nout,FILE='./demo.output')
      REWIND(nout)
*---------------------------------------------------------------------
      CALL GLIMIT(50000) 
      CALL GOUTPU(16)
      CALL HLIMIT(npawc) 


!     Read the command line parameters
      num_args = command_argument_count()

      if (num_args .lt. 3) then
         print*, 'Not enough parameters!', num_args
         print*, 'demo.exe <centreOfMassEnergy> <InputFile>
     $        <NumberOfEvents>'
         do i=1, num_args
            call get_command_argument(i, temp)
            print*, temp
         enddo
         goto 51
      endif

      call get_command_argument(1, temp)
      read (temp, '(F10.0)') rootS
      call get_command_argument(2, filename)
      call get_command_argument(3, temp)
      read (temp, '(I10)') nEvents

      print*, "CentreOfMassEnergy",roots
      print*, "Filename  ",filename
      print*, "NumberOfEvents",nEvents

! Simple demonstration program
      CALL BHWdem

 51   END

      SUBROUTINE BHWdem
*     *****************
!---------------------------------------------------------------!
! Simplistic demo of BHWIDE:                                    !
! Generating some low statistic sample, producing a few         !
! histograms and printing output.                               !
!---------------------------------------------------------------!
      IMPLICIT REAL*8 (A-H,O-Z) 
      PARAMETER( pi = 3.1415926535897932d0 )
c     changed the beamspread!!!!!!!
c      parameter (dbeam =1.4d-3)
c      parameter (dbeam =1.0d-3)
c      parameter (dbeam2=1.0d-3)
      parameter (dbeam =1.10d-3)
      parameter (dbeam2=1.10d-3)
c      parameter (dbeam =1.50d-3)
c      parameter (dbeam2=1.10d-3)
! Input/output files
      COMMON / INOUT  / NINP,NOUT
      COMMON / MOMSET / p1(4),q1(4),p2(4),q2(4),phot(100,4),nphot
      COMMON / WGTALL / wtmod,wtcrud,wttrig,wtset(300)       

      integer nEvents
      real*8 roots
      character*200 filename
      common nEvents, filename, roots

      DIMENSION  xpar(100),npar(100)       
      DIMENSION  xpar1(100),npar1(100)
      SAVE / INOUT /, / MOMSET /, / WGTALL /
      REAL*8 XB1,XB2,XDUM,DXB1,DXB2,XEB1,XEB2,PROBA
      COMMON /U_CIRCE/ XB1,XB2
      real*8 xd
      external drndm
      real rndm, rndm2
      real rr
      real prod, limit, srec
c      real px1,py1,pz1,pe1,qx1,qy1,qz1,qe1
      real px2,py2,pz2,pe2,qx2,qy2,qz2,qe2
c      real bpx1,bpy1,bpz1,bpe1,bqx1,bqy1,bqz1,bqe1
      real bpx2,bpy2,bpz2,bpe2,bqx2,bqy2,bqz2,bqe2
      real photdata(4,200)
      real relproba
      real cxb1, cxb2, prr, qrr, eb1, eb2, weight
      integer nphotons, event, seed, iprod, istdhep
      real deb1,deb2,deb1g,deb2g
      common / ntevent / 
c     $     px1,py1,pz1,pe1,qx1,qy1,qz1,qe1,
c     $     px2,py2,pz2,pe2,qx2,qy2,qz2,qe2,cxb1,cxb2, prr, qrr,
c     $     bpx1,bpy1,bpz1,bpe1,bqx1,bqy1,bqz1,bqe1,
     $     bpx2,bpy2,bpz2,bpe2,bqx2,bqy2,bqz2,bqe2,eb1,eb2,
     $     deb1,deb2, deb1g, deb2g, qrr, prr, cxb1, cxb2, weight,
     $     nphotons, photdata, relproba
c      common / nteventp / nphotons, photdata
c      COMMON/STDHEP/istr,lok,lnhout

      character*8 day,time

      character*100 fname
      fname = filename
!
      ninp = 5
! This open on unix machine is not necessary
!#    OPEN(ninp)
! Input parameters for  BHWIDE
      CMSEne = roots
      EBeam = CMSEne/2.0
      ThMinp = 7.0 !511.d-6/cmsene *57.3   ! Detector range ThetaMin [deg] for positrons
      ThMine = 7.0 !1.2/370.*57.3    ! Detector range ThetaMin [deg] for electrons
      ThMaxe = 180.-thmine    ! Detector range ThetaMax [deg] for electrons
      ThMaxp = 180.-thminp    ! Detector range ThetaMax [deg] for positrons
!     for iftach
!      ThMinp = 0.035*180/pi !511.d-6/cmsene *57.3   ! Detector range ThetaMin [deg] for positrons
!      ThMine = THMINP !7.0 !1.2/370.*57.3    ! Detector range ThetaMin [deg] for electrons
!      ThMaxe = 0.155*180/pi    ! Detector range ThetaMax [deg] for electrons
!      ThMaxp = thmaxe    ! Detector range ThetaMax [deg] for positrons

c      ThMinp = 0.001*180./pi !1 mrad
c      ThMine = ThMinp !
c      ThMaxe = 180.-thmine    ! Detector range ThetaMax [deg] for electrons
c      ThMaxp = 180.-thminp    ! Detector range ThetaMax [deg] for positrons


      EnMinp =   0.1    !100   ! Energy minimum [GeV] for detected positrons
      EnMine =   0.1    ! Energy minimum [GeV] for detected electrons

c      EnMinp =   1    !100   ! Energy minimum [GeV] for detected positrons
c      EnMine =   1    ! Energy minimum [GeV] for detected electrons

      Acolli = 180.0    ! Maximum acollinearity [deg] of final e+e-
      epsCMS =   1d-4   ! Infrared cut on photon energy 
!     Try both options for KeyWgt, result should be the same
      KeyWgt =   0   ! WT=1 events, for detector simulation
      KeyWgt =   0   ! weighted events
      KeyRnd =   1   ! RANMAR random numbers
      KeyOpt = 10*KeyWgt + KeyRnd
      KeyEWC = 0     ! QED corrections only 
c      KeyEWC =   1              ! Total O(alpha) ElectroWeak corr. included
!#      KeyLib = 1   ! ElectroWeak corrections from BABAMC (obsolete!)
c      KeyLib =   2   ! ElectroWeak corrections from ALIBABA
      KeyLib =   1   ! ElectroWeak corrections from BABAMC (obsolete!)
!#      KeyMod = 1   ! Hard bremsstr. matrix element from MODEL1
      KeyMod =   2   ! Hard bremsstr. matrix alement from MODEL2
      KeyPia =   1   ! Vacuum polarization option (0/1/2/3)
      KeyRad = 1000*KeyEWC + 100*KeyLib + 10*KeyMod + KeyPia
! Filling BHWIDE parameter arrays
      npar(1) = KeyOpt
      npar(2) = KeyRad
      xpar(1) = CMSEne
      xpar(2) = ThMinp
      xpar(3) = ThMaxp
      xpar(4) = ThMine
      xpar(5) = ThMaxe
      xpar(6) = EnMinp
      xpar(7) = EnMine
      xpar(8) = Acolli
      xpar(9) = epsCMS
 
      iprod = 0

      call datimh (day,time)
      read(time,'(i2,1x,i2,1x,i2)') ih,im,is
      read(day,'(i2,1x,i2,1x,i2)') id,imo,iy
      seed=2678400*imo+86400*id+3600*ih+60*im+is
      PRINT*,'Time ',seed
      seed = mod(seed,900000000)
c      seed = 10
c      PRINT*,'Time ',seed
      CALL MarINi(seed, 0 ,0)

      CALL BHWIDE(-1,xpar,npar)
*
c      call circes(0.01d0,0.01d0,CMSEne,2, 6, 19990415, 2)
c      call circel(dlum)
c      print*, 'lumi:',dlum
c     ********************************************************************************
c        HERE ARE THE INPUT FILE NAMES DEFINED
c     ********************************************************************************
c      open(2,file='lumi2.dat',status='old')
c      open(2,file='lumipara2.dat',status='old')
c      open(2,file='lumicorr.dat',status='old')
c      open(2,file='lumicorr_faker.dat',status='old')
c      open(2,file='lumicirce.dat',status='old')
c      open(2,file='lumicirce_fake.dat',status='old')
c      open(2,file='gp_diff_beams_random.dat',status='old')
c      open(2,file='lumi_diff_real.out',status='old')
c      open(2,file='diff_faker.out',status='old')
c      open(2,file='diff_faker_same.out',status='old')
c      open(2,file='diff_faker_15.out',status='old')
c      open(2,file='gp_diff_2.out',status='old')

c      open(2,file='gpnom.dat',status='old')
c      open(2,file='lumicirce_nom.dat',status='old')
c      open(2,file='lumicirce_nom_fake.dat',status='old')
c      open(2,file='lumicorr_nom.dat',status='old')
c      open(2,file='lumicirce_nom_realfake.dat',status='old')
c      open(2,file='lumicorr_nom_fake.dat',status='old')
c      open(2,file='lumicorr_underfake.dat',status='old')


c      open(2,file='gpasym2.out',status='old')
c      open(2,file='diff_nominal.dat',status='old')
c      open(2,file='diff_nom_fake.dat',status='old')
c      open(2,file='diff_realfake.dat',status='old')changed2
c      open(2,file='para_global.dat',status='old')

c      open(2,file='clic3tev.dat',status='old')

      open(2,file=fname,status='old')


!---------------------------------------------------------------------- 
!Book Ntuple

c      call HROPEN(2,'Data','pdata.hbook','N',1024,ISTAT)
      CALL HROPEN(58,'HISTOS','BHWideOut.hbook','N',2048,ISTAT)

      Call HBNT(998,'Data','D')
      CALL HBNAME(998,'ntevent', bpx2, '
c     $    px1,py1,pz1,pe1,qx1,qy1,qz1,qe1,
c     $     px2,py2,pz2,pe2,qx2,qy2,qz2,qe2,
c     $    bpx1,bpy1,bpz1,bpe1,bqx1,bqy1,bqz1,bqe1,
     $     bpx2,bpy2,bpz2,bpe2,bqx2,bqy2,bqz2,bqe2,
     $     eb1, eb2, deb1, deb2, deb1g, deb2g, qrr, prr, 
     $     cxb1, cxb2, weight,
     $     nphotons[0,200], photdata(4,nphotons), 
     $     relproba')


c      Call HBNT(999,'Photons','D')
c      CALL HBNAME(999,'nteventp', nphotons, 'nphotons[0,200],
c     $     photdata(4,nphotons)')


c      CALL HBNAME(998,'ntevent', nphotons,
c     $     'nphotons[0,100], photdata(4,nphotons)')

c      CALL HCDIR('//PAWC',' ')
      CALL HCDIR('//LIN1',' ')
c      CALL HCDIR('//HISTOS',' ')

c      CALL hbook1(3300,'photons',100,0.,100.,0.)
c      CALL hbook1(3000,'px',100,-1.,1.,0.)
*
!---------------------------------------------------------------------- 
! Number of events to be generated
      NevTot = nEvents

      WRITE(6,*) '   '
      WRITE(6,*) '--------------  BHWdem ----------------------'
      WRITE(6,'(F10.2,A)') NevTot/1.E3,' Kilo-events requested'
      WRITE(6,*)  ' =======> Generation starts...'
      WRITE(6,*) '   '

!---------------------------------------------------------------------- 

      weight = 1
      DO ievent = 1,NevTot
         xb1 = 1.d0
         xb2 = 1.d0
         xeb1 = 1.d0
         xeb2 = 1.d0
         dxb1 = 0.d0
         dxb2 = 0.d0
         proba = 1.d0

 160     read (2,*,end=200) xeb1,xeb2,dxb1,dxb2,dxb1g,dxb2g,proba

         xb1 = xeb1+dxb1+dxb1g
         xb2 = xeb2+dxb2+dxb2g

c         proba=0
c     Below is to be used when input is from CreateLumiFile (7columns)
c 160     continue
c         print *, xeb1,xeb2,dxb1,dxb2,dxb1g,dxb2g,proba
c         goto 200


c     only take events which are positive
         if(xb1.LT.0 .or. xb2.LT.0 ) then
            print *, 'Found negative lumi', xb1, xb2
            goto 160
         endif
         
c         xb1=float(ievent)/10
c         xb2=1

c         print *,'##############################'
 187     CALL BHWIDE( 0,xpar,npar)
         IF (ievent.LE.10) print*,'Beamstrahlung',xb1,xb2

c        call boost
 
c        call evtana(real(cmsene),0)

! Print four-momenta of first events
c         IF (ievent.LE.10) CALL lulist(1)
c        IF (ievent.LE.10) CALL dumps(nout)  
c        IF (ievent.LE.5) CALL dumps( 6)  
c        IF (ievent.LE.1) PRINT*,'Time ',seed

c        IF (ievent.LE.10) PRINT*,'Weight',wtmod,wtcrud,wttrig,wtset(300)

        IF (MOD(ievent,10000).EQ.0) WRITE(nout,*)' ievent = ',ievent
        IF (MOD(ievent,10000).EQ.0) WRITE(6   ,*)' ievent = ',ievent

! Calculate angles for events with non-zero weight
        thetp=0d0
        thetq=0d0
!        IF(KeyWgt.EQ.0 .OR. wtcrud*wttrig.ne.0d0) THEN
c        call boost

c

c           CALL HROUT(0,ICYCLE,' ')
c           CALL hf1(3300,real(nphotons),real(wtmod))
c           CALL hf1(3000,real(px2/cmsene),real(wtmod))
! Weight distribution
c           IF(KeyWgt .NE. 0) CALL hf1(3400,real(wtmod),1.)
c           call evtana(real(cmsene),0)


        cxb1 = real(xb1)
        cxb2 = real(xb2)
        
        call boost



c     call evtana(real(cmsene),5000)
c        IF (MOD(ievent,10).EQ.1) WRITE(6   ,*)' xb1 = ', xb1
        


!Filling Ntuple
c           px1 = real(p1(1))
c           py1 = real(p1(2))
c           pz1 = real(p1(3))
c           pe1 = real(p1(4))
c           qx1 = real(q1(1))
c           qy1 = real(q1(2))
c           qz1 = real(q1(3))
c           qe1 = real(q1(4))

           px2 = real(p2(1))
           py2 = real(p2(2))
           pz2 = real(p2(3))
           pe2 = real(p2(4))
           qx2 = real(q2(1))
           qy2 = real(q2(2))
           qz2 = real(q2(3))
           qe2 = real(q2(4))

           eb1 = real(xeb1)
           eb2 = real(xeb2)
           deb1 = real(dxb1)
           deb2 = real(dxb2)
           deb1g = real(dxb1g)
           deb2g = real(dxb2g)
           
           weight = WTMOD
           relproba = real(proba)
         IF (ievent.LE.10) CALL lulist(1)

         iprod = iprod + 1
c     print*,'Event  ',iprod
         if (weight .GT. 0) THEN

c     if(ievent.LE.10) then
c     Print some output to debug
c     xb1,xb2,px2,py2,pz2,pe2,qx2,qy2,
c     qz2,qe2,iprod,bxb1,bxb2,nphotons
c                 print*,bxb1,bxb2,bpx2,bpy2,bpz2,bpe2,bqx2,bqy2,bqz2,bqe2
c     $                ,cxb1, cxb2, weight, nphotons
c     $                ,qrr,prr
c     do j=1,nphotons
c     print*,photdata(1,j),photdata(2,j),photdata(3,j),
c     $                photdata(4,j)
c     enddo
c              endif
c           if(istdhep.gt.0)call stdhep_write

            call HFNT(998)
c     call HFNT(999)
         ELSE
            GOTO 187
         ENDIF
         
!        ENDIF
      ENDDO
 200  continue
      WRITE(6,*)  ' =======> Generation finished!'
      WRITE(6,*) '   '
      CALL BHWIDE( 2,xpar,npar)
      Xsec = xpar(10)
      Erel = xpar(11)
      Xcru = xpar(20)
! Print x-section
      WRITE(nout,*)'|||||||||||||||||||||||||||||||||||||||||||||||||||'
      WRITE(nout,'(a,f15.8,a)')' Xsec_accep = ',Xsec    ,' Nanob.'
     $                        ,' error      = ',Xsec*Erel,' Nanob.'
      WRITE(nout,*)'|||||||||||||||||||||||||||||||||||||||||||||||||||'

      WRITE(6,*)'|||||||||||||||||||||||||||||||||||||||||||||||||||'
      WRITE(6,'(a,f15.8,a)')' Xsec_accep = ',Xsec    ,' Nanob.'
     $                        ,' error      = ',Xsec*Erel,' Nanob.'
      WRITE(6,*)'|||||||||||||||||||||||||||||||||||||||||||||||||||'
!      WRITE(6,'(a,f10.6)'), 'Beamspread1, if used  ', dbeam
!      WRITE(6,'(a,f10.6)'), 'Beamspread2, if used  ', dbeam2
      WRITE(6,'(a,i7)'),    'Produced, total      ', iprod


!----------------------------------------------------------------------
!----------------------------------------------------------------------
c      CALL HROPEN(58,'HISTOS','pdata.hbk','N',1024,ISTAT)
c      CALL HCDIR('//PAWC',' ')
c      CALL HCDIR('//HISTOS',' ')
      CALL HROUT(0,ICYCLE,' ')
c      CALL HPRNT(998)
      CALL HREND('HISTOS')
      CLOSE(58)
      CALL HCDIR ('//PAWC',' ')
      END
!---------------------------------------------------------------------

      subroutine boost
      implicit none
c      real px1,py1,pz1,pe1,qx1,qy1,qz1,qe1
c      real px2,py2,pz2,pe2,qx2,qy2,qz2,qe2
c      real bpx1,bpy1,bpz1,bpe1,bqx1,bqy1,bqz1,bqe1
      real bpx2,bpy2,bpz2,bpe2,bqx2,bqy2,bqz2,bqe2
      real cxb1, cxb2, prr, qrr, eb1, eb2, deb1,deb2
      real deb1g, deb2g
      real photdata(4,200)
      real weight
      integer nphotons, event

      common / ntevent / 
c     $     px1,py1,pz1,pe1,qx1,qy1,qz1,qe1,
c     $     px2,py2,pz2,pe2,qx2,qy2,qz2,qe2,cxb1,cxb2, prr, qrr,
c     $     bpx1,bpy1,bpz1,bpe1,bqx1,bqy1,bqz1,bqe1,
     $     bpx2,bpy2,bpz2,bpe2,bqx2,bqy2,bqz2,bqe2,eb1,eb2,
     $     deb1, deb2, deb1g, deb2g,
     $     qrr, prr, cxb1, cxb2, weight,
     $     nphotons, photdata

c      common / nteventp / nphotons, photdata


      REAL*8 XB1,XB2
      COMMON /U_CIRCE/ XB1,XB2
      real*8 p1(4),q1(4),p2(4),q2(4),phot(100,4)
      integer nphot
      COMMON / MOMSET / p1,q1,p2,q2,phot,nphot
      real*8 xx
*
      real rp1(4),rq1(4),rp2(4),rq2(4),rphot(4,100)
*
      real p,v
      integer n,k
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)


      real paux(4),plab(4)
      integer i,j
*
* scale beams
      do i=1,4
         rp1(i) = p1(i)*xb1
         rq1(i) = q1(i)*xb2
      enddo
* scale final state (neglecting masses)
      xx = sqrt(xb1*xb2)
      do i=1,4
         plab(i) = rp1(i)+rq1(i)
         rp2(i) = xx*p2(i)
         rq2(i) = xx*q2(i)
         do j=1,nphot
            rphot(i,j) = xx*phot(j,i)
         enddo
      enddo
*
* and boost final state
*
      do i=1,3
         plab(i) = -plab(i)
      enddo
c      plab(4) = plab(4)*(2.-xb1-xb2)
*
      if (real(xx).ne.1. ) then
c     NaN if the two are the same, because plab is null vector?
         if (xb1.ne.xb2) then
            call ucopy (rp2,paux,4)
            call loren4 (plab, paux, rp2)
            call ucopy (rq2,paux,4)
            call loren4 (plab, paux, rq2)
            do j=1,nphot
               call ucopy (rphot(1,j),paux,4)
               call loren4 (plab, paux, rphot(1,j))
            enddo
c     added
c$$$         else 
c$$$            print *,'They were the SAME, same, Same'
c$$$            print *,'LabFrame boost', plab(1),plab(2),plab(3),plab(4)
c$$$            print *, 'P',rp1(1),rp1(2),rp1(3),rp1(4)
c$$$            print *, 'Q',rq1(1),rq1(2),rq1(3),rq1(4)
         endif
      endif
*
* fill Lund common
*
      n = 4+nphot
      k(1,1) = 21
      k(1,2) = -11
      do i=1,4
         p(1,i) = rp1(i)
      enddo
      p(1,5) = 0.5111e-3
*
      k(2,1) = 21
      k(2,2) = 11
      do i=1,4
         p(2,i) = rq1(i)
      enddo
      p(2,5) = 0.5111e-3
*
      k(3,1) = 1
      k(3,2) = -11
      do i=1,4
         p(3,i) = rp2(i)
      enddo
      p(3,5) = 0.5111e-3
*
      k(4,1) = 1
      k(4,2) = 11
      do i=1,4
         p(4,i) = rq2(i)
      enddo
      p(4,5) = 0.5111e-3
*
      do j=1,nphot
         k(j+4,1) = 1
         k(j+4,2) = 22
         do i=1,4
            p(j+4,i) = rphot(i,j)
         enddo
         p(j+4,5) = 0.
      enddo


c      bpx1 = real(rp1(1))
c      bpy1 = real(rp1(2))
c      bpz1 = real(rp1(3))
c      bpe1 = real(rp1(4))
c      bqx1 = real(rq1(1))
c      bqy1 = real(rq1(2))
c      bqz1 = real(rq1(3))
c      bqe1 = real(rq1(4))

      bpx2 = real(rp2(1))
      bpy2 = real(rp2(2))
      bpz2 = real(rp2(3))
      bpe2 = real(rp2(4))
      bqx2 = real(rq2(1))
      bqy2 = real(rq2(2))
      bqz2 = real(rq2(3))
      bqe2 = real(rq2(4))

      nphotons = nphot
      do i=1,4
         do j=1,nphotons
            photdata(i,j) = real(rphot(i,j))

c     photdata(4,j) = nphotons
c     j
         enddo
      enddo



      end

*     
      subroutine drndm(d)
      double precision d
      real r(1)
      CALL marran(r,1)
c      call varran(d,1)
      d = dble(r(1))
      end


      real function sprime(rts,ij1,ij2,igm,imas)
      implicit real*8 (a-h,o-z)
      real p,v,rts
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)
      double precision dp(3,5)
      PARAMETER( pi = 3.1415926535897932d0 )
      parameter (twopi = 2.*pi)
      parameter (cmax = 1.d0)
      drts = dble(rts)
*
      do i=1,5
         dp(1,i) = p(ij1,i)
         dp(2,i) = p(ij2,i)
         if (igm.ne.0) dp(3,i) = p(igm,i)
      enddo
*
      p1   = sqrt(dp(1,1)**2+dp(1,2)**2+dp(1,3)**2)
      p2   = sqrt(dp(2,1)**2+dp(2,2)**2+dp(2,3)**2)
      if (imas.eq.0) then
        am1  = 0.
        am2  = 0.
      elseif (imas.eq.15) then
        am1  = 1.777
        am2  = 1.777
      else      
        am1  = sqrt(max(dp(1,4)**2-p1**2,0.d0))
        am2  = sqrt(max(dp(2,4)**2-p2**2,0.d0))
      endif
      if (igm.eq.0) then
        coa  = dp(1,3)/p1
        if (abs(coa).gt.cmax) coa = sign(cmax,coa)
        ang1 = acos(coa)
        coa  = dp(2,3)/p2
        if (abs(coa).gt.cmax) coa = sign(cmax,coa)
        ang2 = acos(coa)
        if (ang1+ang2.lt.pi) then
          ang1 = pi-ang1
          ang2 = pi-ang2
        endif
      else
        pgm  = sqrt(dp(3,1)**2+dp(3,2)**2+dp(3,3)**2)
        coa  = (dp(1,1)*dp(3,1)+dp(1,2)*dp(3,2)+
     +          dp(1,3)*dp(3,3))/(p1*pgm)
        if (abs(coa).gt.cmax) coa = sign(cmax,coa)
        ang1 = acos(coa)
        coa  = (dp(2,1)*dp(3,1)+dp(2,2)*dp(3,2)+
     +          dp(2,3)*dp(3,3))/(p2*pgm)
        if (abs(coa).gt.cmax) coa = sign(cmax,coa)
        ang2 = acos(coa)
      endif
      coa  = (dp(1,1)*dp(2,1)+dp(1,2)*dp(2,2)+
     +        dp(1,3)*dp(2,3))/(p1*p2)
      if (abs(coa).gt.cmax) coa = sign(cmax,coa)
      ang3 = acos(coa)
      sang = ang1+ang2+ang3
      ang1 = twopi*ang1/sang
      ang2 = twopi*ang2/sang
      s1   = sin(ang1)
      s2   = sin(ang2)
      den  = sin(ang1+ang2)   
*
      eg   = (drts-0.5*am1**2/p1-0.5*am2**2/p2)*den/(den-s1-s2)
*
      sprime = real(sqrt(max(drts**2-2.d0*drts*eg,0.d0)))
*
      end

   
      subroutine evtana(rts,ish)
      COMMON/LUDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200)
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)
      parameter (conv=57.2958)
      real pz(5), pg(5)
      REAL*8 XB1,XB2
      COMMON /U_CIRCE/ XB1,XB2

      logical lacc
      parameter (dthet=0.2e-4)
c      parameter (dthet=0.4e-4)
*
      if (ish.eq.0) Lacc = .false.
*
      do i=1,5
         pz(i) = p(3,i)+p(4,i)
      enddo
      do i=5,n
        if (abs(k(i,1)).eq.1 .and. abs(k(i,2)).eq.22) then
           if ((p(i,1)*p(3,1)+p(i,2)*p(3,2)+p(i,3)*p(3,3))
     $          /(p(i,4)*p(3,4)) .gt. 0.9998 .or.
     $          (p(i,1)*p(4,1)+p(i,2)*p(4,2)+p(i,3)*p(4,3))
     $          /(p(i,4)*p(4,4)) .gt. 0.9998 ) then
              do j=1,5
                 pz(j) = pz(j)+p(i,j)
              enddo
           endif
        endif
      enddo
c      sprb = sqrt(max( (p(1,4)+p(2,4))**2
c     +                -(p(1,1)+p(2,1))**2
c     +                -(p(1,2)+p(2,2))**2
c     +                -(p(1,3)+p(2,3))**2 ,0.001))
      sprb = sqrt(max( pz(4)**2-pz(1)**2-pz(2)**2-pz(3)**2,
     $                 0.001))

      call hfill (ish+6,sprb,0.,1.)
      call hfill (ish+7,sprb,0.,1.)
      call hfill (ish+8,sprb,0.,1.)
*
*
*         theta smearing
*
      do i=3,4
         j = i+100
         p(i,4) = sqrt(p(i,1)**2+p(i,2)**2+p(i,3)**2)
         tho = acos(abs(p(i,3)/p(i,4)))
         call norran(r)
         rr = sqrt(p(i,1)**2+p(i,2)**2)
         dr = 1.+r*dthet*p(i,3)/rr
         p(j,1) = p(i,1)*dr
         p(j,2) = p(i,2)*dr
         p(j,3) = p(i,3)
         p(j,4) = sqrt(p(j,1)**2+p(j,2)**2+p(j,3)**2)
         thn = acos(abs(p(j,3)/p(j,4)))
         if (ish.eq.0) call hf1 (99,tho-thn,1.)
      enddo
*
      do i=1,5
        pg(i) = 0
        pz(i) = 0
      enddo
      iq1  = 0
      iq2  = 0
      do i=1,n
        if (abs(k(i,1)).eq.1 .and. abs(k(i,2)).eq.11) then
          if (k(i,2).gt.0) then
            iq1 = i
          else
            iq2 = i
          endif
        elseif (k(i,2).eq.22) then
          do jj=1,5
            pg(jj) = pg(jj) + p(i,jj)
          enddo
        endif
        if (k(i,1).eq.1) then
          do jj=1,5
            pz(jj) = pz(jj) + p(i,jj)
          enddo
        endif
      enddo
c      spr1 = sqrt(pz(4)**2-pz(1)**2-pz(2)**2-pz(3)**2)
c      spr2 = sqrt(xb1*xb2)*500.
c      print*,sprb,spr1,spr2
      if (iq1*iq2.eq.0) return
      ww = 1.
      th1 = 180.-acos(p(iq1,3)/p(iq1,4))*conv
      th2 = acos(p(iq2,3)/p(iq2,4))*conv
      call hf1(ish+30,th1,1.)
      call hf1(ish+30,th2,1.)
      call hf1(ish+31,th1,1.)
      call hf1(ish+31,th2,1.)
      call hf1(ish+40,min(th1,th2),1.)
      call hf1(ish+41,min(th1,th2),1.)

      call hf1 (ish+1,pg(4),ww)
*
*          analysis
*
c      call hf1 (2,pz(5),ww)
c      call hf1 (20,pz(5),ww)

      do i=1,n
        if (k(i,1) .eq. 1.and.abs(k(i,2)).eq.11) then
          th = acos(abs(p(i,3)/p(i,4)))
          if (k(i,2).gt.0.) then
            thp = th
            ip  = i
          else
            thm = th
            im  = i
          endif
        endif
      enddo
      th = 0.5*(thm+thp)*conv
      if (th.lt.7 .or. th.gt.15.) return
      if (ish.eq.0) lacc = .true.
*
      acol = abs(thm-thp)
      acom = abs(acos(abs(p(103,3)/p(103,4)))-
     $           acos(abs(p(104,3)/p(104,4)))  )
      call hf1 (ish+100,acol,1.)
      call hf1 (ish+101,acol,1.)
*
      call hf1 (ish+1100,acom,1.)
      call hf1 (ish+1101,acom,1.)
*
      call hf1 (ish+2000,acom-acol,1.)
*
      if (pg(4).lt.10.) then
        call hf1 (ish+200,acol,1.)
        call hf1 (ish+201,acol,1.)
      endif
      sprr = sprime(1.,ip,im,0,0)
      if (ish.eq.0) then
         call hf2 (ish+300,sprb/rts,acol,1.)
         call hf2 (ish+400,sprb/rts,sprr,1.)
         call hf2 (ish+430,acol,sprr,1.)
      endif
      call hf1 (ish+150,sprr,1.)
      call hf1 (ish+151,sprr,1.)
      call hf1 (ish+152,sprr,1.)
*
      sprm = sprime(1.,103,104,0,0)
      call hf1 (ish+1150,sprm,1.)
      call hf1 (ish+1151,sprm,1.)
      call hf1 (ish+1152,sprm,1.)
      call hf1 (ish+2050,sprm-sprr,1.)
*     
      if (ish.eq.0) then
         acol_n = acol
         sprr_n = sprr
      else if (lacc) then
         call hf1 (9000,acol-acol_n,1.)
         call hf1 (9050,sprr-sprr_n,1.)
         if (sprr_n.gt.0.9999) then
            call hf1 (9001,acol-acol_n,1.)
            call hf1 (9051,sprr-sprr_n,1.)
         endif
         if (sprr_n.lt.0.99) then
            call hf1 (9002,acol-acol_n,1.)
            call hf1 (9052,sprr-sprr_n,1.)
         endif
      endif

*
      end

C$$      SUBROUTINE STDHEP_INIT
C$$
C$$C...All real arithmetic in double precision.
C$$      IMPLICIT DOUBLE PRECISION(A-H, O-Z)
C$$
C$$C...Parameters.
C$$      COMMON/PYDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200)
C$$
C$$      COMMON/STDHEP/istr,lok,lnhout
C$$
C$$      
C$$      character *80 outputFile
C$$      call getenv("OUTPUTFILE",outputFile)
C$$      write(6,*)'OUTPUTFILE=',outputFile
C$$
C$$C initialize HEP logical units
C$$      lnhwrt=23
C$$      lnhrd=0
C$$      lnhdcy=0
C$$      lnhout=22
C$$      mstu(11)=lnhout
C$$      open(unit=lnhout,file='pythiaGen.lpt',status='new')
C$$      call stdxwinit(outputFile,'StdHep/Pythia example',
C$$     1               nevt,istr,lok)
C$$
C$$      RETURN
C$$      END
C$$
C$$      SUBROUTINE STDHEP_WRITE
C$$
C$$C      PARAMETER (NMXHEP=4000)
C$$C      COMMON/HEPEVT/NEVHEP,NHEP,ISTHEP(NMXHEP),IDHEP(NMXHEP),
C$$C     &JMOHEP(2,NMXHEP),JDAHEP(2,NMXHEP),PHEP(5,NMXHEP),VHEP(4,NMXHEP)
C$$C      DOUBLE PRECISION PHEP, VHEP
C$$
C$$      COMMON/STDHEP/istr,lok,lnhout
C$$
C$$c      call lunhep(1)
C$$      call pyhepc(1)
C$$
C$$      call stdxwrt(1,istr,lok)
C$$
C$$      RETURN
C$$      END
C$$
C$$      SUBROUTINE STDHEP_END
C$$
C$$      COMMON/STDHEP/istr,lok,lnhout
C$$
C$$C          Fill Stdhep common block 1
C$$      call stdflpyxsec(nevt)
C$$C          Write end-of-run record
C$$      call stdxwrt(200,istr,lok)
C$$      if(lok.ne.0) write(lnhout,*) ' Problem writing end run record'
C$$
C$$c...close event file
C$$      call stdxend(istr)
C$$
C$$      RETURN
C$$      END
