//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov 28 09:34:25 2011 by ROOT version 5.26/00a
// from TTree MyTree/MyTree
// found on file: /afs/cern.ch/eng/clic/work2/DiffLumi/LumiFileMC_Smeared.root
//////////////////////////////////////////////////////////

#ifndef MyTree_h
#define MyTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h> 

class MyTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        eb1;
   Double_t        eb2;
   Double_t        deb1;
   Double_t        deb2;
   Double_t        deb1_g;
   Double_t        deb2_g;
   Double_t        E1;
   Double_t        E2;
   Double_t        RelativeProbability;

   // List of branches
   TBranch        *b_eb1;   //!
   TBranch        *b_eb2;   //!
   TBranch        *b_deb1;   //!
   TBranch        *b_deb2;   //!
   TBranch        *b_deb1_g;   //!
   TBranch        *b_deb2_g;   //!
   TBranch        *b_E1;   //!
   TBranch        *b_E2;   //!
   TBranch        *b_RelativeProbability;   //!

   MyTree(TTree *tree=0);
   virtual ~MyTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MyTree_cxx
MyTree::MyTree(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/eng/clic/work2/DiffLumi/LumiFileMC_Smeared.root");
      if (!f) {
         f = new TFile("/afs/cern.ch/eng/clic/work2/DiffLumi/LumiFileMC_Smeared.root");
      }
      tree = (TTree*)gDirectory->Get("MyTree");

   }
   Init(tree);
}

MyTree::~MyTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MyTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MyTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MyTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eb1", &eb1, &b_eb1);
   fChain->SetBranchAddress("eb2", &eb2, &b_eb2);
   fChain->SetBranchAddress("deb1", &deb1, &b_deb1);
   fChain->SetBranchAddress("deb2", &deb2, &b_deb2);
   fChain->SetBranchAddress("deb1_g", &deb1_g, &b_deb1_g);
   fChain->SetBranchAddress("deb2_g", &deb2_g, &b_deb2_g);
   fChain->SetBranchAddress("E1", &E1, &b_E1);
   fChain->SetBranchAddress("E2", &E2, &b_E2);
   fChain->SetBranchAddress("RelativeProbability", &RelativeProbability, &b_RelativeProbability);
   Notify();
}

Bool_t MyTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MyTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MyTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MyTree_cxx
