#include "Utilities.h"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TFile.h>
#include <TH2D.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TString.h>
#include <TError.h>



#include <iostream>
#include <iomanip>



int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  TFile *file = TFile::Open( TString (Utility::GetWorkingDirectory() + "/RootFiles/Input_for_smearing_Dtheta_vs_E.root") );
  TH2D *EvsDeltaAngle;


  gErrorIgnoreLevel = kWarning;

  file->GetObject("H2DTHTREAT504", EvsDeltaAngle);

  int nBinsX = EvsDeltaAngle->GetNbinsX();
  double minE = EvsDeltaAngle->GetXaxis()->GetXmin();
  double maxE = EvsDeltaAngle->GetXaxis()->GetXmax();

  TH1D Resolution("AngularResolution", "AngularResolution;E [GeV]; #sigma_{#theta}", nBinsX, minE, maxE);

  std::vector<double> energy(nBinsX), dEnergy(nBinsX), resTheta(nBinsX), dResTheta(nBinsX);

  TF1 gaus("gaus","gaus(0)", -1e-3, 1e-3);
  gaus.SetParameters(10, 0.0, 1e-4);
  TCanvas canv("fitCanv","fitCanv");

  for (int i = 1; i <= nBinsX ;++i) {

    TH1D* hproj = EvsDeltaAngle->ProjectionY(Form("_px_%i",i),i,i);

    //EvsDeltaAngle->FitSlicesY(&gaus, i, i);
    if(hproj->GetEntries() == 0) continue;
    hproj->Fit(&gaus,"LQ");
    canv.SaveAs(Form ("angleTemp_%i.eps",i));

    std::cout 
      << std::setw(15) << gaus.GetParameter(0) << std::setw(15) << gaus.GetParError(0)
      << std::setw(15) << gaus.GetParameter(1) << std::setw(15) << gaus.GetParError(1)
      << std::setw(15) << gaus.GetParameter(2) << std::setw(15) << gaus.GetParError(2)
      << std::endl;
    //    Resolution.SetBinContent(i, gaus.GetParameter(2));
    energy[i-1]   = EvsDeltaAngle->GetXaxis()->GetBinCenter(i);
    dEnergy[i-1]  = EvsDeltaAngle->GetXaxis()->GetBinWidth(i)/2.0;
    resTheta[i-1] = gaus.GetParameter(2)/1e-6;
    dResTheta[i-1] = gaus.GetParError(2)/1e-6;
  }
  

  TGraphErrors ResGraph(nBinsX, &energy[0], &resTheta[0], &dEnergy[0], &dResTheta[0]);
  ResGraph.SetTitle(";E [GeV]; #sigma_{#theta} [#murad]");

  ResGraph.Draw("AP");

  canv.SaveAs("AngularResolution.eps");

  file->Close();

  return 0;
}
