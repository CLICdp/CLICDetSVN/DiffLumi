/*
 * BHWidePlots.cpp
 *
 *  Created on: Aug 6, 2012
 *      Author: stephane
 */

#include "Utilities.h"

#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

#include <TCanvas.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLorentzVector.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>

double deltaPhi(double phi1, double phi2){
  double dphi = phi1-phi2;
  if(dphi>TMath::Pi()) dphi -= 2*TMath::Pi();
  else if(dphi<=-TMath::Pi()) dphi += 2*TMath::Pi();
  return dphi;
}


/// Strahinjas BetaColl variable, see his LCD note
double getBetaColl(double Theta1, double Theta2) {
  return sin(Theta1+Theta2)/(sin(Theta1)+sin(Theta2));
}

/// effective sqrt{s}, but depending on which way the system is moving we are
/// positive or negative, we also subtract the original value from 1 so that the
/// peak is at zero, like it is for betaColl
double getRelCME(double Theta1, double Theta2) {
  int factor=-1;// we start with -1 so that we have complete correlation between relCME and betaColl
  if((Theta2+Theta1) < TMath::Pi() ) { 
    Theta1 = TMath::Pi() - Theta1; 
    Theta2 = TMath::Pi() - Theta2; 
    factor *= -1;
  }
  return factor*(1-(TMath::Sqrt((TMath::Sin(Theta1)+TMath::Sin(Theta2)+TMath::Sin(Theta1+Theta2))/
				(TMath::Sin(Theta1)+TMath::Sin(Theta2)-TMath::Sin(Theta1+Theta2)))));
}



int plot(TString filename){
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  TFile*file = TFile::Open(filename);
  if(not file) return 1;
  TTree*t = static_cast<TTree*>(file->Get("MyTree"));
  double e1,e2,px1,px2,py1,py2,pz1,pz2,E1,E2;
  t->SetBranchAddress("e1", &e1);
  t->SetBranchAddress("e2", &e2);
  t->SetBranchAddress("E1", &E1);
  t->SetBranchAddress("E2", &E2);
  t->SetBranchAddress("px1", &px1);
  t->SetBranchAddress("py1", &py1);
  t->SetBranchAddress("pz1", &pz1);
  t->SetBranchAddress("px2", &px2);
  t->SetBranchAddress("py2", &py2);
  t->SetBranchAddress("pz2", &pz2);


  TH1D h_relcme("h_relcme","relative cme",1000,0,1.0);
  h_relcme.SetXTitle("x=#sqrt{s_{acol}}/#sqrt{s_{nom}}");
  h_relcme.SetYTitle("dN/dx");
  h_relcme.SetLineColor(kRed);
  h_relcme.GetYaxis()->SetTitleOffset(1.35);

  TH1D h_relcmeMod("h_relcmeMod","Modified relative cme",1000,-1,1.0);
  h_relcme.SetXTitle("x=#sqrt{s_{acol}}/#sqrt{s_{nom}}");
  h_relcme.SetYTitle("dN/dx");
  h_relcme.SetLineColor(kRed);
  h_relcme.GetYaxis()->SetTitleOffset(1.35);

  TH1D h_acop("h_acop","acoplanarity",1000,0,TMath::Pi());
  h_acop.SetXTitle("Acop");
  h_acop.SetYTitle("dN/dAcop");
  h_acop.SetLineColor(kRed);
  h_acop.GetYaxis()->SetTitleOffset(1.35);

  double betaCollRange=1.0;
  TH1D h_betaColl("h_betaColl","Collinear Boost", 1000, -betaCollRange,betaCollRange);
  h_betaColl.SetXTitle("#beta_{coll}");
  h_betaColl.SetYTitle("dN/d#beta_{coll}");

  TH2D h_betaVSaco("h_betaVSaco","Collinear Boost vs. Rel CMS", 1000,  -betaCollRange,betaCollRange, 1000, -1,1);
  h_betaVSaco.SetXTitle("#beta_{coll}");
  h_betaVSaco.SetYTitle("x");

  TH2D h_betaVSacoMod("h_betaVSacoMod","Collinear Boost vs. Rel CMS", 1000,  -betaCollRange,betaCollRange, 1000, -1,1);
  h_betaVSacoMod.SetXTitle("#beta_{coll}");
  h_betaVSacoMod.SetYTitle("#bar{x}");

  TH2D h_deltaEVSaco("h_deltaEVSaco","DeltaE/E vs. Rel CMS", 1000,  -betaCollRange,betaCollRange, 1000, -1,1);
  h_deltaEVSaco.SetXTitle("#sqrt{s_{aco}}");
  h_deltaEVSaco.SetYTitle("E_{1}-E_{2}/(2E_{Beam})");

  TH2D h_deltaEVSbetaColl("h_deltaEVSbetaColl","DeltaE/E vs. Rel CMS", 1000,  -betaCollRange,betaCollRange, 1000, -1,1);
  h_deltaEVSbetaColl.SetXTitle("#beta_{Coll}");
  h_deltaEVSbetaColl.SetYTitle("E_{1}-E_{2}/(2E_{Beam})");


  double maxE1E2(1.01);
  double minE1E2(0.99);

  TH2D h_E1E2_AcoP("h_E1E2_AcoP","E1 vs E2",200,minE1E2, maxE1E2,200,minE1E2, maxE1E2);
  h_E1E2_AcoP.SetXTitle("E_{1}/E_{Beam}");
  h_E1E2_AcoP.SetYTitle("E_{2}/E_{Beam}");

  TH2D h_E1E2_AcoM("h_E1E2_AcoM","E1 vs E2",200,minE1E2, maxE1E2,200,minE1E2, maxE1E2);
  h_E1E2_AcoM.SetXTitle("E_{1}/E_{Beam}");
  h_E1E2_AcoM.SetYTitle("E_{2}/E_{Beam}");


  TH2D h_E1E2_BetaP("h_E1E2_BetaP","E1 vs E2",200,minE1E2, maxE1E2,200,minE1E2, maxE1E2);
  h_E1E2_BetaP.SetXTitle("E_{1}/E_{Beam}");
  h_E1E2_BetaP.SetYTitle("E_{2}/E_{Beam}");

  TH2D h_E1E2_BetaM("h_E1E2_BetaM","E1 vs E2",200,minE1E2, maxE1E2,200,minE1E2, maxE1E2);
  h_E1E2_BetaM.SetXTitle("E_{1}/E_{Beam}");
  h_E1E2_BetaM.SetYTitle("E_{2}/E_{Beam}");



  TH1D h_e1("h_e1","Energy e1",1000,0,1600);
  h_e1.SetXTitle("E [GeV]");
  h_e1.SetYTitle("dN/dE");
  h_e1.SetLineColor(kRed);

  TH2D h_e1_e2("h_e1_e2","E1 vs E2",200,0,1550./1500.0,200,0,1550./1500.0);
  h_e1_e2.SetXTitle("E_{1}/E_{Beam}");
  h_e1_e2.SetYTitle("E_{2}/E_{Beam}");
  //h_e1_e2.GetYaxis()->SetTitleOffset(1.3);

  TH2D h_e1_e2_cut_init("h_e1_e2_cut_init","E1 vs E2",200,0,1550./1500,200,0,1550./1500);
  h_e1_e2_cut_init.SetXTitle("E_{1}/E_{Beam}");
  h_e1_e2_cut_init.SetYTitle("E_{2}/E_{Beam}");
  //h_e1_e2_cut_init.GetYaxis()->SetTitleOffset(1.3);
  TH2D h_e1_e2_cut("h_e1_e2_cut","E1 vs E2",200,0,1550./1500,200,0,1550./1500.0);
  h_e1_e2_cut.SetXTitle("E_{1}/E_{Beam}");
  h_e1_e2_cut.SetYTitle("E_{2}/E_{Beam}");
  //h_e1_e2_cut.GetYaxis()->SetTitleOffset(1.3);

  TH2D h_evsef("h_evsef","sqrts vs sqrts'",500,0,1.01,500,0,3100);
  h_evsef.SetXTitle("#sqrt{s}");
  h_evsef.SetYTitle("#sqrt{s'}");
  h_evsef.GetYaxis()->SetTitleOffset(1.3);
  TH1D h_sqrtsprime("h_sqrtsprime","#sqrt{s'}",300,0.,3100./3000.0);
  h_sqrtsprime.SetXTitle("x=#sqrt{s'}/#sqrt{s_{nom}}");
  h_sqrtsprime.SetYTitle("dN/dx");

  TH1D h_sqrts_cut("h_sqrts_cut","#sqrt{s}",1000,0.,1550./1500.);
  h_sqrts_cut.SetXTitle("#sqrt{s} [GeV]");

  TH2D h_e1_init_vs_e1_final("h_e1_init_vs_e1_final","e1 init vs e1 fin",500,0,200,500,0,1500);

  Long64_t nentries = t->GetEntries();
  //  nentries=10000000;
  for (Long64_t entry = 0; entry<nentries; ++entry){
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(entry, nentries);
#endif

    t->GetEntry(entry);
    TLorentzVector part_e1(px1,py1,pz1,e1);
    TLorentzVector part_e2(px2,py2,pz2,e2);
    TLorentzVector sum = part_e1+part_e2;

    double betacoll = getBetaColl(part_e1.Theta(), part_e2.Theta());
    double relcme = effective_sqrts(part_e1.Theta(),part_e2.Theta());
    double relcmeMod = getRelCME(part_e1.Theta(),part_e2.Theta());

    double s(sqrt(sum.Mag2()));
    if ( s > 1500){// && 2*sqrt(part_e1.E()*part_e2.E())> 1500 && part_e1.E()>150 && part_e2.E()>150) {
      double sini = sqrt(E1*E2);
      h_evsef.Fill(sini,s);
      h_sqrts_cut.Fill(sini);
      h_e1_e2_cut_init.Fill(E1,E2);
      h_e1_e2_cut.Fill(part_e1.E()/1500.0,part_e2.E()/1500.0);

      if (E1*1500.<150){
	//std::cout << "entry "  <<entry << std::endl;
       h_e1_init_vs_e1_final.Fill(E1*1500.,part_e1.E());
      }
    }

    if(betacoll < 0 ) {
      h_E1E2_BetaM.Fill(E1,E2);
    } else {
      h_E1E2_BetaP.Fill(E1,E2);
    }

    if(relcmeMod < 0 ) {
      h_E1E2_AcoM.Fill(E1,E2);
    } else {
      h_E1E2_AcoP.Fill(E1,E2);
    }


    h_sqrtsprime.Fill(s/3000.0);
    h_relcme.Fill(relcme);
    h_relcmeMod.Fill(relcmeMod);
    h_betaColl.Fill(betacoll);
    h_e1.Fill(part_e1.E());
    h_e1_e2.Fill(part_e1.E()/1500.0,part_e2.E()/1500.0);
    h_acop.Fill(fabs(fabs(deltaPhi(part_e1.Phi(),part_e2.Phi()))-TMath::Pi()));

    h_betaVSaco.Fill(betacoll, relcme);
    h_betaVSacoMod.Fill(betacoll, relcmeMod);
    h_deltaEVSaco.Fill((e1-e2)/3000.0, relcmeMod);
    h_deltaEVSbetaColl.Fill((e1-e2)/3000.0, betacoll);

  }// All Entries

  TCanvas c1("c1","c1");
  c1.SetLogy();
  h_relcme.Scale(1/h_relcme.Integral());
  h_relcme.Draw();
  c1.SaveAs("BHWide_Relcme.eps");

  c1.SetLogy();
  h_relcmeMod.Draw();
  c1.SaveAs("BHWide_RelcmeMod.eps");

  c1.Clear();
  h_betaColl.Draw();
  c1.SaveAs("BHWide_BetaColl.eps");

  c1.SetRightMargin(0.16);
  c1.SetLogz(kTRUE);
  c1.SetLogy(kFALSE);

  h_betaVSaco.Draw("colz");
  c1.SaveAs("BHWide_BetaCollvsAco.eps");

  h_betaVSacoMod.Draw("colz");
  c1.SaveAs("BHWide_BetaCollvsAcoMod.eps");

  h_deltaEVSaco.Draw("colz");
  c1.SaveAs("BHWide_DeltaEvsAco.eps");

  h_deltaEVSbetaColl.Draw("colz");
  c1.SaveAs("BHWide_DeltaEvsBetaColl.eps");

  h_E1E2_AcoM.Draw("colz");
  c1.SaveAs("InitialEnergies_AcoM.eps");

  h_E1E2_AcoP.Draw("colz");
  c1.SaveAs("InitialEnergies_AcoP.eps");


  h_E1E2_BetaM.Draw("colz");
  c1.SaveAs("InitialEnergies_BetaM.eps");

  h_E1E2_BetaP.Draw("colz");
  c1.SaveAs("InitialEnergies_BetaP.eps");



  TCanvas c1b("c1b","c1b");
  c1b.SetLogy();
  h_acop.Scale(1/h_acop.Integral());
  h_acop.Draw();
  c1b.SaveAs("BHWide_Acoplanarity.eps");

  TCanvas c2("c2","c2");
  c2.SetLogy();
  h_e1.Scale(1/h_e1.Integral());
  h_e1.Draw();
  c2.SaveAs("BHWide_EnergyElectron.eps");

  TCanvas c3("c3","c3");
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c3);
#endif
  c3.SetLogz();
  h_e1_e2.Scale( 1.0 / h_e1_e2.Integral("width") );
  h_e1_e2.SetAxisRange(1e-3, 1e3,"Z");
  h_e1_e2.Draw("colz");
  c3.SaveAs("BHWide_E1vsE2.eps");

  TCanvas c3_cutinit("c3_cutinit","c3_cutinit");
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c3);
#endif
  c3_cutinit.SetLogz();
  h_e1_e2_cut_init.Draw("colz");
  c3_cutinit.SaveAs("BHWide_E1vsE2_cut_init.eps");

  TCanvas c3b("c3b","c3b");
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c3b);
#endif
  c3b.SetLogz();
  h_e1_e2_cut.Draw("colz");
  c3b.SaveAs("BHWide_E1vsE2_cut.eps");

  TCanvas c4("c4","c4");
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c4);
#endif
  c4.SetLogz();
  h_evsef.Draw("colz");
  c4.SaveAs("sqrts_vs_sqrtsprime.eps");

  TCanvas c5("c5","c5");
  //c4.SetLogz();
  h_sqrtsprime.Scale ( 1.0 / h_sqrtsprime.Integral());
  h_sqrtsprime.GetYaxis()->SetTitleOffset(1.2);
  h_sqrtsprime.Draw();
  c5.SaveAs("sqrtsprime.eps");

  TCanvas c6("c6","c6");
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c6);
#endif
  c6.SetLogy();
  h_sqrts_cut.Draw();
  c6.SaveAs("sqrts_cut.eps");

  TCanvas c7("c7","c7");
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c7);
#endif
  c7.SetLogz();
  h_e1_init_vs_e1_final.Draw("colz");
  c7.SaveAs("e1init_vs_e2fin.eps");

  return 0;
}


int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  TString filename("");
  if(argc < 2) {
    std::cerr << "No filename given... abort\n"
	      << "BHWidePlots bhwhide_tuple.root"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    ;
  }

  return plot(filename);

}
