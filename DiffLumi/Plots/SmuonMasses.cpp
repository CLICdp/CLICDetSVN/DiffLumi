#include <TCanvas.h>
#include <TPad.h>
#include <TGraphErrors.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TMath.h>
#include <TLine.h>


#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <vector>
#include <iostream>

int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif



  // double smuonmasses[nFits] = { 1011.67,1011.65,1011.69,1011.61, 1011.7,1011.63,1011.66,1011.63, 1011.6, 1011.5,1011.61,1011.42,
  // 				1011.65,1011.64,1011.58,1011.39,1011.63,1011.56,1011.67, 1011.5, 1011.3,1011.42,1011.62,1011.38,
  // 				1011.59,1011.38,1011.19,1011.61, 1011.37};
  // double smuonerrors[nFits] = {0.0366892,0.0355886,0.0623576,0.0304144, 0.032895,0.0391323,0.0690497,0.0440234,0.0386718,0.0537801,
  // 			       0.0338573,0.0519472,0.0435964, 0.108593,0.0582851,0.0487877,0.0334735,0.0386894, 0.211369,0.0460069,
  // 			       0.0277925,0.0372372,0.0210156,0.0382493,0.0746058,  0.03749,0.0817942,0.0421799,0.0463253};
  // double neutrmasses[nFits] = {342.78 ,342.74 ,342.83 ,342.64 ,342.83 ,342.69 ,342.79 ,342.68 ,342.64 ,342.42 ,342.62 ,342.26 ,
  // 			       342.73 ,342.72 ,342.59 , 342.2 ,342.69 ,342.53 ,342.76 ,342.44 ,341.96 ,342.26 ,342.65 ,342.18 ,
  // 			       342.59 ,342.15 , 341.7 ,342.65 ,342.12 };
  // double neutrerrors[nFits] = {0.0832744,0.0682089, 0.115343,0.0710667,0.0692955,0.0837979, 0.145561,0.0815774,0.0741971,
  // 			       0.0971164,0.0676326, 0.110206, 0.113787,0.0749349, 0.154652,0.0893701,0.0678057,0.0795172, 
  // 			       0.163455,0.0966994,0.0613262,0.0773192,0.0553603,0.0809633, 0.140024,0.0796511, 0.150214, 
  // 			       0.107358,0.0881719};

  // double smuonmasses[] = {};
  // double smuonerrors[] = {};
  // double neutrmasses[] = {};
  // double neutrerrors[] = {};

  // // BW_5 Run
  // double smuonmasses[] = {1011.69, 1011.63, 1011.66, 1011.64, 1011.56, 1011.30, 1011.62, 1011.37, 1011.18};
  // double smuonerrors[] = {0.032365, 0.0467944, 0.0688951, 0.0467844, 0.0367327, 0.0302396, 0.0201049, 0.0414968, 0.0768408};
  // double neutrmasses[] = {342.831, 342.692, 342.784, 342.701, 342.528, 341.964, 342.654, 342.139, 341.662};
  // double neutrerrors[] = {0.0650372, 0.082228, 0.166143, 0.106862, 0.0712733, 0.0645843, 0.0566434, 0.0859871, 0.159158};


  //BW 6 Run
  double smuonmasses[] = {1011.69 , 1011.63 , 1011.66 , 1011.64 , 1011.56 , 1011.3  , 1011.62 , 1011.37 , 1011.18 , 1011.14 };
  double smuonerrors[] = {0.032365 , 0.0467944, 0.0688951, 0.0467844, 0.0367327, 0.0302396, 0.0201049, 0.0414968, 0.0768408, 0.021448};
  double neutrmasses[] = {342.831, 342.692, 342.784, 342.701, 342.528, 341.964, 342.654, 342.139, 341.662, 341.578};
  double neutrerrors[] = {0.0650372, 0.082228, 0.166143, 0.106862, 0.0712733, 0.0645843, 0.0566434, 0.0859871, 0.159158, 0.0727185};


  std::string Binnings[] = {"30x20x20","30x30x30", "35x20x20", "45x20x20", "50x40x40", "60x50x50", "70x30x30", "75x40x40", "75x50x50", "80x50x50" };

  const int nFits = sizeof(smuonmasses)/sizeof(double);

  std::cout << nFits  << std::endl;
  std::vector<double> xValues(nFits);
  for (int i = 0; i <nFits ;++i) { xValues[i]=i+1; }
  std::vector<double> xErrs(nFits,0.0);


  std::cout << "Smuon mass average: " 
	    << TMath::Mean(nFits, smuonmasses)
	    << " +- " << TMath::RMS(nFits, smuonmasses)
	    << std::endl;

  std::cout << "Neutr mass average: " 
	    << TMath::Mean(nFits, neutrmasses)
	    << " +- " << TMath::RMS(nFits, neutrmasses)
	    << std::endl;

  TCanvas c1("c1","SmuonErrors");
  c1.SetFrameBorderMode(0);
  double topMargin(c1.GetTopMargin());
  double bottomMargin(c1.GetBottomMargin());
  //double rightMargin(c1.GetRightMargin());
  //double leftmargin(c1.GetLeftMargin()); //left Margin is fine

  //c1.Divide(1,2,-1,-1);
  //c1.Divide(1,2,0,0);
  std::vector<TPad*> pads(2,NULL);

  double halfSpace((1-(topMargin + bottomMargin))/2);

  double halfway(1.0-(halfSpace+topMargin));
  pads[0] = new TPad("Pad1", "Pad1", 0.0, halfway, 1.0, 1.0-topMargin, kWhite, short(0), short(0));
  pads[1] = new TPad("Pad2", "Pad2", 0.0, 0.0, 1.0, halfway, kWhite, short(0), short(0));
  
  pads[0]->SetBottomMargin(0);
  pads[0]->SetTopMargin(0.0);

  pads[1]->SetTopMargin(0);
  pads[1]->SetBottomMargin(0.18*(1.0/(halfway)));

  pads[0]->SetLeftMargin(0.22);
  pads[1]->SetLeftMargin(0.22);

  c1.cd();
  pads[0]->Draw();
  c1.cd();
  pads[1]->Draw();


  TGraphErrors smuons(nFits, &xValues[0], smuonmasses, &xErrs[0], smuonerrors);
  TGraphErrors neutrs(nFits, &xValues[0], neutrmasses, &xErrs[0], neutrerrors);

  // smuons.Fit("pol0");
  // neutrs.Fit("pol0");

  pads[0]->cd();
  smuons.Draw("AP");
  smuons.SetMinimum(1011.05);
  smuons.SetMaximum(1011.9);

  TLine smuonLine(0.5, 1011.77, 10.5, 1011.77);
  smuonLine.SetLineStyle(kDashed);
  smuonLine.Draw();

  pads[1]->cd();
  neutrs.Draw("AP");
  neutrs.SetMinimum(341.4);
  neutrs.SetMaximum(343.2);

  TLine neutrLine(0.5, 342.86, nFits+0.5, 342.86);
  neutrLine.SetLineStyle(kDashed);
  neutrLine.Draw();

  //Labels
  double smuonScale(1.0/(pads[0]->GetAbsHNDC()));
  smuons.GetYaxis()->SetLabelSize(0.06    * smuonScale);
  smuons.GetYaxis()->SetLabelOffset(0.015/2);

  double neutrScale(1.0/(pads[1]->GetAbsHNDC()));
  neutrs.GetYaxis()->SetLabelSize(0.06    * neutrScale);
  neutrs.GetYaxis()->SetLabelOffset(0.015/2);
  neutrs.GetXaxis()->SetLabelSize(0.06    * neutrScale);
  neutrs.GetXaxis()->SetTitle("Binning");
  neutrs.GetXaxis()->SetTitleSize(0.06 * neutrScale);
  neutrs.GetXaxis()->SetTitleOffset(1.0);


  const double ypos = neutrs.GetMinimum();
  const double xmin = neutrs.GetXaxis()->GetXmin();
  const double xmax = neutrs.GetXaxis()->GetXmax();
  const int divisions = neutrs.GetXaxis()->GetNdivisions();
  TGaxis *axis = new TGaxis(xmin, ypos, xmax, ypos , 0, 10,divisions,"L");
  axis->SetLabelSize(-1);

  std::cout << pads[1]->GetUxmin()  << std::endl;
  std::cout << pads[1]->GetUxmax()  << std::endl;
  std::cout << pads[1]->GetUymin()  << std::endl;
  std::cout << pads[1]->GetUymax()  << std::endl;

  //Titles
  smuons.GetYaxis()->SetTitleSize(0.06 * smuonScale);
  neutrs.GetYaxis()->SetTitleSize(0.06 * neutrScale);
  const double titleOffset = 1.8;
  smuons.GetYaxis()->SetTitleOffset(titleOffset/smuonScale);
  neutrs.GetYaxis()->SetTitleOffset(1.75/neutrScale);

  neutrs.GetYaxis()->SetTitle("m_{#tilde{#chi}^{0}_{1}} [GeV]");
  smuons.GetYaxis()->SetTitle("m_{#tilde{#mu}} [GeV]");
  
  const double range(nFits+1);
  const double nBins(neutrs.GetXaxis()->GetNbins());
  for (int i = 0; i < nFits ;++i) {
    const int bin(double(i+1)/range*nBins+0.5);
    std::cout << bin  << std::endl;
    neutrs.GetXaxis()->SetBinLabel(bin, Binnings[i].c_str() );
  }

  // neutrs.GetYaxis()->CenterTitle();
  // smuons.GetYaxis()->CenterTitle(kTRUE);

  axis->Draw();
  
  c1.Update();

  c1.SaveAs("SmuonMassResults.eps");

  return 0;
}






