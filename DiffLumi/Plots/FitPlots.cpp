/*
 * FitPlots.cpp
 *
 *  Created on: Aug 15, 2012
 *      Author: stephane
 */
#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

#include <TStyle.h>
#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TProfile.h"
#include <iostream>
int plot(TString filename){
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TFile*file = TFile::Open(filename);
  if(not file) return 1;
  TTree*t = static_cast<TTree*>(file->Get("FitTree"));
  double*params = new double[100];
  double chi2;
  int n_params;
  t->SetBranchAddress("Parameters",params);
  t->SetBranchAddress("Chi2",&chi2);
  t->SetBranchAddress("n_params",&n_params);

  Long64_t nentries = t->GetEntries();
  

  TH1D h_chi2("h_chi2","chi2",nentries,0,nentries);
  h_chi2.SetLineColor(kRed);
  TH2D h_chi2_vs_p0("h_chi2_vs_p0","chi2 vs p0",1000, 0.15, 0.20, 100, 0., 1);
  h_chi2_vs_p0.SetXTitle("p0");
  h_chi2_vs_p0.SetYTitle("#chi^{2}");
  TH2D h_chi2_vs_p1("h_chi2_vs_p1","chi2 vs p1",100, 0.1, 0.3, 100, 0., 1);

  for (Long64_t entry = 0; entry<nentries; ++entry){
    t->GetEntry(entry);
    h_chi2.SetBinContent(entry,chi2);
    h_chi2_vs_p0.Fill(params[0],chi2);
    h_chi2_vs_p1.Fill(params[1],chi2);
  }

  TCanvas c1("c1","c1");
#ifdef USE_RootStyle
  RootStyle::SetPad(&c1);
#endif
  c1.SetLogy();
  h_chi2.Draw();
  c1.SaveAs("FitPlots.eps");

  TCanvas c2("c2","c2");
#ifdef USE_RootStyle
  RootStyle::SetPad(&c2);
#endif
  c2.SetLogy();
  TProfile*prof = h_chi2_vs_p0.ProfileX();
  prof->SetYTitle("#chi^{2}");
  prof->Draw("CL");
  prof->SetLineColor(kRed);
  //c2.SetLogz();
  c2.SaveAs("chi2vsp0.eps");

  TCanvas c3("c3","c3");
#ifdef USE_RootStyle
  RootStyle::SetPad(&c3);
#endif
  c3.SetLogy();
  TProfile*prof2 = h_chi2_vs_p1.ProfileX();
  prof2->Draw("CL");
  prof2->SetLineColor(kRed);
  c3.SaveAs("chi2vsp1.eps");
  return 0;
}


int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  TString filename("");
  if(argc < 2) {
    std::cerr << "No filename given... abort\n"
	      << "FitPlots Fitplot_tuple.root"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    ;
  }
  plot(filename);

  return 0;
}
