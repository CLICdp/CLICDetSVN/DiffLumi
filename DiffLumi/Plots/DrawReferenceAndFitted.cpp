#include <TCanvas.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TString.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
 
#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <cmath>
#include <iostream>
#include <stdexcept>

void DrawReferenceAndFitted(TString);

int main (int ac, char **argc)
{
  if (ac < 2) {
    std::cerr << "No filename provided, aborting!"  << std::endl;
    return 1;
  }

  TString filename(argc[1]);
  std::cout << filename  << std::endl;

  try {
    DrawReferenceAndFitted(filename);
  } catch (std::exception &e) {
    std::cerr << "Error during execution"  << e.what() << std::endl;
    return 1;
  }


  return 0;

}//main

void DrawReferenceAndFitted (TString filename){

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
  gROOT->ForceStyle();
#endif

  TFile *file = TFile::Open(filename);
  if( not file ) {
    throw std::runtime_error ("File not found");
  }
  TH1D* lumi_reference;
  TH1D* lumi_0;
  TH1D* lumi_1;
  TH1D* h_data_0;
  TH1D* h_weights_0;
  TH1D* h_weights_1;

  file->GetObject("lumi_reference"      ,lumi_reference);
  file->GetObject("lumi_0"              ,lumi_0);
  file->GetObject("lumi_1"              ,lumi_1);
  file->GetObject("h_data_0"            ,h_data_0);
  file->GetObject("h_weights_0"         ,h_weights_0);
  file->GetObject("h_weights_1"         ,h_weights_1);

  if( not (lumi_reference || lumi_0 || lumi_1 || h_data_0 || h_weights_0 || h_weights_1 )) {
    //    std::cout << "One or more histograms not found, aborting"  << std::endl;
    throw std::runtime_error("Histograms not found!");
  }


  Double_t resRange = 7.0;
  const int nBinsPull = 100;
  // TH1D *hPulls0 = new TH1D("hPulls0","Before;(N_{GP} - N^{i}_{MC})/#sqrt{N^{i}_{GP}};N_{Bins}", nBinsPull, -resRange, resRange);
  // TH1D *hPulls1 = new TH1D("hPulls1","After;(N_{GP} - N^{i}_{MC})/#sqrt{N^{i}_{GP}};N_{Bins}", nBinsPull, -resRange, resRange);

  TH1D *hPulls0 = new TH1D("hPulls0","Before;#Omega;N_{Bins}", nBinsPull, -resRange, resRange);
  TH1D *hPulls1 = new TH1D("hPulls1","After; #Omega;N_{Bins}", nBinsPull, -resRange, resRange);



#ifdef USE_RootStyle
  RootStyle::MyColor colors;
  RootStyle::SetAllColors(lumi_reference, colors.GetColor());
  RootStyle::SetAllColors(lumi_1        , colors.GetColor());
  RootStyle::SetAllColors(lumi_0        , colors.GetColor());
  RootStyle::SetAllColors(h_weights_1   , lumi_1->GetLineColor());
  RootStyle::SetAllColors(h_weights_0   , lumi_0->GetLineColor());
  RootStyle::SetAllColors(hPulls1       , lumi_1->GetLineColor());
  RootStyle::SetAllColors(hPulls0       , lumi_0->GetLineColor());

#endif

  // TCanvas c1("c1","c1");
  // lumi_reference->DrawNormalized();
  // lumi_1->DrawNormalized("histsame");
  // //  lumi_0->DrawNormalized("same");

  Double_t ScalingFactor1 = double(lumi_reference->GetEntries())/double(lumi_1->GetEntries());
  Double_t ScalingFactor = double(h_data_0->Integral())/double(h_weights_1->Integral());

  std::cout << ScalingFactor  << std::endl;
  std::cout << ScalingFactor/ScalingFactor1  << std::endl;

  // h_weights_0->Scale(ScalingFactor);
  // h_weights_1->Scale(ScalingFactor);

  int nBins = h_data_0->GetNbinsX();
  for (int i = 1; i <= nBins; ++i) {
    double dataCont	= h_data_0->GetBinContent(i);
    double weightsCont0 = h_weights_0->GetBinContent(i);
    double weightsCont1 = h_weights_1->GetBinContent(i);

    double weightsError0 = h_weights_0->GetBinError(i);
    double weightsError1 = h_weights_1->GetBinError(i);

    hPulls0->Fill((dataCont - weightsCont0*ScalingFactor )/sqrt( dataCont + pow(weightsError0*ScalingFactor, 2) ));
    hPulls1->Fill((dataCont - weightsCont1*ScalingFactor )/sqrt( dataCont + pow(weightsError1*ScalingFactor, 2) ));

  }

  {

    TF1* func = new TF1("myGaus","gaus",-10,10);
    func->SetLineStyle(kDotted);
    func->SetLineColor(kBlack);




    TLegend *leg = NULL;
    TCanvas c2("cPulls","pulls");
    hPulls0->Draw();
    hPulls1->Draw("same");


#ifdef USE_RootStyle
    RootStyle::AutoSetYRange(c2, 1.1);
    leg = RootStyle::BuildLegend(c2, 0.2, 0.9-3*0.0625, 0.5, 0.9);
    leg->AddEntry(func,"Gauss Fit","l");
    leg->SetBorderSize(0);
    leg->SetTextSize(0.06);
#endif

    hPulls1->Fit("myGaus","+");
    c2.Clear();
    //redraw because legend goes to far
    hPulls0->Draw();
    leg->Draw();
    hPulls1->Draw("same");

    c2.Update();
    c2.SaveAs("PullDistribution.eps");
  }


  ////////////////////////////
  // Bin distribution plots //
  ////////////////////////////

  TCanvas c3("cWeights","Weights");
  h_data_0   ->SetTitle("GuineaPig;Bin Number;N_{GP},N_{MC}");
  h_weights_0->SetTitle("Before");
  h_weights_1->SetTitle("After");

  h_data_0->Draw();
  h_data_0->GetYaxis()->SetTitleOffset(1.2);
  h_weights_0->Scale(ScalingFactor);
  h_weights_0->Draw("same");
  h_weights_1->Scale(ScalingFactor);
  h_weights_1->Draw("same");

  int binstart=1500;
  h_data_0->SetAxisRange(binstart, binstart+300,"X");
  h_data_0->SetAxisRange(000, 700,"Y");


#ifdef USE_RootStyle
  {
    TLegend *leg = RootStyle::BuildLegend(c3, 0.2, 0.9-3*0.0625, 0.5, 0.9);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.06);
  }
#endif


  c3.SaveAs("WeightDistribution.eps");


  delete hPulls0;
  delete hPulls1;


  file->Close();
  delete file;

  return;


}
