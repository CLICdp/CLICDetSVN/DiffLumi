{

  lumi_reference->DrawNormalized();
  lumi_0->SetLineColor(kRed);
  lumi_1->SetLineColor(kGreen+2);

  lumi_1->DrawNormalized("histsame");
  //  lumi_0->DrawNormalized("same");

  Double_t ScalingFactor1 = double(lumi_reference->GetEntries())/double(lumi_1->GetEntries());

  Double_t ScalingFactor = double(h_data_0->Integral())/double(h_weights_1->Integral());

  std::cout << ScalingFactor  << std::endl;
  std::cout << ScalingFactor/ScalingFactor1  << std::endl;

  Double_t resRange = 5.0;
  Int_t nBins = 100;
  TH1D *hPulls0 = new TH1D("hPulls0","Pulls;(N_{Data} - N^{i}_{MC})/#sqrt{N^{i}_{Data}};Count", nBins, -resRange, resRange);
  TH1D *hPulls1 = new TH1D("hPulls1","Pulls;(N_{Data} - N^{i}_{MC})/#sqrt{N^{i}_{Data}};Count", nBins, -resRange, resRange);

  hPulls1->SetLineColor(kGreen+2);

  // h_weights_0->Scale(ScalingFactor);
  // h_weights_1->Scale(ScalingFactor);
  Double_t chi2(0.0);//can calculate chi2 from final fit result, if weights^2 are present

  int nBins = h_data_0->GetNbinsX();
  for (int i = 1; i <= nBins; ++i) {
    double dataCont = h_data_0->GetBinContent(i);
    double weightsCont0 = h_weights_0->GetBinContent(i);
    double weightsCont1 = h_weights_1->GetBinContent(i);

    double weightsError0 = h_weights_0->GetBinError(i);
    double weightsError1 = h_weights_1->GetBinError(i);
    const double correctedWeightsError(weightsError1*ScalingFactor);
    hPulls0->Fill((dataCont - weightsCont0*ScalingFactor )/sqrt( dataCont + pow(weightsError0*ScalingFactor, 2) ));
    hPulls1->Fill((dataCont - weightsCont1*ScalingFactor )/sqrt( dataCont + correctedWeightsError*correctedWeightsError ));
    
    const double delta = dataCont-ScalingFactor * weightsCont1;
    chi2 += delta*delta / (dataCont + correctedWeightsError*correctedWeightsError);

  }

  std::cout << "the final chi2/ndf is: " << chi2 << " / " << nBins << " = " << chi2/nBins  << std::endl;


  TCanvas *c2 = new TCanvas ("cPulls","pulls");
  hPulls1->Draw();
  hPulls0->Draw("same");

  TCanvas *c3 = new TCanvas("cWeights","Weights");
  h_data_0->Draw();
  h_data_0->SetAxisRange(0, 2000,"Y");
  
  h_weights_1->Scale(ScalingFactor);
  h_weights_1->SetLineColor(kGreen+2);
  h_weights_1->Draw("same");


}
