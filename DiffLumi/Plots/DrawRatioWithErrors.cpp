#include "DrawSpectra.hh"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TCanvas.h>
#include <TError.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TStyle.h>

#include <cmath>
#include <iomanip>
#include <iostream>


int main (int argc, char** argv)
{
  TString filename;
  int nRebins(1);
  TString prefix("FinalRatio_");

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  gErrorIgnoreLevel=kError;

  //Dealing with input parameters
  if(argc < 3) {
    std::cerr << "No filename given... abort\n"
	      << "DrawRatioWithErrors inputRoot.root nRebins [output prefix]"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    nRebins = std::atoi(argv[2]);
    if (argc == 4) {
      prefix = TString(argv[3]);
    }
  }

  TFile *file = TFile::Open(filename);
  if (not file) {
    std::cerr << "File not found" << filename << std::endl;
    std::exit(1);
  }

  //Getting the histogram
  TH1D *noErrorHistogram, *errorHistogram, *gpHistogram;
  //  TH2D *lumi2d;
  file->GetObject("hSqrts", errorHistogram);
  file->GetObject("GP", gpHistogram);
  //file->GetObject("Lumi", lumi2d);

  noErrorHistogram = static_cast<TH1D*>(errorHistogram->Clone("noErrors"));
 
  //  double xMin(errorHistogram->GetXaxis()->GetXmin());
  double xMax(errorHistogram->GetXaxis()->GetXmax());

  // TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/Lumi_GP_Double_Slices50_Random.root";
  // TH1D gpHistogram( DrawLumiSpectrum("GP", fileWithELoss, xMin, xMax, 300000000, false, true, nbins) );

  errorHistogram->SetLineColor(kRed-7);
  errorHistogram->SetMarkerColor(kRed-7);
  noErrorHistogram->SetLineColor(kGreen+2);

  errorHistogram->SetTitle("Model    ;#sqrt{s'}/#sqrt{s_{nom}};dN/dx");
  gpHistogram   ->SetTitle("GuineaPig;#sqrt{s'}/#sqrt{s_{nom}};dN/dx");


  //Just to make sure they are there
  errorHistogram  ->Sumw2();
  noErrorHistogram->Sumw2();
  gpHistogram     ->Sumw2();

  // {
  //   TCanvas temp("cGauss","cGauss");
  //   gpHistogram->Draw();
  //   gpHistogram->SetAxisRange(0.990,1.01,"X");
  //   gpHistogram->Fit("gaus","","",0.995, 1.005);
  //   temp.Update();
  //   temp.SaveAs("topfit1.eps");
  // }
  // {
  //   TCanvas temp("cGauss","cGauss");
  //   errorHistogram->Draw();
  //   errorHistogram->SetAxisRange(0.990,1.01,"X");
  //   errorHistogram->Fit("gaus","","",0.995, 1.005);
  //   temp.Update();
  //   temp.SaveAs("topfit2.eps");
  // }

  // errorHistogram  ->Scale( 1.0 / errorHistogram->Integral  ("Width"));
  // noErrorHistogram->Scale( 1.0 / noErrorHistogram->Integral("Width"));
  // gpHistogram     ->Scale( 1.0 / gpHistogram->Integral     ("Width"));

  // errorHistogram  ->Scale( 1.0 / errorHistogram->Integral  ( errorHistogram  ->FindFixBin(0.5), errorHistogram  ->FindFixBin(1.05), "Width"));
  // noErrorHistogram->Scale( 1.0 / noErrorHistogram->Integral( noErrorHistogram->FindFixBin(0.5), noErrorHistogram->FindFixBin(1.05), "Width"));
  // gpHistogram     ->Scale( 1.0 / gpHistogram->Integral     ( gpHistogram     ->FindFixBin(0.5), gpHistogram     ->FindFixBin(1.05), "Width"));

  //Get the content in the top 1% of the histogram for GP and our histogram
  //How do we also get the error?
  double gpTopErr(0.0), moTopErr(0.0);
  double gpTop = gpHistogram->IntegralAndError   ( gpHistogram   ->FindFixBin(0.99), gpHistogram   ->GetNbinsX(), gpTopErr, "width");
  double moTop = errorHistogram->IntegralAndError( errorHistogram->FindFixBin(0.99), errorHistogram->GetNbinsX(), moTopErr, "width");

  // const double gpTopNormalisation = gpHistogram->Integral( "width" );
  // gpTop /=    gpTopNormalisation;
  // moTop /=    gpTopNormalisation;
  // gpTopErr /= gpTopNormalisation;
  // moTopErr /= gpTopNormalisation;


  // std::cout << errorHistogram->Integral( "width" )  << std::endl;
  // std::cout << gpHistogram   ->Integral( "width" )  << std::endl;
  // std::cout << errorHistogram->Integral( errorHistogram  ->FindFixBin(0.5), errorHistogram  ->FindFixBin(1.05), "width" )  << std::endl;
  // std::cout << gpHistogram   ->Integral( noErrorHistogram->FindFixBin(0.5), noErrorHistogram->FindFixBin(1.05), "width" )  << std::endl;

  errorHistogram  ->Rebin(nRebins);
  noErrorHistogram->Rebin(nRebins);
  gpHistogram     ->Rebin(nRebins);

  //Remove Errors from noErrorHistogram
  for (int i = 1; i <= noErrorHistogram->GetNbinsX() ;++i) {
    noErrorHistogram->SetBinError(i, 0.0);
    //gpHistogram->SetBinError(i,0.0);
  }

  errorHistogram  ->Scale( 1.0 / errorHistogram->Integral("Width"));
  noErrorHistogram->Scale( 1.0 / noErrorHistogram->Integral("Width"));
  gpHistogram     ->Scale( 1.0 / gpHistogram->Integral("Width"));


  //Do MC/GP - 1 instead of (MC-GP)/GP to get smaller errors, as we only get one contribution from GP this way!
  TF1 MinusOne("MinusOne","-1",0.0,2.0);

  TCanvas canv("SpectrumRatios","Luminosity Spectrum Ratios from " + filename);
  errorHistogram->SetFillColor(kGray);
  errorHistogram->SetFillStyle(1001);

  errorHistogram->Draw("F,hist");
  gpHistogram->Draw("e0,same");
  //errorHistogram->SetMarkerStyle(kOpenCircle);

  TH1D *firstDrawnHistogram = errorHistogram;

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(&canv, 0.2, 0.9-2*0.0625, 0.5, 0.9);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.06);
#endif
  //gpHistogram->Draw("e0,same");

  canv.SetLogy(1);
  firstDrawnHistogram->SetAxisRange(2e-5, 1e2, "Y");
  firstDrawnHistogram->SetAxisRange(0.0,  xMax, "X");
  gPad->Update();
  canv.SaveAs( prefix+"Spectrum00.eps");

  firstDrawnHistogram->SetAxisRange(1e-3, 1e2, "Y");
  firstDrawnHistogram->SetAxisRange(0.5,  xMax, "X");
  gPad->Update();
  canv.SaveAs( prefix+"Spectrum50.eps");

  firstDrawnHistogram->SetAxisRange(1e-3, 1e2, "Y");
  firstDrawnHistogram->SetAxisRange(0.7,  xMax, "X");
  gPad->Update();
  canv.SaveAs( prefix+"Spectrum70.eps");

  firstDrawnHistogram->SetAxisRange(0.90, xMax, "X");
  gPad->Update();
  canv.SaveAs( prefix+"Spectrum90.eps");

  firstDrawnHistogram->SetAxisRange(0.95, xMax, "X");
  gPad->Update();
  canv.SaveAs( prefix+"Spectrum95.eps");

  canv.SetLogy(0);
  firstDrawnHistogram->SetAxisRange(0.99, 1.01, "X");
  gPad->Update();
  canv.SaveAs( prefix+"Spectrum99.eps");


  // for (int i = 1; i < errorHistogram->GetNbinsX() ;++i) {
  //   std::cout 
  //     << std::setw(15) << errorHistogram->GetBinCenter(i)
  //     << std::setw(15) << errorHistogram->GetBinContent(i)
  //     << std::setw(15) << errorHistogram->GetBinError(i)/errorHistogram->GetBinContent(i)
  //     << std::setw(15) << gpHistogram->GetBinContent(i)
  //     << std::setw(15) << gpHistogram->GetBinError(i)/gpHistogram->GetBinContent(i)
  //     << std::endl;
  // }


  //Do the ratio
  errorHistogram->Divide(gpHistogram);
  errorHistogram->Add(&MinusOne);

  noErrorHistogram->Divide(gpHistogram);
  noErrorHistogram->Add(&MinusOne);

  // for (int i = 1; i < errorHistogram->GetNbinsX() ;++i) {
  //   std::cout 
  //     << std::setw(15) << errorHistogram->GetBinCenter(i)
  //     << std::setw(15) << errorHistogram->GetBinContent(i)
  //     << std::setw(15) << errorHistogram->GetBinError(i)
  //     << std::setw(15) << errorHistogram->GetBinError(i)/errorHistogram->GetBinContent(i)
  //     << std::setw(15) << noErrorHistogram->GetBinContent(i)
  //     << std::setw(15) << noErrorHistogram->GetBinError(i)
  //     << std::setw(15) << noErrorHistogram->GetBinError(i)/gpHistogram->GetBinContent(i)
  //     << std::endl;
  // }

 

  //Draw Again;
  gStyle->SetErrorX(0);
  errorHistogram->SetYTitle("(MC-GP)/GP");
  errorHistogram->SetMarkerStyle(kDot);
  errorHistogram->Draw("E0");
  noErrorHistogram->Draw("E1,same");
  canv.SetLogy(0);
  //errorHistogram->Draw("E0,same");

  errorHistogram->SetAxisRange(-0.15, 0.15, "Y");

  errorHistogram->SetAxisRange(0.1,  xMax, "X");
  canv.SaveAs( prefix+"Ratio00.eps");

  errorHistogram->SetAxisRange(0.5,  xMax, "X");
  canv.SaveAs( prefix+"Ratio50.eps");

  errorHistogram->SetAxisRange(0.7,  xMax, "X");
  canv.SaveAs( prefix+"Ratio70.eps");

  errorHistogram->SetAxisRange(0.90, xMax, "X");
  canv.SaveAs( prefix+"Ratio90.eps");

  errorHistogram->SetAxisRange(0.95, xMax, "X");
  canv.SaveAs( prefix+"Ratio95.eps");

  errorHistogram->SetAxisRange(0.99, 1.01, "X");
  canv.SaveAs( prefix+"Ratio99.eps");


  std::cout << "The content above 0.99 is:\n"
	    << std::setw(14) << gpTop*100 << " +- " << std::setw(14) << gpTopErr*100 << "\n"
	    << std::setw(14) << moTop*100 << " +- " << std::setw(14) << moTopErr*100 << std::endl;
  std::cout << gpHistogram->GetEntries()  << std::endl;

  //////////////////////
  // Draw 2D Spectrum //
  //////////////////////
  // // Need 3000 Bins and same range for GP to compare these histograms
  // canv.SetRightMargin(0.16);
  // canv.SetLogz();
  // lumi2d->SetAxisRange(0.97, 1.01, "X");
  // lumi2d->SetAxisRange(0.97, 1.01, "Y");
  // lumi2d->Draw("colz");
  // lumi2d->SetXTitle("E_{1}/E_{Beam}");
  // lumi2d->SetYTitle("E_{2}/E_{Beam}");
  // // lumi2d->GetXaxis()->SetNdivisions(502);
  // // lumi2d->GetYaxis()->SetNdivisions(502);
  // lumi2d->Scale( 1./ lumi2d->GetMaximum() );
  // lumi2d->SetAxisRange(1e-5, 1.1, "Z");
  // canv.SaveAs( prefix+"lumi_2D_097.eps");


  return 0;
}
