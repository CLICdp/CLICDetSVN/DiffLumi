#include "Utilities.h"
#include "DrawSpectra.hh"
#include "ConvolutedFunctions.hh"

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>
#include <TString.h>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

int main (int ac, char **argv)
{

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  if(ac < 5) {
    std::cout << "Not enough input variables\n"
	      << "DrawBDSSpread <BeamEnergy> <nBins> <Events> inputfile.root [inputfile2.root ...]"
	      << std::endl;
    return 1;
  }

  double nominalEnergy(1500.0);

  nominalEnergy = std::atof(argv[1]);

  std::cout << "Nominal Beam Energy: " << nominalEnergy  << std::endl;
  if( nominalEnergy <= 0) {
    std::cout << "Error: Nominal Beam Energy " << nominalEnergy  << std::endl;
    exit(1);
  }

  int bins(1000);
  bins = std::atoi(argv[2]);

  int numberOfEvents(300000);
  numberOfEvents = std::atoi(argv[3]);

  std::vector<TString> filenames;
  for (int i = 4; i < ac;++i) {
    filenames.push_back(TString(argv[i]));
  }
  
  
  std::vector<TH1D> histos;
  
  for (std::vector<TString>::iterator it = filenames.begin(); it != filenames.end(); ++it) {
    //TH1D hEnergy(DrawBDS(*it, *it, "MyTree", nominalEnergy, -0.006, 0.0065, 30000000, true));
    TH1D hEnergy(DrawBDS(*it, *it, "MyTree", nominalEnergy, -0.008, 0.008, numberOfEvents, true, true, bins));
    //hEnergy.Rebin(rebin);
    //hEnergy.Scale(1.0/double(rebin));
    histos.push_back(hEnergy);
  }

  TF1 esFunction( ConvolutedFunctions::getESFunction( ConvolutedFunctions::getParameters() ));
  esFunction.SetNpx(1000);

#ifdef USE_RootStyle
  RootStyle::PDFFile file;
  RootStyle::MyColor colors;
  TCanvas c("c","bds");
  bool first=true;
  c.cd();

  for (std::vector<TH1D>::iterator it = histos.begin(); it != histos.end(); ++it) {
    RootStyle::SetAllColors(&(*it), colors.GetColor() );
    std::cout << "Fitting (This can take quite some time if the binning is too fine)..." << it - histos.begin() << std::endl;
    (*it).Fit(&esFunction,"WL","");
    (*it).SetTitle(Form("Beam Energy Spread at %i GeV", int(2*nominalEnergy)));

    c.cd();
    if (first){
      (*it).Draw("");
      first = false;
    } else {
      (*it).Draw("same,hist");
    }

    TCanvas c2("c2","c2");
    c2.cd();
    (*it).Draw("hist");
    // (*it).SetAxisRange(0, 200,"Y");
    //(*it).SetAxisRange(0, 500,"Y");
    TString title = (*it).GetTitle();
    //title.ReplaceAll(".root","");
    std::cout << strlen(title.Data())  << std::endl;
    std::cout << title.Data()  << std::endl;
    RootStyle::BuildLegend(&c2, 0.2, 0.8, 0.9, 0.9);
    file.AddCanvas(&c2, title.Data());
  }


  // {
  //   TCanvas c2("c2","c2");
  //   c2.cd();
  //   std::cout << "Copying Histogram, fillrandom, and try to fit again"  << std::endl;
  //   TH1D *clone = static_cast<TH1D*>(histos[0].Clone("copyFromH"));
  //   clone->Reset();
  //   for (int j = 0; j < 10;++j) {
  //     for (int i = 0; i < 100000 ;++i) {
  // 	clone->Fill(esFunction.GetRandom());
  //     }
  //     std::cout << "Fit " << j
  // 		<< "  Entries: " << clone->GetEntries()
  // 		<< std::endl;
  //     TH1D *clone2 = static_cast<TH1D*>(clone->Clone("copyFromH2"));
  //     std::cout << clone2->GetEntries() << "  " << clone2->Integral()  << std::endl;
  //     clone2->Scale(1./ clone2->Integral("width"));
  //     clone2->Fit(&esFunction,"WL","");
  //     c2.SaveAs(Form("fit%i.eps",j));
  //     delete clone2;
  //   }
  // }


  file.CloseFile();
  TLegend *leg = RootStyle::BuildLegend(&c, 0.2, 0.9, 0.9, 0.9-2*0.0675);
  leg->SetTextSize(0.06);
  RootStyle::AutoSetYRange(c, 1.3);
  c.SaveAs(Form("BDSHistograms_%i_Bins%i.eps", int(nominalEnergy), bins));
#endif

  return 0;
}
