#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif
#include <TStyle.h>
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TMath.h"
#include "TTree.h"
#include "TGraph.h"
#include <iostream>

int plot(TString path){
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  int binx[] = {10,20,30,40,50};
  int biny[] = {10,20,30,40};

  double*x = new double[8];
  double*y = new double[8];
  int b = 0;
  std::vector<TString> blabel;
  for (int i = 0; i<4;++i){
    int by=biny[i];
    for (int j=i; j<5; ++j){
      int bx=binx[j];
      TString fname = path+Form("/final_%i_%i_%i.root",bx,by,by);
      TFile*file = TFile::Open(fname);
      if(not file) continue;
      TTree*t = static_cast<TTree*> (file->Get("FitResult"));
      double minv = 0.;
      t->SetBranchAddress("minval",&minv);
      t->GetEntry(0);
      TString bl = Form("%ix%ix%i",bx,by,by);
      blabel.push_back(bl);
      x[b] = bx*by*by;
      y[b] = minv;
      ++b;
      delete file;
    }
  }
  TCanvas c1("c1","c1");
  TGraph*g = new TGraph(8,x,y);
  g->SetMarkerStyle(33);
  g->SetMarkerColor(kRed);
  g->SetMarkerSize(2);
  g->GetYaxis()->SetTitle("#chi^{2}/ndf");
  //g->GetXaxis()->SetTitle("Nb bins");
  for (unsigned int i = 1; i< blabel.size(); ++i){
    g->GetXaxis()->SetBinLabel(g->GetXaxis()->FindBin(x[i]),blabel.at(i));
  }
  //g->GetXaxis()->LabelsOption("u");
  g->Draw("AP");
  c1.SaveAs("PerfsVSBinning.eps");
  return 0;
}
int main (int argc, char** argv){
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  TString path("");
  if(argc < 1) {
    std::cerr << "No path given... abort\n"
	      << "PerfsVsBinning path (without file name)"
	      << std::endl;
    return 1;
  } else {
    path = TString(argv[1]);
  }
  return plot(path);
}
