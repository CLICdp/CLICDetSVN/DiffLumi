/*
 * MakeLumiPlotsFromRoot.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: stephane
 */
#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

#include "TString.h"
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include <TStyle.h>
#include <TLegend.h>
#include "Utilities.h"
#include "TTree.h"
#include <cstdlib>
#include <iostream>
#include <iomanip>

int TH1_plots(TString filename, int nRebins){
  
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  TH1D*h_sqrts_ref = new TH1D("h_sqrts_ref","sqrts GP",1000 ,0.,1550./1500);
  TH1D*h_sqrts_ref_c = new TH1D("h_sqrts_ref_c","sqrts GP",1000 ,0.,1550./1500);
  h_sqrts_ref->SetXTitle("#sqrt{s'}/#sqrt{s}");
  h_sqrts_ref->SetYTitle("dN/d(#sqrt{s'}/#sqrt{s})");
  h_sqrts_ref->SetLineColor(kRed);
  TString gpname(Utility::GetWorkingDirectory()+"/RootFiles/LumiFileGP.root");
  TFile*fgp = TFile::Open(gpname);
  TTree*gptree = NULL;
  fgp->GetObject("MyTree",gptree);
  if (!gptree) return 1;
  double E1,E2;
  gptree->SetBranchAddress("E1",&E1);
  gptree->SetBranchAddress("E2",&E2);
  for (int i=0;i<3000000;i++){
    gptree->GetEntry(i);
    double s = sqrt(E1*E2);
    h_sqrts_ref->Fill(s);
    if (s>0.5){
      h_sqrts_ref_c->Fill(s);
    }
  }
  fgp->Close();
  h_sqrts_ref->Sumw2();
  h_sqrts_ref->SetAxisRange(h_sqrts_ref->GetXaxis()->GetXmin(),h_sqrts_ref->GetXaxis()->GetXmax(),"X");
  h_sqrts_ref_c->SetAxisRange(h_sqrts_ref_c->GetXaxis()->GetXmin(),h_sqrts_ref_c->GetXaxis()->GetXmax(),"X");

  h_sqrts_ref->Rebin(nRebins);

  h_sqrts_ref->Scale(1./h_sqrts_ref->Integral(h_sqrts_ref->FindBin(.5),h_sqrts_ref->FindBin(1550./1500)));
  h_sqrts_ref_c->Scale(1./h_sqrts_ref_c->Integral(h_sqrts_ref->FindBin(.5),h_sqrts_ref->FindBin(1550./1500)));

  TH1D*h_d_fit = new TH1D("h_d_fit","sqrts fit",1000 ,0.,1550./1500);
  h_d_fit->SetXTitle("#sqrt{s'}/#sqrt{s}");
  h_d_fit->SetYTitle("dN/d(#sqrt{s'}/#sqrt{s})");
  h_d_fit->SetLineColor(kBlue);
  h_d_fit->SetLineStyle(kDashed);
  TH1D*h_d_fit_c = new TH1D("h_d_fit_c","sqrts fit",1000 ,0.,1550./1500);
  h_d_fit_c->SetXTitle("#sqrt{s'}/#sqrt{s}");
  h_d_fit_c->SetYTitle("dN/d(#sqrt{s'}/#sqrt{s})");
  h_d_fit_c->SetLineColor(kBlue);
  TString fname(Utility::GetWorkingDirectory()+"/"+filename);
  TFile*fd = TFile::Open(fname);
  if (!fd) return 1;
  TTree*td = NULL;
  fd->GetObject("MyTree",td);
  if (!td) return 1;
  td->SetBranchAddress("E1",&E1);
  td->SetBranchAddress("E2",&E2);
  Long64_t entries = td->GetEntries();
  for (Long64_t i = 0; i<entries; ++i){
    td->GetEntry(i);
    double s(sqrt(E1*E2));
    h_d_fit->Fill(s);
    if (s>0.5){
      h_d_fit_c->Fill(s);
    }
  }
  fd->Close();
  h_d_fit_c->Sumw2();
  h_d_fit->Sumw2();
  h_d_fit_c->SetAxisRange(h_d_fit_c->GetXaxis()->GetXmin(),h_d_fit_c->GetXaxis()->GetXmax(),"X");
  h_d_fit->SetAxisRange(h_d_fit->GetXaxis()->GetXmin(),h_d_fit->GetXaxis()->GetXmax(),"X");


  h_d_fit_c->Rebin(nRebins);
  h_d_fit->Rebin(nRebins);

  h_d_fit_c->Scale(1./h_d_fit_c->Integral(h_d_fit_c->FindBin(.5),h_d_fit_c->FindBin(1550./1500)));
  h_d_fit->Scale(1./h_d_fit->Integral(h_d_fit->FindBin(.5),h_d_fit->FindBin(1550./1500)));

  TCanvas c1("c1","c1");
  c1.SetLogy();
  h_sqrts_ref->Draw();
  h_d_fit_c->Draw("same");
  h_d_fit->Draw("same");
  c1.SaveAs("FullRange.eps");
  
  //h_sqrts_ref->Sumw2();
  //h_d_fit_c->Sumw2();
  //h_d_fit->Sumw2();


  h_sqrts_ref->SetAxisRange(0.5,1.01);
  h_d_fit_c->SetAxisRange(0.5,1.01);
  h_d_fit->SetAxisRange(0.5,1.01);
  TCanvas c2("c2","c2");
  c2.SetLogy();
  h_sqrts_ref->Draw();
  h_d_fit_c->Draw("same");
  h_d_fit->Draw("same");
  c2.SaveAs("Zoomed05Range.eps");
  h_sqrts_ref->SetAxisRange(0.95,1.01);
  h_d_fit_c->SetAxisRange(0.95,1.01);
  h_d_fit->SetAxisRange(0.95,1.01);
  TCanvas c3("c3","c3");
  c3.SetLogy();
  h_sqrts_ref->Draw();
  h_d_fit_c->Draw("same");
  h_d_fit->Draw("same");
  c3.SaveAs("Zoomed095Range.eps");
  
  TCanvas c4("c4","c4");
  TH1D* h1_diff = (TH1D*)h_sqrts_ref->Clone("ref_copy");
  h1_diff->Add(h_d_fit,h_sqrts_ref,1,-1);
  h1_diff->Divide(h_sqrts_ref);
  h1_diff->SetAxisRange(0.5,1.01,"X");
  //h1_diff->SetAxisRange(-0.1,0.1,"Y");
  h1_diff->Draw();
  h1_diff->SetTitle(filename+"  "+TString(nRebins));
  h1_diff->SetYTitle("(MC-GP)/GP");
  h1_diff->GetYaxis()->SetTitleOffset(1.3);
  c4.SaveAs("Ratio_095.eps");
  

  return 0;
}
int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  //gStyle->SetOptTitle(kTRUE);
  TString filename("");
  int nRebins(1);
  if(argc < 3) {
    std::cerr << "No filename given... abort\n"
	      << "LumiPlots control.root nRebins"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    nRebins = atoi(argv[2]);
  }
  return TH1_plots(filename, nRebins);
}
