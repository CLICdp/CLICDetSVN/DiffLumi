#include "ParameterNames.hh"
#include "Utilities.h"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TAxis.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

typedef std::string String_t;
typedef std::vector<String_t> StringVec;

struct tInfo {
  unsigned int binA, binE, binZ;
  double first;
  double second;
  tInfo(int a, int e, int z, double f, double s): binA(a), binE(e), binZ(z), first(f), second(s) {}
  tInfo(): binA(0), binE(0), binZ(0), first(0.0), second(0.0) {}
};

int getColor(int k) {
  switch(k) {
  case 20: return kBlack;
  case 30: return kBlue;
  case 40: return kGreen+2;
  case 50: return kRed-7;
  default:
    //    std::cout << "Bin Energy not known! " << k  << std::endl;
    return kBlack;
  }
}

int getMarker(int k) {
  switch(k) {
  // case 20: return kOpenTriangleDown;
  // case 30: return kOpenSquare;
  // case 40: return kOpenCross;
  // case 50: return kOpenStar;
  case 20: return kFullStar;
  case 30: return kFullCross;
  case 40: return kFullTriangleDown;
  case 50: return kFullSquare;

  default:
    return kDot;
  }
}


std::vector<double> rangeMin;
std::vector<double> rangeMax;


typedef std::vector< tInfo > VecTInfo;

//bool sortVecTInfoByChi2 (VecTInfo i, VecTInfo j) { return ( i[19].second < j[19].second ); }
bool sortVecTInfoByChi2 (VecTInfo i, VecTInfo j) { return ( i[19].first/i[19].second > j[19].first/j[19].second ); }

typedef std::vector< VecTInfo > ParameterList;


int parameterIDToTablePosition[] = { 19, 0, 1, 2,//chi2/ndf, p_region*3
				     5, 6, 9, 10,//bpeaks
				     15, 16, 17, 18,// b_arms
				     3, 4, 7, 8, //a arms
				     11, 12, 13, 14};//a body

int getPos(int pos) {
  if( pos >= 20 ) {
    throw std::out_of_range( "parameter too large ");
  }
  return parameterIDToTablePosition[pos];
}


std::string gParameterNames[] = {"\\pPeak",
				 "\\pArmA",
				 "\\pArmB",
				 "\\aarmAA",
				 "\\aarmAB",
				 "\\bPeakAA" ,//BeamSpread Peak1 a1
				 "\\bPeakAB" ,//BeamSpread Peak1 a2
				 "\\BSRangeALo",//beamSpreadrange xmin
				 "\\BSRangeAHi",//beamSpreadrange xmax
				 "\\aarmBA",
				 "\\aarmBB",
				 "\\bPeakBA" ,//BeamSpread Peak2 a1
				 "\\bPeakBB" ,//BeamSpread Peak2 a2
				 "\\BSRangeBLo",
				 "\\BSRangeBHi",
				 "\\abodyAA",
				 "\\abodyAB",
				 "\\abodyBA",
				 "\\abodyBB",
				 "\\barmAA",
				 "\\barmAB",
				 "\\BSRALArm",
				 "\\BSRAHArm",
				 "\\barmBA",
				 "\\barmBB",
				 "\\BSRBLArm",
				 "\\BSRBHArm",
				 "\\BSB1aBody",
				 "\\BSB1bBody",
				 "\\BSR1LBody",
				 "\\BSR1HBody",
				 "\\BSBBaBody",
				 "\\BSBBbBody",
				 "\\BSRBLBody",
				 "\\BSRBHBody"};



//void PrintTable(ParameterList& parameterlist, StringVec& fileNames, StringVec& parameterNames);
void PrintTable(ParameterList& parameterlist, StringVec& parameterNames);
void GraphParameters(ParameterList& parameterlist, StringVec& parameterNames, TString const& idString, bool);

int tables(StringVec& files){
  bool pnames=false;
  std::cout << "\nparameterNames" << sizeof(gParameterNames)/sizeof(std::string)  << std::endl;
  //  ParameterList parameterPerFile(files.size(), std::vector< std::pair<double, double> >(35, std::make_pair(0.0,0.0)));
  ParameterList parameterPerFile(files.size());
  StringVec parameterNames (gParameterNames, gParameterNames+35);

  std::vector<bool> cleanedVariables(36, false);
  bool cleanedOnce = false;


  for (StringVec::iterator it = files.begin(); it!= files.end(); ++it){
    TFile* f = TFile::Open( TString(*it) );
    //    std::cout << "FileName " << f->GetName()  << std::endl;
    if (!f){
      std::cout << "File not good"  << std::endl;
      return 1;
    }

    TTree*fitres = NULL;
    f->GetObject("FitResult",fitres);
    if (!fitres){
      std::cout << "Tree not found"  << std::endl;
      return 1;
    }

    Int_t    n_params, status;
    Double_t Param[36][2];
    std::vector<std::string> *varname(NULL); //HAVE TO INITIALIZE THIS TO ZERO!!!! OR PROVIDE THE STORAGE FOR IT!!!
    //See the TTree documentationg regarding objects!

    fitres->SetBranchAddress("n_params", &n_params);
    fitres->SetBranchAddress("Param", Param);
    fitres->SetBranchAddress("ParamName", &varname);
    fitres->SetBranchAddress("status", &status);
    double chi2, ndf;
    int binsX, binsY, binsZ;
    fitres->SetBranchAddress("minval", &chi2);
    fitres->SetBranchAddress("binsx",  &binsX);
    fitres->SetBranchAddress("binsy",  &binsY);
    fitres->SetBranchAddress("binsz",  &binsZ);

    fitres->GetEntry(0);
    ndf = binsX*binsY*binsZ;

    if(status != 0) {
      std::cout << "Status not 0, don't use this parameter set! "
		<< f->GetPath()
		<< std::endl;
      //      parameterPerFile.pop_back()

      for (int i = 0; i<n_params;++i){ //why -1?
	parameterPerFile[it-files.begin()].push_back(tInfo(binsX, binsY, binsZ, 0.0, -1.0));
      }
      continue;
    }

    if (!pnames){
      //for (int i=0; i<n_params;++i){
      //	std::cout << "hjtrahja"  << std::endl;
      //std::cout << varname->at(i)  << " & " << std::endl;
      //}
      //std::cout << "\\\\" <<std::endl;
      pnames = true;
    }

    bool cleaning = false;
    //std::cout << n_params  << std::endl;
    for (int i = 0; i<n_params;++i){ //why -1?

      if( Param[i][1] > 0.0) {
	parameterPerFile[it-files.begin()].push_back(tInfo(binsX, binsY, binsZ, Param[i][0], Param[i][1]));
      } else if( cleanedVariables[i] == false && cleanedOnce == false) {
	std::cout << "Removing parameterName " << *(parameterNames.begin() + (parameterPerFile.back().size()));
	std::cout << std::setw(15) << Param[i][0] << std::setw(15) << Param[i][1]  << std::endl;
	parameterNames.erase( parameterNames.begin() + (parameterPerFile[0].size()) );
	cleanedVariables[i] = true;
	cleaning = true;
      }

      //      parameterNames.push_back( varname->at(i) );
      //if (Param[i][1]>0) std::cout << Param[i][0]  << " & "<< Param[i][1] << " & ";
    }
    if (cleaning) {
      cleanedOnce = true;
    }
    parameterPerFile[ it - files.begin()].push_back(tInfo(binsX, binsY, binsZ, chi2, ndf));

    if(cleanedOnce && cleaning) {
      //Add chi2/ndf
      // parameterNames[parameterPerFile.back().size()-1] = "\\chindf";
      parameterNames.push_back("\\chindf");
    }
    f->Close();
    delete f;
  }

  std::cout << parameterPerFile.size()  << std::endl;

  //PrintTable(parameterPerFile, files, parameterNames);
  if(files.size() > 20) {
    std::cout << "Too many files, not printing table"  << std::endl;
  } else {
    PrintTable(parameterPerFile, parameterNames);
  }
  GraphParameters(parameterPerFile, parameterNames, files[0], true);
  GraphParameters(parameterPerFile, parameterNames, files[0], false);

  return 0;
}

int main (int argc, char** argv) {

  //gStyle->SetOptTitle(kTRUE);


  //sorted like the final output...
  //  rangeMin.push_back(0.0); rangeMax.push_back(17.0);

  rangeMin.push_back(0.9); rangeMax.push_back(1.3);


  rangeMin.push_back(0.232); rangeMax.push_back(0.258);
  rangeMin.push_back(0.258); rangeMax.push_back(0.277);
  rangeMin.push_back(0.258); rangeMax.push_back(0.277);

  rangeMin.push_back(-0.6); rangeMax.push_back(0.10);
  rangeMin.push_back(-0.6); rangeMax.push_back(0.10);
  rangeMin.push_back(-0.6); rangeMax.push_back(0.10);
  rangeMin.push_back(-0.6); rangeMax.push_back(0.10);

  rangeMin.push_back(-0.8); rangeMax.push_back(-0.15);
  rangeMin.push_back(-0.5); rangeMax.push_back( 1.20);
  rangeMin.push_back(-0.8); rangeMax.push_back(-0.15);
  rangeMin.push_back(-0.5); rangeMax.push_back( 1.20);

  // rangeMin.push_back(-0.1); rangeMax.push_back(0.10);
  // rangeMin.push_back(-0.63); rangeMax.push_back(-0.58);
  // rangeMin.push_back(-0.1); rangeMax.push_back(0.10);
  // rangeMin.push_back(-0.63); rangeMax.push_back(-0.58);

  rangeMin.push_back(-0.05); rangeMax.push_back( 0.05);
  rangeMin.push_back(-0.66); rangeMax.push_back(-0.56);
  rangeMin.push_back(-0.05); rangeMax.push_back( 0.05);
  rangeMin.push_back(-0.66); rangeMax.push_back(-0.56);

  rangeMin.push_back(-0.008); rangeMax.push_back(0.008);
  rangeMin.push_back(-0.68 ); rangeMax.push_back(-0.62);
  rangeMin.push_back(-0.008); rangeMax.push_back(0.008);
  rangeMin.push_back(-0.68 ); rangeMax.push_back(-0.62);


#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
  //gStyle->SetOptTitle(kTRUE);
#endif

  if( argc < 2 ) {
    std::cerr << "Use: Tables <filenames>" << std::endl;
    return 1;
  }
  StringVec files;
  //files.push_back("/bhwide_smeared_no_xsec_separate/BHWide%s_smeared_Separate_final.root");
  //files.push_back("/gen_no_xsec_overlap/control_Overlap_Generator.root");

  for (int i = 1; i < argc ;++i) {
    files.push_back( String_t (argv[i]) );
    std::cout << files.back()  << std::endl;
  }

  //  std::cout << "Number of Files " << files.size()  << std::endl;

  // files.push_back("/gen_no_xsec_separate/Generator_Separate_final.root");
  // files.push_back("/bhwide_no_xsec_separate/BHWide_Separate_final.root");
  // files.push_back("/bhwide_smeared_no_xsec_separate/BHWide%s_smeared_Separate_final.root");

  // files.push_back("/gen_xsec_separate/Generator_xsec_Separate_final.root");
  // files.push_back("/bhwide_xsec_separate/BHWide_xsec_Separate_final.root");
  // files.push_back("/bhwide_smeared_xsec_separate/BHWide_xsec_smeared_Separate_final.root");
  //if ( files.empty() ) return 0;
  return tables(files);
}



//typedef std::vector< std::vector< std::pair<double, double> > > ParameterList;
//void PrintTable(ParameterList& parameterlist, StringVec& fileNames, StringVec& parameterNames) {
void PrintTable(ParameterList& parameterlist, StringVec& parameterNames) {

  //We have a list of all the parameters plus errors Our vector is first the
  //file, then the parameter which gives a pair of doubles, value and error, we
  //have to loop over the parameters and then over the files
  //  const size_t maxPar(parameterlist[0].size());
  const size_t maxPar(20);
  const size_t parameterPerBlock(4);
  //    std::cout << std::setw(10) << parameterNames[nPar] << " & "  << std::endl;
  for (size_t startPar = 0; startPar < maxPar; startPar+=parameterPerBlock) {

    std::cout << "\\toprule\\%"  << std::endl;

    //Print the Parameter Names
    for (size_t nPar1 = startPar; nPar1 < startPar+parameterPerBlock; ++nPar1) {
      int nPar(0);
      try{
	nPar = getPos(nPar1);
      } catch (std::out_of_range &e) {
	continue;
      }
      if( (nPar1 < maxPar) ) {
	std::cout << std::setw(33) << std::string("\\tabtt{") + parameterNames[nPar] + std::string("}");
	if( nPar1 < startPar + parameterPerBlock - 1 ) { std::cout << " & "; }
      } else {
	std::cout << "\\tabtt{}";
      }
    }
    std::cout << " \\\\\\midrule\\%"  << std::endl;


    for (size_t nFile = 0; nFile < parameterlist.size() ;++nFile) {
      bool skipped = false;
      for (size_t nPar1 = startPar; (nPar1 < startPar+parameterPerBlock );++nPar1) {
	int nPar = getPos(nPar1);
	if( parameterlist[nFile][nPar].second < 0 ) { skipped = true; break; }
	if( nPar1 == 0 ) {
	  std::cout.precision(0);
	  std::cout << std::setw(15) << "\\tabtt{ "
		    << std::setw(7) << std::fixed << parameterlist[nFile][nPar].first <<  " / "
		    << std::setw(7) << std::fixed << parameterlist[nFile][nPar].second
		    << "}";
	  std::cout.precision(6);
	} else if( (nPar1 < maxPar) ) {
	  std::cout << std::setw(15) << std::fixed << parameterlist[nFile][nPar].first <<  " & "
		    << std::setw(15) << std::fixed << parameterlist[nFile][nPar].second;
	  //      if( nFile < parameterlist.size() - 1 ) std::cout << " & ";
	} else {
	  std::cout << std::setw(15) << "\\tabempty"<<  " & "
		    << std::setw(15) << "\\tabempty";
	}
	if( nPar1 < startPar+ parameterPerBlock - 1 ) std::cout << " & ";
      }
      if (not skipped) { std::cout << "  \\\\" << std::endl;}
    }
    std::cout << "\\bottomrule"  << std::endl;
  }

  return;
}


void GraphParameters(ParameterList& parameterlist, StringVec&, TString const& idString, bool useDefinedRange=true) {

  std::sort(parameterlist.begin(), parameterlist.end(), sortVecTInfoByChi2);

  gStyle->SetLineScalePS(1);
  gStyle->SetTitleFontSize(0.08);
  //Make a canvas with 4 by 5 pads, fill each pad with a graph showing the spread of the parameter
  int a4scale=3;
  TCanvas cPar("cPar", idString, 2970/a4scale, 2100/a4scale);//a4 format
  //  cPar.Divide(4,5); //Divide causes unncecessary margin around pads, have to do it by hand
  //Make the new pads...
  std::vector<TPad*> pads(20);
  double xSize=1.0/4;
  double ySize=1.0/5;
  for (int i = 0; i < 20; ++i) {
    TString name = Form("Pad%i",i+1);
    double xOffset = xSize*(i%4);
    double yOffset = fabs(1.0 - ySize*(i/4));
    pads.at(i) = new TPad(name,name, xOffset, fabs(yOffset-ySize), xOffset+xSize, yOffset, kWhite, short(0), short(0));
    pads.at(i)->SetTopMargin(0.01);
    pads.at(i)->SetRightMargin(0.06);
    pads.at(i)->SetLeftMargin(0.23);
    // pads.at(i)->SetMargin(0.0)
    cPar.cd();
    pads.at(i)->Draw();
  }

  const size_t NumberOfFiles(parameterlist.size());

  for (size_t i = 0; i < parameterlist[0].size();++i) {
    std::vector<double> vPars;
    std::vector<double> zeros(NumberOfFiles, 0);
    std::vector<double> lPars;
    std::vector<double> ePars;
    std::vector<int>    color;
    std::vector<int>    marker;
    int nPar(-1);
    try {
      nPar = getPos(i);
    } catch (std::out_of_range& e) {
      break;
    }
    for (size_t nFile = 0; nFile < NumberOfFiles ;++nFile) {

      //deal with chi2/ndf
      if( nPar == 19 ) {
	if( parameterlist[nFile][0].second < 0) continue;
	vPars.push_back(nFile+1);
	lPars.push_back( parameterlist[nFile][nPar].first / parameterlist[nFile][nPar].second);
	ePars.push_back( 0.0 );
      } else if( parameterlist[nFile][nPar].second < 0 ) {
	continue;
      } else {
	vPars.push_back(nFile+1);
	lPars.push_back(parameterlist[nFile][nPar].first);
	ePars.push_back(parameterlist[nFile][nPar].second);
      }

      if ( parameterlist[nFile][nPar].binZ != 1) { //only use colors, if we have 3d fits
	color.push_back( parameterlist[nFile][nPar].binE );
      } else {
	color.push_back( 20 ); //= kBlack
      }
      //Print order of bins to see if the acollinearity bins is increasing
      if ( i*10 == parameterlist[nFile][0].binE ) {
	std::cout << "Binning ID " << std::setw(3) << nFile << "   "
		  << std::setw(3)  << parameterlist[nFile][0].binA << " x "
		  << std::setw(3)  << parameterlist[nFile][0].binE << " x "
		  << std::setw(3)  << parameterlist[nFile][0].binZ
		  << std::setw(13) << parameterlist[nFile][19].first / parameterlist[nFile][19].second
		  << std::endl;
      }

		
    }//all files

    TMultiGraph  *mg = new TMultiGraph();
    //mg->SetTitle(TString(getLatexName(nPar)));
    mg->SetName( TString(getLatexName(nPar)));
    for (unsigned int j = 0; j < vPars.size(); ++j ) {
      //TGraphErrors*graph = new TGraphErrors(vPars.size(), &vPars[0], &lPars[0], &zeros[0], &ePars[0]);
      TGraphErrors*graph = new TGraphErrors(1, &vPars[j], &lPars[j], &zeros[j], &ePars[j]);
      int myColor = getColor(color[j]);
      int myMarker = getMarker(color[j]);
      graph->SetMarkerStyle(myMarker);
      graph->SetMarkerColor(myColor);
      graph->SetLineColor(myColor);
      graph->SetMarkerSize(0.8);

      // if( i == 0 ) { //chi2/ndf, no error bars make marker larger
      // 	graph->SetMarkerSize(1);
      // 	graph->SetMarkerStyle(kFullSquare);
      // }

      mg->Add(graph);
    }
    //std::cout << nPar+1 << "  " << graph->GetTitle()  << std::endl;
    //TPad * pad = static_cast<TPad*>(cPar.cd(i+1));
    // pad->SetLeftMargin( pad->GetLeftMargin() +0.05);
    // pad->SetRightMargin(pad->GetRightMargin()-0.05);
    // pad->SetTopMargin(0.00);
    // pad->SetBottomMargin(0.07);
    // pad->SetFillColor(i);

    pads.at(i)->cd();
    mg->Draw("APS");
    mg->GetXaxis()->SetLimits(0.0, mg->GetXaxis()->GetXmax());
    if (useDefinedRange) {
      mg->GetYaxis()->SetRangeUser(rangeMin[i], rangeMax[i]);
    }
    mg->GetYaxis()->SetTitle(TString(getLatexName(nPar)));
    mg->GetYaxis()->SetTitleSize(0.15);
    mg->GetYaxis()->SetTitleOffset(0.7);
    mg->GetXaxis()->SetLabelSize(0.08);
    mg->GetYaxis()->SetLabelSize(0.08);
    mg->GetXaxis()->SetTitle("Binning ID");
    mg->GetXaxis()->SetTitleSize(0.08);

    if( i == 0 ) { //Add a legend
      TLegend *leg =  new TLegend(0.35, 0.95-4*0.0675*0.1/0.06, 0.9, 0.95);
      leg->SetTextSize(0.1);

      TLegendEntry *ent20 = leg->AddEntry("","Energy Bins 20","p");
      TLegendEntry *ent30 = leg->AddEntry("","Energy Bins 30","p");
      TLegendEntry *ent40 = leg->AddEntry("","Energy Bins 40","p");
      TLegendEntry *ent50 = leg->AddEntry("","Energy Bins 50","p");

      ent20->SetMarkerStyle(getMarker(20));
      ent30->SetMarkerStyle(getMarker(30));
      ent40->SetMarkerStyle(getMarker(40));
      ent50->SetMarkerStyle(getMarker(50));

      ent20->SetMarkerColor(getColor(20));
      ent30->SetMarkerColor(getColor(30));
      ent40->SetMarkerColor(getColor(40));
      ent50->SetMarkerColor(getColor(50));

      leg->Draw();
    }


  }//all parameters

  TString title="ParameterSpread";
  if( not useDefinedRange ) {
    title +="_FreeRange";
  }

  cPar.SaveAs(title+".eps");
  TString copy(idString);
  copy.ReplaceAll(".root","");
  cPar.Print(title+".pdf","Title:"+copy);


  return;
}
