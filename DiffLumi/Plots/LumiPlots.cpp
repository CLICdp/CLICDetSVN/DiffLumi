/*
 * LumiPlots.cpp
 *
 *  Created on: Jul 25, 2012
 *      Author: stephane
 */
#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

#include "TString.h"
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include <TStyle.h>
#include <TLegend.h>

#include <cstdlib>
#include <iostream>
#include <iomanip>

int TH1_plots(TString filename, int nRebins){
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TFile*file = TFile::Open(filename);
  if(not file) return 1;
  TH1D*h_ref = 0;
  file->GetObject("lumi_reference",h_ref);
  if (! h_ref) return 1;
  h_ref->SetLineColor(kRed);
  h_ref->SetTitle("Reference: GP");
  h_ref->SetXTitle("#sqrt{s'}/#sqrt{s}");
  h_ref->SetYTitle("dN/d(#sqrt{s'}/#sqrt{s})");

  double maxgp = h_ref->GetBinCenter(h_ref->GetMaximumBin());
  int lowerbingp = h_ref->FindBin(maxgp-0.01*maxgp);
  int maxbingp = h_ref->FindBin(maxgp+0.01*maxgp);
  std::cout << "GP integral " << h_ref->Integral(lowerbingp,maxbingp)/h_ref->Integral()<<std::endl;

  TH1D*h_before = 0;
  file->GetObject("lumi_0",h_before);
  if (! h_before) return 1;
  h_before->SetLineColor(kGreen+2);
  h_before->SetTitle("Before Fit");
  h_before->SetXTitle("#sqrt{s'}/#sqrt{s}");

  TH1D*h_after = 0;
  file->GetObject("lumi_1",h_after);
  if (! h_after) return 1;
  h_after->SetLineColor(kBlue);
  h_after->SetTitle("After Fit");
  h_after->SetXTitle("#sqrt{s'}/#sqrt{s}");
  
  int lowerbin = h_after->FindBin(maxgp-0.01*maxgp);
  int maxbin = h_after->FindBin(maxgp+0.01*maxgp);
  std::cout << "Fit integral " << h_after->Integral(lowerbin,maxbin)/h_after->Integral()<<std::endl;
  
  h_ref->Sumw2();
  h_before->Sumw2();
  h_after->Sumw2();

  h_ref->SetAxisRange(h_ref->GetXaxis()->GetXmin(),h_ref->GetXaxis()->GetXmax(),"X");
  h_before->SetAxisRange(h_before->GetXaxis()->GetXmin(),h_before->GetXaxis()->GetXmax(),"X");
  h_after->SetAxisRange(h_after->GetXaxis()->GetXmin(),h_after->GetXaxis()->GetXmax(),"X");


//  TH1D h_int("h_int","h_int", 1000, 0., h_ref->GetXaxis()->GetXmax());
//  double inte=0;
//  for (int i = 1; i <= h_ref->GetNbinsX(); i++) {
//    inte += h_ref->Integral(i,i);
//    h_int.SetBinContent(i,inte);
//  }
//  double max = h_int.GetMaximum();
//  double slice = max*1./300;
//  double binsx[301] = {};
//  int multi = 0;
//  binsx[0]=0.;
//
//  for (int i =1; i<=h_int.GetNbinsX();i++) {
//    if (h_int.GetBinContent(i) > double(multi+1)*slice){
//      multi++;
//      binsx[multi] = h_int.GetBinLowEdge(i);
//      std::cout<<"bin "<<binsx[multi]<<std::endl;
//    }
//  }
//  binsx[300]=h_ref->GetXaxis()->GetXmax();

  
  h_ref->Rebin(nRebins);

//  TH1*h_ref_reb = h_ref->Rebin(300,"ref_rebin",binsx);
//  std::cout<<"ici"<<std::endl;
//  TCanvas c4("c4","c4");
//  h_ref_reb->Draw();
//  //h_int.Draw();
//  c4.SaveAs("testreb.eps");


  h_before->Rebin(nRebins);
  h_after->Rebin(nRebins);


  //Normalise histograms
  h_ref->Scale(1./h_ref->Integral());
  h_before->Scale(1./h_before->Integral());
  h_after->Scale(1./h_after->Integral());

  h_ref->GetXaxis()->SetRangeUser(0.,1.1);
  h_ref->SetAxisRange(5e-5,1,"Y");
  //h_ref->SetMinimum(5e-5);
  h_before->SetAxisRange(5e-5,1,"Y");
  //h_before->SetMinimum(5e-5);
  h_after->SetAxisRange(5e-5,1,"Y");
  //h_after->SetMinimum(5e-5);

  
  TCanvas c1("c1","c1");
  c1.SetLogy();
  h_ref->Draw("hist");
  //h_before->Draw("hist:same");
  h_after->Draw("hist:same");
#ifdef USE_RootStyle
  {
    RootStyle::AutoSetYRange (c1, 1.1);
    TLegend* leg = RootStyle::BuildLegend(c1, 0.3, 0.6, 0.6, 0.9);
    leg->SetMargin(0.16);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.06);
  }
#endif
  //TLegend*leg = c1.BuildLegend(0.3,0.6,0.6,0.9);
  //leg->SetBorderSize(0);
  //leg->SetFillColor(0);
  //leg->SetMargin(0.16);
  //leg->SetTextSize(0.06);
  c1.SaveAs("FullLumiLogy.eps");


  
  TCanvas c2("c2","c2");
  c2.SetLogy();
  h_ref->SetAxisRange(0.95,1.01, "X");
  h_ref->SetAxisRange(5e-3,7e-1,"Y");
  h_before->SetAxisRange(0.95,1.01, "X");
  h_after->SetAxisRange(0.95,1.01, "X");
  h_ref->Draw("hist");
  //h_before->Draw("hist:same");
  h_after->Draw("hist:same");
  //TLegend*leg2 = c2.BuildLegend(0.4,0.7,0.9,0.9);
  //leg2->SetBorderSize(0);
  //leg2->SetFillColor(0);
  TLegend* leg2 = NULL;
#ifdef USE_RootStyle
  {
    RootStyle::AutoSetYRange (c2, 1.1);
    leg2 = RootStyle::BuildLegend(c2, 0.3, 0.7, 0.9, 0.9);
    leg2->SetMargin(0.16);
    leg2->SetBorderSize(0);
    leg2->SetTextSize(0.06);
  }
#endif
  c2.SaveAs("ZoomedLumiLogy.eps");

  TCanvas c2b("c2b","c2b");
  h_ref->GetYaxis()->SetTitleOffset(1.4);
  h_ref->Draw("hist");
  //h_before->Draw("hist:same");
  h_after->Draw("hist:same");
  if (leg2)
    leg2->Draw();
  c2b.SaveAs("ZoomedLumi.eps");
  
  TCanvas c3("c3","c3");
  TH1D* h1_diff = (TH1D*)h_ref->Clone("ref_copy");
  h1_diff->Add(h_after,h_ref,1,-1);
  h1_diff->Divide(h_ref);
  h1_diff->SetAxisRange(0.1,1.01,"X");
  h1_diff->SetAxisRange(-0.5,1.5,"Y");
  //h1_diff->SetAxisRange(-0.04,0.08,"Y");
  h1_diff->Draw();
  h1_diff->SetTitle(filename+"  "+TString(nRebins));
  h1_diff->SetYTitle("(MC-GP)/GP");
  h1_diff->GetYaxis()->SetTitleOffset(1.3);
  c3.SaveAs("Ratio_all.eps");

  TCanvas c4("c4","c4");
  h1_diff->SetAxisRange(0.5,1.01,"X");
  h1_diff->SetAxisRange(-0.15,0.15,"Y");
  h1_diff->Draw();
  c4.SaveAs("Ratio_0.5.eps");

  TCanvas c5("c5","c5");
  h1_diff->SetAxisRange(0.95,1.01,"X");
  //h1_diff->SetAxisRange(-0.1,0.06,"Y");
  h1_diff->Draw();
  c4.SaveAs("Ratio_0.95.eps");
  return 0;
}

int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  //gStyle->SetOptTitle(kTRUE);
  TString filename("");
  int nRebins(1);
  if(argc < 3) {
    std::cerr << "No filename given... abort\n"
	      << "LumiPlots control.root nRebins"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    nRebins = atoi(argv[2]);
  }
  return TH1_plots(filename, nRebins);
}
