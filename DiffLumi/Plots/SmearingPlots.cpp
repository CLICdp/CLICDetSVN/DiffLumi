/*
 * SmearingPlots.cpp
 *
 *  Created on: Aug 27, 2012
 *      Author: stephane
 */
#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif
#include <TStyle.h>
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TMath.h"

#include <iostream>

int plot(TString filename, float cterm, float stocterm){
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TFile*file = TFile::Open(filename);
  if(not file) return 1;
  TH2D* h1 = static_cast<TH2D*>(file->Get("H2DTHTREAT504"));
  h1->SetXTitle("E [GeV]");
  h1->SetYTitle("#Delta#theta [rad]");
  TCanvas c1("c1","c1");
  c1.SetLogz();
#ifdef USE_RootStyle
  RootStyle::SetPad2D(&c1);
#endif
  h1->Draw("colz");
  c1.SaveAs("DeltaTheta_vs_E.eps");

  double p_e1 = 1.;
  double e_factor = TMath::Sqrt(stocterm*stocterm/p_e1 + 
				cterm*cterm);
  std::cout <<"e_factor needs to be used somewhere "<<e_factor<<std::endl;
  return 0;
}
int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  TString filename("");
  float cterm = 0.0;
  float stocterm = 0.0;
  if(argc < 3) {
    std::cerr << "No filename given... abort\n"
	      << "SmearingPlots InputForSmearing.root 0.24 0.014"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    cterm = atof(argv[2]);
    stocterm = atof(argv[3]);
  }
  plot(filename,cterm,stocterm);

  return 0;
}
