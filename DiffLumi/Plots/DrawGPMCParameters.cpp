#include "ParameterNames.hh"

#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1D.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TPaveText.h>
#include <TMath.h>
#include <TError.h>

#include <vector>
#include <iostream>
#include <iomanip>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif


typedef std::vector< std::string > StringVec;


int MakeParaPlots(StringVec fileNames);


int main (int ac, char **argv)
{
  
  gErrorIgnoreLevel=kWarning;

  StringVec filenames;
  for (int i = 1; i < ac;++i) {
    filenames.push_back(argv[i]);
  }

  if(filenames.empty() ) {
    std::cout << "No files given"  << std::endl;
    return 1;
  }

  MakeParaPlots(filenames);

  return 0;

}


int MakeParaPlots(StringVec fileNames) {

  std::vector<TH1D> hist_vec;
  for (int j=0;j<35;++j){
    TH1D h1(Form("h_%d",j),"pulls",10000,-0.5,0.5);
    hist_vec.push_back(h1);
  }

  std::vector<  std::vector< double > > parVectors(35);
  std::vector<  std::vector< double > > errVectors(35);

  for(unsigned int i = 0; i < fileNames.size(); i++){//loop on files
    int empty=0;
    Double_t        Param[35][2];   //[n_params]
    Int_t           status;
    Int_t           covQual;
    //    Double_t        Value;
    TFile*f = TFile::Open(fileNames[i].c_str());
    if (f==NULL) continue;
    TTree*fr = NULL;
    //    TTree*init = NULL;
    f->GetObject("FitResult",fr);
    if (fr == NULL) continue;
    // f->GetObject("InitialParameters",init);
    // if (init == NULL) continue;
    
    fr->SetBranchAddress("Param", Param);
    fr->SetBranchAddress("status", &status);
    fr->SetBranchAddress("covQual", &covQual);
    //    init->SetBranchAddress("Value",&Value);
    //    Long64_t n_params = fr->GetEntries();
    fr->GetEntry(0);
    if (status!=0 || covQual!=3) {
      f->Close();
      delete f;
      continue;
    }
    for (int j  =0; j<35; ++j){
      if (Param[j][1]==0) {
	empty++;
	continue;
      }
      //init->GetEntry(j);

      //      double pull = (Param[j][0]-Value)/Param[j][1];
      hist_vec.at(j).Fill(Param[j][0]);
      parVectors[j].push_back(Param[j][0]);
      errVectors[j].push_back(Param[j][1]);
    }
    f->Close();
    delete f;
  }
  int hent = 0;
  TCanvas c("c","c");
  //c.Divide(2,16);
  gStyle->SetOptStat(1111);
  gStyle->SetOptFit(1111);
  //c.SaveAs("Pulls.eps[");


  for (int i = 0; i<35;++i){
    //c.cd(i);
    TH1D h = hist_vec.at(i);
    if (h.GetEntries()<1) continue;
    h.SetXTitle(Form("Pull %s", getLatexName(i).c_str()));
    h.SetYTitle("Entries [A.U.]");
    TF1 f1("f1","gaus",-5,5);
    //h.SetName(gParameterNames[i].c_str());
    h.Draw();
    hent = h.GetEntries();
    h.Fit("f1");
    double mean = h.GetMean();
    double rms = h.GetRMS();
    double mu = f1.GetParameter(1);
    double mue = f1.GetParError(1);
    double sigma = f1.GetParameter(2);
    double sigmae = f1.GetParError(2);

    //from Parameters
    double pMean = TMath::Mean(parVectors[i].begin(), parVectors[i].end());
    double eMean = TMath::RMS(parVectors[i].begin(), parVectors[i].end());

    double pErr = TMath::Mean(errVectors[i].begin(), errVectors[i].end());
    double eErr = TMath::RMS(errVectors[i].begin(), errVectors[i].end());


    TPaveText p(0.6,0.6,0.9,0.9,"brNDC");

    h.SetAxisRange(mean-3*rms, mean+3*rms,"X");

    p.SetTextSize(0.04);
    p.SetBorderSize(0);
    p.AddText(Form("Mean = %2.3f",mean));
    p.AddText(Form("RMS = %2.3f",rms));
    p.AddText(Form("#mu = %2.3f +/- %2.3f",mu,mue));
    p.AddText(Form("#sigma = %2.3f +/- %2.3f",sigma,sigmae));
    std::cout.precision(5);
    std::cout << std::setw(10) << getLatexMacro(i).c_str() << " & "
	      << std::setw(15) << pMean << " & "
	      << std::setw(15) << eMean << " & " 
	      << std::setw(15) << pErr  << " & "
	      << std::setw(15) << pErr/eMean  << " & ~ \\\\"<<std::endl;
    p.Draw();
    c.SaveAs(Form("Pulls_%d.eps",i));
  }
  c.Update();
  //c.SaveAs("Pulls.eps]");
  std::cout << "entries "  << hent << std::endl;
  return 0;
}
