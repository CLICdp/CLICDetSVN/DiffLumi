#include "Utilities.h"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TCanvas.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TTree.h>

#include <iostream>
#include <iomanip>


TH1D DrawEnergy(TString name, TString fileName, TString treeName, double beamEnergy, double minE=-0.01, double maxE=0.01);
TH2D DrawEnergyVsZ(TString name, TString fileName, TString treeName, double beamEnergy, double zHalfLength);

int main () {

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  TCanvas canv("canv","canv");

  TH2D  histo3000 = DrawEnergyVsZ("hist3000", Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root", "MyTree", 1500.0, 150);
  histo3000.Draw("col");
  canv.SaveAs("EnergyVsZ3000.eps");

  TH2D histo500 = DrawEnergyVsZ("hist500", Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_500GeV.root", "bds500", 250.0, 300);
  histo500.Draw("col");
  canv.SaveAs("EnergyVsZ500.eps");



  TH1D  histoE3000 = DrawEnergy("histE3000", Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root", "MyTree", 1500.0, -0.0055, 0.0065);
  histoE3000.Draw();
  canv.SaveAs("EnergySpread3000.eps");

  TH1D histoE500 = DrawEnergy("histE500", Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_500GeV.root", "bds500", 250.0, -0.005, 0.011);
  histoE500.Draw();
  canv.SaveAs("EnergySpread500.eps");

  return 0;
}


TH1D DrawEnergy(TString name, TString fileName, TString treeName, double beamEnergy, double minE, double maxE){

  TH1D histo(name, name,
	     200, minE, maxE);
  
  histo.SetXTitle("#DeltaE/E_{Beam}");
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  TTree *tree = (TTree*)file->Get(treeName);
  if(not tree) {
    std::cerr << "Tree " << treeName << " not found in file " << fileName  << std::endl;
    exit(1);
  }

  double E1, z;
  if( not (tree->SetBranchAddress("E1", &E1) == 0)) {
    tree->SetBranchAddress("E", &E1);
  }
  tree->SetBranchAddress("z", &z);
  long long entries = tree->GetEntries();

  std::cout << "Running over file: " << fileName  << " .... " << entries << std::endl;

  for (long long i = 0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);
    E1 /= beamEnergy;
    histo.Fill(E1-1.0);
  }

  histo.Scale( 1./histo.Integral("width") );
  
  std::cout << std::endl; 
  file->Close();

  return histo;

}//DrawEnergySpread


TH2D DrawEnergyVsZ(TString name, TString fileName, TString treeName, double beamEnergy, double zHalfLength){

  TH2D histo(name, name,
	     200,  -zHalfLength, zHalfLength,
	     200, -0.01, 0.01);
  
  histo.SetXTitle("Z [#mum]");
  histo.SetYTitle("#DeltaE/E_{Beam}   ");//The three whitespaces at the end are there for a reason!
  histo.GetYaxis()->SetNdivisions(502);

  TFile *file = TFile::Open(fileName);
  TTree *tree = (TTree*)file->Get(treeName);
  if(not tree) {
    std::cerr << "Tree " << treeName << " not found in file " << fileName  << std::endl;
    exit(1);
  }

  double E1, z;
  if( not (tree->SetBranchAddress("E1", &E1) == 0)) {
    tree->SetBranchAddress("E", &E1);
  }
  tree->SetBranchAddress("z", &z);
  long long entries = tree->GetEntries();

  std::cout << "Running over file: " << fileName  << " .... " << entries << std::endl;

  for (long long i = 0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);
    E1 /= beamEnergy;
    histo.Fill(z, E1-1.0);
  }

  std::cout << std::endl; 
  file->Close();

  return histo;

}//DrawEnergySpread
