#include "Utilities.h"
#include "DrawSpectra.hh"

#include "Riostream.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMultiGraph.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TROOT.h"
#include "TStyle.h"
#include <TLegend.h>
#include "TTree.h"
#include "TSpline.h"


#include <iostream>


#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

void MakeCrossTree(TString inFile, TString outFile);
void DrawCrossSections(TString file1, TString file2, double energy);

double CalculateEffectiveCrossSection(TString lumiFile, TGraph* gXsec, double energy);

TGraph* e1e1(TString filename, double factor=1){
  static int counter=0;
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TFile*f = TFile::Open(filename);
  if( not f ) { std::cerr << "File not found"  << std::endl; return NULL; }

  TTree*t = dynamic_cast<TTree*>(f->Get("Tree"));
  if (not t) { std::cerr << "Tree not found"  << std::endl; return NULL; }

  //t->ReadFile("e1e1_xsec.txt");
  //t->Write();
  int E=0;
  double xsec = 0.0;
  t->SetBranchAddress("E",&E);
  t->SetBranchAddress("xsec",&xsec);
  Long64_t entries=  t->GetEntries();

  std::vector<double> aEng(entries), aXsec(entries);

  for (Long64_t i = 0; i< entries; ++i){
    t->GetEntry(i);
    //std::cout<<E<<"  "<<xsec<<std::endl;
    aEng[i] = E;
    aXsec[i] = xsec*factor;
  }
  f->Close();

  TCanvas c1("c1","c1");
  c1.SetLogy();
  TGraph *graph = new TGraph(entries, &aEng[0], &aXsec[0]);
  graph->SetTitle(";#sqrt{s} [GeV];#sigma(e^{+}e^{-} #rightarrow e^{+}e^{-}) [fb]");
  graph->Draw("APL");
  TF1 f1("f1","[0]*pow(x,2*[1])",50,3000);
  f1.SetParameters(1e10, -1.0);
  //graph->Fit(&f1);
  const TString ti("spline1");
  TSpline5 *spline = new TSpline5(ti,graph);
  spline->SetLineColor(kBlue);
  spline->Draw("same");
  c1.SaveAs(Form("BhabhaCrossSectionFitted_%i.eps", counter));
  //f1.Write();
  counter++;
  return graph;
}

int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  gStyle->SetLineScalePS(4);
  gStyle->SetHistLineWidth(4);
  //gStyle->SetOptTitle(kTRUE);
  TString filename("");
  if(argc < 3) {
    std::cerr << "No filename given... abort\n"
	      << "BhabhaCrossSection <CM Energy> whizardCross.root [BHWideCross.root]"
	      << "\n or \nBhabhaCrossSection <CM Energy> infile.txt outfile.root READ"
	      << std::endl;
    return 1;
  } else if(argc == 3) {
    filename = TString(argv[2]);
    double energy = std::atof(argv[1]);
    e1e1(filename, energy);
  } else if(argc == 4) {
    double energy = std::atof(argv[1]);
    filename = TString(argv[2]);
    TString filename2 = TString(argv[3]);
    DrawCrossSections(filename,filename2, energy);
  } else if (argc ==  5) {
    //double energy = std::atof(argv[1]);
    TString fileIn = TString(argv[2]);
    TString fileOut = TString(argv[3]);
    MakeCrossTree(fileIn, fileOut);
  }


  return 0;
}

void DrawCrossSections(TString file1, TString file2, double energy){

    TGraph *graph1 = e1e1(file1);
    if( not graph1 ) { std::cerr << "Error with graph1"  << std::endl; return; }
    graph1->SetName("Whizard");
    graph1->SetTitle("Whizard");
    TGraph *graph2 = e1e1(file2, 1e6);
    if( not graph2 ) { std::cerr << "Error with graph2"  << std::endl; return; }
    graph2->SetName("BHWide");
    graph2->SetTitle("BHWide");

#ifdef USE_RootStyle
    RootStyle::MyColor color;
    RootStyle::SetAllColors(graph1, color.GetColor());
    RootStyle::SetAllColors(graph2, color.GetColor());
#endif

    TCanvas c1("c1","Bhabha Cross Sections");
    graph1->Draw("AP");
    graph2->Draw("P");
    c1.SetLogy();
    TLegend *leg = NULL;
#ifdef USE_RootStyle
    leg = RootStyle::BuildLegend(&c1, 0.6, 0.9 - 2 * 0.0625, 0.9, 0.9);
#else
    leg = c1.BuildLegend(0.4, 0.9 - 2 * 0.0625, 0.9, 0.9);
#endif
    leg->SetBorderSize(0);
    leg->SetTextSize(0.06);

    c1.DrawFrame(-50, 8e3, energy*1.01, 3e7,";#sqrt{s} [GeV];#sigma(e^{+}e^{-} #rightarrow e^{+}e^{-}) [fb]");
    graph1->Draw("P");
    graph2->Draw("P");
    leg->Draw();

    c1.SaveAs("BhabhaCrossSections.eps");

    if(energy > 2000) {
      CalculateEffectiveCrossSection(TString(Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root"), graph1, energy);
      CalculateEffectiveCrossSection(TString(Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root"), graph2, energy);
    } else {
      CalculateEffectiveCrossSection(TString(Utility::GetWorkingDirectory()+"/RootFiles/Lumi350_Smeared_2.root"), graph1, energy);
      CalculateEffectiveCrossSection(TString(Utility::GetWorkingDirectory()+"/RootFiles/Lumi350_Smeared_2.root"), graph2, energy);
    }

    return;

}

void MakeCrossTree(TString inFile, TString outFile)  {
  TFile *file = TFile::Open(outFile,"RECREATE");

  TTree *tree = new TTree("Tree","Tree");
  tree->ReadFile(inFile,"E/I:xsec/D");

  // double eng, xsec;
  // tree->SetBranchAddress("E",&eng);
  // tree->SetBranchAddress("xsec",&xsec);

  // double aEng[3100], aXsec[3100];
  // for (int i = 0; i < 3100 ;++i) {
  //   tree->GetEntry(i);
  //   aEng[i] = eng;
  //   aXsec[i] = xsec*1e6;
  // }

  // TGraph *graph = new TGraph(3100, aEng, aXsec);
  // graph->SetTitle(";Energy [GeV];Xsec [fb]");
  // TCanvas *c1 = new TCanvas("c1","c1",800,700);
  // c1->SetLogy();
  // graph->Draw("AP");
  // graph->GetYaxis()->SetRangeUser(8e3,3e7);
  // c1->SaveAs("BHWideCross.eps");
  file->Write();
  file->Close();
  delete file;

  return;
}


/**
 * Calculate the Effective Cross section by Summing the product of lumispectrum and cross-section
 */

double CalculateEffectiveCrossSection(TString lumiFile, TGraph* gXsec, double energy) {
  //  TGraph *gXsec = e1e1(xSecFile, 1.0); //unit depends on input file;

  TFile *file = TFile::Open(lumiFile);
  TTree *tree = (TTree*)file->Get("MyTree");
  int bins = 200;
  double maxS = energy*1.03;
  TH1D hXsec("hXsec","Bhabha cross-section;#sqrt{s} [GeV]; Normalised Distributions [a.u.]", 1000, 0, maxS);
  TH1D hLumi("hLumi","GP Basic",bins, 0, maxS);
  TH1D hLuXs("hLuXs","GP #times cross-section",bins, 0, maxS);
  hLumi.SetXTitle( hXsec.GetXaxis()->GetTitle() );
  hLumi.SetYTitle( hXsec.GetYaxis()->GetTitle() );

  double E1, E2;
  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);
  Long64_t entries = 10000000;
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;
  std::cout << "Running over file: " << lumiFile << " .... " << entries << std::endl;

  double effectiveXSec(0.0);

  for (Long64_t i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);
    if( E1 < 0 || E2 < 0) continue;
    const double roots = energy*sqrt(E1*E2);
    const double xsec = gXsec->Eval(roots);
    // std::cout << std::setw(15)  << roots << std::setw(15) << xsec  << std::endl;
    if ( roots > 0.5*energy ) {
      effectiveXSec += xsec;
    }
    hLumi.Fill(roots);
    hLuXs.Fill(roots, xsec);
  }

  for (int i = 10; i < hXsec.GetNbinsX();++i) {
    const double roots = hXsec.GetBinCenter(i);
    const double xsec = gXsec->Eval(roots);
    hXsec.SetBinContent(i, xsec);
  }
  std::cout << std::endl;

  effectiveXSec /= double(entries);

  std::cout << "The effective cross-section is " << effectiveXSec*1e-3  << " pb" << std::endl;

  file->Close();
  delete gXsec;

  hXsec.Scale( 10.0 / hXsec.Integral("width") );
  hLumi.Scale( 1.0 / hLumi.Integral() );
  hLuXs.Scale( 1.0 / hLuXs.Integral() );

  TCanvas c("cWeighted","Cross-section weighted Lumi-spectrum");
  hXsec.Draw();
  hLumi.Draw("same");
  hLuXs.Draw("same");

  c.SetLogy();

    TString gpWeightedFile  = Utility::GetWorkingDirectory()+"/RootFiles/Lumi_GP_BHWeighted_Double_slives50_Random.root";
    TH1D histogram(DrawLumiSpectrum("hGPWeighted", gpWeightedFile, 0.0, maxS, 5000000, false, true, bins, energy));
    histogram.SetTitle("GP Scaled");
    histogram.Scale(1./histogram.Integral() );
  if (energy > 2000) {
    histogram.Draw("same,hist");
  }

#ifdef USE_RootStyle
  RootStyle::MyColor colors;
  RootStyle::SetAllColors(&hXsec, colors.GetColor() );
  RootStyle::SetAllColors(&hLumi, colors.GetColor() );
  RootStyle::SetAllColors(&hLuXs, colors.GetColor() );
  RootStyle::SetAllColors(&histogram, colors.GetColor() );
  //TLegend *leg = RootStyle::BuildLegend(c, 0.3, 0.65, 0.6, 0.9);
  TLegend *leg = RootStyle::BuildLegend(c, 0.3, 0.7125, 0.6, 0.9); 
  leg->SetBorderSize(0);
  leg->SetTextSize(0.06);
  leg->SetFillColor(10);
#endif

  hXsec.SetAxisRange(2e-5, 3.6, "Y");
  hLumi.SetAxisRange(1e-6, 0.5, "Y");
  static int counter = 0;
  c.SaveAs(Form("WeightedCrossSection_%i.eps", counter));

  counter++;
  return effectiveXSec*1e-3;
}
