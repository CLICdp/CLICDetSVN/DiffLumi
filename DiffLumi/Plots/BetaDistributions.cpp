#include "BetaFunction.h"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TMath.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TLegend.h>
#include <TString.h>


/***
 * Draw beta distributions with different parameter combinations
 */


int main () {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
  RootStyle::MyColor colors;
#endif

  // TwoBS funcs;

  double para[4][2] = {{ -0.2, -0.3},
		       { -0.9,  0.4},
		       {  3.0, -0.5},
		       {  0.5,  0.4}};

  BetaFunction TwoPeak(para[0][0],para[0][1]);
  BetaFunction OnePeak(para[1][0],para[1][1]);
  BetaFunction PeakOne(para[2][0],para[2][1]);
  BetaFunction NoPeaks(para[3][0],para[3][1]);

  TwoPeak.SetBetaLimit(1.0);
  OnePeak.SetBetaLimit(1.0);
  PeakOne.SetBetaLimit(1.0);
  NoPeaks.SetBetaLimit(1.0);

  TF1 fTwoPeak( TString::Format("a_{1} = % 1.1f  a_{2} = % 1.1f",para[0][0],para[0][1]),&TwoPeak, 0.0, 1, 0);
  TF1 fOnePeak( TString::Format("a_{1} = % 1.1f  a_{2} = % 1.1f",para[1][0],para[1][1]),&OnePeak, 0.0, 1, 0);
  TF1 fPeakOne( TString::Format("a_{1} = % 1.1f  a_{2} = % 1.1f",para[2][0],para[2][1]),&PeakOne, 0.0, 1, 0);
  TF1 fNoPeaks( TString::Format("a_{1} = % 1.1f  a_{2} = % 1.1f",para[3][0],para[3][1]),&NoPeaks, 0.0, 1, 0);

  fTwoPeak.SetNpx(1000);
  fOnePeak.SetNpx(1000);
  fPeakOne.SetNpx(1000);
  fNoPeaks.SetNpx(1000);

  // std::cout << TwoPeak.GetNormalization()  << std::endl;
  // std::cout << OnePeak.GetNormalization()  << std::endl;
  // std::cout << PeakOne.GetNormalization()  << std::endl;
  // std::cout << NoPeaks.GetNormalization()  << std::endl;

  TCanvas canv("canv","canv");
  canv.DrawFrame(0, 0.01, 1, 100,";x;b(x#;a_{1},a_{2})");
  canv.SetLogy();
#ifdef USE_RootStyle
  fTwoPeak.SetLineColor(colors.GetColor());
  fOnePeak.SetLineColor(colors.GetColor());
  fPeakOne.SetLineColor(colors.GetColor());
  fNoPeaks.SetLineColor(colors.GetColor());
  fTwoPeak.SetLineWidth(3);
  fOnePeak.SetLineWidth(3);
  fPeakOne.SetLineWidth(3);
  fNoPeaks.SetLineWidth(3);
#endif

  fTwoPeak.Draw("same");
  fOnePeak.Draw("same");
  fPeakOne.Draw("same");
  fNoPeaks.Draw("same");

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(&canv, 0.3, 0.9-4*0.0625, 0.6, 0.9);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
  leg->DeleteEntry();
#endif

  canv.SaveAs("BetaDistributions.eps");

  return 0;

}
