#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TTree.h>

#include <cstdlib>
#include <iostream>
#include <iomanip>
#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif


int TH1_plots(TString filename, int nRebins) {
  //gROOT->ProcessLine(".L CLICStyle.C");
  //CLICStyle();
  //gStyle->SetLineScalePS(0.1);

  //TFile*f = new TFile("final.root");
  //TFile*f = new TFile("final_no_beam_spread_body_E_smeared_nominal.root");
  TFile*f = TFile::Open(filename);
  //TFile*f = new TFile("../CreateLumiFile/MakeLumiFile/final_BHWide_BHWideTuples_3e5_E_Smeared_0.20_0.05.root");
  TH1D*h1 = (TH1D*)f->Get("RefHisto_ref");
  //TH1D*h1 = (TH1D*)f->Get("RefHisto");
  h1->SetXTitle("#sqrt{s}");


  TH1D*h2 = (TH1D*)f->Get("lumiHisto0_b_before");
  //TH1D*h2 = (TH1D*)f->Get("Before");
  h2->SetXTitle("#sqrt{s}");
  TH1D* h3 = (TH1D*)f->Get("lumiHisto1_b_after");
  //TH1D*h3 = (TH1D*)f->Get("After");
  h3->SetXTitle("#sqrt{s}");


  h1->Sumw2();
  h2->Sumw2();
  h3->Sumw2();


  //Reset The axis ranges
  h1->SetAxisRange(h1->GetXaxis()->GetXmin(),h1->GetXaxis()->GetXmax(),"X");
  h2->SetAxisRange(h2->GetXaxis()->GetXmin(),h2->GetXaxis()->GetXmax(),"X");
  h3->SetAxisRange(h3->GetXaxis()->GetXmin(),h3->GetXaxis()->GetXmax(),"X");


  TLegend*l = new TLegend(0.2,0.8,0.4,0.9);
  l->AddEntry(h1,"Guinea Pig","L");
  //l->AddEntry(h2,"Input Parameters","L");
  l->AddEntry(h3,"Fitted Parameters","L");
  l->SetTextSize(0.05);
  l->SetBorderSize(0);

  h1->Rebin(nRebins);
  h2->Rebin(nRebins);
  h3->Rebin(nRebins);


  h1->SetLineColor(kRed);
  h2->SetLineColor(kBlue);
  h3->SetLineColor(kGreen+2);

  // std::cout << h1->Integral()  << std::endl;
  // std::cout << h2->Integral()  << std::endl;
  // std::cout << h3->Integral()  << std::endl;

  //Normalise histograms
  h1->Scale(1./h1->Integral());
  h2->Scale(1./h2->Integral());
  h3->Scale(1./h3->Integral());

  h1->GetXaxis()->SetRangeUser(0.,1.009);

  h1->SetMinimum(3e-4);
  h2->SetMinimum(3e-4);
  h3->SetMinimum(3e-4);


  TCanvas c1("c1","c1");
  c1.SetLogy();
  //  h1->GetXaxis()->SetRangeUser(0.6,1.01);
  h1->Draw();
  //h2->Draw("same");
  h3->Draw("same");
  l->Draw();
  c1.Print("final_res.pdf(");


  h1->SetAxisRange(0.6,1.01);
  c1.Print("final_res.pdf");

  c1.SetLogy(0);
  h1->SetAxisRange(0.9,1.01);
  c1.Print("final_res.pdf");

  h1->SetAxisRange(0.95,1.01);
  c1.Print("final_res.pdf");


  c1.SetLogy(0);
  TH1D* h1_diff = (TH1D*)h1->Clone("h1_diff");
  h1_diff->Add(h3,h1,1,-1);
  h1_diff->Divide(h1);
  h1_diff->SetAxisRange(0.6,1.009,"X");
  //  h1_diff->SetAxisRange(-0.15,0.15,"Y");
  //  h1_diff->SetAxisRange(-0.04,0.08,"Y");
  h1_diff->Draw();
  h1_diff->SetTitle(filename+"  "+TString(nRebins));
  h1_diff->SetYTitle("(GP-MC)/GP");
  h1_diff->GetYaxis()->SetTitleOffset(1.3);
  c1.Print("final_res.pdf");

  h1_diff->SetAxisRange(0.9,1.009);
  c1.Print("final_res.pdf");

  h1_diff->SetAxisRange(0.97, 1.03,"X");
  c1.Print("final_res.pdf)");


//   TH1D*h1_e1 = (TH1D*)f->Get("h_e1_ref");
//   h1_e1->SetXTitle("E1 GeV");
//   h1_e1->Sumw2();
//   TH1D*h2_e1 = (TH1D*)f->Get("enE1_before");
//   h2_e1->SetXTitle("E1 GeV");
//   h2_e1->Sumw2();
//   TH1D*h3_e1 = (TH1D*)f->Get("enE1_after");
//   h3_e1->SetXTitle("E1 GeV");
//   h3_e1->Sumw2();
//   h1_e1->Rebin(2);
//   h1_e1->SetLineColor(kRed);
//   h1_e1->Scale(1/h1_e1->Integral());
//   //h1_e1->GetXaxis()->SetRangeUser(0.,1.009);
//   h1_e1->SetMinimum(3e-4);
//   h2_e1->Rebin(2);
//   h2_e1->SetMinimum(3e-4);
//   h2_e1->Scale(1/h2_e1->Integral());
//   h2_e1->SetLineColor(kBlue);
//   h3_e1->Rebin(2);
//   h3_e1->SetLineColor(kGreen+4);
//   h3_e1->SetMinimum(3e-4);
//   h3_e1->Scale(1/h3_e1->Integral());

//   TCanvas *c3 = new TCanvas("c3","c3");
//   c3->SetLogy();
//   h1_e1->Draw();
//   h2_e1->Draw("same");
//   h3_e1->Draw("same");
//   l->Draw();
//   c3->Print("final_res.pdf");
//   TH1D*h1_e2 = (TH1D*)f->Get("h_e2_ref");
//   h1_e2->SetXTitle("E2 GeV");
//   h1_e2->Sumw2();
//   TH1D*h2_e2 = (TH1D*)f->Get("enE2_before");
//   h2_e2->SetXTitle("E2 GeV");
//   h2_e2->Sumw2();
//   TH1D*h3_e2 = (TH1D*)f->Get("enE2_after");
//   h3_e2->SetXTitle("E2 GeV");
//   h3_e2->Sumw2();
//   h1_e2->Rebin(2);
//   h1_e2->SetLineColor(kRed);
//   h1_e2->Scale(1/h1_e2->Integral());
//   //h1_e2->GetXaxis()->SetRangeUser(0.,1.009);
//   h1_e2->SetMinimum(3e-4);
//   h2_e2->Rebin(2);
//   h2_e2->SetMinimum(3e-4);
//   h2_e2->Scale(1/h2_e2->Integral());
//   h2_e2->SetLineColor(kBlue);
//   h3_e2->Rebin(2);
//   h3_e2->SetLineColor(kGreen+4);
//   h3_e2->SetMinimum(3e-4);
//   h3_e2->Scale(1/h3_e2->Integral());

//   TCanvas *c4 = new TCanvas("c4","c4");
//   c4->SetLogy();
//   h1_e2->Draw("");
//   h2_e2->Draw("same");
//   h3_e2->Draw("same");
//   l->Draw();
//   c4->Print("final_res.pdf");


//   Double_t binsx[] = {0,  0.212487, 0.250951, 0.278496, 0.301215, 0.321194, 0.339346, 0.356001,
//                       0.371472, 0.385976, 0.399745, 0.413026, 0.425935, 0.438372, 0.450231, 0.461832, 0.472909, 0.483826,
//                       0.494424, 0.504634, 0.514602, 0.524253, 0.533879, 0.54333, 0.552498, 0.561545, 0.570431, 0.579059,
//                       0.587464, 0.595662, 0.603768, 0.611631, 0.61945, 0.62713, 0.634613, 0.641986, 0.649222, 0.656337,
//                       0.663306, 0.670226, 0.676941, 0.683564, 0.690059, 0.696419, 0.7027, 0.708782, 0.714815, 0.720704,
//                       0.726489, 0.732194, 0.737829, 0.743284, 0.748691, 0.753964, 0.759166, 0.764321, 0.769361, 0.774293,
//                       0.779139, 0.783936, 0.788612, 0.793204, 0.797686, 0.802132, 0.80649, 0.81083, 0.815057, 0.819188,
//                       0.823262, 0.827284, 0.831209, 0.835072, 0.838895, 0.842643, 0.846282, 0.849876, 0.853411, 0.856905,
//                       0.860333, 0.863714, 0.867009, 0.870205, 0.873371, 0.876495, 0.879611, 0.882612, 0.885557, 0.888488,
//                       0.891327, 0.894162, 0.896905, 0.899614, 0.902282, 0.904904, 0.907462, 0.910024, 0.912487, 0.914927,
//                       0.917315, 0.91965, 0.92192, 0.924187, 0.926405, 0.928559, 0.930664, 0.932738, 0.934768, 0.936801,
//                       0.938774, 0.940718, 0.942595, 0.944431, 0.946244, 0.948041, 0.949768, 0.951474, 0.953153, 0.954781,
//                       0.956365, 0.957933, 0.959478, 0.960982, 0.962435, 0.963854, 0.965239, 0.966612, 0.967928, 0.969239,
//                       0.970507, 0.971726, 0.972933, 0.974089, 0.975222, 0.976326, 0.977397, 0.97845, 0.979479, 0.980479,
//                       0.981428, 0.982364, 0.983278, 0.984152, 0.985006, 0.985822, 0.986613, 0.987387, 0.988115, 0.988824,
//                       0.989502, 0.990163, 0.990791, 0.991389, 0.991962, 0.992505, 0.993026, 0.993518, 0.99398, 0.994402,
//                       0.994794, 0.995092, 0.995325, 0.995537, 0.995734, 0.99592, 0.996095, 0.996261, 0.996419, 0.99657,
//                       0.996715, 0.996855, 0.996989, 0.997119, 0.997245, 0.997369, 0.997489, 0.997607, 0.997722, 0.997833,
//                       0.997943, 0.998051, 0.998156, 0.998261, 0.998365, 0.998465, 0.998565, 0.998665, 0.998764, 0.99886,
//                       0.998955, 0.99905, 0.999142, 0.999235, 0.999326, 0.999416, 0.999504, 0.999591, 0.999676, 0.99976,
//                       0.999841, 0.999922,  1.
//   };
//   cout << sizeof(binsx)/sizeof(double);


//   TH1D*h1_eff = (TH1D*)f->Get("h_cme_ref");
//   h1_eff->SetXTitle("eff sqrts");
//   h1_eff->Sumw2();
//   TH1D*h2_eff = (TH1D*)f->Get("eff_sqrtsH_before");
//   h2_eff->SetXTitle("eff sqrts");
//   h2_eff->Sumw2();
//   TH1D*h3_eff = (TH1D*)f->Get("eff_sqrtsH_after");
//   h3_eff->SetXTitle("eff sqrts");
//   h3_eff->Sumw2();
//   TH1D*h1_eff_r = h1_eff->Rebin(200,"h_cme_ref_r",binsx);
//   h1_eff->SetLineColor(kRed);
//   h1_eff->Scale(1/h1_eff_r->Integral());
//   h1_eff->GetXaxis()->SetRangeUser(0.7,1.1);
//   h1_eff->SetMinimum(3e-4);
//   TH1D*h2_eff_r = h2_eff->Rebin(200,"eff_before_r",binsx);
//   h2_eff->SetMinimum(3e-4);
//   h2_eff->Scale(1/h2_eff_r->Integral());
//   h2_eff->SetLineColor(kBlue);
//   TH1D*h3_eff_r = h3_eff->Rebin(200,"eff_after_r",binsx);
//   h3_eff->SetLineColor(kGreen+4);
//   h3_eff->SetMinimum(3e-4);
//   h3_eff->Scale(1/h3_eff_r->Integral());



//   TCanvas *c5 = new TCanvas("c5","c5");
//   c5->SetLogy();
//   h1_eff->Draw();
//   h2_eff->Draw("same");
//   h3_eff->Draw("same");
//   l->Draw();
//   c5->Print("final_res.pdf");

  return 0;

}

int main (int argc, char** argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  //gStyle->SetOptTitle(kTRUE);
  TString filename("");
  int nRebins(1);
  if(argc < 3) {
    std::cerr << "No filename given... abort\n"
	      << "LumiRatio final.root nRebins"
	      << std::endl;
    return 1;
  } else {
    filename = TString(argv[1]);
    nRebins = atoi(argv[2]);
  }
  TH1_plots(filename, nRebins);
  return 0;
}
