#ifndef DrawSpectra_hh
#define DrawSpectra_hh 1

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TTree.h>

#include <iostream>
#include <cmath>
#include <cstdlib>


typedef std::vector<TH1D> HistoVector;

//inline double getSigma() { return 3e-4; }
inline double getSigma() { return 0.0; }

TH1D DrawLumiSpectrum(TString name, TString fileName, double smin, double smax, int entries = 1000000, bool isGP=true, bool correlated=true, int nBins=1000, double factor = 1.0);
TH2D DrawLumi2D(TString name, TString fileName, double smin, double smax, int entries = 1000000, bool isGP=true, bool correlated=true);
TH1D DrawBDS(TString name, TString fileName, TString treeName="eleTree", double beamEnergy=1500.0, double smin=0.99, double smax=0.99 , int entries=-1, bool isGP=false, bool scale=true, int bins=1000);

TH1D DrawEnergySpread(TString name, TString fileName, double smin, double smax, int entries = 1000000, bool isGP=true);

TH1D DrawEnergy(TString name, TString fileName, int entries = 1000000, bool isGP=true, double minEfrac=0.995, int nBins=1000);


TH1D DrawLumiSpectrum(TString name, TString fileName, double smin, double smax, int entries, bool isGP, bool correlated, int nBins, double factor){

  TH1D histo(name,name, nBins, smin, smax);
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  TTree *tree = (TTree*)file->Get("MyTree");

  double E1, E2;
  int skipNEvents=10000;
  std::vector<double> E2temps(skipNEvents, 0.0);
  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;

  std::cout << "Running over file: " << fileName << " .... " << entries << std::endl;

  for (int i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);
    E2temps[i%skipNEvents]=E2;
    if(not correlated) E2=E2temps[(i+1)%skipNEvents];//from last time around, lose first events due to 0.0

    if(isGP) {
      E1 /= 1500.0;
      E2 /= 1500.0;
    }

    // E1 += gRandom->Gaus(0.0, getSigma() );
    // E2 += gRandom->Gaus(0.0, getSigma() );


    histo.Fill(sqrt(E1*E2)*factor);


  }
  std::cout << std::endl;
  file->Close();

  histo.Scale(1./histo.Integral("width") );

  return histo;

}//DrawLumiSpectrum



TH2D DrawLumi2D(TString name, TString fileName, double smin, double smax, int entries, bool isGP, bool correlated){

  const int bins = 80;
  TH2D histo(name,name, bins, smin, smax, bins, smin, smax);
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  TTree *tree = (TTree*)file->Get("MyTree");

  double E1, E2, E2temp(0.0), E2temp2(0.0);
  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;

  std::cout << "Running over file: " << fileName << " .... " << entries << std::endl;

  for (int i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif

    tree->GetEntry(i);

    E2temp2=E2;
    if(not correlated) E2=E2temp;//from last time around, lose first event due to 0.0


    if(isGP) {
      E1 /= 1500.0;
      E2 /= 1500.0;
    }

    // E1 += gRandom->Gaus(0.0, getSigma() );
    // E2 += gRandom->Gaus(0.0, getSigma() );

    histo.Fill(E1,E2);
    E2temp = E2temp2;

  }//For all entries
  std::cout << std::endl;
  file->Close();

  histo.Scale(1./histo.Integral("width") );

  return histo;

}//DrawLumiSpectrum


TH1D DrawEnergySpread(TString name, TString fileName, double, double , int entries, bool isGP){
  std::cout << "Running over file: " << fileName << " .... " << entries << std::endl;

  //TH1D histo(name, name, 1000,  -0.0046*1.1, 0.0054*1.1);
  TH1D histo(name, name, 800,  -0.0046*1.1, 0.0054*1.1);
  //TH1D histo(name, name, gBins,  -0.0046*1.1, 0.0054*1.1);
  //  TH1D histo(name, name, 1000, smin, smax);
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  if(not file) {
    std::cerr << "File Not Found: " << fileName  << std::endl;
    std::exit(1);
  }
  TTree *tree = (TTree*)file->Get("MyTree");
  double E1, E2;
  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;
  for (int i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);

    if(isGP) {
      E1 /= 1500.0;
      E2 /= 1500.0;
    }

    // E1 += gRandom->Gaus(0.0, getSigma() );
    // E2 += gRandom->Gaus(0.0, getSigma() );

    //only take particles, that are in the peak
    if(E2 > 0.995) histo.Fill(E1-1.0);
    if(E1 > 0.995) histo.Fill(E2-1.0);

    // histo.Fill(E1-1.0);
    // histo.Fill(E2-1.0);

  }
  std::cout << std::endl;
  file->Close();

  histo.Scale(1./histo.Integral("width") );

  return histo;

}//DrawEnergySpread

TH1D DrawBDS(TString name, TString fileName, TString treeName, double beamEnergy, double smin, double smax, int entries, bool isGP, bool scale, int bins){

  TH1D histo(name, name, bins,  smin, smax);
  histo.SetXTitle("x=#DeltaE/E_{Beam}");
  histo.SetYTitle("dN/dx");
  //  TH1D histo(name, name, 200,  -0.0046*1.1, 0.0054*1.1);
  //  TH1D histo(name, name, gBins,  -0.0046*1.1, 0.0054*1.1);
  //  TH1D histo(name, name, 1000, smin, smax);
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  TTree *tree = (TTree*)file->Get(treeName);
  if(not tree) {
    std::cerr << "Tree " << treeName << " not found in file " << fileName  << std::endl;
    exit(1);
  }

  double E1;
  if( not (tree->SetBranchAddress("E", &E1) == 0)) {
    std::cout << "Using Branch E1 instead"  << std::endl;
    tree->SetBranchAddress("E1", &E1);
  }
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;

  std::cout << "Running over file: " << fileName  << " .... " << entries << std::endl;

  for (int i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);
    if(isGP) {
      E1 /= beamEnergy;
    }
    //    E1 += gRandom->Gaus(0.0, getSigma() );
    histo.Fill(E1-1.0);
  }
  std::cout << std::endl;
  file->Close();
  if(scale) histo.Scale(1./histo.Integral("width") );

  return histo;

}//DrawEnergySpread


/// entries is the number of entries for each of the histograms!
HistoVector DrawSeparatedEnergySpread(TString name, TString fileName, double smin, double smax, int entries, bool isGP) {

  int nBins(200);

  HistoVector histograms;
  histograms.push_back( TH1D(name+"1", "Energy spread in #it{Peak}", nBins,  smin, smax) );
  histograms.push_back( TH1D(name+"2", "Energy spread in #it{Arms}", nBins,  smin, smax) );

  TH1D& histo1 = histograms[0];
  TH1D& histo2 = histograms[1];

  histo1.Sumw2();
  histo2.Sumw2();

  histo1.SetXTitle("x=#DeltaE/E_{Beam}");
  histo2.SetXTitle("x=#DeltaE/E_{Beam}");

  histo1.SetYTitle("dN/dx");
  histo2.SetYTitle("dN/dx");


  TFile *file = TFile::Open(fileName);
  if(not file) {
    std::cerr << "File Not Found: " << fileName  << std::endl;
    exit(1);
  }
  TTree *tree = (TTree*)file->Get("MyTree");
  double E1, E2;
  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);
  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;

  std::cout << "Running over file: " << fileName << " .... " << entries << std::endl;
  int i = -1;
  //  for (int i =0 ; i < entries; ++i) {
  do {
    ++i;
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);

    if(isGP) {
      E1 /= 1500.0;
      E2 /= 1500.0;
    }

    if(E2 > 0.995) {
      if ( histo1.GetEntries() < entries && ( (E1-1.0) > histo1.GetXaxis()->GetXmin() ))  histo1.Fill(E1-1.0);
    } else {
      if ( histo2.GetEntries() < entries && ( (E1-1.0) > histo2.GetXaxis()->GetXmin() ) ) histo2.Fill(E1-1.0);
    }

    if(E1 > 0.995) {
      if ( histo1.GetEntries() < entries && ( (E2-1.0) > histo1.GetXaxis()->GetXmin() ) ) histo1.Fill(E2-1.0);
    } else {
      if ( histo2.GetEntries() < entries && ( (E2-1.0) > histo2.GetXaxis()->GetXmin() ) ) histo2.Fill(E2-1.0);
    }

  } while ( (histo1.GetEntries() < entries || histo2.GetEntries() < entries ) && i < tree->GetEntries() );
  std::cout << std::endl;
  file->Close();

  //  double content = histo1.Integral("width")+histo2.Integral("width");

  histo1.Scale( 1. / histo1.Integral("width") );
  histo2.Scale( 1. / histo2.Integral("width") );


  return histograms;

}//DrawSeparatedEnergySpread


TH1D DrawEnergy(TString name, TString fileName, int entries, bool isGP, double minEfrac, int nBins){


  TH1D histo(name, name, nBins,  minEfrac, 1.1);
  histo.Sumw2();
  TFile *file = TFile::Open(fileName);
  if(not file) {
    std::cerr << "File Not Found: " << fileName  << std::endl;
    exit(1);
  }
  TTree *tree = (TTree*)file->Get("MyTree");
  double E1, E2;
  tree->SetBranchAddress("E1", &E1);
  tree->SetBranchAddress("E2", &E2);

  entries = ( tree->GetEntries() < entries) ? tree->GetEntries() : entries;
  std::cout << "Running over file: " << fileName << " .... " << entries << std::endl;

  for (int i =0 ; i < entries; ++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, entries);
#endif
    tree->GetEntry(i);

    if(isGP) {
      E1 /= 1500.0;
      E2 /= 1500.0;
    }

    // E1 += gRandom->Gaus(0.0, getSigma() );
    // E2 += gRandom->Gaus(0.0, getSigma() );

    //only take particles, that are in the peak
    if(E1 > minEfrac) histo.Fill(E1);
    if(E2 > minEfrac) histo.Fill(E2);

  }
  std::cout << std::endl;
  file->Close();

  histo.Scale(1./histo.Integral("width") );

  return histo;

}//DrawEnergy



#endif // DrawSpectra_hh
