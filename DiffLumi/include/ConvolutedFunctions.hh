#ifndef ConvolutedFunctions_hh
#define ConvolutedFunctions_hh 1

#include "BetaFunction.h"
#include "BetaFunctionBS.h"

#include "Utilities.h"

/***
 * What do we want to do with this? Easily set up convoluted functions to fit histograms, in different programs
 */


const double gBetalimit(0.995);
double gLowerLimit(0.0);
double gUpperLimit(0.995);

double bf1(double*x, double *p) {
  const double norm = 1./(TMath::Beta(1.+p[0], 1.+p[1])*TMath::BetaIncomplete(gBetalimit, 1.+p[0], 1.+p[1]));
  //const double norm = 1./(TMath::Beta(1.+p[0], 1.+p[1]));
  //const double norm(1.0);
  const double res (norm * pow(x[0], p[0]) * pow ( 1-x[0], p[1] ));
  //  std::cout << x[0] << "  " << norm << " " << p[0] << "  " << p[1]  << "  " << res << std::endl;
  return p[2] * res;
}

// 6 parameters
double bf2(double *x, double *p) {
  //const double propB = 1-p[4];
  //  const double propB = p[5];
  //  return p[2] * bf1(x, p) + p[5] * bf1(x, p+3);
  p[5] = 1.0 - p[2];
  return bf1(x, p) + bf1(x, p+3);
}

// 9 parameters
double bf3(double *x, double *p) {
  //const double propC = 1-p[6]-p[7];
  //const double propC = p[8];
  //return p[6] * bf1(x, p) + p[7] * bf1(x, p+3) + propC * bf1(x,p+6);
  p[8] = 1.0 - p[2] - p[5];
  return bf1(x, p) + bf1(x, p+3) + bf1(x,p+6);
}


// 12 parameters
double bf4(double *x, double *p) {
  //const double propC = 1-p[6]-p[7];
  //const double propC = p[8];
  //return p[6] * bf1(x, p) + p[7] * bf1(x, p+3) + propC * bf1(x,p+6);
  p[11] = 1.0 - p[2] - p[5] - p[8];
  return bf1(x, p) + bf1(x, p+3) + bf1(x,p+6)+ bf1(x,p+9);
}


namespace ConvolutedFunctions {

  const double* getParameters() {
    static constexpr double bsbfa = -0.45, bsbfb    = -0.351617;
    static constexpr double bsLow = -0.00465, bsHigh =  0.00545;
    static constexpr double parameter[] = { bsbfa, bsbfb,
					bsLow, bsHigh,
					0.00013};
    return parameter;
  }

  const double* getParameters2() {
    static const double* parameter(getParameters());
    static const double bsbfa = parameter[0], bsbfb  = parameter[1];
    static const double bsLow = parameter[2], bsHigh = parameter[3];
    static const double parameter2[] = { bsbfa, bsbfb,
					 bsLow, bsHigh,
					 bsbfa, 1.0,
					 bsLow, bsHigh,
					 parameter[4], //sigma
					 0.5};//ratio
    return parameter2;
  }


  BetaFunctionBS* getBeamSpread() {
    const double* parameter = getParameters();
    static BetaFunctionBS * beamspread = new BetaFunctionBS(parameter[0],parameter[1],parameter[2],parameter[3]);
    return beamspread;
  }

  //we need this one too, because of the static nature!
  BetaFunctionBS* getBeamSpread2() {
    const double* parameter = getParameters();
    static BetaFunctionBS *beamspread = new BetaFunctionBS(parameter[0],parameter[1],parameter[2],parameter[3]);
    return beamspread;
  }

  BetaFunction* getBetaFunction(){
    const double* parameter = getParameters();
    static BetaFunction *betafunction = new BetaFunction(parameter[0],parameter[1]);
    return betafunction;
  }

  BetaFunction* getBeamstrahlung(){
    const double* parameter = getParameters();
    static BetaFunction *betafunction = new BetaFunction(parameter[0],parameter[1]);
    return betafunction;
  }


  double ConvBetaGauss(double *tau, double *p) {
    return (*(getBeamSpread()))(tau[0])*TMath::Gaus(p[0]-tau[0],0.0,p[1],kTRUE);
  }

  double ConvBetaGauss2(double *tau, double *p) {
    return (*(getBeamSpread2()))(tau[0])*TMath::Gaus(p[0]-tau[0],0.0,p[1],kTRUE);
  }

  double ConvBeamGauss(double *tau, double *p) {
    return (*(getBeamstrahlung()))(tau[0])*TMath::Gaus(p[0]-tau[0],0.0,p[1],kTRUE);
  }


  //Do the convolution between the gauss function and the beamspread function...
  double energySpread(double *x, double *p) {
    getBeamSpread()->ChangeParameters(p);
    static TF1 conv2("conv2",ConvBetaGauss,-0.01,0.01, 2);
    conv2.SetParameters(x[0],p[4]);
    return Utility::Integrate(&conv2, p[2], p[3], 0.00004);
  }

  double energySpread2(double *x, double *p) {
    getBeamSpread()->ChangeParameters(p);
    getBeamSpread2()->ChangeParameters(&(p[4]));
    static TF1 conv1("conv1",ConvBetaGauss,-0.01,0.01, 2);
    static TF1 conv2("conv2",ConvBetaGauss2,-0.01,0.01, 2);
    const double& sigma(p[8]);
    const double& ratio(p[9]);
    conv1.SetParameters(x[0],sigma);
    conv2.SetParameters(x[0],sigma);
    return ratio*Utility::Integrate(&conv1, p[2], p[3], 0.00004)+(1-ratio)*Utility::Integrate(&conv2, p[6], p[7], 0.00004);
  }


  double BeamSpread(double *x, double *p) {
    getBeamSpread()->ChangeParameters(p);
    return (*(getBeamSpread()))(x[0]);
  }

  ///Test the normalisation for the betaBeamspreadFunction
  double BeamSpread2(double* x, double* p) {
    const double t( ( x[0] - p[2]) / ( p[3] - p[2] ) );
    if ( 0 <= t && t <= 1 ) {
      const double norm = 1./(TMath::Beta(1.+p[0], 1.+p[1]))/( p[3] - p[2] );
      return norm * pow( t, p[0] ) * pow( 1. - t, p[1]);
    }
    return 0;
  }


  double bfFunction(double *x, double *p) {
    getBetaFunction()->ChangeParameters(p);
    return (*(getBetaFunction()))(x[0]);
  }

  TF1 getESFunction( const double *parameter) {

    TF1 esFunction("Fit",energySpread,-0.01,0.01,5);
    esFunction.SetParameters(parameter);
    esFunction.SetParName(0, "a1");
    esFunction.SetParName(1, "a2");
    esFunction.SetParName(2, "xmin");
    esFunction.SetParName(3, "xmax");
    esFunction.SetParName(4, "sigma");
    esFunction.SetNpx(1000);

    // esFunction.SetParLimits(0, -0.6, -0.01);
    // esFunction.SetParLimits(1, -0.6, -0.01);
    // esFunction.SetParLimits(2, -0.005,   -0.004);
    // esFunction.SetParLimits(3,  0.005,    0.006);
    // esFunction.SetParLimits(4, 0.00005,   0.00017);

    // esFunction.SetParLimits(0, -0.6, -0.01);
    // esFunction.SetParLimits(1, -0.6, -0.01);
    // esFunction.SetParLimits(2, -0.009,   -0.002);
    // esFunction.SetParLimits(3,  0.002,    0.009);
    // esFunction.SetParLimits(4, 0.00005,   0.001);

    esFunction.SetParLimits(0, -0.7, 1.01);
    esFunction.SetParLimits(1, -0.7, 1.01);
    esFunction.SetParLimits(2, -0.009,   -0.002);
    esFunction.SetParLimits(3,  0.002,    0.009);
    esFunction.SetParLimits(4, 0.00005,   0.0012);


    //  esFunction.SetParLimits(4, 0.000011,   0.001);

    return esFunction;

  }


  TF1 getES2Function(const double *parameter) {

    TF1 es2Function("Fit2",energySpread2,-0.01,0.01,10);
    es2Function.SetLineColor(kGreen+2);
    es2Function.SetNpx(1000);
    es2Function.SetParameters(parameter);
    es2Function.SetParLimits(0, -0.8, -0.2);
    es2Function.SetParLimits(1, -0.8, -0.2);
    es2Function.SetParLimits(2, -0.005,   -0.004);
    es2Function.SetParLimits(3,  0.005,    0.006);
    //  es2Function.SetParLimits(4, -0.6, 1.0);
    es2Function.SetParLimits(4, -0.8, -0.01);
    es2Function.SetParLimits(5,  0.0, 10.0);
    es2Function.SetParLimits(6, -0.005,   -0.004);
    es2Function.SetParLimits(7,  0.005,    0.006);
    es2Function.SetParLimits(8,  0.000011,   0.00017);
    es2Function.SetParLimits(9,  0.0,   1.0);

    es2Function.SetParName(0, "a1");
    es2Function.SetParName(1, "a2");
    es2Function.SetParName(2, "xmin");
    es2Function.SetParName(3, "xmax");
    es2Function.SetParName(4, "b1");
    es2Function.SetParName(5, "b2");
    es2Function.SetParName(6, "xmin2");
    es2Function.SetParName(7, "xmax2");
    es2Function.SetParName(8, "sigma");
    es2Function.SetParName(9, "p");


    es2Function.SetLineColor(kBlue);

    // esFunction.FixParameter(0,bsbfa);
    // esFunction.FixParameter(1,bsbfb);
    // esFunction.FixParameter(2,bsLow);
    // esFunction.FixParameter(3,bsHigh);
    //  esFunction.FixParameter(4,0.00013);

    // esFunction.ReleaseParameter(2);
    // esFunction.ReleaseParameter(3);

    // esFunction.SetParLimits(2, -0.005,   -0.004);
    // esFunction.SetParLimits(3,  0.005,    0.006);

    // esFunction.FixParameter(0, esFunction.GetParameter(0));
    // esFunction.FixParameter(1, esFunction.GetParameter(1));
    // // esFunction.FixParameter(2,bsLow);
    // // esFunction.FixParameter(3,bsHigh);
    // esFunction.FixParameter(4, esFunction.GetParameter(4));

    return es2Function;
  }//getES2Function


  // Thanks to Lorenzo Moneta http://root.cern.ch/phpBB3/viewtopic.php?f=3&t=11317

  class Chebyshev {
  public:
    Chebyshev(int n, double xmin, double xmax) :
      fA(xmin), fB(xmax), range(fB-fA), twoInvrange(2.0/range), offset(twoInvrange*fA + 1), order(n),
      fT(std::vector<double>(n))  {}
    ~Chebyshev() { }
    double operator() (const double * xx, const double *p) {
      double x(xx[0]);
      return (*this)(x, p);
    }

    double operator() (const double xx, const double *p) {
      //double x = (xx[0] - fA - fB)/(fB-fA);
      double x =  twoInvrange * xx - offset;
      if(x > 1.0) return 0.0;
      //std::cout << std::setw(10) << x;
      if (order == 1) return p[0];
      if (order == 2) return p[0] + x*p[1];
      // build the polynomials
      fT[0] = 1;
      fT[1] = x;
      for (int i = 1; i < order-1; ++i) {
	fT[i+1] =  2 *x * fT[i] - fT[i-1];
      }
      double sum = p[0]*fT[0];
      //      for (int i = 1; i <= order; ++i) { //why start at i=1?, why until <= order?
      for (int i = 1; i < order; ++i) { //why start at i=1? Because sum has it
					//already // why <= order? is not the
					//same in order==2, so go until < order, so for order n we need n parameters
	sum += p[i] * fT[i];
      }
      return sum;
    }

  private:
    double fA;
    double fB;
    double range;
    double twoInvrange;
    double offset;
    int order;
    std::vector<double> fT; // polynomial
    //std::vector<double> fC; // coefficients
  };//Chebyshev


}//namespace




inline double GSLIntegrate(double (*function)(double, void*),const double* parameter,
				   double xmin, double xmax, double) {
  static ROOT::Math::GSLIntegrator integrator(ROOT::Math::IntegrationOneDim::kADAPTIVE,
					      // try
					      // ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR
					      // if something goes wrong next
					      // time...
					      //1e-7, //absTolerance
					      // 1e-5, //relativeTolerance
					      // 1000);//numberOfIterations
					      1e-8, //absTolerance
					      1e-6, //relativeTolerance
					      3000);//numberOfIterations
  integrator.SetFunction(function, (void*) parameter);
  //  integrator.SetRelTolerance(precision);
  return integrator.Integral(xmin, xmax);
}


class BeamStrahlungTripple{

public:
  BeamStrahlungTripple(double *parameter):
    beta1( BetaFunction(parameter[1], parameter[2]) ),
    beta2( BetaFunction(parameter[4], parameter[5]) ),
    beta3( BetaFunction(parameter[7], parameter[8]) ),
    beta4( BetaFunction(parameter[10],parameter[11]) ),
    _p(parameter)
  {
    this->beta1.SetBetaLimit(this->betalimit);
    this->beta2.SetBetaLimit(this->betalimit);
    this->beta3.SetBetaLimit(this->betalimit);
    this->beta4.SetBetaLimit(this->betalimit);
  }

  double operator() (double x) const {
    if ( this->betalimit <= x ) 
      return _p[12] * this->delta(x);
    else 
      return _p[0] * this->beta1(x)
	+ _p[3] * this->beta2(x)
	+ _p[6] * this->beta3(x)
	+ _p[9] * this->beta4(x);
  }

  void SetParameters(double *parameter){
    // for (int i = 0; i < _nPar;++i) {
    //   _p[i] = parameter[i];
    // }
    _p[12] = 1.0 - _p[0] - _p[3] -_p[6] - _p[9];
    this->beta1.ChangeParameters(parameter+1);
    this->beta2.ChangeParameters(parameter+4);
    this->beta3.ChangeParameters(parameter+7);
    this->beta4.ChangeParameters(parameter+10);
    _p = parameter;
     _p = parameter;
  }
  //  double GetSigma() const { return _p[9]; }

  double delta(double x) const {
    //  if( 0.9999 < x ) {    return 1.0;  } else {    return 0.0;  }
    return ( this->betalimit <= x && x <= 2.0 - betalimit ) ? 0.5/(1.0-betalimit) : 0.0 ;
  }


  // void Info() const {
  //   std::cout << "Beta1, betalimit " << beta1.GetBetaLimit()  << std::endl;
  //   std::cout << "Beta1, norm      " << beta1.GetNormalization()  << std::endl;
  //   std::cout << "Beta1, 1/norm      " << 1/beta1.GetNormalization()  << std::endl;
  //   std::cout << "Beta1, p1      " << beta1.GetP1()  << std::endl;
  //   std::cout << "Beta1, p2      " << beta1.GetP2()  << std::endl;
  // }



private:
  BetaFunction beta1, beta2, beta3, beta4;
  double *_p;
  //static const double betalimit=0.9999;
  static constexpr double betalimit=0.9999;
  //static const double betalimit=1-1.0/1000;//0.999995;
};

BeamStrahlungTripple *gBST;



double ConvBeamstrahlungBetaGauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  const double sigma = ((double*)p)[1];
  //  if(x <= gLowerLimit || gUpperLimit <= x) return 0.0;
  const double xMtau (x-tau);
  //if( xMtau <= 0.0 || 1.0 <= xMtau ) return 0.0;
  if( xMtau <= 0.0 ) return 0.0;
  //  if( x-tau <= 0.0) return 0.0;
  //  if( x-tau >= 0.99995 ) return 0.0;
  return TMath::Gaus(tau,0.0,sigma,kTRUE) * (*gBST)(xMtau) ;
}




//For use in TF1
double BeamstrahlungGauss(double *x, double *p) {

  static bool create(true);
  if(create) {
    gBST = new BeamStrahlungTripple( p+1 );
    create = false;
  } else {
    gBST->SetParameters ( p+1 );
  }

  const double para1Beta[] = {x[0], p[0]};
  const double fiveSigma = 5.0 * p[0];
  const double value( GSLIntegrate(&ConvBeamstrahlungBetaGauss, para1Beta, -fiveSigma, fiveSigma, 0.0));
  return value;
  //  return (*gBST)(x[0]);
  
}

ConvolutedFunctions::Chebyshev *gCheb = NULL;


double ConvChebGauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  const double sigma = ((double*)p)[1];
  //  if(x <= gLowerLimit || gUpperLimit <= x) return 0.0;
  const double xMtau (x-tau);
  //if( xMtau <= 0.0 || 1.0 <= xMtau ) return 0.0;
  if( xMtau <= 0.0 ) return 0.0;
  //  if( x-tau <= 0.0) return 0.0;
  //  if( x-tau >= 0.99995 ) return 0.0;
  return TMath::Gaus(tau,0.0,sigma,kTRUE) * (*gCheb)(xMtau, ((double*)p)+2) ;
}


//For use in TF1
double ChebyshevGauss(double *x, double *p) {

  const double sigma = p[1];
  //replace first piece with x value, rewrite it again later


  //  return (*gCheb)(x, p+2);

  p[0] = x[0];
  const double fiveSigma = 5.0 * sigma;
  const double value( GSLIntegrate(&ConvChebGauss, p, -fiveSigma, fiveSigma, 0.0));
  p[0] = 0.0;
  return value;

}


 #endif // ConvolutedFunctions_hh
