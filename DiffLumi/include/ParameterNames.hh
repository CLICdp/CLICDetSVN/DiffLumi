#ifndef ParameterNames_hh
#define ParameterNames_hh 1

#include <string>


std::string getLatexMacro(int i) {

std::string gParameterNames[] = {"\\pPeak",
				 "\\pArmA",
				 "\\pArmB",
				 "\\aarmAA",
				 "\\aarmAB",
				 "\\bPeakAA" ,//BeamSpread Peak1 a1
				 "\\bPeakAB" ,//BeamSpread Peak1 a2
				 "\\BSRangeALo",//beamSpreadrange xmin
				 "\\BSRangeAHi",//beamSpreadrange xmax
				 "\\aarmBA",
				 "\\aarmBB",
				 "\\bPeakBA" ,//BeamSpread Peak2 a1
				 "\\bPeakBB" ,//BeamSpread Peak2 a2
				 "\\BSRangeBLo",
				 "\\BSRangeBHi",
				 "\\abodyAA",
				 "\\abodyAB",
				 "\\abodyBA",
				 "\\abodyBB",
				 "\\barmAA",
				 "\\barmAB",
				 "\\BSRALArm",
				 "\\BSRAHArm",
				 "\\barmBA",
				 "\\barmBB",
				 "\\BSRBLArm",
				 "\\BSRBHArm",
				 "\\BSB1aBody",
				 "\\BSB1bBody",
				 "\\BSR1LBody",
				 "\\BSR1HBody",
				 "\\BSBBaBody",
				 "\\BSBBbBody",
				 "\\BSRBLBody",
				 "\\BSRBHBody"};
 return gParameterNames[i];
}


std::string getLatexName(int i) {
  std::string gParameterNames[] = {"p_{Peak}",
				 "p_{Arm1}",
				 "p_{Arm2}",
				 "#eta_{Arm1}^{a}",
				 "#eta_{Arm1}^{b}",
				 "#omega_{Peak1}^{a}" ,//BeamSpread Peak1 a1
				 "#omega_{Peak1}^{b}" ,//BeamSpread Peak1 a2
				 // "\\BSRangeALo",//beamSpreadrange xmin
				 // "\\BSRangeAHi",//beamSpreadrange xmax
				 "#eta_{Arm2}^{a}",
				 "#eta_{Arm2}^{b}",
				 "#omega_{Peak2}^{a}" ,//BeamSpread Peak2 a1
				 "#omega_{Peak2}^{b}" ,//BeamSpread Peak2 a2
				 // "B_{SRangeBLo}",
				 // "B_{SRangeBHi}",
				 "#eta_{Body1}^{a}",
				 "#eta_{Body1}^{b}",
				 "#eta_{Body2}^{a}",
				 "#eta_{Body2}^{b}",
				 "#omega_{Arm1}^{a}",
				 "#omega_{Arm1}^{b}",
				 // "B_{SRALArm}",
				 // "B_{SRAHArm}",
				 "#omega_{Arm2}^{a}",
				 "#omega_{Arm2}^{b}",
                                 "#chi^{2}/ndf",
				 "B_{SRBLArm}",
				 "B_{SRBHArm}",
				 "B_{SB1aBody}",
				 "B_{SB1bBody}",
				 "B_{SR1LBody}",
				 "B_{SR1HBody}",
				 "B_{SBBaBody}",
				 "B_{SBBbBody}",
				 "B_{SRBLBody}",
				 "B_{SRBHBody}"};

  if(i < 35) {
    return gParameterNames[i];
  }
  return "";
}



#endif // ParameterNames_hh
