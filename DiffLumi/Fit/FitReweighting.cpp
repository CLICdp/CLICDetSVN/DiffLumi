#include "TupleGenerator.hh"
#include "TupleBHWide.hh"
#include "ReweightingGenerator.hh"
#include "ReweightingBHWide.hh"
#include "ModelOverlap.hh"
#include "ModelSeparate.hh"

#include "CutGenerator.hh"
#include "CutBHWide.hh"
#include "CutBHWideSmeared.hh"

#include "Utilities.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TFile.h"
#include <TMatrixTSym.h>

#include <Math/Functor.h>
#include <Minuit2/Minuit2Minimizer.h>
#include <tclap/CmdLine.h>
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>
#include <vector>

#include <sys/stat.h>

int main(int argc, char *argv[]){

  //Account for the time used
  TStopwatch timer;

  std::string file2("");
  std::string file1("");
  int mc_stat_to_use(3000000);
  int data_stat_to_use(3000000);
  double energycut(200./3000.); // in fractions of 3 TeV
  double energy(1500.);
  int binsx=30,binsy=30,binsz=1;
  std::string rootpath(Utility::GetWorkingDirectory()+"/RootFiles");
  std::string logconfpath(Utility::GetWorkingDirectory()+"/Logger");
  std::string modelName("Separate");//or Overlap
  std::string DataType("BHWide");
  bool scan(false);
  bool doMCvsMC(false);
  bool smearing(false);
  bool recreateB(false);
  bool applyXsec(false);
  AbstractCut*mycut=NULL;
  try {  
    TCLAP::CmdLine cmd("Check the defaults.", ' ', "0.0");
    std::vector<std::string> allowedmodels;
    allowedmodels.push_back("Overlap");
    allowedmodels.push_back("Separate");
    TCLAP::ValuesConstraint<std::string> allowedmodelsVals( allowedmodels );
    std::vector<std::string> allowedlevels;
    allowedlevels.push_back("Generator");
    allowedlevels.push_back("GeneratorSymmetricBinning");
    allowedlevels.push_back("BHWide");
    TCLAP::ValuesConstraint<std::string> allowedlevelsVals( allowedlevels );

    TCLAP::SwitchArg xsecArg("","applyXsec","Use the cross section applied files",cmd, false);

    TCLAP::SwitchArg mcvsmcArg("","MCvsMC","Run MC files vs MC files to test reconstruction",cmd, false);

    TCLAP::SwitchArg doScanArg("","do_scan","Do parameter scan",cmd, false);

    TCLAP::SwitchArg recreateBArg("","RecreateBinning",
				  "Recreate binning, even if already available",
				  cmd, false);

    TCLAP::SwitchArg smearingArg("","Smearing","Smearing of the energy, only for BHWide",
				 cmd, false);

    TCLAP::ValueArg<std::string> logpathArg("","logconfigpath","Path to logger config",false,
					    logconfpath,"string",cmd);

    
    TCLAP::ValueArg<std::string> rootpathArg("","rootpath","Path to root files",false,
					     rootpath,"string",cmd);

    TCLAP::ValueArg<int> binzArg("","Binz","Bins in z",false, binsz, "int",cmd);

    TCLAP::ValueArg<int> binyArg("","Biny","Bins in y",false, binsy, "int",cmd);

    TCLAP::ValueArg<int> binxArg("","Binx","Bins in x",false, binsx, "int",cmd);

    TCLAP::ValueArg<double> ecutArg("c","Cut","Cut on the energy",false,energycut,"double",cmd);
    TCLAP::ValueArg<double> eArg("e","Energy","Nominal beam energy",false,energy,"double",cmd);

    TCLAP::ValueArg<std::string> levelArg("","level","Fit level",false,
					  DataType,&allowedlevelsVals);
    cmd.add(levelArg);
    TCLAP::ValueArg<std::string> modelArg("","model","Model to use",false,
					  modelName,&allowedmodelsVals);
    cmd.add(modelArg);
    TCLAP::ValueArg<int> mcstatArg("m","MC","Number of MC events to use",false,
				   mc_stat_to_use,"int",cmd);
    TCLAP::ValueArg<int> datastatArg("d","data","Number of data events to use",false,
				     data_stat_to_use,"int",cmd);
    
    

    TCLAP::ValueArg<std::string> file2Arg("","fileMC","File to use as MC",false, file2, "string", cmd);
    TCLAP::ValueArg<std::string> file1Arg("","fileData","File to use as Data",false, file1, "string", cmd);

    cmd.parse( argc, argv );

    mc_stat_to_use = mcstatArg.getValue();
    data_stat_to_use = datastatArg.getValue();
    energycut = ecutArg.getValue();
    energy = eArg.getValue();
    binsx = binxArg.getValue();
    binsy = binyArg.getValue();
    binsz = binzArg.getValue();
    rootpath = rootpathArg.getValue();
    logconfpath = logpathArg.getValue();
    modelName = modelArg.getValue();
    DataType = levelArg.getValue();
    smearing = smearingArg.getValue();
    recreateB = recreateBArg.getValue();
    scan = doScanArg.getValue();
    applyXsec = xsecArg.getValue();
    doMCvsMC = mcvsmcArg.getValue();

    file1 = file1Arg.getValue();
    file2 = file2Arg.getValue();

  }
  catch (TCLAP::ArgException &e){  // catch any exceptions 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return 1;
  }
  log4cplus::Logger root = log4cplus::Logger::getRoot();
  log4cplus::ConfigureAndWatchThread configureThread(LOG4CPLUS_TEXT( logconfpath+"/log4cplus.properties"), 5 * 1000);

  //const Double_t bsbfa = -5.23204e-01, bsbfb  =  -4.09878e-01;
  //const Double_t bsLow = -4.65737e-03, bsHigh =   5.47594e-03;

  // const Double_t bsbfa = -3.36e-01, bsbfb  =  -2.98e-01;
  // const Double_t bsLow = -4.679e-3, bsHigh =   5.495e-03;

  const Double_t bsbfa = -5.23204e-01, bsbfb  =  -4.09878e-01;//from CreateLumiFileAbstract
  const Double_t bsLow = -4.65737e-03, bsHigh =   5.47594e-03;

  const int n_params = 35;

  Double_t parameter2d[n_params] = { 
    0.15, 0.25, 0.25,		//For the different parts      0  1  2
    2.e0 , -7.e-1 ,		//with delta function          3  4
    bsbfa, bsbfb   ,		// beamspread parameter        5  6
    bsLow   , bsHigh ,		//Range for BeamSpread         7  8
    2.e0 , -7.e-1 ,		//with delta function          9 10
    bsbfa , bsbfb,		//beam spread                 11 12
    bsLow   , bsHigh ,		//Range for BeamSpread        13 14
    1.e-1 ,  -5.e-1 ,		//beta body1                  15 16
    1.e-1 ,  -5.e-1,    	//beta body2                  17 18 
    bsbfa , 0.3,		//beam spread                 19 20
    bsLow   , bsHigh ,		//Range for BeamSpread        21 22
    bsbfa , 0.3,		//beam spread                 23 24
    bsLow   , bsHigh, 		//Range for BeamSpread        25 26
    bsbfa , 0.3,		//beam spread                 27 28
    bsLow   , bsHigh ,		//Range for BeamSpread        29 30
    bsbfa , 0.3,		//beam spread                 31 32
    bsLow   , bsHigh };		//Range for BeamSpread        33 34


  // //Original parameters?!? from CreateLumiFileAbstract
  // Double_t parameter2d[35] = { 
  //   0.25, 0.25, 0.25,		//For the different parts      0  1  2
  //   2.5e0 , -7.5e-1 ,		//with delta function          3  4
  //   bsbfa, bsbfb   ,		// beamspread parameter        5  6
  //   bsLow   , bsHigh ,		//Range for BeamSpread         7  8
  //   2.5e0 , -7.5e-1 ,		//with delta function          9 10
  //   bsbfa , bsbfb,		//beam spread                 11 12
  //   bsLow   , bsHigh ,		//Range for BeamSpread        13 14
  //   1.5e-1 ,  -5.5e-1 ,		//beta body1                  15 16
  //   1.5e-1 ,  -5.5e-1,    	//beta body2                  17 18
  //   bsbfa , 0.35,		//beam spread                 19 20
  //   bsLow   , bsHigh ,		//Range for BeamSpread        21 22
  //   bsbfa , 0.35,		//beam spread                 23 24
  //   bsLow   , bsHigh, 		//Range for BeamSpread        25 26
  //   bsbfa , 0.35,		//beam spread                 27 28
  //   bsLow   , bsHigh ,		//Range for BeamSpread        29 30
  //   bsbfa , 0.35,		//beam spread                 31 32
  //   bsLow   , bsHigh };		//Range for BeamSpread        33 34



  AbstractModel *theModel = AbstractModel::getModel(modelName,parameter2d);

  LOG4CPLUS_INFO(root, "Model is "<<theModel->GetName());
  LOG4CPLUS_INFO(root, "Nominal beam energy is "<<energy<<" GeV");
  LOG4CPLUS_INFO(root, "Fit Level is "<<DataType);
  LOG4CPLUS_INFO(root, "Will use "<<binsx<<"x"<<binsy<<"x"<<binsz<<" bins");
 
  std::string dfile;
  std::string mcfile;
  std::string add_name=Form("%s",DataType.c_str());

  //Use files from the input
  if( file1.length() != 0 && file2.length() != 0 ) {
    dfile = file1.c_str();
    mcfile = file2.c_str();
  } else if (DataType == "BHWide"){
    LOG4CPLUS_INFO(root, "Cut sqrt(e1*e2) < "<<energycut<<" *3TeV events, where e1 and e2 are outgoing electrons");
    //energycut = 0.0;
    if (!applyXsec){
      if (smearing){
	dfile = rootpath+"/BHWide_GP_smeared.root";
	mcfile = rootpath+Form("/BHWide_MC_%s_smeared.root", theModel->GetName().c_str());
	add_name += "_smeared";
      }
      else{
	dfile = rootpath+"/BHWide_GP.root";
	mcfile = rootpath+Form("/BHWide_MC_%s.root", theModel->GetName().c_str());
      }
    } else {
      if (smearing){
	dfile = rootpath+"/BHWide_GP_BHWeighted_Double_slives50_Random_smeared.root";
	mcfile = rootpath+Form("/BHWide_MC_BHWeigthed_%s_smeared.root", theModel->GetName().c_str());
	add_name += "_xsec_smeared";
      }
      else{
	dfile = rootpath+"/BHWide_GP_BHWeighted_Double_slives50_Random.root";
	mcfile = rootpath+Form("/BHWide_MC_BHWeigthed_%s.root", theModel->GetName().c_str());
	add_name += "_xsec";
      }
    } 
  } else if (DataType == "Generator" || DataType == "GeneratorSymmetricBinning" ){
    if (applyXsec){
      //dfile = rootpath+"/LumiFileGP.root";
      dfile = rootpath+"/Lumi_GP_BHWeighted_Double_slives50_Random.root";
      mcfile = rootpath+Form("/Chain/LumiFileMC_%s.root", theModel->GetName().c_str());
      add_name += "_xsec";
    } else {
      dfile = rootpath+"/Lumi_GP_Double_Slices50_Random.root";
      mcfile = rootpath+Form("/LumiFileMC_%s_3.root", theModel->GetName().c_str());
    }
  }

  //Deal with the cuts
  LOG4CPLUS_INFO(root, "Using sqrts > "<< energycut <<" *3TeV events only.");
  if (DataType == "Generator"|| DataType == "GeneratorSymmetricBinning"){
    mycut = AbstractCut::getCut("Generator");
    mycut->setCutVal(energycut*energycut);
  } else {
    if( smearing ) {
      mycut = AbstractCut::getCut("BHWide_smeared");
      mycut->setCutVal(energycut*3000.);
    } else {
      mycut = AbstractCut::getCut("BHWide");
      mycut->setCutVal(energycut*3000.);
    } 
  }// if not generator     



  //If we want to run MC file with model vs itself
  if(doMCvsMC) {
    dfile = mcfile;
  }

  LOG4CPLUS_INFO(root, "Data File is "<< dfile);
  LOG4CPLUS_INFO(root, "Data events "<< data_stat_to_use);

  LOG4CPLUS_INFO(root, "MC  File  is "<< mcfile);
  LOG4CPLUS_INFO(root, "MC  events "<< mc_stat_to_use);

  //now that mc file is defined, try to get the parameters from it
  Utility::ReadParameters(mcfile, parameter2d);

  //just in case: update theModel with new parameters
  theModel->ChangeParameters(parameter2d);

  //  std::string binningfile = rootpath+Form("./Binning_epb_%ix%ix%i_%s.root", 
  std::string binningfile = Form("./Binning_epb_%ix%ix%i_%s.root", 
				 binsx, binsy, binsz, add_name.c_str());

  LOG4CPLUS_INFO(root, "Binning File  is "<< binningfile);

  //ReweightingGenerator rew(theModel,binsx*binsy*binsz);
  ReweightingBHWide rew(theModel,binsx*binsy*binsz);
  LOG4CPLUS_INFO(root, "Create Reweighting Obj");
  //TupleBHWide data(rootpath+"/BHWide_GP_smeared.root",data_stat_to_use);
  AbstractTuple *data = AbstractTuple::getTuple(DataType,dfile,data_stat_to_use);
  data->SetNominalBeamEnergy(energy);
  LOG4CPLUS_INFO(root, "Created Data Tuple");

  //TupleBHWide data(Utility::GetWorkingDirectory()+Form("/RootFiles/BHWide_MC_%s_smeared.root",
  //					       theModel->GetName().c_str()),data_stat_to_use);
  //TupleGenerator data(Utility::GetWorkingDirectory()+"/RootFiles/LumiFileGP.root",data_stat_to_use);
  data->SetCut(mycut);
  data->SetBins(binsx,binsy,binsz);
  LOG4CPLUS_INFO(root, "Creating or Loading Binning");

  struct stat buffer ;
  if ( ! stat( binningfile.c_str(), &buffer )){
    if (!recreateB){
      data->LoadEquiProbability(binningfile);
    }
    else{
      data->CreateEquiProbability(binningfile);
    }
  } else {
    data->CreateEquiProbability(binningfile);
  }

  LOG4CPLUS_INFO(root, "Done with Binning");

  //data->GetBinning()->CreateTGeoFile("tgeoTemp.root");

  data->FillLumiHistogram(Form("control_%s_%s.root",theModel->GetName().c_str(),add_name.c_str()));
  LOG4CPLUS_INFO(root, "Read Data Vector");
  long dataEntries = data->FillVectors(rew.getDataVec());
  if (!dataEntries) {
    delete theModel;
    LOG4CPLUS_ERROR(root, "No entries in data pass the cuts");
    return 1;
  }
  //TupleBHWide MC(rootpath+Form("/BHWide_MC_%s_smeared.root", theModel->GetName().c_str()),  
  //  		 mc_stat_to_use);
  AbstractTuple *MC = AbstractTuple::getTuple(DataType,mcfile,mc_stat_to_use);
  //TupleGenerator MC(Utility::GetWorkingDirectory()+Form("/RootFiles/LumiFileMC_%s_1.root", 
    //							theModel->GetName().c_str() ) ,
    //		    mc_stat_to_use);
  MC->SetNominalBeamEnergy(energy);
  MC->SetCut(mycut);
  MC->SetBinning(data->GetBinning());
  LOG4CPLUS_INFO(root, "Read MC Vector");

  rew.getEvents()->reserve(mc_stat_to_use);

  long mcEntries = MC->FillVectors(rew.getMCVec(),rew.getEvents(),rew.getWeightVec());
  if (!mcEntries) {
    delete theModel;
    LOG4CPLUS_ERROR(root, "No entries in MC pass the cuts");
    return 1;
    
  }


  //LOG4CPLUS_INFO(root, "" << *data.GetBinning());

  rew.setEntryFactor(double(dataEntries)/double(mcEntries));//This has to be determined at some point. 

  rew.FillControlHistograms(Form("control_%s_%s.root",theModel->GetName().c_str(),add_name.c_str()));

  ROOT::Math::Functor FCNFunction(rew, n_params);

  ROOT::Minuit2::Minuit2Minimizer theMinimizer;

  theMinimizer.SetFunction(FCNFunction);

  theMinimizer.SetMaxFunctionCalls(20000);
  theMinimizer.SetMaxIterations(100);
  theMinimizer.SetPrecision(1e-20);
  theMinimizer.SetStrategy(2);
  theMinimizer.SetPrintLevel(3);

  Double_t errorFraction = 10.0;

  Utility::SetVariable(theMinimizer, Utility::kLimited,   0, "Peak",    parameter2d[0], fabs(parameter2d[0]/errorFraction),   0.0,  0.4);
  Utility::SetVariable(theMinimizer, Utility::kLimited,   1, "Arm1",    parameter2d[1], fabs(parameter2d[1]/errorFraction),   0.0,  0.3);
  Utility::SetVariable(theMinimizer, Utility::kLimited,   2, "Arm2",    parameter2d[2], fabs(parameter2d[2]/errorFraction),   0.0,  0.3);
  bool freeEm  = true;
  bool freeEm2 = true;
  bool freeEm3 = (theModel->GetName()=="Overlap") ? true : false;
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,   3, "BFArm1A" ,    parameter2d[3],  fabs(parameter2d[3]/errorFraction),   0.0, 10.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,   4, "BFArm1B" ,    parameter2d[4],  fabs(parameter2d[4]/errorFraction),  -1.0,  0.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,   5, "BSB1a" ,      parameter2d[5],  fabs(parameter2d[5]/errorFraction),  -1.0,  0.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,   6, "BSB1b" ,      parameter2d[6],  fabs(parameter2d[6]/errorFraction),  -1.0,  0.0);
  Utility::SetVariable(theMinimizer, Utility::kFixed,                                7, "BSRange1Lo",  parameter2d[7],  fabs(parameter2d[7]/errorFraction),   0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                                8, "BSRange1Hi",  parameter2d[8],  fabs(parameter2d[8]/errorFraction),   0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, freeEm2 ? Utility::kLimited : Utility::kFixed,  9, "BFArm2A" ,    parameter2d[9],  fabs(parameter2d[9]/errorFraction),   0.0, 10.0);
  Utility::SetVariable(theMinimizer, freeEm2 ? Utility::kLimited : Utility::kFixed, 10, "BFArm2B" ,    parameter2d[10], fabs(parameter2d[10]/errorFraction), -1.0,  0.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,  11, "BSB2a" ,      parameter2d[11], fabs(parameter2d[11]/errorFraction), -1.0,  0.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,  12, "BSB2b" ,      parameter2d[12], fabs(parameter2d[12]/errorFraction), -1.0,  0.0);
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               13, "BSRange2Lo",  parameter2d[13], fabs(parameter2d[13]/errorFraction),  0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               14, "BSRange2Hi",  parameter2d[14], fabs(parameter2d[14]/errorFraction),  0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, freeEm3 ? Utility::kLimited : Utility::kFixed, 15, "BFBod1A" ,    parameter2d[15], fabs(parameter2d[15]/errorFraction),  0.0, 10.0);//
  Utility::SetVariable(theMinimizer, freeEm3 ? Utility::kLimited : Utility::kFixed, 16, "BFBod1B" ,    parameter2d[16], fabs(parameter2d[16]/errorFraction), -1.0,  0.0);//
  Utility::SetVariable(theMinimizer, freeEm3 ? Utility::kLimited : Utility::kFixed, 17, "BFBod2A" ,    parameter2d[17], fabs(parameter2d[17]/errorFraction),  0.0, 10.0);//
  Utility::SetVariable(theMinimizer, freeEm3 ? Utility::kLimited : Utility::kFixed, 18, "BFBod2B" ,    parameter2d[18], fabs(parameter2d[18]/errorFraction), -1.0,  0.0);//
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,  19, "BSB1a_Arm",   parameter2d[19],  0.1,                               -1.0, 10.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,  20, "BSB1b_Arm",   parameter2d[20],  0.1,                               -1.0, 10.0);
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               21, "BSR1L_Arm",   parameter2d[21],  0.0,                                0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               22, "BSR1H_Arm",   parameter2d[22],  0.0,                                0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,  23, "BSB2a_Arm",   parameter2d[23],  0.1,                               -1.0, 10.0);
  Utility::SetVariable(theMinimizer, freeEm ? Utility::kLimited : Utility::kFixed,  24, "BSB2b_Arm",   parameter2d[24],  0.1,                               -1.0, 10.0);
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               25, "BSR2L_Arm",   parameter2d[25],  0.0,                                0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               26, "BSR2H_Arm",   parameter2d[26],  0.0,                                0.0,  0.0);//f
  //BeamSpread Body 1		     						    		       		        
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               27, "BSB1a_Body",  parameter2d[27],  0.01,                              -1.0, 10.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               28, "BSB1b_Body",  parameter2d[28],  0.01,                              -1.0, 10.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               29, "BSR1L_Body",  parameter2d[29],  0.0,                                0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               30, "BSR1H_Body",  parameter2d[30],  0.0,                                0.0,  0.0);//f
  //BeamSpread Body 2		     						    		       		        
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               31, "BSB2a_Body",  parameter2d[31],  0.01,                              -1.0, 10.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               32, "BSB2b_Body",  parameter2d[32],  0.01,                              -1.0, 10.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               33, "BSR2L_Body",  parameter2d[33],  0.0,                                0.0,  0.0);//f
  Utility::SetVariable(theMinimizer, Utility::kFixed,                               34, "BSR2H_Body",  parameter2d[34],  0.0,                                0.0,  0.0);//f

  Int_t n_min_params = theMinimizer.NDim();

  LOG4CPLUS_INFO(root, "Free variables: " << theMinimizer.NFree());
  for (int i=0;i<n_params;i++){
    LOG4CPLUS_INFO(root,"" //<<theMinimizer.GetParName(i)<<"="
		   << std::setw(15) << theMinimizer.X()[i]
		   <<"+/-"
		   << std::setw(15) << theMinimizer.Errors()[i]);
  }
  //==============================================================================================
  //==============================================================================================
  // RUN THE MINIMIZER

  TStopwatch timer2;
  theMinimizer.Minimize();
  timer2.Stop();

  LOG4CPLUS_INFO(root, " Used "<< timer2.RealTime()<<" s of realtime");
  LOG4CPLUS_INFO(root, " Used "<< timer2.CpuTime() <<" s of CPU");

  //==============================================================================================
  //==============================================================================================
  //==============================================================================================
  //==============================================================================================
  ///NOW MINIMIZATION IS FINISHED

  Double_t edm(theMinimizer.Edm());
  Double_t minval(theMinimizer.MinValue());
  Int_t status(theMinimizer.Status());
  Int_t covQual(theMinimizer.CovMatrixStatus());
  rew.FillControlHistograms(Form("control_%s_%s.root",theModel->GetName().c_str(),add_name.c_str()));


  theMinimizer.PrintResults();
  ///NOW Store the results
  TTree t_output("FitResult","FitResult");
  Double_t Parameters[n_params][2];
  //  Double_t Correlations[n_params][n_params];
  std::vector<std::vector<double> > correlations(n_params);
  std::vector<std::string> varname(n_params);
  t_output.Branch("n_params",&n_min_params,"n_params/I");
  t_output.Branch("Param",Parameters,"Parameters[n_params][2]/D");
  //t_output.Branch("Correlations",Correlations,"Correlations[n_params][n_params]/D"); 
  t_output.Bronch("Correlations","std::vector<std::vector<double> >",&correlations);
  t_output.Bronch("ParamName","std::vector<std::string>",&varname);
  t_output.Branch("edm",&edm,"edm/D");
  t_output.Branch("minval",&minval,"minval/D");
  t_output.Branch("binsx",&binsx,"binsx/I");
  t_output.Branch("binsy",&binsy,"binsy/I");
  t_output.Branch("binsz",&binsz,"binsz/I");
  t_output.Branch("status",&status,"status/I");
  t_output.Branch("covQual",&covQual,"covQual/I");


  LOG4CPLUS_INFO(root, "Results:\n");
  
  //n_params=theMinimizer.NDim();
  for (int i=0;i<n_params;i++){
    Parameters[i][0]=theMinimizer.X()[i] ;
    Parameters[i][1]=theMinimizer.Errors()[i];
    varname.push_back(theMinimizer.VariableName(i));
    LOG4CPLUS_INFO(root, std::setw(15) << Parameters[i][0] << " +- " << std::setw(15) << Parameters[i][1]);
  }

  TMatrixTSym<double> corr_mat(n_params);
  for (int i=0;i<n_params;i++){
    std::vector<double> corrj(n_params);
    for (int j=0;j<n_params;j++){
      //      Correlations[i][j]=theMinimizer.Correlation(i,j);
      //std::cout<<"corr "<<Correlations[i][j]<<std::endl;
      corrj.push_back(theMinimizer.Correlation(i,j));
      corr_mat(i,j) = theMinimizer.Correlation(i,j);

    }
    correlations.push_back(corrj);
  }
  t_output.Fill();//because of correlation matrix

  
  TFile*mc_data_tuple = TFile::Open(mcfile.c_str());
  TTree*input_p = NULL;
  mc_data_tuple->GetObject("InitialParameters",input_p);

  TFile*f_final = TFile::Open(Form("%s_%s_final.root",add_name.c_str(),theModel->GetName().c_str()),
			      "RECREATE");
  t_output.Write();
  corr_mat.Write();
  if (input_p){ TTree*copy = input_p->CloneTree(); copy->Write(); }
  f_final->Close();
  delete f_final;
  if (scan){
    double x[100];
    double y[100];
    unsigned int steps = 100;
    for (int p =0; p < n_params ; ++p){
      theMinimizer.Scan(p,steps,x,y,0.,0.); 
      TGraph*g = new TGraph(steps,x,y);
      g->SetName(Form("p%i", p));
      //rew.SaveTree();//save tree
      TFile *f_proj = TFile::Open(Form("control_%s_%s.root",theModel->GetName().c_str(),add_name.c_str()),
				  "UPDATE");

      if (input_p){ TTree*copy = input_p->CloneTree(); copy->Write(); }

      g->Write();
      f_proj->Close();
      delete f_proj;
    }
  }

  delete theModel;
  timer.Stop();

  LOG4CPLUS_INFO(root, " Used "<<timer.RealTime()<<" s of realtime");
  LOG4CPLUS_INFO(root, " Used "<<timer.CpuTime() <<" s of CPU");

  
  return 0;
}
