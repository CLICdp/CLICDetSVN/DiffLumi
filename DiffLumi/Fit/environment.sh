#--------------------------------------------------------------------------------
#     GSL
#--------------------------------------------------------------------------------
export GSL_HOME="/afs/cern.ch/sw/lcg/external/GSL/1.10/x86_64-slc5-gcc43-opt"
export PATH="$GSL_HOME/bin:$PATH"
export LD_LIBRARY_PATH="$GSL_HOME/lib:$LD_LIBRARY_PATH"


#--------------------------------------------------------------------------------
#     ROOT
#--------------------------------------------------------------------------------
export ROOTSYS="/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/ROOT/v5-30-00"
export PATH="$ROOTSYS/bin:$PATH"
export LD_LIBRARY_PATH="../Fit/FunctionBase/:/usr/local/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH="$ROOTSYS/lib:$LD_LIBRARY_PATH"

#--------------------------------------------------------------------------------
#     CMake
#--------------------------------------------------------------------------------
export PATH="/afs/cern.ch/sw/lcg/external/CMake/2.8.6/x86_64-slc5-gcc43-opt/bin:$PATH"
