int OtherPlotsPoly(  ) {
  gStyle->SetOptStat(0);
  
  TFile*f1 = TFile::Open("ref_histos.root");

  TH2Poly*h1 = (TH2Poly*)f1->Get("Reference");
  h1->SetName("Ref");
  
  TFile*f2 = TFile::Open("newf.root");
  TH2Poly*h2 = (TH2Poly*)f2->Get("NewParameters");
  h2->SetName("NewP");
  
  TCanvas*c0 = new TCanvas("c0","c0");
  //c0->SetLogz();
  h1->Draw("colz");
  
  TCanvas*c1 = new TCanvas("c1","c1");
  //c1->SetLogz();
  h2->Draw("colz");
  

  TCanvas*c3 = new TCanvas("c3","c3");
  TH2Poly*h2_diff = (TH2Poly*)h2->Clone("h2_diff");
  h2_diff->SetTitle("");
  TList* listOfBins_2 = h2->GetBins();
  TList* listOfBins_1 = h1->GetBins();
  TList* listOfBins_2d = h2_diff->GetBins();
  for (int i = 0;i<listOfBins_2->GetSize();++i){
    double p1_cont = 1.*(dynamic_cast<TH2PolyBin*>(listOfBins_1->At(i)))->GetContent();
    double p2_cont = 1.*(dynamic_cast<TH2PolyBin*>(listOfBins_2->At(i)))->GetContent();
    //cout<<i<<"  "<<(p2_cont-p1_cont)/p1_cont<<endl;
    if (p2_cont/p1_cont<1.75){
      (dynamic_cast<TH2PolyBin*>(listOfBins_2d->At(i)))->SetContent(p2_cont/p1_cont);
    }
    else{
      (dynamic_cast<TH2PolyBin*>(listOfBins_2d->At(i)))->SetContent(1.75);
    }
    
  }
  
  h2_diff->Draw("colz");


  return 0;
  
}
