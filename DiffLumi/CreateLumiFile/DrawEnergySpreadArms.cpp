#include "Utilities.h"
#include "ConvolutedFunctions.hh"
#include "DrawSpectra.hh"

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

using Utility::Integrate;
using Utility::DrawConfidenceInterval;

using namespace ConvolutedFunctions;

int main (int argc, char **argv) {

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
  RootStyle::MyColor colors;
#endif

  if (argc < 3) {
    std::cout << "Not enough parameters"  << std::endl;
    std::cout << "DrawEnergySpreadArms <BeamEnergy> <InputFile>"  << std::endl;
    return 1;
  }
  //  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root";

  double nominalEnergy = std::atof(argv[1]);
  TString fileWithELoss  = TString(argv[2]);
  double smin(0.0), smax(0.0);

  TF1 functions[2] = {getESFunction( getParameters() ), getESFunction( getParameters())};

  if( nominalEnergy > 1000) {
    smin=-0.0046*1.1;
    smax= 0.0054*1.1;
    //fix upper beta dist parameter for tailFunction
    functions[1].SetParLimits(1, 0.0, 10.0);
    functions[1].SetParameter(1, 1.0);
  // for(int i = 0; i <= 1; ++i) {
  //   functions[i].FixParameter( 2, -0.00467888 );
  //   functions[i].FixParameter( 3,  0.00549502 );
  //   functions[i].FixParameter( 4,  0.000136742);
  // }


    
  } else {
    smin =-0.006;
    smax = 0.008;

    functions[0].SetParameters(   -0.532481, -0.422864, -0.00451798, 0.00559365, 0.000778668);
    functions[1].SetParameters(   -0.532481,  1.422864, -0.00451798, 0.00559365, 0.000778668);

    functions[1].SetParLimits(0, -0.7, 10.01);
    functions[1].SetParLimits(1, -0.7, 10.01);
    functions[1].SetParLimits(2, -0.006,   -0.003);
    functions[1].SetParLimits(3,  0.003,    0.007);
    functions[1].SetParLimits(4, 0.00005,   0.0012);

    for(int i = 0; i <= 1; ++i) {
      functions[i].FixParameter( 2, -0.00451665);
      functions[i].FixParameter( 3,  0.00559533);
      functions[i].FixParameter( 4, 0.000798061);
    }


  }

  //HistoVector histograms( DrawSeparatedEnergySpread("PeakArms", fileWithELoss, smin, smax, 100000000, false));
  HistoVector histograms( DrawSeparatedEnergySpread("PeakArms", fileWithELoss, smin, smax, 300000, false));

  std::vector<Color_t> lineColors;
  std::vector<Color_t> fillColors;
  lineColors.push_back(kRed-7);
  fillColors.push_back(kPink+2);

  lineColors.push_back(kBlue);
  fillColors.push_back(kCyan+1);

  TCanvas canv("canv","Energy Spread Peak and Arms");
  TGraphErrors* fitCL[2];
  TVirtualFitter::SetPrecision(0.0001);
  for (int i = 0; i <= 1 ; ++i) {
    std::cout << "\n"  << std::endl;
    //    histograms[i].Fit( &functions[i] , "");
    // std::cout << "*******************************************************************************************"  << std::endl;
    //histograms[i].Fit( &functions[i] , "EV");
    // std::cout << "*******************************************************************************************"  << std::endl;
    histograms[i].Fit( &functions[i] , "WL");
    std::cout << "Fit " << i << " Entries " << histograms[i].GetEntries()  << std::endl;
    fitCL[i] = DrawConfidenceInterval(functions[i],0.99);
#ifdef USE_RootStyle
    Color_t color = colors.GetColor();
    RootStyle::SetAllColors( &histograms[i], color);
    functions[i].SetLineColor( lineColors.at(i) );
    fitCL[i]->SetLineColor( lineColors.at(i)  );
    fitCL[i]->SetFillColor( fillColors.at(i) );
    fitCL[i]->SetTitle("Fit + 99% C.L.");
#endif
  }//loop over histograms and fits


  histograms[0].Draw("");
  fitCL[0]->Draw("l3");

  histograms[1].Draw("same");
  fitCL[1]->Draw("l3");

#ifdef USE_RootStyle
  {
    //RootStyle::AutoSetYRange (canv, 1.1);
    histograms[0].SetAxisRange(0,400, "y");
    TLegend* leg = RootStyle::BuildLegend(canv, 0.3, 0.9-5*0.0625, 0.6, 0.9);
    leg->SetHeader("Fixed Parameters");
    //leg->SetMargin(0.16);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.06);
  }
#endif

  functions[0].Draw("same");
  functions[1].Draw("same");


  canv.SaveAs("EnergySpreadPeakArms.eps");

  return 0;
}//Main
