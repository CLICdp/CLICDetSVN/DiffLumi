//#include "include_funcs.h"

#include "AbstractModel.hh"
#include "ModelOverlap.hh"
#include "ModelSeparate.hh"
#include "Utilities.h"
#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TRandom3.h>
#include <TStyle.h>
#include <TTree.h>
#include <TSpline.h>

#include <fstream>
#include <iostream>
#include <iomanip>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>

#include <tclap/CmdLine.h>

using namespace std;
using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace TCLAP;

enum Parts{ kNONE=-1, kPeak, kArm1, kArm2, kBody};

int main(int argc, char *argv[]) {

  bool writefile(true);
  std::string prefix("");
  Long_t numberOfEvents(30000000); //thirty Million
  std::string model("Separate");
  bool crosssectionweight(false);
  double e_cut(0.0);
  std::string paramfile("");
  std::string add_name("");
  try {
    CmdLine cmd("Generate events according to one of our models", ' ', "0.0");
    ValueArg<std::string> prefixArg("","prefix","Prefix of output files",false,
				    prefix,"string",cmd);

    ValueArg<std::string> paramfileArg("","parameters","Parameter file to use",false,
				       paramfile,"string",cmd);
    std::vector<std::string> allowedmodels;
    allowedmodels.push_back("Overlap");
    allowedmodels.push_back("Separate");
    ValuesConstraint<std::string> allowedmodelsVals( allowedmodels );
    ValueArg<std::string> modelArg("","model","Model to use",false,
				   model,&allowedmodelsVals);
    cmd.add(modelArg);
    ValueArg<int> mcstatArg("m","MC","Number of MC events to generate",false,
			    numberOfEvents,"int",cmd);
    
    TCLAP::SwitchArg writefileArg("","writeFile","Write the text file for BHWide input?",
				  cmd, false);
    TCLAP::SwitchArg xsecArg("","ApplyXSec","Apply the cross section shape?",
				  cmd, false);
    TCLAP::ValueArg<double> ecutArg("c","ecut","energy cut in GeV",false,e_cut,
				    "double",cmd);

    cmd.parse( argc, argv );

    model = modelArg.getValue();
    numberOfEvents = mcstatArg.getValue();
    writefile = writefileArg.getValue();
    crosssectionweight = xsecArg.getValue();
    e_cut = ecutArg.getValue();
    paramfile = paramfileArg.getValue();
    prefix = prefixArg.getValue();
  }
  catch (TCLAP::ArgException &e){  // catch any exceptions 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return 1;
  }
  ConfigureAndWatchThread configureThread(LOG4CPLUS_TEXT(Utility::GetWorkingDirectory()+"/Logger/log4cplus.properties"), 5 * 1000);
  Logger root = Logger::getRoot();
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  //Long_t peakcn=0, arm1cn=0, arm2cn=0, bodycn=0;
  //Long_t counter = 0;
  if (prefix != ""){
    prefix += "_";
  }

  gRandom = new TRandom3(0);
  TSpline5*f_xsec = NULL;
  double max_xsec = 0.0;

  LOG4CPLUS_INFO(root, "Using sqrts > "<<e_cut <<" GeV events only");

  if (crosssectionweight){
    LOG4CPLUS_INFO(root,"Applying cross section");
    ///first thing we do is to get the cross section
    TFile*fcrossection = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/CrossSectionsBHWide.root"));
    if (!fcrossection){
      LOG4CPLUS_FATAL(root,"Cross section file was not found");
      return 1;
    }
    TTree*xsecttree = NULL;
    fcrossection->GetObject("Tree",xsecttree);
    if (!xsecttree){
      LOG4CPLUS_FATAL(root,"Cross section tree was not found!");
      return 1;
    }
    Long64_t entries=  xsecttree->GetEntries();
    int E=0;
    double xsec = 0.0;
    xsecttree->SetBranchAddress("E",&E);
    xsecttree->SetBranchAddress("xsec",&xsec);
    std::vector<double> aEng(entries), aXsec(entries);
    for (Long64_t i = 0; i< entries; ++i){
      xsecttree->GetEntry(i);
      aEng[i] = E;
      aXsec[i] = xsec;
    }
    fcrossection->Close();
    TGraph graph(entries, &aEng[0], &aXsec[0]);
    f_xsec = new TSpline5("spline3",&graph);
    //std::cout << "spline "  << f_xsec->Eval(3) << std::endl;
    
    if (e_cut < 1)//small enough value
      max_xsec= f_xsec->Eval(f_xsec->GetXmin());
    else
      max_xsec= f_xsec->Eval(e_cut);
    
  }

  Double_t x1, x2, dx1, dx2, dx1gauss, dx2gauss;
  Double_t E1,E2;
  Double_t RelativeProbability = 1;

  Int_t h2Bins = 200;
  Double_t h2Low = 0.97, h2Hih = 1.01;
  std::vector<TH2D> histos;

  histos.push_back(TH2D("h2Peak","Peak", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Arm1","Arm1", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Arm2","Arm2", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Body","Body", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Alll","All" , h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));


  //RooFitDataSetFilling *rfdata = new RooFitDataSetFilling("CreatedLumiFile");
  //rfdata->AddVariable("sqrts",0.,3040.);

  // //From FitESConvolution
  // const Double_t bsbfa = -5.23204e-01, bsbfb  =  -4.09878e-01;
  // const Double_t bsLow = -4.65737e-03, bsHigh =   5.47594e-03;
  // const Double_t BeamSpreadSmearing = 1.37607e-04;

  //From FitESConvolution fitted to Double lumi
  // const Double_t bsbfa = -0.522336,   bsbfb  =  -0.409289 ;
  // const Double_t bsLow = -0.00467888, bsHigh =  0.00549502;
  const Double_t BeamSpreadSmearing = 0.00013674;
  // Double_t parameter2d[] = { 
  //   //0.28, 0.23, 0.23,		//For the different parts      0  1  2
  //   0.25, 0.25, 0.25,		//For the different parts      0  1  2
  //   2.5e0 , -7.5e-1 ,		//with delta function          3  4
  //   bsbfa, bsbfb   ,		// beamspread parameter        5  6
  //   bsLow   , bsHigh ,		//Range for BeamSpread         7  8
  //   2.5e0 , -7.5e-1 ,		//with delta function          9 10
  //   bsbfa , bsbfb,		//beam spread                 11 12
  //   bsLow   , bsHigh ,		//Range for BeamSpread        13 14
  //   1.5e-1 ,  -5.5e-1 ,		//beta body1                  15 16
  //   1.5e-1 ,  -5.5e-1,    	//beta body2                  17 18
  //   bsbfa , 0.35,		//beam spread                 19 20
  //   bsLow   , bsHigh ,		//Range for BeamSpread        21 22
  //   bsbfa , 0.35,		//beam spread                 23 24
  //   bsLow   , bsHigh, 		//Range for BeamSpread        25 26
  //   bsbfa , 0.35,		//beam spread                 27 28
  //   bsLow   , bsHigh ,		//Range for BeamSpread        29 30
  //   bsbfa , 0.35,		//beam spread                 31 32
  //   bsLow   , bsHigh };		//Range for BeamSpread        33 34


  //const Double_t bsbfa = -0.522336*1.1,   bsbfb  =  -0.409289*0.9 ;
  Double_t parameter2d[] = { 
    // 0.28, 0.23, 0.23,		//For the different parts      0  1  2
    // 3.e0 , -6.5e-1 ,		//with delta function          3  4
    // bsbfa, bsbfb   ,		//beamspread parameter        5  6
    // bsLow   , bsHigh ,		//Range for BeamSpread         7  8
    // 3.e0 , -6.5e-1 ,		//with delta function          9 10
    // bsbfa , bsbfb,		//beam spread                 11 12
    // bsLow   , bsHigh ,		//Range for BeamSpread        13 14
    // 2.5e-1 ,  -4.5e-1 ,		//beta body1                  15 16
    // 2.5e-1 ,  -4.5e-1,    	//beta body2                  17 18
    // bsbfa , 0.45,		//beam spread                 19 20
    // bsLow   , bsHigh ,		//Range for BeamSpread        21 22
    // bsbfa , 0.45,		//beam spread                 23 24
    // bsLow   , bsHigh, 		//Range for BeamSpread        25 26
    // bsbfa , 0.45,		//beam spread                 27 28
    // bsLow   , bsHigh ,		//Range for BeamSpread        29 30
    // bsbfa , 0.45,		//beam spread                 31 32
    // bsLow   , bsHigh };		//Range for BeamSpread        33 34
    //From the Fit50x50_NewPar results;
    2.488465e-01,    2.584529e-01,    2.618790e-01,    1.860193e-01,   -6.091112e-01,   -2.686201e-01,   -2.930817e-01,   -4.678880e-03,    5.495020e-03,    1.364102e-01,   -6.058294e-01,   -2.752505e-01,   -3.043557e-01,   -4.678880e-03,    5.495020e-03,    8.409268e-03,   -6.653408e-01,    9.994329e-03,   -6.622097e-01,   -4.359329e-01,    5.544771e-01,   -4.678880e-03,    5.495020e-03,   -4.389765e-01,    5.366775e-01,   -4.678880e-03,    5.495020e-03,   -5.223360e-01,    3.500000e-01,   -4.678880e-03,    5.495020e-03,   -5.223360e-01,    3.500000e-01,   -4.678880e-03,      5.495020e-03};



  TString param_names[] = {
    "Peak", "Arm1", "Arm2", "BFArm1A", "BFArm1B", "BSB1a", "BSB1b", "BSRange1Lo", "BSRange1Hi",
    "BFArm2A", "BFArm2B", "BSB2a", "BSB2b", "BSRange2Lo", "BSRange2Hi", "BFBod1A", "BFBod1B",
    "BFBod2A", "BFBod2B", "BSB1a_Arm", "BSB1b_Arm", "BSR1L_Arm", "BSR1H_Arm", 
    "BSB2a_Arm", "BSB2b_Arm", "BSR2L_Arm", "BSR2H_Arm", "BSB1a_Body", "BSB1b_Body", 
    "BSR1L_Body", "BSR1H_Body", "BSB2a_Body", "BSB2b_Body", "BSR2L_Body", "BSR2H_Body"
  };

  if (paramfile != ""){
    Utility::ReadFitResult(paramfile, parameter2d);
    add_name = "_notdef";
  }
  else{
    add_name = "_def";
  }

  //  TH1D h1Beamspread("h1Beamspread","h1Beamspread",1000,  parameter2d[7]*1.1, parameter2d[8]*1.1);
  TH1D h1Beamspread("h1Beamspread","h1Beamspread",200,  -0.0046*1.1, 0.0054*1.1);
  h1Beamspread.SetXTitle("#Delta E");
  h1Beamspread.SetYTitle("dN/d#Delta E");

  AbstractModel *theModel = AbstractModel::getModel(model,parameter2d);
  std::ofstream ofs(Form("%sLumiFile_%s%s.out", prefix.c_str(),theModel->GetName().c_str(),add_name.c_str()));
  LOG4CPLUS_INFO(root,"Will write text events to "<<Form("%sLumiFile_%s%s.out",
							 prefix.c_str(),
							 theModel->GetName().c_str(),
							 add_name.c_str()));
  //TFile*ft = TFile::Open( TString(Utility::GetWorkingDirectory()+Form("/RootFiles/LumiFileMC_%s%s.root", theModel->GetName().c_str(), add_name.c_str() ) ) ,"RECREATE");
  TFile*ft = TFile::Open( TString( Form("./%sLumiFileMC_%s%s.root", prefix.c_str(), 
					theModel->GetName().c_str(), add_name.c_str() ) ) ,"RECREATE");
  LOG4CPLUS_INFO(root,"Opening file "
		 << std::setw(15) << ft->GetPathStatic()
		 << " and will create " << numberOfEvents
		 << " events.");

  //  TFile*ft = TFile::Open("LumiFileData.root","RECREATE");
  TTree*t = new TTree("MyTree","MyTree");
  t->Branch("eb1",&x1,"eb1/D");
  t->Branch("eb2",&x2,"eb2/D");
  t->Branch("deb1",&dx1,"deb1/D");
  t->Branch("deb2",&dx2,"deb2/D");
  t->Branch("deb1_g",&dx1gauss,"deb1_g/D");
  t->Branch("deb2_g",&dx2gauss,"deb2_g/D");
  t->Branch("E1",&E1,"E1/D");
  t->Branch("E2",&E2,"E2/D");  
  t->Branch("RelativeProbability",&RelativeProbability,"RelativeProbability/D");

  TTree*inputParams = new TTree("InitialParameters","InitialParameters");
  double p_value = 0;
  TString p_name("");
  inputParams->Branch("Name","TString",&p_name);
  inputParams->Branch("Value",&p_value,"Value/D");
  for (int i = 0 ; i< 35; ++i){
    p_value = parameter2d[i];
    p_name = param_names[i];
    inputParams->Fill();
  }
  inputParams->Write();
  Long64_t tried = 0;

  AbstractModel::Parts Chosen = AbstractModel::kNONE;
  Long_t evts =0;
  do{
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(evts, numberOfEvents);
#endif
    Chosen = theModel->GetRandomVariates(x1, x2, dx1, dx2, dx1gauss, dx2gauss, RelativeProbability, BeamSpreadSmearing);

    E1 = (x1+dx1+dx1gauss);
    E2 = (x2+dx2+dx2gauss);
    tried ++;
    double sqrts = 3000.*TMath::Sqrt(E1*E2);
    if (sqrts < e_cut ) continue; 
    if (f_xsec){
      if (f_xsec->Eval(sqrts) < gRandom->Uniform(max_xsec)){
	continue;
      }
    }

    std::pair<std::string, double> v ("sqrts",sqrts);
    std::vector<std::pair<std::string, double> > vec;
    vec.push_back(v);
    //rfdata->AddToDataSet(vec);

    //std::cout<<"\r Accepted "<<evts<<std::endl;
    if(writefile) {
      ofs << "   " << x1 << "   " << x2
          << "   " << dx1 <<"    " << dx2
          << "   " << dx1gauss <<"    " << dx2gauss
          << "   " << (x1+dx1+dx1gauss)<< "   " << (x2+dx2+dx2gauss)
          << "   " << RelativeProbability << std::endl;
    }


    t->Fill();

    histos[Chosen].Fill(E1, E2);
    histos[4].Fill(E1, E2);
    if(Chosen != AbstractModel::kBody) {
      h1Beamspread.Fill(dx1+dx1gauss);
      h1Beamspread.Fill(dx2+dx2gauss);
    }

    double probability ( theModel->CalculateProbability(x1, x2, dx1, dx2, RelativeProbability) );
    if( fabs (  RelativeProbability / probability  - 1 ) > 0.000001) {
      LOG4CPLUS_WARN(root,"Arg2 "
		     << std::setw(15) << x1
		     << std::setw(15) << x2
		     << std::setw(15) << dx1
		     << std::setw(15) << dx2
		     << std::setw(15) << probability 
		     << std::setw(15) << RelativeProbability  
		     << std::setw(15) << probability/ RelativeProbability  
		     << std::setw(15) << Chosen);
    }
    ++evts;
  }while(evts < numberOfEvents);//for numberOfEvents
  LOG4CPLUS_INFO( root, "Efficiency: "<< 1.*evts/tried);
  LOG4CPLUS_INFO( root, "Entries in the Tree: " << t->GetEntries());
  t->Write();
  ft->Write();
  ft->Close();

  
  //rfdata->MakeRooKeysPdf("sqrts");
  //rfdata->SaveToFile("roofitest.root");


  TFile *ESFile = TFile::Open( TString(Utility::GetWorkingDirectory()+"/CreateLumiFile/EnergySpreadHisto.root"));
  TH1D *GPEsHisto = static_cast<TH1D*>(ESFile->Get("hist"));
  GPEsHisto->Rebin(5);
  TH1D *GPEsHisto2 = static_cast<TH1D*>(GPEsHisto->Clone("hist2"));

  TCanvas c1("MCbeamspread","MC BeamSpread");
  GPEsHisto->SetLineColor(kBlue);
  h1Beamspread.SetLineColor(kRed);
  GPEsHisto->DrawNormalized();
  h1Beamspread.DrawNormalized("same");
  c1.SaveAs( TString(theModel->GetName()+"_MCBeamSpread.eps"));
  TFile*fout = new TFile( TString(theModel->GetName()+"_CreateMCFile.root"),"RECREATE");
  h1Beamspread.Write();
  GPEsHisto->Write();
  fout->Close();

  c1.Clear();
  GPEsHisto->Scale(1./GPEsHisto->Integral());
  GPEsHisto2->Scale(1./GPEsHisto2->Integral());
  h1Beamspread.Scale(1./h1Beamspread.Integral());
  GPEsHisto->Add(&h1Beamspread, -1.);
  GPEsHisto->Divide(GPEsHisto2);
  GPEsHisto->Draw();
  c1.SaveAs( TString(theModel->GetName()+"_Ratio.eps"));

  c1.Clear();
  c1.Divide(2,2);
  for (int i = 1; i <= 4; ++i) {
    c1.cd(i);
    c1.cd(i)->SetLogz();
    histos[i-1].Draw("colz");
  }
  c1.SaveAs( TString(theModel->GetName()+"_Parts.eps"));

  c1.Clear();
  c1.SetLogz();
  histos[4].Draw("colz");
  c1.SaveAs( TString(theModel->GetName()+"_MC2D.eps"));


  return 0;
}// main
