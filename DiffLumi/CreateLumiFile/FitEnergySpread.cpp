#include "BetaFunctionBS.h"
#include "Utilities.h"
using namespace Utility;

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <Minuit2/Minuit2Minimizer.h>
#include <TRandom3.h>
#include <Math/Functor.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1D.h>
#include <TError.h>

#include <iostream>
#include <iomanip>


/***
 * Try to fit the beam energy spread coming from the linac simulation more
 * rigorously, instead of adjusting the gauss width by hand, we will give it as
 * a free parameter for minuit. Though we will have to fit the distribution with
 * MC generation of numbers.
 *
 */



class BeamSpreadFitter{

public:
  inline double operator()(const double *parameter) const;

  BeamSpreadFitter():m_spreadHisto(NULL),m_fitHisto(NULL) {
    TFile *ESFile = TFile::Open( TString(Utility::GetWorkingDirectory() + "/build/energySpreads.root"));
    m_spreadHisto = static_cast<TH1D*>(ESFile->Get("hist"));
    m_spreadHisto->Rebin(5);
    m_fitHisto = static_cast<TH1D*>(m_spreadHisto->Clone("fitter"));
    m_fitHisto->SetLineColor(kBlue);
  }

  double calculateChi2() const;
  
private:
  TH1D *m_spreadHisto;
  TH1D *m_fitHisto;

};


double BeamSpreadFitter::calculateChi2() const {

  double chi2(0.0);
  for (int i = 1; i <= m_spreadHisto->GetNbinsX() ;++i) {
    const double spread(m_spreadHisto->GetBinContent(i));
    if (spread < 10) continue;
   
    const double fit( m_fitHisto->GetBinContent(i));
    const double diff (spread - fit);
    //dont use fit error, we can't check for zero entries, because otherwise an empty histogram has zero chi2!
    chi2 += (diff*diff)/(spread);
  }
 
  return chi2;
}



double BeamSpreadFitter::operator()(const double* parameter) const {

  m_fitHisto->Reset();
  //  delete gRandom;
  //  gRandom = new TRandom(12345);
  gRandom->SetSeed(12345);
  // std::cout << gRandom->GetSeed() << std::endl;
  // std::cout << gRandom->Uniform(1.0)  << std::endl;
  BetaFunctionBS BeamSpread(parameter[0],  parameter[1] , parameter[2],  parameter[3]);
  int nEntries(int(m_spreadHisto->GetEntries()));
  //  std::cout << "Filling " << nEntries << " entries to histo"  << std::endl;

  std::streamsize streamSize( std::cout.precision() );
  std::cout << std::scientific;
  std::cout.precision(6);
  // std::cout << std::setw(14) << chi2;
  std::cout << " Para: ";
  for (int i = 0; i < 5; ++i) {
    //    std::cout << "Parameter " << i << "  " << params[i]  << std::endl;
    std::cout.precision(10);
    std::cout << std::setw(18)  <<  parameter[i];
  }
  std::cout << std::flush;
  //  std::cout.unsetf ( std::ios_base::precision );
  //  nEntries = 10*150000;
  //have to scale the histogram to the spread numbers...
  double scale ( m_spreadHisto->Integral() / double(nEntries) );

  for (int i = 0; i < nEntries;++i) {
    //    RootStyle::PrintTreeProgess(i, nEntries);
    double x1 = BeamSpread.GetRandom();
    /// std::cout << x1  << std::endl;
    x1 += gRandom->Gaus(0,parameter[4]);
    
    m_fitHisto->Fill(x1, scale);

  }

 
  const double chi2(this->calculateChi2());
  //  std::cout << "Chi2 is " << chi2  << std::endl;

  std::cout << "  Chi2: " << std::setw(18) << chi2;
  std::cout << std::endl;


  TCanvas c("c1","c1");
  m_spreadHisto->Draw();
  m_fitHisto->Draw("same");
  c.SaveAs("temp.eps");


  //Reset OstreamFormat
  std::cout.unsetf ( std::ios_base::floatfield );
  std::cout.precision(streamSize);

  return chi2;
}



int main () {

  gErrorIgnoreLevel=kWarning;
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  //Define our minuit Object
  ROOT::Minuit2::Minuit2Minimizer minuitInstance;

  //We have parameter, gauss width, upper and lower limit, and the two beta distribution parameter
  BeamSpreadFitter bsf;
  ROOT::Math::Functor FCNFunction(bsf, 5);

  minuitInstance.SetFunction(FCNFunction);
  minuitInstance.SetMaxFunctionCalls(20000);
  minuitInstance.SetMaxIterations(100);
  minuitInstance.SetPrecision(1e-5);
  minuitInstance.SetStrategy(2);
  minuitInstance.SetPrintLevel(3);

  Double_t errorFraction = 10.0;

  const Double_t bsLow = -0.00465, bsHigh =  0.00545;
  const Double_t bsbfa = -0.45, bsbfb    = -0.351617;

  // Double_t parameter[] = {  -0.4149, -0.3421,
  // 			    -0.00465, 0.00545,
  // 			    0.00013};

  Double_t parameter[] = {  bsbfa, bsbfb,
			    bsLow, bsHigh,
			    0.00013};

  SetVariable(minuitInstance, kLimited,  0, "a",     parameter[0],  fabs(parameter[0]/errorFraction), -1.0,  0.0);
  SetVariable(minuitInstance, kLimited,  1, "b",     parameter[1],  fabs(parameter[1]/errorFraction), -1.0,  0.0);
  SetVariable(minuitInstance, kLimited,  2, "low",   parameter[2],  fabs(parameter[2]/errorFraction), -0.005, -0.003);
  SetVariable(minuitInstance, kLimited,  3, "high",  parameter[3],  fabs(parameter[3]/errorFraction),  0.003,  0.006);
  SetVariable(minuitInstance, kLimited,  4, "Width", parameter[4],  fabs(parameter[4]/errorFraction),  0.00001,  0.0003);



  for (int i=0;i<5;i++){
    std::cout //<<minuitInstance.GetParName(i)<<"="
      << std::setw(15) << minuitInstance.X()[i]
      <<"+/-"
      << std::setw(15) << minuitInstance.Errors()[i]
      << std::endl;
  }

  //  minuitInstance.Minimize();

  minuitInstance.PrintResults();


  return 0;





}//main
