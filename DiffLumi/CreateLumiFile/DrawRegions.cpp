#include "Utilities.h"

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TString.h>
#include <TStyle.h>


#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

void DrawRegions(TString const& filename, TString const& title);


int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  gStyle->SetTitleX(0.3);
  gStyle->SetTitleW(0.4);

  DrawRegions ( TString(Utility::GetWorkingDirectory() + "/RootFiles/LumiFileMC_Separate_1.root")  ,"Separate" );
  DrawRegions ( TString(Utility::GetWorkingDirectory() + "/RootFiles/LumiFileMC_Overlap_1.root")  ,"Overlap" );

}//main

void DrawRegions(TString const& fileName, TString const& title) {

  Int_t h2Bins = 200;
  Double_t h2Low = 0.97, h2Hih = 1.01;
  std::vector<TH2D> histos;

  histos.push_back(TH2D("h2Peak","Peak;x_{1};x_{2}", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Arm1","Arm1;x_{1};x_{2}", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Arm2","Arm2;x_{1};x_{2}", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Body","Body;x_{1};x_{2}", h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  histos.push_back(TH2D("h2Alll","All;x_{1};x_{2}" , h2Bins, h2Low, h2Hih, h2Bins, h2Low, h2Hih));
  

  TFile* file = TFile::Open( fileName);
  TTree *treePtr;
  if( !file ) { std::cout << "File not found " << std::endl; return; }
  file->GetObject("MyTree", treePtr);

  TTree& t = *treePtr;

  Double_t x1, x2, dx1, dx2, dx1gauss, dx2gauss;
  Double_t E1,E2;

  t.SetBranchAddress("eb1",&x1);
  t.SetBranchAddress("eb2",&x2);
  t.SetBranchAddress("deb1",&dx1);
  t.SetBranchAddress("deb2",&dx2);
  t.SetBranchAddress("deb1_g",&dx1gauss);
  t.SetBranchAddress("deb2_g",&dx2gauss);
  t.SetBranchAddress("E1",&E1);
  t.SetBranchAddress("E2",&E2);
  

  for (int i = 0; i < t.GetEntries();++i) {
    t.GetEntry(i);
    int select = 3;
    if( x1 > 0.9999 && x2 > 0.9999 ) {
      select = 0;
    } else  if( x1 > 0.9999 && x2 <= 0.9999 ) {
      select = 1;
    } else  if( x1 <= 0.9999 && x2 > 0.9999 ) {
      select = 2;
    }//third case is default

    histos[select].Fill(E1, E2);
    histos[4].Fill(E1, E2);
  }
 
  for (std::vector<TH2D>::iterator iter = histos.begin(); iter != histos.end(); ++iter) {
    (*iter).Scale(1./(*iter).Integral() );
    (*iter).SetAxisRange(1e-7,3e-3,"Z");
  }

  file->Close();
  delete file;
  TCanvas canv("cRegion", "Lumi Regions");
  canv.Divide(2,2);
  gStyle->SetOptTitle(1);
  for (int i = 1; i <= 4; ++i) {
    canv.cd(i);
    canv.cd(i)->SetLogz();
    canv.cd(i)->SetRightMargin(0.16);
    histos[i-1].Draw("colz");
  }
  canv.SaveAs( TString(title+"_Parts.eps"));


  gStyle->SetOptTitle(0);
  canv.Clear();
  canv.SetLogz();
  canv.GetPad(0)->SetRightMargin(0.16);
  histos[4].Draw("colz");

  canv.SaveAs( TString(title+"_MC2D.eps"));


  return;

}//main
