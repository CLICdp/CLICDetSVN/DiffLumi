#include "Utilities.h"
#include "DrawSpectra.hh"
#include "ConvolutedFunctions.hh"
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TStyle.h>
#include <TROOT.h>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

void SetFunctionParameterNames(std::vector<TF1>& functions);
void SetFunctionParLimits(std::vector<TF1>& functions);

void FitBeamstrahlung(TH1D& GPHisto, double limit=0.0);
void FitChebyshev(TH1D& GPHisto, double limit=0.0);
using Utility::DrawConfidenceInterval;


//const double betaLimit(0.999);
const int fMax = 3;
const int fMin = 0;

int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  // gStyle->SetOptFit(1111);
  // gStyle->SetOptStat(1111);
  // gROOT->ForceStyle();


  Double_t smin = 0.00, smax = 1.10;
  //  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root";

  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/Lumi350_Smeared_2.root";

  //TH1D hEParticle2  = DrawEnergy("EPart2", fileWithELoss, 300000000, false, 0.0, int(400*1.1/1.02));
  //TH1D GPHisto  = DrawLumiSpectrum("roots", fileWithELoss ,smin, smax, 300000000,false, true, 200);
  TH1D GPHisto  = DrawLumiSpectrum("roots", fileWithELoss ,smin, smax, 300000000,false, true, 3000);

  // TH1D GPHisto  = DrawLumiSpectrum("roots", fileWithELoss ,smin, smax, 300000000,false, true, 200);
  // GPHisto.SetTitle("#sqrt{s'}/#sqrt{s_{Nom}};x=#sqrt{s'}/#sqrt{s_{Nom}};dN/dx");

  // FitBeamstrahlung(GPHisto, 0.0);
  // GPHisto.SetName("roots05");
  // FitBeamstrahlung(GPHisto, 0.5);

  TH1D hEParticle  = DrawEnergy("EPart", fileWithELoss, 300000000, false, 0.0, int(200*1.1/1.02));
  //TH1D hEParticle  = DrawEnergy("EPart", fileWithELoss, 1500000/2, false, 0.0, int(400*1.1/1.02));
  //  TH1D hEParticle  = DrawEnergy("EPart", fileWithELoss, 300000000, false, 0.0, 300);
  hEParticle.SetTitle("Particle Energy;x=Particle Energy/E_{Beam};dN/dx");


  {
    TCanvas canv("canv","canv");
    hEParticle.Draw();
#ifdef USE_RootStyle
    {//wrap in brackets to limit scope
      TLegend *leg;
      RootStyle::MyColor colors;
      hEParticle.SetLineColor( colors.GetColor() );
      RootStyle::AutoSetYRange(canv, 1.5);
      leg = RootStyle::BuildLegend(canv, 0.2, 0.8375, 0.5, 0.9);
      leg->SetTextSize(0.06);
      leg->SetBorderSize(0);
    }
#endif
    canv.SetLogy();
    hEParticle.SetAxisRange(1e-3, 1e2, "y");
    canv.SaveAs("ParticleEnergySpectrum.eps");
  }

  hEParticle.Scale( 1.0 / hEParticle.Integral(1, hEParticle.FindFixBin(gBetalimit)-1, "width"));

  hEParticle.SetName("EPart00");
  FitBeamstrahlung(hEParticle, 0.0);

  hEParticle.SetName("EPart03");
  FitBeamstrahlung(hEParticle, 0.3);

  hEParticle.SetName("EPart05");
  FitBeamstrahlung(hEParticle, 0.5);

  hEParticle.SetName("EPart06");
  FitBeamstrahlung(hEParticle, 0.6);

  hEParticle.SetName("EPart07");
  FitBeamstrahlung(hEParticle, 0.7);

  FitChebyshev(hEParticle, 0.0);
  FitChebyshev(hEParticle, 0.5);

  return 0;
}

void FitBeamstrahlung(TH1D& GPHisto, double limit) {
  TCanvas c1("c1","c1");

  //Create different TF1 for the fit
  std::vector<TString> FunctionTitle;

  // FunctionTitle.push_back("b(x; a_{1}, a_{2})");
  // FunctionTitle.push_back("p_{a}b(x; a_{1}, a_{2})+p_{b}b(x; b_{1}, b_{2})");
  // FunctionTitle.push_back("p_{a}b(x; a_{1}, a_{2})+p_{b}b(x; b_{1}, b_{2})+p_{c}b(x; c_{1}, c_{2})");

  // FunctionTitle.push_back("b(x; #vec{A})");
  // FunctionTitle.push_back("p_{A}b(x; #vec{A})+p_{B}b(x; #vec{B})");
  // FunctionTitle.push_back("p_{A}b(x; #vec{A})+p_{B}b(x; #vec{B})+p_{C}b(x; #vec{C})");


  FunctionTitle.push_back("1 Beta-Distribution");
  FunctionTitle.push_back("2 Beta-Distributions");
  FunctionTitle.push_back("3 Beta-Distributions");
  FunctionTitle.push_back("4 Beta-Distributions");

  std::vector<TF1> functions;
  functions.push_back(TF1("bf1",bf1, limit, gBetalimit, 3));
  functions.push_back(TF1("bf2",bf2, limit, gBetalimit, 6));
  functions.push_back(TF1("bf3",bf3, limit, gBetalimit, 9));
  functions.push_back(TF1("bf4",bf4, limit, gBetalimit, 12));
  // double parameters[3][8] = {{ 2, -0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
  //			     { 2, -0.5, 2.0,-0.5, 0.5, 0.0, 0.0, 0.0},
  //			     { 2, -0.5, 2.0,-0.5, 2.0,-0.5, 0.3, 0.3}};
  //                           0    1    2    3    4    5    6    7    8

  double parameters[4][12] = {{ 2.0, -0.5, 1.0, 0.0,  0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0, 0.0},
			      { 2.0, -0.5, 0.5, 1.0, -0.7, 0.5, 0.0,  0.0, 0.0, 0.0, 0.0, 0.0},
			      { 2.0, -0.5, 0.3, 2.0, -0.5, 0.3, 2.0, -0.5, 0.3, 0.0, 0.0, 0.0},
			      { 2.0, -0.5, 0.2, 2.0, -0.5, 0.2, 2.0, -0.5, 0.2, 2.0,-0.5, 0.4}};

  // double parameters[3][9] = {{ 0.947796, -0.564212, 0.50334, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000},
  //			     { 0.947796, -0.564212, 1.87781, 23.9155, 0.50334, 590.879, 0.00000, 0.00000, 0.00000},
  //			     // { 0.947796, -0.564212, 1.87781, 23.9155, 1.35318, 3.11387, 0.7    , 590.879, 6.35651 }};
  //			     // { 0.947796, -0.564212, 1.87781, 23.9155, 1.35318, 3.11387, 0.50334, 590.879, 6.35651}};
  //			     {1.11532, -0.546704, 2.3127, 18.8041, 1.61987, 2.89385, 0.634409, 590.94, 6.58696}};


  for (int i = fMin; i < fMax;++i) {
    functions[i].SetParameters(parameters[i]);
    functions[i].SetTitle(FunctionTitle[i]);
  }

  SetFunctionParameterNames(functions);
  SetFunctionParLimits(functions);

  GPHisto.Draw();
  //  functions[0].Draw("same");
  //Have to increase number of iterations
  TVirtualFitter::SetMaxIterations(100000);
  //TGraphErrors* cls[fMax];

  std::vector<Color_t> lineColors;
  std::vector<Color_t> fillColors;
  lineColors.push_back(kSpring);
  fillColors.push_back(kGreen+3);

  lineColors.push_back(kRed-7);
  fillColors.push_back(kPink+2);

  lineColors.push_back(kBlue);
  fillColors.push_back(kCyan+1);



  //for (int i = fMax - 1; i >= 0; --i) {
  for (int i = fMin; i < fMax; ++i) {
    std::cout << "*******************************************************"  << std::endl;
    std::cout << "Fitting function with Chi2 " << i+1 << std::endl;
    TFitResultPtr fitR = GPHisto.Fit(&functions[i],"R+S");
    // std::cout << "Now fitting LogLikelihood"  << std::endl;
    // TFitResultPtr fitR2 = GPHisto.Fit(&functions[i],"R+SWL");
    // cls[i] = DrawConfidenceInterval(functions[i], 0.68);
    // cls[i]->SetLineColor( lineColors.at(i)  );
    // cls[i]->SetFillColor( fillColors.at(i) );
    //cls[i]->SetTitle("Fit + 99% C.L.");
    std::cout << "*******************************************************"  << std::endl;
    //fitR->Print("V");



#ifdef USE_RootStyle
    for (int j = 0; j <= i; ++j) {
      functions[j].Draw("same");
    }
  //{//wrap in brackets to limit scope
    TLegend *leg;
    RootStyle::MyColor colors;
    GPHisto.    SetLineColor( colors.GetColor() );
    RootStyle::AutoSetYRange(c1, 1.5);
    leg = RootStyle::BuildLegend(c1, 0.2, 0.9-(i+2)*0.0625, 0.5, 0.9);
    leg->SetTextSize(0.06);
    leg->SetBorderSize(0);

    for (int j = 0; j <= i;++j) {
      functions[j].SetLineColor(colors.GetColor() );
      functions[j].Draw("same");
      //      cls[i]->Draw();
    }
    c1.SetLogy();
    GPHisto.SetAxisRange(1e-3, 1e2, "y");
    c1.SaveAs(TString("BeamStrahlungFit_")+GPHisto.GetName()+TString(Form("_NB%i.eps", i)));
    for (int j = 0; j <=i ;++j) {
      TObject* obj = GPHisto.GetFunction(Form("bf%i",j+1));
      if(obj) obj->Delete();
    }

    // for (int j = 0; j <=i ;++j) {
    //   TObject* obj = GPHisto.GetFunction(Form("bf%i",j+1));
    //   if(obj) obj->Delete();
    // }

#endif
  }//for loop

  c1.SetLogy();
  GPHisto.SetAxisRange(1e-3, 1e2, "y");
  // GPHisto.SetXTitle("x");
  // GPHisto.SetYTitle("dN/dx");

  c1.SaveAs(TString("BeamStrahlungFit_")+GPHisto.GetName()+TString(".eps"));
  for (int i = 0; i < fMax;++i) {
    TObject* obj = GPHisto.GetFunction(Form("bf%i",i+1));
    if(obj) obj->Delete();
  }




}//FitBeamstrahlung

////////////////////////////////////////////////////////////////////////////////////////////////////
// Fit With Chebyshev
////////////////////////////////////////////////////////////////////////////////////////////////////

void FitChebyshev(TH1D& GPHisto, double limit) {

#ifdef USE_RootStyle
    RootStyle::PDFFile myfile(Form("BeamStrahlung_Cheb_%i.pdf", int(100*limit)));
#endif
    for(int n = 1; n < 31; n+=1) {
      //  int n = 50;
      ConvolutedFunctions::Chebyshev cheb(n,limit,gBetalimit);

      TF1 chebFunction("chebFunc", &cheb, limit, gBetalimit, n, "Chebyshev" );
      chebFunction.SetNpx(1000);
      for (int i = 0; i < n; ++i) chebFunction.SetParameter(i,1);
      std::cout << "*********************************************************************************************"  << std::endl;
      TFitResultPtr r = GPHisto.Fit(&chebFunction,"SR","");
      TMatrixDSym cov = r->GetCovarianceMatrix();  //  to access the covariance matrix
      r->Print("V");     // print full informatio

      TCanvas c("c","c");
      GPHisto.Draw();
      c.SetLogy();
      TGraphErrors* fitCSlice50(DrawConfidenceInterval(chebFunction, 0.68));
      fitCSlice50->SetFillColor(kPink+2);
      fitCSlice50->SetLineColor(kRed-7);
      fitCSlice50->SetTitle("Fit1 + 99% C.L.");
      fitCSlice50->Draw("l3");

      TVirtualFitter::GetFitter()->PrintResults(2, 0.2);
#ifdef USE_RootStyle
      myfile.AddCanvas(&c, Form("Degree %i", n));
#endif

      //  c.SaveAs("Fit_BDS_Chebyshev.eps");
    }
#ifdef USE_RootStyle
    myfile.CloseFile();
#endif
  }
////////////////////////////////////////////////////////////////////////////////////////////////////


void SetFunctionParameterNames(std::vector<TF1>& functions) {
  functions[0].SetParName(0,"a_{1}");
  functions[0].SetParName(1,"a_{2}");
  functions[0].SetParName(2,"p_a");

  functions[1].SetParName(0,"a_{1}");
  functions[1].SetParName(1,"a_{2}");
  functions[1].SetParName(2,"p_a");
  functions[1].SetParName(3,"b_{1}");
  functions[1].SetParName(4,"b_{2}");
  functions[1].SetParName(5,"p_b");

  functions[2].SetParName(0,"a_{1}");
  functions[2].SetParName(1,"a_{2}");
  functions[2].SetParName(2,"p_a");
  functions[2].SetParName(3,"b_{1}");
  functions[2].SetParName(4,"b_{2}");
  functions[2].SetParName(5,"p_b");
  functions[2].SetParName(6,"c_{1}");
  functions[2].SetParName(7,"c_{2}");
  functions[2].SetParName(8,"p_c");


  functions[3].SetParName(0,"a_{1}");
  functions[3].SetParName(1,"a_{2}");
  functions[3].SetParName(2,"p_a");
  functions[3].SetParName(3,"b_{1}");
  functions[3].SetParName(4,"b_{2}");
  functions[3].SetParName(5,"p_b");
  functions[3].SetParName(6,"c_{1}");
  functions[3].SetParName(7,"c_{2}");
  functions[3].SetParName(8,"p_c");
  functions[3].SetParName(9,"d_{1}");
  functions[3].SetParName(10,"d_{2}");
  functions[3].SetParName(11,"p_d");


  return;
}



void SetFunctionParLimits(std::vector<TF1>& functions) {

  //  double maxValues[12] = { 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0};
  double maxValues[12] = { 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0};


  functions[0].SetParLimits(0, -0.0, maxValues[0]);
  functions[0].SetParLimits(1, -1.0, maxValues[1]);


  functions[1].SetParLimits(0, -0.0, maxValues[0]);
  functions[1].SetParLimits(1, -1.0, maxValues[1]);
  functions[1].SetParLimits(3, -0.0, maxValues[3]);
  functions[1].SetParLimits(4, -1.0, maxValues[4]);
  functions[1].SetParLimits(2,  0.0, 1.0);

  functions[2].SetParLimits(0, -0.0, maxValues[0]);
  functions[2].SetParLimits(1, -1.0, maxValues[1]);
  functions[2].SetParLimits(3, -0.0, maxValues[3]);
  functions[2].SetParLimits(4, -1.0, maxValues[4]);
  functions[2].SetParLimits(6, -0.0, maxValues[6]);
  functions[2].SetParLimits(7, -1.0, maxValues[7]);


  // functions[2].SetParLimits(2, 0, 0.5);
  // functions[2].SetParLimits(5, 0, 0.5);

  functions[3].SetParLimits(0 , -0.0,  maxValues[ 0]);
  functions[3].SetParLimits(1 , -1.0,  maxValues[ 1]);

  functions[3].SetParLimits(3 , -0.0,  maxValues[ 3]);
  functions[3].SetParLimits(4 , -1.0,  maxValues[ 4]);

  functions[3].SetParLimits(6 , -0.0,  maxValues[ 6]);
  functions[3].SetParLimits(7 , -1.0,  maxValues[ 7]);

  functions[3].SetParLimits(9 , -0.0,  maxValues[ 9]);
  functions[3].SetParLimits(10, -1.0,  maxValues[10]);


  //Fix the contrained parameters
  functions[0].FixParameter(2, 1.0);
  functions[1].FixParameter(5, 1.0);
  functions[2].FixParameter(8, 1.0);
  functions[3].FixParameter(11, 1.0);

  return;
}


