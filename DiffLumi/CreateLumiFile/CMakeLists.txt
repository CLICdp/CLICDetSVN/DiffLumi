LINK_LIBRARIES( RootStyle )

ADD_SUBDIRECTORY ( src )

ADD_EXECUTABLE ( CreateLumiFileAbstract  CreateLumiFileAbstract.cpp )

ADD_EXECUTABLE ( DrawEnergySpreadArms DrawEnergySpreadArms.cpp )
TARGET_LINK_LIBRARIES( DrawEnergySpreadArms Functions )

ADD_EXECUTABLE ( DrawRegions DrawRegions.cpp )

ADD_EXECUTABLE ( FitBeamstrahlung FitBeamstrahlung.cpp )
TARGET_LINK_LIBRARIES ( FitBeamstrahlung Functions )

TARGET_LINK_LIBRARIES ( CreateLumiFileAbstract Reweighting  )

ADD_EXECUTABLE( DrawEnergySpread DrawEnergySpread.cpp)
ADD_EXECUTABLE( FitEnergySpread FitEnergySpread.cpp)
ADD_EXECUTABLE( FitNoBSLumiSpectrum FitNoBSLumiSpectrum.cpp)
ADD_EXECUTABLE( DrawLumiSpectrum DrawLumiSpectrum.cpp)
ADD_EXECUTABLE( FitESConvolution FitESConvolution.cpp)

TARGET_LINK_LIBRARIES( FitNoBSLumiSpectrum Functions)
TARGET_LINK_LIBRARIES( FitEnergySpread Functions)
TARGET_LINK_LIBRARIES( FitESConvolution Functions)

IF( Cppcheck_FOUND )
  ADD_CPPCHECK( DrawEnergySpread    UNUSED_FUNCTIONS STYLE POSSIBLE_ERROR FAIL_ON_WARNINGS)
  ADD_CPPCHECK( FitEnergySpread     UNUSED_FUNCTIONS STYLE POSSIBLE_ERROR FAIL_ON_WARNINGS)
  ADD_CPPCHECK( FitNoBSLumiSpectrum UNUSED_FUNCTIONS STYLE POSSIBLE_ERROR FAIL_ON_WARNINGS)
  ADD_CPPCHECK( DrawLumiSpectrum    UNUSED_FUNCTIONS STYLE POSSIBLE_ERROR FAIL_ON_WARNINGS)
  ADD_CPPCHECK( FitESConvolution    UNUSED_FUNCTIONS STYLE POSSIBLE_ERROR FAIL_ON_WARNINGS)
ENDIF()