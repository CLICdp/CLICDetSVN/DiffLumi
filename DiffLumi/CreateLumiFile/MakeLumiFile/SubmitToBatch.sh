#!/bin/bash


#BSUB -J MakeLumi[1-57]
#BSUB -q 1nd
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/Bout/MakeLumi.%J_%I
###StdErr
#BSUB -e /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/Bout/MakeLumi.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64"


##for LSB_JOBINDEX in {1..56}; do

##Now prepare input parameters
tempnumber=$(( $LSB_JOBINDEX - 1 ))
parameter=$(( $tempnumber % 28 ))
plusminus=0
if [ $LSB_JOBINDEX -eq 57 ]; then
    plusminus=0
elif [ $tempnumber -lt 28 ]; then
    plusminus=1
else
    plusminus=-1
fi


echo "$parameter  $plusminus"

#done
#exit



source /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/environment.sh

export LD_LIBRARY_PATH=./:$LD_LIBRARY_PATH

cp /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/libFunctions.so .
cp /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/CreateFilesPerParam .
cp /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/final_BHWide3e5.root .

./CreateFilesPerParam $parameter $plusminus

cp Nominal*.root  /afs/cern.ch/eng/clic/work2/DiffLumi/MakeLumiFileBin/Output