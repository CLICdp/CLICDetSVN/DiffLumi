// $Id: $
// Include files 



// local


//-----------------------------------------------------------------------------
// Implementation file for class : compareplots
//
// 2011-11-01 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
int compareplots(  ) {
  TFile*f1 = TFile::Open("Nominal.root");
  TH1D*h_nom = (TH1D*)f1->Get("lumi_spectrum");
  h_nom->Scale(1/h_nom->Integral("width"));
  TFile*f2 = TFile::Open("Nominal_p1s.root");
  TH1D*h_nomp1 = (TH1D*)f2->Get("lumi_spectrum");
  h_nomp1->Scale(1/h_nomp1->Integral("width"));
  TFile*f3 = TFile::Open("Nominal_m1s.root");
  TH1D*h_nomm1 = (TH1D*)f3->Get("lumi_spectrum");
  h_nomm1->Scale(1/h_nomm1->Integral("width"));
  
  TFile*f4 = TFile::Open("GuineaPigSpectrum.root");
  TH1D*h_gp = (TH1D*)f4->Get("lumi_spectrum");
  h_gp->Scale(1/h_gp->Integral("width"));

  h_nom->SetXTitle("#sqrt{4E1E2}");
  h_nomp1->SetXTitle("#sqrt{4E1E2}");
  h_nomm1->SetXTitle("#sqrt{4E1E2}");
  h_gp->SetXTitle("#sqrt{4E1E2}");

  h_nom->SetLineColor(kRed);
  h_nomp1->SetLineColor(kBlue);
  h_nomm1->SetLineColor(kGreen+2);

  TCanvas* c1 = new TCanvas("c1","c1");
  c1->SetLogy();
  
  h_gp->Draw();
  h_nomm1->Draw("same");
  h_nom->Draw("same");
  h_nomp1->Draw("same");

  TLegend*l = new TLegend(0.6,0.6,0.8,0.8);
  l->SetFillColor(0);
  l->AddEntry(h_gp,"Guinea Pig","L");
  l->AddEntry(h_nomm1,"Nominal -1 #sigma","L");
  l->AddEntry(h_nom,"Nominal","L");
  l->AddEntry(h_nomp1,"Nominal +1 #sigma","L");
  l->Draw();

  TCanvas*c2 = new TCanvas("c2","c2");
  c2->Divide(2,2);
  c2->cd(1);
  TH1D*h_div_nom_gp = (TH1D*)h_nom->Clone("Div_nom_gp");
  h_div_nom_gp->SetTitle("Nominal/GP");
  h_div_nom_gp->Divide(h_nom,h_gp);
  h_div_nom_gp->Draw();
  c2->cd(2);
  TH1D*h_div_nom_p1 = (TH1D*)h_nom->Clone("Div_nom_p1");
  h_div_nom_p1->SetTitle("Nominal+1s/Nominal");
  h_div_nom_p1->Divide(h_nomp1,h_nom);
  h_div_nom_p1->Draw();
  c2->cd(3);
  TH1D*h_div_nom_m1 = (TH1D*)h_nom->Clone("Div_nom_m1");
  h_div_nom_m1->SetTitle("Nominal-1s/Nominal");
  h_div_nom_m1->Divide(h_nomm1,h_nom);
  h_div_nom_m1->Draw();
  

  TFile*f_out = new TFile("Spectra.root","RECREATE");
  h_nom->Write("nominal");
  h_nomp1->Write("nominal+1s");
  h_nomm1->Write("nominal-1s");
  h_gp->Write("guineapig");
  
  f_out->Close();
  
}
