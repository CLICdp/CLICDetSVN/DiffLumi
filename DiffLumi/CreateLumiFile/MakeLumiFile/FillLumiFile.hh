// $Id: $
#ifndef CREATELUMIFILE_FILLLUMIFILE_HH 
#define CREATELUMIFILE_FILLLUMIFILE_HH 1

// Include files
#include <TRandom3.h>
#include "Math/QuantFuncMathCore.h"
#include "BetaFunction.h"
#include "BetaFunctionBS.h"

#include <TFile.h>
#include <TTree.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

/** @class FillLumiFile FillLumiFile.hh CreateLumiFile/FillLumiFile.hh
 *  
 *
 *  @author Stephane Poss
 *  @date   2011-11-01
 */
class FillLumiFile {
public: 
  /// Standard constructor
  FillLumiFile( ); 

  virtual ~FillLumiFile( ); ///< Destructor

  void SetParameters(double* params);
  void Loop(int numberofevents);
  void saveInFile(std::string fname);

  inline void setWrite(bool tree,bool histo) {m_writeTree = tree; m_writeHisto = histo;}
  

protected:

private:

  Double_t random;
  Double_t BeamEnergy;
  Double_t m_xb1, m_xb2, peak, arm1, arm2, body, x1, x2, m_dx1, m_dx2, m_dx1g, m_dx2g;
  Double_t m_E1,m_E2;
  Double_t m_RelativeProbability;
  Double_t BeamSpreadSmearing;
  

  TTree*m_t;
  
  BetaFunctionBS *m_b1bs,*m_b2bs,*m_b1bsArm,*m_b2bsArm;
  BetaFunction *m_b1betaArm,*m_b2betaArm,*m_b1betaBody,*m_b2betaBody;
  bool m_writeTree,m_writeHisto;
  
  TH1D*m_h_lumi;
  TH2D*m_h_lumi2D;
  
};
#endif // CREATELUMIFILE_FILLLUMIFILE_HH
