// $Id: $
// Include files



// local
#include "FillLumiFile.hh"

//-----------------------------------------------------------------------------
// Implementation file for class : FillLumiFile
//
// 2011-11-01 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FillLumiFile::FillLumiFile(  ) {
  gRandom = new TRandom3(0);
  m_t = new TTree("MyTree","MyTree");
  m_t->Branch("eb1",&m_xb1,"eb1/D");
  m_t->Branch("eb2",&m_xb2,"eb2/D");
  m_t->Branch("deb1",&m_dx1,"deb1/D");
  m_t->Branch("deb2",&m_dx2,"deb2/D");
  m_t->Branch("deb1_g",&m_dx1g,"deb1_g/D");
  m_t->Branch("deb2_g",&m_dx2g,"deb2_g/D");
  m_t->Branch("E1",&m_E1,"E1/D");
  m_t->Branch("E2",&m_E2,"E2/D");

  m_t->Branch("RelativeProbability",&m_RelativeProbability,"RelativeProbability/D");

  x1 = x2 = 0.;
  
  BeamEnergy = 1500.;

  BeamSpreadSmearing = 0.00013;

  m_RelativeProbability = 1;

  m_b1bs = m_b2bs = NULL;
  m_b1betaArm = m_b2betaArm = m_b1betaBody =m_b2betaBody = NULL;
  m_b1bsArm = m_b2bsArm = NULL;


  m_writeTree = m_writeHisto = true;

  m_h_lumi = new TH1D("lumi_spectrum","Lumi Spectrum",3060,-1.5,3058.5);
  m_h_lumi2D = new TH2D("lumi_spectrum_2d","Lumi Spectrum",1530,-0.75,1526.25,1530,-0.75,1526.25);
}
//=============================================================================
// Destructor
//=============================================================================
FillLumiFile::~FillLumiFile() {
  delete m_t;
  delete m_b1bs;
  delete m_b2bs;
  delete m_b1betaArm;
  delete m_b2betaArm;
  delete m_b1betaBody;
  delete m_b2betaBody;
  delete m_b1bsArm;
  delete m_b2bsArm;
  delete m_h_lumi;
  
}

//=============================================================================


//=========================================================================
//
//=========================================================================
void FillLumiFile::SetParameters (double*parameters ) {
  m_b1bs = new BetaFunctionBS(parameters[5],  parameters[6] , parameters[7],  parameters[8]  );
  m_b2bs = new BetaFunctionBS(parameters[11], parameters[12], parameters[13], parameters[14] );

  m_b1betaArm = new BetaFunction(parameters[3],  parameters[4]);
  m_b2betaArm  = new BetaFunction(parameters[9],  parameters[10]);
  m_b1betaBody = new BetaFunction(parameters[15], parameters[16]);
  m_b2betaBody = new BetaFunction(parameters[17], parameters[18]);

  m_b1bsArm = new BetaFunctionBS(parameters[20],  parameters[21], parameters[22], parameters[23] );
  m_b2bsArm = new BetaFunctionBS(parameters[24],  parameters[25], parameters[26], parameters[27] );

  peak = parameters[0];
  arm1 = parameters[1];
  arm2 = parameters[2];
  body = 1.- peak - arm1 - arm2;

}

//=========================================================================
//
//=========================================================================
void FillLumiFile::Loop (int nbevts ) {
  int counter =0;
  
  do {
    random = gRandom->Uniform(1.);
    m_RelativeProbability = 1;

    if(random > 0. && random <= peak) {
      x1 = 1.0;
      x2 = 1.0;
      counter++;
      m_RelativeProbability *= peak;
      //Now care for the beam spread
      m_dx1 = m_b1bs->GetRandom();
      m_dx2 = m_b2bs->GetRandom();
      m_RelativeProbability *= m_b1bs->evaluate(m_dx1)*m_b2bs->evaluate(m_dx2);
      //h1Beamspread.Fill( dx1+gRandom->Gaus(0,BeamSpreadSmearing) );
    } else if( random > peak  && random <= (peak + arm1)){

      x2 = m_b2betaArm->GetRandom();
      x1 = 1.0;
      counter++;
      m_RelativeProbability *= m_b2betaArm->evaluate(x2) * arm1;
      //Now care for the beam spread
      m_dx1 = m_b1bsArm->GetRandom();
      m_dx2 = m_b2bsArm->GetRandom();
      m_RelativeProbability *= m_b1bsArm->evaluate(m_dx1)*m_b2bsArm->evaluate(m_dx2);

    } else if ( random > (peak + arm1) && random <= (peak + arm1 + arm2)){

      x1 = m_b1betaArm->GetRandom();
      x2 = 1.0;
      counter++;
      m_RelativeProbability *= m_b1betaArm->evaluate(x1) * arm2;
      //Now care for the beam spread
      m_dx1 = m_b1bsArm->GetRandom();
      m_dx2 = m_b2bsArm->GetRandom();
      m_RelativeProbability *= m_b1bsArm->evaluate(m_dx1)*m_b2bsArm->evaluate(m_dx2);

    } else if (random > (peak + arm1 + arm2) && random < 1.) {

      x1 = m_b1betaBody->GetRandom();
      x2 = m_b2betaBody->GetRandom();
      counter++;
      m_RelativeProbability *= m_b1betaBody->evaluate(x1)*m_b2betaBody->evaluate(x2) * body;
      //Now care for the beam spread
      m_dx1 = m_b1bsArm->GetRandom();
      m_dx2 = m_b2bsArm->GetRandom();
      m_RelativeProbability *= m_b1bsArm->evaluate(m_dx1)*m_b2bsArm->evaluate(m_dx2);

    }//Finished BetaFunction parts


    m_xb1 = x1;
    m_xb2 = x2;

    Double_t m_dx1gauss = gRandom->Gaus(0,BeamSpreadSmearing);
    Double_t m_dx2gauss = gRandom->Gaus(0,BeamSpreadSmearing);

    //filter out events where the total energy is <0
    if( (m_xb2+m_dx2+m_dx2gauss)<0 || (m_xb1+m_dx1+m_dx1gauss)<0) {
      nbevts++;
      continue;
    }


    //    correctiondata->Fill();
    if(counter%10000==0) {
      std::printf("\r");//return to beginning of line
      //      std::cout << "Running:" << std::setw(10) << int(100*(double(counter)/numberOfEvents)) << "%";
      std::cout << "Running:" << std::setw(10) << counter ;
      std::fflush(stdout);
    }
    //if(writefile) {
    //  ofs << "   " << xb1*BeamEnergy << "   " << xb2*BeamEnergy
    //      << "   " << dx1*BeamEnergy <<"    " << dx2*BeamEnergy
    //      << "   " << (xb1+dx1)*BeamEnergy<< "   " << (xb2+dx2)*BeamEnergy
    //      << "   " << RelativeProbability << std::endl;
    //}
    m_xb1 = m_xb1*BeamEnergy;
    m_xb2 = m_xb2*BeamEnergy;
    m_dx1 = m_dx1*BeamEnergy;
    m_dx2 = m_dx2*BeamEnergy;
    m_dx1g = m_dx1gauss*BeamEnergy;
    m_dx2g = m_dx2gauss*BeamEnergy;
    m_E1 = m_xb1+m_dx1+m_dx1g;
    m_E2 = m_xb2+m_dx2+m_dx2g;
    m_t->Fill();
    m_h_lumi->Fill(TMath::Sqrt(4*m_E1*m_E2));
    m_h_lumi2D->Fill(m_E1,m_E2);
    

  } while(counter < nbevts);

}


//=========================================================================
//
//=========================================================================
void FillLumiFile::saveInFile (std::string fname ) {
  if (m_writeTree || m_writeHisto){
    TFile* f_out = TFile::Open(fname.c_str(),"RECREATE");
    if (m_writeTree)
      m_t->Write();
    if (m_writeHisto){
      m_h_lumi->Write();
      m_h_lumi2D->Write();
    }
    f_out->Close();
    delete f_out;
  }
}
