#include "BetaFunctionBS.h"
#include "Utilities.h"
#include "ConvolutedFunctions.hh"
#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <Math/Functor.h>
#include <Minuit2/Minuit2Minimizer.h>
#include <TCanvas.h>
#include <TError.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TMath.h>
#include <TRandom3.h>
#include <TVirtualFitter.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TTree.h>
#include <TStyle.h>
#include <TROOT.h>


#include <iostream>
#include <iomanip>


/***
 * Try to fit the beam energy spread coming from the linac simulation more
 * rigorously, instead of adjusting the gauss width by hand, we will give it as
 * a free parameter for minuit. Though we will have to fit the distribution with
 * a convolution of two functions
 *
 */

using Utility::Integrate;
using Utility::DrawConfidenceInterval;


void  Fit1500GeV(TString filename);
void  Fit250GeV(TString filename);
void  Fit175GeV(TString filename);

void Fit1500Chebyshev(TString filename="energySpreads.root");

int main (int argc, char **argv) {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  TString filename;
  int energy(0);

  //  Fit1500Chebyshev("bdsEnergySpreads.root");
  if(argc < 3){
    std::cerr << "Please provide beam energy (175, 250,  or 1500) and energySpread filename"  << std::endl;
  } else {
    energy = atoi(argv[1]);
    filename = argv[2];
  }

  //  gErrorIgnoreLevel=kWarning;

  if(energy == 1500) {
    Fit1500GeV(filename);
  } else if (energy == 250) {
    Fit250GeV(filename);
  } else if (energy == 175) {
    Fit175GeV(filename);
  } else {
    std::cerr << "Energy of " << energy << " is not known"  << std::endl;
    return 1;
  }


  //DrawSeparateHistograms();

  return 0;

}//Main


void Fit1500Chebyshev(TString filename) {

  // gStyle->SetOptFit(1111);
  // gStyle->SetOptStat(1111);
  gROOT->ForceStyle();
  
  TFile *ESFile = TFile::Open(filename);
  TH1D* bdsSpread = dynamic_cast<TH1D*>(ESFile->Get("hist"));
  TH1D* bdsSpread2 = dynamic_cast<TH1D*>(ESFile->Get("hist2"));


  if(not bdsSpread || not bdsSpread2 ) {
    std::cerr << "Histograms not found in file!"  << std::endl;
    exit(1);
  }

  bdsSpread->Rebin(5);
  bdsSpread->Scale( 1./bdsSpread->Integral("width") );
  bdsSpread->SetXTitle("x=#DeltaE/E_{Beam}");
  bdsSpread->SetYTitle("dN/dx");
  bdsSpread->SetTitle("Energy spread");
  bdsSpread->SetAxisRange(0, 540, "Y");
  bdsSpread2->Scale ( 1./bdsSpread2->Integral("width") );

  TH1D* oldSpread = dynamic_cast<TH1D*>(bdsSpread->Clone("clone"));


  TVirtualFitter::SetMaxIterations(100000);
  //  double xmin(bdsSpread->GetXaxis()->GetXmin()), xmax(bdsSpread->GetXaxis()->GetXmax());
  //  double xmin(-4.679e-3+0.1e-3), xmax(5.495e-3-0.1e-3);
  double xmin(bdsSpread->GetXaxis()->GetBinLowEdge(1)), xmax(bdsSpread->GetXaxis()->GetBinLowEdge(201));
  std::cout << xmin << "   " << xmax  << std::endl;
  std::cout << "Number of Bins: " << bdsSpread->GetNbinsX()  << std::endl;

#ifdef USE_RootStyle
  RootStyle::PDFFile myfile("hist_cheb3.pdf");
#endif

  std::vector<TGraphErrors*> errorGraphs;

  for(int n = 1; n <= 50; ++n ) {
  
    ConvolutedFunctions::Chebyshev cheb(n,xmin,xmax);

    TF1 chebFunction("chebFunc", &cheb, xmin, xmax, n, "Chebyshev" );
    chebFunction.SetNpx(1000);
    for (int i = 0; i <=n; ++i) {
      chebFunction.SetParameter(i,1);
    }
    std::cout << "*********************************************************************************************"  << std::endl;
    std::cout << "Fitting cheb degree: " << n  << std::endl;
    TFitResultPtr r = bdsSpread->Fit(&chebFunction,"S","");
    TMatrixDSym cov = r->GetCovarianceMatrix();  //  to access the covariance matrix
    //    r->Print("V");     // print full informatio
    r->Print();     // print full informatio
    
    
    TCanvas c("c","c");
    bdsSpread->Draw();
    TGraphErrors* fitCSlice50(DrawConfidenceInterval(chebFunction, 0.68));
    fitCSlice50->SetFillColor(kPink+2);
    fitCSlice50->SetLineColor(kRed-7);
    fitCSlice50->SetTitle("Fit + 99% C.L.");
    fitCSlice50->Draw("l3");


    TVirtualFitter::GetFitter()->PrintResults(2, 0.2);
    if ( n == 26 || n == 10 || n == 5 || n == 35 ) {
      c.SaveAs(Form("EnergySpreadChebyshev_%i.eps", n));
      fitCSlice50->SetTitle(Form("Chebyshev Degree %i",n));
      errorGraphs.push_back(dynamic_cast<TGraphErrors*>(fitCSlice50->Clone(Form("name%i",n))));
    }
#ifdef USE_RootStyle
    myfile.AddCanvas(&c, Form("Degree %i", n));
#endif
    
    
  }
#ifdef USE_RootStyle
    myfile.CloseFile();
#endif



#ifdef USE_RootStyle
  TCanvas cPers("cPers","Chebyshev fits to beam energy spread");
  oldSpread->Draw();
  RootStyle::MyColor colors; colors.GetColor();//take black out
  for (std::vector<TGraphErrors*>::iterator it = errorGraphs.begin(); it != errorGraphs.end(); ++it) {
    TGraphErrors* gr = *it;
    gr->Draw("l3");
    Color_t color = colors.GetColor();
    gr->SetLineColor(color);
    gr->SetMarkerColor(color);
    gr->SetFillColor(color);
  }
  TLegend* leg = RootStyle::BuildLegend(&cPers, 0.3, 0.9-5*0.0625, 0.6, 0.9);//(- 0.9 (* (/ 0.125 2) 5))
  //leg->SetMargin(0.16);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
  cPers.SaveAs("EnergySpreadChebyshev.eps");
#endif


// #ifdef NOTEVERDEFINED
//   }
// #endif
  return;    
}//Fit1500Chebyshev

  
//****************************************************************************************************

void  Fit1500GeV(TString filename) {

  //TFile *ESFile = TFile::Open("../CreateLumiFile/EnergySpreadHisto.root");
  TFile *ESFile = TFile::Open("energySpreads.root");
  TFile *ESFile2 = TFile::Open(filename);
  if ( not ESFile || not ESFile2 ){
    std::cerr << "File not found\n"
	      << filename << "\n"
	      << "../CreateLumiFile/EnergySpreadHisto.root"
	      << std::endl;
    exit(1);
  }
  TH1D* spreadHisto = dynamic_cast<TH1D*>(ESFile2->Get("hist"));
  TH1D* bdsSpread   = dynamic_cast<TH1D*>(ESFile2->Get("hist2"));
  if( not spreadHisto || not bdsSpread ) {
    std::cerr << "Histogram not found" << std::endl;
    exit(1);
  }
  spreadHisto->SetTitle("Energy spread");
  bdsSpread->SetTitle("Energy spread;x=#DeltaE/E_{Beam};dN/dx");
//spreadHisto->Rebin(5);
  bdsSpread->Rebin(5);
  spreadHisto->Scale( 1./spreadHisto->Integral("width") );
  bdsSpread->Scale( 1./bdsSpread->Integral("width") );

  const double maxYvalue(400);
  TCanvas c("c","c");
  //  spreadHisto->Fit(&esFunction,"","");
  spreadHisto->Draw();
  spreadHisto->SetTitle("Energy spread;x=#DeltaE/E_{Beam};dN/dx");
  spreadHisto->SetAxisRange(0, maxYvalue, "Y");
  {
#ifdef USE_RootStyle
    TLegend *legHisto2 = RootStyle::BuildLegend(c, 0.3, 0.9-0.0625, 0.6, 0.9);
    legHisto2->SetTextSize(0.06);
    legHisto2->SetBorderSize(0);
#endif
  }
  c.SaveAs("GPEnergySpread.eps");

  TF1 esFunction( ConvolutedFunctions::getESFunction( ConvolutedFunctions::getParameters() ));

  esFunction.SetNpx(1000);
  bdsSpread->Draw("hist");
  std::cout << "Fitting SpreadHisto2"  << std::endl;
  bdsSpread->Fit(&esFunction,"WL","");

  TGraphErrors* fitCSlice50(DrawConfidenceInterval(esFunction, 0.99));
  fitCSlice50->SetFillColor(kPink+2);
  fitCSlice50->SetLineColor(kRed-7);
  fitCSlice50->SetTitle("Fit + 99% C.L.");
  fitCSlice50->Draw("l3");

#ifdef USE_RootStyle
  TLegend *legHisto2 = RootStyle::BuildLegend(c, 0.3, 0.7, 0.9, 0.9);
  legHisto2->SetMargin(0.16);
  legHisto2->SetTextSize(0.06);
  legHisto2->SetBorderSize(0);
#endif

  c.SaveAs("GPEnergySpreadFit.eps");


  std::cout << "**********************************************************"  << std::endl;
  std::cout << "Fitting SpreadHisto2 (from the file argument)"  << std::endl;
  bdsSpread->Fit(&esFunction,"WL","");
  //GetTheConfidenceIntervalgraph
  TGraphErrors* fitC1(DrawConfidenceInterval(esFunction, 0.99));
  fitC1->SetFillColor(kGreen+2);
  fitC1->SetLineColor(kRed-7);
  fitC1->SetTitle("Fit + 99% C.L.");

  //  spreadHisto->SetYTitle("1/N dN/dE");
  // esFunction.Draw("same");
  // esFunction.SetLineColor(kRed-7);

  fitC1->Draw("l3");

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(c, 0.3, 0.9-2*0.0625, 0.6, 0.9);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
  double scale = RootStyle::AutoSetYRange(c, 1.0);
  RootStyle::AutoSetYRange(c, 540.0/scale);
  bdsSpread->Draw("");
  fitC1->Draw("l3");
  esFunction.Draw("samel");
  leg->Draw();
#endif
  //Now Redraw histogram to be above the error bands
  //  bdsSpread->SetLineColor(kBlue);
  //  bdsSpread->Draw("same");

  // beamspread.SetNpx(1000);
  // beamspread.Draw();

  // beamspread2.SetLineColor(kBlue);
  // beamspread2.SetNpx(1000);
  // //  beamspread2.Draw("same");
  // beamspread2.Draw();

  // betafunction.SetNpx(1000);
  // betafunction.Draw("same");
  //  bdsSpread->SetAxisRange(0, maxYvalue, "Y");

  c.SaveAs("FitWithConv.eps");


  //  return;

  TF1 beamspread("beamspread", ConvolutedFunctions::BeamSpread, -0.01, 0.01, 4);
  beamspread.SetParameters( ConvolutedFunctions::getParameters() );

  TF1 beamspread2("beamspread2", ConvolutedFunctions::BeamSpread2, -0.01, 0.01, 4);
  //  TF1 beamspread2("beamspread2",BeamSpread2, -0.1, 1.1, 4);
  beamspread2.SetParameters( ConvolutedFunctions::getParameters() );

  TF1 betafunction("betafunction", ConvolutedFunctions::bfFunction, -0.1,1.1, 2);
  // betafunction.SetParameter(0, 2);
  // betafunction.SetParameter(1, 10);

  //Just for testing purposes
  std::cout << "Function Integral " << Integrate(&esFunction, -0.01, 0.01, 0.00001)  << std::endl;
  std::cout << "BS Integral " << Integrate(&beamspread,       -0.01, 0.01, 0.00001)  << std::endl;
  std::cout << "BS2 Integral " << Integrate(&beamspread2,     -0.01, 0.01, 0.00001)  << std::endl;
  std::cout << "BF Integral " << Integrate(&betafunction, 0.0, 1.0,      0.000009)  << std::endl;

  //  TFile* newSpreadFile = TFile::Open(filename);
  TH1D *spreadNoLoss, *gpSpreadHisto;
  ESFile2->GetObject("GPESpread2", spreadNoLoss);
  ESFile2->GetObject("GPESpread", gpSpreadHisto);
  spreadNoLoss->SetXTitle("#DeltaE/E_{Beam}");
  gpSpreadHisto->SetXTitle("#DeltaE/E_{Beam}");
  spreadNoLoss->SetAxisRange(0, maxYvalue, "Y");

  std::cout << "**********************************************************"  << std::endl;


  // es2Function.SetParLimits(0, -0.6, -0.2);
  // es2Function.SetParLimits(1, -0.6,  0.0);
  // es2Function.SetParLimits(2, -0.005,   -0.004);
  // es2Function.SetParLimits(3,  0.005,    0.006);
  // es2Function.SetParLimits(4, 0.000011,   0.00017);

  esFunction.FixParameter(2,esFunction.GetParameter(2));
  esFunction.FixParameter(3,esFunction.GetParameter(3));
  esFunction.FixParameter(4,esFunction.GetParameter(4));

  TF1 es2Function( ConvolutedFunctions::getES2Function( ConvolutedFunctions::getParameters2() ));

  //Use (some of) the parameters we got from previous fit
  // es2Function.FixParameter(0,esFunction.GetParameter(0));
  // es2Function.FixParameter(1,esFunction.GetParameter(1));
  es2Function.FixParameter(2,esFunction.GetParameter(2));
  es2Function.FixParameter(3,esFunction.GetParameter(3));

  es2Function.FixParameter(6,esFunction.GetParameter(2));
  es2Function.FixParameter(7,esFunction.GetParameter(3));
  es2Function.FixParameter(8,esFunction.GetParameter(4));
  es2Function.FixParameter(9,0.65);


  c.Clear();
  //spreadNoLoss->Rebin(8);
  //  std::cout << "Smoothing"  << std::endl;
  //  spreadNoLoss->Smooth(1000);
  spreadNoLoss->SetTitle("GP w/o Beamstrahlung");
  spreadNoLoss->Scale(1./spreadNoLoss->Integral("width"));
  spreadNoLoss->Draw();

  gpSpreadHisto->SetTitle("GP with Beamstrahlung");
  gpSpreadHisto->Scale(1./gpSpreadHisto->Integral("width"));
  gpSpreadHisto->Draw("same");
  gpSpreadHisto->SetLineColor(kMagenta);

  esFunction.SetLineColor(kRed-7);
  //  esFunction.SetFillColor(kGreen+2);
  std::cout << "Fitting GP w/o energy loss"  << std::endl;
  spreadNoLoss->Fit(&esFunction, "WL");

  // std::cout << "**********************************************************"  << std::endl;
  // std::cout << "Now fitting the GP spectrum with ELOSS"  << std::endl;

  // gpSpreadHisto->Fit(&esFunction, "WL");
  // gpSpreadHisto->SetAxisRange(0, maxYvalue, "Y");


  //  TGraphErrors* fitC2(NULL);
  TGraphErrors *fitC2( DrawConfidenceInterval(esFunction, 0.99) );
  fitC2->SetTitle("Fit + 99% C.L.");
  fitC2->SetLineColor(kRed-7);
  fitC2->SetFillColor(kGreen+2);
  fitC2->Draw("3l");

  //  esFunction.Draw("same");
  spreadNoLoss->SetAxisRange(0, maxYvalue, "Y");
  spreadNoLoss->Draw("same");

#ifdef USE_RootStyle
  leg = RootStyle::BuildLegend(c, 0.3, 0.7, 0.9, 0.9);
  leg->SetMargin(0.16);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif
  spreadNoLoss->Draw("same");
  gpSpreadHisto->Draw("same");
#ifdef USE_Customroot
  RootStyle::AutoScaleYAxis(c, 1.2);
#endif
  c.SaveAs("NoElossWithConv.eps");

  std::cout << "**********************************************************"  << std::endl;

  c.Clear();
  //  spreadNoLoss->Rebin(4);
  //  spreadNoLoss->SetTitle("GP w/o Beamstrahlung");
  //  spreadNoLoss->Scale(1./spreadNoLoss->Integral("width"));
  spreadNoLoss->Draw();
  std::cout << "Fitting with linear combination"  << std::endl;
  TFitResultPtr fit1Ptr =   spreadNoLoss->Fit(&es2Function, "WLS");
  fit1Ptr->PrintCovMatrix(std::cout);
  // std::cout << "Fixing parameter 9, and fitting again"  << std::endl;
  // es2Function.FixParameter(9,es2Function.GetParameter(9));
  // TFitResultPtr fit2Ptr = spreadNoLoss->Fit(&es2Function, "WLS");
  // fit2Ptr->PrintCovMatrix(std::cout);

  TGraphErrors* fitC3(DrawConfidenceInterval(es2Function, 0.99));
  fitC3->SetTitle("Fit2 + 99% C.L.");
  fitC3->SetLineColor(kBlue);
  fitC3->SetFillColor(kCyan+1);

  fitC2->Draw("3l");
  fitC3->Draw("3l");

#ifdef USE_RootStyle
  leg = RootStyle::BuildLegend(c, 0.3, 0.7, 0.9, 0.9);
  leg->SetMargin(0.16);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif

  esFunction.Draw("same");
  es2Function.Draw("same");

  spreadNoLoss->Draw("same");

  spreadNoLoss->SetAxisRange(0, maxYvalue, "Y");
  c.SaveAs("NoElossWithConv2.eps");


  return;
  std::cout << "**********************************************************"  << std::endl;
  std::cout << "Fit Spectrum with eloss with single function and linear combination"  << std::endl;
  //Fit with normal function again
  TF1 singleFunc( ConvolutedFunctions::getESFunction( ConvolutedFunctions::getParameters() ) );
  //Use xmin, xmax and sigma from bdsFit
  singleFunc.FixParameter(2,esFunction.GetParameter(2));
  singleFunc.FixParameter(3,esFunction.GetParameter(3));
  singleFunc.FixParameter(4,esFunction.GetParameter(4));

  gpSpreadHisto->Fit(&singleFunc, "WL");

  TGraphErrors* fitSingle(DrawConfidenceInterval(singleFunc, 0.99));
  fitSingle->SetTitle("Fit + 99% C.L.");
  fitSingle->SetLineColor(kRed-7);
  fitSingle->SetFillColor(kGreen+2);

  TF1 doubleFunc( ConvolutedFunctions::getES2Function( ConvolutedFunctions::getParameters2() ) );
  doubleFunc.FixParameter(2,singleFunc.GetParameter(2));
  doubleFunc.FixParameter(3,singleFunc.GetParameter(3));
  doubleFunc.FixParameter(6,singleFunc.GetParameter(2));
  doubleFunc.FixParameter(7,singleFunc.GetParameter(3));
  doubleFunc.FixParameter(8,singleFunc.GetParameter(4));
  //  doubleFunc.FixParameter(9,0.65);

  gpSpreadHisto->Fit(&doubleFunc, "WL");

  TGraphErrors* fitDouble(DrawConfidenceInterval(doubleFunc, 0.99));
  fitDouble->SetTitle("Fit2 + 99% C.L.");
  fitDouble->SetLineColor(kBlue);
  fitDouble->SetFillColor(kCyan+1);

  TCanvas canvas3("canvas3","Fit linear combination");
  gpSpreadHisto->Draw();
  gpSpreadHisto->SetAxisRange(0, 250, "Y");
  fitSingle->Draw("3l");
  fitDouble->Draw("3l");


#ifdef USE_RootStyle
  leg = RootStyle::BuildLegend(canvas3, 0.3, 0.7, 0.9, 0.9);
  leg->SetMargin(0.16);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif


  singleFunc.Draw("same");
  doubleFunc.Draw("same");
  canvas3.SaveAs("FitSpreadWithDouble.eps");

  return;
}//Fit1500GeV




void Fit250GeV(TString filename) {

  TFile *ESFile = TFile::Open(filename);
  TH1D* spreadHisto = static_cast<TH1D*>(ESFile->Get("EnergySpread500GeV"));
  spreadHisto->SetTitle("E Spread E_{Beam} = 250 GeV");
  spreadHisto->Rebin(5);
  spreadHisto->Scale( 1./spreadHisto->Integral("width") );


  TF1 esFunction( ConvolutedFunctions::getESFunction( ConvolutedFunctions::getParameters() ));
  esFunction.SetParLimits(0, -0.7, -0.001);
  esFunction.SetParLimits(1, -0.7, -0.001);
  esFunction.SetParLimits(2, -0.002,   -0.000);
  esFunction.SetParLimits(3,  0.007,    0.009);
  esFunction.SetParLimits(4, 0.0005,   0.2);

  //  esFunction.FixParameter(2, -
  //  esFunction.SetParLimits(4, 0.000011,   0.001);



  // TF1 es2Function("Fit2",energySpread2,-0.01,0.01,10);
  // es2Function.SetLineColor(kGreen+2);
  // es2Function.SetNpx(1000);
  // es2Function.SetParameters(parameter2);
  // es2Function.SetParLimits(0, -0.6, -0.2);
  // es2Function.SetParLimits(1, -0.6, -0.2);
  // es2Function.SetParLimits(2, -0.005,   -0.004);
  // es2Function.SetParLimits(3,  0.005,    0.006);
  // //  es2Function.SetParLimits(4, -0.6, 1.0);
  // es2Function.SetParLimits(4, -0.6, 1.01);
  // es2Function.SetParLimits(5,  0.0, 10.0);
  // es2Function.SetParLimits(6, -0.005,   -0.004);
  // es2Function.SetParLimits(7,  0.005,    0.006);
  // es2Function.SetParLimits(8,  0.000011,   0.00017);
  // es2Function.SetParLimits(9,  0.0,   1.0);

  // es2Function.SetParName(0, "a1");
  // es2Function.SetParName(1, "a2");
  // es2Function.SetParName(2, "xmin");
  // es2Function.SetParName(3, "xmax");
  // es2Function.SetParName(4, "b1");
  // es2Function.SetParName(5, "b2");
  // es2Function.SetParName(6, "xmin2");
  // es2Function.SetParName(7, "xmax2");
  // es2Function.SetParName(8, "sigma");
  // es2Function.SetParName(9, "p");

  // es2Function.SetLineColor(kBlue);


  const double maxYvalue(250);
  TCanvas c("c","c");
  //  spreadHisto->Fit(&esFunction,"","");
  spreadHisto->Draw();
  spreadHisto->SetXTitle("x=#DeltaE/E_{Beam}");
  spreadHisto->SetAxisRange(0, maxYvalue, "Y");
  c.SaveAs("GPEnergySpread500.eps");

  esFunction.SetNpx(1000);

  std::cout << "**********************************************************"  << std::endl;
  std::cout << "Fitting SpreadHisto"  << std::endl;
  spreadHisto->Fit(&esFunction,"WL","");
  //GetTheConfidenceIntervalgraph
  TGraphErrors* fitC1(DrawConfidenceInterval(esFunction,0.99));
  fitC1->SetFillColor(kGreen+2);
  fitC1->SetLineColor(kRed-7);
  fitC1->SetTitle("Fit + 99% C.L.");

  fitC1->Draw("l3");

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(c, 0.2, 0.7, 0.9, 0.9);
  leg->SetMargin(0.16);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif
  //Now Redraw histogram to be above the error bands
  spreadHisto->Draw("same");
  spreadHisto->SetAxisRange(0, maxYvalue, "Y");
  c.SaveAs("FitWithConv500.eps");

  //  return 0;

  // //Use the paramters we got from previous fit
  // es2Function.FixParameter(0,esFunction.GetParameter(0));
  // es2Function.FixParameter(1,esFunction.GetParameter(1));
  // es2Function.FixParameter(2,esFunction.GetParameter(2));
  // es2Function.FixParameter(3,esFunction.GetParameter(3));

  // es2Function.FixParameter(6,esFunction.GetParameter(2));
  // es2Function.FixParameter(7,esFunction.GetParameter(3));
  // es2Function.FixParameter(8,esFunction.GetParameter(4));

  return;
}//Fit250GeV




void Fit175GeV(TString filename) {

  TFile *ESFile = TFile::Open(filename);
  TH1D* spreadHisto = static_cast<TH1D*>(ESFile->Get("hist0350"));
  spreadHisto->SetTitle("E Spread E_{Beam} = 175 GeV");
  spreadHisto->Rebin(5);
  spreadHisto->Scale( 1./spreadHisto->Integral("width") );


  TF1 esFunction( ConvolutedFunctions::getESFunction( ConvolutedFunctions::getParameters() ));
  esFunction.SetParLimits(0, -0.7, -0.001);
  esFunction.SetParLimits(1, -0.7, -0.001);
  esFunction.SetParLimits(2, -0.006,    0.000);
  esFunction.SetParLimits(3,  0.000,    0.009);
  esFunction.SetParLimits(4, 0.00005,   0.2);


  const double maxYvalue(250);
  TCanvas c("c","c");
  //  spreadHisto->Fit(&esFunction,"","");
  spreadHisto->Draw();
  spreadHisto->SetXTitle("x=#DeltaE/E_{Beam}");
  spreadHisto->SetAxisRange(0, maxYvalue, "Y");
  c.SaveAs("GPEnergySpread350.eps");

  esFunction.SetNpx(1000);

  std::cout << "**********************************************************"  << std::endl;
  std::cout << "Fitting SpreadHisto"  << std::endl;
  spreadHisto->Fit(&esFunction,"WL","");
  //GetTheConfidenceIntervalgraph
  TGraphErrors* fitC1(DrawConfidenceInterval(esFunction,0.99));
  fitC1->SetFillColor(kGreen+2);
  fitC1->SetLineColor(kRed-7);
  fitC1->SetTitle("Fit + 99% C.L.");

  fitC1->Draw("l3");

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(c, 0.2, 0.7, 0.9, 0.9);
  leg->SetMargin(0.16);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif
  //Now Redraw histogram to be above the error bands
  spreadHisto->Draw("same");
  spreadHisto->SetAxisRange(0, maxYvalue, "Y");
  c.SaveAs("FitWithConv350.eps");


  return;
}//Fit250GeV
