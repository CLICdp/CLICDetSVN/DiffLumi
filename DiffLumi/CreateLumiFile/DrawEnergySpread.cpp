#ifdef USE_RootStyle
#include <RootStyle.hh>
#include <TLegend.h>
#endif

#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>


int main () {
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  TFile *ESFile = TFile::Open("EnergySpreadHisto.root");
  TH1D *GPEsHisto = static_cast<TH1D*>(ESFile->Get("hist"));
  GPEsHisto->Rebin(5);
  //TH1D *GPEsHisto2 = dynamic_cast<TH1D*>(GPEsHisto->Clone("hist2"));

  TFile *ESFile2 = TFile::Open("energySpreads.root");
  TH1D* spreadHisto = static_cast<TH1D*>(ESFile2->Get("hist"));

  int nRebin=1;
  GPEsHisto  ->Rebin(2*nRebin);
  spreadHisto->Rebin(nRebin);


  GPEsHisto->SetTitle("Old;x=#DeltaE/E_{Beam};dN/dx");
  spreadHisto->SetTitle("50 Slices");

  GPEsHisto  ->Scale(1.0/ GPEsHisto  ->Integral("width"));
  spreadHisto->Scale(1.0/ spreadHisto->Integral("width"));

  {
  TCanvas c1("MCbeamspread","MC BeamSpread");
  GPEsHisto->SetLineColor(kGreen+2);
  //  h1Beamspread.SetLineColor(kRed);
  GPEsHisto->Draw("hist");
  //  spreadHisto->Draw("histsame");

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(c1, 0.3, 0.825, 0.7,0.9);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif
  c1.SaveAs("beamspreadOld.eps");
  }


  {
  TCanvas c1("MCbeamspread","MC BeamSpread");
  GPEsHisto->SetLineColor(kGreen+2);
  //  h1Beamspread.SetLineColor(kRed);
  GPEsHisto->Draw("hist");
  spreadHisto->Draw("histsame");

#ifdef USE_RootStyle
  TLegend *leg = RootStyle::BuildLegend(c1, 0.3, 0.75, 0.7,0.9);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
#endif
  c1.SaveAs("beamspreadOldNew.eps");
  }

  return 0;
}//main 
