#include "Utilities.h"
#include "DrawSpectra.hh"

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

void Draw500GeV();
void Draw350GeV();
void Draw3000GeV();
void DrawUncorrelated();
void MakeBeamEnergySpread();

int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  //DrawUncorrelated();

  Draw350GeV();
  // Draw500GeV();
  //Draw3000GeV();

  //MakeBeamEnergySpread();

  return 0;
}

void Draw350GeV() {

  TString file3000GeV(Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root");
  TString file0350GeV(Utility::GetWorkingDirectory()+"/RootFiles/Lumi350_Smeared_2.root");


  TString file3000BDS = Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root";
  TString file0350BDS = Utility::GetWorkingDirectory() + "/RootFiles/BDS350_Smeared_2.root";


  double smin(0.00), smax(1.05);
  int nbins(1000);

  TLegend *leg = NULL;
  TCanvas c1("c1","350 GeV vs 3 TeV");


  {

    bool scale = false;
    const double sminE=-0.0070*1.1;
    const double smaxE= 0.0070*1.1;

    TH1D sh(DrawBDS("hist3000",file3000BDS,"MyTree", 1500, sminE, smaxE, 100000000, true, scale=false));
    TH1D sh2(DrawBDS("hist0350",file0350BDS, "MyTree", 175, sminE, smaxE, 300000, true, scale=false));

    sh.Draw();
    sh2.SetLineColor(kGreen+2);
    sh2.Draw("same");
    c1.SaveAs("EnergySpread350.eps");

    TFile *file = TFile::Open("EnergySpread350.root","RECREATE");
    sh.Write();
    sh2.Write();

    file->Close();


  }


#ifdef USE_RootStyle
  {//wrap in brackets to limit scope

    TH1D GP3000GeV = DrawLumiSpectrum("GP3000GeV", file3000GeV ,smin, smax, 3000000,false, true, nbins);
    TH1D GP0350GeV = DrawLumiSpectrum("GP0350GeV", file0350GeV, smin, smax, 3000000,false, true, nbins);  
    GP3000GeV.SetTitle("CLIC     3  TeV; x=#sqrt{s}/#sqrt{s_{nom}};dN/dx");
    GP0350GeV.SetTitle("CLIC 350 GeV; x=#sqrt{s}/#sqrt{s_{nom}};dN/dx");

    std::vector<double> points = { 0.99, 0.90, 0.80, 0.70, 0.50 };
    std::cout << "********************************************************************************"  << std::endl;
    RootStyle::PrintCumulativeFractionAt( &GP3000GeV, points, false );
    std::cout << "********************************************************************************"  << std::endl;
    RootStyle::PrintCumulativeFractionAt( &GP0350GeV, points, false );
    std::cout << "********************************************************************************"  << std::endl;
    
    RootStyle::MyColor colors;
    GP3000GeV.Draw("hist,");
    GP0350GeV.Draw("hist,same");
    c1.SetLogy();
    GP3000GeV.SetLineColor( colors.GetColor() );
    GP0350GeV.SetLineColor( colors.GetColor() );
    RootStyle::EqualiseHistogramPeak( c1 );
    RootStyle::AutoSetYRange(c1, 1.4);
    GP3000GeV.SetAxisRange(1e-3, 1e2,"Y");
    leg = RootStyle::BuildLegend(c1, 0.2, 0.9-2*0.0625, 0.5, 0.9);
    leg->SetTextSize(0.06);
    leg->SetBorderSize(0);
    c1.SaveAs("Lumi350GeV_Full.eps");
  }

  {//wrap in brackets to limit scope
    c1.SetLogy(0);

    TH1D GP3000GeV = DrawLumiSpectrum("GP3000GeV", file3000GeV ,smin, smax, 3000000,false, true, nbins);
    TH1D GP0350GeV = DrawLumiSpectrum("GP0350GeV", file0350GeV, smin, smax, 3000000,false, true, nbins);  
    GP3000GeV.SetTitle("CLIC     3  TeV; x=#sqrt{s}/#sqrt{s_{nom}};dN/dx");
    GP0350GeV.SetTitle("CLIC 350 GeV; x=#sqrt{s}/#sqrt{s_{nom}};dN/dx");

    RootStyle::MyColor colors;
    GP3000GeV.Draw("hist,");
    GP0350GeV.Draw("hist,same");
    GP3000GeV.SetLineColor( colors.GetColor() );
    GP0350GeV.SetLineColor( colors.GetColor() );
    RootStyle::EqualiseHistogramPeak( c1 );
    RootStyle::AutoSetYRange(c1, 1.4);
    GP3000GeV.SetAxisRange(1e-3, 1e2,"Y");
    leg = RootStyle::BuildLegend(c1, 0.2, 0.9-2*0.0625, 0.5, 0.9);
    leg->SetTextSize(0.06);
    leg->SetBorderSize(0);
    c1.SaveAs("Lumi350GeV_Full_Linear.eps");
  }
  
  {//wrap in brackets to limit scope
    c1.SetLogy(0);

    TH1D GP3000GeV = DrawLumiSpectrum("GP3000GeV", file3000GeV, 0.99, 1.01, 3000000,false, true, 300);
    TH1D GP0350GeV = DrawLumiSpectrum("GP0350GeV", file0350GeV, 0.99, 1.01, 3000000,false, true, 300);  
    GP3000GeV.SetTitle("CLIC     3  TeV; x=#sqrt{s}/#sqrt{s_{nom}};dN/dx");
    GP0350GeV.SetTitle("CLIC 350 GeV; x=#sqrt{s}/#sqrt{s_{nom}};dN/dx");
    RootStyle::MyColor colors;
    GP3000GeV.SetLineColor( colors.GetColor() );
    GP0350GeV.SetLineColor( colors.GetColor() );


    GP3000GeV.Draw("hist,");
    GP0350GeV.Draw("hist,same");

    RootStyle::EqualiseHistogramPeak( c1 );
    RootStyle::AutoSetYRange(c1, 1.4);

    leg = RootStyle::BuildLegend(c1, 0.2, 0.9-2*0.0625, 0.5, 0.9);
    leg->SetTextSize(0.06);
    leg->SetBorderSize(0);

    c1.SaveAs("Lumi350GeV_Peak.eps");
  }


  {
    TCanvas c2d("c2d","Lumi 350 GeV");
    c2d.SetRightMargin(0.16);
    c2d.SetLogz(1);

    TH2D lumi2d(DrawLumi2D("Lumi2D", file0350GeV, 0.97, 1.01, 300000000, false, false));
    lumi2d.Draw("colz");
    //    lumi2d.SetAxisRange(1, 1e4, "Z");
    lumi2d.SetXTitle("E_{1}/E_{Beam}");
    lumi2d.SetYTitle("E_{2}/E_{Beam}");
    
    c2d.SaveAs("Lumi350GeV_2D.eps");

  }

#endif



  return;
}//Draw350GeV

void DrawUncorrelated() {

  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root";
  //TString fileWithELoss  = "/afs/cern.ch/work/s/sailer/GP/NewSlices3/Lumi/LumiGP_NewSlices_12345.root";
  //TString fileWithELoss  = "/data/sailer/temp/LumiGP_NewSlices_12345.root";
  TCanvas c1("c1","Uncorrelated");
  double smin(0.990), smax(1.02);
  int nbins(1000);
  TH1D GPHisto  = DrawLumiSpectrum("GP", fileWithELoss ,smin, smax, 300000000,false, true, nbins);
  TH1D GPHistoUnc  = DrawLumiSpectrum("Uncorrelated", fileWithELoss, smin, smax, 300000000,false, false, nbins);  

  TLegend *leg = NULL;
  GPHisto.SetXTitle("x=#sqrt{s'}/#sqrt{s_{nom}}");
  GPHisto.SetYTitle("1/N dN/dx");
  GPHisto.    SetTitle("GuineaPig");
  GPHistoUnc. SetTitle("Uncorrelated");

  GPHisto.   Draw();
  GPHistoUnc.Draw("same");

  //c1.SetLogy();

  GPHisto.SetAxisRange(0.99, 1.01, "X");
  GPHisto.SetAxisRange(0, 250, "Y");


#ifdef USE_RootStyle
  {//wrap in brackets to limit scope
    RootStyle::MyColor colors;
    GPHisto.    SetLineColor( colors.GetColor() );
    GPHistoUnc. SetLineColor( colors.GetColor() );
    //RootStyle::AutoSetYRange(c1, 10.5);
    RootStyle::AutoSetYRange(c1, 1.1);
    leg = RootStyle::BuildLegend(c1, 0.5, 0.9-2*0.0625, 0.8, 0.9);
    leg->SetTextSize(0.06);
    leg->SetBorderSize(0);
  }
#endif
  c1.SaveAs("Uncorrelated.eps");

  //Ratio
  {
    GPHistoUnc.Add(&GPHisto, -1);
    GPHistoUnc.Divide(&GPHisto);
    GPHistoUnc.Draw();
    c1.SetLogy(0);
    c1.SaveAs("UncorrelatedRatio.eps");
  }

  {
    //uses bins = 
    TCanvas c2("c2","c2");
    c2.SetRightMargin(0.16);
    TH2D zoom2d_3 = DrawLumi2D("Unc2d_3", fileWithELoss, 0.97, 1.01, 300000000, false, false);
    zoom2d_3.Draw("colz");
    c2.SetLogy(0);
    c2.SetLogz(1);
    zoom2d_3.SetXTitle("E_{1}/E_{Beam}");
    zoom2d_3.SetAxisRange(1, 1e4, "Z");
    zoom2d_3.SetYTitle("E_{2}/E_{Beam}");
    c2.SaveAs("Uncorrelated_2d_zoom_3.eps");
  }

  {
    TCanvas c2("c2","c2");
    c2.SetRightMargin(0.16);
    TH2D zoom2d_1 = DrawLumi2D("Unc2d_1", fileWithELoss, 0.0, 1.01, 300000000, false, false);
    zoom2d_1.Draw("colz");
    c2.SetLogy(0);
    c2.SetLogz(1);
    zoom2d_1.SetXTitle("E_{1}/E_{Beam}");
    zoom2d_1.SetAxisRange(1e-3, 1e3, "Z");
    zoom2d_1.SetYTitle("E_{2}/E_{Beam}");
    c2.SaveAs("Uncorrelated_2d_zoom_1.eps");
  }


  {
    //uses bins = 
    TCanvas c2("c2","c2");
    c2.SetRightMargin(0.16);
    TH2D zoom2d_2 = DrawLumi2D("Unc2d_2", fileWithELoss, 0.5, 1.01, 300000000, false, false);
    zoom2d_2.Draw("colz");
    c2.SetLogy(0);
    c2.SetLogz(1);
    zoom2d_2.SetXTitle("E_{1}/E_{Beam}");
    zoom2d_2.SetAxisRange(1e-3, 1e3, "Z");
    zoom2d_2.SetYTitle("E_{2}/E_{Beam}");
    c2.SaveAs("Uncorrelated_2d_zoom_2.eps");
  }

  {
    //uses bins = 
    TCanvas c2("c2","c2");
    c2.SetRightMargin(0.16);
    TH2D zoom2d_3 = DrawLumi2D("zoom2d", fileWithELoss, 0.97, 1.01, 300000000, false, true);
    zoom2d_3.Draw("colz");
    c2.SetLogy(0);
    c2.SetLogz(1);
    zoom2d_3.SetXTitle("E_{1}/E_{Beam}");
    zoom2d_3.SetAxisRange(1e-3, 1e3, "Z");
    zoom2d_3.SetYTitle("E_{2}/E_{Beam}");
    c2.SaveAs("GP_2d_zoom_2.eps");
  }

  {
    //uses bins = 
    TCanvas c2("c2","c2");
    c2.SetRightMargin(0.16);
    TH2D zoom2d_3 = DrawLumi2D("zoom2d", fileWithELoss, 0.0, 1550.0/1500.0, 300000000, false, true);
    zoom2d_3.SetAxisRange(0.97, 1.01, "X");
    zoom2d_3.SetAxisRange(0.97, 1.01, "Y");
   
    zoom2d_3.Draw("colz");
    c2.SetLogy(0);
    c2.SetLogz(1);
    zoom2d_3.SetXTitle("E_{1}/E_{Beam}");
    zoom2d_3.SetYTitle("E_{2}/E_{Beam}");
    zoom2d_3.Scale( 1./ zoom2d_3.GetMaximum() );
    zoom2d_3.SetAxisRange(1e-5, 1.1, "Z");

    c2.SaveAs("GP_2d_zoom_3.eps");
  }
  return;



}





void Draw500GeV() {

  //  TString fileGPInput = Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root";
  TString fileGPInput = Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root";

  TH1D hEnergy(DrawBDS("EnergySpread500GeV", Utility::GetWorkingDirectory()+"/RootFiles/bds500.root", "bds500", 250.0, -0.01, 0.011, 300100, true));

  //  TH1D hEnergy1500(DrawBDS("EnergySpread3000GeV", Utility::GetWorkingDirectory()+"/RootFiles/bdsElectrons.root", "bds1500", 1500.0, -0.01, 0.011, 300100, true));
  //  TH1D hEnergy1500(DrawBDS("hist","/data/sailer/Files/BDSafs/ElectronsAFS.root", "eleTree", 1500, -0.01, 0.011, 300000, true));
  TH1D hEnergy1500(DrawBDS("hist", fileGPInput, "MyTree", 1500, -0.01, 0.011, 300000, true));

  {
    TCanvas canv("canv","canv");
    hEnergy.Draw();
    hEnergy1500.Draw("same");
    canv.SaveAs("EnergySpread500GeV.eps");
  }

  {
    TCanvas canv("canv","canv");
    hEnergy1500.Draw();
    canv.SaveAs("EnergySpread3000GeV.eps");
  }



  TFile *file = TFile::Open("ESpread500.root","RECREATE");
  hEnergy.Write();
  hEnergy1500.Write();
  file->Close();

  return;
}//Draw500GeV


void Draw3000GeV() {
  TCanvas c1("c1","c1");

  Double_t smin = 0.99, smax = 1.01;

  //TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/LumiFileGP_Slices50_Random.root";
  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root";
  //  TString fileWithELoss2 = Utility::GetWorkingDirectory()+"/RootFiles/LumiFileGP.root";


  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Full Range Lumi Plot
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  TH1D GPESpreadLong = DrawLumiSpectrum("GPESpreadLong", fileWithELoss, 0.0, 1.1, 300000000, false);


  c1.Clear();
  c1.SetLogy(1);
  GPESpreadLong.Scale( 1.0 / GPESpreadLong.Integral() );
  GPESpreadLong.Draw("hist");
  GPESpreadLong.SetXTitle("x=#sqrt{s'}/#sqrt{s_{nom}}");
  GPESpreadLong.SetYTitle("1/N dN/dx");
  c1.SaveAs("LumiFullRange.eps");
  c1.SetLogy(0);

  // TFile *outFile = TFile::Open("GPSpectrumForSmuonfit.root","RECREATE");
  // GPESpreadLong.SetName("hSqrts");
  // GPESpreadLong.Write();
  // outFile->Write();
  // outFile->Close();
  //return;
  ////////////////////////////////////////////////////////////////////////////////////////////////////



  TH1D GPHisto  = DrawLumiSpectrum("GP", fileWithELoss ,smin, smax, 300000000,false);
  TH1D GPHistoUnc  = DrawLumiSpectrum("Uncorrelated", fileWithELoss, 0.5, smax, 300000000,false, false);  

  // TH1D GPHisto2     = DrawLumiSpectrum("GPRandom",           fileWithELoss2, smin, smax, 30000000,false);
  // TH1D GPHistoUnc2  = DrawLumiSpectrum("UncorrelatedRandom", fileWithELoss2, smin, smax, 30000000,false, false);  

  // TH1D GPHisto  = DrawLumiSpectrum("GP", "../LumiFileGP.root",smin, smax, 3000000);
  // TH1D GPHistoUnc  = DrawLumiSpectrum("Uncorrelated", "../LumiFileGP.root",smin, smax, 3000000,true, false);  

  // TH1D MCHisto = DrawLumiSpectrum("MC","LumiFile_2_MC10M.root",smin, smax);
  // TH1D MCHisto2 = DrawLumiSpectrum("MC2","LumiFileMC_3.root",smin, smax);
  // TH1D MCHisto = DrawLumiSpectrum("MC","LumiFileMC_4.root",smin, smax);
  // TH1D MCHisto5 = DrawLumiSpectrum("MC5","LumiFileMC_5.root",smin, smax);
  // TH1D MCHisto6 = DrawLumiSpectrum("MC6","LumiFileMC_6.root",smin, smax);


  TLegend *leg = NULL;
  c1.Clear();
  GPHisto.SetXTitle("#sqrt{s'}/#sqrt{s}");
  GPHisto.    SetTitle("GuineaPig");

  //  TString fileNoEloss  = Utility::GetWorkingDirectory() + "/RootFiles/LumiNoEloss_Slices50_Random.root";
  TString fileNoEloss = Utility::GetWorkingDirectory() + "/RootFiles/DoubleLumiGP_NoELoss.root";
  TString fileGPInput = Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root";

  smin -= 1.0; smax -= 1.0;
  TH1D GPESpread     = DrawEnergySpread("GPESpread", fileWithELoss,smin, smax, 300000000, false);
  TH1D GPESpread2    = DrawEnergySpread("GPESpread2", fileNoEloss,smin, smax, 300000000, false);

  // TH1D GPESpreadD  = DrawEnergySpread("GPESpreadD", fileWithELossD,smin, smax, 300000000, false);
  // TH1D GPESpread2D = DrawEnergySpread("GPESpread2D", fileNoElossD,smin, smax, 300000000, false);

  TH1D MCESpread   = DrawEnergySpread("MCESpread", "../RootFiles/LumiFileMC_Take_16.root",smin, smax, 300000000, false);

  TH1D GPHisto2 = DrawLumiSpectrum("GP2",fileNoEloss,smin, smax, 30000000, true);
  TH1D MCHisto  = DrawLumiSpectrum("MC", "../RootFiles/LumiFileMC_Take_16.root",smin, smax, 300000000, false);



  c1.Clear();

  GPHisto.Draw();
  GPHisto2.Draw("same");
  GPHisto2.SetLineColor(kRed-7);

  MCHisto.SetLineColor(kRed);
  MCHisto.Draw("same");

  c1.SaveAs("LumiSpectra.eps");

  GPESpread.SetXTitle("#DeltaE/E");

  GPESpread .SetTitle("GP with Eloss");
  GPESpread2.SetTitle("GP without Eloss");

  // GPESpreadD .SetTitle("GPD with Eloss");
  // GPESpread2D.SetTitle("GPD without Eloss");

  // GPESpreadD .SetLineColor(kOrange);
  // GPESpread2D.SetLineColor(kOrange+4);

  //  GPESpread2.Draw("same");
  GPESpread2.SetLineColor(kRed-7);
  //  MCESpread.SetLineColor(kRed);
  //  MCESpread.Draw("same");

  //  TFile *ESFile = TFile::Open("../CreateLumiFile/EnergySpreadHisto.root");
  //  TH1D* spreadHisto = static_cast<TH1D*>(ESFile->Get("hist"));
  //  spreadHisto->Sumw2();
  double sminE=-0.0046*1.1;
  double smaxE= 0.0054*1.1;

  TH1D sh(DrawBDS("hist",fileGPInput,"MyTree", 1500, sminE, smaxE, 100000000, true));
  //  TH1D sh2(DrawBDS("hist2","/data/sailer/Files/BDSafs/ElectronsAFS.root", "eleTree", 1500, sminE, smaxE, 100000000, true));
  TH1D sh2(DrawBDS("hist2",Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root", "MyTree", 1500, sminE, smaxE, 300000, true));

  TH1D* spreadHisto(&sh);
  TH1D* spreadHisto2(&sh2);
  spreadHisto->SetTitle("GP Input");
  //  spreadHisto->Rebin(1);
  spreadHisto->Scale(1./spreadHisto->Integral("width"));
  spreadHisto2->Scale(1./spreadHisto2->Integral("width"));


  spreadHisto->SetLineColor(kGreen+2);


  // spreadHisto2->SetLineColor(kBlue);
  // spreadHisto2->SetTitle("BDS AFS");
  // //spreadHisto2->Draw("same");

#ifdef USE_RootStyle
 {//wrap in brackets to limit scope
  RootStyle::MyColor colors;
  RootStyle::SetAllColors( spreadHisto, colors.GetColor() );
  RootStyle::SetAllColors( &GPESpread , colors.GetColor() );
  RootStyle::SetAllColors( &GPESpread2, colors.GetColor() );
 }
#endif

  spreadHisto->SetTitle("GP Input");
  GPESpread.   SetTitle("GP with Beamstrahlung");
  GPESpread2.  SetTitle("GP without Beamstrahlung");

  spreadHisto->Draw("hist,");
  GPESpread.   Draw("hist,same");
  GPESpread2.  Draw("hist,same");

  int nRebin = 10;
  spreadHisto->Rebin(nRebin);
  GPESpread.   Rebin(nRebin);
  GPESpread2.  Rebin(nRebin);

  // GPESpreadD.   Draw("hist,same");
  // GPESpread2D.  Draw("hist,same");



#ifdef USE_RootStyle
  RootStyle::AutoSetYRange(c1, 1.1);
  leg = RootStyle::BuildLegend(c1, 0.3, 0.7, 0.9, 0.9);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.05);
  leg->SetMargin(0.13);
#endif
  c1.SaveAs("EnergySpreads.eps");

//////////////////////////////////////////////////
// Draw the two things from the bds files again //
//////////////////////////////////////////////////
  spreadHisto->Draw();
  spreadHisto2->Draw("same");

  std::cout << spreadHisto2->Integral()  << std::endl;

#ifdef USE_RootStyle
  RootStyle::AutoSetYRange(c1, 1.1);
  leg = RootStyle::BuildLegend(c1, 0.5, 0.7, 0.9, 0.9);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.05);
#endif
  spreadHisto->Draw("same");
  c1.SaveAs("EnergySpreadsBDS.eps");

  c1.SetRightMargin(0.16);
  c1.SetLogz();

  
  TH2D h2GP000(DrawLumi2D("h2GP000", fileWithELoss, 0.0, 1.01, 30000000,false));
  h2GP000.Draw("colz");
  h2GP000.SetXTitle("E_{1}/E_{Beam}");
  h2GP000.SetAxisRange(1e-3, 1e3, "Z");
  h2GP000.SetYTitle("E_{2}/E_{Beam}");
  h2GP000.GetXaxis()->SetNdivisions(502);
  h2GP000.GetYaxis()->SetNdivisions(502);
  c1.SaveAs("GP_2d_000.eps");
  

  
  TH2D h2GP050(DrawLumi2D("h2GP050", fileWithELoss, 0.5, 1.01, 30000000,false));
  h2GP050.Draw("colz");
  h2GP050.SetXTitle("E_{1}/E_{Beam}");
  h2GP050.SetYTitle("E_{2}/E_{Beam}");
  h2GP050.GetXaxis()->SetNdivisions(502);
  h2GP050.GetYaxis()->SetNdivisions(502);
  c1.SaveAs("GP_2d_050.eps");
  
  
  TH2D h2GP_Zoom(DrawLumi2D("h2GP_099", fileWithELoss, 0.99, 1.01, 30000000,false));
  h2GP_Zoom.Draw("colz");
  h2GP_Zoom.SetXTitle("E_{1}/E_{Beam}");
  h2GP_Zoom.SetYTitle("E_{2}/E_{Beam}");
  h2GP_Zoom.GetXaxis()->SetNdivisions(502);
  h2GP_Zoom.GetYaxis()->SetNdivisions(502);
  c1.SaveAs("GP_2d_099.eps");
  

  TFile *file = TFile::Open("energySpreads.root","RECREATE");

  GPESpread .Write();
  GPESpread2.Write();  
  MCESpread .Write();
  spreadHisto->Write();
  spreadHisto2->Write();
  h2GP000.Write();
  h2GP050.Write();
  h2GP_Zoom.Write();
  GPESpreadLong.Write();

  GPHisto .Write();
  GPHisto2.Write();  
  MCHisto .Write();

  file->Close();


  return;
}



void MakeBeamEnergySpread(){
  double sminE=-0.0046*1.1;
  double smaxE= 0.0054*1.1;
  bool scale = false;
  TString fileGPInput = Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root";
  TH1D sh(DrawBDS("hist",fileGPInput,"MyTree", 1500, sminE, smaxE, 100000000, true, scale=false));
  //  TH1D sh2(DrawBDS("hist2","/data/sailer/Files/BDSafs/ElectronsAFS.root", "eleTree", 1500, sminE, smaxE, 100000000, true));
  TH1D sh2(DrawBDS("hist2",Utility::GetWorkingDirectory() + "/RootFiles/bdsElePos_3000GeV_Slices50.root", "MyTree", 1500, sminE, smaxE, 300000, true, scale=false));


  TFile *file = TFile::Open("bdsEnergySpreads.root","RECREATE");
  sh.Write();
  sh2.Write();

  file->Close();



  return;
}
