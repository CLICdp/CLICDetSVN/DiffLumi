#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

using namespace std;

ofstream of;

void WriteParticle(int stat, float vp[4], int dau1, int dau2, float mass, int pid);

int main(int argc, char* argv[]){
  if(argc<3){
    cerr << " Syntax: hepevt2HEPEvt <Root-Tree-File> <Filename_>" << endl;
    return 0;
  } else {
    TFile *input = TFile::Open(argv[1]);
    TTree *ntuple = (TTree*)input->Get("h998");
    //    TTree *photons = (TTree*)input->Get("h999");


    int NoE=0;
    int NoOfParticles;
    float bp[4], bq[4];
    float eb1,eb2;
    double mass = 0.00051099891;
    int nphotons;
    float photdata[100][4];

    ntuple->SetBranchAddress("nphotons",&nphotons);
    ntuple->SetBranchAddress("photdata",&photdata);
    ntuple->SetBranchAddress("eb1",&eb1);
    ntuple->SetBranchAddress("eb2",&eb2);
    ntuple->SetBranchAddress("bpx2",&bp[0]);
    ntuple->SetBranchAddress("bpy2",&bp[1]);
    ntuple->SetBranchAddress("bpz2",&bp[2]);
    ntuple->SetBranchAddress("bqx2",&bq[0]);
    ntuple->SetBranchAddress("bqy2",&bq[1]);
    ntuple->SetBranchAddress("bqz2",&bq[2]);

    //   ntuple->SetBranchAddress("bqe2",&bqe2);
    //   ntuple->SetBranchAddress("bpe2",&bpe2);

    int nevents = ntuple->GetEntries();
    //    nevents = 200000;
    int evtperfile = 10000;
    string filename;
    for(int j = 0; j < nevents; j++) {
      if(j%evtperfile == 0) {
	if(j > 0) { 
	  of.close();
	  cout << ":  Wrote " << NoE << " Events" <<  endl;
	}
	char dummy[200] = "";
	char* datei0 = argv[2];
	char* dateiname0 = Form("%i.HEPEvt",j/evtperfile);
	strcat(dummy,datei0);
	if(j/evtperfile < 10) {
	  strcat(dummy,"0000");
	} else if(j/evtperfile < 100) {
	  strcat(dummy,"000");
	} else if(j/evtperfile < 1000) {
	  strcat(dummy,"00");
	} else if(j/evtperfile < 10000) {
	  strcat(dummy,"0");
	}


	strcat(dummy,dateiname0);
	of.open(dummy);
	cout << "Writing to file " << dummy;
	NoE=0;
      }

      ntuple->GetEntry(j);
      //ntuple->GetEntry(j);

      //take care of the input particles
      float p1 = TMath::Sqrt((eb1*eb1)-(mass*mass));
      float p2 = TMath::Sqrt((eb2*eb2)-(mass*mass));
      float bp1[3] = {0,0,p1};
      float bp2[3] = {0,0,-p2};
      
      NoOfParticles = 2 + 2 + nphotons;

      of.width(3);
      of << right << NoOfParticles << endl;
      
      WriteParticle(3, bp1, 3, 4, mass, 11);
      WriteParticle(3, bp2, 3, 4, mass, -11);

      WriteParticle(1, bp, 0, 0, mass, 11);
      WriteParticle(1, bq, 0, 0, mass, -11);

      for(int i=0; i < nphotons; i++){
	WriteParticle(1, photdata[i], 0, 0, double(0.), 22);
      }
      NoE++;


    }
    cout << ":  Wrote " << NoE << " Events" <<  endl;
    of.close();



    return 0;
  }
}


void WriteParticle(int stat, float vp[4], int dau1, int dau2, float mass, int pid) {
  of.width(10);
  of << stat; //Particle in Final State, must be one for generator Status
  of.width(10);
  of << pid;//Particle ID -11, 11, 22
  of.width(10);
  of << dau1;//All Particles 
  of.width(10);
  of << dau2;//are immortal
  of.width(16);
  of << scientific << setprecision (8) << vp[0];
  of.width(16);
  of << scientific << setprecision (8) << vp[1];
  of.width(16);
  of << scientific << setprecision (8) << vp[2];
  of.width(16);
  of << scientific << setprecision (8) << mass;
  of << endl;
}
