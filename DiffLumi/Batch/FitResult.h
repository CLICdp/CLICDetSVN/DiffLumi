//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov  1 15:12:38 2011 by ROOT version 5.30/00
// from TTree FitResult/FitResult
// found on file: ../../Fit/final_BHWide.root
//////////////////////////////////////////////////////////

#ifndef FitResult_h
#define FitResult_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <string>
class FitResult {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Declaration of leaf types
  Int_t           n_params;
  Double_t        Param[28][2];   //[n_params]
  //Double_t        Correlations[28][28];   //[n_params]
  std::vector<std::vector<double> > *Correlations;

  std::vector<std::string>  *ParamName;
  Double_t        edm;

  // List of branches
  TBranch        *b_n_params;   //!
  TBranch        *b_Param;   //!
  TBranch        *b_Correlations;   //!
  TBranch        *b_ParamName;   //!
  TBranch        *b_edm;   //!

  FitResult(TTree *tree=0);
  virtual ~FitResult();
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef FitResult_cxx
FitResult::FitResult(TTree *tree)
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../Fit/final_BHWide.root");
    if (!f || !f->IsOpen()) {
      f = new TFile("../../Fit/final_BHWide.root");
    }
    f->GetObject("FitResult",tree);

  }
  Init(tree);
}

FitResult::~FitResult()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t FitResult::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t FitResult::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void FitResult::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  Correlations = 0;
  ParamName = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("n_params", &n_params, &b_n_params);
  fChain->SetBranchAddress("Param", Param, &b_Param);
  fChain->SetBranchAddress("Correlations", &Correlations, &b_Correlations);
  fChain->SetBranchAddress("ParamName", &ParamName, &b_ParamName);
  fChain->SetBranchAddress("edm", &edm, &b_edm);
  Notify();
}

Bool_t FitResult::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void FitResult::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
#endif // #ifdef FitResult_cxx
