#include "FitResult.h"

// local
#include "FillLumiFile.hh"
#include "ModelSeparate.hh"
#include "ModelOverlap.hh"
#include <iostream>
#include <sstream>
#include <log4cplus/configurator.h>
#include <log4cplus/logger.h>
#include "Utilities.h"
using namespace log4cplus;

int main(int argc, const char* argv[] )
{
  ConfigureAndWatchThread configureThread(LOG4CPLUS_TEXT(Utility::GetWorkingDirectory()+"/Logger/log4cplus.properties"), 5 * 1000);


  TString filename("");
  std::string model;
  if (argc<5){
    std::cout<<"Not enough parameters"<<std::endl;
    return 0;
  }
  
  filename = TString(argv[1]);
  model = argv[2];
  int i_par = atoi(argv[3]);
  int factor = atoi(argv[4]);

  Long_t numberOfEvents = 50000000;
  TFile*f_in = TFile::Open(filename);
  TTree*t_in = (TTree*)f_in->Get("FitResult");
  FitResult fr(t_in);
  fr.GetEntry(0);
  double p[fr.n_params];
  for (int i=0;i<fr.n_params;++i){
    p[i]=fr.Param[i][0];
  }
  double par_ones = fr.Param[i_par][0]+factor*fr.Param[i_par][1];
  f_in->Close();

  p[i_par] = par_ones;
  
  //AbstractModel*theModel = new ModelSeparate(p);
 
  AbstractModel *theModel = AbstractModel::getModel(model,p);

  FillLumiFile* f_plus1 = new FillLumiFile(theModel);
  f_plus1->Loop(numberOfEvents);
  f_plus1->setWrite(false,true);//write histo but not tree

  std::ostringstream fname;
  fname<<"Nominal_par_"<<i_par<<"_";
  if (factor>0)
    fname<<"+1sigma";
  else if (factor<0)
    fname<<"-1sigma";
  else
    fname<<"nominal";
  fname<<".root";
    
  f_plus1->saveInFile(fname.str());
  delete f_plus1;

//   p[i_par] = par_minusones;
//   FillLumiFile* f_minus1 = new FillLumiFile();
//   f_minus1->SetParameters(p);
//   f_minus1->Loop(numberOfEvents);
//   f_minus1->setWrite(false,true);//write histo but not tree
//   f_minus1->saveInFile("Nominal_minus1s.root");
//   delete f_minus1;

  return 0;
}
