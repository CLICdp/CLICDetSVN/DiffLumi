#!/bin/bash

for binx in {10..150..10}
do
  export BINX=$binx
  export BINY=$binx
  echo $BINX $BINY
  mkdir -p /afs/cern.ch/work/s/sposs/public/MCMC_${binx}_${binx}/Bout
  #bsub < BatchRunFit.sh
done
