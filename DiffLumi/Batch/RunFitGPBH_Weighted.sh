#!/bin/bash

#BSUB -J RunFitGPBH_W[13-72]
#BSUB -q 2nd
#BSUB -n4
#BSUB -R "span[hosts=1]"
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitGPBH_W.%J_%I
#BSUB -e /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitGPBH_W.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64"


##Now prepare input parameters

JOBTYPE=FitGPBH_Weighted_2

FILEGP=/afs/cern.ch/eng/clic/work2/DiffLumi/BHWide_GP_BHWeighted_Double_slives50_Random.root
FILEMC=/afs/cern.ch/work/s/sposs/public/Parameter2_BHWide_Overlap_def.root


if [ $LSB_JOBINDEX -eq 1 ]; then       BINS=10;    BINYZ=10
elif [ $LSB_JOBINDEX -eq 2 ]; then     BINS=20;    BINYZ=10
elif [ $LSB_JOBINDEX -eq 3 ]; then     BINS=20;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 4 ]; then     BINS=30;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 5 ]; then     BINS=30;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 6 ]; then     BINS=40;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 7 ]; then     BINS=40;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 8 ]; then     BINS=50;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 9 ]; then     BINS=50;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 10 ]; then    BINS=60;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 11 ]; then    BINS=70;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 12 ]; then    BINS=80;    BINYZ=30

elif [ $LSB_JOBINDEX -eq 13 ]; then    BINS=10;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 14 ]; then    BINS=10;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 15 ]; then    BINS=10;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 16 ]; then    BINS=10;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 17 ]; then    BINS=15;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 18 ]; then    BINS=15;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 19 ]; then    BINS=15;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 20 ]; then    BINS=15;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 21 ]; then    BINS=20;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 22 ]; then    BINS=20;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 23 ]; then    BINS=20;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 24 ]; then    BINS=20;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 25 ]; then    BINS=25;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 26 ]; then    BINS=25;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 27 ]; then    BINS=25;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 28 ]; then    BINS=25;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 29 ]; then    BINS=30;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 30 ]; then    BINS=30;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 31 ]; then    BINS=30;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 32 ]; then    BINS=30;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 33 ]; then    BINS=35;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 34 ]; then    BINS=35;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 35 ]; then    BINS=35;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 36 ]; then    BINS=35;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 37 ]; then    BINS=40;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 38 ]; then    BINS=40;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 39 ]; then    BINS=40;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 40 ]; then    BINS=40;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 41 ]; then    BINS=45;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 42 ]; then    BINS=45;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 43 ]; then    BINS=45;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 44 ]; then    BINS=45;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 45 ]; then    BINS=50;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 46 ]; then    BINS=50;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 47 ]; then    BINS=50;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 48 ]; then    BINS=50;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 49 ]; then    BINS=55;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 50 ]; then    BINS=55;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 51 ]; then    BINS=55;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 52 ]; then    BINS=55;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 53 ]; then    BINS=60;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 54 ]; then    BINS=60;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 55 ]; then    BINS=60;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 56 ]; then    BINS=60;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 57 ]; then    BINS=65;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 58 ]; then    BINS=65;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 59 ]; then    BINS=65;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 60 ]; then    BINS=65;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 61 ]; then    BINS=70;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 62 ]; then    BINS=70;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 63 ]; then    BINS=70;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 64 ]; then    BINS=70;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 65 ]; then    BINS=75;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 66 ]; then    BINS=75;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 67 ]; then    BINS=75;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 68 ]; then    BINS=75;    BINYZ=50
elif [ $LSB_JOBINDEX -eq 69 ]; then    BINS=80;    BINYZ=20
elif [ $LSB_JOBINDEX -eq 70 ]; then    BINS=80;    BINYZ=30
elif [ $LSB_JOBINDEX -eq 71 ]; then    BINS=80;    BINYZ=40
elif [ $LSB_JOBINDEX -eq 72 ]; then    BINS=80;    BINYZ=50
fi 

JOBNAME=${JOBTYPE}_${BINS}x${BINYZ}x${BINYZ}

mkdir Inputfiles
cp $FILEMC Inputfiles/
cp $FILEGP Inputfiles/

FILEMC=${PWD}/Inputfiles/$( basename $FILEMC )
FILEGP=${PWD}/Inputfiles/$( basename $FILEGP )

ls -l Inputfiles/

echo "INPUTFILES"
echo $FILEMC
echo $FILEGP

model=Overlap

OUTPUTPATH=${HOME}/work/public/Results/${JOBTYPE}/${JOBNAME}
mkdir -p ${OUTPUTPATH}


echo "Checking out"
svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi &> /dev/null
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=4
source ../Fit/environment.sh
echo "Compiling"
cmake .. &> /dev/null
make -j4 &> /dev/null

./Fit/FitReNewPar -m 10000000 -d 3000000 --level BHWide --model $model --fileMC ${FILEMC} -c 0.5 --Binx ${BINS} --Biny ${BINYZ} --Binz ${BINYZ} --fileData ${FILEGP}

appstatus=$?

ls -l

cp output.log *.root ${OUTPUTPATH}


exit $appstatus
