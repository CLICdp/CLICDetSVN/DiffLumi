#!/bin/bash

#BSUB -J RunFitMCMC[1-1000]
#BSUB -q 1nd
#BSUB -n4
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/work/s/sposs/public/createlumifiles/Bout/CreateLumi.%J_%I
###StdErr
#BSUB -e /afs/cern.ch/work/s/sposs/public/createlumifiles/Bout/CreateLumi.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64 span[hosts=1]"


##Now prepare input parameters

model=Overlap
binx=100
biny=100

##This is defined by the submitting script BatchRunFit_Binning.sh
if [ $BINX ]; then
  binx=$BINX
fi
if [ $BINY ]; then
  biny=$BINY
fi

svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=4
source ../Fit/environment.sh
cmake ..
make -j4

./CreateLumiFile/CreateLumiFileAbstract -m 15000000 --model $model --writeFile --ecut 200
appstatus=$?
echo "Done generating first file"
ls
mv LumiFileMC_${model}_def.root LumiFile_${model}_def_1.root

./CreateLumiFile/CreateLumiFileAbstract -m 5000000 --model $model --writeFile --ecut 200
echo "Done generating second file"
ls
mv LumiFileMC_${model}_def.root LumiFile_${model}_def_2.root

./Fit/FitReweighting -m 10000000 -d 3000000 --level Generator --model $model --fileMC ./LumiFile_${model}_def_1.root -c 0.5 --Binx ${binx} --Biny ${biny} --Binz 1 --fileData LumiFile_${model}_def_2.root
appstatus=$?
#cp control_${model}_Generator.root /afs/cern.ch/work/s/sposs/public/MCMC_${biny}_${biny}/control_${model}_Generator_${LSB_JOBINDEX}.root
cp Generator_${model}_final.root /afs/cern.ch/work/s/sposs/public/MCMC_${binx}_${biny}/Generator_${model}_final_${LSB_JOBINDEX}.root
ls

exit $appstatus
