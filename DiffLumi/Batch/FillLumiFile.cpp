// $Id: $
// Include files

// local
#include "FillLumiFile.hh"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

//-----------------------------------------------------------------------------
// Implementation file for class : FillLumiFile
//
// 2011-11-01 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FillLumiFile::FillLumiFile( AbstractModel*model ) : 
  m_model(model) {
  
  gRandom = new TRandom3(0);
  m_t = new TTree("MyTree","MyTree");
  m_t->Branch("eb1",&m_xb1,"eb1/D");
  m_t->Branch("eb2",&m_xb2,"eb2/D");
  m_t->Branch("deb1",&m_dx1,"deb1/D");
  m_t->Branch("deb2",&m_dx2,"deb2/D");
  m_t->Branch("deb1_g",&m_dx1g,"deb1_g/D");
  m_t->Branch("deb2_g",&m_dx2g,"deb2_g/D");
  m_t->Branch("E1",&m_E1,"E1/D");
  m_t->Branch("E2",&m_E2,"E2/D");

  m_t->Branch("RelativeProbability",&m_RelativeProbability,"RelativeProbability/D");

  x1 = x2 = 0.;
  
  BeamEnergy = 1500.;

  BeamSpreadSmearing = 1.37607e-04;

  m_RelativeProbability = 1;

  m_writeTree = m_writeHisto = true;
  m_writeFile = false;
  m_h_lumi = new TH1D("lumi_spectrum","Lumi Spectrum",3060,-1.5,3058.5);
  m_h_lumi2D = new TH2D("lumi_spectrum_2d","Lumi Spectrum",1530,-0.75,1526.25,1530,-0.75,1526.25);
}
//=============================================================================
// Destructor
//=============================================================================
FillLumiFile::~FillLumiFile() {
  //delete m_t;
  //delete m_h_lumi;
  //delete m_h_lumi2D;
  
}

//=============================================================================


//=========================================================================
//
//=========================================================================
void FillLumiFile::Loop (int nbevts ) {
  std::ofstream ofs;
  if (m_writeFile) {
    ofs.open(Form("LumiFile_%s.out",m_model->GetName().c_str()));
  }
  AbstractModel::Parts Chosen = AbstractModel::kNONE;
  for (int i = 0; i < nbevts;++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, nbevts);
#endif
    Chosen = m_model->GetRandomVariates(m_xb1, m_xb2, m_dx1, m_dx2, m_dx1g, m_dx2g, 
					m_RelativeProbability, BeamSpreadSmearing);
    
    m_E1 = m_xb1+m_dx1+m_dx1g;
    m_E2 = m_xb2+m_dx2+m_dx2g;
    m_t->Fill();
    m_h_lumi->Fill(TMath::Sqrt(4*m_E1*m_E2*BeamEnergy*BeamEnergy));
    m_h_lumi2D->Fill(m_E1*BeamEnergy,m_E2*BeamEnergy);

    if(m_writeFile) {
      ofs << "   " << m_xb1 << "   " << m_xb2
          << "   " << m_dx1 <<"    " << m_dx2
          << "   " << m_dx1g <<"    " << m_dx2g
          << "   " << (m_xb1+m_dx1+m_dx1g)<< "   " << (m_xb2+m_dx2+m_dx2g)
          << "   " << m_RelativeProbability << std::endl;
    }

    double probability ( m_model->CalculateProbability(m_xb1, m_xb2, m_dx1, m_dx2, m_RelativeProbability) );
    if( fabs (  m_RelativeProbability / probability  - 1 ) > 0.000001) {
      std::cout<<"WARN "
		     << std::setw(15) << m_xb1
		     << std::setw(15) << m_xb2
		     << std::setw(15) << m_dx1
		     << std::setw(15) << m_dx2
		     << std::setw(15) << probability 
		     << std::setw(15) << m_RelativeProbability  
		     << std::setw(15) << probability/ m_RelativeProbability  
	       << std::setw(15) << Chosen<<std::endl;
    }
   
  }//for numberOfEvents

}


//=========================================================================
//
//=========================================================================
void FillLumiFile::saveInFile (std::string fname ) {
  if (m_writeTree || m_writeHisto){
    TFile* f_out = TFile::Open(fname.c_str(),"RECREATE");
    if (m_writeTree)
      m_t->Write();
    if (m_writeHisto){
      m_h_lumi->Write();
      m_h_lumi2D->Write();
    }
    f_out->Close();
    delete f_out;
  }
}
