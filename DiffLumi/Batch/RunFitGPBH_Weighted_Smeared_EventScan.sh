#!/bin/bash

#BSUB -J RunFitGPBH_W_S_N[1-11]
#BSUB -q 2nw
##BSUB -n1
##BSUB -R "span[hosts=1]"
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitGPBH_W_S_N_5.%J_%I
#BSUB -e /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitGPBH_W_S_N_5.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64"


##Now prepare input parameters

JOBTYPE=FitGPBH_Weighted_Smeared_EventScan_5

#FILEGP=/afs/cern.ch/eng/clic/work/DiffLumi/BHWide_GP_BHWeighted_Double_slives50_Random_smeared.root
FILEGP=/afs/cern.ch/eng/clic/work2/DiffLumi/BHWide_GP_BHWeighted_Smearead_NewSlices_12345.root
FILEMC=/afs/cern.ch/work/s/sposs/public/Parameter2_BHWide_Overlap_Smeared_def.root 

BINS=40
BINYZ=50

NUMBEROFEVENTS=$(( 500000 + $(( $LSB_JOBINDEX * 500000 )) ))

echo "We are using: $NUMBEROFEVENTS"

JOBNAME=${JOBTYPE}_${BINS}x${BINYZ}x${BINYZ}_N${NUMBEROFEVENTS}

mkdir Inputfiles
cp $FILEMC Inputfiles/
cp $FILEGP Inputfiles/

FILEMC=${PWD}/Inputfiles/$( basename $FILEMC )
FILEGP=${PWD}/Inputfiles/$( basename $FILEGP )

ls -l Inputfiles/

echo "INPUTFILES"
echo $FILEMC
echo $FILEGP

model=Overlap

OUTPUTPATH=${HOME}/work/public/Results/${JOBTYPE}/${JOBNAME}
mkdir -p ${OUTPUTPATH}

echo ${OUTPUTPATH}

echo "Checking out"
svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi &> /dev/null
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=1
source ../Fit/environment.sh
echo "Compiling"
cmake .. &> /dev/null
make -j4 &> /dev/null

./Fit/FitReNewPar -m 20000000 -d ${NUMBEROFEVENTS} --level BHWide --model $model --fileMC ${FILEMC} -c 0.5 --Binx ${BINS} --Biny ${BINYZ} --Binz ${BINYZ} --fileData ${FILEGP} --Smearing

appstatus=$?

ls -l

cp output.log *.root ${OUTPUTPATH}


exit $appstatus
