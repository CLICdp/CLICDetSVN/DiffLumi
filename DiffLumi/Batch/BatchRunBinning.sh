#!/bin/bash


#BSUB -J MakeLumi[1-9]
#BSUB -q 2nd
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/work/s/sposs/RunNewFit/Bout/RunFit.%J_%I
###StdErr
#BSUB -e /afs/cern.ch/work/s/sposs/RunNewFit/Bout/RunFit.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64 span[hosts=1]"
#BSUB -n 4 

##for LSB_JOBINDEX in {1..2}; do

##Now prepare input parameters
if [ $LSB_JOBINDEX -eq 1 ]; then
    binx=10
    binyz=10
elif [ $LSB_JOBINDEX -eq 2 ]; then
    binx=20
    binyz=10
elif [ $LSB_JOBINDEX -eq 3 ]; then
    binx=20
    binyz=20
elif [ $LSB_JOBINDEX -eq 4 ]; then
    binx=30
    binyz=20
elif [ $LSB_JOBINDEX -eq 5 ]; then
    binx=30
    binyz=30
elif [ $LSB_JOBINDEX -eq 6 ]; then
    binx=40
    binyz=30
elif [ $LSB_JOBINDEX -eq 7 ]; then
    binx=40
    binyz=40
elif [ $LSB_JOBINDEX -eq 8 ]; then
    binx=50
    binyz=40
elif [ $LSB_JOBINDEX -eq 9 ]; then
    binx=50
    binyz=50
fi 
echo "$binx  $binyz"

#done
#exit

svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=4
source ../Fit/environment.sh
cmake ..
make -j4
cp /afs/cern.ch/work/s/sposs/public/BHWide_GP_smeared.root .
cp /afs/cern.ch/work/s/sposs/public/BHWide_MC_Separate_smeared.root .

./Fit/FitReweighting -d 3000000 -m 10000000 --level Overlap --Binx $binx --Biny $binyz --Binz $binyz

cp *_final.root /afs/cern.ch/work/s/sposs/RunNewFit/Output/final_${binx}_${binyz}_${binyz}.root
cp control*.root /afs/cern.ch/work/s/sposs/RunNewFit/Output/control_${binx}_${binyz}_${binyz}.root

