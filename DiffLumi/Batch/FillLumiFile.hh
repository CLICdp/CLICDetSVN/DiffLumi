// $Id: $
#ifndef CREATELUMIFILE_FILLLUMIFILE_HH 
#define CREATELUMIFILE_FILLLUMIFILE_HH 1

// Include files
#include "AbstractModel.hh"

#include <TFile.h>
#include <TTree.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

/** @class FillLumiFile FillLumiFile.hh CreateLumiFile/FillLumiFile.hh
 *  
 *
 *  @author Stephane Poss
 *  @date   2011-11-01
 */
class FillLumiFile {
public: 
  /// Standard constructor
  FillLumiFile(AbstractModel* model); 

  virtual ~FillLumiFile( ); ///< Destructor

  void Loop(int numberofevents);
  void saveInFile(std::string fname);

  inline void setWrite(bool tree,bool histo=false, bool file=false) {
    m_writeTree = tree; m_writeHisto = histo; m_writeFile = file;}
  

protected:

private:
  AbstractModel *m_model;
  Double_t random;
  Double_t BeamEnergy;
  Double_t m_xb1, m_xb2, peak, arm1, arm2, body, x1, x2, m_dx1, m_dx2, m_dx1g, m_dx2g;
  Double_t m_E1,m_E2;
  Double_t m_RelativeProbability;
  Double_t BeamSpreadSmearing;
  

  TTree*m_t;
  
  bool m_writeTree,m_writeHisto,m_writeFile;
  
  TH1D*m_h_lumi;
  TH2D*m_h_lumi2D;
  
};
#endif // CREATELUMIFILE_FILLLUMIFILE_HH
