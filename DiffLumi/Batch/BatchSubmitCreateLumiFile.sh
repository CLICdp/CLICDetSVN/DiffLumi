#!/bin/bash

#BSUB -J CreateLumi[1-200]
#BSUB -q 1nd
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/work/s/sposs/public/createlumifiles/Bout/CreateLumi.%J_%I
###StdErr
#BSUB -e /afs/cern.ch/work/s/sposs/public/createlumifiles/Bout/CreateLumi.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64"


##Now prepare input parameters

model=Overlap

svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
cd DiffLumi
mkdir build
cd build
source ../Fit/environment.sh
cmake ..
make

./CreateLumiFile/CreateLumiFileAbstract -m 100000 --model $model --writeFile --ApplyXSec --ecut 200
appstatus=$?
cp ../RootFiles/LumiFileMC_${model}.root /afs/cern.ch/work/s/sposs/public/createlumifiles/LumiFileMC_${model}_${LSB_JOBINDEX}.root
cp ./LumiFile_${model}.out /afs/cern.ch/work/s/sposs/public/createlumifiles/LumiFile_${model}_${LSB_JOBINDEX}.out
exit $appstatus
