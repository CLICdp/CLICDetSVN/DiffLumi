#!/bin/bash

##BSUB -J FitBin[50-200:10]
#BSUB -J FitBin[210-300:10]
#BSUB -q 1nd
#BSUB -n4
#BSUB -R "span[hosts=1]"
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B

###StdOut
#BSUB -o /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitBinning.%J_%I
#BSUB -e /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitBinning.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64"


##Now prepare input parameters
BINS=${LSB_JOBINDEX}
JOBTYPE=Binning2
JOBNAME=${JOBTYPE}_${BINS}x${BINS}
FILEMC=/afs/cern.ch/user/s/sailer/work/public/software/DiffLumi/RootFiles/LumiFileMC_NewPara_Overlap_def.root
FILEGP=/afs/cern.ch/user/s/sailer/clic/work2/DiffLumi/Lumi_GP_Double_Slices50_Random.root

model=Overlap

svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=4
source ../Fit/environment.sh
cmake ..
make -j4


./Fit/FitReNewPar -m 10000000 -d 3000000 --level Generator --model $model --fileMC ${FILEMC} -c 0.5 --Binx ${BINS} --Biny ${BINS} --Binz 1 --fileData ${FILEGP}
appstatus=$?

OUTPUTPATH=${HOME}/work/public/Results/${JOBTYPE}
mkdir -p ${OUTPUTPATH}

cp control_${model}_Generator.root ${OUTPUTPATH}/control_${model}_Generator_${JOBNAME}.root
cp Generator_${model}_final.root   ${OUTPUTPATH}/Generator_${model}_final_${JOBNAME}.root
cp output.log ${OUTPUTPATH}/output_${JOBNAME}.root
ls

exit $appstatus
