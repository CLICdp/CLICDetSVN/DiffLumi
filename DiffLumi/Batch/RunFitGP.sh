#!/bin/bash

#BSUB -J RunFitGPMC[1-200]
#BSUB -q 1nd
#BSUB -n4
#BSUB -R "span[hosts=1]"
#### Submit to test q, set hours to run, and notify when job starts

##For testing, test queue 180 minutes, and notify (-B) when starting
##BSUB -q test
##BSUB -W 180
##BSUB -B


###StdOut
#BSUB -o /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitGPMC.%J_%I
#BSUB -e /afs/cern.ch/user/s/sailer/batchoutput/DiffLumi/FitGPMC.%J_%I
###Resource Requirement
#BSUB -R "type==SLC5_64"


##Now prepare input parameters

BINS=100
JOBTYPE=FitGPMC
JOBNAME=${JOBTYPE}_${BINS}x${BINS}
#FILEMC=/afs/cern.ch/user/s/sailer/clic/work2/DiffLumi/LumiFileMC_NewPara_Overlap_BHWeighted.root
FILEGP=/afs/cern.ch/user/s/sailer/clic/work2/DiffLumi/Lumi_GP_BHWeighted_Double_slives50_Random.root

OUTPUTPATH=${HOME}/work/public/Results/${JOBTYPE}/${JOBNAME}
mkdir -p ${OUTPUTPATH}


model=Overlap

FILEMC=LumiFile_${model}_def_1.root


svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=4
source ../Fit/environment.sh
cmake ..
make -j4

./CreateLumiFile/CreateLumiFileAbstract -m 15000000 --model $model --ecut 200
appstatus=$?
echo "Done generating first file"
ls
mv LumiFileMC_${model}_def.root ${FILEMC}


./Fit/FitReNewPar -m 10000000 -d 3000000 --level Generator --model $model --fileMC ${FILEMC} -c 0.5 --Binx ${BINS} --Biny ${BINS} --Binz 1 --fileData ${FILEGP}

appstatus=$?

ls -l

cp control_${model}_Generator.root ${OUTPUTPATH}/control_${model}_Generator_${LSB_JOBINDEX}.root
cp Generator_${model}_final.root   ${OUTPUTPATH}/Generator_${model}_final_${LSB_JOBINDEX}.root
cp output.log                      ${OUTPUTPATH}/output_${LSB_JOBINDEX}.root

exit $appstatus
