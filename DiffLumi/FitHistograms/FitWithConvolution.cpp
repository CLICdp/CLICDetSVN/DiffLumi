#include "Utilities.h"
#include "DrawSpectra.hh"
#include "ConvolutedFunctions.hh"
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLegend.h>
#include <TRandom.h>
#include <TMath.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TStyle.h>
#include <TROOT.h>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif


void FitBeamstrahlungConv(TH1D& GPHisto, double limit=0.0);
void FitChebychevGauss(TH1D& GPHisto, double limit=0.0);
TH1D * ReadFile();

int main ()
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  // gStyle->SetOptFit(1111);
  // gStyle->SetOptStat(1111);
  // gROOT->ForceStyle();


  // Double_t smin = 0.00, smax = 1.10;
  // TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root";

  //TH1D hEParticle2  = DrawEnergy("EPart2", fileWithELoss, 300000000, false, 0.0, int(400*1.1/1.02));
  //TH1D GPHisto  = DrawLumiSpectrum("roots", fileWithELoss ,smin, smax, 300000000,false, true, 200);
  //  TH1D GPHisto  = DrawLumiSpectrum("roots", fileWithELoss ,smin, smax, 300000000,false, true, 1000) ;

  TH1D* GPHisto = ReadFile();
  if ( not GPHisto ) {
    std::cout << "Histogram not found!"  << std::endl;
    return 1;
  }
  //FitBeamstrahlungConv(*GPHisto, 0.0);
  FitChebychevGauss(*GPHisto);
  return 0;
}



void FitBeamstrahlungConv(TH1D& GPHisto, double) {

  TVirtualFitter::SetMaxIterations(100000);

  gLowerLimit=GPHisto.GetBinLowEdge(GPHisto.FindFirstBinAbove(0.0));
  std::cout << "Lower Edge is: " << gLowerLimit  << std::endl;
  //  gUpperLimit=0.995;
  gUpperLimit=1.05;
  const int nBeta = 3;

  //Histogram has to be properly normalized!
  //GPHisto.Scale( 1.0 / GPHisto.Integral(1, GPHisto.FindFixBin(gUpperLimit), "width"));
  GPHisto.Scale( 1.0 / GPHisto.Integral("width"));

  //  TF1 BeamStrahlungConv("convFunc", BeamstrahlungGauss, limit, gBetalimit, 10, "BeamStrahlungGauss" );
  TF1 BeamStrahlungConv("convFunc", BeamstrahlungGauss, gLowerLimit, gUpperLimit, 14, "BeamStrahlungGauss" );
  BeamStrahlungConv.SetNpx(3000);
  //TF1 BeamStrahlungConv("convFunc", bf3, limit , gBetalimit, 9, "BeamStrahlungGauss" );
  double parameter[14] = {0.005,
			  0.2, 2.0, -0.5,
			  0.2, 2.0, -0.5,
			  0.2, 2.0, -0.5,
			  0.2, 2.0, -0.5,
			  0.3};
		      // { 2.0, -0.5, 0.3, 2.0, -0.5, 0.3, 2.0, -0.5, 0.3, 0.01};
  BeamStrahlungConv.SetParameters(parameter);


  //Sigma parameter
  //BeamStrahlungConv.FixParameter( 0, 0.005);

  BeamStrahlungConv.SetParLimits( 0,  0.0, 1e-2); BeamStrahlungConv.SetParName( 0,"sig");

  //  BeamStrahlungConv.SetParLimits( 1,  0.0, 100); BeamStrahlungConv.SetParName( 1,"p_a");
  BeamStrahlungConv.SetParLimits( 2, -0.0,1000); BeamStrahlungConv.SetParName( 2,"a_{1}");
  BeamStrahlungConv.SetParLimits( 3, -1.0,   0); BeamStrahlungConv.SetParName( 3,"a_{2}");
  //  BeamStrahlungConv.SetParLimits( 4, -100.0, 100); BeamStrahlungConv.SetParName( 4,"p_b");
  BeamStrahlungConv.SetParLimits( 5, -0.0, 100); BeamStrahlungConv.SetParName( 5,"b_{1}");
  BeamStrahlungConv.SetParLimits( 6, -1.0,  10); BeamStrahlungConv.SetParName( 6,"b_{2}");
  //  BeamStrahlungConv.SetParLimits( 7,  0.0, 100); BeamStrahlungConv.SetParName( 7,"p_c");
  BeamStrahlungConv.SetParLimits( 8, -0.0, 100); BeamStrahlungConv.SetParName( 8,"c_{1}");
  BeamStrahlungConv.SetParLimits( 9, -1.0,  10); BeamStrahlungConv.SetParName( 9,"c_{2}");
  //  BeamStrahlungConv.SetParLimits(10, -1000.0, 100); BeamStrahlungConv.SetParName(10,"p_d");
  BeamStrahlungConv.SetParLimits(11, -0.0, 100); BeamStrahlungConv.SetParName(11,"d_{1}");
  BeamStrahlungConv.SetParLimits(12, -1.0,  10); BeamStrahlungConv.SetParName(12,"d_{2}");

  BeamStrahlungConv.SetParLimits(13,  0.0,  10); BeamStrahlungConv.SetParName(13,"delta");

  if(nBeta <= 3) {
    //uncomment to have only three betafunctions
    BeamStrahlungConv.FixParameter(10, 0.0);
    BeamStrahlungConv.FixParameter(11, 1.0);
    BeamStrahlungConv.FixParameter(12, 1.0);
  }
  if(nBeta <= 2) {
    //uncomment this and above for only two betafunction
    BeamStrahlungConv.FixParameter( 7, 0.0);//pc
    BeamStrahlungConv.FixParameter( 8, 1.0);//c1
    BeamStrahlungConv.FixParameter( 9, 1.0);//c2
  }
  if(nBeta <= 1) {
    //uncomment this and bove to have only single betafunction
    BeamStrahlungConv.FixParameter( 4, 0.0);//pb
    BeamStrahlungConv.FixParameter( 5, 1.0);//b1
    BeamStrahlungConv.FixParameter( 6, 1.0);//b2
  }
  if(nBeta == 0) {
    //uncomment this and bove to have only single betafunction
    BeamStrahlungConv.FixParameter(1 , 0.0);
    BeamStrahlungConv.FixParameter(2, 1.0);
    BeamStrahlungConv.FixParameter(3, 1.0);
  }

  BeamStrahlungConv.FixParameter(13, 1.0);






  GPHisto.Fit(&BeamStrahlungConv,"R");
  GPHisto.SetAxisRange(gLowerLimit-0.1, 1.05,"X");
  TCanvas canv("cConv","cConv");
  canv.SetLogy();
  GPHisto.Draw();
  canv.SaveAs("BeamStrahlungFit_Conv.eps");

}



TH1D* ReadFile () {
  //  TString fileWithELoss  = "/afs/cern.ch/user/b/blaising/Mtest/funisredep/205/funisredep310205nmLAS.root";
  TString fileWithELoss  = "/afs/cern.ch/eng/clic/data/JJB/FitIsrEdep/funisredep310205nmLAS.root";
  int ippid=205;
  //int binx=155;
  TString sFun="nmLAS";
  TFile * funisredep=TFile::Open(fileWithELoss);
  if(not funisredep) {
    std::cout << "File not Found!"  << std::endl;
    return NULL;
  }
  funisredep->cd();
  funisredep->ls();
//
  TString hname = Form("h1funisredep%d"+sFun,ippid);
  std::cout << "**:name " << hname << std::endl;
  TH1D * H1SQRTS = (TH1D*)gDirectory->Get(hname);
  std::cout << "**: hname " << hname << " " << H1SQRTS << std::endl;
  if(not H1SQRTS) {
    funisredep->Close();
    return NULL;
  }
//  double xMax=H1SQRTS->GetMaximum();
//  cout << "**: xMax " << xMax << endl;
//
  TCanvas c1("c1"," Histos ",0,0,800,700);
  //c1.Divide(1,1);
  // int pad=0;
  // pad++;
  // c1->cd(pad);
  gPad->SetLogy(1);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
//

  H1SQRTS->Draw("");
  //  c1->SaveAs("../hist/H1SQRTS.eps");
  TH1D * GPHisto;
  GPHisto = (TH1D*)H1SQRTS->Clone();

  return GPHisto;
  funisredep->Close();
}




void FitChebychevGauss(TH1D& GPHisto, double) {


  TVirtualFitter::SetMaxIterations(100000);

  gLowerLimit=GPHisto.GetBinLowEdge(GPHisto.FindFirstBinAbove(0.0));
  std::cout << "Lower Edge is: " << gLowerLimit  << std::endl;
  //  gUpperLimit=0.995;
  gUpperLimit=1.05;
  const int nDegrees = 30;


  gCheb = new ConvolutedFunctions::Chebyshev(nDegrees, gLowerLimit, 1.0);

  //Histogram has to be properly normalized!
  GPHisto.Scale( 1.0 / GPHisto.Integral("width"));

  //we are going to abuse the first parameter to pass the xValue to the convolution!
  TF1 ChebyshevConv("convFunc", ChebyshevGauss, gLowerLimit, gUpperLimit, nDegrees+2, "BeamStrahlungGauss" );
  ChebyshevConv.SetNpx(3000);
  //TF1 ChebyshevConv("convFunc", bf3, limit , gBetalimit, 9, "BeamStrahlungGauss" );
  double parameter[nDegrees+2];
  parameter[1] = 0.005;
  for (int i = 2; i < nDegrees+2 ;++i) {  parameter[i] = 1.0;  }
  ChebyshevConv.SetParameters(parameter);

  ChebyshevConv.FixParameter(0, 1.0);
  ChebyshevConv.SetParName(0,"ignore");
  ChebyshevConv.SetParName(1,"sigma");



  GPHisto.Fit(&ChebyshevConv,"R");
  GPHisto.SetAxisRange(gLowerLimit-0.1, 1.05,"X");
  TCanvas canv("cConv","cConv");
  canv.SetLogy();
  GPHisto.Draw();
  canv.SaveAs("ChebyshevFit_Conv.eps");
}
