#!/bin/bash

## set the job name
##BSUB -J SmuonFit[1,2,5,6,7,8,9,10,13,14,15,16,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,36,37,38,39,42,43,44,45,46,47,50,51]
#BSUB -J SmuonFitT
## set the batch queue name
##BSUB -q 8nh
#BSUB -q 8nh
###BSUB -q 1nh
#BSUB -o  /afs/cern.ch/user/s/sailer/scratch0/batchoutput/SmuonFit/SmuonFit_BW10_6.%J.%I
#BSUB -e  /afs/cern.ch/user/s/sailer/scratch0/batchoutput/SmuonFit/SmuonFit_BW10_6.%J.%I

###BSUB -R "type==SLC5_64"
export DEBUG=OFF
export SEL=1
echo "**Start doall.sh job:"
start_time=`date +%X`
echo "*-Start time: $start_time "
echo "*:Jobname is: $JOBNAME"



jpid=$(( $LSB_JOBINDEX - 26 ))
jpid=0

if [ $jpid -eq 0 ]; then
    p18=nm
elif [ $jpid -lt 0 ]; then
    p18=p${jpid:1}m
else
    p18=p${jpid}p
fi

export FUNC=$p18

echo "PID $jpid and p18 $p18"


if [ "$RUNBATCH" == "T" ]; then
  echo "Checking out"
  svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi &> /dev/null
  cd DiffLumi
  mkdir build
  cd build
  export OMP_NUM_THREADS=1
  source ../Fit/environment.sh
  cmake ..
  echo "Compiling"
  make -j1 &> /dev/null
fi

#cp ${inputpath}/${FINALFILE} .

#../build/ErrorPropagation/OverlapErrorPropagation BHWide_Overlap_final.root $jpid ##outputs Numerical..._${jpid}.root

#../build/ErrorPropagation/OverlapErrorPropagation ${FINALFILE} $jpid ##outputs Numerical..._${jpid}.root
#cp ${inputpath}/NumericalEstimateOutput_${jpid}.root .


## using GP file
cp /afs/cern.ch/work/s/sailer/public/software/DiffLumi/SmuonAnalysis/GPSpectrumForSmuonfit.root NumericalEstimateOutput_${jpid}.root 

if [ $? -ne 0 ]; then
    echo "ERROR: Not a valid fit result, abort!"
    exit 1
fi

pid=$jpid #parameter ID we want to create a lumi convoluted distribution for (+/- for the variation). 0 is the nominal

echo "running MakeBsIsrEdep"
#../../build/SmuonAnalysis/MakeBsIsrEdep --process 205 --parameterType $p18 --paramidvar $pid --outputpath . --inputpath ./ 

#/copying them here
#cp /afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/RooK/smu2smu2_386-chne-poL0_nosc_3000_1.000_RooKeysPdf.root RooKfunisredep205_nm.root
#cp /afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/h1funisredep/205/ThSfunisredep310205nmLAS.root ThSfunisredep205_nm.root ##actually not used, but still needed...

appstatus=$?

if [[ $appstatus -ne 0 ]]; then
 exit $appstatus
fi

##FIXME
#outputpath=/afs/cern.ch/work/s/sposs/public/Smuon/RooKeys/${binx}x${biny}x${binz}/${p18}/
#mkdir -p $outputpath
#cp RooKfunisredep*.root $outputpath
#cp Th*.root $outputpath

#######################################################
## Run the fit
#######################################################

### set parameters

#inputdatafile=../RootFiles/rtmva_205_smear2e-5t0_pol0_0BX_e2500_2_C80_L_2000_plots.root
inputdatafile=../../RootFiles/rtmva_205_smear2e-5t0_pol0_0BX_e2500_2_C80_L_2000_plots_BW10.root
inputBSISRXSECpath=./

echo "Running EnerDistFit"

../../build/SmuonAnalysis/EnerDistFit $p1 $p2 $p3 $p4 $p5 $p6 $p7 $p8 $p9 $p10 $p11 $p12 $p13 $p14 $p15 $p16 $p17 $p18 $p19 $p20 $p21 $p22 $inputdatafile $inputBSISRXSECpath $pid $binx $biny $binz

appstatus=$?

##FIXME
#outputpathfit=/afs/cern.ch/work/s/sposs/public/Smuon/fit/${binx}x${biny}x${binz}/${p18}/
outputpathfit=/afs/cern.ch/eng/clic/work2/DiffLumi/SmuonResults_BW10_6/${binx}x${biny}x${binz}/${p18}/
mkdir -p $outputpathfit

cp FitResult_* $outputpathfit

end_time=`date +%X`
echo "* End time: $end_time "
echo "**End statfit.sh job:"

exit $appstatus
