#!/bin/bash
#
export DEBUG=OFF   
export SEL=1

#
## set the job name
#BSUB -J ROOK
## set the batch queue name
#BSUB -q 8nh
###BSUB -q 1nh
#BSUB -o  /afs/cern.ch/work/s/sposs/public/BsIsrEdepRooK/batchout/ROOK.%J_%I
#BSUB -e  /afs/cern.ch/work/s/sposs/public/BsIsrEdepRooK/batchout/ROOK.%J_%I
#BSUB -R "type==SLC5_64"

## use bsub < startfit.sh to submit this file
echo "**Start MakeRooKeysPdf.sh job:"
start_time=`date +%X`
echo "*-Start time: $start_time "
echo "*:Jobname is: $JOBNAME"

svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
cd DiffLumi
mkdir build
cd build
export OMP_NUM_THREADS=4
source ../Fit/environment.sh
cmake ..
make -j4

pid=0 #parameter ID we want to create a lumi convoluted distribution for (+/- for the variation). 0 is the nominal
pname=nm #p1p, p1m, for naming convention

#secondparameter is not really clear what it's meant for
./SmuonAnalysis/MakeBsIsrEdep --process 205 --parameterType $pname --paramidvar $pid --outputpath .

appstatus=$?

outputpath=/afs/cern.ch/work/s/sposs/public/Smuon/205/${pname}/
mkdir -p $outputpath

cp RooKfunisredep*.root $outputpath
cp Th*.root $outputpath

end_time=`date +%X`
echo "* End time: $end_time "
echo "**End statfit.sh job:"

exit $appstatus
