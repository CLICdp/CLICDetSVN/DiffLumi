#!/bin/bash
#
export DEBUG=OFF   
export SEL=1

#
export SPECSEL=C80
##export FIT=SUB
export TEST=FIT
export ECOR=YES       
export ECOR=NO     
export MAXLOOP=1            
par15=DIST
#
## use bsub < startfit.sh to submit this file
echo "**Start startfit.sh job:"
start_time=`date +%X`
echo "*-Start time: $start_time "
#echo "Parameters: $0 $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12}  "
#echo "Parameters: ${13} ${14} ${15} ${16} ${17} ${18} "

par11=${11}

#exit
##export PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.6.2/x86_64-slc5/bin:${PATH}
##export LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.6.2/x86_64-slc5/lib64:${LD_LIBRARY_PATH}

## set the job name
#BSUB -J SFIT[1-1]
## set the batch queue name
##BSUB -q 8nh
#BSUB -q 8nh
###BSUB -q 1nh
#BSUB -o  /afs/cern.ch/work/s/sposs/public/BsIsrEdepRooK/fit/batchout/SFIT.%J_%I
#BSUB -e  /afs/cern.ch/work/s/sposs/public/BsIsrEdepRooK/fit/batchout/SFIT.%J_%I
###BSUB -R "type==SLC5_64"

echo "*:Jobname is: $JOBNAME"
#echo "*:INPUTFILE: $INPUTFILE"
#INPUTDIR="/afs/cern.ch/user/b/blaising/scratch1/slepton/batch/input/
#ls -al $INPUTDIR$INPUTFILE
#echo "*:OUTPUTFILE: $OUTPUTFILE"

echo "-working directory is: "
pwd
#cp $INPUTDIR$INPUTFILE $INPUTFILE
#ls -al $INPUTFILE
##exit
echo "par11 $par11"
inputdatafile=../RootFiles/rtmva_205_smear2e-5t0_pol0_0BX_e2500_2_C80_L_2000_plots.root
inputBSISRXSECpath=/afs/cern.ch/work/s/sposs/public/Smuon/205/${p18}/

if [[ $runBATCH = 'T' ]]; then
  svn co  svn+ssh://svn.cern.ch/reps/clicdet/trunk/DiffLumi/DiffLumi
  cd DiffLumi
  mkdir build
  cd build
  export OMP_NUM_THREADS=4
  source ../Fit/environment.sh
  cmake ..
  make -j4
fi

../build/SmuonAnalysis/EnerDistFit $p1 $p2 $p3 $p4 $p5 $p6 $p7 $p8 $p9 $p10 $p11 $p12 $p13 $p14 $p15 $p16 $p17 $p18 $p19 $p20 $p21 $p22 $inputdatafile $inputBSISRXSECpath 

appstatus=$?

mkdir -p /afs/cern.ch/work/s/sposs/public/BsIsrEdepRooK/fit/${p18}/

cp FitResult_* /afs/cern.ch/work/s/sposs/public/BsIsrEdepRooK/fit/${p18}/
##cp $OUTPUTFILE "/afs/cern.ch/user/b/blaising/scratch1/runzprime/prod/output/"$OUTPUTFILE 
end_time=`date +%X`
echo "* End time: $end_time "
echo "**End statfit.sh job:"

exit $appstatus
