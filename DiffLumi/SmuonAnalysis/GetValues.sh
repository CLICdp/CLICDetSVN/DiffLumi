#!/bin/bash 


function doGetValues() {
    basefolder=$1
    folder=$2
    
    cd $folder
#    echo "$folder  " >> $FILE
    values=""
    for subfolder in $( ls -lfd nm p?p p1?p p2?p p?m p1?m p2?m ) ; do
#	echo $folder/$subfolder
	cd $subfolder

#	values="$values $( root -b GetValueFromRootFile.C\(\"\"\) )
	FILENAME=$(ls --color=never FitResult_*.root)
	STRING=$(root -b -q /afs/cern.ch/work/s/sailer/public/software/DiffLumi/SmuonAnalysis/GetValueFromRootFile.C\(\"$FILENAME\"\))
#	echo $STRING
#	echo $subfolder $( echo $STRING | grep RESULT | sed s/.*Slep// | sed s/+.*//)
	values="$values $( echo $STRING | grep RESULT | sed s/.*Slep// | sed s/+.*//)"
	valuesG="$valuesG $( echo $STRING | grep RESULT | sed s/.*Gaug// | sed s/+.*//)"
#	values="$values $( grep Fit FitResult_*.txt | sed s/.*mS// | sed s/+.*// )"
	cd ..
    done ## subfolder
#    echo $values
    RESA=`/afs/cern.ch/work/s/sailer/public/software/DiffLumi/build/ErrorPropagation/CalculateResult /afs/cern.ch/work/s/sailer/public/Results/FitGPBH_Weighted_Smeared_3/FitGPBH_Weighted_Smeared_3_${folder}/BHWide_Overlap_final.root $values | grep "Final Result"`
    RESB=`/afs/cern.ch/work/s/sailer/public/software/DiffLumi/build/ErrorPropagation/CalculateResult /afs/cern.ch/work/s/sailer/public/Results/FitGPBH_Weighted_Smeared_3/FitGPBH_Weighted_Smeared_3_${folder}/BHWide_Overlap_final.root $valuesG | grep "Final Result"`
 
    echo FOLDER $folder
    echo RESA $RESA
    echo RESB $RESB
#    echo VAL $values
#    echo VALG $valuesG

    values=""
    valuesG=""
}


export ROOT_HIST=0
export -f doGetValues

basefolder=$PWD

echo $PWD

#FILE=$basefolder/output.txt

parallel -k -j8  doGetValues $basefolder ::: $( ls -d *0x* )

