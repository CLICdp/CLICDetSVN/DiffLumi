#! /bin/bash


NUMBEROFPARAMETERS=19
NUMBEROFBINNINGS=72


for BinningID in {1..72}; do
#for BinningID in 73; do

    if [ $BinningID -eq 1 ]; then       BINS=10;    BINYZ=10
    elif [ $BinningID -eq 2 ]; then     BINS=20;    BINYZ=10
    elif [ $BinningID -eq 3 ]; then     BINS=20;    BINYZ=20
    elif [ $BinningID -eq 4 ]; then     BINS=30;    BINYZ=20
    elif [ $BinningID -eq 5 ]; then     BINS=30;    BINYZ=30
    elif [ $BinningID -eq 6 ]; then     BINS=40;    BINYZ=30
    elif [ $BinningID -eq 7 ]; then     BINS=40;    BINYZ=40
    elif [ $BinningID -eq 8 ]; then     BINS=50;    BINYZ=40
    elif [ $BinningID -eq 9 ]; then     BINS=50;    BINYZ=50
    elif [ $BinningID -eq 10 ]; then    BINS=60;    BINYZ=30
    elif [ $BinningID -eq 11 ]; then    BINS=70;    BINYZ=30
    elif [ $BinningID -eq 12 ]; then    BINS=80;    BINYZ=30
    elif [ $BinningID -eq 13 ]; then    BINS=10;    BINYZ=20
    elif [ $BinningID -eq 14 ]; then    BINS=10;    BINYZ=30
    elif [ $BinningID -eq 15 ]; then    BINS=10;    BINYZ=40
    elif [ $BinningID -eq 16 ]; then    BINS=10;    BINYZ=50
    elif [ $BinningID -eq 17 ]; then    BINS=15;    BINYZ=20
    elif [ $BinningID -eq 18 ]; then    BINS=15;    BINYZ=30
    elif [ $BinningID -eq 19 ]; then    BINS=15;    BINYZ=40
    elif [ $BinningID -eq 20 ]; then    BINS=15;    BINYZ=50
    elif [ $BinningID -eq 21 ]; then    BINS=20;    BINYZ=20
    elif [ $BinningID -eq 22 ]; then    BINS=20;    BINYZ=30
    elif [ $BinningID -eq 23 ]; then    BINS=20;    BINYZ=40
    elif [ $BinningID -eq 24 ]; then    BINS=20;    BINYZ=50
    elif [ $BinningID -eq 25 ]; then    BINS=25;    BINYZ=20
    elif [ $BinningID -eq 26 ]; then    BINS=25;    BINYZ=30
    elif [ $BinningID -eq 27 ]; then    BINS=25;    BINYZ=40
    elif [ $BinningID -eq 28 ]; then    BINS=25;    BINYZ=50
    elif [ $BinningID -eq 29 ]; then    BINS=30;    BINYZ=20
    elif [ $BinningID -eq 30 ]; then    BINS=30;    BINYZ=30
    elif [ $BinningID -eq 31 ]; then    BINS=30;    BINYZ=40
    elif [ $BinningID -eq 32 ]; then    BINS=30;    BINYZ=50
    elif [ $BinningID -eq 33 ]; then    BINS=35;    BINYZ=20
    elif [ $BinningID -eq 34 ]; then    BINS=35;    BINYZ=30
    elif [ $BinningID -eq 35 ]; then    BINS=35;    BINYZ=40
    elif [ $BinningID -eq 36 ]; then    BINS=35;    BINYZ=50
    elif [ $BinningID -eq 37 ]; then    BINS=40;    BINYZ=20
    elif [ $BinningID -eq 38 ]; then    BINS=40;    BINYZ=30
    elif [ $BinningID -eq 39 ]; then    BINS=40;    BINYZ=40
    elif [ $BinningID -eq 40 ]; then    BINS=40;    BINYZ=50
    elif [ $BinningID -eq 41 ]; then    BINS=45;    BINYZ=20
    elif [ $BinningID -eq 42 ]; then    BINS=45;    BINYZ=30
    elif [ $BinningID -eq 43 ]; then    BINS=45;    BINYZ=40
    elif [ $BinningID -eq 44 ]; then    BINS=45;    BINYZ=50
    elif [ $BinningID -eq 45 ]; then    BINS=50;    BINYZ=20
    elif [ $BinningID -eq 46 ]; then    BINS=50;    BINYZ=30
    elif [ $BinningID -eq 47 ]; then    BINS=50;    BINYZ=40
    elif [ $BinningID -eq 48 ]; then    BINS=50;    BINYZ=50
    elif [ $BinningID -eq 49 ]; then    BINS=55;    BINYZ=20
    elif [ $BinningID -eq 50 ]; then    BINS=55;    BINYZ=30
    elif [ $BinningID -eq 51 ]; then    BINS=55;    BINYZ=40
    elif [ $BinningID -eq 52 ]; then    BINS=55;    BINYZ=50
    elif [ $BinningID -eq 53 ]; then    BINS=60;    BINYZ=20
    elif [ $BinningID -eq 54 ]; then    BINS=60;    BINYZ=30
    elif [ $BinningID -eq 55 ]; then    BINS=60;    BINYZ=40
    elif [ $BinningID -eq 56 ]; then    BINS=60;    BINYZ=50
    elif [ $BinningID -eq 57 ]; then    BINS=65;    BINYZ=20
    elif [ $BinningID -eq 58 ]; then    BINS=65;    BINYZ=30
    elif [ $BinningID -eq 59 ]; then    BINS=65;    BINYZ=40
    elif [ $BinningID -eq 60 ]; then    BINS=65;    BINYZ=50
    elif [ $BinningID -eq 61 ]; then    BINS=70;    BINYZ=20
    elif [ $BinningID -eq 62 ]; then    BINS=70;    BINYZ=30
    elif [ $BinningID -eq 63 ]; then    BINS=70;    BINYZ=40
    elif [ $BinningID -eq 64 ]; then    BINS=70;    BINYZ=50
    elif [ $BinningID -eq 65 ]; then    BINS=75;    BINYZ=20
    elif [ $BinningID -eq 66 ]; then    BINS=75;    BINYZ=30
    elif [ $BinningID -eq 67 ]; then    BINS=75;    BINYZ=40
    elif [ $BinningID -eq 68 ]; then    BINS=75;    BINYZ=50
    elif [ $BinningID -eq 69 ]; then    BINS=80;    BINYZ=20
    elif [ $BinningID -eq 70 ]; then    BINS=80;    BINYZ=30
    elif [ $BinningID -eq 71 ]; then    BINS=80;    BINYZ=40
    elif [ $BinningID -eq 72 ]; then    BINS=80;    BINYZ=50
##for special fit ## 99 with files given to JJ, with files created like JJ to check we didn't change the code
## 199 / 299 = with the spectra I gave to JJ long time ago
    elif [ $BinningID -eq 73 ]; then    BINS=99;    BINYZ=99
    elif [ $BinningID -eq 74 ]; then    BINS=199;    BINYZ=199
    elif [ $BinningID -eq 75 ]; then    BINS=299;    BINYZ=299
    fi

    export binx=$BINS
    export biny=$BINYZ
    export binz=$BINYZ

    echo "b" | ./MakeRunFullFit.sh 


done ##binnings
