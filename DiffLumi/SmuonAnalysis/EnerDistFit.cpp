#include "RooGlobalFunc.h"
//
#include <Math/Integrator.h>
#include <Riostream.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TFrame.h>
#include <TGraph.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TLine.h>
#include <TMath.h>
#include <TMinuit.h>
#include <TPaveText.h>
#include <TPostScript.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TRandom3.h>
#include <TRint.h>
#include <TStopwatch.h>
#include <TStyle.h>
#include <TTree.h>

#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

#include <iomanip>

int globalCounter;

inline int FindFastBin(TH1 const  * const histo, double const& value) {
  static const double xmin(histo->GetXaxis()->GetXmin());
  static const double xmax(histo->GetXaxis()->GetXmax());
  static const int nBins(histo->GetNbinsX());
  static const double invBinWidth( double(nBins)/(xmax-xmin) );
  return int( (value - xmin) * invBinWidth )+1;
}

inline double InterpolateFast(TH1D const * const histo, double& value) {
  static const double xmin(histo->GetXaxis()->GetXmin());
  static const double xmax(histo->GetXaxis()->GetXmax());
  static const int nBins(histo->GetNbinsX());
  const int thisBin(FindFastBin (histo, value));
  static const double invBinWidth( double(nBins)/(xmax-xmin) );
  static const double binWidth( 1./invBinWidth );
  //  if ( nBins > 550 ) return 0.0;
  const double y0(histo->fArray[thisBin]);
  const double y1(histo->fArray[thisBin+1]);
  const double x0((thisBin)*binWidth+xmin);
  const double x1(x0+binWidth);
  // std::cout << __func__
  // 	    << std::setw(15) << y0
  // 	    << std::setw(15) << y1
  // 	    << std::setw(15) << x0
  // 	    << std::setw(15) << x1
  // 	    << std::setw(15) << value
  // 	    << std::setw(15) << thisBin
  // 	    << std::setw(15) << (y0 + (y1-y0) * invBinWidth * ( value - x0 ))
  // 	    << std::endl;

  return (y0 + (y1-y0) * invBinWidth *( value - x0));

}

typedef std::pair<float,float> pairff;
//
#include "RooWorkspace.h"
#include "RooKeysPdf.h"
//
using namespace std;
using namespace RooFit ;
//
double dSqrtsmin=2070.;
double dSqrtsmax=3040;
int IntFunc_UxSFlag=0;
//
double dfrac=0;
double dsigma1=0;
double dsigma2=0;
//
TH1D *thS;
TF1 * fU, * fS, *fD, *XFS;
TF1 * fUxfS, * fUxfD, * fUcfS;
TF1 * IntfUxfD;
TF1 * fUxfSxfD;
TF1 * IntfUxfSxfD;
//TF1 * IntfUxfS;
TF1 * IntfUcfS;
//
int debugU=0;
int debugD=0;
int debugS=0;
int debugUD=0;
int debugIUD=0;
int debugUS=0;
int debugUSD=0;
int debugIUS=0;
int debugIUSD=0;
//
Int_t nbins;
Double_t xsec=1;
int Icall=0;
int xsub=0;
double fEcm=3000.;
int iECM=3000;
int nTries=0;
string sChi2="AF"; // Analytic function
//
double dprec=0.001;
string sDcut="DC4";
//int effcor=0;
int ippid=0;
int Ndfd=0;
int iprpp=0;
//int MaxLoop=2;
int pm10=0;
//int pm10=1; // PM10
int nbinx=310;
//
TH1F * hdata;
TH1F * hdataL;
TH1F * hdataH;
TH1F * hbeam;
TH1F * hbeambox;
TH1F * hBox;
TH1F * hbkg;
TH1F * hbkgT; // background from TrainTree
TH1F * hbkgF;
//
TRandom *rnd;
//
TF1 * bkgF;
TH1F * hsignal;
TF1 * fECOR;
TF1 * fLER;
//
TH1F * H1HISTSPB;  // S+B from testtree
TH1F * H1HISTBKG;  // B from testtree
TH1F * H1HISTBKGF;
TH1F * H1HISTBKGT; // B from TrainTree
TH1F * H1HISTSPBMBKG; // S+B(from testtree)-B(feom ttraintree
TH1F * H1HISTSIGC0;
TH1F * H1HISTSIGCX;
TH1F * H1HISTSIGCOR;
//
double rlum;
double dsmear;
double bkgpeak;
int fixjer;
int fixnorm;
double dxnorm=1.;
//
//
void InitAnal(int, char **,  vector<int> &, vector<string> &,
	      vector<float> &, vector<float> &, vector<float> &,
	      vector<float> &, vector<float> &, vector<float> &, vector<float> &,
	      vector<string> &, vector<float> &);
//
int massFit(TPostScript * , TCanvas *, int &, TH1F *, char **, int, char *, int, TH1F * = NULL);
//
//Double_t isrFunction(Double_t *, Double_t *);
Double_t boxf(Double_t *, Double_t * );
Double_t boxfl(Double_t *, Double_t * );
void fcn(Int_t & , Double_t *, Double_t &, Double_t *, Int_t );
void getFileName(char * , char * , char **, char *, char *, char *);
Double_t twogaus(Double_t *, Double_t *);
//
double Func_U(double *, double *); // Unif
double Func_S(double *, double *); // Sqrts
double Func_D(double *, double *); // Dres
double Func_UxS(double *, double *); // UnifxSqrts
double Func_UxD(double *, double *); // UnifxSqrts
double IntFunc_UxD(double *, double *); // Integral of UnifxD
double IntFunc_UxS(double *, double *); // Integral of UnifxSqrts

double Func_UxSxD(double *, double *); // UnifxSqrts
double IntFunc_UxSxD(double *, double *); // UnifxSqrts

double Func_UcS(double *, double *); // UnifxSqrts
double IntFunc_UcS(double *, double *); // Integral of UnifxSqrts


//int main(TH1F * hdata, TH1F * hbeam) // to run outside root
int main(int argc, char * argv[]) // to run outside root
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  cout << "**:EnerDistFit:dSqrtsmin "<< dSqrtsmin << endl;
  //
  cout << "argc " << argc << endl;
  for (int i=0; i<argc; ++i) cout << " argv[" << i << "] " << argv[i] << endl;
  //
  int ilast=argc-1;
  //
  //return 0;
  char argv1[20];
  char argv2[20];
  char argv3[20];
  char argv4[20];
  char argv5[20];
  char argv6[20];
  char argv7[20];
  char argv8[20];
  char argv9[20];
  char argv10[20];
  //char argv11[20];
  char argv12[20]; // FIT= SIG(signal only) or SPB(S+B) or SPBMB (S-B)
  char argv13[20]; // TEST=FIT or NOFIT (make stack plot)
  //char argv14[20]; // MaxLoop
  //char cECOR[20]; // ECOR
  char argv16[20]; // FUNC for beam
  char argv17[20]; // BLUMI for beam
  //
  sDcut=argv[2];
  cout << "**: sDcut " << sDcut << endl;
  //return 0;
  sprintf(argv1,"%s",argv[1]);
  sprintf(argv2,"%s",argv[2]);
  sprintf(argv3,"%s",argv[3]);
  sprintf(argv4,"%s",argv[4]);
  sprintf(argv5,"%s",argv[5]);
  sprintf(argv6,"%s",argv[6]);
  sprintf(argv7,"%s",argv[7]);
  sprintf(argv8,"%s",argv[8]);
  sprintf(argv9,"%s",argv[9]);

  cout << "-argc " << argc
    //<< " argv0 " << argv0
       << " argv1 " << argv1
       << " argv2 " << argv2
       << " argv3 " << argv3
       << " argv4 " << argv4
       << " argv5 " << argv5
       << " argv6 " << argv6
       << " argv7 " << argv7
       << " argv8 " << argv8
       << " argv9 " << argv9
       << endl;
  //
  if(strncmp(argv[3],"e1400",5)==0) {
    fEcm=1400.; iECM=1400;
  } else if(strncmp(argv[3],"e2500",5)==0) {
    fEcm=3000.; iECM=3000;
  } else {
    cout << "**:Undefine SqrtOfS value " << endl;
    return 0;
  }
  cout << "**:iECM " << iECM << endl;
  //return 0;
  //
  ippid=atoi(argv[4]);
  if(ippid==0) {
    cout << "**: Bad ippid==0 " << endl;
    return -1;
  } else {
    cout << "**: ippid: " << ippid << endl;
  }
  //
  nTries = atoi(argv[11]);
  if(nTries==0) {
    cout << "**: Bad nTries==0 " << endl;
    return -1;
  } else {
    cout << "**: nTries: " << nTries << endl;
  }
  //
  //
  char * trnenv;
  char cdebug[20]; //
  trnenv = getenv("DEBUG");
  if(trnenv == NULL) {
    cout << " Undefined DEBUG : " << endl;
    return 1;
  } else {
    sprintf(cdebug,"%s",trnenv);
    cout << "**: cdebug is: " << cdebug << endl;
  }
  int debug=0;
  if(strncmp(cdebug,"ON",2)==0) debug=1;
  //
  trnenv = getenv("FUNC");
  if(trnenv == NULL) {
    cout << " Undefined beam function : " << endl;
    return 1;
  } else {
    sprintf(argv16,"%s",trnenv);
    cout << "**: argv16 is: " << argv16 << endl;
  }
  if(strncmp(argv16,"nom00",5)==0) {
    cout << "**: nominal function " << endl;
  }
  //
  //argv[argc]=argv16;
  //cout << "**: argv[argc] " << argv[argc] << " " << argc << endl;
  //
  trnenv = getenv("BLUMI");
  if(trnenv == NULL) {
    cout << " Undefined BLUMI function : " << endl;
    return 1;
  } else {
    sprintf(argv17,"%s",trnenv);
    cout << "**: argv17 is: " << argv17 << endl;
  }
  if(strncmp(argv17,"500",3)==0) {
    cout << "**: one year lumi " << endl;
  }
  //
  //argc++;
  //argv[argc]=argv17;
  //cout << "**: argv[argc] " << argv[argc] << " " << argc << endl;
  //argc++;
  //
  for (int i=0; i<argc; ++i) cout << " argv[" << i << "] " << argv[i] << endl;
  //

  pm10 = atoi(argv[20]);
  nbinx = atoi(argv[21]);
  cout << "**:pm10 " << pm10 << " nbinx " << nbinx << endl;
  //
  trnenv = getenv("TEST");
  int itest=0;
  if(trnenv == NULL) {
    cout << " Undefined TEST : " << endl;
    return 1;
  } else {
    sprintf(argv13,"%s",trnenv);
    cout << "**: argv13 is: " << argv13 << endl;
  }
  //
  if(strncmp(argv13,"YES",3)==0) itest=1;
  if(strncmp(argv13,"FIT",3)==0) itest=4;
  if(strncmp(argv13,"NOFIT",3)==0) itest=4; // disply SPB
  cout << "**: itest is: " << itest << endl;
  //
  trnenv = getenv("SPECSEL");
  if(trnenv == NULL) {
    cout << " Undefined selection: " << endl;
    return 1;
  } else {
    sprintf(argv10,"%s",trnenv);
    cout << "**: argv10 is: " << argv10 << endl;
  }
  //
  int ifit=0;
  trnenv = getenv("FIT");
  if(trnenv == NULL) {
    cout << " Undefined FIT type: " << endl;
    return 1;
  } else {
    sprintf(argv12,"%s",trnenv);
    //ifit=1;
    cout << "**: argv12 is: " << argv12 << endl;
  }
  //cout << "**:Force exit:1: " << endl;
  //return 0;
  //
  if(iECM==3000) {
    if(ippid==205) dSqrtsmin=2070.;
    if(ippid==202) dSqrtsmin=2070.;
    //if(ippid==213) dSqrtsmin=2300.;
    if(ippid==213) dSqrtsmin=2350.;
    //dSqrtsmax=3100.;
    dSqrtsmax=3060.;
    if(ippid==205)  dSqrtsmax=3060.;
  } else if(iECM==1400) {
    if(ippid==205) dSqrtsmin=1200.;
    if(ippid==202) dSqrtsmin=1200.;
    if(ippid==213) dSqrtsmin=1300.;
    dSqrtsmax=1430.;
  } else {
    return 0;
  }
  cout << "**:ippid " << ippid
       << " dSqrtsmin " << dSqrtsmin
       << " dSqrtsmax " << dSqrtsmax << endl;
  //
  //return 0;
  vector<int> vippid;
  vector<string> vsppid;
  vector<float> vxkevt;
  vector<float> vxkevtbr;
  vector<float> vxkevtg;
  vector<float> vxkevtL;
  vector<float> vxkevtgL;
  vector<float> vlumi;
  vector<float> vppxsec;
  vector<float> vhscale;
  vector<string> vptitle;
  vector<int> vcol;
  vcol.clear();
  //
  InitAnal(debug, argv,  vippid , vsppid, vxkevt, vxkevtg, vxkevtL, vxkevtgL, vlumi,
	   vppxsec,vhscale,vptitle, vxkevtbr);
  //
  for (int ip = 0; ip < int(vsppid.size()); ip++) {
    if(vippid[ip]==205) vcol.push_back(1);
    if(vippid[ip]==202) vcol.push_back(1);
    if(vippid[ip]==213) vcol.push_back(1);
    if(vippid[ip]==39) vcol.push_back(6);  //susy
    if(strncmp(argv[4],"202",3)==0) {
      if(vippid[ip]==504) vcol.push_back(2); //
      if(vippid[ip]==505) vcol.push_back(3); // ee
    }
    if(strncmp(argv[4],"213",3)==0) {
      if(vippid[ip]==201) vcol.push_back(2); //
      if(vippid[ip]==510) vcol.push_back(3); //
    }
    if(strncmp(argv[4],"205",3)==0) {
      if(vippid[ip]==501) vcol.push_back(2); //
      if(vippid[ip]==502) vcol.push_back(3); // ee
      if(vippid[ip]==503) vcol.push_back(7); // eenunu
      if(vippid[ip]==506) vcol.push_back(kOrange); // eenunu
    }
    if(vippid[ip]==22) vcol.push_back(3);
    if(vippid[ip]==125) vcol.push_back(4);
    if(vippid[ip]==25) vcol.push_back(2);
    cout << "**: sppid " << vsppid[ip] << " " << vippid[ip] << " ppxsec " << vppxsec[ip]
	 << " vptitle " << vptitle[ip]
	 << " vcol " << vcol[ip] << endl;
  }
  cout << "**: vppxsec " << vppxsec[0] << endl;
  xsec=vppxsec[0];

  //FIT type or display stacked histo(BKG)
  if(strncmp(argv12,"SPB",3)==0 || strncmp(argv12,"SUB",3)==0) ifit=3;
  cout << "**: ifit " << ifit << endl;
  //
  //  char rfname5[100];
  char rfname[80];
  char filetype[80];
  strcpy(filetype,"root");
  //================================================================
  // HERE DETERMINE THE DATA FILE NAME
  getFileName(filetype , rfname , argv, argv10, argv12, argv13);
  //================================================================
  cout << "**: filetype " << filetype << " rfname " << rfname << endl;
  TFile *f1 = TFile::Open(rfname);
  //Data
  //  f1->ls();
  f1->cd();
  //
  char psname[120];
  char filetypeb[120];
  sprintf(filetypeb,"%s","posts");
  cout << "**: filetypeb " << filetypeb << " ilast " << ilast << " " << argv[ilast] << endl;
  //if(strncmp(argv[15],"STACK",5)==0 || strncmp(argv12,"NOFIT",5)==0) {
  //  getFileName(filetypeb , psname , argv, argv10, argv12, argv13);//cannot work as psname is empty
  //} else {
  if(pm10==0) {
    sprintf(psname,"%s/%s/%s/%s/%s/%s%s%s%s%s%s%s%s%s%s%s.eps",
	    "/afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/fit",argv[4],"hist",argv[4],argv[10],//path
	    "FITh1funisredep",argv[21],argv[4],argv[2],argv16,argv[12],argv17,"_",argv[19],"_",argv[3]//filename
	    );
  } else {
    std::cout << "pm != 10, cannot proceed"  << std::endl;
    return 0;
  }
    //}
  cout << "**: filetypeb " << filetypeb << " psname " << psname << " argv[ilast] " << argv[ilast] << endl;
  //
  //return 0;
  int sel=0;
  trnenv = getenv("SEL");
  if(trnenv == NULL) {
    cout << "**: Undefined selection type: " << endl;
    return 1;
  } else {
    if(strcmp(trnenv,"1")==0) sel=1; // no rebin
    if(strcmp(trnenv,"2")==0) sel=2; // rebin
    cout << "**: Selection type:1=norebin,2=rebin " << sel << endl;
  }
  dsmear=0;
  if(strncmp(argv8,"smear2e-5",9)==0) dsmear=0.00002;
  if(strncmp(argv8,"smear4e-5",9)==0) dsmear=0.00004;
  if(strncmp(argv8,"smear6e-5",9)==0) dsmear=0.00006;
  if(strncmp(argv8,"smear8e-5",9)==0) dsmear=0.00008;
  if(strncmp(argv8,"smear2e-4",9)==0) dsmear=0.0002;
  //
  fixjer=0;
  fixnorm=0;
  if(strncmp(argv[13],"fixj",4)==0)fixjer=1;
  if(strncmp(argv[13],"fixn",4)==0)fixnorm=1;
  if(strncmp(argv[4],"202",3)==0) {
    dsmear=0.00004;
  }
  if(strncmp(argv[4],"213",3)==0) {
    dsmear=0.00004;
  }
  //
  //
  TCanvas *c1 = new TCanvas("c1"," Plots ",0,0,800,700); // cdr style
  //
  if(debug==1) {
    c1->Divide(3,2);
  } else {
    if(strncmp(argv13,"FUNC",4)==0) {
      cout << "**: divide (2,1) " << endl;
      c1->Divide(1,1);
    } else {
      if(itest==0) c1->Divide(2,2);
      if(itest==1) c1->Divide(3,2);
      if(itest >1) c1->Divide(1,1);
    }
  }
  //
  int pad=0;
  //  must always be behind
  TPostScript *ps = new TPostScript(psname,111); // portrait
  //TPostScript *ps = new TPostScript(psname,112); // landscape
  ps->NewPage();
  //
  cout << "**:vsppid.size(): " << vsppid.size() << endl;
  vector<string> HistName;
  HistName.clear();          // background histos
  vector<string> HistNameB;  // histo from test tree with cuts
  HistNameB.clear();
  vector<string> HistNameC;  // histos from train tree without cut
  HistNameC.clear();
  int iarg=2;
  //
  char HISTN[20];
  for (int ip = 0; ip < int(vsppid.size()); ip++) {
    strcpy(HISTN,argv[iarg]);
    //cout << "**:1:HISTN    " << HISTN << endl;
    //printf("\nLast char: %c\n",HISTN[(strlen(HISTN)-1)]);
    char ccut[1];
    sprintf(ccut,"%c",HISTN[(strlen(HISTN)-1)]);
    //
    const char * field;
    field=vsppid[ip].c_str();
    char * pch;
    char cppid[20];
    strcpy(cppid,field);
    pch = strtok (cppid,"VC");
    if(debug>0) cout << "**: :ppid:pch " << pch << endl;
    if(debug>0) cout << "**: :ppid:pch " << pch << " ccut " << ccut << endl;
    strcat(HISTN,pch);
    cout << "**:2:HistName :Push  " << HISTN << endl;
    HistName.push_back(HISTN);
    //
    char HISTNB[20];
    strcpy(HISTNB,"H1LPAMC");
    strcat(HISTNB,ccut);
    strcpy(cppid,field);
    pch = strtok (cppid,"VC");
    if(debug>0) cout << "**: :ppid:pch " << pch << endl;
    strcat(HISTNB,pch);
    cout << "**:2:HistNameB:Push  " << HISTNB << endl;
    HistNameB.push_back(HISTNB);
    //
    char HISTNC[20];
    strcpy(HISTNC,"H1LPAMC0");
    strcpy(cppid,field);
    pch = strtok (cppid,"VC");
    if(debug>0) cout << "**: :ppid:pch " << pch << endl;
    strcat(HISTNC,pch);
    cout << "**:2:HistNameC:Push  " << HISTNC << endl;
    HistNameC.push_back(HISTNC);
  } // end for
  //
  //return(0);
  TH1F *H1LPA=NULL;
  const char * H1NAME;
  H1NAME=HistName[0].c_str();
  f1->GetObject(H1NAME,H1LPA);
  cout << "**: H1LPA=H1NAME " << H1NAME << " vhscale " << vhscale[0] << endl;
  if(H1LPA==0 ) {cout << "**: H1LPA error " << H1LPA << endl; return(9);}
  //
  TH1F *H1LPC=NULL;
  //const char * H1NAME;
  H1NAME=HistNameB[0].c_str();
  f1->GetObject(H1NAME,H1LPC);
  if(H1LPC==0 ) { cout << "**: H1LPC error " << H1LPC << endl;
    //return(9);
  }
  cout << "**: H1LPC=H1NAME " << H1NAME << " vhscale " << vhscale[0] << endl;
  //return(0);
  //
  //TH1F * H1LPGSIG;
  //  TH1F * H1LPGSIGNR; // no rebin
  strcpy(HISTN,argv[iarg]);
  //
  fLER=new TF1("fLER",twogaus,-0.0002,0.0002,6);
  Double_t dpar[8];
  int itf1=1; // to use ffR
  itf1=0;
  TF1 * ffR; //
  if(itf1==1) {
    if(ippid==205 || ippid==202 || ippid==213)  {
      cout << "**:read tf1fname: argv[4] " << argv[4] << " argv[3] " << argv[3] << endl;
      char tf1fname[80];
      sprintf(tf1fname,"%s%s%s%s%s","./input/",argv[4],"_",argv[3],"_BX000_H1DPVOPV2A.root");
      cout << "**:tf1fname: " << tf1fname << endl;
      TFile *file1;
      file1=TFile::Open(tf1fname);
      file1->cd();
      file1->ls();
      //ff->ls();
      file1->GetObject("ff",ffR);
      for (int ip = 0; ip < 6; ip++) {
	dpar[ip]=ffR->GetParameter(ip);
	char tline[20];
	sprintf(tline,"      dpar[%d]=%8.3e",ip,dpar[ip]);
	cout << "**:ffR parameters:ip " << ip << " " << tline << endl ;
      }
      //return 0;
    } else {
      cout << "**:Not fit for PPID " << ippid << endl ;
      return 0;
    }
  } else if(itf1==0) {
    if(ippid==202) { //16.06.2001 new twogauss function and parameters
      if(iECM==3000) {
	dpar[0]=2.23330e+03;
	dpar[1]=2.36229e-06;
	dpar[2]=1.43414e-05;
	dpar[3]=8.25361e-01;
	dpar[4]=-9.81328e-06;
	dpar[5]=7.66403e-05;
	cout << "**: fLER set for 202: 3000 " << endl;
      } else if(iECM==1400) {
	//      19.04.2012
	dpar[0]=4.24921e+03;
	dpar[1]=5.99575e-06;
	dpar[2]=1.67826e-05;
	dpar[3]=8.54530e-01;
	dpar[4]=3.01450e-05;
	dpar[5]=1.00000e-04;
	cout << "**: fLER set for 202: 1400 " << endl;
      } else {
	cout <<"**: iECM bug 202 " << endl;
	return -1;
      }
    } else if(ippid==205) {
      if(iECM==3000) {
	dpar[0]=4.74749e+03;
	dpar[1]=1.21797e-06;
	dpar[2]=1.45330e-05;
	dpar[3]=9.57583e-01;
	dpar[4]=6.38386e-06;
	dpar[5]=4.92388e-05;
	cout << "**: fLER set for 205: 3000 " << endl;
      } else if(iECM==1400) {
	//      last fit 19.04.2012
	dpar[0]=3.97828e+03;
	dpar[1]=5.75811e-06;
	dpar[2]=1.53388e-05;
	dpar[3]=9.07952e-01;
	dpar[4]=1.46118e-05;
	dpar[5]=5.00000e-05;
	cout << "**: fLER set for 205: 1400 " << endl;
      } else {
	cout <<"**: iECM bug 205 " << endl;
	return -1;
      }
    } else if(ippid==213) {
      if(iECM==3000) {
	dpar[0]=2.78739e+02;
	dpar[1]=4.23250e-06;
	dpar[2]=1.41598e-05;
	dpar[3]=8.09730e-01;
	dpar[4]=-1.10434e-05;
	dpar[5]=8.16745e-05;
	cout << "**: fLER set for 213, 3000 " << endl;
      } else if(iECM==1400) {
	//      last fit 19.04.2012 BX000SEL2B
	dpar[0]=1.81067e+02;
	dpar[1]=1.00000e-05;
	dpar[2]=1.74885e-05;
	dpar[3]=8.27755e-01;
	dpar[4]=6.34490e-06;
	dpar[5]=1.10000e-04;
	cout << "**: fLER set for 213: 1400 " << endl;
      } else {
	cout <<"**: iECM bug 213 " << endl;
	return -1;
      }
    } else {
      return -1;
    }
  } // end if itf1
  fLER->SetParameters(dpar[0],dpar[1],dpar[2],dpar[3],dpar[4],dpar[5]);
  dsigma1=dpar[2];
  dsigma1=2.e-5;
  dsigma2=dpar[5];
  dfrac  =dpar[3];
  cout << "**:fLer:dsigma1 " << dsigma1 << " dsigma2 " << dsigma2 << endl ;
  // Define the functions
  fU = new TF1("fU", Func_U, 0, 1500, 6);  //unif, add two parameters for low and high bound
  fU->SetParameters(70000,1010,340,3000, 100, 1000);
  //Gausssian function representing the Dres
  fD = new TF1("fD", Func_D, -400, 400, 6);
  double emu=300.;
  double dx1=dsigma1*emu*emu;
  double dx2=dsigma2*emu*emu;
  cout << "**:dx1 " << dx1 << " dx2 " << dx2 << endl;
  fD->SetParameters(1,0,dx1,dfrac,0,dx2);
  //

  //=================================================================================================
  //   HERE DEFINE THE INPUT PATH OF THE LUMI SPECTRUM
  //=================================================================================================

  // Function representing beam spread
  char rfnames[180];
  char rfnamex[180];
  if(pm10==0) {
    sprintf(rfnames,"%s/%s%s_%s.root",
	    argv[23],"RooKfunisredep",argv[4],argv[17]);
    sprintf(rfnamex,"%s/%s%s_%s.root",
	    argv[23],"ThSfunisredep",argv[4],argv[17]);
  } else if(pm10==1) {
    return 0;
  }
  //

  TFile *file = new TFile(rfnames);
  file->ls();
  file->GetObject("fsqrts",fS);
  fS->ls();
  //
  TFile *filex = new TFile(rfnamex);
  filex->ls();
  filex->GetObject("h1LumiS",thS);
  if(thS==0) {
    cout << "**:Unknown histogram thS: " << endl;
    return 0;
  } else {
    int mbinx = thS->GetNbinsX();
    cout << "**:Known histogram thS: " << thS << " mbinx " << mbinx << endl;
    thS->Sumw2();
    thS->Scale(1./(thS->GetSumOfWeights()*thS->GetBinWidth(1)));
  }
  //
  if(iECM==3000) {
    cout << "**:fS->Eval(2990) " << fS->Eval(2990) << endl;
    //cout << "**:thS->GetBinContent(thS->FindBin(2990)) " << thS->GetBinContent(thS->FindBin(2990)) << endl;
  } else if(iECM==1400) {
    cout << "**:fS->Eval(1390) " << fS->Eval(1390) << endl;
  }//
   //return 0;
  fUxfD = new TF1("fUxfD", Func_UxD, 0, 1500, 1);
  fUxfD->SetParameter(0, 300); // value of emu where to draw Delta emu
  if(iECM==3000) {
    fU->SetParameter(3, 3000); //sqrts
  } else if(iECM==1400) {
    fU->SetParameter(3, 1400); //sqrts
  }
  cout << "**:fUxfD : Func_UxD Defined " << " debugUD " << debugUD << endl;
  debugUD=0;
  if(debugUD>0) cout << "**:fUxfD: fUxfD->Eval(300) " << fUxfD->Eval(300) << endl;
  IntfUxfD = new TF1("IntfUxfD", IntFunc_UxD, 0, 1500, 1);
  cout << "**:IntfUxfD : IntFunc_UxD Defined " << endl;
  // L(sqrts)*f(m0,m1,sqrts,emu) x[0]=sqrts, par0=emu
  fUxfS = new TF1("fUxfS", Func_UxS, 0, 1500, 1); //Unif*Sqrts) to use in integration
  if(iECM==3000) {
    fUxfS->SetParameter(0, 2500); //sqrts
  } else if(iECM==1400) {
    fUxfS->SetParameter(0, 1300); //sqrts
  }
  //L(sqrts)*f(m0,m1,sqrts,emu) x[0]=sqrts, par0=emu

  // IntfUxfS = new TF1("IntfUxfS", IntFunc_UxS, 0, 1500, 0); //Integral of Unif*Sqrts
  // debugIUS=0;
  // if(debugIUS>0) {
  //   debugUS=1;
  //   cout << "**:fUxfS: IntfUxfS->Eval(300) " << endl;
  //   cout  << IntfUxfS->Eval(300.) << endl;
  // }

  //L(sqrts)*f(m0,m1,sqrts,emu) x[0]=sqrts, par0=emu
  fUxfSxfD = new TF1("fuxfSxfD", Func_UxSxD, 0, 1500, 1); //L*(Unif*Dres) to use in integration
  //
  //TF1 for convolution of L(sqrts)*f(m0,m1,sqrts,emu)
  IntfUxfSxfD = new TF1("IntfUxfSxfD", IntFunc_UxSxD, 0, 1500, 0 ); //L*Unif*Dres
  //MainTest
  debugIUSD=0;
  if(debugIUSD>0) {
    debugUD=0;  debugIUD=0;
    cout << "**:fUxfSxfD: IntfUxfSxfD->Eval(300) " << endl;
    cout  << IntfUxfSxfD->Eval(300.) << endl;
    return 0;
  }

  ///Here, decide what type of fit we want 

  f1->cd();
  if( strncmp(argv13,"FIT",3)==0 && strncmp(argv12,"SIG",3)==0) {
    cout << "**: FIT SIGnal " << endl;
    int ixnb = H1LPA->GetNbinsX();
    int inmin = 0;
    double dbinw =H1LPA->GetBinWidth(inmin);
    double dxlow =H1LPA->GetBinLowEdge(inmin)+dbinw;
    double dxhigh=H1LPA->GetBinLowEdge(ixnb)+dbinw;
    cout << " ixnb " << ixnb << " dxlow " << dxlow << " dxhigh " << dxhigh << endl;
    char htitlecor[80];
    char htitlesigc0[80];
    char htitlesigcX[80];
    strcpy(htitlecor,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlesigc0,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlesigcX,( (TH1F *) H1LPA)->GetTitle());
    strcat(htitlecor," S Corr");
    strcat(htitlesigc0," S C0");
    strcat(htitlesigcX," S CX");
    H1HISTSIGC0 = new TH1F("H1HISTSIGC0",htitlesigc0,ixnb,dxlow,dxhigh);
    H1HISTSIGCX = new TH1F("H1HISTSIGCX",htitlesigcX,ixnb,dxlow,dxhigh);
    H1HISTSIGCOR   = new TH1F("H1HISTCOR",htitlecor,ixnb,dxlow,dxhigh);
    H1NAME=HistNameB[0].c_str();
    cout << "**:Fit Signal:1:H1NAME " << H1NAME << endl;
    TH1F *H1XXX = (TH1F*)gDirectory->Get(H1NAME);
    float ysnorm=H1XXX->GetEntries()/H1XXX->GetSumOfWeights();
    cout << " Integral H1XXX " << H1XXX->Integral()
	 << " SumOfWei H1XXX " << H1XXX->GetSumOfWeights()
	 << " Entries  H1XXX " << H1XXX->GetEntries() << " ysnorm " << ysnorm << endl;
    H1HISTSIGCX=(TH1F*)H1XXX->Clone();
    H1NAME=HistNameC[0].c_str();
    cout << "**:Fit Signal:2:H1NAME " << H1NAME << endl;
    H1XXX = (TH1F*)gDirectory->Get(H1NAME);
    H1HISTSIGC0=(TH1F*)H1XXX->Clone();
    H1HISTSIGCOR->Divide(H1HISTSIGC0,H1HISTSIGCX,1,1);
    for(Int_t iBin = 0; iBin < H1HISTSIGCOR->GetNbinsX();iBin++){
      double xx =H1HISTSIGCOR->GetBinLowEdge(iBin);
      double dc = H1HISTSIGCOR->GetBinContent(iBin-1);
      cout << "Fit Sig:Cor: i " << iBin << " xx " << xx << " dc " << dc << endl;
    }
    //H1HISTSPBMBKG->Multiply(H1HISTSPBMBKG,H1HISTSIGCOR,1,1);
    H1LPA->Multiply(H1LPA,H1HISTSIGCOR,1,1);
    //
    for(Int_t iBin = 0; iBin < H1LPA->GetNbinsX();iBin++){
      double xx =H1LPA->GetBinLowEdge(iBin);
      double dc = H1LPA->GetBinContent(iBin-1);
      cout << "Fit Sig:H1LPA:i " << iBin << " xx " << xx << " nx " << dc << endl;
    }
    //
    TH1F * hdata2 = (TH1F*)H1LPA->Clone();
    if(strncmp(argv[14],"rebin",5)==0) {
      hdata = (TH1F*)hdata2->Rebin(2,"hdata");
      cout << "**: Rebin Done " << endl;
    } else {
      cout << "**:hdata defined no rebin " << endl;
      hdata=(TH1F*)H1LPA->Clone();
      hdata->SetName("hdata");
    }

    //============================================================================
    // FIT DONE HERE
    //============================================================================
    int status = massFit( ps, c1, pad, hdata, argv, sel, argv10,itest);
    if (status){
      return status;
    }
    //============================================================================
    cout << "**: close psname " << psname << endl;
    ps->Close();
    cout << " file closed after SIG massfit " << endl;
    return(0);
  } else if( strncmp(argv12,"SPB",3)==0 || strncmp(argv12,"SUB",3)==0  ) { //SPB
    cout << "**:SPB or SUB: Fit S+B-B " << endl;
    TH1F * H1HISTSOB;
    // int ixnb = H1LPA->GetNbinsX();
    // int inmin = 0;
    // double dbinw =H1LPA->GetBinWidth(inmin);
    // double dxlow =H1LPA->GetBinLowEdge(inmin)+dbinw;
    // double dxhigh=H1LPA->GetBinLowEdge(ixnb)+dbinw;
    //cout << " ixnb " << ixnb << " dxlow " << dxlow << " dxhigh " << dxhigh << endl;
    char htitlespb[80];
    char htitlebkg[80];
    char htitlesob[80];
    char htitlecor[80];
    char htitlesigc0[80];
    char htitlesigcX[80];
    strcpy(htitlespb,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlebkg,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlesob,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlecor,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlesigc0,( (TH1F *) H1LPA)->GetTitle());
    strcpy(htitlesigcX,( (TH1F *) H1LPA)->GetTitle());
    strcat(htitlespb," SPB ");
    strcat(htitlebkg,"; E [GeV]; dN/dE");
    strcat(htitlesob," S / B");
    strcat(htitlecor," S Corr");
    strcat(htitlesigc0," S C0");
    strcat(htitlesigcX," S CX");
    //
    cout << " htitlebkg " << htitlebkg << endl;
    H1HISTSPB=(TH1F*)H1LPA->Clone();
    H1HISTSPB->Reset();
    H1HISTBKG=(TH1F*)H1LPA->Clone();
    H1HISTBKG->Reset();
    H1HISTBKGF=(TH1F*)H1LPA->Clone();
    H1HISTBKGF->Reset();
    H1HISTBKGT=(TH1F*)H1LPA->Clone();
    H1HISTBKGT->Reset();
    H1HISTSPBMBKG=(TH1F*)H1LPA->Clone();
    H1HISTSPBMBKG->Reset();
    H1HISTSOB=(TH1F*)H1LPA->Clone();
    H1HISTSOB->Reset();
    H1HISTSIGC0=(TH1F*)H1LPA->Clone();
    H1HISTSIGC0->Reset();
    H1HISTSIGCX=(TH1F*)H1LPA->Clone();
    H1HISTSIGCX->Reset();
    H1HISTSIGCOR=(TH1F*)H1LPA->Clone();
    H1HISTSIGCOR->Reset();
    //const char * H1NAME;
    THStack * H1STACKSPB   = new THStack("H1STACKTSPB","Stack S+B");
    //
    // Add signal; now done first
    int ip=0;
    H1NAME=HistName[ip].c_str();
    TH1F *H1LPAB = (TH1F*)gDirectory->Get(H1NAME);
    cout << "**: H1LPAB=H1NAME:Signal    " << H1NAME << endl;
    if(H1LPAB==0 ) {cout << "**: H1LPAB error " << H1LPAB << endl; return(9);}
    H1HISTSPB->Add(H1HISTSPB,H1LPAB,1,1);
    H1STACKSPB->Add(H1LPAB);
    H1LPAB->SetFillColor(0);
    cout << " H1STACKSPB " << H1STACKSPB << endl;
    //
    //int iprint=1;
    const char * H1NAMEB;
    for (ip = 1; ip < int(vsppid.size()); ip++) { // background from testtree
      H1NAME=HistName[ip].c_str();
      f1->GetObject(H1NAME,H1LPAB);
      cout << "**:Backgrounds from H1NAME:testtree    " << H1NAME << endl;
      if(H1LPAB==0 ) {cout << "**: H1LPAB error " << H1LPAB << endl; return 0;}
      float ybnorm1=H1LPAB->GetEntries()/H1LPAB->GetSumOfWeights();
      cout << " H1NAME " << H1NAME << " Integral H1LPAB:cut " << H1LPAB->Integral()
	   << " SumOfWei H1LPAB " << H1LPAB->GetSumOfWeights()
	   << " Entries  H1LPAB " << H1LPAB->GetEntries() << " ybnorm1 " << ybnorm1 << endl;
      //
      H1NAMEB=HistNameC[ip].c_str();
      TH1F *H1LPABB = NULL;
      f1->GetObject(H1NAMEB,H1LPABB);
      if(H1LPABB==0 ) {cout << "**: H1LPABB error " << H1LPABB << endl; return 0;}
      //float ybnorm=H1LPABB->GetEntries()/H1LPABB->GetSumOfWeights();
      //cout << " H1NAMEB " << H1NAMEB << " Integral H1LPABB:no cut " << H1LPABB->Integral()
      //   << " SumOfWei H1LPABB " << H1LPABB->GetSumOfWeights()
      //   << " Entries  H1LPABB " << H1LPABB->GetEntries() << " ybnorm " << ybnorm << endl;
      //
      // ixnb = H1LPAB->GetNbinsX();
      // inmin = 0;
      // dbinw =H1LPAB->GetBinWidth(inmin);
      // dxlow =H1LPAB->GetBinLowEdge(inmin)+dbinw;
      // dxhigh=H1LPAB->GetBinLowEdge(ixnb)+dbinw;
      // float NINTEG = H1LPAB->Integral();
      // float NENTRIES = H1LPAB->GetEntries();
      // cout << " ixnb " << ixnb << " low " << dxlow << " high " << dxhigh
      // 	   << " integ " << NINTEG << " entries " << NENTRIES << endl;
      //
      H1HISTBKG->Add(H1HISTBKG,H1LPAB,1,1);
      H1HISTSPB->Add(H1HISTSPB,H1LPAB,1,1);
      H1LPAB->SetFillColor(vcol[ip]);
      H1LPAB->SetLineColor(vcol[ip]);
      H1STACKSPB->Add(H1LPAB);
    } // end for ip
    //      return(0);
    // Background from TrainTree
    for (ip = 1; ip < int(vsppid.size()); ip++) {
      H1NAME=HistNameB[ip].c_str();
      f1->GetObject(H1NAME,H1LPAB);
      cout << "**:Backgrounds from H1NAME:Traintree    " << H1NAME << endl;
      if( strncmp(argv[2],"H1BPAH",6)==0 ) {
	if(H1LPAB==0 ) {cout << "**: H1LPAB error " << H1LPAB << endl; break;}
      } else {
	if(H1LPAB==0 ) {cout << "**: H1LPAB error " << H1LPAB << endl; return(9);}
      }
      // ixnb = H1LPAB->GetNbinsX();
      // inmin = 0;
      // double dbinw =H1LPAB->GetBinWidth(inmin);
      // double dxlow =H1LPAB->GetBinLowEdge(inmin)+dbinw;
      // double dxhigh=H1LPAB->GetBinLowEdge(ixnb)+dbinw;
      // float NINTEG = H1LPAB->Integral();
      // float NENTRIES = H1LPAB->GetEntries();
      // cout << " ixnb " << ixnb << " low " << dxlow << " high " << dxhigh
      // 	   << " integ " << NINTEG << " entries " << NENTRIES << endl;
      H1HISTBKGT->Add(H1HISTBKGT,H1LPAB,1,1);
    } // end background from traintree
    //
    //Subtract H1HISTBKGT
    H1HISTSPBMBKG->Add(H1HISTSPB,H1HISTBKGT,1,-1);
    for(Int_t iBin = 0; iBin < H1HISTSPBMBKG->GetNbinsX();iBin++){
      double xx =H1HISTSPBMBKG->GetBinLowEdge(iBin);
      double nx = H1HISTSPBMBKG->GetBinContent(iBin-1);
      if(nx>0) cout << " i " << iBin << " xspbmbkg " << xx << " nx " << nx << endl;
    }
    printf("Integral of H1HISTSPBMBKG = %g\n",H1HISTSPBMBKG->Integral()); //
    H1NAME=HistNameB[0].c_str();
    cout << "**:prepare correction:1:H1NAME " << H1NAME << endl;
    TH1F *H1XXX1 = (TH1F*)gDirectory->Get(H1NAME);
    float ysnorm=H1XXX1->GetEntries()/H1XXX1->GetSumOfWeights();
    cout << " Integral H1XXX1 " << H1XXX1->Integral()
	 << " SumOfWei H1XXX1 " << H1XXX1->GetSumOfWeights()
	 << " Entries  H1XXX1 " << H1XXX1->GetEntries() << " ysnorm " << ysnorm << endl;
    H1HISTSIGCX=(TH1F*)H1XXX1->Clone();
    //
    for(Int_t iBin = 0; iBin < H1HISTSIGCX->GetNbinsX();iBin++){
      double xx =H1HISTSIGCX->GetBinLowEdge(iBin);
      double nx = H1HISTSIGCX->GetBinContent(iBin-1);
      if(nx>0) cout << " i " << iBin << " xcx " << xx << " nx " << nx << endl;
    }
    //    effi correction
    if(strncmp(argv16,"hie",3)==0 || strncmp(argv16,"his",3)==0 ) {
      cout << "**: compute syst err hie " << endl;
      for(Int_t iBin = 0; iBin < H1HISTSIGCX->GetNbinsX();iBin++){
	double xx =H1HISTSIGCX->GetBinLowEdge(iBin);
	double nx = H1HISTSIGCX->GetBinContent(iBin-1);
	double nxysn=nx*ysnorm;
	double nxysnc=0;
	double nxc=0;
	if(nxysn>10) {
	  nxysnc=gRandom->Gaus(nxysn,sqrt(nxysn));
	  nxc=nxysnc/ysnorm;
	  if(nxc>0) H1HISTSIGCX->SetBinContent(iBin-1,nxc);
	} else {
	  nxc=nx;
	}
	nxc = H1HISTSIGCX->GetBinContent(iBin-1);
	cout << "**:hie:i " << iBin << " xx " << xx << " nx " << nx << " nxysn " << nxysn << " nxc " << nxc << endl;
      }
    }
    H1NAME=HistNameC[0].c_str();
    cout << "**:prepare correction:2:H1NAME " << H1NAME << endl;
    TH1F *H1XXX2 = (TH1F*)gDirectory->Get(H1NAME);
    float ybnorm=H1XXX2->GetEntries()/H1XXX2->GetSumOfWeights();
    cout << "**:Signal:Integral no cut " << H1XXX2->Integral()
	 << " SumOfWei " << H1XXX2->GetSumOfWeights()
	 << " Entries  " << H1XXX2->GetEntries() << " ybnorm " << ybnorm << endl;

    H1HISTSIGC0=(TH1F*)H1XXX2->Clone();
    for(Int_t iBin = 0; iBin < H1HISTSIGC0->GetNbinsX();iBin++){
      double xx =H1HISTSIGC0->GetBinLowEdge(iBin);
      double nx = H1HISTSIGC0->GetBinContent(iBin-1);
      if(nx>0) cout << " i " << iBin << " xc0 " << xx << " nx " << nx << endl;
    }

    H1HISTSIGCOR->Divide(H1HISTSIGC0,H1HISTSIGCX,1,1);

    for(Int_t iBin = 0; iBin < H1HISTSIGCOR->GetNbinsX();iBin++){
      double xx =H1HISTSIGCOR->GetBinLowEdge(iBin);
      double dc = H1HISTSIGCOR->GetBinContent(iBin-1);
      cout << "Fit S+B-B:Cor:i " << iBin << " xx " << xx << " dc " << dc << endl;
      if(dc>2) H1HISTSIGCOR->SetBinContent(iBin-1,1.);
    }
    //
    H1HISTSPBMBKG->Multiply(H1HISTSPBMBKG,H1HISTSIGCOR,1,1);
    if(strncmp(argv12,"SPB",3)==0) hdata=(TH1F*)H1HISTSPB->Clone();
    if(strncmp(argv12,"SUB",3)==0) hdata=(TH1F*)H1HISTSPBMBKG->Clone();
    hdata->SetName("hdata");
    cout << "**: HERE B  " << endl;
    for(Int_t iBin = 0; iBin < H1HISTSPBMBKG->GetNbinsX();iBin++){
      double xx =H1HISTSPBMBKG->GetBinLowEdge(iBin);
      double nx =H1HISTSPBMBKG->GetBinContent(iBin-1);
      if(nx>0) cout << "SPBMBKG: i " << iBin << " xc0 " << xx << " nx " << nx << endl;
    }
    //
    hbkg =(TH1F*)H1HISTBKG->Clone();
    hbkg->SetName("hbkg");
    hbkgT =(TH1F*)H1HISTBKGT->Clone();
    hbkgT->SetName("hbkgT");
    hbkgF =(TH1F*)H1HISTBKG->Clone();
    hbkgF->SetName("hbkgF");
    //cout << "**: HERE C  " << H1ECM125NC << endl;
    //hbeam=(TH1F*)H1ECM125NC->Clone();
    //hbeambox=(TH1F*)H1ECM125NC->Clone();
    cout << "**: HERE D " << endl;
    if(strncmp(argv12,"SPB",3)==0) {
      printf("Integral of hdata  (S+B) = %g\n",hdata->Integral()); //S+B
      printf("Integral of hbkg   (B) = %g\n",hbkg->Integral()); //B
      printf("Integral of H1LPAB (S) = %g\n",H1LPAB->Integral()); //S
    }
    if(strncmp(argv12,"SUB",3)==0) {
      printf("Integral of hdata  (S+B-B) = %g\n",hdata->Integral()); //
      printf("Integral of hbkg   (B) = %g\n",hbkg->Integral()); //B
      printf("Integral of H1LPAB (S) = %g\n",H1LPAB->Integral()); //S
    }
    cout << "**: ifit " << ifit << endl;
    //int kfit=0;
    if(strncmp(argv13,"FIT",3)==0) { //SPB or SPBMB
      if(debug==0) pad=0;
      if(strncmp(argv12,"SUB",3)==0) { //SPBMB
	cout << "**:call massfit: sel " << sel << " argv10 " << argv10 << endl;
	xsub=1;

	// TH1F* hdataclone = static_cast<TH1F*>(hdata->Clone("hdataclone"));
	// hdataclone->Reset();
	// hdataclone->FillRandom(hdata, hdata->Integral());
	// //    TH1F* temp = hdata;
	// hdata = hdataclone;

	int status = massFit( ps, c1, pad, hdata, argv, sel, argv10,itest);
	if (status){
	  return status;
	}
	cout << "**: SUB Fit: close psname " << psname << endl;
	//      temp to save a C file
	//c1->SaveAs(mcname);
	//cout << "**: mc file closed " << mcname << endl;
	ps->Close();
	cout << " file closed " << endl;
	return(0);
      }
    }// end if fit
  }//endif(argv12
  return(0);
  ///// END Main
}
//
int massFit(TPostScript * ps , TCanvas * c1, int & pad, TH1F * hdata, char * argv[], int sel,
	     char * argv10, int itest, TH1F * )
{
  cout << "**: massFit " << " ippid " << ippid << " sel " << sel << " itest " << itest
       << " pad " << pad << " fEcm " << fEcm
       << endl;
  //
  int fullMinuit = 1;
  nbins = hdata->GetNbinsX();
  hBox = new TH1F("hBox"," ",hdata->GetNbinsX(), hdata->GetBinLowEdge(1),
		  hdata->GetBinLowEdge(hdata->GetNbinsX())+hdata->GetBinWidth(1));
  //
  //  int fixmass=0;
  //if(strncmp(argv[12],"fixm",4)==0)fixmass=1;
  cout << "**: massFit nbins " << nbins << " fixjer " << fixjer
       << " fixnorm " << fixnorm << " nTries " << nTries << endl;
  //instantiate Minuit for 3 parameters
  int kmpar=3;
  TMinuit *myMinuit = new TMinuit(kmpar);
  //set pointer to function
  myMinuit->SetFCN(fcn);
  //
  Double_t arglist[2];
  Int_t ierflg = 0;
  arglist[0] = 1; // up value for chi2
  myMinuit->mnexcm("SET ERR", arglist ,1,ierflg);
  //
  //set default start values
  static Double_t vstart[7] = {1030.0, 370.0, 71.0, 0.21, 0.00004, 0.021, 0.3 };
  static Double_t step[7]     = {5.0    , 5.0 , 0.05, 0.0001, 0.0000005,  0.0001, 0.001};
  //static Double_t step[7]     = {0.5    , 0.5 , 0.0001, 0.0001, 0.0000005,  0.0001, 0.001};
  //for point L the lower limit must be lower; msel=1010; mchi0=340.
  //static Double_t lowrange[7] = {800.  , 200., 0.95  , 0.0  , 0.0   ,  0.0  , 0.1};
  //static Double_t highrange[7] = {1200. , 540., 1.05, 0.40 , 0.0005,   0.04 , 0.8};
  static Double_t lowrange[7] = {900.  , 300., 10.  , 0.0  , 0.0   ,  0.0  , 0.1};
  static Double_t highrange[7] = {1100. , 440., 1000., 0.40 , 0.0005,   0.04 , 0.8};
  //
  if(strncmp(argv[12],"M",1)==0 ) {
    cout << "**: Set range for model 3 " << endl;
    if(strncmp(argv[4],"213",3)==0) {
      lowrange[0]=620;  //snue
      lowrange[1]=440;  //chi1
      highrange[0]=660;
      highrange[1]=520;
      vstart[0]=640.;
      vstart[1]=480.;
      vstart[2]=75000./20.;
      lowrange[2]=60000./20.;
      highrange[2]=90000./20.;
      cout << "**: range set for 213 M " << endl;
    } else if(strncmp(argv[4],"205",3)==0) {
      lowrange[0]=520;  //smuon
      lowrange[1]=320;  //chi0
      highrange[0]=580;
      highrange[1]=380;
      vstart[0]=555.;
      vstart[1]=352.;
      vstart[2]=45000./2.;
      lowrange[2]=40000./2.;  //smuon
      highrange[2]=50000./2.;
      cout << "**: range set for 205 M " << endl;
    } else if(strncmp(argv[4],"202",3)==0) {
      lowrange[0]=520;  //smuon
      lowrange[1]=320;  //chi0
      highrange[0]=580;
      highrange[1]=380;
      vstart[0]=555.;
      vstart[1]=352.;
      vstart[2]=450000./6.;
      lowrange[2]=400000./6.;
      highrange[2]=600000./6.;
      cout << "**: range set for 202 M " << endl;
    } else {
      cout << "**: range set bug " << endl;
      return 1;
    }
  } else if(strncmp(argv[12],"L",1)==0 ) {
    cout << "**: Set range for model 2=L " << endl;
    if(strncmp(argv[4],"205",3)==0) {
      lowrange[0]=970;  //smuon
      lowrange[1]=320;  //chi0
      highrange[0]=1030;
      highrange[1]=380;
      //vstart[0]=1007.1;
      //vstart[1]=342.1;
      vstart[0]=1000.1;
      vstart[1]=350.1;
      if(dxnorm<10) {
	vstart[2]=63000.;
	lowrange[2]=25000;  //smuon
	highrange[2]=65000.;
      } else {
	vstart[2]=0.67;
	lowrange[2]=0.57;  //smuon
	highrange[2]=0.77;
      }
      cout << "**: range set for 205 L " << endl;
    } else if(strncmp(argv[4],"202",3)==0) {
      //vstart[0]=1013.1;
      //vstart[1]=342.1;
      lowrange[0]=960;  //smuon
      lowrange[1]=310;  //chi0
      highrange[0]=1040;
      highrange[1]=390;
      vstart[0]=1000.1;
      vstart[1]=345.1;
      if(dxnorm<10) {
	vstart[2]=450000;
	lowrange[2]=400000;
	highrange[2]=600000.;
      } else {
	vstart[2]=6.1;
	lowrange[2]=5.0;
	highrange[2]=7.1;
      }
      cout << "**: range set for 202 L " << endl;
    } else if( strncmp(argv[4],"213",3)==0 ) {
      lowrange[0]=1050;  //snu
      lowrange[1]=620;  //chi1pm
      highrange[0]=1150;
      highrange[1]=660;
      vstart[0]=1090.1;
      vstart[1]=635.1;
      if(dxnorm<10) {
	vstart[2]=75000;
	lowrange[2]=60000.;
	highrange[2]=90000.;
      } else {
	vstart[2]=0.86;
	lowrange[2]=0.86;
	highrange[2]=0.96;
      }
      cout << "**: range set for 213 " << endl;
    } else {
      cout << "**: range set bug " << endl;
      return  1;
    }
  }
  //
  //float integdat=hdata->Integral();
  if(ippid==205 || ippid==202 || ippid==213) {
    myMinuit->mnparm(0, "mass_smuon", vstart[0], step[0], lowrange[0],highrange[0],ierflg);
    myMinuit->mnparm(1, "mass_chi0", vstart[1], step[1], lowrange[1],highrange[1],ierflg);
    myMinuit->mnparm(2, "norm", vstart[2], step[2], lowrange[2],highrange[2],ierflg);
  } else {
    return 1;
  }
  //
  //TGraph *gr2;
  //
  double mass0,mass1;
  double err0,err1;
  double fea0,fea1,fea2,fea3,fea4;
  double errf0,errf1,errf2,errf3,errf4;
  //
  float fE0, fE1;
  double dchi2;
  cout << "**: lowrange[0] " << lowrange[0] << " highrange[0] " <<  highrange[0]
       << " lowrange[1] " << lowrange[1] << " highrange[1] " <<  highrange[1]
       << " lowrange[2] " << lowrange[2] << " highrange[2] " <<  highrange[2]
       << endl;
  //
  if(fullMinuit==1){
    //double err = 1.0; // chi2
    double prec = 1e-10;
    dprec=prec;
    myMinuit->mnexcm("SET EPS",  &prec,     1, ierflg);
    //-Start of job time
    time_t t1;
    t1 = time(NULL);
    string ct1;
    cout << " time at 1: " << ctime(&t1) << endl;
    //
    arglist[0] = 1000; // nuber of calls
    arglist[1] = 1.0; // tolerance default is 0.1*UP
    myMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);
    //
    // double stra=2.;
    // myMinuit->mnexcm("SET STR",  &stra, 1, ierflg);
    // myMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);
    //
    Double_t amin,edm,errdef;
    Int_t nvpar,nparx,icstat;
    myMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
    myMinuit->mnprin(1,amin);
    myMinuit->mnprin(2,edm);
    //
    myMinuit->GetParameter(0,mass0,err0);
    myMinuit->GetParameter(1,mass1,err1);
    myMinuit->GetParameter(2,fea0,errf0);
    myMinuit->GetParameter(3,fea1,errf1);
    myMinuit->GetParameter(4,fea2,errf2);
    myMinuit->GetParameter(5,fea3,errf3);
    myMinuit->GetParameter(6,fea4,errf4);
    dchi2=amin;
    cout << " " << endl;
    cout << "Mass Slepton " << mass0 << "+/-" << err0 << endl;
    cout << "**: Fit " << argv[18] << ":" << argv[17] << ":" << argv[2];
    cout << ":M Slepton " << mass0 << "+/-" << err0 << " : ";
    cout << "M Gaugino " << mass1 << "+/-" << err1 << " : chi2 " << amin
	 << " box " << argv[11] << " norm " << fea0 << endl;
    //
    TString tline;
    cout << "**:Fill tline " << endl;
    tline = TString(Form("**:Fit: %s %s:%s:%s:mS %7.2f +/-%7.2f :mG %7.2f +/-%7.2f :chi2 %6.2f %s norm %5.2f +/-%5.2f %s %8.3e %d ",
			argv[4],
			argv[17],argv[19],argv[2],
			mass0,err0,mass1,err1,amin,argv[11],fea0,errf0,sChi2.c_str(),dprec,pm10));
    cout << "**:Write tline " << endl;
    cout << tline << endl;
    if(iECM==3000) {
      fU->SetParameter(3,3000); //sqrts
    } else if(iECM==1400) {
      fU->SetParameter(3,1400); //sqrts
    }
    fU->SetParameter(0,fea0*dxnorm);
    fU->SetParameter(1,mass0);
    fU->SetParameter(2,mass1);

    char fileout[280];
    if(pm10==0) {
      sprintf(fileout,"%s%d%s%s%s%s%s%s%s.txt",
	      "./FitResult_",nbinx,argv[4],argv[3],argv[2],argv[17],argv[18],"_",argv[19]);
      //                    nbinx,argv[4],argv[3],argv[2],argv[18],argv[19],"_",argv[17],".txt");
    } else {
      return 1;
    }
    TFile*f_fit = TFile::Open(Form("%s%d%s%s%s%s%s%s%s.root",
				   "./FitResult_",nbinx,argv[4],argv[3],argv[2],argv[17],argv[18],"_",argv[19]),"RECREATE");
    TTree*t = new TTree("FitResult","FitResult");
    t->Branch("slepmass",&mass0,"slepmass/D");
    t->Branch("slepmass_e",&err0,"slepmass_e/D");
    t->Branch("gaugmass",&mass1,"gaugmass/D");
    t->Branch("gaugmass_e",&err1,"gaugmass_e/D");
    t->Branch("chi2",&amin,"chi2/D");
    t->Branch("edm",&edm,"edm/D");
    t->Branch("covQual",&icstat,"covQual/I");
    Int_t f_status = myMinuit->GetStatus();
    t->Branch("status",&f_status,"status/I");
    t->Branch("norm",&fea0,"norm/D");
    t->Branch("norm_e",&errf0,"norm_e/D");
    int parameter_id = atoi(argv[24]); // the parameter that had its variation tested
    t->Branch("pid",&parameter_id,"pid/I");
    int binx = atoi(argv[25]);    
    int biny = atoi(argv[26]);
    int binz = atoi(argv[27]);
    t->Branch("binx",&binx,"binx/I");
    t->Branch("biny",&biny,"biny/I");
    t->Branch("binz",&binz,"binz/I");
    hdata->Write();
    t->Fill();
    t->Write();
    f_fit->Close();
    cout << "**:fileout " << fileout << endl;
    //cout << "**:Force exit:5: " << endl;
    //exit(0);

    ofstream fp(fileout);
    if(!fp) {
      cout << "**: File open error " << fileout << endl;
      return 1;
    } else {
      cout << "**: File open success " << fileout << endl;
    }
    fp << tline;
    fp.close();
    //
    //cout << " fea2 " << fea2 << " fea3 " << fea3 << " fea4 " << fea4 << endl;
    //float fEcm=3000;
    float fS2=fEcm*fEcm;
    float fA=fEcm/4.;
    float fB=1.-(mass1*mass1)/(mass0*mass0);
    float fC=sqrt(1.-4.*mass0*mass0/fS2 );
    fE0=fA*fB*(1.+fC);
    fE1=fA*fB*(1.-fC);
    cout << "**: fE0 " << fE0 << " fE1 " << fE1 << endl;
    cout << " " << endl;
    // Double_t fitpar[8];
    // fitpar[0]=mass0;
    // fitpar[1]=mass1;
    // fitpar[2]=fea0*dxnorm;
    // fitpar[3]=fea1;
    // fitpar[4]=fea2;
    // fitpar[5]=fea3;
    // fitpar[6]=fea4;
  } else {
    cout << "**:fullMininuit BUG " << endl;
    return 1;
  } // end if(fullMinuit)
  //
  c1->SetFillColor(0);
  if(pad==4) { c1->Update(); ps->NewPage(); pad = 0; }
  pad++;
  c1->cd(pad);
  gPad->SetLogy(0);
  c1->SetFillColor(0);  //canvas color // ok
  gStyle->SetOptStat(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetStatColor(0);
  //
  double nentries;
  //
  if(strncmp(argv[4],"205",3)==0) hdata->SetTitle("dN/dE(#mu^{#pm});E [GeV];Events");
  if(strncmp(argv[4],"201",3)==0) hdata->SetTitle("dN/dE(e);E [GeV];dN/dE");
  if(strncmp(argv[4],"202",3)==0) hdata->SetTitle("dN/dE(e^{#pm});E [GeV];Events");
  if(strncmp(argv[4],"213",3)==0) hdata->SetTitle("dN/dE(e^{#pm});E [GeV];Events");
  //
  cout << "** Draw plots " << endl;
  hdata->SetLineColor(4);
  double xp0=hdata->GetMaximum();
  hdata->GetYaxis()->SetRangeUser(0,xp0*1.4);
  hdata->Draw("E");
  if(sChi2=="AF" ) {
    IntfUxfSxfD->SetLineColor(4);
    cout << "**: IntfUxfSxfD->Draw " << endl;
    IntfUxfSxfD->Draw("same");
    //double dentries=IntfUxfSxfD->Integral(dSqrtsmin,dSqrtsmax);
    //cout << "**:dentries " << dentries << endl;
  }
  //
  nentries=hdata->Integral();
  //
  char text1[80];
  char text2[80];
  char text3[80];
  char text4[80];
  //
  TPaveText *htit;
  if(itest==4) {
    htit = new TPaveText(0.30,0.88,0.90,1.,"NDC");
    htit->SetTextSize(0.03);
  } else {
    htit = new TPaveText(0.20,0.88,0.90,1.,"brNDC");
    htit->SetTextSize(0.03);
  }
  htit->SetTextColor(4);
  htit->SetTextAlign(12); // centered vertically and left
  char tline[80];
  char tline2[80];
  sprintf(text1," %8.2f #pm %5.2f ",mass0,err0);
  sprintf(text2," %8.2f #pm %5.2f ",mass1,err1);
  if(strncmp(argv[4],"205",3)==0) strcpy(tline,"M #tilde{#mu} = ");
  if(strncmp(argv[4],"201",3)==0) strcpy(tline,"M #tilde{e_{L}} = ");
  if(strncmp(argv[4],"202",3)==0) strcpy(tline,"M #tilde{e_{R}} = ");
  if(strncmp(argv[4],"213",3)==0) strcpy(tline,"M #tilde{#nu} = ");
  strcat(tline,text1);
  //
  htit->AddText(tline);
  strcpy(tline,"M #tilde{#chi^{0}} = ");
  strcat(tline,text2);
  htit->AddText(tline);
  sprintf(text3," %8.2f ",fE0);
  sprintf(text4,"%8.2f",fE1);
  //
  strcpy(tline2,"E_{H}=  ");
  strcat(tline2,text3);
  strcat(tline2," ,E_{L}= ");
  strcat(tline2,text4);
  htit->AddText(tline2);
  htit->SetFillColor(0);
  //htit->Draw();
  //
  TLegend *leg;
  int xcdr=1;
  if(xcdr==0) {
    leg = new TLegend(0.175,0.75,0.855,0.91);
  } else {
    leg = new TLegend(0.205,0.75,0.855,0.91);
  }
  leg->SetTextSize(0.03);
  if(itest==4)  leg->SetTextSize(0.03);
  char leg1[80];
  char leg2[80];
  char leg3[80];
  //char leg4[80];
  //char texL1[80];
  strcpy(leg1,argv[4]);
  if(strncmp(argv[4],"202",3)==0) strcpy(leg1,
					 "#tilde{e_{R}^{+}}+#tilde{e_{R}^{-}} #rightarrow e^{+}+e^{-}+#tilde{#chi_{1}^{0}}+#tilde{#chi_{1}^{0}}" );
  if(strncmp(argv[4],"213",3)==0) strcpy(leg1,
					 "#tilde{#nu_{e}}+#tilde{#nu_{e}} #rightarrow e^{+}+e^{-} + #tilde{#chi_{1}^{-}} +#tilde{#chi_{1}^{+}} " );
  if(strncmp(argv[4],"205",3)==0) strcpy(leg1,
					 "#tilde{#mu_{R}^{+}}+#tilde{#mu_{R}^{-}} #rightarrow #mu^{+}+#mu^{-}+#tilde{#chi_{1}^{0}}+#tilde{#chi_{1}^{0}}" );
  //
  if(strncmp(argv[4],"202",3)==0 || strncmp(argv[4],"205",3)==0) {
    //if(strncmp(argv[8],"smear2e-5",9)==0) strcat(leg1,"; #delta P_{t}/P_{t}^{2}=2.10^{-5}" );
    //if(strncmp(argv[8],"smear6e-5",9)==0) strcat(leg1,"; #delta P_{t}/P_{t}^{2}=6.10^{-5}" );
  }
  if(strncmp(argv10,"DST",3)==0) strcat(leg1,"DST: ");
  leg->AddEntry(hdata,leg1,"LE")->SetTextColor(4);
  strcpy(leg1,argv[9]);
  if(strncmp(argv[9],"pol5",4)==0) strcpy(leg1,"Pol: e^{-}L20,R80; e^{+}L0,R0" );
  if(strncmp(argv[9],"pol0",4)==0) strcpy(leg1,"Pol: e^{-}L 0,R 0; e^{+}L 0,R 0" );
  //
  strcpy(leg3," ");
  char cerr[20];
  if(strncmp(argv[4],"205",3)==0) sprintf(cerr," #delta P_{t}/P_{t}^{ 2}= %8.1e ",dsmear);
  if(strncmp(argv[4],"202",3)==0) sprintf(cerr," #delta P_{t}/P_{t}^{ 2}= %8.1e ",dsmear);
  if(strncmp(argv[4],"213",3)==0) sprintf(cerr," #delta P_{t}/P_{t}^{ 2}= %8.1e ",dsmear);
  cout << "**: cerr " << cerr << endl;
  //
  //char legtit[40];
  strcpy(leg2,"Fit: S = ");
  if(xsub==1) strcpy(leg2,"Fit:S+B(Data)-B(MC)");
  cout << "**: massfit: argv2[2] " << argv[2] << endl;
  //
  sprintf(text2,", events:%5.0f",nentries);
  strcat(leg2,text2);
  //sprintf(leg4,";%d;%d",fixmass,fixjer);
  //if(strncmp(argv[4],"202",3)==0) sprintf(leg4,";%d;%d",effcor,fixjer);
  //if(strncmp(argv[4],"213",3)==0) sprintf(leg4,";%d;%d",effcor,fixjer);
  leg->AddEntry(hBox,leg2,"L")->SetTextColor(1);
  //leg->SetHeader("The Legend Title");
  //
  sprintf(text1," %8.2f #pm %5.2f ",mass0,err0);
  if(strncmp(argv[4],"205",3)==0) strcpy(tline,"M #tilde{#mu} = ");
  if(strncmp(argv[4],"201",3)==0) strcpy(tline,"M #tilde{e_{L}} = ");
  if(strncmp(argv[4],"202",3)==0) strcpy(tline,"M #tilde{e_{R}} = ");
  if(strncmp(argv[4],"213",3)==0) strcpy(tline,"M #tilde{#nu} = ");
  strcat(tline,text1);
  leg->AddEntry(hBox,tline,"P")->SetTextColor(1);
  strcpy(tline,"M #tilde{#chi^{0}} = ");
  if(strncmp(argv[4],"213",3)==0) strcpy(tline,"M #tilde{#chi_{1}}^{#pm} =");
  sprintf(text2," %8.2f #pm %5.2f ",mass1,err1);
  strcat(tline,text2);
  sprintf(text2,", #chi^{2}/ ndf %3.1f / %d",dchi2,Ndfd);
  if(strncmp(argv[4],"213",3)==0) sprintf(text2,", #chi^{2} / ndf %3.1f /35",dchi2);
  strcat(tline,text2);
  leg->AddEntry(hBox,tline,"P")->SetTextColor(1);
  leg->SetFillColor(0);
  leg->Draw();
  cout << "** Draw plots done in massfit " << endl;
  return 0;
}//end massfit

//______________________________________________________________________________
void fcn(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t )
{
  TStopwatch timer2;
  //cout << "**: fcn: iflag " << iflag << " nTries " << nTries << endl;
  par[2]=par[2]*dxnorm;
  Double_t mass_smuon = par[0];
  Double_t mass_chi0 = par[1];
  Double_t dbnorm = par[2];
  //if(Icall < 2) {
  cout << "**:P0 " << par[0] << " P1 " << par[1] << " P2 " << par[2] << endl;
  //}
  if(dbnorm<0.1) dbnorm=1;
  fU->SetParameter(0,par[2]);
  fU->SetParameter(1,par[0]);
  fU->SetParameter(2,par[1]);

  //AS: Calculate the boundaries of the Uniform distribution
  const double mass0( fU->GetParameter(1) );
  const double mass1( fU->GetParameter(2) );
  const double localfEcm ( fU->GetParameter(3) );
  // if(debugIUD>0) cout << "**:IntFunc_UxD:x " << x[0]
  // 		      << " dx1 " << dx1 << " fEcm " << localfEcm << endl;
  const double fS2   ( localfEcm*localfEcm );
  const double fA    ( localfEcm*0.25 );
  const double fB    ( 1.-(mass1*mass1)/(mass0*mass0) );
  const double fC    ( sqrt(1.-4.*mass0*mass0/fS2 ) );
  const double fatfb ( fA*fB );
  const double fE0   ( fatfb*(1.+fC) );
  const double fE1   ( fatfb*(1.-fC) );

  fU->SetParameter(4, fE0);
  fU->SetParameter(5, fE1);

  //
  // Double_t ber0 = par[2];
  // Double_t ber1 = par[3];
  // Double_t ber2 = par[4];
  // Double_t ber3 = par[5];
  // Double_t ber4 = par[6];
  //
  // Double_t pres = 2.8e-5;
  // pres = dsmear;
  //
  Int_t iFirst = 6;
  Double_t sqrts0 =fEcm;
  Double_t beta;
  Double_t emuon0, emuon1;//, xdbnorm;
  //
  // Double_t x = 0.;
  // Double_t xl = 0.;
  Double_t sqrts = sqrts0;
  //
  Float_t norm = hdata->Integral();
  //
  beta = sqrt(1. - pow(mass_smuon,2)/pow((sqrts0/2.),2));
  emuon0 = (sqrts0/4.) * (1. - beta) *
    (1. - pow(mass_chi0,2)/pow(mass_smuon,2));
  emuon1 = (sqrts/4.) * (1. + beta) *
    (1. - pow(mass_chi0,2)/pow(mass_smuon,2));
  //xdbnorm=dbnorm;
  //
  if( iprpp < 2 ) {
    cout << "**:emuon0 " << emuon0 << " emuon1 " << emuon1 << " norm " << norm  << endl;
  }
  int nRTries=0;

  //AS not used later-on
  // double dsqrt=3000.;
  // if(iECM==3000) {
  //   dsqrt=3000.;
  // } else if(iECM==1400) {
  //   dsqrt=1400.;
  // }
  //To get tEL and EH values
  // beta = sqrt(1. - mass_smuon*mass_smuon/((dsqrt/2.)*(dsqrt/2.)));
  // emuon1 = (dsqrt/4.) * (1. + beta) *
  //   (1. - (mass_chi0*mass_chi0)/(mass_smuon*mass_smuon));
  // emuon0 = (dsqrt/4.) * (1. - beta) *
  //   (1. - (mass_chi0*mass_chi0)/(mass_smuon*mass_smuon));
  // 

  Double_t chi2=0.;
  //int ipr1=0;
  //Float_t contBC=0;
  Ndfd=0;
  //
  if(sChi2=="AF") {
    double dnorm=fU->GetParameter(0);
    double dmassS=fU->GetParameter(1);
    double dmassG=fU->GetParameter(2);
    double dSqrts=fU->GetParameter(3);
    if(Icall<2) {
      cout << "**:Fit:AF:Icall " << Icall << " dnorm " << dnorm << " mS " << dmassS << " mG " 
	   << dmassG << " sQrts " << dSqrts << endl;
    }
    static const int lastBinWithData(hdata->FindLastBinAbove(0.0));
    //Main:ComputeChi2
    for(Int_t ib=iFirst; ib <= lastBinWithData; ib++){
      //TStopwatch timer3;
      const double binData ( hdata->GetBinContent(ib) );
      //if( binData > 10 ) {
      if( binData > 4 ) {
	Ndfd++;
	//	Double_t err =0; // err is err2
	const double xx( hdata->GetBinCenter(ib) );
	const double err2( hdata->GetBinError(ib)*hdata->GetBinError(ib) );
	const double dTh ( IntfUxfSxfD->Eval(xx) );
	const double delta( dTh - binData );
	chi2 += delta*delta/err2;
	if(Icall<2) {
	  cout << "**:Fit:AF:xx " << xx << " Th " << dTh << " data "
	       << binData << " chi2 " << chi2 << endl;
	}
	//std::cout << "Used "<<timer3.RealTime()<<" s of realtime and "<<timer3.CpuTime()<<" s of CPU" << std::endl;
      }
    } // endfor
  } // end if sChi2
  //
  Icall++;
  f=chi2;
  ///In case the integration failed, the chi2=0, so need to force the fcn out of the hole
  if (f<0.1){
    f=100000.;
  }

  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;
  gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  //myMinuit->mnprin(1,amin);
  //myMinuit->mnprin(2,edm);
  cout << "**:fcn2:edm " << edm
    //<< " dsqrt " << dsqrt
       << " nT " << nTries
       << " nRTries " << nRTries
       << " ms " << mass_smuon << " mc " << mass_chi0 << " norm " << dbnorm
       << " ppid " << ippid << " chi2 " << chi2 << " Ndfd " << Ndfd
       << " stat " << icstat << " call " << Icall << endl;

  if(chi2>100000) {
    cout << "**:Force exit:chi2>100000: " << endl;
    exit(0);
  }

  std::cout << "Used "<<timer2.RealTime()<<" s of realtime and "<<timer2.CpuTime()<<" s of CPU" << std::endl;

  //

  std::cout << "Used "<<timer2.RealTime()<<" s of realtime" << std::endl;
  std::cout << "Used "<<timer2.CpuTime()<<" s of CPU" << std::endl;


} // end fcn
//
void  getFileName(char * filetype , char * fname , char * argv[], char * argv10,
		  char * argv12, char * argv13)
{
  cout << "**: GetFileName: filetype: " << filetype << " argv[5] " << argv[5] << endl;
  if(strncmp(filetype,"root",4)==0) {
    //strcpy(fname,"/afs/cern.ch/eng/clic/data/JJB/tmva/\0");
    if(strncmp(argv[5],"rtmva",5)==0) {
      cout << "**: tmva: argv[1] " << argv[1] << endl;
      //strcat(fname,argv[4]); //ppid
      //strcat(fname,"/");
      //strcat(fname,argv[10]); //ppid
      //strcat(fname,"/");
      //strcat(fname,argv[1]);
      //strcat(fname,".root");
      strcpy(fname,argv[22]);
    } else {
      std::cout << "It cannot be something else than tmva in our case"  << std::endl;
      exit(1);
    }
    cout << "**: GetFileName: fname " << fname << endl;
  } else if(strncmp(filetype,"posts",5)==0) {
    strcpy(fname,"./hist/\0");
    strcat(fname,argv[4]); //ppid
    strcat(fname,"/");
    strcat(fname,argv[10]); //2
    strcat(fname,"/");
    if(strncmp(argv13,"FIT",3)==0) {
      if(strncmp(argv12,"SPB",3)==0) strcat(fname,"FITSPB_");
      if(strncmp(argv12,"SUB",3)==0) strcat(fname,"FITSUB_");
      if(strncmp(argv12,"SIG",3)==0) strcat(fname,"FITSIG_");
    }
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SPB",3)==0 && strncmp(argv[15],"STACK",5)==0)
      strcat(fname,"SPBSTACK_");
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SPB",3)==0 && strncmp(argv[15],"STACK",5)!=0)
      strcat(fname,"SPB_");
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SIG",3)==0) strcat(fname,"SIG_");
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SUB",3)==0) strcat(fname,"SUB_");
    if(strncmp(argv13,"ECM",3)==0 && strncmp(argv12,"NOFIT",5)==0) strcat(fname,"ECM_");
    if(strncmp(argv13,"FUN",3)==0 && strncmp(argv12,"NOFIT",5)==0) strcat(fname,"FUN_");

    strcat(fname,argv[5]);
    strcat(fname,"_");
    strcat(fname,argv[2]);
    strcat(fname,"_");
    strcat(fname,argv[4]);
    strcat(fname,"_");
    strcat(fname,argv[8]);
    strcat(fname,"_");
    strcat(fname,argv[9]);
    strcat(fname,"_");
    strcat(fname,argv[7]);
    strcat(fname,"_");
    strcat(fname,argv[3]);
    strcat(fname,"_");
    strcat(fname,argv[10]);
    if(strncmp(argv[12],"L",1)==0) strcat(fname,"_L");
    if(strncmp(argv10,"NV9",3)==0) strcat(fname,"_NV9");
    if(strncmp(argv10,"C98",3)==0) strcat(fname,"_C98");
    if(strncmp(argv10,"C50",3)==0) strcat(fname,"_C50");
    if(strncmp(argv10,"C80",3)==0) strcat(fname,"_C80");
    if(strncmp(argv10,"C8M",3)==0) strcat(fname,"_C8M");
    if(strncmp(argv10,"C90",3)==0) strcat(fname,"_C90");
    if(strncmp(argv10,"C95",3)==0) strcat(fname,"_C95");
    if(strncmp(argv10,"DST",3)==0) strcat(fname,"_DST");
    if(strncmp(argv10,"NBR",3)==0) strcat(fname,"_NBR");
    if(strncmp(argv10,"NFSRC",5)==0) strcat(fname,"_NFSRC");
    if(strncmp(argv10,"NTHC",4)==0) strcat(fname,"_NTHC");
    strcat(fname,"_");
    strcat(fname,argv[16]);
    strcat(fname,".eps");
  } else if(strncmp(filetype,"macro",5)==0) {
    strcpy(fname,"./hist/");
    if(strncmp(argv[5],"205_old",7)==0) {
      strcat(fname,argv[11]); //old
    } else if(strncmp(argv[5],"tmva",4)==0) {
      strcat(fname,argv[4]); //ppid
    } else {
      strcat(fname,argv[4]); //ppid
    }
    strcat(fname,"/");
    strcat(fname,argv[10]); //ppid
    strcat(fname,"/");
    if(strncmp(argv13,"FIT",3)==0) {
      if(strncmp(argv12,"SPB",3)==0) strcat(fname,"FITSPB_");
      if(strncmp(argv12,"SUB",3)==0) strcat(fname,"FITSUB_");
      if(strncmp(argv12,"SIG",3)==0) strcat(fname,"FITSIG_");
    }
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SPB",3)==0 && strncmp(argv[15],"STACK",5)==0)
      strcat(fname,"SPBSTACK_");
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SPB",3)==0 && strncmp(argv[15],"STACK",5)!=0)
      strcat(fname,"SPB_");
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SIG",3)==0) strcat(fname,"SIG_");
    if(strncmp(argv13,"NOFIT",3)==0 && strncmp(argv12,"SUB",3)==0) strcat(fname,"SUB_");
    if(strncmp(argv13,"ECM",3)==0 && strncmp(argv12,"NOFIT",5)==0) strcat(fname,"ECM_");
    if(strncmp(argv13,"FUN",3)==0 && strncmp(argv12,"NOFIT",5)==0) strcat(fname,"FUN_");

    strcat(fname,argv[5]);
    strcat(fname,"_");
    strcat(fname,argv[2]);
    strcat(fname,"_");
    strcat(fname,argv[4]);
    strcat(fname,"_");
    strcat(fname,argv[8]);
    strcat(fname,"_");
    strcat(fname,argv[9]);
    strcat(fname,"_");
    strcat(fname,argv[7]);
    strcat(fname,"_");
    strcat(fname,argv[3]);
    strcat(fname,"_");
    strcat(fname,argv[10]);
    if(strncmp(argv[12],"L",1)==0) strcat(fname,"_L");
    if(strncmp(argv10,"NV9",3)==0) strcat(fname,"_NV9");
    if(strncmp(argv10,"C98",3)==0) strcat(fname,"_C98");
    if(strncmp(argv10,"C50",3)==0) strcat(fname,"_C50");
    if(strncmp(argv10,"C80",3)==0) strcat(fname,"_C80");
    if(strncmp(argv10,"C8M",3)==0) strcat(fname,"_C8M");
    if(strncmp(argv10,"C90",3)==0) strcat(fname,"_C90");
    if(strncmp(argv10,"C95",3)==0) strcat(fname,"_C95");
    if(strncmp(argv10,"DST",3)==0) strcat(fname,"_DST");
    if(strncmp(argv10,"NBR",3)==0) strcat(fname,"_NBR");
    if(strncmp(argv10,"NFSRC",5)==0) strcat(fname,"_NFSRC");
    if(strncmp(argv10,"NTHC",4)==0) strcat(fname,"_NTHC");
    strcat(fname,"_");
    strcat(fname,argv[16]);
    strcat(fname,".C");
  } else {
    cout << "**: undefined filetype " << endl;
    exit(0);
  }
}
///
Double_t twogaus(Double_t *x, Double_t *par){
  Double_t fx = par[0]*(par[3]*TMath::Gaus(x[0],par[1],par[2]) + (1.-par[3])*TMath::Gaus(x[0],par[4],par[5]))  ;
  return fx;
}
//


//
void InitAnal(int , char * argv[],  vector<int> & vippid , vector<string> & vsppid,
	      vector<float> & vxkevt, vector<float> & vxkevtg, vector<float> & vxkevtL,
	      vector<float> & vxkevtgL, vector<float> & vlumi, vector<float> & vppxsec,
	      vector<float> & vhscale, vector<string> & vptitle,
	      vector<float> & vxkevtbr)
{
  cout << "**: InitAnal  " << endl;
  char ppidlist[100];
  char dirname[100];
  sprintf(dirname,"%s","/afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/input/");
  if( strncmp(argv[9],"pol0",4)==0) {
    sprintf(ppidlist,"%s%s%s%s%s%s%s",dirname,argv[4],"/","ppidlist","_",argv[12],".txt");
  } else {
    cout << "**: ppidlist bug " << argv[9] << endl;
    exit(-1);
  }
  cout << "**: ppidlist filename name is: " << ppidlist  << endl;
  FILE *fph = fopen(ppidlist,"r");
  if(!fph) {
    printf("--: File open error\n");
    exit(-1);
  } else {
    printf("**: File open success\n");
  }
  //exit(-1);
  // when you change here don't forget to change fget
  char line[140];
  int nline;
  int localippid=0;
  float ppxsec=0;
  float hscale=0;
  fgets(line,140,fph); // read the number of lines
  //cout << "**: line-1 " << line << endl;
  sscanf(line,"%d\n",&nline);
  cout << "**: Number of lines " << nline << endl;
  char ppidline[140];
  for (int ip = 0; ip < nline; ip++) {
    fgets(line,140,fph); // read the next lines
    //cout << " line " << line << endl;
#warning "fixed??"
    sscanf(line,"%140c\n",ppidline);
    //cout << "** ppidline is: " << ppidline << endl;
    if( strncmp(ppidline,"VC",2)==0) {
      char * valu[11];
      char * pch;
      pch = strtok (ppidline,",");
      //cout << "**: pch " << pch << endl;
      int iok = 0;
      while (pch != NULL)
	{
	  valu[iok]=pch;
	  pch = strtok (NULL, ",");
	  //cout << "valu " << iok << " " << valu[iok] << endl;
	  iok++;
	}
      localippid = atoi(valu[1]); //
      sscanf(valu[2],"%g",&ppxsec);
      sscanf(valu[3],"%g",&hscale);
      cout << "**: vsppid " << valu[0] << " vippid " << valu[1]
	   << " ppxsec " << valu[2] << " hscale " << valu[3] << " ptitle "
	   << valu[4] << endl;
      vsppid.push_back(valu[0]);
      vippid.push_back(localippid);
      vxkevt.push_back(0.);
      vxkevtbr.push_back(0.);
      vxkevtg.push_back(0.);
      vxkevtL.push_back(0.);
      vxkevtgL.push_back(0.);
      vlumi.push_back(0.);
      vppxsec.push_back(ppxsec);
      vhscale.push_back(hscale);
      vptitle.push_back(valu[4]);
    } else if( strncmp(ppidline,"CC",2)==0) {
      cout << "**: Comment line " << endl;
    } else if( strncmp(ppidline,"END",3)==0) {
      cout << "**: END Read ppid list " << endl;
      break;
    } // end if
  } // end for
} // end InitAnal
//
inline double Func_U(double *x, double *par)
{
  if(par[5] < x[0] && x[0] < par[4]) {
    return par[0]/(par[4]-par[5]);
  } 
  return 0;
}

inline double Func_D(double *x, double *par)
{
  //double Fx=0;;
  return par[0]*(par[3]*TMath::Gaus(x[0],par[1],par[2],kTRUE) + (1-par[3])*TMath::Gaus(x[0],par[4],par[5],kTRUE));
  //x = par[0]*TMath::Gaus(x[0],par[1],par[2],kTRUE);
  // if(debugD>0) cout << "**:fD:x " << x[0] << " par[1] " << par[1]
  // 		    << " par[2] " << par[2] << " Fx " << Fx << endl;
  //return Fx;
}

double Func_S(double *x, double *) // This function is not called when RooKeys us used
{
  double Fx=0;
  Double_t xx=x[0];
  //Fx = par[0]*TMath::Power(xx,par[1]) * TMath::Power(dSqrtsmax-xx,par[2]) * TMath::Gaus(xx,xx,xx*par[4]);
  //Fx = par[0]*TMath::Power(xx,par[1]) * TMath::Power(par[3]-xx,par[2]) * TMath::Gaus(xx,xx,xx*par[4]);
  Fx=fS->Eval(x[0]);
  if(xx>2980 && xx< 3020) Fx=fS->Eval(x[0])*1.2;
  //if(xx>dSqrtsmax) Fx=0;
  if(debugS>0) cout << "**:fS:x " << x[0] << " Fx " << Fx << endl ;
  return Fx;
}

double Func_UxS( double *x, double *par ) //Unif*Sqrts is fUxfS
{
  //Call_Func_UxS++;
  double Fx=0;
  if(IntFunc_UxSFlag==0) {
    fU->SetParameter(3,par[0]); //sqrts
  } else if(IntFunc_UxSFlag==1) {
    fU->SetParameter(3,x[0]); //sqrts
  }
  //
  //if(IntFunc_UxSFlag==1 && Call_Func_UxS<4) cout << "**:Func_UxS:x " << x[0] << " P0 " << par[0]
  //<< " IntFunc_UxSFlag " << IntFunc_UxSFlag << endl;
  //
  //when Func_UxS is called by IntFunc_UxS x[0]=sqrts and par[0]=emu
  //Use a global variable IntFunc_UxSFlag to switch the definition
  if(IntFunc_UxSFlag==0) {
    //Fx= f3->Eval(par[0]) * f_12->Eval(x[0]);  //x[0]=emu, par[0]=sqrts
    Fx= fS->Eval(par[0]) * fU->Eval(x[0]);  //x[0]=emu, par[0]=sqrts
    cout << "**:Func_UxS:x " << x[0] << " P0 " << par[0] << " fU->Eval(x[0]) " << fU->Eval(x[0])
	 << " fS->Eval(par[0]) " << fS->Eval(par[0])
	 << " Fx " << Fx <<endl;
  } else if(IntFunc_UxSFlag==1) {
    Fx= fU->Eval(par[0]) * fS->Eval(x[0]);  //x[0]=sqrts, par[0]=emu
    //if(Call_Func_UxS<4) cout << "**:Func_UxS:x " << x[0] << " P0 " << par[0] << " fU->Eval(par[0]) " << fU->Eval(par[0])
    //<< " fS->Eval(x[0]) " << fS->Eval(x[0])
    //<< " Fx " << Fx <<endl;

  }
  if(debugUS>0) cout << "**:Func_UxS:x " << x[0] << " P0 " << par[0]
		     << " fU->Eval(par[0]) " << fU->Eval(par[0])
		     << " fS->Eval(x[0]) " << fS->Eval(x[0])
		     << " IntFunc_UxSFlag " << IntFunc_UxSFlag << " Fx " << Fx << endl;

  //exit(0);
  return Fx;
}

//
double IntFunc_UxS( double *x, double * ) //Unif*Sqrts is fUxfS
{
  IntFunc_UxSFlag=1; // to change variable definition in Func_UxS
  //exit(0);
  //
  fUxfS->SetParameter(0, x[0]); //

  //  ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE,
  //  ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
  static ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVE,
				  1e-8, //absTolerance
				  1e-4, //relativeTolerance
				  3000);//numberOfIterations
  ig.SetFunction(*fUxfS);
  //
  double dig = -1;
  dig = ig.Integral(dSqrtsmin,dSqrtsmax);
  if(debugIUS>0) cout << "**:IntFunc_UxS:x " << x[0]  << " Int " << dig << endl;
  return dig;
  //exit(0);
}

//

double Func_UxD( double *x, double *par )  //Unif*Dres convolution
{
  //double Fx=0;
  //double dx=0;
  //double xx=0;
  //Fx = fU->Eval(x[0])*fD->Eval( par[0] - x[0] );
  return fU->Eval(x[0])*fD->Eval( par[0] - x[0] );
  // if(debugUD>0) cout << "**:fUcD:x " << x[0] << " par[0] " << par[0]
  // 		     << " fU " << fU->Eval(x[0]) << " fD " << fD->Eval( par[0] - x[0] )
  // 		     << " Fx " << Fx << " xx " << xx << endl;
  //return Fx;
}

double IntFunc_UxD( double *x, double * ) // IntFuxFd  //Unif*Dres
{
  //double dx=x[0]*x[0]*0.00002;
  const double xsquared(x[0]*x[0]);
  const double dx1=xsquared*dsigma1;
  const double dx2=xsquared*dsigma2;
  //double dx=x[0]*x[0]*0.00006;
  //if(x[0]<300) dx=x[0]*x[0]*0.00006;
  //  fD->SetParameter(2,dx); // dres
  fD->SetParameter(2,dx1); // dres
  fD->SetParameter(3,dfrac); // dres
  fD->SetParameter(5,dx2); // dres
  // if(debugIUD>0) cout << "**:IntFunc_UxD:x " << x[0]
  // 		      << " dx1 " << dx1 << " P0 " << par[0] << endl;
  //exit(0);
  //
  fUxfD->SetParameter(0, x[0]); // E
  //ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
  static ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVE,
				  1e-8, //absTolerance
				  1e-4, //relativeTolerance
				  3000);//numberOfIterations
  ig.SetFunction(*fUxfD);
  //fU->SetParameter(3,par[0]); //sqrts

  //if(debugIUD>0) cout << "**:IntFunc_UxD:fE0 " << fE0 << " fE1 " << fE1 << " fEcm " << localfEcm << endl;
  //exit(0);

  // double dL=100.;
  // double dH=1400.;
  //double integ=-1;
  // if(iECM==1400) {
  //   dL=50.;
  //   dH=400.;
  // }

  //exit(0);

  // if(fE0>dH || fE1 < dL) {
  //   cout << "**:IntFunc_UxD:fE0 " << fE0 << " fE1 " << fE1
  // 	 << " fEcm " << localfEcm << " iECM " << iECM
  // 	 << " dL " << dL << " dH " << dH
  // 	 << endl;
  // } else {
  //   integ = ig.Integral(fE1,fE0);
  // }

  //integ = ig.Integral(fE1,fE0);
  return ig.Integral(fU->GetParameter(5),fU->GetParameter(4));
  // if(integ<0) {
  //   debugIUD=1;
  //   cout << "**:Integration problem in IntFunc_UxD " << endl;
  // }
  // if(debugIUD>0) cout << "**:IntFunc_UxD:fE0 " << fE0 << " fE1 " << fE1
  // 		      << " fEcm " << localfEcm << " ig " << integ << endl;
  // if(integ<0) exit(0);

  //return integ;
}

//
double Func_UxSxD( double *x, double *par ) //L*Unif*Dres is fUxfSxfD
{
  //globalCounter++;
  //if(debugUSD>0) cout << "**:Func_UxSxD:x " << x[0] << " P0 " << par[0] << endl;
  //x=sqrts; p0=emu
  //exit(0);
  //Call_Func_UxSxD++;
  //double Fx=0;
  //  fU->SetParameter(3,x[0]); //sqrts
  //AS: Calculate the boundaries of the Uniform distribution
  const double mass0( fU->GetParameter(1) );
  const double mass1( fU->GetParameter(2) );
  const double localfEcm ( x[0] );
  // if(debugIUD>0) cout << "**:IntFunc_UxD:x " << x[0]
  // 		      << " dx1 " << dx1 << " fEcm " << localfEcm << endl;
  const double fS2   ( localfEcm*localfEcm );
  const double fA    ( localfEcm*0.25 );
  const double fB    ( 1.-(mass1*mass1)/(mass0*mass0) );
  const double fC    ( sqrt(1.-4.*mass0*mass0/fS2 ) );
  const double fatfb ( fA*fB );
  const double fE0   ( fatfb*(1.+fC) );
  const double fE1   ( fatfb*(1.-fC) );

  fU->SetParameter(4, fE0);
  fU->SetParameter(5, fE1);

  //double mass0=GetParameter(1);
  //double mass1=fU->GetParameter(2);
  // double fEcm =x[0];
  // double fS2=fEcm*fEcm;
  //double fA=fEcm/4.;
  //double fB=1.-(mass1*mass1)/(mass0*mass0);
  //double fC=sqrt(1.-4.*mass0*mass0/fS2 );
  //double fE0=fA*fB*(1.+fC);
  //double fE1=fA*fB*(1.-fC);
  // CHECKME!!!

  //double inter(InterpolateFast(thS, x[0]));

  // std::cout << std::setw(15) << x[0]
  // 	    << std::setw(15) << fS->Eval(x[0])  
  // 	    << std::setw(15) << thS->fArray[FindFastBin(thS, x[0])]
  // 	    << std::setw(15) << inter
  // 	    << std::setw(15) << fS->Eval(x[0])/inter
  // 	    << std::endl;

  return fS->Eval(x[0]) * IntfUxfD->Eval(par[0]);  //x[0]=sqrts, par[0]=emu
  //return thS->fArray[FindFastBin(thS, x[0])] * IntfUxfD->Eval(par[0]);  //x[0]=sqrts, par[0]=emu
  //return InterpolateFast(thS, x[0]) * IntfUxfD->Eval(par[0]);  //x[0]=sqrts, par[0]=emu
  //
  // if(debugUSD>0) {
  //   cout << "**:Func_UxSxD:x " << x[0] << " P0 " << par[0] << " fS>Eval(x[0]) " << fS->Eval(x[0])
  // 	 << " IntfUxfD->Eval(par[0]) " << IntfUxfD->Eval(par[0])
  // 	 << " Fx " << Fx << endl;
  // }
  //exit(0);
  //return Fx;
}

double IntFunc_UxSxD( double *x, double * ) // x=emu
{
  //IntFunc_UxSFlag=1;
  //Call_IntFunc_UxSxD++;
  //if(debugIUSD>0) cout << "**:IntFunc_UxSxD:x " << x[0] << endl;
  fUxfSxfD->SetParameter(0, x[0]);
  static ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVE,
				  1e-8, //absTolerance
				  1e-4, //relativeTolerance
				  3000);//numberOfIterations
  ig.SetFunction(*fUxfSxfD);
  //globalCounter=0;
  //ig.SetFunction(*IntfUxfS);
  double dig=-1;
  //if(debugIUSD>0) cout << "**:IntFunc_UxSxD:x " << x[0]  << " Int " << dig << endl;
  dig = ig.Integral(dSqrtsmin,dSqrtsmax); // below 2020 integration fails
  //std::cout << "We called this " << globalCounter  << std::endl;
  // if(dig<0 || dig > 1000) {
  //   debugIUD=1;
  //   cout << "**:Integration problem in IntFunc_UxSxD " << endl;
  // }
  //  if(dig<0) debugIUSD=1;
  //if(debugIUSD>0) cout << "**:IntFunc_UxSxD:x " << x[0]  << " Int " << dig << endl;
  //cout << "**:IntFunc_UxSxD:x " << x[0]  << " Int " << dig << endl;
  if(dig<0) exit(0);
  //exit(0);
  return dig;

}

////
double Func_UcS( double *x, double *par )  //Unif*Dres convolution
{
  double Fx=0;
  //Fx= fU->Eval(x[0])*fS->Eval( par[0] - x[0] );
  Fx= fU->Eval(par[0])*fS->Eval( x[0] - par[0] );
  //cout << "**:Func_UcS:x " << x[0] << " P0 " << par[0] << " fU->Eval(x[0]) " << fU->Eval(x[0])
  //<< " fS->Eval(par[0]) " << fS->Eval(par[0])
  if(fU->Eval(par[0])>0) {
    cout << "**:Func_UcS:x " << x[0] << " P0 " << par[0] << " fU->Eval(par[0]) " << fU->Eval(par[0])
	 << " fS->Eval(x[0]) " << fS->Eval(x[0])
	 << " Fx " << Fx << endl;
  }
  return Fx;
}

double IntFunc_UcS( double *x, double * ) //  //Unif*Dres
{
  fUcfS->SetParameter(0, x[0]);
  static ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
  ig.SetFunction(*fUcfS);
  //
  double mass0=fU->GetParameter(1);
  double mass1=fU->GetParameter(2);
  double localfEcm =fU->GetParameter(3);
  if(localfEcm<10) localfEcm=localfEcm*3000.;  //
  double fS2=localfEcm*localfEcm;
  double fA=fEcm/4.;
  double fB=1.-(mass1*mass1)/(mass0*mass0);
  double fC=sqrt(1.-4.*mass0*mass0/fS2 );
  double fE0=fA*fB*(1.+fC);
  double fE1=fA*fB*(1.-fC);
  //return ig.Integral(fE1,fE0);
  fUcfS->SetParameter(0, x[0]);
  double dig= ig.Integral(fE1,fE0);
  //double dig= ig.Integral(fE1-100.,fE0+100.); //03.01.2013
  //double dig= ig.Integral(dSqrtsmin,dSqrtsmax);
  cout << "**:IntFunc_UcS:dSqrtsmin " << dSqrtsmin << " dSqrtsmax " << dSqrtsmax
       << " fEcm " << fEcm << " " << fU->GetParameter(3) << " dig " << dig << endl;
  return dig;
}
