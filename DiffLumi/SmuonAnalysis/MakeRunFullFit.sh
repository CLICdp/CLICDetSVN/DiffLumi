#!/bin/bash
echo -ne "-Select mode (b=batch/l=local/e=exit) "
read XBATCH
if [[ $XBATCH = "b" ]]
 then
   BATCH=YES
elif [[ $XBATCH = "l" ]]
 then
  BATCH=NO
else
 exit
fi


#comming from Loop....sh
#export binx=50
#export biny=50
#export binz=50
#export jpid=0 #parameter ID (-19..0..+19)

#export p18  ###=nm #parameter name (nm=nominal, p1p= 1stparameter+10s, p1m=1stparameter-10s, p2p, p2m,...
#export p18=p1p
#export p18=p1m

export TEST=FIT
export FIT=SUB
export p2=H1LPADC4
export p3=e2500
export p4=205
export p5=rtmva
export p6=selall
export p7=0BX
export p8=smear2e-5t0
export p9=pol0
export p10=2
export p11=1000000
export p12=L
export p13=fixn
export p14=norebin
export p15=DIST
export p16=""
export p17=2000_plots
export FUNC=$p18
export p19=AS
export BLUMI=$p19
export p20=1
export p21=0
export p22=155
export SPECSEL=C80
export p1=$p5"_"$p4"_"$p8"_"$p9"_"$p7"_"$p3"_"$p10"_"$SPECSEL"_"$p12"_"$p17

#export inputpath=/afs/cern.ch/user/s/sailer/work/public/BHWide_xsec_smeared_Overlap_3000Bins_P_ErrorTimes10/${binx}x${biny}x${binz}/
export inputpath=/afs/cern.ch/work/s/sailer/public/Results/FitGPBH_Weighted_Smeared_3/FitGPBH_Weighted_Smeared_3_${binx}x${biny}x${binz}
export FINALFILE=BHWide_Overlap_final.root
#inputpath is where the NumericalEstimateOutput files are

##for 99/199/299 input file
#export inputpath=/afs/cern.ch/work/s/sailer/public/BHWide_xsec_smeared_Overlap_3000Bins_PM/
#export FINALFILE=BHWide_xsec_smeared_Overlap_final.root


export JOBNAME="Fit_""$p4"_"$p8"_"$p17"_"$p18"_"$p19"_"$p21"_"$p4"_"${binx}x${biny}x${binz}"
#echo "-Submit Job $jpid  $JOBNAME   "

if [ "$BATCH" = "NO" ]
       then
#       echo "-Submit Job $JOBNAME"
       export RUNBATCH="F"
       ./doall.sh
else
       echo "**:Is it ok ?[y/n] "
#       read YOK
       YOK="y"
       if [[ $YOK = "y" ]]
	 then
	 xwho=`whoami`:`date +"%d-%b-%Y-%T"`;
	 echo $xwho >> bjobs.lis
#	 echo "Submit Job: $JOBNAME " >> bjobs.lis
	 export RUNBATCH="T"
	 bsub < doall.sh >> bjobs.lis
       else
	exit
       fi
fi
