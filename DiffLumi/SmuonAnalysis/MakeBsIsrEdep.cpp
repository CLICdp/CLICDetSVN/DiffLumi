#include "RooGlobalFunc.h"
//
#ifdef USE_RootStyle
#include "RootStyle.hh"
#endif

#include "TRandom.h"
#include "TLegend.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TPostScript.h"
#include "TStyle.h"
#include "TMath.h"
#include "TROOT.h"
#include "TLegendEntry.h"
//
#include "RooKeysPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooKeysPdf.h"
#include "RooWorkspace.h"
#include <tclap/CmdLine.h>


using namespace RooFit ;

using namespace std;
Double_t fdm12(Double_t *xx, Double_t *par);
Double_t isrFunction(Double_t *, Double_t *);
//
int main(int argc, char *argv[])
{
#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  vector<string> sargv;
  std::string outputpath(".");
  std::string inputpath("/afs/cern.ch/user/s/sailer/work/public/BHWide_xsec_smeared_Overlap_3000Bins_PM/");
  try {
    TCLAP::CmdLine cmd("Check the defaults.", ' ', "0.0");
    std::vector<std::string> allowedProcesses;
    allowedProcesses.push_back("205");
    TCLAP::ValuesConstraint<std::string> allowedProcessesVals( allowedProcesses );

    TCLAP::ValueArg<string> processArg("","process","Process to fit: 205 for smuon",false,"205",
				       &allowedProcessesVals );
    cmd.add(processArg);
    TCLAP::ValueArg<string> parameterTypeArg("","parameterType","Type of parameter: nm, p1p, etc.",
					  false,"nm","string",cmd);

    TCLAP::ValueArg<string> paramVarArg("","paramidvar","Parameter ID and variation: -1, 25, -27",false,
					"0","string",cmd);
    TCLAP::ValueArg<string>  outputpathArg("","outputpath","Outputpath",false,
					   ".","string",cmd);
    
    TCLAP::ValueArg<string>  inputpathArg("","inputpath","inputpath to the NumericalEstimateOutput_*",false,
					   ".","string",cmd);
    
    cmd.parse( argc, argv );
    sargv.push_back(processArg.getValue());      //0
    sargv.push_back(parameterTypeArg.getValue());//1
    sargv.push_back(paramVarArg.getValue());     //2
    outputpath = outputpathArg.getValue();
    inputpath = inputpathArg.getValue();
  }
  catch(TCLAP::ArgException &e){  // catch any exceptions 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return 1;
  }


  // char string1[80];
  // char * valu[7];
  // char * vfile[1];
  // char * pch;
  // pch = strtok (argv[2],"_");
  // int iok=0;
  // //valu[iok]=pch;
  // //cout << "**: valu[0] " << valu[0] << endl;
  // while (pch != NULL)
  //   {
  //     valu[iok]=pch;
  //     sargv.push_back(valu[iok]);
  //     cout << "**:pch: " << pch << " iok " << iok << " valu " << valu[iok] << endl;
  //     //pch = strtok (0, "_");
  //     pch = strtok (NULL, "_");
  //     iok++;
  //   }
  //sargv.push_back(argv[3]); //0=nm, 1=p1p, ...
  cout << "**:sargv.size " << sargv.size() << endl;
  for (unsigned int i=0; i<sargv.size(); ++i) {
    cout << "**:i " << i << " sargv " << sargv[i] << endl;
  }
  std::cout << "**:outputpath "  << outputpath << std::endl;
  //exit(0);

  //done initializing stuff

  cout << "**:Start MakeBsIsrEdep:" << endl;
  //Read e1e1 (ISR) h1xExm histogram
  char rfname1[140];
  char fname1[80];
  sprintf(fname1,"%s","e1e1_396-sm-poL0_whic_3000.root");
  sprintf(rfname1,"%s%s","/afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/hist/",fname1);
  cout << "**: rfname1 " << rfname1 <<": This is the ISR spectrum"<< endl;
  TFile *f1 = new TFile(rfname1);
  f1->ls();
  char hname1[80];
  sprintf(hname1,"%s","h1xEcm");
  f1->cd();
  TH1F * th1 =  NULL;
  f1->GetObject(hname1, th1);
  if(NULL==th1) {
    cout << "**:Unknown histogram: " << hname1 << endl;
    return(0);
  } else {
    int nbinx = th1->GetNbinsX();
    cout << "**:Known histogram:th1: " << hname1 << " th1 " << th1 << " nbinx " << nbinx << endl;
  }
  //



  int pm10=0;
  cout << "**:pm10 " << pm10 << endl;
  //exit(0);
  //Read BS histogram hSqrts
  char rfname2[140];
  if(pm10==0) {
    sprintf(rfname2,"%s/%s%s%s",
	    inputpath.c_str(),
	    "NumericalEstimateOutput_",sargv[2].c_str(),".root");
  } else {
    //exit(0);
    sprintf(rfname2,"%s/%s%s%s",
	    inputpath.c_str(),
	    "NumericalEstimateOutput_",sargv[2].c_str(),".root");
  }
  std::cout << "** root file2 name is: " << rfname2 
	    << ": this is the reconstructed (AS+SP) lumi spectrum" << std::endl;
  TFile * f2=0;
  f2=TFile::Open(rfname2);
  cout << "**:f2 opened " << rfname2 << endl;
  f2->cd();
  f2->ls();
  char hname[80];
  sprintf(hname,"%s","hSqrts");
  TH1D *th2=NULL;
  f2->GetObject(hname,th2);
  if(th2==0) {
    cout << "**:Unknown histogram th2: " << hname << endl;
    exit(0);
  } else {
    int nbinx = th2->GetNbinsX();
    cout << "**:Known histogram th2: " << hname << " th2 " << th2 << " nbinx " << nbinx << endl;
  }
  //
  char rfname3[140];
  char fname3[80];
  cout << "**:sargv[0] " << sargv[0] << endl;
  if(sargv[0]=="205") { // read smuon sqrt(s) histogram
    sprintf(fname3,"%s","smu2smu2_386-chne-poL0_nosc_3000.root");
  } else if(sargv[0]=="202") {
    sprintf(fname3,"%s","se2se2_388-chne-poL0_nosc_3000.root");
  } else if(sargv[0]=="213") {
    sprintf(fname3,"%s","snuesnue_390-chne-poL0_nosc_3000.root");
  } else {
    cout << "**:fname3 BUG " << endl;
    exit(0);
  }
  sprintf(rfname3,"%s%s","/afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/hist/",fname3);
  std::cout << "**: rfname3 " << rfname3 << ": This is (maybe) the cross section dependance"<<std::endl;
  TFile *f3 = new TFile(rfname3);
  f3->ls();
  char hname3[80];
  sprintf(hname3,"%s","h1xEcm");
  f3->cd();
  TH1F * th3 =  NULL;
  f3->GetObject(hname3,th3);
  if(th3==NULL) {
    cout << "**:Unknown histogram th3: " << hname3 << endl;
    exit(0);
  } else {
    int nbinx = th3->GetNbinsX();
    cout << "**:Known histogram th3: " << hname3 << " th3 " << th3 << " nbinx " << nbinx << endl;
  }
  //

  //Done getting the 3 histograms

  // exit(0);
  //Define lumi spectrum histogram
  TH1D *h1beam;
  TH1D *h1LumiS;
  int nbinx=550;
  h1beam= new TH1D("h1beam","histo filled from th1 AS",nbinx,2000.,3100.);
  h1LumiS= new TH1D("h1LumiS","BsISREdep",nbinx,2000.,3100.);
  //
  TH1D * th4;
  th4= new TH1D("th4","histo filled from th3",nbinx,2000.,3100.);

  //get the cross section function
  TF1 *fEdepR=NULL; //obtained from file
  TF1 *fEdep=NULL; // created from fEdepR parameters
  double rlow =2000;
  double rhigh=3100;
  double mslep=0.;
  double xsectmax=0;
  Double_t dpar[12];
  int itf1=1; // to use ffR
  if(itf1==1) { //Read parameters of energy dependence functuion
    cout << "**:read tf1fname for: argv[1] " << sargv[0] << endl;
    char tf1fname[80];
    sprintf(tf1fname,"%s%s%s","/afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/Edep/hist/enerdep",
	    sargv[0].c_str(),".root");
    cout << "**:tf1fname: " << tf1fname << endl;
    TFile *file1;
    file1=TFile::Open(tf1fname);
    file1->cd();
    file1->ls();
    file1->GetObject("fEdep",fEdepR);
    if (fEdepR==0){
      std::cout << "Function fEdep was not found in the file, will stop now"  << std::endl;
      exit(1);
    }
    for (int ip = 0; ip < 12; ip++) {
      dpar[ip]=fEdepR->GetParameter(ip);
      //cout << "**:fEdep parameters:ip " << ip << " " << dpar[ip] << endl ;
    }
    fEdep = new TF1("fEdep",fdm12,rlow,rhigh,12);
    for (int ip=0; ip<12; ip++) {
      fEdep->SetParameter(ip,dpar[ip]);
      //cout << "**: ip " << ip << " dpar " << dpar[ip] << endl;
    }
    if(sargv[0]=="205") {
      mslep=1010.;
      xsectmax=1.21/1.2;
      cout << "**:Set fEdep for 205 " << endl;
    } else if(sargv[0]=="202") {
      mslep=1010.;
      xsectmax=10.6857/1.25; //
      cout << "**:Set fEdep for 202 " << endl;
    } else if(sargv[0]=="213") {
      rlow =2200;
      mslep=1097.;
      xsectmax=26.3116/1.25;
      cout << "**:Set fEdep for 213 " << endl;
    }
  }
  //got the cross section function
  
  //
  //exit(0);
  //
  RooWorkspace* ws = new RooWorkspace("ws") ;
  RooRealVar dsqrts("dsqrts","#sqrt{s}",2000.,3100); // Declare observable
  RooArgSet m_observables(dsqrts);
  ws->import(m_observables);
  RooDataSet m_data("data","data",RooArgSet(dsqrts));
  //
  //exit(0);
  int nentries=10000000;
  double sqrtsmin=2*mslep;
  double sqrtsminscaled=2*mslep/3000.;
  int igood=0;
  double Bssqrts=0;
  double IsrEdepsqrts = 1.; // ISR
  int Iscale=1;
  if(Iscale == 1) {
    th1->Sumw2();
    th1->Scale(1./(th1->GetSumOfWeights()*th1->GetBinWidth(1)));
  }
  //cout << "**: Loop " << endl;
  
  //Lumi spectum histogram filling
  for (Int_t i=0; i<nentries; i++) {
    Bssqrts  = th2->GetRandom(); //Bs
    IsrEdepsqrts = th1->GetRandom()/3000. -1; // ISR
    double BsxIsrEdepsqrts=(Bssqrts+IsrEdepsqrts)*3000; //BS modified by ISR
    if(i<20) {
      std::cout << "**:Bssqrts " << Bssqrts << " IsrEdepsqrts " << IsrEdepsqrts*3000.
		<< " BsxIsrEdepsqrts " << BsxIsrEdepsqrts
		<< std::endl;
    }
    if(BsxIsrEdepsqrts>sqrtsmin) {
      double xsect=fEdep->Eval(BsxIsrEdepsqrts)/(xsectmax*10000);//get the cross section at the "new" sqrts
      h1beam->Fill(BsxIsrEdepsqrts,xsect);
      h1LumiS->Fill(BsxIsrEdepsqrts,xsect);
      double BsIsrEdepsqrts = th3->GetRandom();
      th4->Fill(BsIsrEdepsqrts);//this is to have the same stat everywhere, so it "copies" th3
      igood++;
    }
  } // end for
  cout << "**: igood " << igood << endl;
  //
  int ientries=100000;
  //Fill RooKeys data set
  for (Int_t i=0; i<ientries; i++) {
    double sqrts  = h1beam->GetRandom(); //BsxIsrXEdep
    dsqrts.setVal(sqrts);
    m_data.add(RooArgSet(dsqrts));
  }
  //Make Rookeys
  ws->import(m_data);
  cout << "**: import data done " << endl;
  RooKeysPdf keys("fsqrts","fsqrts",dsqrts,m_data);//this is what takes forever
  cout << "**:import key done " << endl;
  TF1 *ffsqrts = keys.asTF(m_observables);
  cout << "**:ffsqrts done " << endl;

  //Now we are saving the stuff

  //
  char rfnameo[180];
  char rfnamex[180];
  if(pm10==0) {
    sprintf(rfnameo,"%s/%s%s_%s.root",
	    outputpath.c_str(),"RooKfunisredep", sargv[0].c_str(), 
	    sargv[1].c_str() );
    sprintf(rfnamex,"%s/%s%s_%s.root",
	    outputpath.c_str(),"ThSfunisredep", sargv[0].c_str(),
	    sargv[1].c_str());
  } else if(pm10==1) {
    exit(0);
    sprintf(rfnameo,"%s/%s%s_%s.root",
	    outputpath.c_str(),"RooKfunisredep", sargv[0].c_str(),
	    sargv[1].c_str());
    //           argv[2],"L",argv[2],".root" );
  } else {
    cout << "**:pm10 bug " << endl;
    exit(0);
  }
  //exit(0);
  //save Rookeys
  cout << "**:Open rfnameo: " << rfnameo << endl;
  TFile *fr = TFile::Open(rfnameo,"RECREATE");
  fr->cd();
  fr->ls();
  ffsqrts->ls();
  ffsqrts->Write();
  fr->ls();
  ws->Write();//save workspace
  fr->Close();
  cout << "**:rfnameo closed " << rfnameo << endl;
  //Save histogram
  cout << "**:Open rfnamex: " << rfnamex << endl;
  TFile *fThS = TFile::Open(rfnamex,"RECREATE");
  fThS->cd();
  fThS->ls();
  h1LumiS->Write();
  fThS->Write();
  fThS->ls();
  fThS->Close();
  cout << "**:rfnamex closed " << rfnamex << endl;

  //draw and save stuff

  //
  //int Iscale=1;
  //exit(0);
  char psname[180];
  sprintf(psname,"%s/%s%s_%s.eps",
	  outputpath.c_str(),"RooKfunisredep",sargv[0].c_str(),
	  sargv[1].c_str());
  //Draw control plots
  cout << "**:psname " << psname << endl;
  //exit(0);
  TCanvas *c1;
  c1 = new TCanvas("c1"," Histos ",0,0,800,700);
  //
  //gStyle->SetOptStat(1);
  //TPostScript *ps;
  //ps = new TPostScript(psname,111); //
  //
  c1->Divide(3,2);
  //
  int pad=0;
  pad++;
  cout << "**:DrawL:pad " << pad << endl;
  c1->cd(pad);
  gPad->SetLogy(1);
  th2->Draw(); //BS
  //
  pad++;
  c1->cd(pad);
  cout << "**:DrawL:pad " << pad << endl;
  //Iscale=0;
  if(Iscale==1) { // done above
    //th1->Sumw2();
    //th1->Scale(1./(th1->GetSumOfWeights()*th1->GetBinWidth(1)));
  }
  th1->GetXaxis()->SetRangeUser(2000,3040);
  gPad->SetLogy(1);
  th1->Draw(); //ISR
  //
  TH1D * H1RATIOA;
  H1RATIOA=(TH1D*)h1beam->Clone();
  H1RATIOA->Divide(h1beam,th4);
  pad++;
  c1->cd(pad);
  cout << "**:DrawL:pad " << pad << endl;
  h1beam->GetXaxis()->SetRangeUser(2000,3100);
  gPad->SetLogy(1);
  if(Iscale==1) {
    h1beam->Sumw2();
    h1beam->Scale(1./(h1beam->GetSumOfWeights()*h1beam->GetBinWidth(1)));
    h1LumiS->Sumw2();
    h1LumiS->Scale(1./(h1LumiS->GetSumOfWeights()*h1LumiS->GetBinWidth(1)));
  }
  h1beam->Draw(); // BS*ISR*EDEP
  h1LumiS->SetLineColor(6);
  h1LumiS->Draw("same");
  //draw Rookeys
  ffsqrts->SetLineColor(7);
  ffsqrts->Draw("same");
  //
  pad++;
  c1->cd(pad);
  cout << "**:DrawL:pad " << pad << endl;
  if(Iscale==1) {
    th4->Sumw2();
    th4->Scale(1./(th4->GetSumOfWeights()*th4->GetBinWidth(1)));
  }
  gPad->SetLogy(1);
  h1beam->GetXaxis()->SetRangeUser(2000,3100);
  th4->GetXaxis()->SetRangeUser(2000,3100);
  th4->SetLineColor(1);
  th4->Draw(); //from run 386
  h1beam->SetLineColor(2);
  h1beam->Draw("same"); // BS*ISR*EDEP
  //
  pad++;
  c1->cd(pad);
  cout << "**:DrawL:pad " << pad << endl;
  H1RATIOA->SetMaximum(1.5);
  H1RATIOA->SetMinimum(0.5);
  H1RATIOA->Draw();
  //
  c1->SaveAs(psname);
  //ps->Close();
  cout << "**: ps file closed " << psname << endl;
  return 0;
} //end of main
//

//
Double_t fdm12(Double_t *xx, Double_t *par)
{
  Double_t x = xx[0];
  if(x>3000) x=3000.;
  Double_t res = par[0]+par[1]*x + par[2]*x*x + par[3]*x*x*x + par[4]*x*x*x*x + par[5]*x*x*x*x*x
    + par[6]*x*x*x*x*x*x
    + par[7]*x*x*x*x*x*x*x
    + par[8]*x*x*x*x*x*x*x*x
    + par[9]*x*x*x*x*x*x*x*x*x
    +par[10]*x*x*x*x*x*x*x*x*x*x
    +par[11]*x*x*x*x*x*x*x*x*x*x*x;
  return res;
}
Double_t isrFunction(Double_t *x, Double_t *par){
  //Float_t xx =x[0]/(3010.);
  Float_t xx =x[0]/(3011.);
  if(xx<0.)xx=0.;
  //cout << "**: isrF in: x[0] " << x[0] << " xx " << xx << " par[0] " << par[0] << endl;
  Double_t eta = par[0];
  Double_t f = 0.5*eta*pow((1.-xx),(0.5*eta-1.))*(1.+0.5*eta)*exp((-1./8.)*(eta+(3.14*3.14/6.-1.)*eta*eta))*(0.5*(1.+xx*xx)-eta/8.*(0.5*(1.+3.*xx*xx)*log(xx)+(1.-xx*xx)));
  Double_t val = par[1]*f;
  //cout << "**: isrF out:xx " << xx << " val " << val << " par[1] " << par[1] << " f " << f << endl;
  return val;
}
