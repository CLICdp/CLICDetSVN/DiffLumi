#!/bin/bash
export DEBUG=OFF
##export ECOR=NO
##export MAXLOOP=1           
##export LUMI=1
export TEST=FIT
export SPECSEL=C80
export SEL=1
#
echo -ne "-Select mode (b=batch/l=local/e=exit) "
read XBATCH
if [[ $XBATCH = "b" ]]
 then
   BATCH=YES
elif [[ $XBATCH = "l" ]]
 then
  BATCH=NO
else
 exit
fi
### set parameters
export FIT=SUB
export p2=H1LPADC4
export p3=e2500
export p4=205
export p5=rtmva
export p6=selall
export p7=0BX
export p8=smear2e-5t0
export p9=pol0
export p10=2
export p11=1000000
export p12=L
export p13=fixn
export p14=norebin
export p15=DIST
export p16=""
export p17=2000_plots
export p18=nm
#export p18=p1p
#export p18=p1m
export FUNC=$p18
export p19=AS
export BLUMI=$p19
export p20=1
export p21=0
export p22=155
export SPECSEL=C80
##below is the data file name
export p1=$p5"_"$p4"_"$p8"_"$p9"_"$p7"_"$p3"_"$p10"_"$SPECSEL"_"$p12"_"$p17
#
echo "**:p1 $p1 "
output="./startfit_"$p5"_"$p3"_"$p17"_"$p18"_"$p19"_"$p4".txt"
echo "**:output $output "
export JOBNAME="Fit_""$p4"_"$p8"_"$p17"_"$p18"_"$p19"_"$p21"_"$p4"
echo "-Submit Job $JOBNAME"   
#cd /afs/cern.ch/eng/clic/data/JJB/BsIsrEdepRooK/batch
##cp -p /afs/cern.ch/user/b/blaising/pythia/stdhep/phr/V04EnerDistFit/batch/startfit.sh .
if [ "$BATCH" = "NO" ]     
       then                 
       echo "-Submit Job $JOBNAME" > $output
       export runBATCH=F
       ./startfit.sh >> $output      
else                    
       echo "**:Is id ok ? "
       read YOK
       if [[ $YOK = "y" ]]
         then
         xwho=`whoami`:`date +"%d-%b-%Y-%T"`;
         echo $xwho >> bjobs.lis
         echo "Submit Job: $JOBNAME " >> bjobs.lis
	 export runBATCH=T
         bsub < startfit.sh >> bjobs.lis
       else
        exit
       fi
fi
## exit
