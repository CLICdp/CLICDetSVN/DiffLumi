//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct 28 09:09:27 2011 by ROOT version 5.28/00
// from TTree h998/Data
// found on file: bhwideout.root
//////////////////////////////////////////////////////////

#ifndef BHWide_h
#define BHWide_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

class BHWide {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         bpx2;
   Float_t         bpy2;
   Float_t         bpz2;
   Float_t         bpe2;
   Float_t         bqx2;
   Float_t         bqy2;
   Float_t         bqz2;
   Float_t         bqe2;
   Float_t         eb1;
   Float_t         eb2;
   Float_t         deb1;
   Float_t         deb2;
   Float_t         deb1g;
   Float_t         deb2g;
   Float_t         qrr;
   Float_t         prr;
   Float_t         cxb1;
   Float_t         cxb2;
   Float_t         weight;
   Int_t           nphotons;
   Float_t         photdata[800][4];   //[nphotons]
   Float_t         relproba;

   // List of branches
   TBranch        *b_bpx2;   //!
   TBranch        *b_bpy2;   //!
   TBranch        *b_bpz2;   //!
   TBranch        *b_bpe2;   //!
   TBranch        *b_bqx2;   //!
   TBranch        *b_bqy2;   //!
   TBranch        *b_bqz2;   //!
   TBranch        *b_bqe2;   //!
   TBranch        *b_eb1;   //!
   TBranch        *b_eb2;   //!
   TBranch        *b_deb1;   //!
   TBranch        *b_deb2;   //!
   TBranch        *b_deb1g;   //!
   TBranch        *b_deb2g;   //!
   TBranch        *b_qrr;   //!
   TBranch        *b_prr;   //!
   TBranch        *b_cxb1;   //!
   TBranch        *b_cxb2;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_nphotons;   //!
   TBranch        *b_photdata;   //!
   TBranch        *b_relproba;   //!

   BHWide(TTree *tree=0);
   virtual ~BHWide();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef BHWide_cxx
BHWide::BHWide(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("bhwideout.root");
      if (!f) {
         f = new TFile("bhwideout.root");
      }
      tree = (TTree*)gDirectory->Get("h998");

   }
   Init(tree);
}

BHWide::~BHWide()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t BHWide::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t BHWide::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void BHWide::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("bpx2", &bpx2, &b_bpx2);
   fChain->SetBranchAddress("bpy2", &bpy2, &b_bpy2);
   fChain->SetBranchAddress("bpz2", &bpz2, &b_bpz2);
   fChain->SetBranchAddress("bpe2", &bpe2, &b_bpe2);
   fChain->SetBranchAddress("bqx2", &bqx2, &b_bqx2);
   fChain->SetBranchAddress("bqy2", &bqy2, &b_bqy2);
   fChain->SetBranchAddress("bqz2", &bqz2, &b_bqz2);
   fChain->SetBranchAddress("bqe2", &bqe2, &b_bqe2);
   fChain->SetBranchAddress("eb1", &eb1, &b_eb1);
   fChain->SetBranchAddress("eb2", &eb2, &b_eb2);
   fChain->SetBranchAddress("deb1", &deb1, &b_deb1);
   fChain->SetBranchAddress("deb2", &deb2, &b_deb2);
   fChain->SetBranchAddress("deb1g", &deb1g, &b_deb1g);
   fChain->SetBranchAddress("deb2g", &deb2g, &b_deb2g);
   fChain->SetBranchAddress("qrr", &qrr, &b_qrr);
   fChain->SetBranchAddress("prr", &prr, &b_prr);
   fChain->SetBranchAddress("cxb1", &cxb1, &b_cxb1);
   fChain->SetBranchAddress("cxb2", &cxb2, &b_cxb2);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("photdata", photdata, &b_photdata);
   fChain->SetBranchAddress("relproba", &relproba, &b_relproba);
   Notify();
}

Bool_t BHWide::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void BHWide::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t BHWide::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef BHWide_cxx
