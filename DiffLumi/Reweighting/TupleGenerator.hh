#ifndef TupleGenerator_hh
#define TupleGenerator_hh 1

/** Tuple for Initial Electron Fit
 */

#include "AbstractTuple.hh"

/** \class TupleGenerator
    \brief Tuple for Initial Electron Fit

    Gives access to the tuple needed for the Initial Electron Fit, 
    creates the binning, and fills the data vectors
 */
class TupleGenerator : public AbstractTuple
{
public:
  /** Contructor
   * @param filename Name of the file containing the MyTree tuple 
   * @param nentries Number of events to use
   */
  TupleGenerator(TString filename, Long64_t nentries = -1):
    AbstractTuple(filename, nentries){
    Init();
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("TupleGenerator"));
  }
  ~TupleGenerator()
  {  }
  long FillVectors( std::vector<double>*d,
		    std::vector<Event> *values=NULL,
		    std::vector<double>*w=NULL) const;
  void CreateEquiProbability(TString fname = "" );
  void FillLumiHistogram(TString filename = "");
protected:
  void Init();

};

#endif // TupleGenerator_hh
