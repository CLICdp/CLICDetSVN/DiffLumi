#ifndef TupleBHWide_hh
#define TupleBHWide_hh 1

#include "AbstractTuple.hh"
#include "Utilities.h"

/** \class TupleBHWide
    \brief Tuple for the BHWide sample

    This class adds support for final state electrons needed for the BHWide level fit. 
    It takes care of creating the binning and filling the vectors
 */
class TupleBHWide : public AbstractTuple
{     
public:
  /** Constructor */
  TupleBHWide(TString filename, Long64_t nentries = -1): 
    AbstractTuple(filename, nentries),
    m_px1(0.0),		 
    m_py1(0.0),		 
    m_pz1(0.0),		 
    m_e1(0.0),		 
    m_px2(0.0),		 
    m_py2(0.0),		 
    m_pz2(0.0),		 
    m_e2(0.0),		 
    m_nphotons(0.0),		 
    m_photdata(),   
    m_relativeProbability(0.0)
  { 
    Init(); 
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("TupleBHWide"));
  }
  ~TupleBHWide(){ LOG4CPLUS_DEBUG(m_log, "Cleaning up"); }
  long FillVectors( std::vector<double>*d, 
		    std::vector<Event>* values = NULL, 
		    std::vector<double>* w = NULL) const;
  void CreateEquiProbability(TString fname = "" );
  //inline void SetEnergyCut(double cut){m_cut = cut*m_nominalBeamEnergy*2;}
  void FillLumiHistogram(TString filename = "");
private:
  void Init();
  
  Double_t m_px1;//!< px for e1
  Double_t m_py1;//!< py for e1
  Double_t m_pz1;//!< pz for e1
  Double_t m_e1;//!< e for e1
  Double_t m_px2;//!< px for e2
  Double_t m_py2;//!< py for e2
  Double_t m_pz2;//!< pz for e2
  Double_t m_e2;//!< e for e2
  Int_t    m_nphotons;//!< Number of photons
  Double_t m_photdata[20][4];   //[nphotons]
  Double_t m_relativeProbability;
};

#endif // TupleBHWide_hh
