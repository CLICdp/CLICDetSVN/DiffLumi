#include "ModelOverlap.hh"

#include <iomanip>
#include <iostream>
#include <Math/GSLIntegrator.h>

#ifdef DEBUG
double ModelOverlap::CalculateProbability(double eb1, double eb2, double deb1, double deb2, double proba) const {
#else
double ModelOverlap::CalculateProbability(double eb1, double eb2, double deb1, double deb2, double) const {
#endif

  double probability(1.0);
  double BetaLimit(static_cast<BetaFunction*>(m_BeamstrahlungArm1)->GetBetaLimit());

#ifdef DEBUG
  LOG4CPLUS_DEBUG(m_log, "" <<   std::setw(15) << eb1
		  <<   std::setw(15) << eb2  <<  std::setw(15) << deb1
		  <<   std::setw(15) << deb2 <<  std::setw(15) << proba);
#endif
  if( eb1 >= BetaLimit && eb2 >= BetaLimit ) {
    probability *= m_peak;
    probability *= (*m_BeamSpread1)(deb1) * (*m_BeamSpread2)(deb2);

#ifdef DEBUG
    if (probability/proba < 0.5){
      LOG4CPLUS_DEBUG(m_log, ""  << " Peak " <<   std::setw(15)  << m_peak
		      << " Proba " <<   std::setw(15)  << probability );
    }
#endif


  } else if ( eb1 < BetaLimit && eb2 >= BetaLimit ) {

    probability *= m_arm1;
    probability *= (*m_BeamstrahlungArm1)(eb1);
    
    probability *=  (*m_BeamSpread1)(deb1);
    probability *=  (*m_BeamSpreadArm2)(deb2);

#ifdef DEBUG
    if (probability/proba < 0.5){
      LOG4CPLUS_DEBUG(m_log, ""  << " arm1 " <<   std::setw(15)  << m_arm1
		      << std::setw(15)  << (*m_BeamstrahlungArm1)(eb1)
		      << " Proba " <<   std::setw(15)  << probability );
    }
#endif
    
  } else if ( eb1 >= BetaLimit && eb2 < BetaLimit ) {
    probability *= m_arm2 * (*m_BeamstrahlungArm2)(eb2);//same beam strahlung functions as the body for B2

    probability *= (*m_BeamSpreadArm1)(deb1);
    probability *= (*m_BeamSpread2)(deb2);


#ifdef DEBUG
    if (probability/proba < 0.5){
      LOG4CPLUS_DEBUG(m_log, "" << " arm2 " <<   std::setw(15) << m_arm2
		      <<   std::setw(15)  << (*m_BeamstrahlungArm2)(eb2)
		      << " Proba " <<   std::setw(15)  << probability );
    }
#endif

  } else {
    probability *=  (*m_BeamstrahlungBody1)(eb1) * (*m_BeamstrahlungBody2)(eb2) * m_body;

#ifdef DEBUG
    if (probability/proba < 0.5){
      LOG4CPLUS_DEBUG(m_log, ""  << " body " <<   std::setw(15)<< (*m_BeamstrahlungBody1)(eb1)
		      <<   std::setw(15)  << (*m_BeamstrahlungBody2)(eb2)
		      << " Proba " <<   std::setw(15)  << probability );
    }
#endif

  }

  LOG4CPLUS_DEBUG(m_log, ""  << std::setw(15) << (*m_BeamSpread1)(deb1)
		  << std::setw(15) << (*m_BeamSpread2)(deb2));

  return probability;
}


//==================================================================================================================
AbstractModel::Parts ModelOverlap::GetRandomVariates(double& x1, double& x2, double& dx1, double& dx2, 
						     double& dx1gauss, double& dx2gauss, double& RelativeProbability, 
						     double BeamSpreadSmearing) const{
  
  static int counter(0), peakcn(0), arm1cn(0), arm2cn(0), bodycn(0);
  Parts Chosen = kNONE;
  int counting(0);
  do {
    x1  = -1;
    x2  = -1;
    dx1 = -1;
    dx2 = -1;
    double random = gRandom->Uniform(1.);
    RelativeProbability = 1;

    if(random > 0. && random <= m_peak) {
      x1 = 1.0;
      x2 = 1.0;
      counter++;
      peakcn++;
      RelativeProbability *= m_peak;
      //Now care for the beam spread
      dx1 = m_BeamSpread1->GetRandom();
      dx2 = m_BeamSpread2->GetRandom();
      RelativeProbability *= (*m_BeamSpread1)(dx1)*(*m_BeamSpread2)(dx2);
      Chosen = kPeak;
    } else if( random > m_peak  && random <= (m_peak + m_arm1)){

      x1 = m_BeamstrahlungArm1->GetRandom();
      x2 = 1.0;
      counter++;
      arm1cn++;
      RelativeProbability *= m_BeamstrahlungArm1->evaluate(x1)  * m_arm1;
      //Now care for the beam spread
      dx1 = m_BeamSpread1->GetRandom();
      dx2 = m_BeamSpreadArm2->GetRandom();
      RelativeProbability *= (*m_BeamSpread1)(dx1)* (*m_BeamSpreadArm2)(dx2);
      Chosen = kArm1;
    } else if ( random > (m_peak + m_arm1) && random <= (m_peak + m_arm1 + m_arm2)){

      x1 = 1.0;
      x2 = m_BeamstrahlungArm2->GetRandom();
      counter++;
      arm2cn++;
      RelativeProbability *= m_BeamstrahlungArm2->evaluate(x2) * m_arm2;
      //Now care for the beam spread
      dx1 = m_BeamSpreadArm1->GetRandom();
      dx2 = m_BeamSpread2->GetRandom();
      RelativeProbability *= m_BeamSpreadArm1->evaluate(dx1) * m_BeamSpread2->evaluate(dx2);
      Chosen = kArm2;
    } else if (random > (m_peak + m_arm1 + m_arm2) && random < 1.) {

      x1 = m_BeamstrahlungBody1->GetRandom();
      x2 = m_BeamstrahlungBody2->GetRandom();

      counter++;
      bodycn++;
      RelativeProbability *= m_BeamstrahlungBody1->evaluate(x1) * m_BeamstrahlungBody2->evaluate(x2) * m_body;

      dx1 = 0;
      dx2 = 0;

      Chosen = kBody;
    }//Finished BetaFunction parts
    else {
      LOG4CPLUS_ERROR(m_log, "What?? No region fits?");
      continue;
    }

    dx1gauss = gRandom->Gaus(0,BeamSpreadSmearing);
    dx2gauss = gRandom->Gaus(0,BeamSpreadSmearing);

    //filter out events where the total energy is <0
    if( (x2+dx2+dx2gauss)<0 || (x1+dx1+dx1gauss)<0) {
      Chosen = kNONE;
      counting++;
      //      std::cout << "WHAT " << counting <<  std::endl;
      continue;
    }

    // std::cout << "Arg1 "
    // 	      << std::setw(15) << x1
    // 	      << std::setw(15) << x2
    // 	      << std::setw(15) << dx1
    // 	      << std::setw(15) << dx2
    // 	      << std::setw(15) << RelativeProbability  
    // 	      << std::setw(45) << Chosen
    // 	      << std::endl;


    const double probability(CalculateProbability(x1, x2, dx1, dx2,0.0));
    if( isinf( probability )) {
      LOG4CPLUS_ERROR(m_log,"Is Inf!! Ignore this event\n"
		      << std::setw(15) << x1
		      << std::setw(15) << x2
		      << std::setw(15) << dx1
		      << std::setw(15) << dx2
		      << std::setw(15) << probability 
		      << std::setw(15) << RelativeProbability  
		      << std::setw(15) << probability/ RelativeProbability );
      Chosen = kNONE;
      continue;
    }

  } while (Chosen == kNONE);

  return Chosen;
}

