#ifndef AbstractTuple_hh
#define AbstractTuple_hh 1

#include "StoreBinning.hh"
#include "Event.hh"
#include "AbstractCut.hh"
#include <TString.h>
//#include <TTree.h>
#include <TChain.h>
#include <vector>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

/** \var typedef std::vector<double> doubleVec;
 * \brief type def to simplify our lives
 */
typedef std::vector<double> doubleVec;


/*!
 * \class AbstractTuple
 * \brief Base tuple class
 *
 * This Class is the abstract tuple class. It's used as an interface.
 */
class AbstractTuple {     

public:
  /*! 
   * Contructor
   * @param filename File path to the file containing the MyTree tuple
   * @param nentries Number of entries to use
   */
  AbstractTuple( TString filename, Long64_t nentries=-1 ): 
    m_filename(filename),
    m_entries(nentries),
    //m_file(TFile::Open("file:"+filename)),
    m_tree(NULL),
    m_storedBins(0),
    m_nbinx(1), m_nbiny(1), m_nbinz(1),
    m_cutclass(NULL),
    m_eb1(0.0), m_eb2(0.0), m_deb1(0.0), m_deb2(0.0),
    m_E1(0.0), m_E2(0.0),
    m_relativeProbability(0.0),
    m_log(),
    m_nominalBeamEnergy(1500.0)
  { } 

  /**
   * Destructor
   */
  virtual ~AbstractTuple() { 
    //m_file->Close(); 
    delete m_tree;
    delete m_cutclass;
  }

  //! copy contructor
  AbstractTuple(const AbstractTuple&);
  //! copy contructor
  AbstractTuple&  operator=(const AbstractTuple&);

  //! Method to get the binning
  inline const StoreBinning* GetBinning(){return &m_storedBins;} 

  /** Method to set the binning

      When the binning was determined on the Data sample, it needs to be reused for the Monte Carlo
      @param binning Pointer to the binning class
   */
  inline void SetBinning(const StoreBinning* binning){m_storedBins = *binning;}
  
  /** Method to set the beam energy
      @param energy Nominal beam energy
   */
  inline void SetNominalBeamEnergy(double energy){m_nominalBeamEnergy = energy;}

  //! Define the cut class to use when filling the vector
  //inline void SetEnergyCut(double cut){m_energy_cut2 = cut*cut*9e-6;}//9e-6 is needed to convert to tev squared
  void SetCut(AbstractCut* cut){m_cutclass = cut;}//9e-6 is needed to convert to tev squared
  //! Define the number of bins
  inline void SetBins(int binsx, int binsy, int binsz=1){ 
    m_nbinx = binsx; 
    m_nbiny = binsy; 
    m_nbinz = binsz; 
    m_storedBins = StoreBinning(m_nbinx, m_nbiny, m_nbinz);  
  }
  //! Fill the data vectors
  virtual long FillVectors( doubleVec*d, 
			    std::vector<Event> *values=NULL, 
			    doubleVec*w=NULL) const = 0;
  /*! When binning was created previously, reuse it
   * @param filename Name of the root file containing the binning
   */
  virtual void LoadEquiProbability(TString filename = "");
  /*! Create the binning
   * @param filename name of the root file where to write the binning
   */
  virtual void CreateEquiProbability(TString filename = "") = 0;

  //! Fill lumi histogram, useful for comparing data/MC
  virtual void FillLumiHistogram(TString filename = "") = 0;

  /*! Tuple Factory
   * @param Tupletype type of the tuple needed
   * @param filename Name of the file containing the MyTree tuple
   * @param stat Number of events to use
   */
  static AbstractTuple* getTuple(std::string Tupletype, TString filename, int stat);

protected:
  virtual void Init() = 0; //!< Initialize the tuple

  TString m_filename; //!< Input file containing the MyTree TTree
  Long64_t m_entries; //!< Number of entries in the tree
  //TFile *m_file;
  //TTree *m_tree;
  TChain *m_tree; //!< Pointer to the tree
  StoreBinning m_storedBins; //!< Reference to binning
  int m_nbinx;//!< Number of x bins
  int m_nbiny;//!< Number of y bins
  int m_nbinz;//!< Number of z bins
  AbstractCut* m_cutclass; //!< Cut class

  double m_eb1; //!< Energy of the incoming electron
  double m_eb2; //!< Energy of the other incoming electron
  double m_deb1;//!< Variation to the nominal energy first electron
  double m_deb2;//!< Variation to the nominal energy second electron
  double m_E1; //!< Total energy of the first electron
  double m_E2; //!< Total energy of the seond electron
  double m_relativeProbability; //!< Relative probability for that event to happen

  log4cplus::Logger m_log;//!< Logger class
  double m_nominalBeamEnergy; //!< Nominal beam energy
};

#endif // AbstractTuple_hh
