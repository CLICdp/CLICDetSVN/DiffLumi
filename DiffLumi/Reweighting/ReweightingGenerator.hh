#ifndef ReweightingGenerator_hh
#define ReweightingGenerator_hh 1

#include "AbstractReweighting.hh"

/** \class ReweightingGenerator
    \brief Class for Initial Electron Fit
 */
class ReweightingGenerator: public AbstractReweighting {
public:
  /** Constructor */
  ReweightingGenerator(AbstractModel* model, int nBins):
    AbstractReweighting(model, nBins)
  {
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("ReweightingGenerator"));
  }
  virtual ~ReweightingGenerator()
  {};

  void FillControlHistograms(TString filename="");

private:

};



#endif //ReweightingGenerator_hh
