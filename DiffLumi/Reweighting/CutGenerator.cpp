#include "CutGenerator.hh"

bool CutGenerator::cut(double E1, double E2){
  if (E1*E2 < m_cutval){
    LOG4CPLUS_TRACE(m_log,"Event FAILS cut.");
    return true;
  } else{
    LOG4CPLUS_TRACE(m_log,"Event passes cut.");
    return false;
  }
}
