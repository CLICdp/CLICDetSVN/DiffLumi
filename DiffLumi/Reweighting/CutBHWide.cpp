#include "CutBHWide.hh"

bool CutBHWide::cut(const TLorentzVector p1, const TLorentzVector p2){
  TLorentzVector sum = p1+p2;
  if (sqrt(sum.Mag2()) < m_cutval){
    LOG4CPLUS_TRACE(m_log,"Event FAILS cut.");
    return true;
  } else{
    LOG4CPLUS_TRACE(m_log,"Event passes cut.");
    return false;
  }
}
