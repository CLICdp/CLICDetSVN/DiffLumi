#ifndef ModelSeparate_hh
#define ModelSeparate_hh 1

#include "AbstractModel.hh"

/** \class ModelSeparate
    \brief Class for the Separate model
 */
class ModelSeparate  : public AbstractModel
{     

public:
  /** Constructor */
  ModelSeparate(const double *parameter): AbstractModel(parameter)
  {  
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("ModelSeparate"));
    double bsLow = parameter[7];
    static_cast<BetaFunction*>(m_BeamstrahlungArm1 )->SetBetaLimit( 1. + bsLow );
    static_cast<BetaFunction*>(m_BeamstrahlungArm2 )->SetBetaLimit( 1. + bsLow );
    static_cast<BetaFunction*>(m_BeamstrahlungBody1)->SetBetaLimit( 1. + bsLow );
    static_cast<BetaFunction*>(m_BeamstrahlungBody2)->SetBetaLimit( 1. + bsLow );
    //LOG4CPLUS_DEBUG(m_log,"Beta limit ="<<1. + bsLow);
  }//ctor

  ~ModelSeparate()
  {  }

  double CalculateProbability(double eb1, double eb2, double deb1, double deb2, double) const;
  AbstractModel::Parts GetRandomVariates(double& x1, double& x2, double& dx1, double& dx2, 
					 double& dx1gauss, double& dx2gauss, double& RelativeProbability, 
					 double BeamSpreadSmearing) const;


  std::string GetName() const { return "Separate"; }

  inline int GetNParameters(){return 35;}
private:

};

#endif // ModelSeparate_hh
