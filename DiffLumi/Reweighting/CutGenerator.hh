#ifndef CutGenerator_hh
#define CutGenerator_hh

#include "AbstractCut.hh"

/** \class CutGenerator
    \brief Cuts for Initial Electron Fit
 */
class CutGenerator:public AbstractCut
{
public:
  /** Constructor*/
  CutGenerator():
    AbstractCut()
  {
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("CutGenerator"));
  }
  ~CutGenerator(){};
  /**Apply the cut*/
  bool cut(double E1, double E2);

};
#endif
