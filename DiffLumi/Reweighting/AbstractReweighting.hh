#ifndef AbstractReweighting_hh
#define AbstractReweighting_hh 1

#include "AbstractModel.hh"
#include "AbstractTuple.hh"
#include "Event.hh"
#include "TString.h"
#include "TTree.h"
#include "TFile.h"
#include <vector>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include "omp.h"

/* Obtain a backtrace and print it to stdout. */

typedef std::vector<double> doubleVec;

/** \class AbstractReweighting
    \brief Base Reweighting class
 */
class AbstractReweighting {

public:
  /**Constructor*/
  AbstractReweighting(AbstractModel* model, int nBins):
    m_model(model),
    m_bins(nBins),
    m_invndf(1./nBins),
    m_Data     (m_bins, 0.0),
    m_MCweights(m_bins, 0.0),
    m_MCweightsSquared(m_bins, 0.0),
    m_MCEntries(m_bins, 0.0),
    m_Events(0),
    m_log(),
    m_binWeights(omp_get_max_threads(), std::vector<double> (m_bins, 0.0)),
    m_binWeightsSquared(omp_get_max_threads(), std::vector<double> (m_bins, 0.0)),
    //m_file(NULL),
    //m_tree(NULL),
    m_EntryFactor(1.0),
    m_treechi2(0.0),
    m_n_params(model->GetNParameters()),
    m_filename(""),
    m_params(new double[m_n_params]),
    m_cut(0.0),
    theErrorDef(1e-20)
  { }

  /**Comparison operator needed for minuit interface*/
  AbstractReweighting& operator= (const AbstractReweighting& AR){
    std::cout << __PRETTY_FUNCTION__  << std::endl;
    m_model	  = AR.m_model;
    m_bins	  = AR.m_bins;
    m_invndf	  = 1./m_bins;
    m_Data	  = AR.m_Data;
    m_MCweights	  = AR.m_MCweights;
    m_MCweightsSquared	  = AR.m_MCweightsSquared;
    m_MCEntries	  = AR.m_MCEntries;
    m_Events	  = AR.m_Events;
    m_log         = AR.m_log;
    m_binWeights  = AR.m_binWeights;
    m_binWeightsSquared  = AR.m_binWeightsSquared;
    m_EntryFactor = AR.m_EntryFactor;
    m_treechi2	  = AR.m_treechi2;
    m_n_params	  = m_model->GetNParameters();
    m_filename	  = AR.m_filename;
    m_params	  = new double[m_n_params];
    m_cut	  = AR.m_cut;
    theErrorDef	  = 1e-20;
    for (int i = 0; i < m_n_params;++i) {
      m_params[i] = AR.m_params[i];
    }
    return *this;
  }


  /**Need explicit copy c'tor*/
  AbstractReweighting(AbstractReweighting const& AR):
    m_model(AR.m_model),
    m_bins(AR.m_bins),
    m_invndf(1./m_bins),
    m_Data     (AR.m_Data),
    m_MCweights(AR.m_MCweights),
    m_MCweightsSquared(AR.m_MCweightsSquared),
    m_MCEntries(AR.m_MCEntries),
    m_Events(AR.m_Events),
    m_log(AR.m_log),
    m_binWeights(AR.m_binWeights),
    m_binWeightsSquared(AR.m_binWeightsSquared),
    m_EntryFactor(AR.m_EntryFactor),
    m_treechi2(AR.m_treechi2),
    m_n_params(m_model->GetNParameters()),
    m_filename(AR.m_filename),
    m_params(new double[m_n_params]),
    m_cut(AR.m_cut),
    theErrorDef(AR.theErrorDef)
  {
    for (int i = 0; i < m_n_params;++i) {
      m_params[i] = AR.m_params[i];
    }
  } //copy c'tor

  virtual ~AbstractReweighting() 
  { 
    delete[] m_params; 
  }//must not delete m_model as this does not own it.
  
  /** Create the fitting histograms
   */
  void MakeHistograms();

  /** Return the data vector pointer, needed for filling it
   */
  doubleVec* getDataVec(){return &m_Data;}
  
  /** Get the weight vector pointer, needed for filling it
   */
  doubleVec* getWeightVec(){return &m_MCweights;}

  /** Return the MC vector pointer, needed for filling it
   */
  doubleVec* getMCVec(){return &m_MCEntries;}

  /** Get the events vector
   */
  std::vector<Event>* getEvents(){return &m_Events;}

  /** Set the global entry factor, used in the chi2 computation
   */
  void setEntryFactor(double entryF=1.){m_EntryFactor = entryF;}

  //virtual void setCut(double cut)=0;

  /** As name suggests...
  */
  virtual void FillControlHistograms(TString filename = "") = 0;
  //inline void SetTreeFileName(TString filename = ""){m_filename = filename;}
  //virtual void SaveTree();

protected:
  /** Compute the chi2 once all the weights have been computed
   */
  double ComputeChi2();
  /** Fill the weights for every events*/
  int FillWeights();
  /** Clean all temporary objects*/
  void CleanUp();
  /** Fill the default histograms using base info*/
  void FillDefaultHistograms(TString filename = "");
  //void FillTree(const double* p, const double chi2);
  AbstractModel *m_model;//!< Container for the Model
  int m_bins; //!< Number of bins
  double m_invndf; //!< inverse of the number of degrees of freedom
  doubleVec m_Data; //!< Data vector
  doubleVec m_MCweights; //!< MC weights vector
  doubleVec m_MCweightsSquared; //!< MC weights squared vector
  doubleVec m_MCEntries; //!< MC Entries vector 
  std::vector<Event> m_Events; //!< Events vector

  log4cplus::Logger m_log; //!< Logger class
  std::vector<doubleVec > m_binWeights; //!< Weights for every bin
  std::vector<doubleVec > m_binWeightsSquared; //!< Same as above but squared
  static TFile*m_file; //!< Pointer to the file containing ?
  static TTree*m_tree; //!< Pointer to the tree ?

private:
  double m_EntryFactor;//!< Ratio between the number of events in the MC and those in the Data
  double m_treechi2;//!< ??
  int m_n_params; //!< Number of parameters
  TString m_filename; //!< file name where to store the histograms
  double *m_params; //!< Array to the parameters
  double m_cut;//!< Cut value used for filling the histograms 

public:
  //For the Minuit2 Fitter
  /**Needed for Minuit interface*/
  double operator() (const double* x);
  /**Needed for Minuit interface*/
  double Up() const { return 1.; }
  /**Needed for Minuit interface*/
  void setErrorDef(double def) {theErrorDef = def;}
  double theErrorDef;//!< Needed by Minuit

};


#endif // AbstractReweighting_hh
