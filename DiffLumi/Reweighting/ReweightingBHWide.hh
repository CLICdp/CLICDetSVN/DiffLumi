#ifndef ReweightingBHWide_hh
#define ReweightingBHWide_hh 1

#include "AbstractReweighting.hh"

/** \class ReweightingBHWide
    \brief Class for BHWide fitting
 */
class ReweightingBHWide: public AbstractReweighting {
public:
  /** Constructor */
  ReweightingBHWide(AbstractModel* model, int nBins):
    AbstractReweighting(model, nBins)
  {
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("ReweightingBHWide"));
  }
  virtual ~ReweightingBHWide()
  {};
  void FillControlHistograms(TString filename = "");
private:

};



#endif //ReweightingBHWide_hh
