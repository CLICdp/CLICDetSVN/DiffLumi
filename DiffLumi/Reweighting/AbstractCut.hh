#ifndef AbstractCut_hh
#define AbstractCut_hh

#include <TLorentzVector.h>
#include <string>


#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

/** \class AbstractCut
    \brief Base cut class
 */

class AbstractCut {
public:
  /**Constructor*/
  AbstractCut():
    m_cutval(0.0),m_log()
  { }
  virtual ~AbstractCut()
  { }
  /**Copy contructor*/
  AbstractCut(const AbstractCut&);
  /**Comparison operator*/
  AbstractCut& operator=(const AbstractCut&);
  
  /** Cut for final state electrons
   */
  virtual bool cut(const TLorentzVector , const TLorentzVector ){return true;}

  /** Cut for initial electron fit
   */
  virtual bool cut(const double , const double ){return true;}

  /** Set the cut value
      @param val Value of the cut
   */
  virtual void setCutVal(double val){m_cutval = val;}
  
  /** Factory
      @param cuttype Cut instance you want to use
   */
  static AbstractCut* getCut(std::string cuttype);

protected:
  double m_cutval;//!< Cut value
  log4cplus::Logger m_log; //!< Logger class
};

#endif
