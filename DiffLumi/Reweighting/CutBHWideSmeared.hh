#ifndef CutBHWideSmeared_hh
#define CutBHWideSmeared_hh

#include "AbstractCut.hh"

class CutBHWideSmeared: public AbstractCut
{
public:
  CutBHWideSmeared(): 
    AbstractCut()
  {
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("CutBHWideSmeared"));
  }
  ~CutBHWideSmeared(){};
  
  bool cut(const TLorentzVector p1, const TLorentzVector p2);

};
#endif
