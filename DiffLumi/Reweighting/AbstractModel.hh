#ifndef AbstractModel_hh
#define AbstractModel_hh 1

#include "BetaFunction.h"
#include "BetaFunctionBS.h"
#include "IBaseFunction.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <string>


/** \class AbstractModel
    \brief Base Model class
 */
class AbstractModel 
{     

public:

  enum Parts{ kNONE=-1, kPeak, kArm1, kArm2, kBody};

  /** Contructor
      @param parameters Array of parameters. Maximum size: 35.
   */
  AbstractModel(const double *parameters);

  /**Copy constructor*/
  AbstractModel(const AbstractModel&);
  /**Comparison operator*/
  AbstractModel& operator=(const AbstractModel&);

  virtual ~AbstractModel();

  /** Compute the probability of an event given its properties
   */
  virtual double CalculateProbability(double eb1, double eb2, double deb1, double deb2, double) const = 0;
  /** Generate an event properties
   */
  virtual Parts GetRandomVariates(double& x1, double& x2, double& dx1, double& dx2, double& dx1Gauss, double& dx2Gauss,
				  double& RelativeProbability, double BeamSpreadSmearing) const = 0;

  /** Change the parameters' values, used between minimizer iterations.
   */
  //Cannot implement changeParameters in abstract, because it depends on the
  //IBaseFunction implementations
  virtual void ChangeParameters(const double *parameter);

  /** Get the number of parameters
   */
  virtual int GetNParameters() = 0;

  /** Obtain the model name
   */
  virtual std::string GetName() const = 0;

  /** Model Factory
      @param modelName Name of the model to obtain
      @param parameter Array of parameters needed for the construction of the object
   */
  static AbstractModel* getModel(std::string modelName,const double *parameter);
protected:
  virtual void Instantiate(const double *parameter);//!< Create the individual functions and call their integral

  double m_peak;//!< Fraction of peak events
  double m_arm1;//!< fraction of events in the arm1
  double m_arm2;//!< fraction of events in the arm2
  double m_body;//!< fraction of events in the body
  IBaseFunction* m_BeamSpread1;//!< function for the peak
  IBaseFunction* m_BeamSpread2;//!< function for the peak
  IBaseFunction* m_BeamSpreadArm1;//!< function for the arm1
  IBaseFunction* m_BeamSpreadArm2;//!< function for the arm2
  IBaseFunction* m_BeamSpreadBody1;//!< function for the body
  IBaseFunction* m_BeamSpreadBody2;//!< function for the body
  IBaseFunction* m_BeamstrahlungArm1;//!< function for the arm1
  IBaseFunction* m_BeamstrahlungArm2;//!< function for the arm2
  IBaseFunction* m_BeamstrahlungBody1;//!< function for the body
  IBaseFunction* m_BeamstrahlungBody2;//!< function for the body

  log4cplus::Logger m_log; //!< Logger class

};


#endif // AbstractModel_hh
