#include "AbstractModel.hh"
#include "ModelSeparate.hh"
#include "ModelOverlap.hh"
AbstractModel::AbstractModel(const double* parameters):
  m_peak(parameters[0]), m_arm1(parameters[1]), m_arm2(parameters[2]), m_body(1.0 - m_peak - m_arm1 - m_arm2),
  m_BeamSpread1(NULL),
  m_BeamSpread2(NULL),
  m_BeamSpreadArm1(NULL),
  m_BeamSpreadArm2(NULL),
  m_BeamSpreadBody1(NULL),
  m_BeamSpreadBody2(NULL),
  m_BeamstrahlungArm1(NULL),
  m_BeamstrahlungArm2(NULL),
  m_BeamstrahlungBody1(NULL),
  m_BeamstrahlungBody2(NULL),
  m_log(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("AbstractModel")))
{
  Instantiate(parameters);
}//Ctor

AbstractModel::~AbstractModel(){
  delete m_BeamSpread1;
  delete m_BeamSpread2;
  delete m_BeamSpreadArm1;
  delete m_BeamSpreadArm2;
  delete m_BeamSpreadBody1;
  delete m_BeamSpreadBody2;
  delete m_BeamstrahlungArm1;
  delete m_BeamstrahlungArm2;
  delete m_BeamstrahlungBody1;
  delete m_BeamstrahlungBody2;
}

AbstractModel* AbstractModel::getModel(std::string modelName, const double *parameter){
  if (modelName=="Separate") return new ModelSeparate(parameter);
  else if (modelName=="Overlap") return new ModelOverlap(parameter);
  else {
    return NULL;
  }
}

void AbstractModel::ChangeParameters(const double *parameter) {

  m_BeamSpread1->ChangeParameters(&(parameter[5]));
  m_BeamSpread2->ChangeParameters(&(parameter[11]));

  m_BeamSpreadArm1->ChangeParameters(&(parameter[19]));
  m_BeamSpreadArm2->ChangeParameters(&(parameter[23]));

  m_BeamSpreadBody1->ChangeParameters(&(parameter[27]));
  m_BeamSpreadBody2->ChangeParameters(&(parameter[31]));

  m_BeamstrahlungArm1    ->ChangeParameters(&(parameter[3]));
  m_BeamstrahlungArm2    ->ChangeParameters(&(parameter[9]));

  m_BeamstrahlungBody1->ChangeParameters(&(parameter[15]));
  m_BeamstrahlungBody2->ChangeParameters(&(parameter[17]));

  m_peak = parameter[0];
  m_arm1 = parameter[1];
  m_arm2 = parameter[2];

  m_body = 1. - m_peak - m_arm1 - m_arm2;
  //m_EntryFactor = parameter[19];//Not there anymore, as determined when filling the tuples

  return;
}

void AbstractModel::Instantiate(const double *parameters) {
  //Create the Function Objects and set the parameters for them
  m_BeamSpread1 = new BetaFunctionBS(parameters[5],  parameters[6] , parameters[7],  parameters[8]  );
  m_BeamSpread2 = new BetaFunctionBS(parameters[11], parameters[12], parameters[13], parameters[14] );
  m_BeamSpreadArm1 = new BetaFunctionBS(parameters[19],  parameters[20], parameters[21], parameters[22] );
  m_BeamSpreadArm2 = new BetaFunctionBS(parameters[23],  parameters[24], parameters[25], parameters[26] );
  m_BeamSpreadBody1 = new BetaFunctionBS(parameters[27],  parameters[28], parameters[29], parameters[30] );
  m_BeamSpreadBody2 = new BetaFunctionBS(parameters[31],  parameters[32], parameters[33], parameters[34] );
  m_BeamstrahlungArm1 = new BetaFunction(parameters[3],  parameters[4]); 
  m_BeamstrahlungArm2 = new BetaFunction(parameters[9],  parameters[10]);
  m_BeamstrahlungBody1 = new BetaFunction(parameters[15], parameters[16]);
  m_BeamstrahlungBody2 = new BetaFunction(parameters[17], parameters[18]);


}//Instatiate
