#include "TupleBHWide.hh"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"

#include <algorithm>
#include <cmath>
#include <set>
#include <stdexcept>

void TupleBHWide::Init()//called from the AbstractTuple constructor
{
  // m_file->GetObject("MyTree", m_tree);
  // if (NULL == m_tree){
  //   LOG4CPLUS_FATAL(m_log, "MyTree is not in " << m_filename);
  //   exit(1);
  //}
  m_tree = new TChain("MyTree","MyTree");
  if (!m_tree->Add(m_filename)){
    LOG4CPLUS_FATAL(m_log, "MyTree is not in " << m_filename);
    exit(1);
  }
  m_tree->SetBranchAddress("eb1", &m_eb1);
  m_tree->SetBranchAddress("eb2", &m_eb2);
  m_tree->SetBranchAddress("deb1", &m_deb1);
  m_tree->SetBranchAddress("deb2", &m_deb2);
  m_tree->SetBranchAddress("E1", &m_E1);
  m_tree->SetBranchAddress("E2", &m_E2);
  m_tree->SetBranchAddress("px1", &m_px1);
  m_tree->SetBranchAddress("py1", &m_py1);
  m_tree->SetBranchAddress("pz1", &m_pz1);
  m_tree->SetBranchAddress("e1", &m_e1);
  m_tree->SetBranchAddress("px2", &m_px2);
  m_tree->SetBranchAddress("py2", &m_py2);
  m_tree->SetBranchAddress("pz2", &m_pz2);
  m_tree->SetBranchAddress("e2", &m_e2);
  m_tree->SetBranchAddress("nphotons", &m_nphotons);
  m_tree->SetBranchAddress("photdata",  m_photdata);
  m_tree->SetBranchAddress("RelativeProbability", &m_relativeProbability);

  if( m_entries < 0 || m_entries > m_tree->GetEntries() ) {
    m_entries = m_tree->GetEntries();
  }
  return ;
}

long TupleBHWide::FillVectors(std::vector<double>*data, std::vector<Event> *events, std::vector<double>*weights) const {
  LOG4CPLUS_DEBUG(m_log, "FillVectors");
  long acceptedEvents(0), skippedEvents(0);
  Long64_t TreeEntries(m_tree->GetEntries());
  for ( Long64_t i = 0; (i < m_entries + skippedEvents && i < TreeEntries); ++i){

    if(i%(m_entries/100)==0) {
      std::cout << '\r' << "Filling: " << std::setw(10) <<int(100*(double(i)/m_entries)) << "%" << std::flush;
    }

    int ret = m_tree->GetEntry(i);
    if ( ret <= 0 ){
      LOG4CPLUS_TRACE(m_log, "BREAK  " << "  " << i << " retval " << ret);
      break;
    }

    if( m_E1 < 0 || m_E2 < 0 ) {
      ++skippedEvents;//need to add one so that the number of events processed is right
      continue;
    }

    ///here we need to put the cuts


    TLorentzVector lve1(m_px1,m_py1,m_pz1,m_e1);
    TLorentzVector lve2(m_px2,m_py2,m_pz2,m_e2);
    if ( m_cutclass->cut(lve1,lve2)) {
      ++skippedEvents;//need to add one so that the number of events processed is right
      continue;
    }
    double acol = effective_sqrts( lve1.Theta(), lve2.Theta() );

    if ( isnan(acol) ) {
    	continue;
    }

    LOG4CPLUS_TRACE(m_log, "Acol: " << acol 
		    << "  e1:"<< lve1.E()/m_nominalBeamEnergy 
		    << "  e2:" << lve2.E()/m_nominalBeamEnergy);
    int bin = m_storedBins.GetBinNumber(acol, lve1.E()/m_nominalBeamEnergy, lve2.E()/m_nominalBeamEnergy);
    try {
      data->at(bin) +=1;
      ++acceptedEvents;
      if (events){
	events->push_back(Event(m_eb1, m_eb2, m_deb1, m_deb2, 1./m_relativeProbability, bin, m_E1, m_E2));
	weights->at(bin) = 0.0;
      }
    } catch (std::out_of_range &e) {
      std::cout << e.what()  << std::endl;
      lve1.Print();
      lve2.Print();
      std::cout << "Bin " << bin
		<< " event " << i
		<< std::setw(15) << m_px1 << std::setw(15) <<  m_py1 << std::setw(15) <<  m_pz1 << std::setw(15) <<  m_e1
		<< std::setw(15) << m_px2 << std::setw(15) <<  m_py2 << std::setw(15) <<  m_pz2 << std::setw(15) <<  m_e2
		<< " acol " << acol
		<< " theta1 " << lve1.Theta()
		<< " theta2 " << lve2.Theta()
		<< " e1:"<< lve1.E()/m_nominalBeamEnergy 
		<< " e2:" << lve2.E()/m_nominalBeamEnergy
		<< std::endl;
    }
  }
  std::cout << std::endl;//for the filling \r flush
  return acceptedEvents;
}

void TupleBHWide::CreateEquiProbability(TString filename)
{
  LOG4CPLUS_INFO(m_log, "CreateEquiProbability");
  const double acolMax(1.0001);
  //  const double energyMax(1.04);//used to be 1550/1500
  const double energyMax(1600.0/1500.0);//used to be 1550/1500
  Long64_t entries = m_tree->GetEntries();
  TH1D h_acol("h_acol", "h_acol", m_entries, 0, acolMax);

  LOG4CPLUS_INFO(m_log, "Reading events for the first time.");
  int isnanCounter(0);
  {
    Long64_t i = 0, evts = 0;
    do{
      if(i%(entries/100)==0) {
	std::cout << '\r' << "Reading: " << std::setw(10) <<int(100*(double(i)/entries)) << "%" << std::flush;
      }

      if (m_tree->GetEntry(i)<=0){
	break;
      }

      i++;
      if (i > entries){
	LOG4CPLUS_FATAL(m_log, "Skipped too many events, abort!");
	exit(1);
      }
      if( m_E1 < 0 || m_E2 < 0) continue;
      
      TLorentzVector lve1(m_px1,m_py1,m_pz1,m_e1);
      TLorentzVector lve2(m_px2,m_py2,m_pz2,m_e2);
      if (m_cutclass->cut(lve1,lve2)) {
	continue;
      }

      double acol(effective_sqrts( lve1.Theta(), lve2.Theta() ));
      if ( isnan(acol) ) {
	isnanCounter++;
	continue;
      }

      //if (acol < m_cut){
      //	continue;
      //}

      h_acol.Fill( acol );
      evts++;
    } while (evts<m_entries);
    LOG4CPLUS_INFO(m_log, " i " << i <<  "  evts " << evts << " events in tree " << entries);
  
  }

  LOG4CPLUS_INFO(m_log,  "Rejected " << isnanCounter << " events because isNan.");

  LOG4CPLUS_INFO(m_log, "Integrating Acol Histogram");

  TH1D h_int_acol("h_int_acol","h_int_acol", m_entries, 0, acolMax);
  double inte_acol=0;
  for (int i = 1; i <= m_entries; i++) {
    inte_acol += h_acol.Integral(i,i);
    h_int_acol.SetBinContent(i,inte_acol);
  }

  double max_acol = h_int_acol.GetMaximum();
  double slice_acol = max_acol/double(m_nbinx);
  std::vector<double> binsx(m_nbinx+1,0.0);
  int multi = 0;
  binsx[0]=0.;

  LOG4CPLUS_INFO(m_log, "Binning in X");

  for (int i =1; i<=h_int_acol.GetNbinsX();i++) {
    if (h_int_acol.GetBinContent(i) > double(multi+1)*slice_acol){
      multi++;
      binsx[multi] = h_int_acol.GetBinLowEdge(i);
      LOG4CPLUS_TRACE(m_log,"binx: "<< binsx[multi]);
    }
  }
  binsx[m_nbinx]=acolMax;

  const int nLargeBinsY = 100000;
  std::vector<double> binsyt(nLargeBinsY+1, 0.0);
  binsyt[0]=0.;

  for (int i = 1;i<nLargeBinsY;i++) {
    binsyt[i]=binsyt[i-1]+(energyMax)/double(nLargeBinsY);
  }
  binsyt[nLargeBinsY]=energyMax;

  int isnanCounter2(0);
  
  TH2D h2d("h2d","h2d",m_nbinx,&binsx[0],nLargeBinsY,&binsyt[0]);
  {
    Long64_t i=0, evts=0;
    do{
      if (m_tree->GetEntry(i)<=0){
	break;
      }
      i++;

      if( m_E1 < 0 || m_E2 < 0) continue;
      
      TLorentzVector lve1(m_px1,m_py1,m_pz1,m_e1);
      TLorentzVector lve2(m_px2,m_py2,m_pz2,m_e2);
      if (m_cutclass->cut(lve1,lve2)) {
	continue;
      }
      double acol(effective_sqrts( lve1.Theta(), lve2.Theta() ) );
      //if (acol< m_cut) continue;
      if ( isnan(acol) ) {
	isnanCounter2++;
	continue;
      }

      h2d.Fill(acol, lve1.E()/m_nominalBeamEnergy);
      evts++;
    }while (evts < m_entries);
  }

  LOG4CPLUS_INFO(m_log,  "Rejected " << isnanCounter2 << " events because isNan.");


  std::vector< std::vector<double> > vectbiny(m_nbinx+1);
  std::cout << "Binning for y"  << std::endl;
  for (int i = 1; i <= m_nbinx; i++) {
    std::cout << '\r' << "Bin: " << std::setw(5) << i << std::flush;

    TH1D*htemp = NULL;
    htemp = h2d.ProjectionY("h_temp",i,i);
    TH1D h_int_e1(Form("h_int_e1_%i", i), "h_int_e1", nLargeBinsY, 0, energyMax);
    double inte_e1 = 0;
    for (int j = 1; j <= nLargeBinsY; j++) {
      inte_e1 += htemp->Integral(j,j);
      h_int_e1.SetBinContent(j, inte_e1);
    }
    //h_int_e2.Write();
    double max_e1 = h_int_e1.GetMaximum();
    const double slice_e1 = max_e1/double(m_nbiny);
    std::vector<double> binsytmp(m_nbiny+1,0.0);
    multi = 0;
    binsytmp[0]=0.;

    for (int j = 1; j <= h_int_e1.GetNbinsX(); j++) {
      if (h_int_e1.GetBinContent(j) > double(multi+1)*slice_e1){
	multi++;
	binsytmp[multi] = h_int_e1.GetBinLowEdge(j);
	LOG4CPLUS_TRACE(m_log,"n binx:"<<i<<" biny:"<<binsytmp[multi]);
      }
    }

    binsytmp[m_nbiny]=energyMax;
    vectbiny[i]=binsytmp;

    delete htemp;
  }

  // std::cout << std::endl << "Creating the tempHistos. Seriously, if you are not running this on lxplus with "
  // 	    << "50GB RAM to spare you should kill this now!"  << std::endl;

  //create all the htemps, so we only have to go over the tree once.  We use a
  //std::vector instead of a std::set because storage size is smaller and
  //because we can use random_access iterators later on!
  std::vector< std::vector< std::vector<double> > > htemp(m_nbinx, std::vector< std::vector<double> >(m_nbiny));
							  //[m_nbinx][m_nbiny

  std::cout << "Filling them"  << std::endl;
  {
    Long64_t ievt=0, evts=0;
    int isnanCounter3(0);
    do{
      if (m_tree->GetEntry(ievt)<=0){
	break;
      }
      ievt++;

      if( ievt % 10000 == 0) {
	std::cout << '\r' << "Filling " << std::setw(5) << ievt << std::flush;
      }

      if( m_E1 < 0 || m_E2 < 0) continue;
      
      TLorentzVector lve1(m_px1, m_py1, m_pz1, m_e1);
      TLorentzVector lve2(m_px2, m_py2, m_pz2, m_e2);

      if (m_cutclass->cut(lve1,lve2)) {
	continue;
      }	  

      double acol = effective_sqrts( lve1.Theta(), lve2.Theta() );
      if ( isnan(acol) ) {
	isnanCounter3++;
	continue;
      }
      //if ( acol < m_cut) continue;
      int i = 0;
      try {
	while ( binsx[i] < acol  ) {
	  i++;
	}
      } catch (std::out_of_range& e) {
	std::cout << e.what()  << std::endl;
	std::cout << " Binx " << i  << "   " << acol  << std::endl;
      }

      // if( i > 50 ) {
      // 	std::cout << " Histo " << i
      // 		  << std::setw(15) << acol
      // 		  << std::setw(15) << lve1.E()
      // 		  << std::endl;
      // }

      std::vector<double> const& binsy = vectbiny.at(i);

      //figure out which j
      int j = 0;
      try {
	while ( binsy.at(j) < lve1.E()/m_nominalBeamEnergy ){
	  j++;
	}
      } catch (std::out_of_range& e) {
	std::cout << e.what()  << std::endl;
	lve1.Print();
	lve2.Print();
	std::cout << "Entry " << ievt
		  << " Binx " << i
		  << " acol " << acol
		  << std::setw(15) << m_px1 << std::setw(15) <<  m_py1 << std::setw(15) <<  m_pz1 << std::setw(15) <<  m_e1
		  << std::setw(15) << m_px2 << std::setw(15) <<  m_py2 << std::setw(15) <<  m_pz2 << std::setw(15) <<  m_e2
		  << std::setw(15) << lve1.Theta()
		  << std::setw(15) << lve2.Theta()
		  << " Biny " << j
		  << " eng " << lve1.E()
		  << std::endl;
	continue;
      }
      
      htemp[i-1][j-1].push_back( lve2.E()/m_nominalBeamEnergy );
      evts++;

    }while (evts < m_entries);
    LOG4CPLUS_INFO(m_log,  "Rejected " << isnanCounter3 << " events because isNan.");
  }
  std::cout << std::endl << "Done filling"  << std::endl;

  //Now look at the 3rd dimension
  std::cout << std::endl << "Binning for Z"  << std::endl;
  for (int i = 1; i <= m_nbinx; ++i) {
   
    std::vector<double> const& binsy = vectbiny.at(i);
    for (int j = 1; j <= m_nbiny; ++j) {
      std::cout << '\r'
                << "Bin X: " << std::setw(5) << i << "  "
		<< "Bin Y: " << std::setw(5) << j << std::flush;

      std::vector<double>& thisVector = htemp.at(i-1).at(j-1);
      std::sort(thisVector.begin(), thisVector.end());

      double integral = (0.0);
      double thisContent(double(thisVector.size()));
      double slice_e2 = double(thisContent)/double(m_nbinz);
      std::vector<double> binsz(m_nbinz+1,0.0);
      binsz[0]=0.;

      multi = 0;

      //get the bin boundaries from the events
      for (std::vector<double>::const_iterator it = thisVector.begin(); it != thisVector.end()-1; ++it) {
	integral++;
	if( double(integral) >= double(multi+1) * slice_e2 ) {

	  multi++;
	  binsz.at(multi) = (*(it+1) + *it)/2.0; //take the average distance between this event and the next

	  // std::cout << std::setw(3) << multi << std::setw(15) << binsz.at(multi)  
	  // 	    << std::setw(5) << integral
	  // 	    << std::setw(15) << slice_e2 * double(multi)
	  // 	    << std::endl;

	}
      }

      //last one at energyMax 
      binsz[m_nbinz]=energyMax;
 
     for (int k = 1; k <= m_nbinz; ++k) {
	m_storedBins.SetBinBoundaries(i-1,j-1,k-1,binsx[i],binsy[j],binsz[k]);
	LOG4CPLUS_DEBUG(m_log, "bin upper range : "<< binsx.at(i)<<"  "<< binsy.at(j) << "  " << binsz.at(k));
      }
    }
  }
  std::cout << std::endl;

  if (filename.Length()) {
    LOG4CPLUS_DEBUG(m_log,"Saving binning to "<<filename.Data());
    m_storedBins.SaveToFile(filename);
  }
  return ;
}
void TupleBHWide::FillLumiHistogram(TString filename){
  TFile*file = TFile::Open(filename,"UPDATE");
  TH1D lumi("lumi_reference","Lumi",10000 ,0,1.05);
  long skippedEvents(0);
  long treeEntries(m_tree->GetEntries());
  for ( int i = 0; (i < m_entries + skippedEvents) && (i < treeEntries); ++i) {
    m_tree->GetEntry(i);
    //double eb1r = m_eb1 + m_deb1;
    //double eb2r = m_eb2 + m_deb2;
    if(m_E1 < 0 || m_E2 < 0) {
      ++skippedEvents; //we need to add one to still process the right number of events...
      continue;
    }
    TLorentzVector lve1(m_px1,m_py1,m_pz1,m_e1);
    TLorentzVector lve2(m_px2,m_py2,m_pz2,m_e2);

    if (m_cutclass->cut(lve1,lve2)) {
      ++skippedEvents;
      continue;
    }
    //double acol(effective_sqrts( lve1.Theta(), lve2.Theta() ));
    //if (acol < m_cut){
    //  ++skippedEvents;
    //  continue;
    //}
    lumi.Fill(sqrt(m_E1*m_E2));
  }
  lumi.Write();
  file->Close();
  delete file;
  return;
}
