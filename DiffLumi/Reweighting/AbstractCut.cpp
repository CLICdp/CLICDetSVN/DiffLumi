#include "AbstractCut.hh"

#include "CutBHWide.hh"
#include "CutBHWideSmeared.hh"
#include "CutGenerator.hh"

AbstractCut* AbstractCut::getCut(std::string cuttype){
  if (cuttype=="BHWide"){
    return new CutBHWide();
  }else if (cuttype=="BHWide_smeared"){
    return new CutBHWideSmeared();
  }else if (cuttype=="Generator"){
    return new CutGenerator();
  }else{
    return NULL;
  }
}
