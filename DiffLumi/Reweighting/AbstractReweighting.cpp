#include "AbstractReweighting.hh"
#include "TStopwatch.h"
#include "TH1D.h"
#include "TFile.h"
#include "Event.hh"
#include <sstream>
#include <cmath>
#include "TROOT.h"

TFile * AbstractReweighting::m_file(0);
TTree * AbstractReweighting::m_tree(0);

double AbstractReweighting::operator()(const double* params)
{
  TStopwatch timer2;
  
  //One needs to get new objects for iteration, in particular new definitions for the probas
  m_model->ChangeParameters(params);

  std::ostringstream text;
  text << "Para: ";
  for (int i = 0; i < m_n_params; ++i) {
    //    std::cout << "Parameter " << i << "  " << params[i]  << std::endl;
    text.precision(4);
    text << std::setw(12)  <<  params[i];
  }
  text.unsetf( std::ios_base::scientific );
  LOG4CPLUS_DEBUG(m_log, text.str());

  if( this->FillWeights()){
    exit(2);
  }
  //compute chi2
  //const double chi2(this->ComputeChi2()*m_invndf);
  const double chi2(this->ComputeChi2());
  LOG4CPLUS_INFO(m_log, "Chi2 = "<< std::scientific << std::setw(14) << chi2); 
  
  //Now that we are done with this call, cleanup
  CleanUp();
  
  timer2.Stop();
  LOG4CPLUS_DEBUG(m_log, "Used "<<timer2.RealTime()<<" s of realtime");
  LOG4CPLUS_DEBUG(m_log, "Used "<<timer2.CpuTime()<<" s of CPU");
  return chi2;
}
int AbstractReweighting::FillWeights()
{
  const unsigned int evt_size = m_Events.size();
  int fail = 0;
  //  int counter = 0;
#pragma omp parallel
  {
#pragma omp for schedule(dynamic)
    for (unsigned int i =0 ; i < evt_size ; ++i){
      const Event event(m_Events[i]);
      const double evt_Proba(m_model->CalculateProbability( event.eb1, event.eb2,
							    event.deb1, event.deb2,
							    0.0));
      if (evt_Proba<0){
       	LOG4CPLUS_FATAL(m_log, "For some reason evt_Proba is negative: "<<evt_Proba );
#pragma omp atomic
       	fail += 1;
       	continue;
      }
      m_binWeights[omp_get_thread_num()][event.binNumber] += evt_Proba*event.invRelativeProbability;
      m_binWeightsSquared[omp_get_thread_num()][event.binNumber] += 
	evt_Proba*event.invRelativeProbability
	*evt_Proba*event.invRelativeProbability;
      // if ( counter < 50) {
      // 	counter++;
      // 	std::cout 
      // 	  << std::setw(15) << event.eb1
      // 	  << std::setw(15) << event.eb2
      // 	  << std::setw(15) << event.deb1
      // 	  << std::setw(15) << event.deb2
      // 	  << std::setw(15) << evt_Proba
      // 	  << std::setw(15) << event.relativeProbability
      // 	  << std::setw(15) << evt_Proba/event.relativeProbability
      // 	  << std::endl;

      // }
    }
    
  }
  if (fail){
    return fail;
  }
  for (unsigned int i = 0; i < m_binWeights[0].size();++i) {
    double thisBinContent(0.0);
    double thisBinContentSquared(0.0);
    for (int j = 0; j < omp_get_max_threads() ; ++j) {
      thisBinContent += m_binWeights[j][i];
      thisBinContentSquared += m_binWeightsSquared[j][i];
      m_binWeights[j][i] = 0.0;
      m_binWeightsSquared[j][i] = 0.0;
    }
    m_MCweights[i] = thisBinContent * m_EntryFactor;
    m_MCweightsSquared[i] = thisBinContentSquared * m_EntryFactor * m_EntryFactor;
    LOG4CPLUS_TRACE(m_log, "Bin "<<i<<" has entry "<<m_MCweights[i]);
  }
  return 0;
}

double AbstractReweighting::ComputeChi2()
{
  double chi2(0.);
  const int nBins(m_Data.size());

  double integralMC(0.0), integralData(0.0);

#pragma omp parallel reduction(+:integralData,integralMC)
  {

#pragma omp for schedule(dynamic)
    for (int i = 0; i < nBins; ++i) {
      //const double newEntr(m_MCEntries[i]);
      // if ( newEntr <= 7) {
      // 	LOG4CPLUS_DEBUG(m_log, "Not enough entries in Entries bin "<<i<<": "<<newEntr);
      // 	continue;
      // } 
      //const double refCont(m_Data[i]);
      // if ( refCont <= 7) {
      // 	LOG4CPLUS_DEBUG(m_log, "Not enough entries in Data bin "<<i<<": "<<refCont);
      // 	continue;
      // }
      integralData += m_Data[i];
      integralMC   += m_MCweights[i];
    }//All Bins

    //std::exit(1);
  }// parallel pragma

  double NormalizationFactor(integralData/integralMC);

  //#pragma omp parallel num_threads(OMP_NUM_THREADS) reduction(+:chi2)
#pragma omp parallel reduction(+:chi2)
  {

#pragma omp for schedule(dynamic)
    for (int i = 0; i < nBins; ++i) {

      //const double newEntr(m_MCEntries[i]);
      // if ( newEntr <= 7) {
      // 	LOG4CPLUS_DEBUG(m_log, "Not enough entries in Entries bin "<<i<<": "<<newEntr);
      // 	continue;
      // } 
      const double refCont(m_Data[i]);
      // if ( refCont <= 7) {
      // 	LOG4CPLUS_DEBUG(m_log, "Not enough entries in Data bin "<<i<<": "<<refCont);
      // 	continue;
      // }
      //    if(! ((refErr > 7) && (newEntr > 7))) continue;
      
      const double newCont(m_MCweights[i]);
      const double newError(m_MCweightsSquared[i]);
      //LOG4CPLUS_DEBUG(m_log, "The ref error is NOT computed properly, needs to be fixed");
      // const double refErr(sqrt(m_Data[i]));///////THIS NEEDS TO BE SORTED OUT

      const double delta(newCont * NormalizationFactor - refCont);
      
      // std::cout 
      // 	<< std::setw(15) << refCont
      // 	<< std::setw(15) << newCont
      // 	<< std::setw(15) << delta  
      // 	<< std::setw(15) << m_EntryFactor
      // 	<< std::endl;

      const double chiTemp(delta*delta / (refCont  + 
					  //newEntr*m_EntryFactor*m_EntryFactor
					  newError
					  *NormalizationFactor*NormalizationFactor));
      
      chi2 += chiTemp;
      
    }//All Bins

    //std::exit(1);
  }// parallel pragma
  if (chi2<0) {
    LOG4CPLUS_ERROR(m_log,"chi2 negative, putting back to a high value");
    chi2 = 1e6;
  }

  return chi2;
}
void AbstractReweighting::CleanUp(){
  //not necessary to clean up, as is used in assignment anyway in FillWeights
  // for (unsigned int i =0 ; i<m_MCweights.size() ; ++i){
  //   m_MCweights[i] = 0.0;
  //   m_MCweightsSquared[i] = 0.0;
  // }
}

void AbstractReweighting::FillDefaultHistograms(TString filename){
  TFile*outputf = TFile::Open(filename,"UPDATE");
  static int counter = 0;
  //double cut = m_cut*m_cut;
  TH1D h_weights(Form("h_weights_%i", counter),"Weights histogram",m_MCweights.size(),0,m_MCweights.size());
  TH1D h_data(Form("h_data_%i", counter),"data histogram",m_MCweights.size(),0,m_MCweights.size());
  TH1D h_lumi(Form("lumi_%i", counter), "Luminosity", 10000 ,0,1.05);

  TH1D h_weights_dist(Form("h_weights_dist_%i", counter),"Distribution of Weights",1000,0, 100);
  h_weights.Sumw2();
  h_lumi.Sumw2();
  h_data.Sumw2();

  for (unsigned int i = 1; i <= m_MCweights.size() ; ++i){
    h_data.SetBinContent(i,m_Data[i-1]);
  }

  for (unsigned int i =0; i<m_Events.size();++i){
    Event event = m_Events[i];
    const double evt_Proba(m_model->CalculateProbability( event.eb1, event.eb2,
							  event.deb1, event.deb2,
							  0.0));
    double e1 = event.E1;
    double e2 = event.E2;
    //    double w  = h_weights.GetBinContent(event.binNumber+1);
    const double weight(evt_Proba*event.invRelativeProbability);
    h_weights.Fill(event.binNumber+0.5,weight);//dont have access to scaling
					       //factor, but that is constant
					       //for all entries, so we can
					       //always apply it later

    h_lumi.Fill(sqrt(e1*e2),weight);
    h_weights_dist.Fill(weight);
  } 

  h_weights_dist.Write();
  h_weights.Write();
  h_data.Write();
  h_lumi.Write();
  outputf->Close();
  counter++;
  delete outputf;
  return;
}

// void AbstractReweighting::FillTree(const double* params, const double chi2){
//   if (!m_file) {
//     LOG4CPLUS_INFO(m_log,"Opening "<<m_filename<<" to store fit progress.");
//     m_file = TFile::Open(m_filename,"RECREATE");
//     m_tree = new TTree("FitTree","FitTree");
//     m_tree->Branch("n_params", &m_n_params,"n_params/I");
//     m_tree->Branch("Parameters",m_params,"Parameters[n_params]/D");
//     m_tree->Branch("Chi2",&m_treechi2,"Chi2/D");
//   }
//   for (int i =0;i<m_n_params; ++i){
//     m_params[i] = params[i];
//   }
//   m_treechi2 = chi2;
//   m_tree->Fill();
//   return;
// }
// void AbstractReweighting::SaveTree(){
//   if (!m_file) {
//     LOG4CPLUS_ERROR(m_log,"File was not initialized, cannot write to it!");
//     return;
//   }
//   m_file->cd();
//   m_tree->Write();
//   m_file->Close();
// }
