#include "TupleGeneratorSymmetricBinning.hh"
#include "TH1D.h"
#include "TH2D.h"
#include <algorithm>

void TupleGeneratorSymmetricBinning::CreateEquiProbability(TString filename)
{
  LOG4CPLUS_DEBUG(m_log, "CreateEquiProbability");
  double energyMax = 1.05;

  std::vector<double> energies;
  {
    int i =0, evts = 0;
    do {
      m_tree->GetEntry(i);
      i++;
      if (i > m_tree->GetEntries()){
	LOG4CPLUS_FATAL(m_log, "Skipped too many events, abort!");
	exit(1);
      }
      if(m_E1 < 0 || m_E2 < 0) continue;
      if(m_cutclass->cut(m_E1,m_E2)) continue;
      evts++;
      energies.push_back(m_E1);
      energies.push_back(m_E2);
    }while (evts<m_entries);
  }

  sort(energies.begin(), energies.end());
  //std::cout << m_nbinx << "  " << m_nbiny  << std::endl;
  Long64_t vect_size = energies.size();
  int slices_x = floor(vect_size/(double)m_nbinx);
  std::vector<double> binsx(m_nbinx+1,0.0);
  binsx[0] = 0.;
  for (int i = 1 ; i<m_nbinx ; ++i){
    binsx[i] = (energies.at(i*slices_x)+energies.at((i*slices_x)+1))/2.;
  }
  binsx[m_nbinx]=energyMax;

  for (int i = 1 ; i<=m_nbinx ; ++i){
    for (int j = 1; j<=m_nbinx; ++j){
      m_storedBins.SetBinBoundaries(i-1,j-1,binsx.at(i),binsx.at(j));
      LOG4CPLUS_TRACE(m_log, "bin upper range : "<< binsx[i]<<"  "<< binsx[j]);
    }
  }
  if (filename.Length()) {
    LOG4CPLUS_DEBUG(m_log,"Saving binning to "<<filename.Data());
    m_storedBins.SaveToFile(filename);
  }
  return;
}
