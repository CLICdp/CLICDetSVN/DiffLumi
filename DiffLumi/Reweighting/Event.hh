#ifndef Event_hh
#define Event_hh 1
/** \class Event
    \brief Describe what a MC event is
 */
class Event
{
public:  
  double eb1;//!< Energy of the first electron
  double eb2;//!< Energy of the second electron
  double deb1;//!< Effect of the beam strahlung for 1st electron
  double deb2;//!< Effect of the beam strahlung for 2nd electron
  double invRelativeProbability;//!< Relative probability

  int binNumber;//!< Bin number in which the event is

  double E1;//!< Total Energy of 1st electron
  double E2;//!< Total Energy of 2nd electron

  /**Default constructor*/
  Event():
    eb1(0.0), eb2(0.0),  deb1(0.0), deb2(0.0), invRelativeProbability(0.0), binNumber(0.0), E1(0.0), E2(0.0){}

  /**Constructor given all values*/
  Event(double Eng1, double Eng2, double dEng1, double dEng2, double RP, int BN, double e1, double e2):
    eb1(Eng1), eb2(Eng2),  deb1(dEng1), deb2(dEng2), invRelativeProbability(RP), binNumber(BN), E1(e1), E2(e2){}

};

#endif //Event_hh
