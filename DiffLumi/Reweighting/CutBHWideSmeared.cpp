#include "CutBHWideSmeared.hh"

bool CutBHWideSmeared::cut(const TLorentzVector p1, const TLorentzVector p2){
  TLorentzVector sum = p1+p2;
  if (sqrt(sum.Mag2()) < m_cutval || 2*sqrt(p1.E()*p2.E()) < m_cutval || p1.E()<150 || p2.E()<150){
    LOG4CPLUS_TRACE(m_log,"Event FAILS cut.");
    return true;
  } else{
    LOG4CPLUS_TRACE(m_log,"Event passes cut.");
    return false;
  }
}
