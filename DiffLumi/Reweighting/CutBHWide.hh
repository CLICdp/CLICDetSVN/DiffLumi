#ifndef CutBHWide_hh
#define CutBHWide_hh
#include "AbstractCut.hh"
/** \class CutBHWide
    \brief Cuts for BHWide
 */
class CutBHWide: public AbstractCut
{
public:
  /**Constructor*/
  CutBHWide():
    AbstractCut(){
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("CutBHWide"));
  }
  ~CutBHWide(){};

  /**Apply the cuts*/
  bool cut(const TLorentzVector p1, const TLorentzVector p2);
  
};
#endif
