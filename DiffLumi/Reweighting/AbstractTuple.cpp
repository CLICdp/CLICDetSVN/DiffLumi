#include "AbstractTuple.hh"
#include "TH1D.h"
#include <cmath>
#include "TupleBHWide.hh"
#include "TupleGenerator.hh"
#include "TupleGeneratorSymmetricBinning.hh"

void AbstractTuple::LoadEquiProbability(TString filename){
  LOG4CPLUS_DEBUG(m_log, "Reading binning from file "<< filename.Data());
  if (!filename.Length()){
    LOG4CPLUS_FATAL(m_log, "Empty filename when loading binning, cannot proceed" );
    exit(1);
  }
  m_storedBins = StoreBinning(filename);

}


AbstractTuple* AbstractTuple::getTuple(std::string Tupletype, TString filename, int stat){
  if (Tupletype == "Generator"){
    return new TupleGenerator(filename,stat);
  }
  else if (Tupletype == "GeneratorSymmetricBinning"){
    return new TupleGeneratorSymmetricBinning(filename,stat);
  }
  else if (Tupletype == "BHWide"){
    return new TupleBHWide(filename,stat);
  }
  else{
    return NULL;
  }
}
