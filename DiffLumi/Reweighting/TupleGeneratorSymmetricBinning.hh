#ifndef TupleGeneratorSymmetricBinning_hh
#define TupleGeneratorSymmetricBinning_hh 1

#include "TupleGenerator.hh"

/** \class TupleGeneratorSymmetricBinning
    \brief Tuple for Intitial electron fit w/ sym binning

    Same as TupleGenerator class, but with symmetric binning
 */
class TupleGeneratorSymmetricBinning : public TupleGenerator
{
public:
  /** Contructor */
  TupleGeneratorSymmetricBinning(TString filename, Long64_t nentries = -1):
    TupleGenerator(filename, nentries){
    Init();
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("TupleGeneratorSymmetricBinning"));
  }
  ~TupleGeneratorSymmetricBinning()
  {  }

  void CreateEquiProbability(TString fname = "" );

};

#endif //TupleGeneratorSymmetricBinning_hh
