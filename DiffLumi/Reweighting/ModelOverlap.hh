#ifndef ModelOverlap_hh
#define ModelOverlap_hh 1

#include "AbstractModel.hh"

/** \class ModelOverlap
    \brief Class for the overlapping model
 */
class ModelOverlap  : public AbstractModel
{     

public:
  /** Constructor */
  ModelOverlap(const double* parameters): AbstractModel(parameters)
  {
    m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("ModelOverlap"));
    static_cast<BetaFunction*>( m_BeamstrahlungArm1 )->SetBetaLimit(0.9999);
    static_cast<BetaFunction*>( m_BeamstrahlungArm2 )->SetBetaLimit(0.9999);
    //Body kept at 0.995
    static_cast<BetaFunction*>( m_BeamstrahlungBody1 )->SetBetaLimit(0.995);
    static_cast<BetaFunction*>( m_BeamstrahlungBody2 )->SetBetaLimit(0.995);
  }//ctor

  ~ModelOverlap()
  {  }

  double CalculateProbability(double eb1, double eb2, double deb1, double deb2, double) const;
  AbstractModel::Parts GetRandomVariates(double& x1, double& x2, double& dx1, double& dx2, 
					 double& dx1Gauss, double& dx2Gauss, double& RelativeProbability, 
					 double BeamSpreadSmearing) const;

  std::string GetName() const { return "Overlap"; }
  inline int GetNParameters(){return 35;}

private:

};

#endif // ModelOverlap_hh
