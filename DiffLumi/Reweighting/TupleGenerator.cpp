#include "TupleGenerator.hh"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
void TupleGenerator::Init()//called from the constructor
{
//m_file->GetObject("MyTree", m_tree);
//if (NULL == m_tree){
//  LOG4CPLUS_FATAL(m_log, "TupleGenerator: MyTree is not in "
//		    << m_filename);
//  exit(1);
//}
  m_tree = new TChain("MyTree","MyTree");
  m_tree->Add(m_filename);
  if (!m_tree->Add(m_filename)){
    LOG4CPLUS_FATAL(m_log, "MyTree is not in " << m_filename);
    exit(1);
  }
  m_tree->SetBranchAddress("eb1", &m_eb1);
  m_tree->SetBranchAddress("eb2", &m_eb2);
  m_tree->SetBranchAddress("deb1", &m_deb1);
  m_tree->SetBranchAddress("deb2", &m_deb2);
  m_tree->SetBranchAddress("E1", &m_E1);
  m_tree->SetBranchAddress("E2", &m_E2);
  m_tree->SetBranchAddress("RelativeProbability", &m_relativeProbability);

  if( m_entries < 0 || m_entries > m_tree->GetEntries()) {
    m_entries =  m_tree->GetEntries();
  }
  return ;
}

long TupleGenerator::FillVectors(std::vector<double>*data, std::vector<Event> *events, std::vector<double>*weights) const {
  LOG4CPLUS_DEBUG(m_log, "FillVectors");
  long acceptedEvents(0), skippedEvents(0);
  long treeEntries(m_tree->GetEntries());
  for ( int i = 0; (i < m_entries + skippedEvents) && (i < treeEntries); ++i) {
    m_tree->GetEntry(i);
    //double eb1r = m_eb1 + m_deb1;
    //double eb2r = m_eb2 + m_deb2;
    if(m_E1 < 0 || m_E2 < 0) {
      ++skippedEvents; //we need to add one to still process the right number of events...
      continue;
    }
    if (m_cutclass->cut(m_E1,m_E2)){
      ++skippedEvents; //we need to add one to still process the right number of events...
      continue;
    }

    int bin = m_storedBins.GetBinNumber(m_E1,m_E2);

    LOG4CPLUS_TRACE(m_log, "Found bin "<<bin);
    data->at(bin) +=1.;
    ++acceptedEvents;
    if (events){
      events->push_back(Event(m_eb1, m_eb2, m_deb1, m_deb2, 1./m_relativeProbability, bin, m_E1, m_E2));
      weights->at(bin) = 0.;
    }
  }
  return acceptedEvents;
}

void TupleGenerator::CreateEquiProbability(TString filename)
{
  LOG4CPLUS_DEBUG(m_log, "CreateEquiProbability");
  double energyMax = 1.05;
  TH1D h_E1("h_E1", "h_E1", m_entries, 0, energyMax);

  {
    int i =0, evts = 0;
    do {
      m_tree->GetEntry(i);
      i++;
      if (i > m_tree->GetEntries()){
	LOG4CPLUS_FATAL(m_log, "Skipped too many events, abort!");
	exit(1);
      }
      if(m_E1 < 0 || m_E2 < 0) continue;
      if(m_cutclass->cut(m_E1,m_E2)) continue;
      evts++;
      h_E1.Fill( m_E1 );
    }while (evts<m_entries);
  }


  TH1D h_int_E1("h_int_E1","h_int_E1", m_entries, 0, energyMax);
  double inte_E1=0;
  for (int i = 1; i <= m_entries; i++) {
    inte_E1 += h_E1.Integral(i,i);
    h_int_E1.SetBinContent(i,inte_E1);
  }

  double max_E1 = h_int_E1.GetMaximum();
  double slice_E1 = max_E1*1./m_nbinx;
  std::vector<double> binsx(m_nbinx+1,0.0);
  int multi = 0;
  binsx[0]=0.;

  for (int i =1; i<=h_int_E1.GetNbinsX();i++) {
    if (h_int_E1.GetBinContent(i) > double(multi+1)*slice_E1){
      multi++;
      binsx[multi] = h_int_E1.GetBinLowEdge(i);
    }
  }
  binsx[m_nbinx]=energyMax;
  const int nLargeBinsY = 1000000;
  std::vector<double> binsyt(nLargeBinsY+1, 0.0);
  binsyt[0]=0.;

  for (int i = 1;i<nLargeBinsY;i++){
    binsyt[i]=binsyt[i-1]+(energyMax)/double(nLargeBinsY);
  }
  binsyt[nLargeBinsY]=energyMax;

  TH2D h2d("h2d","h2d",m_nbinx,&binsx[0],nLargeBinsY,&binsyt[0]);
  {
    int i =0, evts = 0;
    do{
      m_tree->GetEntry(i);
      i++;
      if(m_E1 < 0 || m_E2 < 0) continue;
      if(m_cutclass->cut(m_E1,m_E2)) continue;
      evts++;
      h2d.Fill(m_E1,m_E2);
    }while(evts < m_entries);
  }

  std::vector< std::vector<double> > vectbiny(m_nbinx+1);
  for (int i = 1; i <= m_nbinx; i++){
    TH1D*htemp = NULL;
    htemp = h2d.ProjectionY("h_temp",i,i);
    TH1D h_int_E2(Form("h_int_E2_%i", i), "h_int_E2", nLargeBinsY, 0, energyMax);
    double inte_E2 = 0;
    for (int j = 1; j <= nLargeBinsY; j++) {
      inte_E2 += htemp->Integral(j,j);
      h_int_E2.SetBinContent(j, inte_E2);
    }
    //h_int_e2.Write();
    double max_E2 = h_int_E2.GetMaximum();
    const double slice_E2 = max_E2*1./m_nbiny;
    std::vector<double> binsytmp(m_nbiny+1,0.0);
    multi = 0;
    binsytmp[0]=0.;

    for (int j = 1; j <= h_int_E2.GetNbinsX(); j++) {
      if (h_int_E2.GetBinContent(j) > double(multi+1)*slice_E2) {
	multi++;
	binsytmp[multi] = h_int_E2.GetBinLowEdge(j);
      }
    }

    binsytmp[m_nbiny]=energyMax;
    vectbiny[i]=binsytmp;//don't delete binsytmp

    delete htemp;
  }
  for (int i = 1; i<=m_nbinx; ++i) {
    std::vector<double> const& binsy = vectbiny.at(i);
    for (int j = 1; j<=m_nbiny; ++j) {
      m_storedBins.SetBinBoundaries(i-1,j-1,binsx[i],binsy[j]);
      LOG4CPLUS_TRACE(m_log, "bin upper range : "<< binsx[i]<<"  "<< binsy[j]);
    }
  }

  if (filename.Length()) {
    LOG4CPLUS_DEBUG(m_log,"Saving binning to "<<filename.Data());
    m_storedBins.SaveToFile(filename);
  }

  return ;
}
void TupleGenerator::FillLumiHistogram(TString filename){
  TFile*file = TFile::Open(filename,"UPDATE");
  TH1D lumi("lumi_reference","Lumi",10000 ,0.,1.05);
  long skippedEvents(0);
  long treeEntries(m_tree->GetEntries());
  for ( int i = 0; (i < m_entries + skippedEvents) && (i < treeEntries); ++i) {
    m_tree->GetEntry(i);
    //double eb1r = m_eb1 + m_deb1;
    //double eb2r = m_eb2 + m_deb2;
    if(m_E1 < 0 || m_E2 < 0) {
      ++skippedEvents; //we need to add one to still process the right number of events...
      continue;
    }
    if (m_cutclass->cut(m_E1,m_E2)){
      ++skippedEvents; //we need to add one to still process the right number of events...
      continue;
    }
    lumi.Fill(sqrt(m_E1*m_E2));
  }
  lumi.Write();
  file->Close();
  delete file;
  return;
}
