// $Id: $
// Include files 



// local
#include "LumiToRoot.hh"

#include <TMath.h>
#include <iostream>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : LumiToRoot
//
// 2011-10-05 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LumiToRoot::LumiToRoot( std::string treename, Double_t NominalEnergy ) {
  m_rootfile=NULL;
  m_eb1 = m_eb2 = m_x = m_y = m_z = m_tc = m_px1e1 = m_py1e1 = m_pz1e1 = 0.0;
  m_px2e2 = m_py2e2 = m_pz2e2 = m_px1 = m_py1 = m_pz1 = m_px2 = m_py2 = m_pz2 = 0.0;
  m_deb1 = m_deb2 = m_deb1_g = m_deb2_g = m_relproba = 0.0;
  m_label = 0;
  m_tree = new TTree(treename.c_str(),treename.c_str());
  m_tree->Branch("E1",&m_eb1,"E1/D");
  m_tree->Branch("E2",&m_eb2,"E2/D");
  m_tree->Branch("eb1",&m_eb1,"eb1/D");
  m_tree->Branch("eb2",&m_eb2,"eb2/D");
  m_tree->Branch("deb1",&m_deb1,"deb1/D");
  m_tree->Branch("deb2",&m_deb2,"deb2/D");
  m_tree->Branch("deb1_g",&m_deb1_g,"deb1_g/D");
  m_tree->Branch("deb2_g",&m_deb2_g,"deb2_g/D");
  m_tree->Branch("RelativeProbability",&m_relproba,"RelativeProbability/D");

  m_nominalEnergy = NominalEnergy;

  //   m_tree->Branch("x",&m_x,"x/D");
  //   m_tree->Branch("y",&m_y,"y/D");
  //   m_tree->Branch("z",&m_z,"z/D");
  //   m_tree->Branch("tc",&m_tc,"tc/D");
  //   m_tree->Branch("px1e1",&m_px1e1,"px1e1/D");
  //   m_tree->Branch("py1e1",&m_py1e1,"py1e1/D");
  //   m_tree->Branch("pz1e1",&m_pz1e1,"pz1e1/D");
  //   m_tree->Branch("px2e2",&m_px1e1,"px2e2/D");
  //   m_tree->Branch("py2e2",&m_py1e1,"py2e2/D");
  //   m_tree->Branch("pz2e2",&m_pz1e1,"pz2e2/D");
  //   m_tree->Branch("px1",&m_px1,"px1/D");
  //   m_tree->Branch("py1",&m_py1,"py1/D");
  //   m_tree->Branch("pz1",&m_pz1,"pz1/D");
  //   m_tree->Branch("px2",&m_px2,"px2/D");
  //   m_tree->Branch("py2",&m_px2,"py2/D");
  //   m_tree->Branch("pz2",&m_px2,"pz2/D");
  //   m_tree->Branch("label",&m_label,"label/I");
  
  h_spectrum = new TH1D("lumi_spectrum","lumi spectrum",1600,0,3100);
  h_spectrum_2d = new TH2D("lumi_spectrum_2d","lumi spectrum",1530,-0.5,1529.5,1530,-0.5,1529.5);
  
}
//=============================================================================
// Destructor
//=============================================================================
LumiToRoot::~LumiToRoot() {
  delete m_tree;
} 

//=============================================================================

//=========================================================================
//  
//=========================================================================
void LumiToRoot::readFile (std::string fname ) {
  m_file.open((char*)fname.c_str() , std::ifstream::in);
  int count = 0;
  int nLines = 0;
  
  std::string buffer;
  while ( std::getline( m_file, buffer, '\n')  ) {
    TString inputstring(buffer);
    TObjArray	*inputbits	= inputstring.Tokenize(" ");
    if(inputbits->GetEntries() == 10) {
      m_eb1   = (((TObjString*)inputbits->At( 0))->String()).Atof();
      m_eb2   = (((TObjString*)inputbits->At( 1))->String()).Atof();
      m_x     = (((TObjString*)inputbits->At( 2))->String()).Atof();
      m_y     = (((TObjString*)inputbits->At( 3))->String()).Atof();
      m_z     = (((TObjString*)inputbits->At( 4))->String()).Atof();
      m_tc    = (((TObjString*)inputbits->At( 5))->String()).Atof();
      m_px1e1 = (((TObjString*)inputbits->At( 6))->String()).Atof();
      m_py1e1 = (((TObjString*)inputbits->At( 7))->String()).Atof();
      m_px2e2 = (((TObjString*)inputbits->At( 8))->String()).Atof();
      m_py2e2 = (((TObjString*)inputbits->At( 9))->String()).Atof();
    } else if ( inputbits->GetEntries() == 17) {
      m_eb1   = (((TObjString*)inputbits->At( 0))->String()).Atof();
      m_eb2   = (((TObjString*)inputbits->At( 1))->String()).Atof();
      m_x     = (((TObjString*)inputbits->At( 2))->String()).Atof();
      m_y     = (((TObjString*)inputbits->At( 3))->String()).Atof();
      m_z     = (((TObjString*)inputbits->At( 4))->String()).Atof();
      m_tc    = (((TObjString*)inputbits->At( 5))->String()).Atof();
      m_px1e1 = (((TObjString*)inputbits->At( 6))->String()).Atof();
      m_py1e1 = (((TObjString*)inputbits->At( 7))->String()).Atof();
      m_px2e2 = (((TObjString*)inputbits->At( 8))->String()).Atof();
      m_py2e2 = (((TObjString*)inputbits->At( 9))->String()).Atof();
      m_px1   = (((TObjString*)inputbits->At(10))->String()).Atof();
      m_py1   = (((TObjString*)inputbits->At(11))->String()).Atof();
      m_pz1   = (((TObjString*)inputbits->At(12))->String()).Atof();
      m_px2   = (((TObjString*)inputbits->At(13))->String()).Atof();
      m_py2   = (((TObjString*)inputbits->At(14))->String()).Atof();
      m_pz2   = (((TObjString*)inputbits->At(15))->String()).Atof();
      m_label = (((TObjString*)inputbits->At(16))->String()).Atof();
    } else if ( inputbits->GetEntries() == 0 ) {
      //reached end of file
      delete inputbits;
      break;
    } else {
      std::cerr << "Found " << inputbits->GetEntries() << " columns"  << std::endl;
      std::cerr << "Wrong number of columns, aborting!"  << std::endl;
      delete inputbits;
      exit (1);
    }
    nLines++;
    delete inputbits;

    //m_file>>m_eb1>>m_eb2>>m_x>>m_y>>m_z>>m_tc>>m_px1e1>>m_py1e1>>m_px2e2>>m_py2e2;
    if (m_eb1<0 || m_eb2<0) {
      count++;
      continue;
    }
    //if (count<300000){
    h_spectrum->Fill(TMath::Sqrt(4*m_eb1*m_eb2));
    h_spectrum_2d->Fill(m_eb1,m_eb2);
    //}
    //count++;
    m_eb1 = m_eb1/m_nominalEnergy;
    m_eb2 = m_eb2/m_nominalEnergy;
  
    m_deb1=0.;
    m_deb2=0.;
    m_deb1_g=0.;
    m_deb2_g=0.;
    m_relproba = 1.;
    m_tree->Fill();
  }// for all lines in the file
  m_file.close();
  std::cout << "Threw away " << std::setw(10)<< count << " events"<<std::endl;
  std::cout << "Have       " << std::setw(10)<< m_tree->GetEntries() << " events in the tree"  << std::endl;
  std::cout << "Had        " << std::setw(10)<< nLines << " lines in the file"  << std::endl;
}


//=========================================================================
//  
//=========================================================================
void LumiToRoot::saveTTree ( std::string fname ) {
  m_rootfile = new TFile( fname.c_str(),"RECREATE");
  m_tree->Write();
  m_rootfile->Close();
  
  TFile fout("GuineaPigSpectrum.root","RECREATE");
  h_spectrum->Write();
  h_spectrum_2d->Write();
  
  fout.Close();
  
}

