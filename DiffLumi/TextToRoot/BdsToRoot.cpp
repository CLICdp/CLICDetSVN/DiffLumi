// $Id: $
// Include files 



// local
#include "BdsToRoot.hh"

//-----------------------------------------------------------------------------
// Implementation file for class : BdsToRoot
//
// 2011-10-05 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BdsToRoot::BdsToRoot( std::string treename ) {
  m_rootfile = NULL;
  m_tree = new TTree(treename.c_str(),treename.c_str());
  m_tree->Branch("E",&m_eb,  "E/D");
  m_tree->Branch("x",&m_x,   "x/D");
  m_tree->Branch("y",&m_y,   "y/D");
  m_tree->Branch("z",&m_z,   "z/D");
  m_tree->Branch("xp",&m_xp,"xp/D");
  m_tree->Branch("yp",&m_yp,"yp/D");

  
}
//=============================================================================
// Destructor
//=============================================================================
BdsToRoot::~BdsToRoot() {
  delete m_tree;
} 

//=============================================================================

//=========================================================================
//  
//=========================================================================
void BdsToRoot::readFile (std::string fname ) {
  m_file.open((char*)fname.c_str() , std::ifstream::in);
  while ( m_file>>m_eb>>m_x>>m_y>>m_z>>m_xp>>m_yp ) {
    m_tree->Fill();
  }
  m_file.close();
}


//=========================================================================
//  
//=========================================================================
void BdsToRoot::saveTTree (std::string fname ) {
  m_rootfile = new TFile(fname.c_str(),"RECREATE");
  m_tree->Write();
  m_rootfile->Close();
  
}

int main(int argc, char* argv[] )
{
  BdsToRoot* t = new BdsToRoot("MyTree");
  if (argc < 3) {
    std::cerr << "Not enough parameters"  << std::endl;
    std::cerr << "BdsToRoot outputfile.root inputfile1 [inputfile2...]"  << std::endl;
    return 1;
  }
  for (int i = 2; i < argc ;++i) {
    std::cout << "Opening file " << argv[i]  << std::endl;
    t->readFile(argv[i]);
  }
  t->saveTTree(argv[1]);
  
  return 0;
}
