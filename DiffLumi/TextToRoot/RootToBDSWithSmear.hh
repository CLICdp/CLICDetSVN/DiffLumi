#ifndef RootToBDSWithSmear_hh
#define RootToBDSWithSmear_hh 1


class TFile;
class TTree;

#include <string>


class RootToBDSWithSmear{
public:
  RootToBDSWithSmear( std::string const& inFilename, std::string const& outFilename, double, double, bool );
  ~RootToBDSWithSmear();

  void writeOut();

  double getNominamEnergy() const { return m_nominalEnergy; }
  void setNominalEnergy( double nomE ) { m_nominalEnergy = nomE; }

private:
  TFile *m_rootFile;
  TTree *m_tree;
  std::string m_outFilename;

  double m_nominalEnergy;
  double m_smearE;
  double m_smearZ;
  bool m_writeFile;
  //Tree Variables
  double m_eb, m_x, m_y, m_z, m_xp, m_yp;




};

#endif // RootToBDSWithSmear_hh
