// $Id: $
// Include files 
// local
#include "RootToBDSWithSmear.hh"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TRandom3.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>


#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <sstream>
#include <fstream>


RootToBDSWithSmear::RootToBDSWithSmear( std::string const& inFilename, std::string const& outFilename, 
					double energySmear,  
					double zSmear,
					bool writeFile):
  m_rootFile(TFile::Open(inFilename.c_str())),
  m_tree(NULL),
  m_outFilename(outFilename),
  m_nominalEnergy(175),
  m_smearE(energySmear),
  m_smearZ(zSmear),
  m_writeFile(writeFile),
  m_eb(0.0),
  m_x(0.0),
  m_y(0.0),
  m_z(0.0),
  m_xp(0.0),
  m_yp(0.0) {
  m_rootFile->GetObject("MyTree",m_tree);
  m_tree->SetBranchAddress("E",&m_eb);
  m_tree->SetBranchAddress("x",&m_x);
  m_tree->SetBranchAddress("y",&m_y);
  m_tree->SetBranchAddress("z",&m_z);
  m_tree->SetBranchAddress("xp",&m_xp);
  m_tree->SetBranchAddress("yp",&m_yp);
  
}// c'tor

void RootToBDSWithSmear::writeOut() {



  const int entriesPerFile(300000);
  int fileCounter(0);
  const int numberOfEntries (m_tree->GetEntries());
  TRandom3 rad(0);
  const double minE(173.5), maxE(175.0);
  const double minZ(95.0), maxZ(105.0);
  TH1D energyDistribution1("hEnergy1","Input; Energy [GeV]; N [a.u.]", 200, minE, maxE);
  TH1D energyDistribution2("hEnergy2",Form("%1.2f%s; Energy [GeV]; N [a.u.]",m_smearE,"%"), 200, minE, maxE);

  double zLimit(300);
  TH1D zDistribution1("hZ1","Input; z [#mum]; N [a.u.]", 200, -zLimit, zLimit);
  TH1D zDistribution2("hZ2",Form("%1.1f #mum; z [#mum]; N [a.u.]",m_smearZ), 200, -zLimit, zLimit);

  TH2D hEvsZ1("hEvsZ1","E vs Z; z [#mum]; #DeltaE/E_{nom}", 3000, -350, 350, 1000, -0.010, 0.010);
  TH2D hEvsZ2("hEvsZ2","E vs Z; z [#mum]; #DeltaE/E_{nom}", 3000, -350, 350, 1000, -0.010, 0.010);

  std::ofstream* outFile = NULL;

  for (int i = 0; i < numberOfEntries;++i) {
    //Open textFile every entriesPerFile events
    if( m_writeFile ) {
      if ( i % entriesPerFile == 0) {
	std::cout << "Opening new File: " << fileCounter  << std::endl;
	if( outFile ) {
	  outFile->close();
	  delete outFile;
	}
	std::stringstream currentFilename; currentFilename << m_outFilename << "_" << fileCounter << ".txt";
	outFile = new std::ofstream( currentFilename.str().c_str(), std::ios::trunc );
	outFile->precision(9);
	//outFile->setf( std::ios::scientific );
	fileCounter++;
      }
    }
    m_tree->GetEntry(i);

    hEvsZ1.Fill(m_z, m_eb/m_nominalEnergy-1);

    if( minZ < m_z && m_z < maxZ ) energyDistribution1.Fill(m_eb);
    m_eb += rad.Gaus(0.0, m_smearE);
    if( minZ < m_z && m_z < maxZ ) energyDistribution2.Fill(m_eb);


    zDistribution1.Fill(m_z);
    m_z += rad.Gaus(0.0, m_smearZ);
    zDistribution2.Fill(m_z);

    hEvsZ2.Fill(m_z, m_eb/m_nominalEnergy-1);


    if ( m_writeFile ) {
      *outFile <<  m_eb
	       << " " << m_x
	       << " " << m_y
	       << " " << m_z
	       << " " << m_xp
	       << " " << m_yp
	       << std::endl;
    }    

  }//for all events

  {
    std::cout << "********************************************************************************"  << std::endl;
    std::cout << " Fitting Energy Distribution"  << std::endl;
    energyDistribution1.Fit("gaus");
    energyDistribution2.Fit("gaus");
    TCanvas canv("c", "Energy Distributions");
    energyDistribution1.Draw();
    energyDistribution2.Draw("same");
    energyDistribution2.SetLineColor(kGreen+2);
#ifdef USE_RootStyle
    RootStyle::AutoSetYRange(canv, 1.3);
    TLegend* leg = RootStyle::BuildLegend(canv, 0.6, 0.9, 0.9, 0.9-2*0.0675);
    leg->SetTextSize(0.06);
#endif    
    canv.SaveAs("smearedBDS.eps");
  }


  {
    std::cout << "********************************************************************************"  << std::endl;
    std::cout << " Fitting Z Distribution"  << std::endl;
    zDistribution1.Fit("gaus");
    zDistribution2.Fit("gaus");
    TCanvas canv("c", "Z Distributions");
    zDistribution1.Draw();
    zDistribution2.Draw("same");
    zDistribution2.SetLineColor(kGreen+2);
#ifdef USE_RootStyle
    RootStyle::AutoSetYRange(canv, 1.3);
    TLegend* leg = RootStyle::BuildLegend(canv, 0.6, 0.9, 0.9, 0.9-2*0.0675);
    leg->SetTextSize(0.06);
#endif    
    canv.SaveAs("smearedBDS_Z.eps");
  }

  {

    TCanvas canvBefore("c3","Before");
    canvBefore.SetRightMargin(0.16);
    hEvsZ1.Draw("colz");
    hEvsZ1.GetYaxis()->SetTitleOffset(1.1);
    canvBefore.SaveAs("EvsZBefore.eps");

    TCanvas canvAfter("c3","After");
    canvAfter.SetRightMargin(0.16);
    hEvsZ2.Draw("colz");
    hEvsZ2.GetYaxis()->SetTitleOffset(1.1);
    canvAfter.SaveAs("EvsZAfter.eps");


    TCanvas canv2("c2","E vs Z", 1600, 700);
    canv2.Divide(2,1);
    canv2.cd(1);
    hEvsZ1.Draw("colz");

    canv2.cd(2);
    hEvsZ2.Draw("colz");

    canv2.SaveAs("EvsZ.eps");

  }


}


RootToBDSWithSmear::~RootToBDSWithSmear() {
  m_rootFile->Close();
} //d'tor

int main(int argc, char* argv[] )
{

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  if (argc != 7) {
    std::cerr << "Wrong Number of Parameters"  << std::endl;
    std::cerr << "RootToBDSWithSmear outputfile.txt inputFile.root <NominalEnergy> <SmearE> <SmearZ> <(WriteFile)Yes/No>"  
	      << std::endl;
    return 1;
  }

  std::string outFilename(argv[1]);
  std::string inFilename(argv[2]);
  double nomE       ( std::atof(argv[3]) );
  double energySmear( std::atof(argv[4]) );
  double zSmear     ( std::atof(argv[5]) );
  std::string inputWrite(argv[6]);
  bool writeFile = false;
  if ( inputWrite.compare("Yes") == 0 ) {
    writeFile = true;
  } else if ( inputWrite.compare("No") == 0 ) {
    writeFile = false;
  } else {
    std::cerr << "Unknown Value for last parameter, just Yes or No!" << std::endl;
    return 1;
  }

  RootToBDSWithSmear obj(inFilename, outFilename, energySmear, zSmear, writeFile);
  obj.setNominalEnergy( nomE );
  obj.writeOut();

  return 0;
}
