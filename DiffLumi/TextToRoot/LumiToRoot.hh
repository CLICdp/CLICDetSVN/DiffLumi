// $Id: $
#ifndef LUMI_STUDIES_LUMITOROOT_H 
#define LUMI_STUDIES_LUMITOROOT_H 1

// Include files

#include "TFile.h"
#include "TTree.h"
#include "Riostream.h"
#include "TH1D.h"
#include "TH2D.h"
/** @class LumiToRoot LumiToRoot.h Lumi_studies/LumiToRoot.h
 *  
 *
 *  @author Stephane Poss
 *  @date   2011-10-05
 */
class LumiToRoot {
public: 
  /// Standard constructor
  LumiToRoot(std::string, Double_t NominalEnergy=1500.0 ); 

  virtual ~LumiToRoot( ); ///< Destructor

  void readFile(std::string);
  void copyToTree();
  void saveTTree(std::string);
protected:
  std::ifstream m_file;
  
private:
  TFile*m_rootfile;
  TTree*m_tree;
  Double_t m_eb1,m_eb2,m_x,m_y,m_z,m_tc,m_px1e1,m_py1e1,m_pz1e1;
  Double_t m_px2e2,m_py2e2,m_pz2e2,m_px1,m_py1,m_pz1,m_px2,m_py2,m_pz2;
  Double_t m_deb1,m_deb2,m_deb1_g,m_deb2_g,m_relproba;
  Int_t m_label;
  Double_t m_nominalEnergy; 
  TH1D*h_spectrum;
  TH2D*h_spectrum_2d;
 
};
#endif // LUMI_STUDIES_LUMITOROOT_H
