#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TTree.h>
#include <TFile.h>

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>

int main(int argc, char* argv[] ) {

  if (argc < 3) {
    std::cerr << "Not enough parameters"  << std::endl;
    std::cerr << "TreeToText <inputfile.root> <outputfile.txt> [MaxNumberOfEvents]"  << std::endl;
    return 1;
  }

  std::string inputFileName(argv[1]);
  std::string outputFileName(argv[2]);

    

  TFile *file = TFile::Open(inputFileName.c_str());
  if(not file) {
    std::cerr << "File " << inputFileName << " not found" << std::endl;
    return 1;
  }

  TTree *tree;
  file->GetObject("MyTree",tree);

  Double_t m_eb1, m_eb2, m_deb1, m_deb2, m_deb1_g, m_deb2_g, m_relproba;

  tree->SetBranchAddress("eb1",&m_eb1);
  tree->SetBranchAddress("eb2",&m_eb2);
  tree->SetBranchAddress("deb1",&m_deb1);
  tree->SetBranchAddress("deb2",&m_deb2);
  tree->SetBranchAddress("deb1_g",&m_deb1_g);
  tree->SetBranchAddress("deb2_g",&m_deb2_g);
  tree->SetBranchAddress("RelativeProbability",&m_relproba);




  long long maxEntries=-1;
  if(argc > 3) {
    maxEntries = std::atoi(argv[3]);
  } else {
    maxEntries = tree->GetEntries();
  }
  std::cout << "Writing " << maxEntries << " entries from tree"  << std::endl;

  //Opening the output textfile
  std::ofstream outputFile(outputFileName.c_str());

  for (long long i = 0; i < maxEntries ;++i) {
    tree->GetEntry(i);
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, maxEntries);
#endif

    outputFile.setf( std::ios::scientific );
    const int precision = 8;
    outputFile.precision(8);

    outputFile << std::setw(precision+8) << m_eb1 
	       << std::setw(precision+8) << m_eb2
	       << std::setw(precision+8) << m_deb1
	       << std::setw(precision+8) << m_deb2
	       << std::setw(precision+8) << m_deb1_g
	       << std::setw(precision+8) << m_deb2_g
	       << std::setw(precision+8) << m_relproba 
	       << "\n";
  }

  std::cout << std::endl;

  outputFile.close();

  return 0;
}
  


