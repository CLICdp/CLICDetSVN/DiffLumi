ADD_LIBRARY( TextToRoot SHARED LumiToRoot.cpp BdsToRoot.cpp)

ADD_EXECUTABLE ( ReadLumi ReadLumi.cpp )
ADD_EXECUTABLE ( BdsToRoot BdsToRoot.cpp )

ADD_EXECUTABLE ( TreeToText TreeToText.cpp )
ADD_EXECUTABLE ( TreeToHEPEvt TreeToHEPEvt.cpp )

ADD_EXECUTABLE ( RootToBDSWithSmear RootToBDSWithSmear.cpp )

TARGET_LINK_LIBRARIES ( ReadLumi TextToRoot )
TARGET_LINK_LIBRARIES ( RootToBDSWithSmear RootStyle )

INSTALL(TARGETS TreeToText TreeToHEPEvt
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)
