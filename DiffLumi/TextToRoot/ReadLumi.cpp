// $Id: $
#include "LumiToRoot.hh"

#include <cstdlib>

int main(int argc, char* argv[] )
{
  

  if (argc < 4) {
    std::cerr << "Not enough parameters"  << std::endl;
    std::cerr << "ReadLumi outputfile.root <BeamEnergy> inputfile1 [inputfile2...]"  << std::endl;
    return 1;
  }
  double nominalEnergy(1500.0);
  nominalEnergy = std::atof(argv[2]);
  std::cout << "Nominal Beam Energy: " << nominalEnergy  << std::endl;

  LumiToRoot* t = new LumiToRoot("MyTree", nominalEnergy);


  for (int i = 3; i < argc ;++i) {
    std::cout << "Opening file " << argv[i]  << std::endl;
    t->readFile(argv[i]);
  }
  t->saveTTree(argv[1]);
  
  return 0;
}
