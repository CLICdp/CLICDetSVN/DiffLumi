#include <TFile.h>
#include <TTree.h>

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <istream>
#include <map>
#include <sstream>
#include <string>
#include <ios>
#include <iomanip>


const float Pi = acos(-1);
const float DegToRad = Pi/180.;


class BHWideTree{

private:
  TFile *m_file;
  TTree *m_tree;

public:
  BHWideTree(std::string const& filename);
  Long64_t GetEntry(Long64_t j);
  Long64_t GetEntries();

  Double_t eb1;
  Double_t eb2;
  Double_t deb1;
  Double_t deb2;
  Double_t deb1_g;
  Double_t deb2_g;
  Double_t E1;
  Double_t E2;
  Double_t px1;
  Double_t py1;
  Double_t pz1;
  Double_t e1;
  Double_t px2;
  Double_t py2;
  Double_t pz2;
  Double_t e2;
  Int_t    nphotons;
  Double_t photdata[30][4];   //[nphotons]
  
};


int main(int argc, char* argv[]) {

  if(argc < 3) {
    std::cout << "Missing Filenames! To Run conversion use:\nTreeToHEPEvt <BHWide_file.root> <OutPutBase> <EventsPerFile>" << std::endl;
    exit(1);
  }

  std::string infile, outfile;
  infile  = argv[1];
  outfile = argv[2];
  int EventsPerFile = std::atoi(argv[3]);

  std::map<int, double> Mass;
  Mass[11] = 0.000510998910;
  Mass[22] = 0.0;

  // Open Root File and read the Tree
  BHWideTree bht(infile);

  std::ifstream beamfile;
  std::string line;

  // output-file precision

  int	count = 0, prec = 8;

  int fileCounter(0);
  std::ofstream *fout (NULL);

  for (int i = 0; i < bht.GetEntries() ;++i) {
    bht.GetEntry(i);
  
    if ( i%EventsPerFile == 0 ) {
      if(fout != NULL) {
	fout->close();
      }
      fileCounter++;
      std::stringstream fileName;
      fileName << outfile << fileCounter << ".HEPEvt";
      fout = new std::ofstream( fileName.str().c_str(), std::ios::trunc );
      fout->precision(prec);
      fout->setf( std::ios::scientific );
    }

    int particlesInEvent(bht.nphotons+2);
    (*fout) << std::setw(3) << particlesInEvent << std::endl;                           // 1. line = # of particles

    (*fout) << std::setw(5) << 1            // 1.col = state of particle (1=initial)
	    << std::setw(5) << 11            // 2.col = particle id
	    << std::setw(5) << 0            // 3.col = from particle
	    << std::setw(5) << 0            // 4.col = til particle
	    << std::setw(18) << bht.px1     // px
	    << std::setw(18) << bht.py1     // py
	    << std::setw(18) << bht.pz1     // pz
	    << std::setw(18) << Mass[abs(11)] << std::endl;   // mass

    (*fout) << std::setw(5) << 1            // 1.col = state of particle (1=initial)
	    << std::setw(5) << -11            // 2.col = particle id
	    << std::setw(5) << 0            // 3.col = from particle
	    << std::setw(5) << 0            // 4.col = til particle
	    << std::setw(18) << bht.px2     // px
	    << std::setw(18) << bht.py2     // py
	    << std::setw(18) << bht.pz2     // pz
	    << std::setw(18) << Mass[abs(11)] << std::endl;   // mass

    for (int j = 0; j < bht.nphotons ;++j) {
      (*fout) << std::setw(5) << 1            // 1.col = state of particle (1=initial)
	      << std::setw(5) << 22            // 2.col = particle id
	      << std::setw(5) << 0            // 3.col = from particle
	      << std::setw(5) << 0            // 4.col = til particle
	      << std::setw(18) << (*bht.photdata)[j+0]     // px
	      << std::setw(18) << (*bht.photdata)[j+1]     // py
	      << std::setw(18) << (*bht.photdata)[j+2]     // pz
	      << std::setw(18) << Mass[abs(22)] << std::endl;   // mass
    }//for the photons
    

    
  }//For all events


  std::cout << "Wrote " << count << " Events." << std::endl;
  return 0;
}// main




BHWideTree::BHWideTree(std::string const& filename):m_file(TFile::Open(filename.c_str())),m_tree(NULL) {

  m_file->GetObject("MyTree",m_tree);
    
  m_tree->SetBranchAddress("eb1", &eb1);
  m_tree->SetBranchAddress("eb2", &eb2);
  m_tree->SetBranchAddress("deb1", &deb1);
  m_tree->SetBranchAddress("deb2", &deb2);
  m_tree->SetBranchAddress("deb1_g", &deb1_g);
  m_tree->SetBranchAddress("deb2_g", &deb2_g);
  m_tree->SetBranchAddress("E1", &E1);
  m_tree->SetBranchAddress("E2", &E2);
  m_tree->SetBranchAddress("px1", &px1);
  m_tree->SetBranchAddress("py1", &py1);
  m_tree->SetBranchAddress("pz1", &pz1);
  m_tree->SetBranchAddress("e1", &e1);
  m_tree->SetBranchAddress("px2", &px2);
  m_tree->SetBranchAddress("py2", &py2);
  m_tree->SetBranchAddress("pz2", &pz2);
  m_tree->SetBranchAddress("e2", &e2);
  m_tree->SetBranchAddress("nphotons", &nphotons);
  m_tree->SetBranchAddress("photdata", photdata);

}

Long64_t BHWideTree::GetEntry(Long64_t j) {
  return  m_tree->GetEntry(j);
}

Long64_t BHWideTree::GetEntries() {
  return  m_tree->GetEntries();
}
