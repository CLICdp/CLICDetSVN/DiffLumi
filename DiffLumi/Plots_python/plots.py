from ROOT import TCanvas, TFile, TTree, TGraphErrors, TMultiGraph, TH1D, std, gROOT, TF1, gStyle
from array import array
import os, glob
from christians_tools.Canvas import Canvas

gROOT.Reset()

flist = glob.glob("/afs/cern.ch/work/s/sposs/public/MCMC_*/*.root")
bindict = {}
gParameterNames = ["p_{Peak}",
                   "p_{Arm1}",
                   "p_{Arm2}",
                   "#eta_{Arm1}^{1}",
                   "#eta_{Arm1}^{2}",
                   "#omega_{Peak1}^{1}" ,
                   "#omega_{Peak1}^{2}" ,
                   "\\BSRangeALo",#beamSpreadrange xmin
                   "\\BSRangeAHi",#beamSpreadrange xmax
                   "#eta_{Arm2}^{1}",
                   "#eta_{Arm2}^{2}",
                   "#omega_{Peak2}^{1}" ,##BeamSpread Peak2 a1
                   "#omega_{Peak2}^{2}" ,##BeamSpread Peak2 a2
                   "B_{SRangeBLo}",
                   "B_{SRangeBHi}",
                   "#eta_{Body1}^{1}",
                   "#eta_{Body1}^{2}",
                   "#eta_{Body2}^{1}",
                   "#eta_{Body2}^{2}",
                   "#omega_{Arm1}^{1}",
                   "#omega_{Arm1}^{2}",
                   "B_{SRALArm}",
                   "B_{SRAHArm}",
                   "#omega_{Arm2}^{1}",
                   "#omega_{Arm2}^{2}",
                   "B_{SRBLArm}",
                   "B_{SRBHArm}",
                   "B_{SB1aBody}",
                   "B_{SB1bBody}",
                   "B_{SR1LBody}",
                   "B_{SR1HBody}",
                   "B_{SBBaBody}",
                   "B_{SBBbBody}",
                   "B_{SRBLBody}",
                   "B_{SRBHBody}"]

binsdef = {}
binsdef["10_10"]=10
binsdef["20_20"]=20
binsdef["30_30"]=30
binsdef["40_40"]=40
binsdef["50_50"]=50
binsdef["60_60"]=60
binsdef["70_70"]=70
binsdef["80_80"]=80
binsdef["90_90"]=90
binsdef["100_100"]=100
binsdef["110_110"]=110
binsdef["120_120"]=120
binsdef["130_130"]=130
binsdef["140_140"]=140
binsdef["150_150"]=150

for f in flist:
  bins = f.split("/")[7].split("MCMC_")[1]
  if not bins in bindict:
      bindict[bins] = []
  bindict[bins].append(f)

pullsplot = {}
pulls_b  = {}
pulls_b_e = {}

for i in range(len(gParameterNames)):
    pulls_b[gParameterNames[i]] = {}
    pulls_b_e[gParameterNames[i]] = {}

for bins, flist in bindict.items():
    for i in range(len(gParameterNames)):
        h1 = TH1D("h_%s"%i,"pulls",50,-5,6)
        pullsplot[gParameterNames[i]] = h1
    #print pullsplot
    for f in flist:
        fin = TFile(f)
        if not fin or fin.IsZombie():
            continue
        fr = TTree()
        init = TTree()
        fin.GetObject("FitResult",fr)
        if not fr:
            print "No tree found"
            continue
        fin.GetObject("InitialParameters",init)
        if not init:
            print "No init tree found"
            continue
        n_params = fr.GetEntries();
        fr.GetEntry(0);
        if fr.status!=0 or fr.covQual!=3:
            continue
            
        pdict = {}
        for p in range(70):
            pname = gParameterNames[p / 2]
            if not pname in pdict:
                pdict[pname] = {}
            if p % 2==0:
                pdict[pname]["val"]=fr.Param[p]
            else:
                pdict[pname]["err"]=fr.Param[p]
        initvals = {}
        for j in range(35):
            init.GetEntry(j)
            initvals[gParameterNames[j]] = init.Value
        pulls = {}
        for pname,vals in pdict.items():
            if vals["err"] > 0:
                pulls[pname] = (vals["val"]-initvals[pname])/vals["err"]
                #print pname, pullsplot[pname]
                try:
                    pullsplot[pname].Fill(pulls[pname])
                except:
                    print pullsplot
                    #exit()
            else:
                pulls[pname] = 0.
        #print pulls
        #break
        fin.Close()
    #continue
    for i in range(len(gParameterNames)):
        if not bins in pulls_b:
            pulls_b[gParameterNames[i]][bins] = 0.
            pulls_b_e[gParameterNames[i]][bins] = 0.
        if pullsplot[gParameterNames[i]].GetEntries():
            f1 = TF1("f1","gaus",-5,6)
            pullsplot[gParameterNames[i]].Fit(f1,"R:Q")
            #pullsplot[gParameterNames[i]].Draw()
            #f1.Draw("same")
            pulls_b[gParameterNames[i]][bins] = f1.GetParameter(1)
            #pulls_b_e[gParameterNames[i]][bins] = f1.GetParError(1)
            pulls_b_e[gParameterNames[i]][bins] = f1.GetParameter(2)
        
    
#print pulls_b
#print pulls_b_e
a4scale=3
c = Canvas(4,5,xSize=2970/a4scale, ySize=2100/a4scale)
gStyle.SetLineScalePS(1)
gStyle.SetTitleFontSize(0.08)
parameterIDToTablePosition = [ 25, 0, 1, 2,
                               5, 6, 11, 12,
                               19, 20, 23, 24,
                               3, 4, 9, 10,
                               15, 16, 17, 18]
for i in range(1,20):
    pname = gParameterNames[parameterIDToTablePosition[i]]
    print pname
#for pname in pulls_b.keys():
    array_x = array("f") 
    array_x_e = array("f")
    array_y = array("f")
    array_y_e = array("f")
    bad = False
    for bins in pulls_b[pname].keys():
        array_x.append(binsdef[bins])
        array_x_e.append(0.)
        if pulls_b[pname][bins]==0:
            bad = True
        array_y.append(float(pulls_b[pname][bins]))
        array_y_e.append(float(pulls_b_e[pname][bins]))
    #if bad :
    #    continue
    #print array_x, array_y, array_x_e, array_y_e
    #c.cd(idx)
    pad = c.getPad(i+1)
    pad.SetLeftMargin( pad.GetLeftMargin() +0.05)
    pad.SetRightMargin(pad.GetRightMargin()-0.05)
    pad.SetTopMargin(0.00)
    pad.SetBottomMargin(0.07)
    ge = TGraphErrors(len(array_x), array_x, array_y, array_x_e, array_y_e)
    ge.GetYaxis().SetTitle(pname)
    ge.SetMarkerStyle(2)
    c.addGraph(i+1,ge,"APS")
    panel = c.getPanel(i+1)
    #panel.setTitle(ytitle=pname)
    panel.draw()
    #c.draw()
    panel.getDrawnObject().GetYaxis().SetRangeUser(-1.25,1.25)
    panel.getDrawnObject().GetYaxis().SetTitleSize(0.15)
    panel.getDrawnObject().GetYaxis().SetTitleOffset(0.7)
    panel.getDrawnObject().GetXaxis().SetLabelSize(0.08)
    panel.getDrawnObject().GetYaxis().SetLabelSize(0.08)
    #c.addText(gParameterNames.index(pname)+1,pname,1)
    c.getCanvas().Update()
    #break
c.getCanvas().SaveAs("BinningMCMC.eps")
