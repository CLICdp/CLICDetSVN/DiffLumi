// $Id: $
// Include files 



// local
#include "SmearEnergy.hh"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH2D.h"
#include "TF1.h"
#include "Utilities.h"
#include <tclap/CmdLine.h>

int main(int argc, char *argv[])
{
  std::string inputfile("");
  std::string outputfile("");
  std::string smearingFile("");
  try {  
    TCLAP::CmdLine cmd("Check the defaults.", ' ', "0.0");
    TCLAP::ValueArg<std::string> fileOutArg("","outputfile","File to use as output",false, outputfile, "string", cmd);
    TCLAP::ValueArg<std::string> fileInArg("","inputfile","File to use as input",true, inputfile, "string", cmd);
    TCLAP::ValueArg<std::string> fileWithSmearingInformation(TString(Utility::GetWorkingDirectory()+"/RootFiles/Input_for_smearing_Dtheta_vs_E.root").Data(),"smearingFile","File containing resolution curves",false, smearingFile, "string", cmd);
    cmd.parse( argc, argv );
    inputfile = fileInArg.getValue();
    outputfile = fileOutArg.getValue();
    smearingFile = fileWithSmearingInformation.getValue();
  }
  catch (TCLAP::ArgException &e){  // catch any exceptions 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return 1;
  }
  TFile*f = NULL;
  if (inputfile != "") {
    f = TFile::Open(TString(inputfile));
  }
  else{
    std::cout << "Input file not defined, use with -h"  << std::endl;
    return 1;
  }
  if (outputfile == ""){
    std::cout << "Output file not set, will use SmearingOutput.root"  << std::endl;
    outputfile = "SmearingOutput.root";
  }
  //TFile*f = TFile::Open("/afs/cern.ch/work/s/sposs/public/BHWide_MC_no_beam_spread_body.root");
  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_GP.root"));
  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_MC_Separate_2.root"));
  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_MC_Separate.root"));
  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_MC_Overlap.root"));

  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_GP_BHWeighted_Double_slives50_Random.root"));
  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_MC_BHWeigthed_Separate.root"));
  //TFile*f = TFile::Open(TString(Utility::GetWorkingDirectory()+"/RootFiles/BHWide_MC_BHWeigthed_Overlap.root"));
  TTree*t = (TTree*)f->Get("MyTree");
  
  SmearEnergy se((TChain*)t,
		 TString(outputfile));
  
  se.setSmearingE(0.243,0.0123);
  //se.setSmearingTheta(7e-6);
  //se.setSmearingPhi(5e-5);
  TFile*f_histo = TFile::Open(TString(smearingFile));
  
  se.setSmearingThetaHisto(static_cast<TH2D*>(f_histo->Get("H2DTHTREAT504")));
  se.Loop();
  se.Save();
  
  
  return 0;
  
}
