// $Id: $
// Include files



// local
#include "SmearEnergy.hh"
#include "TMath.h"
#include "Utilities.h"
//-----------------------------------------------------------------------------
// Implementation file for class : SmearEnergy
//
// 2011-11-07 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SmearEnergy::SmearEnergy(TChain*ch, TString fname ):MCTupleBHWide(ch) {
  m_fout = TFile::Open(fname,"RECREATE");
  m_tree = new TTree("MyTree","MyTree");
  m_tree->Branch("eb1",&m_eb1,"eb1/D");
  m_tree->Branch("eb2",&m_eb2,"eb2/D");
  m_tree->Branch("E1",&m_E1,"E1/D");
  m_tree->Branch("E2",&m_E2,"E2/D");
  m_tree->Branch("deb1",&m_deb1,"deb1/D");
  m_tree->Branch("deb2",&m_deb2,"deb2/D");
  m_tree->Branch("deb1_g",&m_deb1_g,"deb1_g/D");
  m_tree->Branch("deb2_g",&m_deb2_g,"deb2_g/D");
  m_tree->Branch("px1",&m_px1,"px1/D");
  m_tree->Branch("py1",&m_py1,"py1/D");
  m_tree->Branch("pz1",&m_pz1,"pz1/D");
  m_tree->Branch("e1",&m_e1,"e1/D");
  m_tree->Branch("px2",&m_px2,"px2/D");
  m_tree->Branch("py2",&m_py2,"py2/D");
  m_tree->Branch("pz2",&m_pz2,"pz2/D");
  m_tree->Branch("e2",&m_e2,"e2/D");
  m_tree->Branch("nphotons",&m_nphotons,"nphotons/I");
  m_tree->Branch("photdata",m_photdata,"photdata[nphotons][4]/D");
  m_tree->Branch("RelativeProbability",&m_RelativeProbability,"RelativeProbability/D");

  m_tree->Branch("Acolinearity", &m_acol, "Acolinearity/D");
  m_h_smear = NULL;
  m_h_smeared = new TH1D("m_h_smeared","E smeared", 300,-0.03,0.03);
  // Double_t binsy[] = {173.499, 259.315, 339.497, 418.048, 495.54, 570.271, 642.931, 
  //                     711.929, 777.561, 839.274, 897.389, 951.666, 1002.67, 1050.13,
  //                     1094.5, 1135.73, 1173.87, 1209.19, 1241.53, 1271.33, 1298.62, 
  //                     1323.62, 1346.27, 1366.8, 1385.35, 1401.97, 1416.64, 1429.58, 
  //                     1440.93, 1450.74, 1459.13, 1466.24, 1472.07, 1476.76, 1480.43,
  //                     1483.31, 1485.63, 1487.58, 1489.29, 1490.84, 1492.27, 1493.64,
  //                     1494.97, 1496.3, 1497.64, 1499.02, 1500.5, 1502.12, 1503.95,
  //                     1506.19, 1509.32};
  
  //m_h_e1_vs_e2 = new TH2D("m_h_e1_vs_e2","e1 vs e2",50,binsy,50,binsy);
  m_h_e1_vs_e2 = new TH2D("m_h_e1_vs_e2","e1 vs e2",100,0,1550,100,0,1550);
  m_diff_px = new TH1D("m_h_diffpx","diff px",1000,-1,1);
  m_diff_py = new TH1D("m_h_diffpy","diff py",1000,-1,1);
  m_diff_pz = new TH1D("m_h_diffpz","diff pz",1000,-1,1);
  m_h_acol_res = new TH1D("h_acol_res","acol resol",1000,-0.005,0.005);
  
  m_smearing_E = 0.0;
  m_smearing_E_constant = 0.0;
  m_smearing_th = 0.;
  m_smearing_phi = 0.;
  m_smearing_th_hist = NULL;
  gRandom = new TRandom3(0);

  m_a = new double[4];
  m_b = new double[4];
  m_a[0] = 8.19e-4; m_b[0] = 9.07e-3;
  m_a[1] = 9.86e-5; m_b[1] = 3.83e-3;
  m_a[2] = 3.87e-5; m_b[2] = 1.59e-3;
  m_a[3] = 1.97e-5; m_b[3] = 7.22e-4;

}
//=============================================================================
// Destructor
//=============================================================================
SmearEnergy::~SmearEnergy() {
  delete m_fout;
  delete gRandom;
  delete[] m_a;
  delete[] m_b;

}

//=============================================================================
void SmearEnergy::setSmearingThetaHisto(TH2D* smear ){ 
  m_smearing_th_hist = smear; 
  int nbins = m_smearing_th_hist->GetNbinsX();
  for (int i = 0; i<nbins ; ++i){
    TH1D* hproj = m_smearing_th_hist->ProjectionY(Form("_px_%i",i),i,i);
    double E = m_smearing_th_hist->GetBinLowEdge(i);
    m_hitos_th_vs_E.insert(std::pair<double, TH1D*>(E,hproj));
  }
  m_fout->cd();
  return;
}
//=========================================================================
//
//=========================================================================
void SmearEnergy::Loop ( ) {

  if (!fChain) return;
  std::cout<<"Looping"<<std::endl;

  Long64_t nentries = fChain->GetEntries();

  //nentries=100000;

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;

    m_eb1 = eb1;
    m_eb2 = eb2;
    m_E1 = E1;
    m_E2 = E2;
    m_deb1 = deb1;
    m_deb2 = deb2;
    m_deb1_g = deb1_g;
    m_deb2_g = deb2_g;
    m_px1 = px1;
    m_py1 = py1;
    m_pz1 = pz1;
    m_px2 = px2;
    m_py2 = py2;
    m_pz2 = pz2;

    m_e1 = e1;
    m_e2 = e2;
    TLorentzVector part_e1(m_px1,m_py1,m_pz1,m_e1);
    TLorentzVector part_e2(m_px2,m_py2,m_pz2,m_e2);
    double acol_before(effective_sqrts(part_e1.Theta(),part_e2.Theta()));
    if (m_smearing_E && m_smearing_th){
      SmearFactor(part_e1);
      SmearFactor(part_e2);
    }
    else if (m_smearing_E && m_smearing_th_hist){
      SmearFactorHisto(part_e1);
      SmearFactorHisto(part_e2);
    }
    else {
      SmearHisto(part_e1);
      SmearHisto(part_e2);
    }
    m_diff_px->Fill((part_e1.Px() - m_px1)/m_px1);
    m_diff_py->Fill((part_e1.Py() - m_py1)/m_py1);
    m_diff_pz->Fill((part_e1.Pz() - m_pz1)/m_pz1);

    m_e1 = part_e1.E();
    m_e2 = part_e2.E();
    m_px1 = part_e1.Px();
    m_py1 = part_e1.Py();
    m_pz1 = part_e1.Pz();
    m_px2 = part_e2.Px();
    m_py2 = part_e2.Py();
    m_pz2 = part_e2.Pz();

    for (int i =0;i<4;++i)
      m_photdata[0][i]=0;
    m_nphotons = nphotons;

    
    for (int i = 0; i < nphotons; ++i){
      for (int j = 0; j < 3; ++j)
        m_photdata[i][j] = photdata[i][j];
      m_photdata[i][3] = gRandom->Gaus(photdata[i][3],TMath::Sqrt(photdata[i][3])*m_smearing_E);
      TLorentzVector ph(0,0,0,0);
      ph.SetPxPyPzE(m_photdata[i][0],m_photdata[i][1],m_photdata[i][2],m_photdata[i][3]);
      
      if (part_e1.Angle(ph.Vect())<3*TMath::Pi()/180){
	m_e1+=ph.E();
      }else if (part_e2.Angle(ph.Vect())<3*TMath::Pi()/180){
        m_e2+=ph.E();
      }
      
      //if (part_e1.Angle(ph.Vect())<part_e2.Angle(ph.Vect())){
      // m_e1+=ph.E();
      //}else {
      // m_e2+=ph.E();
      //}
        
    }


    m_RelativeProbability = RelativeProbability;
    m_acol = effective_sqrts(part_e1.Theta(),part_e2.Theta());
    m_h_acol_res->Fill(m_acol-acol_before);
    
    m_h_e1_vs_e2->Fill(m_e1,m_e2);

    m_tree->Fill();

  }
  std::cout<<"Done looping"<<std::endl;

}

void SmearEnergy::SmearFactor(TLorentzVector&part1){
  double p_e1 = part1.E();
  double smearfact_e1 = TMath::Sqrt(m_smearing_E*m_smearing_E/p_e1 + 
				    m_smearing_E_constant*m_smearing_E_constant);
  double smear_e1 = p_e1*smearfact_e1;
    
  //now smear the energy;
  double newe1 = gRandom->Gaus(p_e1,smear_e1);
  m_h_smeared->Fill((newe1-p_e1)/p_e1);
  p_e1 = newe1;
  
  part1.SetTheta(gRandom->Gaus(part1.Theta(),m_smearing_th));
  part1.SetPhi(gRandom->Gaus(part1.Phi(),m_smearing_phi));
  part1.SetE(p_e1);
  return;
}
void SmearEnergy::SmearFactorHisto(TLorentzVector&part1){
  double p_e1 = part1.E();
  double smearfact_e1 = TMath::Sqrt(m_smearing_E*m_smearing_E/p_e1 + 
				    m_smearing_E_constant*m_smearing_E_constant);
  double smear_e1 = p_e1*smearfact_e1;
    
  //now smear the energy;
  double deltae1 = gRandom->Gaus(0,smear_e1);
  m_h_smeared->Fill((deltae1)/p_e1);
  double smear_factor(1+deltae1/p_e1);
  p_e1 = p_e1*smear_factor;
  //new smear theta
  std::map<double, TH1D*>::iterator it = m_hitos_th_vs_E.lower_bound(part1.E());
  
  part1.SetE(p_e1);
  part1.SetPx(part1.X()*smear_factor);
  part1.SetPy(part1.Y()*smear_factor);
  part1.SetPz(part1.Z()*smear_factor);
  part1.SetTheta(part1.Theta()+(*it).second->GetRandom());
  return;
}

void SmearEnergy::SmearHisto(TLorentzVector&part1){
  double p_e1 = part1.E();
  
  double t_e1 = part1.Theta();

  double delta_e1 = 0;
  double delta_t1 = 0;

  m_h_smear->GetRandom2(delta_e1,delta_t1);

  part1.SetE(p_e1*p_e1*delta_e1+p_e1);
  part1.SetTheta(t_e1+delta_t1);
  return;
}

//=========================================================================
//
//=========================================================================
void SmearEnergy::Save () {
  m_tree->Write();
  m_h_e1_vs_e2->Write();
  m_diff_px->Write();
  m_diff_py->Write();
  m_diff_pz->Write();
  m_h_acol_res->Write();
  m_h_smeared->Write();
  m_fout->Write();
  m_fout->Close();

}


//=========================================================================
//
//=========================================================================
int  SmearEnergy::getIndex (double theta ) {
  int index =0;
  if (theta > (80*TMath::Pi()/180.))
    index = 3;
  else if (theta > (30*TMath::Pi()/180.))
    index = 2;
  else if (theta > (20*TMath::Pi()/180.))
    index = 1;
  else
    index = 0;
  return index;

}

//=========================================================================
//  
//=========================================================================
double SmearEnergy::deltaE (TLorentzVector vec ) {
  double deltae = 0;
  double sinth = TMath::Sin(vec.Theta());
  double p = vec.P();
  double e = vec.E();
  
  int index = getIndex(vec.Theta());
  deltae = TMath::Sqrt(m_a[index]*m_a[index]+
                       (m_b[index]*m_b[index]/
                        (p*p*sinth*sinth)))*e*e*sinth;
  

  return deltae;
}
