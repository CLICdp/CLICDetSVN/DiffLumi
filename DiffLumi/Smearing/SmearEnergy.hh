// $Id: $
#ifndef SMEARENERGY_SMEARENERGY_HH
#define SMEARENERGY_SMEARENERGY_HH 1

// Include files
#include <TChain.h>

#include <TRandom3.h>
#include "TString.h"
#include "MCTupleBHWide.h"
#include <TFile.h>
#include "Riostream.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
/** @class SmearEnergy SmearEnergy.hh SmearEnergy/SmearEnergy.hh
 *
 *
 *  @author Stephane Poss
 *  @date   2011-11-07
 */
class SmearEnergy: public MCTupleBHWide {
public:
  /// Standard constructor
  SmearEnergy(TChain* ch=0, TString fname="");
  SmearEnergy(const SmearEnergy&);
  SmearEnergy& operator=(const SmearEnergy&);

  virtual ~SmearEnergy( ); ///< Destructor

  inline void setSmearingE(double smear, double consterm = 0.0) { m_smearing_E = smear; 
    m_smearing_E_constant = consterm;}
  inline void setSmearingTheta(double smear ) { m_smearing_th = smear; }
  void setSmearingThetaHisto(TH2D* smear );

  inline void setSmearingPhi(double smear ) { m_smearing_phi = smear; }
  void setSmearingHisto(TH2D*h){m_h_smear = h;}
  void Loop();
  void Save();
  

protected:

private:

  TTree*m_tree;
  double m_smearing_E;
  double m_smearing_E_constant;
  double m_smearing_th;
  double m_smearing_phi;
  
  Double_t        m_eb1;
  Double_t        m_eb2;
  Double_t        m_E1;
  Double_t        m_E2;
  Double_t        m_deb1;
  Double_t        m_deb2;
  Double_t        m_deb1_g;
  Double_t        m_deb2_g;
  Double_t        m_px1;
  Double_t        m_py1;
  Double_t        m_pz1;
  Double_t        m_e1;
  Double_t        m_px2;
  Double_t        m_py2;
  Double_t        m_pz2;
  Double_t        m_e2;
  Int_t           m_nphotons;
  Double_t        m_photdata[100][4];   //[nphotons]
  Double_t        m_RelativeProbability;
  Double_t        m_acol;
  TH1D*m_h_smeared,*m_diff_px,*m_diff_py,*m_diff_pz,*m_h_acol_res;
  TH2D*m_h_e1_vs_e2;
  TH2D*m_h_smear,*m_smearing_th_hist;
  std::map<double, TH1D*> m_hitos_th_vs_E;
  TRandom3*gRandom;
  TFile*m_fout;
  double*m_a,*m_b;
  
  int getIndex(double);
  double deltaE (TLorentzVector);
  void SmearFactor(TLorentzVector&p);
  void SmearFactorHisto(TLorentzVector&p);
  void SmearHisto(TLorentzVector&p);
};
#endif // SMEARENERGY_SMEARENERGY_HH
