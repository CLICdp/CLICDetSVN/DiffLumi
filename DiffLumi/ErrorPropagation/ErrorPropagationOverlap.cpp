#include "ErrorPropagationOverlap.hh"
#include "ErrorPropagation.hh"
#include "BetaFunctionBS.h"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TMath.h>
#include <TString.h>
#include <TVectorD.h>

#include <iostream>

using ErrorPropagation::Integrate;
using ErrorPropagation::IntegrateInner;

using ErrorPropagation::GetBeamSpreadPeak1;
using ErrorPropagation::GetBeamSpreadPeak2;
using ErrorPropagation::GetBeamSpreadArm1;
using ErrorPropagation::GetBeamSpreadArm2;

using ErrorPropagation::GetGaussSigma;

using ErrorPropagation::GetBeamStrahlungArm1;
using ErrorPropagation::GetBeamStrahlungArm2;
using ErrorPropagation::GetBeamStrahlungBody1;
using ErrorPropagation::GetBeamStrahlungBody2;

void ErrorPropagationOverlap::SetParameters(double* newPar){
  ErrorPropagation::SetParameters(newPar);
  SetBetaLimits();
  return;
}

void ErrorPropagationOverlap::SetBetaLimits() {
  GetBeamStrahlungArm1()->SetBetaLimit(0.9999);
  GetBeamStrahlungArm2()->SetBetaLimit(0.9999);

  GetBeamStrahlungBody1()->SetBetaLimit(0.995);
  GetBeamStrahlungBody2()->SetBetaLimit(0.995);
}


double ErrorPropagationOverlap::m_peak() { return ErrorPropagation::GetParameters()[0];}
double ErrorPropagationOverlap::m_arm1() { return ErrorPropagation::GetParameters()[1];}
double ErrorPropagationOverlap::m_arm2() { return ErrorPropagation::GetParameters()[2];}
double ErrorPropagationOverlap::m_body() { return 1.0 - m_peak() - m_arm1() - m_arm2();}


double ErrorPropagationOverlap::ConvBeamSpreadPeak1Gauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  const double sigma = GetGaussSigma();
  return (*(GetBeamSpreadPeak1()))(tau)*TMath::Gaus(x-tau,0.0,sigma,kTRUE);
}

double ErrorPropagationOverlap::ConvBeamSpreadPeak2Gauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  const double sigma = GetGaussSigma();
  return (*(GetBeamSpreadPeak2()))(tau)*TMath::Gaus(x-tau,0.0,sigma,kTRUE);
 
}

double ErrorPropagationOverlap::ConvBeamSpreadArm1Gauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  const double sigma = GetGaussSigma();

  if ( tau <= ErrorPropagation::GetPeakMin() ) return 0.0;
  if ( ErrorPropagation::GetPeakMax() <= tau ) return 0.0;

  return (*(GetBeamSpreadArm1()))(tau)*TMath::Gaus(x-tau,0.0,sigma,kTRUE);
}

double ErrorPropagationOverlap::ConvBeamSpreadArm2Gauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  const double sigma = GetGaussSigma();
  if ( tau <= ErrorPropagation::GetPeakMin() ) return 0.0;
  if ( ErrorPropagation::GetPeakMax() <= tau ) return 0.0;
  return (*(GetBeamSpreadArm2()))(tau)*TMath::Gaus(x-tau,0.0,sigma,kTRUE);
}

// double ErrorPropagationOverlap::ConvBeamSpreadArm1Gauss(double tau, void *p) {
//   const double x = ((double*)p)[0];
//   const double sigma = GetGaussSigma();
//   return (*(GetBeamSpreadArm1()))(tau)*TMath::Gaus(x-tau,0.0,sigma,kTRUE);
// }


double ErrorPropagationOverlap::ConvBeamSpreadPeak1Beta(double tau, void *p) {
  const double x = ((double*)p)[0];
  if( x-tau >= 0.9999 ) return 0.0;
  if( x-tau <= 0.0 ) return 0.0;
  // if ( tau <= ErrorPropagation::GetPeakMin() ) return 0.0;
  // if ( ErrorPropagation::GetPeakMax() <= tau ) return 0.0;
  return (*(GetBeamSpreadPeak1()))(tau) * (*(GetBeamStrahlungArm1()))(x-tau) ;

  //const double sigma = GetGaussSigma();
  // return (*(GetBeamSpreadPeak1()))(tau) * (x-tau) ;
  // return 1.0 * (*(GetBeamStrahlungArm1()))(x-tau) ;
  // return (*(GetBeamSpreadPeak1()))(tau) * TMath::Gaus(x-tau,0.0,1e-4,kTRUE);
  //return TMath::Gaus(tau,0.0,1e-3,kTRUE) * (*(GetBeamStrahlungArm1()))(x-tau);
}

double ErrorPropagationOverlap::ConvBeamSpreadPeak1Beta(double x1){
  const double para1Beta[] = {x1};
  const double peakMin = ErrorPropagation::GetPeakMin();
  const double peakMax = ErrorPropagation::GetPeakMax();
  return IntegrateInner(&ConvBeamSpreadPeak1Beta, para1Beta, peakMin, peakMax);
}


double ErrorPropagationOverlap::ConvBeamSpreadPeak2Beta(double tau, void *p) {
  const double x = ((double*)p)[0];
  if( x-tau >= 0.9999 ) return 0.0;
  if( x-tau <= 0.0 ) return 0.0;
  // if ( tau <= ErrorPropagation::GetPeakMin() ) return 0.0;
  // if ( ErrorPropagation::GetPeakMax() <= tau ) return 0.0;
  return (*(GetBeamSpreadPeak2()))(tau) * (*(GetBeamStrahlungArm2()))(x-tau) ;

}

double ErrorPropagationOverlap::ConvBeamSpreadPeak2Beta(double x2){
  const double para2Beta[] = {x2};
  const double peakMin = ErrorPropagation::GetPeakMin();
  const double peakMax = ErrorPropagation::GetPeakMax();
  return IntegrateInner(&ConvBeamSpreadPeak2Beta, para2Beta, peakMin, peakMax);
}


double ErrorPropagationOverlap::ConvConvBeamSpreadPeak1BetaGauss(double lambda, void*p) {
  const double y = ((double*)p)[0];
  return TMath::Gaus(lambda, 0.0, ErrorPropagation::GetGaussSigma(), kTRUE) * ConvBeamSpreadPeak1Beta(y-lambda);
}


double ErrorPropagationOverlap::ConvConvBeamSpreadPeak2BetaGauss(double lambda, void*p) {
  const double y = ((double*)p)[0];
  return TMath::Gaus(lambda, 0.0, ErrorPropagation::GetGaussSigma(), kTRUE) * ConvBeamSpreadPeak2Beta(y-lambda);
}



double ErrorPropagationOverlap::CalculateProbability(double x1, double x2) {
 
  double peakVal(0.0), arm1Val(0.0), arm2Val(0.0), bodyVal(0.0);

  // //Peak is convolution of gauss with beam energy spread, do we have the sigma
  // //somewhere? ok, gotta hardcode is constant anyway for now
  // const double para1[] = {x1-1.0};
  // const double para2[] = {x2-1.0};

  // const double para1Beta[] = {x1};
  // const double para2Beta[] = {x2};

  // const double peakMin= ErrorPropagation::GetPeakMin();
  // const double peakMax = ErrorPropagation::GetPeakMax();
  // const double fiveSigma = 5.0 * ErrorPropagation::GetGaussSigma();

  // double peakValPart1( Integrate(&ConvBeamSpreadPeak1Gauss, para1, peakMin, peakMax) );
  // double peakValPart2( Integrate(&ConvBeamSpreadPeak2Gauss, para2, peakMin, peakMax) );

  // double arm1ValPart2( Integrate(&ConvBeamSpreadArm2Gauss, para2, peakMin, peakMax) );
  // double arm1ValPart1( Integrate(&ConvConvBeamSpreadPeak1BetaGauss,para1Beta, -fiveSigma, fiveSigma));

  peakVal = GetX1ValuePeak(x1) * GetX2ValuePeak(x2);
  arm1Val = GetX1ValueArm1(x1) * GetX2ValueArm1(x2);

  return ErrorPropagation::m_peak() * peakVal 
    + ErrorPropagation::m_arm1() * arm1Val 
    + ErrorPropagation::m_arm2() * arm2Val 
    + ErrorPropagation::m_body() * bodyVal;
}

double ErrorPropagationOverlap::GetX1ValuePeak(double x1){
  const double para1[] = {x1-1.0};
  const double peakMin= ErrorPropagation::GetPeakMin();
  const double peakMax = ErrorPropagation::GetPeakMax();
  const double peakValPart1( Integrate(&ConvBeamSpreadPeak1Gauss, para1, peakMin, peakMax) );
  return peakValPart1;
}

double ErrorPropagationOverlap::GetX2ValuePeak(double x2) {
  const double para2[] = {x2-1.0};
  const double peakMin= ErrorPropagation::GetPeakMin();
  const double peakMax = ErrorPropagation::GetPeakMax();
  const double peakValPart2( Integrate(&ConvBeamSpreadPeak2Gauss, para2, peakMin, peakMax) );
  return peakValPart2;
}


double ErrorPropagationOverlap::GetX1ValueArm1(double x1) {
  const double para1Beta[] = {x1};
  const double fiveSigma = 5.0 * ErrorPropagation::GetGaussSigma();
  const double arm1ValPart1( Integrate(&ConvConvBeamSpreadPeak1BetaGauss,para1Beta, -fiveSigma, fiveSigma));
  return arm1ValPart1;
}

double ErrorPropagationOverlap::GetX2ValueArm1(double x2){
  const double para2[] = {x2-1.0};
  const double peakMin= ErrorPropagation::GetPeakMin();
  const double peakMax = ErrorPropagation::GetPeakMax();
  const double arm1ValPart2( Integrate(&ConvBeamSpreadArm2Gauss, para2, peakMin, peakMax) );
  return arm1ValPart2;
}



double ErrorPropagationOverlap::GetX1ValueArm2(double x1){
  const double para1[] = {x1-1.0};
  const double peakMin= ErrorPropagation::GetPeakMin();
  const double peakMax = ErrorPropagation::GetPeakMax();
  const double arm2ValPart1( Integrate(&ConvBeamSpreadArm1Gauss, para1, peakMin, peakMax) );
  return arm2ValPart1;
}

double ErrorPropagationOverlap::GetX2ValueArm2(double x2) {
  const double para2Beta[] = {x2};
  const double fiveSigma = 5.0 * ErrorPropagation::GetGaussSigma();
  const double arm2ValPart2( Integrate(&ConvConvBeamSpreadPeak2BetaGauss, para2Beta, -fiveSigma, fiveSigma));
  return arm2ValPart2;
}





double ErrorPropagationOverlap::GetX1ValueBody(double x1) {
  const double para1Beta[] = {x1};
  const double fiveSigma = 5.0 * ErrorPropagation::GetGaussSigma();
  const double BodyValPart1( Integrate(&ConvBeamstrahlungBeta1Gauss, para1Beta, -fiveSigma, fiveSigma));
  return BodyValPart1;
}

double ErrorPropagationOverlap::GetX2ValueBody(double x2) {
  const double para2Beta[] = {x2};
  const double fiveSigma = 5.0 * ErrorPropagation::GetGaussSigma();
  const double BodyValPart2( Integrate(&ConvBeamstrahlungBeta2Gauss, para2Beta, -fiveSigma, fiveSigma));
  return BodyValPart2;
}



double ErrorPropagationOverlap::ConvBeamstrahlungBeta1Gauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  if( x-tau >= 0.995 ) return 0.0;
  if( x-tau <= 0.0) return 0.0;
  const double sigma = GetGaussSigma();
  return TMath::Gaus(tau,0.0,sigma,kTRUE) * (*(GetBeamStrahlungBody1()))(x-tau) ;
}

double ErrorPropagationOverlap::ConvBeamstrahlungBeta2Gauss(double tau, void *p) {
  const double x = ((double*)p)[0];
  if( x-tau >= 0.995 ) return 0.0;
  if( x-tau <= 0.0 ) return 0.0;
  const double sigma = GetGaussSigma();
  return TMath::Gaus(tau,0.0,sigma,kTRUE) * (*(GetBeamStrahlungBody2()))(x-tau) ;
}

void ErrorPropagationOverlap::CreateMyLumiFile(double* parameter, TString const& outputSuffix, int nBins, unsigned long long nSqrts,
					       double xMin, double xMax) {

  ErrorPropagationOverlap::SetParameters(parameter);
  std::cout << "Running " << outputSuffix  << std::endl;

  TH1D x1ValuesPeak("x1ValuesPeak", "x1ValuesPeak", nBins, xMin, xMax);
  TH1D x2ValuesPeak("x2ValuesPeak", "x2ValuesPeak", nBins, xMin, xMax);
  TH1D x1ValuesArm1("x1ValuesArm1", "x1ValuesArm1", nBins, xMin, xMax);
  TH1D x2ValuesArm1("x2ValuesArm1", "x2ValuesArm1", nBins, xMin, xMax);
  TH1D x1ValuesArm2("x1ValuesArm2", "x1ValuesArm2", nBins, xMin, xMax);
  TH1D x2ValuesArm2("x2ValuesArm2", "x2ValuesArm2", nBins, xMin, xMax);
  TH1D x1ValuesBody("x1ValuesBody", "x1ValuesBody", nBins, xMin, xMax);
  TH1D x2ValuesBody("x2ValuesBody", "x2ValuesBody", nBins, xMin, xMax);

  TH2D MyLumi("Lumi","MyLumi", nBins, xMin, xMax, nBins, xMin, xMax);

  for (int i = 1; i <= nBins ;++i) {

#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, nBins);
#endif

    double x1 = x1ValuesPeak.GetXaxis()->GetBinCenter(i);
    double x2 = x1;

    // std::cout << '\r'<< std::setw(5)  << i << std::setw(15) << x1 << std::flush;

    x1ValuesPeak.SetBinContent(i, ErrorPropagationOverlap::GetX1ValuePeak(x1));

    // std::cout << std::setw(15) << x1ValuesPeak.GetBinContent(i) << std::flush;

    x2ValuesPeak.SetBinContent(i, ErrorPropagationOverlap::GetX2ValuePeak(x2));

    // std::cout << std::setw(15) << x2ValuesPeak.GetBinContent(i) << std::flush;

    x1ValuesArm1.SetBinContent(i, ErrorPropagationOverlap::GetX1ValueArm1(x1));

    // std::cout << std::setw(15) << x1ValuesArm1.GetBinContent(i) << std::flush;
      
    x2ValuesArm1.SetBinContent(i, ErrorPropagationOverlap::GetX2ValueArm1(x2));
    // std::cout << std::setw(15) << x2ValuesArm1.GetBinContent(i) << std::flush;

    x1ValuesArm2.SetBinContent(i, ErrorPropagationOverlap::GetX1ValueArm2(x1));
    // std::cout << std::setw(15) << x1ValuesArm2.GetBinContent(i) << std::flush;
      
    x2ValuesArm2.SetBinContent(i, ErrorPropagationOverlap::GetX2ValueArm2(x2));
    // std::cout << std::setw(15) << x2ValuesArm2.GetBinContent(i) << std::flush;

    x1ValuesBody.SetBinContent(i, ErrorPropagationOverlap::GetX1ValueBody(x1));
    // std::cout << std::setw(15) << x1ValuesBody.GetBinContent(i) << std::flush;
      
    x2ValuesBody.SetBinContent(i, ErrorPropagationOverlap::GetX2ValueBody(x2));
    // std::cout << std::setw(15) << x2ValuesBody.GetBinContent(i) << std::flush;

    // std::cout << std::endl;
  }
  std::cout << "Done Calculating"  << std::endl;

  for (int i = 1; i <= nBins; ++i) {
    for (int j = 1; j <= nBins; ++j) {

      //	double value1 = ErrorPropagationOverlap::CalculateProbability(x1, x2);
      double value2 
	= ErrorPropagationOverlap::m_peak()  * x1ValuesPeak.GetBinContent(i) * x2ValuesPeak.GetBinContent(j)
	+ ErrorPropagationOverlap::m_arm1()  * x1ValuesArm1.GetBinContent(i) * x2ValuesArm1.GetBinContent(j)
	+ ErrorPropagationOverlap::m_arm2()  * x1ValuesArm2.GetBinContent(i) * x2ValuesArm2.GetBinContent(j)
	+ ErrorPropagationOverlap::m_body()  * x1ValuesBody.GetBinContent(i) * x2ValuesBody.GetBinContent(j);

      // double x1 = MyLumi.GetXaxis()->GetBinCenter(i);
      // double x2 = MyLumi.GetYaxis()->GetBinCenter(j);
      // std::cout << std::setw(13) << x1
      // 	  << std::setw(13) << x2
      //   //	  << std::setw(13) << value1
      // 	  << std::setw(13) << value2
      // 	  << std::endl;

      MyLumi.SetBinContent(i, j, value2);
    }//all y bins
  }//all x bins


  //Fill the sqrts distribution too
  double x1, x2;
  TH1D hSqrts("hSqrts","#sqrt{s};#sqrt{s'}/#sqrt{s_{nom}};dN/dx", nBins, sqrt(xMin), xMax);
  unsigned long long maxEntries(nSqrts);
  for (unsigned long long i = 0; i < maxEntries;++i) {
#ifdef USE_RootStyle
    RootStyle::PrintTreeProgress(i, maxEntries);
#endif
    MyLumi.GetRandom2(x1,x2);
    hSqrts.Fill(sqrt(x1*x2));
  }
    

  hSqrts.Scale(1./ hSqrts.Integral("width") );

  // canv.SetLogz();
  // MyLumi.Draw("colz");
  // MyLumi.SetAxisRange(1e-3, 1e5,"Z");
  // //MyLumi.SetAxisRange(1e-10, 1e-3,"Z");
  // canv.SaveAs("AnalyticalHistogramOverlap.eps");
  // TCanvas canv("canv","canv");
  // hSqrts.Draw();
  // canv.SaveAs("AnalyticalSqrts_"+outputSuffix+".eps");

  TFile *file = TFile::Open("NumericalEstimateOutput_"+ outputSuffix + ".root","RECREATE");
  //  MyLumi.Write();

  TVectorD parVec(35, parameter);

  parVec.Write("parVector");
  // x1ValuesPeak.Write();
  // x2ValuesPeak.Write();
  // x1ValuesArm1.Write();
  // x2ValuesArm1.Write();
  // x1ValuesArm2.Write();
  // x2ValuesArm2.Write();
  // x1ValuesBody.Write();
  // x2ValuesBody.Write();
  hSqrts.Write();
  file->Close();
}

