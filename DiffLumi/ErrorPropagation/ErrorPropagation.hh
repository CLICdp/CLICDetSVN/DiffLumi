#ifndef ErrorPropagation_hh
#define ErrorPropagation_hh 1

#include "BetaFunctionBS.h"

#include <Math/GSLIntegrator.h>

#include <iostream>


class BetaFunctionBS;
class BetaFunction;

class TFile;
class TString;
class TH1D;
template <class T> class TVectorT;
typedef TVectorT<double> TVectorD;

namespace ErrorPropagation{

  /// Returns an array of parameters
  double* GetParameters();

  double CalculateProbability(double x1, double x2);

  double Integrate(double (*function)(double, void*), const double* parameter,
		   double xmin, double xmax, double precision= 0.00004);

  double IntegrateInner(double (*function)(double, void*), const double* parameter,
			double xmin, double xmax, double precision= 0.00004);

  /// Returns parameters
  inline double GetPeakMin() { return GetParameters()[7]; }
  inline double GetPeakMax() { return GetParameters()[8]; }
  
  inline double m_peak() { return GetParameters()[0];}
  inline double m_arm1() { return GetParameters()[1];}
  inline double m_arm2() { return GetParameters()[2];}
  inline double m_body() { return 1.0 - m_peak() - m_arm1() - m_arm2();}
#warning "GetGaussSigma is constant!!!! for 350 GeV"
  //inline double GetGaussSigma() { return 0.0001367; }  
  inline double GetGaussSigma() { return 0.00079806; }
                                         

  /// We need 
  static BetaFunctionBS *BFBSPeak1(NULL),*BFBSPeak2(NULL),*BFBSArm1(NULL),*BFBSArm2(NULL);
  static BetaFunction *BFArm1(NULL),*BFArm2(NULL),*BFBody1(NULL),*BFBody2(NULL);


  BetaFunctionBS* GetBeamSpreadPeak1();
  BetaFunctionBS* GetBeamSpreadPeak2();
  BetaFunctionBS* GetBeamSpreadArm1();
  BetaFunctionBS* GetBeamSpreadArm2();

  BetaFunction* GetBeamStrahlungArm1();
  BetaFunction* GetBeamStrahlungArm2();
  BetaFunction* GetBeamStrahlungBody1();
  BetaFunction* GetBeamStrahlungBody2();


  static double* parameters(NULL);
  void SetParameters(double const* newPar);
  void ResetFunctions();


class Values{
public:
  Values(TString const& filename);
  Values(TFile *filehandle);
  ~Values();
  double GetProbability(double x1, double x2) const;
  double GetProbability(int x1, int x2) const;
  double GetProbability(int binglobal) const;
  int GetNbins() const;

private:
  TFile *file;

  TH1D *x1ValuesPeak;
  TH1D *x2ValuesPeak;
  TH1D *x1ValuesArm1;
  TH1D *x2ValuesArm1;
  TH1D *x1ValuesArm2;
  TH1D *x2ValuesArm2;
  TH1D *x1ValuesBody;
  TH1D *x2ValuesBody;
  TVectorD *parVector;
  double aPeak, aArm1, aArm2, aBody;
  int nBins;
};

}// namespace


inline BetaFunctionBS* ErrorPropagation::GetBeamSpreadPeak1() { return ErrorPropagation::BFBSPeak1; }
inline BetaFunctionBS* ErrorPropagation::GetBeamSpreadPeak2() { return ErrorPropagation::BFBSPeak2; }
inline BetaFunctionBS* ErrorPropagation::GetBeamSpreadArm1()  { return ErrorPropagation::BFBSArm1; }
inline BetaFunctionBS* ErrorPropagation::GetBeamSpreadArm2()  { return ErrorPropagation::BFBSArm2; }

inline BetaFunction* ErrorPropagation::GetBeamStrahlungArm1() { return ErrorPropagation::BFArm1; }
inline BetaFunction* ErrorPropagation::GetBeamStrahlungArm2() { return ErrorPropagation::BFArm2; }
inline BetaFunction* ErrorPropagation::GetBeamStrahlungBody1(){ return ErrorPropagation::BFBody1; }
inline BetaFunction* ErrorPropagation::GetBeamStrahlungBody2(){ return ErrorPropagation::BFBody2; }


inline double ErrorPropagation::Integrate(double (*function)(double, void*),const double* parameter, 
				   double xmin, double xmax, double) {
  static ROOT::Math::GSLIntegrator integrator(ROOT::Math::IntegrationOneDim::kADAPTIVE,
					      // 1e-7, //absTolerance
					      // 1e-5, //relativeTolerance
					      // 1000);//numberOfIterations
					      1e-7, //absTolerance
					      1e-5, //relativeTolerance
					      3000);//numberOfIterations
  integrator.SetFunction(function, (void*) parameter);
  //  integrator.SetRelTolerance(precision);
  return integrator.Integral(xmin, xmax);
}


inline double ErrorPropagation::IntegrateInner(double (*function)(double, void*),const double* parameter, 
					double xmin, double xmax, double) {
  static ROOT::Math::GSLIntegrator integrator(ROOT::Math::IntegrationOneDim::kADAPTIVE,
					      1e-8, //absTolerance
					      1e-6, //relativeTolerance
					      10000);//numberOfIterations
					      // 1e-7, //absTolerance
					      // 1e-5, //relativeTolerance
					      // 1000);//numberOfIterations

  integrator.SetFunction(function, (void*) parameter);
  //  integrator.SetRelTolerance(precision);
  return integrator.Integral(xmin, xmax);
}


inline double* ErrorPropagation::GetParameters(){
  return ErrorPropagation::parameters;
  // static const double parameter[] = { bsbfa, bsbfb,
  // 				      bsLow, bsHigh,
  // 				      bsbfa, 1.0,
  // 				      bsLow, bsHigh,
  // 				      parameter[4], //sigma
  // 				      0.5};//ratio
  //return parameter;
}

inline void ErrorPropagation::ResetFunctions() {

  double* parameter;
  if(ErrorPropagation::BFBSPeak1) delete ErrorPropagation::BFBSPeak1;
  parameter = GetParameters()+5;
  ErrorPropagation::BFBSPeak1 =  new BetaFunctionBS(parameter[0],parameter[1],parameter[2],parameter[3]);


  if(ErrorPropagation::BFBSPeak2) delete ErrorPropagation::BFBSPeak2;
  parameter = GetParameters()+11;
  ErrorPropagation::BFBSPeak2 =  new BetaFunctionBS(parameter[0],parameter[1],parameter[2],parameter[3]);
  
  if(ErrorPropagation::BFBSArm1) delete ErrorPropagation::BFBSArm1;
  parameter = GetParameters()+19;
  ErrorPropagation::BFBSArm1 =  new BetaFunctionBS(parameter[0],parameter[1],parameter[2],parameter[3]);

  if(ErrorPropagation::BFBSArm2) delete ErrorPropagation::BFBSArm2;
  parameter = GetParameters()+23;
  ErrorPropagation::BFBSArm2 =  new BetaFunctionBS(parameter[0],parameter[1],parameter[2],parameter[3]);


  if(ErrorPropagation::BFArm1) delete ErrorPropagation::BFArm1;
  parameter = GetParameters()+3;
  ErrorPropagation::BFArm1 =  new BetaFunction(parameter[0],parameter[1]);


  if(ErrorPropagation::BFArm2) delete ErrorPropagation::BFArm2;
  parameter = GetParameters()+9;
  ErrorPropagation::BFArm2 =  new BetaFunction(parameter[0],parameter[1]);

  if(ErrorPropagation::BFBody1) delete ErrorPropagation::BFBody1;
  parameter = GetParameters()+15;
  ErrorPropagation::BFBody1 =  new BetaFunction(parameter[0],parameter[1]);
  
  if(ErrorPropagation::BFBody2) delete ErrorPropagation::BFBody2;
  parameter = GetParameters()+17;
  ErrorPropagation::BFBody2 =  new BetaFunction(parameter[0],parameter[1]);

  return;
}//ResetFunctions

inline void ErrorPropagation::SetParameters(double const* newPar) {
  if(ErrorPropagation::parameters) delete[] ErrorPropagation::parameters;
  ErrorPropagation::parameters = new double[35];
  for (int i = 0; i < 35 ;++i) {
    ErrorPropagation::parameters[i] = newPar[i];
  }
  ErrorPropagation::ResetFunctions();
}


#endif // ErrorPropagation_hh
