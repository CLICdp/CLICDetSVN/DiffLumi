#ifndef ErrorPropagationOverlap_hh
#define ErrorPropagationOverlap_hh 1



class BetaFunctionBS;
class TString;

namespace ErrorPropagationOverlap{

  void CreateMyLumiFile(double* parameter, TString const& outputSuffix, int nBins = 3000, unsigned long long nSqrts = 1000000,
			double xMin=0.25, double xMax=1.02);

  double CalculateProbability(double x1, double x2);

  double GetX1ValuePeak(double x1);
  double GetX2ValuePeak(double x2);

  double GetX1ValueArm1(double x1);
  double GetX2ValueArm1(double x2);

  double GetX1ValueArm2(double x1);
  double GetX2ValueArm2(double x2);

  double GetX1ValueBody(double x1);
  double GetX2ValueBody(double x2);


  double ConvBeamSpreadPeak1Gauss(double tau, void *p);
  double ConvBeamSpreadPeak2Gauss(double tau, void *p);

  double ConvBeamSpreadArm1Gauss(double tau, void *p);
  double ConvBeamSpreadArm2Gauss(double tau, void *p);

  double ConvBeamSpreadPeak1Beta(double tau, void *p);
  double ConvBeamSpreadPeak1Beta(double x1);

  double ConvBeamSpreadPeak2Beta(double tau, void *p);
  double ConvBeamSpreadPeak2Beta(double x2);


  double ConvConvBeamSpreadPeak1BetaGauss(double tau, void*p);
  double ConvConvBeamSpreadPeak2BetaGauss(double tau, void*p);

  double ConvBeamstrahlungBeta1Gauss(double tau, void*p);
  double ConvBeamstrahlungBeta2Gauss(double tau, void*p);

  void SetParameters(double* newPar);
  void SetBetaLimits();

  double m_peak();
  double m_arm1();
  double m_arm2();
  double m_body();


}// namespace

#endif // ErrorPropagationOverlap_hh
