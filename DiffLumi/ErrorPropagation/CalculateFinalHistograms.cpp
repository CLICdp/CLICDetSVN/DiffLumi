#include "ErrorPropagationOverlap.hh"
#include "ErrorPropagation.hh"

#include "BetaFunction.h"
#include "BetaFunctionBS.h"
#include "Utilities.h"
#include "DrawSpectra.hh"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TCanvas.h>
#include <TFile.h>
#include <TH2D.h>
#include <TMatrixTSym.h>
#include <TString.h>
#include <TVectorD.h>

#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <vector>

typedef std::vector<TH1D*> HistVector;
typedef std::vector<TH2D*> HistVector2D;

typedef std::vector<ErrorPropagation::Values*> ValueVector;

void PrintMatrixLatex(  TMatrixTSym<double> const& matrix);

int main (int argc, char** argv) {

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif
  const int maxPar = 35;

  if(argc < 4) {
    std::cerr << "Not enough filenames given... abort\n"
	      << "CalculateFinalHistograms GPInputFile.root FitResult.root HistoGrams_ [outputfilename.root]"
	      << std::endl;
    return 1;
  }
  TString outputfilename ("OutPutFile.root");
  if( argc == 5 )  outputfilename = TString(argv[4]);

  TString GPInputFile(argv[1]);

  std::string ParameterFilename(argv[2]);
  TString HistoFilenameStart(argv[3]);
  const int offSet = maxPar;
  const int maxSize = 2*(maxPar+1);
  HistVector histoVector(maxSize, NULL);
  //HistVector2D histoVector2D(36, NULL);
  ValueVector valuevector(maxSize, NULL);
  std::vector<TFile*> fileVector;

  //  TH2D* reference2D;

  //We try to open as many files as possible, but it doesn't matter if they don't exist
  //we must not close them, as we would lose the histograms then

  std::cout << "Reading available files, you can safely ignore errors."  << std::endl;
  for (int i = -maxPar; i <= maxPar ;++i) {
    std::cout << "Opening " << Form(HistoFilenameStart+"%i.root", i)  << std::flush;
    TFile *file = TFile::Open( Form(HistoFilenameStart+"%i.root", i) );
    if(file) {
      // if( i == 0 ) {
      // 	file->GetObject("Lumi",reference2D);
      // }
      fileVector.push_back( file );
      file->GetObject("hSqrts",histoVector[i+offSet]);
      histoVector[i+offSet]->Sumw2();
      histoVector[i+offSet]->Scale( 1./ histoVector[i+offSet]->Integral("width") );
      //      file->GetObject("Lumi",histoVector2D[i]);
      valuevector[i+offSet] = new ErrorPropagation::Values(Form(HistoFilenameStart+"%i.root", i));
      std::cout << std::endl;
    }
    //file->Close();
  }

  if( not histoVector[0+offSet] ) {
    std::cerr << "First Histogram not found ... aborting!" << std::endl;
    exit(1);
  }


  TFile *newFile = TFile::Open(outputfilename,"RECREATE");
  //std::cout << histoVector2D[0]  << std::endl;
  int nBins1D = histoVector[0+offSet]->GetNbinsX();
  double xMin(histoVector[0+offSet]->GetXaxis()->GetXmin()), xMax(histoVector[0+offSet]->GetXaxis()->GetXmax());

  // for (HistVector::iterator it = histoVector.begin(); it != histoVector.end(); ++it) {
  //   std::cout << *it << std::endl;
  //   const int rebinFactor(nBins1D/1000);
  //   std::cout << "Rebinning with " << rebinFactor  << std::endl;
  //   if ( *it ) (*it)->Rebin(rebinFactor);//30 GeV Bins
  // }

  const int nBinsRebinned(histoVector[0+offSet]->GetNbinsX());

  //This one should be rebinned

  TH1D hSqrts      ("hSqrts","#sqrt{s};#sqrt{s'}/#sqrt{s_{nom}};dN/dx", nBinsRebinned, (xMin), xMax);
  TH1D hSqrtsPlus1 ("hSqrtsP1Sig","#sqrt{s}+1Sig;#sqrt{s'}/#sqrt{s_{nom}};dN/dx", nBinsRebinned, (xMin), xMax);
  TH1D hSqrtsMinus1("hSqrtsM1Sig","#sqrt{s}-1Sig;#sqrt{s'}/#sqrt{s_{nom}};dN/dx", nBinsRebinned, (xMin), xMax);


  //This one should not be rebinned
  // TH2D Lumi("Lumi","Lumi", nBins1D, xMin, xMax, nBins1D, xMin, xMax);
  // TH2D LumiPlus1 ("LumiP1Sig","LumiP1Sig", nBins1D, xMin, xMax, nBins1D, xMin, xMax);
  // TH2D LumiMinus1("LumiM1Sig","LumiM1Sig", nBins1D, xMin, xMax, nBins1D, xMin, xMax);

  //Read the parameters and errors, but the only thing we care about is the correlation matrix...)
  double parameter[maxPar],parError[maxPar];
  TMatrixTSym<double> correlationMatrix(maxPar);
  Utility::ReadFitResult(ParameterFilename, parameter, parError, &correlationMatrix);
  //correlationMatrix.Print();

  PrintMatrixLatex(correlationMatrix);


  //Now we have to calculate the error for every single bin
  nBins1D = histoVector[0+offSet]->GetNbinsX();
  const int checkThisBin = histoVector[0+offSet]->GetMaximumBin();


  //first loop over all bins and create the error vector
  //We loop also over over/underflow bins, doesn;t matter, but easier when going to the 2d histogram,
  //don't need a second loop..
  for (int bin = 0; bin <= nBinsRebinned; ++bin) {
    //create the vectors to hold the error terms
    TVectorD vector(maxPar);
    TVectorD vecTrans(maxPar);

    double centralValue(histoVector[0+offSet]->GetBinContent(bin));

    for (int j = 1; j <= maxPar ;++j) {
      if(histoVector[offSet+j] && histoVector[offSet-j] ) {
	//vector[j-1] = histoVector[j]->GetBinContent(bin) - centralValue;
	vector[j-1] = (histoVector[offSet-j]->GetBinContent(bin) - histoVector[offSet+j]->GetBinContent(bin))*0.5;
	if( bin == checkThisBin ) { std::cout << vector[j-1]  << std::endl; }
      } else {
	vector[j-1] = 0.0;
      }
      vecTrans[j-1] = vector[j-1];
    }//for all parameters;

    if( bin == checkThisBin ) {
      //      std::cout << "Error Checking"  << std::endl;
      vector.Print();
    }
    //if( bin == checkThisBin ) vecTrans.Print();

    vector *= correlationMatrix;

    if( bin == checkThisBin ) vector.Print();

    double error = sqrt(vector*vecTrans);
    if( bin == checkThisBin ) std::cout << "Bin " << checkThisBin << "  " << centralValue << " +- " << error  << std::endl;
    hSqrts.SetBinContent(bin, centralValue);
    hSqrts.SetBinError(bin, error);

    hSqrtsPlus1.SetBinContent(bin, centralValue+error);
    hSqrtsMinus1.SetBinContent(bin, centralValue-error);

  }

//   const int nBins2D = Lumi.fN;
//   for (int bin = 0; bin <= nBins2D; ++bin) {
// #ifdef USE_RootStyle
//     RootStyle::PrintTreeProgress(bin, nBins2D);
// #endif

//     //create the vectors to hold the error terms
//     TVectorD vector(35);
//     TVectorD vecTrans(35);
//     double centralValue(valuevector[0+offSet]->GetProbability(bin));

//     for (int j = 1; j <= maxPar ;++j) {
//       if(valuevector[offSet+j] && valuevector[offSet-j]) {
// 	//vector[j-1] = valuevector[j]->GetProbability(bin) - centralValue;
// 	vector[j-1] = (valuevector[offSet+j]->GetProbability(bin) - valuevector[offSet-j]->GetProbability(bin))*0.5;
//       } else {
// 	vector[j-1] = 0.0;
//       }
//       vecTrans[j-1] = vector[j-1];
//     }//for all parameters;

//     vector *= correlationMatrix;
//     double error = sqrt(vector*vecTrans);

//     Lumi.SetBinContent(bin, centralValue);
//     Lumi.SetBinError(bin, error);

//     LumiPlus1. SetBinContent(bin, centralValue+error);
//     LumiMinus1.SetBinContent(bin, centralValue-error);

//   }
  //std::cout << "Closing Files"  << std::endl;
  // for (std::vector<TFile*>::iterator it = fileVector.begin(); it != fileVector.end(); ++it) {
  //   (*it)->Close();
  // }
  //fileVector.clear();


  //Get The original GP Spectrum
  //  TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/Lumi_GP_Double_Slices50_Random.root";
  //TString fileWithELoss  = Utility::GetWorkingDirectory()+"/RootFiles/DoubleLumiGP.root";
  TString fileWithELoss  = GPInputFile;
  //  int nbins(100);//30 GeV
  TH1D GPHisto  = DrawLumiSpectrum("GP", fileWithELoss, (xMin), xMax, 1000000, false, true, nBinsRebinned);


  ////////////////////////////////////////////////////////////////////////////////
  /// Calculate the width of every parameter and calculate the error on the width
  ////////////////////////////////////////////////////////////////////////////////
  const double rangeMin(0.99), rangeMax(1.006);
  TVectorD vectorPeak(maxPar);
  TVectorD vecPeakTrans(maxPar);
  double peakWidth (0.0);
  histoVector[offSet]->SetAxisRange(rangeMin, rangeMax, "X");
  peakWidth = histoVector[offSet]->GetRMS();
  std::cout << "Central Peak Width" << "  " << peakWidth  << std::endl;
  for (int j = 1; j <= maxPar ;++j) {
    if(histoVector[offSet+j] && histoVector[offSet-j]) {

      histoVector[offSet+j]->SetAxisRange(rangeMin, rangeMax,"X");
      histoVector[offSet-j]->SetAxisRange(rangeMin, rangeMax,"X");

      const double diff = (histoVector[offSet+j]->GetRMS() - histoVector[offSet-j]->GetRMS());;


      std::cout << std::setw(3)  <<(j)
		<< std::setw(13) <<  histoVector[offSet+j]->GetRMS() 
		<< std::setw(5)  << " - " 
		<< std::setw(13) <<  histoVector[offSet-j]->GetRMS() 
		<< std::setw(5)  << " = " 
		<< std::setw(13)  << diff
		<< std::endl;
      vectorPeak[j-1] = diff*0.5;
    } else {
      vectorPeak[j-1] = 0.0;
    }
    vecPeakTrans[j-1] = vectorPeak[j-1];
  }//for all parameters;
  
  vectorPeak.Print();

  vectorPeak *= correlationMatrix;
  double peakError = sqrt(vectorPeak*vecPeakTrans);
  std::cout << peakWidth << "+-" << peakError  << std::endl;

  //get the error for the lumi
  GPHisto.SetAxisRange(rangeMin, rangeMax, "X");
  std::cout << "GP Peak" << GPHisto.GetRMS() << "+-" << GPHisto.GetRMSError() << std::endl;




  newFile->cd();

  hSqrts.Write();
  hSqrtsPlus1.Write();
  hSqrtsMinus1.Write();

  // Lumi.Write();
  // LumiPlus1.Write();
  // LumiMinus1.Write();

  GPHisto.Write();

  TCanvas canv("canv","canv");
  hSqrts.SetLineColor(kGreen+2);
  hSqrtsPlus1.SetLineColor(kRed-7);
  hSqrtsMinus1.SetLineColor(kCyan+2);

  hSqrts.Scale( 1./hSqrts.Integral("width") );
  hSqrtsPlus1.Scale( 1./hSqrtsPlus1.Integral("width") );
  hSqrtsMinus1.Scale( 1./hSqrtsMinus1.Integral("width") );

  GPHisto.SetAxisRange(0.0, 1.1, "X");
  GPHisto.Draw();
  hSqrts.Draw("E0,same");
  hSqrtsPlus1.Draw("same");
  hSqrtsMinus1.Draw("same");
  canv.SetLogy();
  canv.SaveAs("sqrtsErrorInterval.eps");
  GPHisto.SetAxisRange(0.9, 1.1, "X");
  canv.SaveAs("sqrtsErrorInterval_09.eps");


  newFile->Close();
  return 0;
}



int parameterIDToTablePosition[] = {0,  1,  2,  5,  6, 11, 12, 19, 20, 23, 24,  3,  4,  9, 10, 15, 16, 17, 18,  7,  8, 13, 14, 21, 22, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34};

// ////Translation Table
// 0     0   
// 1     1   
// 2     2   
// 3     5   
// 4     6   
// 5    11   
// 6    12   
// 7    19   
// 8    20   
// 9    23   
// 10   24   
// 11    3   
// 12    4   
// 13    9   
// 14   10   
// 15   15   
// 16   16   
// 17   17   
// 18   18   
// 19    7   
// 20    8   
// 21   13   
// 22   14   
// 23   21   
// 24   22   
// 25   25   
// 26   26   
// 27   27      
// 28   28   
// 29   29   
// 30   30   
// 31   31   
// 32   32   
// 33   33   
// 34   34   

int getPos(int pos) {  
  if( pos >= 36 ) {
    throw std::out_of_range( "parameter too large ");
  }
  return parameterIDToTablePosition[pos]; 
}

void PrintMatrixLatex(  TMatrixTSym<double> const& matrix) {
  const int precision = 2;
  std::cout.precision(precision);
  for (int i = 0; i < matrix.GetNrows() ;++i) {
    bool printed = false;
    int locI = getPos(i);
    for (int j = 0; j < matrix.GetNcols() ;++j) {
      int locJ = getPos(j);
      if( matrix[locI][locJ] != 0.0) {
	printed = true;
	double value (matrix[locI][locJ]);
	if(fabs(value) < 0.005 ||  i > j ) {
	  std::cout << "     " ;
	} else {
	  std::cout << ((value > 0) ? " " : "")
		    << std::fixed  << value;
	}
	if (j != matrix.GetNcols() - 1) {
	  std::cout << " & ";
	}
      }
    }//all collumns
    if(printed) std::cout << " \\\\"  << std::endl;
    }

  std::cout.precision(7);

}
