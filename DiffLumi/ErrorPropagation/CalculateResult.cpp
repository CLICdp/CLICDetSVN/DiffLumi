#include <TCanvas.h>
#include <TFile.h>
#include <TH2D.h>
#include <TMatrixTSym.h>
#include <TString.h>
#include <TVectorD.h>
#include <TTree.h>

#include <iomanip>
#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <stdexcept>

typedef std::vector<double> DoubleVec;

void ReadFitResult(std::string filename, double* parameter, double* parError, TMatrixTSym<double>* matrix);
void CalculateError( std::string& ParameterFilename, double centralValue, DoubleVec const& inputValues);


int toGlobalParameterNumber(int i) { 
  const int kpar[19] = {1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 16, 17, 18, 19, 20, 21, 24, 25};
  //                    1  2  3  4  5  6  7   8   9  10  11  12  13  14  15  16  17  18  19
  //                    0  1  2  3  4  5  6   9  10  11  12  15  16  17  18  19  20  23  24
  static const std::vector<int> values(kpar, kpar+19);
  return values.at(i);
}

int main (int argc, char** argv) {

  if( argc != 22 && argc != 41 ) {
    std::cerr << "Not enough parameters given " << argc  << "... abort\n"
	      << "CalculateFinalHistograms FitResult.root <nominalValue> <19 other values> [19 other values]"
	      << std::endl;
    return 1;
  }

  std::string ParameterFilename(argv[1]);

  double centralValue( std::atof(argv[2]));
  DoubleVec inputValues(argc-3, 0.0);
  for(int i = 3 ; i < argc; i++) {
    inputValues.at(i-3) = atof(argv[i]);
  }
 
  CalculateError(ParameterFilename, centralValue, inputValues);

  return 0;   
}

void CalculateError( std::string& ParameterFilename, double centralValue, DoubleVec const& inputValues) {
  const int maxPar = 35;
  const unsigned int changeMaxPar = 19;
  
  TMatrixTSym<double> correlationMatrix(maxPar);
  double parameter[maxPar],parError[maxPar];
  ReadFitResult(ParameterFilename, parameter, parError, &correlationMatrix);

  TVectorD vector(maxPar);
  TVectorD vecTrans(maxPar);

  for (int i = 0; i < maxPar;++i) {
    vector[i] = 0.0;
    vecTrans[i] = 0.0;
  }
 

  for (unsigned int j = 0; j < changeMaxPar ; j+=1) {
    try {

      int global = toGlobalParameterNumber(j) - 1;
      if(inputValues.size() == changeMaxPar) {
	std::cout << inputValues.at(j) << std::endl;
	vector[global] = inputValues.at(j) - centralValue;
      } else {
	std::cout << inputValues.at(j) << "   " <<  inputValues.at(j+changeMaxPar) << std::endl;
	vector[global] = (inputValues.at(j) - inputValues.at(j+changeMaxPar))/2;
      }
      vecTrans[global] = vector[global];

    } catch(std::out_of_range& e) {
      std::cout << e.what()  << std::endl;
      std::cout << "Tried to acces: " << j  << std::endl;
      std::cout << inputValues.size()  << std::endl;
    }

    //vector[global] = inputValues[j];

  }//for all parameters;

  vector.Print();
  vector *= correlationMatrix;
  double error = sqrt(vector*vecTrans);

  std::cout << "Final Result: " << centralValue << " +- " << error  << std::endl;

  return;
}


void ReadFitResult(std::string filename, double* parameter, double* parError, TMatrixTSym<double>* matrix) {
 
  TFile* file = TFile::Open(filename.c_str());
  if (not file) {
    std::cerr << "File not found: " << filename  << std::endl;
    exit(1);
  }

  TTree* fitres = NULL;
  file ->GetObject("FitResult",fitres);
  if (not fitres){
    std::cerr << "FitResult tree was not found in "<< filename <<", cannot continue." << std::endl;
    exit(1);
  }

  Int_t     n_params;
  Double_t  Param[36][2]; //36??? why not 35?
  fitres->SetBranchAddress("n_params", &n_params);
  fitres->SetBranchAddress("Param", Param);
  fitres->GetEntry(0);
  for (int i =0; i<n_params;++i){
    parameter[i] = Param[i][0];
    if (parError) parError[i] = Param[i][1];
  }

  if(matrix) {
    std::cout << "Getting Matrix"  << std::endl;
    TMatrixTSym<double> *localMatrix;
    file->GetObject("TMatrixTSym<double>", localMatrix);
    matrix->SetMatrixArray(localMatrix->GetMatrixArray());
  }

  file->Close();
  delete file;
  return;
}
