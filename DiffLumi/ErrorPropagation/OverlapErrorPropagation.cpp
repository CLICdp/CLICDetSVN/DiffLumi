#include "ErrorPropagationOverlap.hh"
//#include "ErrorPropagation.hh"

#include "BetaFunction.h"
#include "BetaFunctionBS.h"
#include "Utilities.h"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TCanvas.h>
#include <TFile.h>
#include <TH2D.h>
#include <TString.h>

#include <iostream>
#include <iomanip>

int main (int argc, char** argv) {

#ifdef USE_RootStyle
  RootStyle::SetCDRStyle2();
#endif

  if(argc < 3) {
    std::cerr << "No filename or parameter number given... abort\n"
	      << std::endl;
    return 1;
  }
  std::string filename(argv[1]);
  int ParameterInput (atoi(argv[2]));
  const int nPar = abs(ParameterInput) - 1;

  const int maxPar = 35;
  if (nPar >= maxPar){
    std::cout<<"You have asked for a parameter that does not exist!"<<std::endl;
    return 1;
  }
  double parameter[maxPar],parError[maxPar], parameterCurrent[maxPar];
  int validFit = Utility::ReadFitResult(filename, parameter, parError);
  if ( validFit != 0) {
    std::cout << "ERROR: Fit not valid"  << std::endl;
    return 1;
  }
 //  //Print Parameters
 //  for (int i = 0; i < maxPar ;++i) {
 //    std::cout << parameter[i]  << std::endl;
 // }

  //return 0;

  // -1 is the default cause, otherwise we change the parameter with the error,
  //  and if the Input is negative, we reduce the parameter;

  //for (int nPar = -1;  nPar < maxPar; ++nPar)
  if( nPar != -1  ) {
    if( parError[nPar] == 0.0) {
      std::cout << "Parameter " << nPar << " without error ... skipping"
		<< std::setw(15) << parameter[nPar]
		<< std::setw(15) << parError[nPar]
		<< std::endl;
      return 0;
    }
  }
  //This is necessary if one wants to loop over all paramters in one go
  for (int k = 0; k < maxPar; ++k) {
    parameterCurrent[k] = parameter[k];
  }
  //check if positive or negative
  if( nPar != -1 ) parameterCurrent[nPar] += (ParameterInput/abs(ParameterInput)) * 4 * parError[nPar];
  
  ErrorPropagationOverlap::CreateMyLumiFile(parameterCurrent, Form("%i",ParameterInput), 3000, 300000000ull, 0.5, 1550.0/1500.0);
  return 0;
}
//parallel -j3 /data/sailer/Work/DiffLumi/Repository/DiffLumi/build471Root534/ErrorPropagation/OverlapErrorPropagation BHWide_Overlap_final.root ::: {-35..35}
