#include "ErrorPropagation.hh"

#include <TFile.h>
#include <TH1D.h>
#include <TString.h>
#include <TVectorD.h>



ErrorPropagation::Values::Values(TString const& filename):
  file(TFile::Open(filename)),
  x1ValuesPeak(static_cast<TH1D*>(file->Get("x1ValuesPeak"))),
  x2ValuesPeak(static_cast<TH1D*>(file->Get("x2ValuesPeak"))),
  x1ValuesArm1(static_cast<TH1D*>(file->Get("x1ValuesArm1"))),
  x2ValuesArm1(static_cast<TH1D*>(file->Get("x2ValuesArm1"))),
  x1ValuesArm2(static_cast<TH1D*>(file->Get("x1ValuesArm2"))),
  x2ValuesArm2(static_cast<TH1D*>(file->Get("x2ValuesArm2"))),
  x1ValuesBody(static_cast<TH1D*>(file->Get("x1ValuesBody"))),
  x2ValuesBody(static_cast<TH1D*>(file->Get("x2ValuesBody"))),
  parVector(static_cast<TVectorD*>(file->Get("parVector"))),
  aPeak((*parVector)(0)), aArm1((*parVector)(0)), aArm2((*parVector)(2)), aBody(1-aPeak-aArm1-aArm2),
  nBins(x1ValuesPeak->GetNbinsX())
{
  if(NULL == file || not x1ValuesPeak ){
    std::cout << "File was not opened or histogram not found"  << std::endl;
    exit(1);
  }

}

ErrorPropagation::Values::Values(TFile* filehandle):
  file(filehandle),
  x1ValuesPeak(dynamic_cast<TH1D*>(file->Get("x1ValuesPeak"))),
  x2ValuesPeak(dynamic_cast<TH1D*>(file->Get("x2ValuesPeak"))),
  x1ValuesArm1(dynamic_cast<TH1D*>(file->Get("x1ValuesArm1"))),
  x2ValuesArm1(dynamic_cast<TH1D*>(file->Get("x2ValuesArm1"))),
  x1ValuesArm2(dynamic_cast<TH1D*>(file->Get("x1ValuesArm2"))),
  x2ValuesArm2(dynamic_cast<TH1D*>(file->Get("x2ValuesArm2"))),
  x1ValuesBody(dynamic_cast<TH1D*>(file->Get("x1ValuesBody"))),
  x2ValuesBody(dynamic_cast<TH1D*>(file->Get("x2ValuesBody"))),
  parVector(dynamic_cast<TVectorD*>(file->Get("parVector"))),
  aPeak((*parVector)(0)), aArm1((*parVector)(0)), aArm2((*parVector)(2)), aBody(1-aPeak-aArm1-aArm2)
{
  if (x1ValuesPeak !=0 ){
    nBins = x1ValuesPeak->GetNbinsX();
  } else {
    std::cout << "Make sure the input file contains the right histograms." <<std::endl;
    exit(1);
  }
  
}


ErrorPropagation::Values::~Values(){
  file->Close();
}

int ErrorPropagation::Values::GetNbins() const {
  return nBins;
}


double ErrorPropagation::Values::GetProbability(double x1, double x2) const {
  return GetProbability(x1ValuesPeak->FindFixBin(x1), x1ValuesPeak->FindFixBin(x2));
}

double ErrorPropagation::Values::GetProbability(int binglobal) const {
  int binx = binglobal%(nBins+2);
  int biny = ((binglobal-binx)/(nBins+2))%(nBins+2);
  return GetProbability(binx, biny);
}
double ErrorPropagation::Values::GetProbability(int i, int j) const {
  return aPeak  * x1ValuesPeak->GetBinContent(i) * x2ValuesPeak->GetBinContent(j)
       + aArm1  * x1ValuesArm1->GetBinContent(i) * x2ValuesArm1->GetBinContent(j)
       + aArm2  * x1ValuesArm2->GetBinContent(i) * x2ValuesArm2->GetBinContent(j)
       + aBody  * x1ValuesBody->GetBinContent(i) * x2ValuesBody->GetBinContent(j);
}
