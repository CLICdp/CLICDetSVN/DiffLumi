// $Id: $
// Include files 

// local
#include "BetaFunctionBS.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BetaFunctionBS
//
// 2011-10-11 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BetaFunctionBS::BetaFunctionBS(double p1, double p2, double pMin, double pMax) : BetaFunction(p1, p2),
										 m_min(pMin), m_max(pMax),m_diff(m_max-m_min) {
  BetaLimit = 1.0;
  this->SetNormalization();
  m_normalization /= m_diff;
}
//=============================================================================
// Destructor
//=============================================================================
BetaFunctionBS::~BetaFunctionBS() {} 

//=========================================================================
//  
//=========================================================================
