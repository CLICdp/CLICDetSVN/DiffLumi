//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun 27 13:52:18 2012 by ROOT version 5.30/00
// from TTree MyTree/MyTree
// found on file: /afs/cern.ch/work/s/sposs/public/BHWide_MC_no_beam_spread_body.root
//////////////////////////////////////////////////////////

#ifndef MCTupleBHWide_h
#define MCTupleBHWide_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

/** \class MCTupleBHWide
    \brief Class holding the MyTree definition
 */
class MCTupleBHWide {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        eb1;
   Double_t        eb2;
   Double_t        deb1;
   Double_t        deb2;
   Double_t        deb1_g;
   Double_t        deb2_g;
   Double_t        E1;
   Double_t        E2;
   Double_t        px1;
   Double_t        py1;
   Double_t        pz1;
   Double_t        e1;
   Double_t        px2;
   Double_t        py2;
   Double_t        pz2;
   Double_t        e2;
   Int_t           nphotons;
   Double_t        photdata[12][4];   //[nphotons]
   Double_t        RelativeProbability;

   // List of branches
   TBranch        *b_eb1;   //!<
   TBranch        *b_eb2;   //!<
   TBranch        *b_deb1;   //!<
   TBranch        *b_deb2;   //!<
   TBranch        *b_deb1_g;   //!<
   TBranch        *b_deb2_g;   //!<
   TBranch        *b_E1;   //!<
   TBranch        *b_E2;   //!<
   TBranch        *b_px1;   //!<
   TBranch        *b_py1;   //!<
   TBranch        *b_pz1;   //!<
   TBranch        *b_e1;   //!<
   TBranch        *b_px2;   //!<
   TBranch        *b_py2;   //!<
   TBranch        *b_pz2;   //!<
   TBranch        *b_e2;   //!<
   TBranch        *b_nphotons;   //!<
   TBranch        *b_photdata;   //!<
   TBranch        *b_RelativeProbability;   //!<

   /**Constructor*/
   MCTupleBHWide(TTree *tree=0);
   virtual ~MCTupleBHWide();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MCTupleBHWide_cxx
MCTupleBHWide::MCTupleBHWide(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/work/s/sposs/public/BHWide_MC_no_beam_spread_body.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/afs/cern.ch/work/s/sposs/public/BHWide_MC_no_beam_spread_body.root");
      }
      f->GetObject("MyTree",tree);

   }
   Init(tree);
}

MCTupleBHWide::~MCTupleBHWide()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MCTupleBHWide::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MCTupleBHWide::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MCTupleBHWide::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eb1", &eb1, &b_eb1);
   fChain->SetBranchAddress("eb2", &eb2, &b_eb2);
   fChain->SetBranchAddress("deb1", &deb1, &b_deb1);
   fChain->SetBranchAddress("deb2", &deb2, &b_deb2);
   fChain->SetBranchAddress("deb1_g", &deb1_g, &b_deb1_g);
   fChain->SetBranchAddress("deb2_g", &deb2_g, &b_deb2_g);
   fChain->SetBranchAddress("E1", &E1, &b_E1);
   fChain->SetBranchAddress("E2", &E2, &b_E2);
   fChain->SetBranchAddress("px1", &px1, &b_px1);
   fChain->SetBranchAddress("py1", &py1, &b_py1);
   fChain->SetBranchAddress("pz1", &pz1, &b_pz1);
   fChain->SetBranchAddress("e1", &e1, &b_e1);
   fChain->SetBranchAddress("px2", &px2, &b_px2);
   fChain->SetBranchAddress("py2", &py2, &b_py2);
   fChain->SetBranchAddress("pz2", &pz2, &b_pz2);
   fChain->SetBranchAddress("e2", &e2, &b_e2);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("photdata", photdata, &b_photdata);
   fChain->SetBranchAddress("RelativeProbability", &RelativeProbability, &b_RelativeProbability);
   Notify();
}

Bool_t MCTupleBHWide::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MCTupleBHWide::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
#endif // #ifdef MCTupleBHWide_cxx
