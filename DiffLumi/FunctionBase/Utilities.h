#ifndef UTILITIES_H 
#define UTILITIES_H 1
#include <TMath.h>
#include <Minuit2/Minuit2Minimizer.h>

#include <Math/WrappedTF1.h>
#include <Math/GSLIntegrator.h>
#include <TGraphErrors.h>
#include <TVirtualFitter.h>
#include <TF1.h>
#include <TMatrixTSym.h>


#include <string>

double effective_sqrts(double th1,double th2);

//This is necessary to turn the directory into a string, directly doesn't work
#define MACRO_STRINGIFY(x) #x
#define MACRO_TOSTRING(x) MACRO_STRINGIFY(x)

namespace Utility {

  enum VariableType { kUnlimited, kFixed, kLowerLimit, kUpperLimit, kLimited };

  void SetVariable(ROOT::Minuit2::Minuit2Minimizer &minimizer, VariableType type, int iVar, 
		   std::string name, double value, double step, double lowLimit, double upLimit);

  //Note the user is responsible for creating the memory of all objects given as pointers!
  int ReadFitResult(std::string filename, double* parameter2d, double* parError=NULL, TMatrixTSym<double>* matrix=NULL);

  void ReadParameters(std::string const& filename, double* parameter2d);

  inline std::string GetWorkingDirectory() {
    return std::string( MACRO_TOSTRING(WORKING_DIRECTORY) );
  }


  inline Double_t Integrate(TF1 * function, Double_t x1, Double_t x2, Double_t precision){
    // Wrap the function
    ROOT::Math::WrappedTF1 wconvo(*function);
    // Create the Integrator
    ROOT::Math::GSLIntegrator ig2(ROOT::Math::IntegrationOneDim::kADAPTIVE);
    // Set parameters of the integration
    ig2.SetFunction(wconvo);
    ig2.SetRelTolerance(precision);
    return ig2.Integral(x1, x2);
  }//Integrate

  //Adapted from $ROOTSYS/tutorials/fit/ConfidenceIntervals.C
  TGraphErrors* DrawConfidenceInterval(TF1& function, double confidenceLevel=0.9999);

}//namespace



#endif
