// $Id: $
#ifndef FUNCTIONBASE_BETAFUNCTIONBS_H 
#define FUNCTIONBASE_BETAFUNCTIONBS_H 1

// Include files
#include "BetaFunction.h"

#include <TMath.h>


/** \class BetaFunctionBS 
    \brief Beta function for beamstrahlung
 *  @author Stephane Poss
 *  @date   2011-10-11
 */
class BetaFunctionBS : public BetaFunction {
public: 
  /** Standard constructor*/
  BetaFunctionBS(double p1, double p2, double pMin, double pMax);
  virtual ~BetaFunctionBS( ); 

  /**Get the function value for a given x*/
  double operator()(double x) const ;
  /**Get the function value for a given x*/
  double operator()(double *x, double*) const ;
  /**Get the function value for a given x*/
  double evaluate(double x) const;
  /**Get a random value according to the function*/
  double GetRandom() const;

  /**Replace the parameters' values*/
  inline void ChangeParameters(const double* p){
    m_p1=p[0];
    m_p2=p[1];
    m_min=p[2];
    m_max=p[3];
    m_diff = m_max - m_min;
    SetNormalization();
    m_normalization /= m_diff;
  };
  
protected:


private:
  double m_min, m_max, m_diff;

};

inline double BetaFunctionBS::operator()(double x) const {
  return evaluate(x);
}

inline double BetaFunctionBS::operator()(double *x, double*) const {
  return evaluate(x[0]);
}

//=========================================================================
//  
//=========================================================================

inline double BetaFunctionBS::evaluate (double x ) const {
  double t = ( x - m_min) / ( m_diff );
  if ( 0 <= t && t < 1 ) {
    return m_normalization * pow( t, m_p1 ) * pow( 1. - t, m_p2);
  }
  return 0;
}


inline double BetaFunctionBS::GetRandom() const {
  double t = ROOT::Math::beta_quantile(gRandom->Uniform(1.),
                                       1.+m_p1,
                                       1.+m_p2 );
  return m_min + t * (m_diff);
}

#endif // FUNCTIONBASE_BETAFUNCTIONBS_H
