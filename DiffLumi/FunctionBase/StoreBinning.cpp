#include "StoreBinning.hh"

#ifdef USE_RootStyle
#include <RootStyle.hh>
#endif

#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TObjString.h>
#include <TCanvas.h>

#include <TGeoManager.h>
#include <TGeoVolume.h>
#include <TGeoMatrix.h>


StoreBinning::StoreBinning(TString filename):
  m_nBins(0),
  m_bins(),
  m_xBins(0),
  m_yBins(0),
  m_zBins(0) {
  TFile *file = TFile::Open(filename);

  TObjString* NumberXBins;
  TObjString* NumberYBins;
  TObjString* NumberZBins;

  file->GetObject("xBins",NumberXBins);
  file->GetObject("yBins",NumberYBins);
  file->GetObject("zBins",NumberZBins);

  TTree *tree;
  file->GetObject("binTree",tree);

  m_nBins = tree->GetEntries();
  m_xBins = (NumberXBins->GetString()).Atoi();
  m_yBins = (NumberYBins->GetString()).Atoi();
  m_zBins = (NumberZBins->GetString()).Atoi();

  double x, y, z;
  tree->SetBranchAddress("x", &x);
  tree->SetBranchAddress("y", &y);
  tree->SetBranchAddress("z", &z);

  m_bins.resize(tree->GetEntries());

  for (int i = 0; i < tree->GetEntries() ;++i) {
    tree->GetEntry(i);
    m_bins.at(i) = Bin(x, y, z);
  }

  file->Close();
  delete file;

}//ctor

void StoreBinning::SetBinBoundaries(int global, Bin bin) {
  m_bins.at(global) = bin;
}

void StoreBinning::SetBinBoundaries(int global, double x, double y, double z) {
  SetBinBoundaries(global, Bin(x, y, z));
}


void StoreBinning::SetBinBoundaries(int i, int j, int k, Bin bin) {
  SetBinBoundaries(GetGlobalBinNumber (i, j, k), bin);
}

void StoreBinning::SetBinBoundaries(int i, int j, int k, double x, double y, double z) {
  SetBinBoundaries(GetGlobalBinNumber(i, j, k), Bin(x, y, z));
}


void StoreBinning::SetBinBoundaries(int i, int j, Bin bin) {
  SetBinBoundaries(GetGlobalBinNumber (i, j, 0), bin);
}

void StoreBinning::SetBinBoundaries(int i, int j, double x, double y) {
  SetBinBoundaries(GetGlobalBinNumber(i, j, 0), Bin(x, y, 1.0));
}


void StoreBinning::GetLocalBinNumber(int global, int& i, int& j, int& k) const {
  k = global % m_zBins;
  j = (( global - k ) / m_zBins) % m_yBins;
  i = ((( global - k ) / m_zBins) - j ) / m_yBins;
  //Check if we found the right bin
  if ( global != GetGlobalBinNumber(i, j, k)) {
    std::cerr << "Did not find the right bin " 
	      << std::setw(10) << global
	      << std::setw(10) << GetGlobalBinNumber(i, j, k)
	      << std::setw(10) << i
	      << std::setw(10) << j
	      << std::setw(10) << k
	      << std::endl;
    exit(1);

  } 
  return;
}

Bin StoreBinning::GetBin(int globalBin) const {
  return m_bins.at(globalBin);
}
Bin StoreBinning::GetBin(int i, int j, int k) const {
  return GetBin(GetGlobalBinNumber(i, j, k));
}

Bin StoreBinning::FindLowEdgeBin(int globalBin) const {
  int i=0, j=0, k=0;
  GetLocalBinNumber(globalBin, i, j, k);
  return FindLowEdgeBin(i, j, k);
}

Bin StoreBinning::FindLowEdgeBin(int i, int j, int k) const {

  double xLow(0.0), yLow(0.0), zLow(0.0);
  if( k != 0) { zLow = m_bins[GetGlobalBinNumber(i, j, k-1)].getZ(); }
  if( j != 0) { yLow = m_bins[GetGlobalBinNumber(i, j-1, k)].getY(); }
  if( i != 0) { xLow = m_bins[GetGlobalBinNumber(i-1, j, k)].getX(); }

  return Bin(xLow, yLow, zLow);
}

int StoreBinning::GetBinNumber2(double x, double y, double z) const {
  Bin bin(x, y, z);
  //  BinVector::const_iterator upperBound( std::upper_bound( m_bins.begin(), m_bins.end(), bin));
  //  BinVector::const_iterator lowerBound( std::lower_bound( upperBound+1, m_bins.end(), bin));
  //double distance(1e10);
  double memXDiff(1e10), memYDiff(1e10), memZDiff(1e10);
  BinVector::const_iterator upperBound = m_bins.begin();
  for (BinVector::const_iterator it = m_bins.begin(); it != m_bins.end(); ++it) {
    //Calculate absolute distance, giving use rectangular boxes for our bins...
    const Bin& itBin= *it;
    double xDiff = itBin.getX() - bin.getX(); if( xDiff < 0 ) continue;
    double yDiff = itBin.getY() - bin.getY(); if( yDiff < 0 ) continue;
    double zDiff = itBin.getZ() - bin.getZ(); if( zDiff < 0 ) continue;

    //    double thisDist = xDiff+yDiff+zDiff;
    if (xDiff < memXDiff && yDiff < memYDiff && zDiff < memZDiff) {
      upperBound = it;
      //distance = thisDist;
      memXDiff = xDiff;
      memYDiff = yDiff;
      memZDiff = zDiff;
    }

  }
#ifdef DEBUG
  std::cout << "we found this bin " << *upperBound  << std::endl;
#endif
  //  std::cout << "we found this bin " << *lowerBound  << std::endl;
  // if( *upperBound < bin )
  //   return lowerBound - m_bins.begin();
  // else 
  int globalBinNumber = upperBound - m_bins.begin();
  int GBN2 = GetBinNumber(x, y, z);
  if(globalBinNumber != GBN2) {
    std::cout << std::setw(25) << "GBN  failed    "
	      << std::setw(15) << std::fixed << x
	      << std::setw(15) << std::fixed << y
	      << std::setw(15) << std::fixed << z << std::endl
	      << std::setw(25) << globalBinNumber
	      << std::setw(15) << std::fixed << (*upperBound).getX()
	      << std::setw(15) << std::fixed << (*upperBound).getY()
	      << std::setw(15) << std::fixed << (*upperBound).getZ() << std::endl
	      << std::setw(25) << GBN2
	      << std::setw(15) << std::fixed << m_bins[GBN2].getX()
	      << std::setw(15) << std::fixed << m_bins[GBN2].getY()
	      << std::setw(15) << std::fixed << m_bins[GBN2].getZ()
	      << std::endl;
    exit(1);
  } else {
    // std::cout << std::setw(25) << "GBN  worked    "
    // 	      << std::setw(15) << x
    // 	      << std::setw(15) << y
    // 	      << std::setw(15) << z << std::endl
    // 	      << std::setw(25) << globalBinNumber
    // 	      << std::setw(15) << (*upperBound).getX()
    // 	      << std::setw(15) << (*upperBound).getY()
    // 	      << std::setw(15) << (*upperBound).getZ() << std::endl
    // 	      << std::setw(25) << GBN2
    // 	      << std::setw(15) << m_bins[GBN2].getX()
    // 	      << std::setw(15) << m_bins[GBN2].getY()
    // 	      << std::setw(15) << m_bins[GBN2].getZ()
    // 	      << std::endl;

  }
  return globalBinNumber;
}

StoreBinning::BinVector::const_iterator StoreBinning::GetBin1D(double value, BinVector::const_iterator first, BinVector::const_iterator last, int stepDistance, int coord) const {
  //Binary Serach algorithm from www.cplusplus.com reference
  BinVector::const_iterator it = m_bins.begin();
  int count(0), step(0);
  count = distance(first,last)/stepDistance;

  // std::cout << std::endl << " Index " << coord
  // 	    << " count " << count
  // 	    << " stepD " << stepDistance
  // 	    << std::endl;
  // int counter = 0;

  //  while ( count > 0 && counter < 20) {
  while ( count > 0 ) {

    // std::cout << " First " << std::setw(5) << first - m_bins.begin()
    // 	      << " Last  " << std::setw(5) << last  - m_bins.begin()
    // 	      << " count " << std::setw(5) << count
    // 	      << " Step  " << std::setw(5) << step
    // 	      << " StepD " << std::setw(5) << stepDistance
    // 	      << std::setw(15) << std::fixed << value << " <? " << std::fixed << (*it)[coord];

    //counter++;
    it = first; 
    step=int(count/2); 
    it+=stepDistance*(int)(step);

    if (!( value <= (*it)[coord])) {
      //      std::cout << "  Larger ";
      first = it+stepDistance; 
      count-=step+1;  
    } else {
      //      std::cout << "  smaller ";
      count=step;
    }

    // std::cout << "  ---  using " 
    // 	      << std::setw(7) << first - m_bins.begin()
    // 	      << std::setw(15) << std::fixed<< (*first)[0]
    // 	      << std::setw(15) << std::fixed<< (*first)[1]
    // 	      << std::setw(15) << std::fixed<< (*first)[2]
    // 	      << std::endl;

  }

  //  std::cout << "Found index " << (first - m_bins.begin())  << std::endl;
  return first;
}


int StoreBinning::GetBinNumber(double x, double y, double z) const {
  BinVector::const_iterator first = m_bins.begin();
  BinVector::const_iterator last = m_bins.end();
  
  //Find the range in X
  BinVector::const_iterator xFirst(GetBin1D(x, first,  last,   m_yBins*m_zBins, 0));
  //Find the range in Y
  BinVector::const_iterator yFirst(GetBin1D(y, xFirst, xFirst + m_yBins*m_zBins, m_zBins, 1));
  //Find the bin in Z
  BinVector::const_iterator final (GetBin1D(z, yFirst, yFirst + m_zBins, 1, 2));
  
#ifdef DEBUG
  std::cout << "we found this bin " << *final  << std::endl;
#endif
  //  std::cout << "we found this bin " << *lowerBound  << std::endl;
  // if( *upperBound < bin )
  //   return lowerBound - m_bins.begin();
  // else
  int globalBinNumber = final - m_bins.begin();
  return globalBinNumber;
}


int StoreBinning::GetBinNumber(const Bin&  bin) const {
  return GetBinNumber(bin.getX(), bin.getY(), bin.getZ());
}


int StoreBinning::GetGlobalBinNumber(int i, int j, int k) const {

  //globalBinNumber has to be the same as the ordering we have on our Bin structure!
  //which means
  //first we count

  //we have bin in x
  //for every bin in y, we have xBins bins
  //for every bin in z we have m_xBins* m_yBins
  //  int globalBinNumber = i + j * m_xBins + k * m_xBins  * m_yBins;
  int globalBinNumber = i * m_yBins *m_zBins + j * m_zBins + k;

  return globalBinNumber;
}


/***
 * Write the stored bins into a root tree and write to filename.
 */
void StoreBinning::SaveToFile(TString filename) const {

  TString xStr, yStr, zStr;

  xStr += m_xBins;
  yStr += m_yBins;
  zStr += m_zBins; 

  TObjString* NumberXBins = new TObjString( xStr );
  TObjString* NumberYBins = new TObjString( yStr );
  TObjString* NumberZBins = new TObjString( zStr );

  TFile *file = TFile::Open(filename , "RECREATE");
  TTree *binTree = new TTree ("binTree","binTree");
  double x, y, z;
  binTree->Branch("x",&x,"x/D");
  binTree->Branch("y",&y,"y/D");
  binTree->Branch("z",&z,"z/D");

  NumberXBins->Write("xBins");
  NumberYBins->Write("yBins");
  NumberZBins->Write("zBins");
  
  for (BinVector::const_iterator it = m_bins.begin(); it != m_bins.end(); ++it) {
    x = (*it).getX();
    y = (*it).getY();
    z = (*it).getZ();
    binTree->Fill();
  }
 
  file->Write();
  file->Close();

  delete NumberXBins;
  delete NumberYBins;
  delete NumberZBins;

  return;

}


void StoreBinning::CreateTGeoFile(TString filename) const {

#ifdef USE_RootStyle
  RootStyle::MyColor colors;
#endif

  TFile *storeFile = TFile::Open(filename, "RECREATE");

  TGeoManager geoManager("world", "store Some Boxes here");
  geoManager.SetVerboseLevel(0);
  TGeoMaterial vacuumMat("Vacuum", 0, 0, 0 );
  TGeoMedium vacuumMed("Vacuum", 1, &vacuumMat);
  double binSizeX(m_bins.back().getX());
  double binSizeY(m_bins.back().getY());
  double binSizeZ(m_bins.back().getZ());

  TGeoVolume *top =  geoManager.MakeBox("Top", &vacuumMed,
					binSizeX,
					binSizeY,
					binSizeZ);
  geoManager.SetTopVolume(top);

  TCanvas geoCanv("geoCanv", "geoCanv");

  for (int i = 0; i < m_nBins;++i) {
    double xBin = m_bins[i].getX();
    double yBin = m_bins[i].getY();
    double zBin = m_bins[i].getZ();

    const Bin& BinLowEdge = FindLowEdgeBin(i);

    double xHalfLength((xBin-BinLowEdge.getX())/2.0);
    double yHalfLength((yBin-BinLowEdge.getY())/2.0);
    double zHalfLength((zBin-BinLowEdge.getZ())/2.0);

    TGeoVolume *daughterBox = geoManager.MakeBox(Form("Bin%i",i),&vacuumMed, 
						 xHalfLength, 
						 yHalfLength, 
						 zHalfLength);

    // std::cout << "Lower Corner " << BinLowEdge  << std::endl;
    // std::cout << "Upper Corner " << m_bins[i]  << std::endl;
#ifdef USE_RootStyle
    daughterBox->SetLineColor(colors.GetColor());
#else
    daughterBox->SetLineColor(kRed+(i%10)-5);
#endif

    daughterBox->SetFillColor(daughterBox->GetLineColor());

    top->AddNode(daughterBox, i, new TGeoTranslation(BinLowEdge.getX()+xHalfLength,
						     BinLowEdge.getY()+yHalfLength,
						     BinLowEdge.getZ()+zHalfLength));
  }

  geoManager.GetTopVolume()->Draw();

  geoCanv.SaveAs("geoPicture.pdf");

  geoManager.CloseGeometry();
  
  geoManager.Write();
  storeFile->Close();
  delete storeFile;
  delete top;

  return;
}
