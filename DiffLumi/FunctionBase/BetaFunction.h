// $Id: $
#ifndef FUNCTIONBASE_BETAFUNCTION_H 
#define FUNCTIONBASE_BETAFUNCTION_H 1

// Include files
#include "IBaseFunction.h"

#include <TMath.h>

#include <TRandom3.h>
#include "Math/QuantFuncMathCore.h"

/** \class BetaFunction
 *  \brief Base Beta function 
 *  
 *
 *  @author Stephane Poss
 *  @date   2011-10-11
 */
class BetaFunction : public IBaseFunction {
public: 
  /** Sandard constructor*/
  BetaFunction(double p1, double p2); 

  virtual ~BetaFunction( ); ///< Destructor

  /**Get the function value at x*/
  double operator()(double x) const ;
  /**Get the function value at x*/
  double evaluate(double x) const;
  /**Get the function value at x*/
  double operator()(double *x, double*) const ;
  /**Get a random value according to x*/
  virtual double GetRandom() const;

  
  inline void ChangeParameters(const double* p){
    m_p1=p[0];
    m_p2=p[1];
    SetNormalization();
  };
  /**Get the beta limit*/
  inline double GetBetaLimit() const {return BetaLimit;}

  inline double GetNormalization() const {return m_normalization;}
  inline double GetP1() const {return m_p1;}
  inline double GetP2() const {return m_p2;}

  /**Set the beta limit*/
  inline void SetBetaLimit(double bl) { BetaLimit = bl; SetNormalization(); }

protected:
  void SetNormalization();
  double BetaLimit;//!< Upper limit until where to integrate 
  double m_p1;//!< Lower value of the function domain definition
  double m_p2;//!< Upper value of the function domain definition

private:

};

inline double BetaFunction::operator()(double *x, double*) const {
  return evaluate(x[0]);
}

//=========================================================================
//  
//=========================================================================
inline double BetaFunction::evaluate (double x ) const {
    return m_normalization * pow(x,m_p1) * pow(1.-x,m_p2);
}



inline void BetaFunction::SetNormalization () {
  m_normalization = 1./(TMath::Beta(1.+m_p1, 1.+m_p2)*TMath::BetaIncomplete(BetaLimit, 1.+m_p1, 1.+m_p2));
}


inline double BetaFunction::GetRandom() const {
  double x = 0;
  do{
    x = ROOT::Math::beta_quantile(gRandom->Uniform(1.),
				  1.+m_p1,
				  1.+m_p2 );
  } while (x > BetaLimit);
  return x;
}

#endif // FUNCTIONBASE_BETAFUNCTION_H
