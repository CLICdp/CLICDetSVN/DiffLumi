#ifndef Bin_hh
#define Bin_hh 1

#include <iomanip>
#include <iostream>
#include <ostream>

/**\class Bin
   \brief Class describing a bin
 */
class Bin {

public:
  /**Default contructor*/
  Bin() { coords[0] = 0.0; coords[1] = 0.0; coords[2] = 0.0; }
  /**Constructor using coordinates*/
  Bin(double X, double Y, double Z) {
    coords[0] = X; coords[1] = Y; coords[2] = Z;
  }
  /**Comparison operator*/
  Bin& operator= (const Bin& rhs) {
    this->coords[0] = rhs.getX();
    this->coords[1] = rhs.getY();
    this->coords[2] = rhs.getZ();
    return *this;
  }

  /**Get the x coordinate of the bin*/
  inline double getX() const { return coords[0]; }
  /**Get the y coordinate of the bin*/
  inline double getY() const { return coords[1]; }
  /**Get the z coordinate of the bin*/
  inline double getZ() const { return coords[2]; }

  /**Get the i-th coordinate of the bin*/
  double operator[](int index) const { return coords[index] ;}

  /**Compare if a bin is above of below, needed for the sorting*/
  bool operator< (const Bin& rhs) const {

    if(  this->getX() < rhs.getX() ) return true;
    else if(  this->getX() > rhs.getX() ) return false;

    if(  this->getY() < rhs.getY() ) return true;
    else if(  this->getY() > rhs.getY() ) return false;

    if(  this->getZ() < rhs.getZ() ) return true;
    else if(  this->getZ() > rhs.getZ() ) return false;

    return false;
  }

  /**Print a given bin*/
  friend std::ostream& operator<<(std::ostream & o, const Bin& rhs) {
    o << std::setw(10) << rhs.coords[0]
      << std::setw(10) << rhs.coords[1]
      << std::setw(10) << rhs.coords[2];
    return o;
  }

private:
  double coords[3];//!< Array of bin coordinates

} ;//class Bin

#endif // Bin_hh
