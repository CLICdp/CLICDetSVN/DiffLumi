// $Id: $
// Include files 



// local
#include "BetaFunction.h"
#include "TMath.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BetaFunction
//
// 2011-10-11 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BetaFunction::BetaFunction(double p1, double p2) :
  BetaLimit(0.999), m_p1(p1), m_p2(p2) {
  this->SetNormalization(); 
}
//=============================================================================
// Destructor
//=============================================================================
BetaFunction::~BetaFunction() {} 

//=========================================================================
//  
//=========================================================================
double BetaFunction::operator()(double x) const {
  //if( 0 < x && x <= BetaLimit) {
  return evaluate(x);
    //}
    //return 0;
}
