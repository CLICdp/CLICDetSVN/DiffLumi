#include "Utilities.h"

#include <TFile.h>
#include <TTree.h>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>

using namespace log4cplus;
using namespace log4cplus::helpers;


double effective_sqrts(double th1, double th2) {
  if((th2+th1) < TMath::Pi() ) { 
    th1 = TMath::Pi() - th1; 
    th2 = TMath::Pi() - th2; 
  }

  return TMath::Sqrt((TMath::Sin(th1)+TMath::Sin(th2)+TMath::Sin(th1+th2))/
                     (TMath::Sin(th1)+TMath::Sin(th2)-TMath::Sin(th1+th2)));
}

void Utility::SetVariable(ROOT::Minuit2::Minuit2Minimizer &minimizer, Utility::VariableType type, 
			  int iVar, std::string name, double value, double step, double lowLimit, double upLimit) {
  switch (type) {
  case kFixed:
    minimizer.SetFixedVariable(iVar, name, value);
    break;
  case kUnlimited:
    minimizer.SetVariable(iVar, name, value, step);
    break;
  case kLowerLimit:
    minimizer.SetLowerLimitedVariable(iVar , name, value, step, lowLimit);
    break;
  case kUpperLimit:
    minimizer.SetUpperLimitedVariable(iVar , name, value, step, upLimit);
    break;
  case kLimited:
    minimizer.SetLimitedVariable(iVar , name, value, step, lowLimit, upLimit);
    break;
  }
  return;
}


TGraphErrors* Utility::DrawConfidenceInterval(TF1& function, double confidenceLevel) {
  //Create a TGraphErrors to hold the confidence intervals
  const int ngr(1000);
  TGraphErrors *grint = new TGraphErrors(ngr);
  grint->SetTitle("Confidence Interval");
  double xmin, xmax;
  function.GetRange(xmin, xmax);
  double range(xmax-xmin);
  for (int i=0; i<=ngr; i++ ) {
    double x = xmin + range * double(i)/double(ngr);
    grint->SetPoint(i, x, 0);
  }
  //Compute the confidence intervals at the x points of the created graph
  (TVirtualFitter::GetFitter())->GetConfidenceIntervals(grint, confidenceLevel);
  //Now the "grint" graph contains function values as its y-coordinates
  //and confidence intervals as the errors on these coordinates
  //  grint->SetFillStyle(3144);
  grint->SetFillStyle(1001);
  return grint;
}//DrawConfidenceInterval


int Utility::ReadFitResult(std::string filename, double* parameter, double* parError, TMatrixTSym<double>* matrix) {
 
   TFile* file = TFile::Open(filename.c_str());
    if (not file) {
      LOG4CPLUS_FATAL(Logger::getRoot(),"Root file "<< filename <<" was not found!");
      exit(1);
    }

    TTree* fitres = NULL;
    file ->GetObject("FitResult",fitres);
    if (not fitres){
      LOG4CPLUS_FATAL(Logger::getRoot(),"FitResult tree was not found in "<< filename <<", cannot continue.");
      exit(1);
    }
    Int_t     n_params, status, covQual;
    Double_t  Param[36][2]; //36??? why not 35?
    fitres->SetBranchAddress("n_params", &n_params);
    fitres->SetBranchAddress("Param", Param);
    fitres->SetBranchAddress("status", &status);
    fitres->SetBranchAddress("covQual", &covQual);
    fitres->GetEntry(0);
    for (int i =0; i<n_params;++i){
      parameter[i] = Param[i][0];
      if (parError) parError[i] = Param[i][1];
    }

    if(status != 0 || covQual != 3 ) {
      std::cout << "ERROR: Fit failed, abort!"  << std::endl;
      file->Close();
      delete file;
      return 1;
    }

    if(matrix) {
      std::cout << "Getting Matrix"  << std::endl;
      TMatrixTSym<double> *localMatrix;
      file->GetObject("TMatrixTSym<double>", localMatrix);
      matrix->SetMatrixArray(localMatrix->GetMatrixArray());
    }

    file->Close();
    delete file;
    return 0;
}

void Utility::ReadParameters(std::string const& filename, double* parameter){
  TFile* file = TFile::Open(filename.c_str());
  if (not file) {
    LOG4CPLUS_FATAL(Logger::getRoot(),"Root file "<< filename <<" was not found!");
    exit(1);
  }
  TTree*init_params = NULL;
  file->GetObject("InitialParameters",init_params);
  if (!init_params){
    LOG4CPLUS_WARN(Logger::getRoot(),"File "<<filename<<" does not contain the InitialParameters tree");
    return;
  }
  Int_t n_params = init_params->GetEntries();
  double value = 0.;
  init_params->SetBranchAddress("Value",&value);
  for (Int_t i = 0; i < n_params; ++i){
    init_params->GetEntry(i);
    parameter[i] = value;
  }
  
}
