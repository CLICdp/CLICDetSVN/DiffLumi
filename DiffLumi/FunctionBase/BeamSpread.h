// $Id: $
#ifndef FUNCTIONBASE_BEAMSPREAD_H 
#define FUNCTIONBASE_BEAMSPREAD_H 1

// Include files
#include "IBaseFunction.h"

#include <TMath.h>

/** \class BeamSpread 
 *  \brief Beam energy spread
 *
 *  @author Stephane Poss
 *  @date   2011-10-11
 */
class BeamSpread : public IBaseFunction{
public: 
  /** Standard constructor*/
  BeamSpread(double p0, double p1, double p2, double p3, double p4 ); 
  ~BeamSpread( ); ///< Destructor

  double operator()(double x) const ;
  double evaluate(double x) const;
  double GetRandom() const { return -1;}
  
  inline void ChangeParameters(const double* p){
    m_dmin=p[0]; 
    m_dmax=p[1]; 
    m_p2=p[2]; 
    m_p3=p[3]; 
    m_p4=p[4];
    this->SetNormalization();
  }

protected:
  inline void SetNormalization();

private:
  double m_dmin, m_dmax, m_p2, m_p3, m_p4;
  
};
#endif // FUNCTIONBASE_BEAMSPREAD_H

inline void BeamSpread::SetNormalization() {
  m_normalization = m_p3/(m_p2*(TMath::SinH(m_p3*(m_p4-m_dmin))-TMath::SinH(m_p3*(m_p4-m_dmax)))-m_dmin+m_dmax);
}


inline double BeamSpread::operator()(double x) const { 
  return evaluate(x);
}

//=========================================================================
//  
//=========================================================================
inline double BeamSpread::evaluate (double x ) const {
  //cout<<"dmin "<<m_dmin<<" x "<<x<<"  dmax "<<m_dmax<<endl;
  if ( m_dmin < x && x <= m_dmax) {
    return m_normalization*(m_p2*TMath::CosH(m_p3*(x-m_p4))+1);
  }
  return 0;
}
