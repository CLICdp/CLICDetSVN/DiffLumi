// $Id: $
#ifndef FUNCTIONBASE_IBASEFUNCTION_H 
#define FUNCTIONBASE_IBASEFUNCTION_H 1

// Include files

/** \class IBaseFunction 
 *  \brief Base function class
 *
 *  @author Stephane Poss
 *  @date   2011-10-11
 */
class IBaseFunction {
public: 
  /** Standard constructor*/
  IBaseFunction( ); 

  virtual ~IBaseFunction( );
  /**Get the function normalization*/
  inline virtual double GetNormalization() const { return m_normalization;} ;
//  virtual double Evaluate(double x)=0;
  /**Get the function value for a given x*/
  virtual double operator()(double x) const {return evaluate(x);} ;
  /**Get a random value according to the function*/
  virtual double GetRandom() const = 0;
  /**Get the function value for a given x*/  
  virtual double evaluate(double x) const = 0;
  /**Change the parameters that define the function*/
  virtual void ChangeParameters(const double*) = 0;
  
protected:
  double m_normalization;//!< function normalisation
  /**Set the normalization*/
  inline virtual void SetNormalization() = 0;
  
private:
  
};
#endif // FUNCTIONBASE_IBASEFUNCTION_H
