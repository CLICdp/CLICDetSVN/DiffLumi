#ifndef STOREBINNING_HH
#define STOREBINNING_HH 1

#include "Bin.hh"

#include <TString.h>

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>


/** \class StoreBinning
    \brief Clas to store the binning
 */
class StoreBinning {     
  typedef std::vector<Bin> BinVector;
public:
  /** Default Constructor */
  StoreBinning(): m_nBins(0), m_bins(m_nBins), m_xBins(0), m_yBins(0), m_zBins(0) {  }

  /**Constructor for a given nulmber of bins*/
  StoreBinning(int nBins): m_nBins(nBins), m_bins(nBins),
			   m_xBins(int(std::pow(nBins, 1.0/3))), 
			   m_yBins(m_xBins), 
			   m_zBins(m_xBins) { 
  }
  /**Contructor for 3D*/
  StoreBinning(int xBins, int yBins, int zBins=1): m_nBins(xBins*yBins*zBins), m_bins(m_nBins),
						   m_xBins(xBins), m_yBins(yBins), m_zBins(zBins) { 
  }
  /**Contructor using a file as input*/
  StoreBinning(TString filename);

  ~StoreBinning() {  }

  /** Get global bin number*/
  int  GetGlobalBinNumber(int i, int j, int k) const;
  /** Get local bin number*/
  void GetLocalBinNumber(int global, int& i, int& j, int& k) const;

  //All indices start at 0;
  /** Set Bin boundaries (3D)
      @param i i-th bin
      @param j j-th bin
      @param k k-th bin
      @param x Value of the lower bound x
      @param y Value of the lower bound y
      @param z Value of the lower bound z
   */
  void SetBinBoundaries(int i, int j, int k, double x, double y, double z);
  /** Set Bin boundaries (3D)
      @param i i-th bin
      @param j j-th bin
      @param k k-th bin
      @param bin Bin pointer
   */
  void SetBinBoundaries(int i, int j, int k, Bin bin);

  /** Set Bin boundaries (2D)
      @param i i-th bin
      @param j j-th bin
      @param x Value of the lower bound x
      @param y Value of the lower bound y
   */
  void SetBinBoundaries(int i, int j, double x, double y);
  /** Set Bin boundaries (2D)
      @param i i-th bin
      @param j j-th bin
      @param bin Bin pointer
   */
  void SetBinBoundaries(int i, int j, Bin bin);

  /** Set Bin boundaries
   @param global Global bin ID
   @param bin Bin object
  */
  void SetBinBoundaries(int global, Bin bin);

  /** Set Bin boundaries
   @param global Global bin ID
   @param x Lower x value for the bin
   @param y Lower y value for the bin
   @param z Lower z value for the bin   
  */
  void SetBinBoundaries(int global, double x, double y, double z=1.0);


  /** Get the global bin*/
  Bin  GetBin(int globalBin) const ;
  /** Get the bin by coordinate*/
  Bin  GetBin(int i, int j, int k) const ;
  /**Find the lower edge of the specified global bin*/
  Bin FindLowEdgeBin(int globalBin) const;
  /**Find the lower edge of the bin identified by its coordinates*/
  Bin FindLowEdgeBin(int i, int j, int k) const;

  /**Get the bin number for a given coordinate*/
  int GetBinNumber(double x, double y, double z=0.0) const;
  /**Get the bin number for a given coordinate*/
  int GetBinNumber2(double x, double y, double z=0.0) const;
  /**Get the bin number for a given bin*/
  int GetBinNumber(const Bin&  bin) const;

  /**Get the number of bins*/
  int GetNBins() const { return m_nBins; }
  
  /**Sort the bins*/
  void Sort() {
    std::sort(m_bins.begin(), m_bins.end());
  }

  /** Allow for printout*/
  friend std::ostream& operator<< (std::ostream& o, const StoreBinning& rhs) {
    o << "Number Of Bins"
      << std::setw(10) << rhs.m_xBins 
      << std::setw(10) << rhs.m_yBins 
      << std::setw(10) << rhs.m_zBins 
      << std::endl;
    for (int i = 0; i < rhs.GetNBins() ;++i) {
      o << std::setw(5) << i << rhs.GetBin(i)  << std::endl;
    }
    return o;
  }
  
  /**Write to file*/
  void SaveToFile(TString filename) const;
  /**Create the TGeo object for display*/
  void CreateTGeoFile(TString filename) const;

private:
  int m_nBins;//!< Number of bins
  BinVector m_bins; //!< Vector of bins
  int m_xBins, m_yBins, m_zBins;

  /**Convert a N-D object to 1D*/
  BinVector::const_iterator GetBin1D
  (double value, BinVector::const_iterator first, BinVector::const_iterator last, int stepDistance, int coord) const;

};

#endif // StoreBinning_hh
