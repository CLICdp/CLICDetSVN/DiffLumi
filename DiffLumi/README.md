#  README for Lumi Spectrum measurement

## How To Compile

Make sure the repository is cloned with recursing through submodules:

```
git clone --recurse-submodules
```
Or update the submodules:
```
git submodule update --init --recursive
```
Note that LOG4CPLUS intself has some submodules!

then create a build folder
```
mkdir build
cd build
cmake ..
make
```

## Running the Software

### Obtaining Data:
  Normally, the Guinea Pig samples are available on AFS under
  /afs/cern.ch/eng/clic/work2/DiffLumi/
  If not then here is the procedure to reobtain them

  1. Get the ROOT files from text
    1. download the lumi files from
  http://clic-beam-beam.web.cern.ch/clic-beam-beam/
  The lumi files are gz files that need to be gunzip and merged. Beware: the files are sorted, one needs to
  either read all the events or randomize them.
    2. go to DiffLumi/TextToRoot
    3. edit readlumi.cpp to read the text file you obtained above. Compile and run
    4. This gives 2 files:
     - file.root that is the root version of the text files
     - GuineaPigSpectrum.root that contains the 1d and 2d lumi spectra. Hold on to this one carefully.
  2. Get the BHWide sample
    1. Go to DiffLumi/BHWideDemo
    2. Get the content of /afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/BHWide/
    3. Untar bhwide.tar, go to bhwide/, source environment.sh
    4. copy DiffLumi/BHWideDemo/demo.f to DiffLumi/BHWideDemo/bhwide/demo/
    5. Depending on the input file, you need to edit the demo.f: the file from GP has 2 relevant columns,
  while the one we create using our code (see later) has 7. There are a couple of lines to uncomment.
    6. run make
    7. export INPUTFILE=path_to_the_text_file_you_got_before
    8. run demo.exe
    9. If you have a very large input text file (>1Million event), you need  to run
  python DiffLumi/BHWideDemo/run.py $INPUTFILE
    10. the file bhwideout.root (obtained either directly with run.py of with conversion from h2root) will be
  converted in 3).

The step 2. is identical when processing the MC sample.

### Obtaining the MC sample
  1. Getting the pre BHWide data
    1. Go to DiffLumi/build (mkdir it if needed)
    2. cmake ..
    3. make
    4. run CreateLumiFile/CreateLumiFileAbstract --help to get a hint on the command line arguments that should be used
  This creates 2 files: LumiFile_SOMETHING.out and LumiFileMC_MODEL.root
  - LumiFile_SOMETHING.out is to be used as input to BHWide
  - LumiFileMC_MODEL.root can be used as input to the Fit like file.root above (pre BHWide data, requires to change several things in the Fit.cpp, see later).
  
  2. get the BHWide data
    1. see 1.2).
    2. make sure demo.f has been updated to account for the different number of columns
  
  You should have after this step 2 root files: 1 for Guinea Pig after BHWide and 1 for the MC sample after BHWide

### Getting usable data for the fit
 1. the root files produced by BHWide cannot be used as such as the entries are not doubles, but floats.
 2. Convert the files using the utility in DiffLumi/BHWideDemo/BHWide.* :
    ```
    root -l final.root
    .L MyTree.C
    .L BHWide.C
    b = BHWide(h998)
    b.Loop()
    .q
    ```
 3. This produces each time a root file that has the proper structure for the fit (make sure you copy the file
after running!).

#### Smearing the energy
 1. get to DiffLumi/SmearEnergy
 2. Edit smear.C and compile (from DiffLumi/build)
This copies the input file but smears the energy and theta of the outgoing electrons, merges photons to electrons

### Fitting
1. Run Fit/FitReweighting --help to get the list of command line arguments.
  It's possible to set the model, the statistics, etc.
  The fit can take a while...
  To reproduce all fits, the best is to open 6 lxplus sessions, create 12 directories, and run the fit
  with different paramters in each.
  - Be careful with the binning: all sessions should have different binnings in order not to overwrite
  - The LumiFileMC_Overlap.root and LumiFileMC_Separate.root are found on pclcd15:/data/sposs/. They are there because
  I have no where else to put them... They are needed for the Generator fit, without cross section effects.
  The ones with the cross section effects are in RootFiles/Chain, so no need to do anything, the code is aware of that.
  
  example commands
  ```
  ./Fit/FitReweighting -m 10000000 -d 3000000 --Binx 20 --Biny 20 --Binz 20 --model Separate --level BHWide -c 0.3 --applyXsec
  ./Fit/FitReweighting -m 10000000 -d 3000000 --Binx 20 --Biny 20 --Binz 20 --model Overlap --level BHWide -c 0.3 --RecreateBinning --applyXsec
  ```


### Once all the fits are done:
1. Run CreateLumiFileAbstract --params themodel_final.root --model themodel so that it uses the fit results as
starting parameters.
2. Run Plots/MakeLumiPlotsFromRoot path_to_lumifile nrebin where path_to_lumifile indicates the
path in which the "new" lumi file is. It's relative to the Utility::GetWorkingDirectory(). Interface should be fixed.
This produces the plots with 0 effects except the binning.
3. Run Plots/LumiPlots control_themodel.root 10 to get the plots with all the effects.



### Error Propagation
1. Once the fit is done, create the Histograms of the parametrization and the one sigma errors with
  ```
          parallel -j10 ./ErrorPropagation/OverlapErrorPropagation BHWide_xsec_smeared_Overlap_final.root {} ::: $(seq -35 35)
  ```
  It will skip not used parameters
  These files (Default: NumericalEstimateOutput_*) should be used by other people (together with the correlation matrix
  in <>final.root) to calculate the errors from the parametrisation.

2. To check the errors on the lumispectrum itself run
   ```
           ./ErrorPropagation/CalculateFinalHistograms <>final.root NumericalEstimateOutput_ OutPutFile.root
   ```
   which creates OutPutFile.root
   ```
           ./Plots/DrawRatioWithErrors OutPutFile.root <nRebin> <OutputPrefix>
   ```
   Which will create Spectra and Ratios containing the error from GuineaPig and from the parametrisation


# That's all folks!
