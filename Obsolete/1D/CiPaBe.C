// my standard includes:
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <assert.h>
#include <map>

// root includes:
#include <TROOT.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TRandom3.h>
#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TTree.h>
#include <TNtuple.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TColor.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TPostScript.h>
#include <TGaxis.h>
#include <TPaveText.h>
#include <TLorentzVector.h>

using namespace std;

double binWidth, FixX, nBins;

class CiPaBe {
public:
	CiPaBe();
    	~CiPaBe();
};


// Finding anorm
	double getNorm (double *a1, double lower) {
		double norm, temp1, temp2, temp4, Eta;
		Eta = 5;
  		temp1 = TMath::Gamma(2.+a1[1]+a1[2]);
		//cout << lower << "  " << "temp1 is: " << temp1 << "  ";
		temp2 = TMath::Gamma(1.+a1[1])*TMath::Gamma(1.+a1[2]);
		temp4 = TMath::BetaIncomplete(pow(lower,Eta),1.+a1[2],1.+a1[1]) * TMath::Beta(1.+a1[2],1.+a1[1]);
		//cout << a1[1] << "   " << a1[2] << "   " << "temp2 is: " << temp2 << "temp 4 is " << temp4 << "  " ;
  		norm = temp1/(temp2 - temp1 * temp4);
		///cout << norm << endl;
		return norm;
	}	

// The fit function
	double Fit1 (double *x, double *a1) { 
		double norm, d1,Eta, tOf, firstbin;
		Eta=5;
		firstbin = binWidth;
		tOf = pow((1-FixX),(1/Eta));
		
 		norm = getNorm(a1, tOf);
		d1=0;	
		//cout << "norm  "  << norm << endl;
		if(x[0] <= firstbin) {
		d1 = a1[0];
   		} else if (x[0] >= tOf) { //where the beta distribution begin
     			d1 = (1.-a1[0]) * norm * Eta * pow(x[0],Eta-1.)*pow(1.-pow(x[0],Eta),a1[1]) * pow(x[0],Eta*a1[2])*binWidth;
		} else if(x[0] > firstbin && x[0] < tOf) { //this is everything in between, empty so ignore for the fit!
    		TF1::RejectPoint();
   		}
		return d1;
	}


CiPaBe::CiPaBe() {
	TH1::SetDefaultSumw2(1);
	gROOT -> SetStyle("Plain");
	gStyle -> SetPalette(1);
	gStyle -> SetOptStat(0);


// Circe Parameterization for Beamstrahlung - Creating 2 histograms: energy spectrum in the variable x (hist1) and energy spectrum in the variable t (hist2).
// Fix all the values of x>FixX to 1.

	
	TNtuple * x12 = new TNtuple("x12","Colliding particles info", "T0:T1:T2:T3:T4:T5:T6:T7:T8:T9:T10:T11:T12:T13:T14:T15:T16");
	int size;
	nBins=100;
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.0.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.1.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.2.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.3.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.4.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.5.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.6.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.7.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.8.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.9.0");
	x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.10.0");

// Energy spectrum in variable x	
	TCanvas * cnvs1 = new TCanvas("Beamstrahlung1","Spectrum of the energies of electrons and positrons after beamstrahlung - 0.0",712,23,500,500);
	TH1D * hist1 = new TH1D("hist1", "",nBins,0,1.1);


// Energy spectrum in variable t before the cutoff
	/*double tmax,tmin;
	tmin = 0;
	tmax = 1;
	TH1D * hist2 = new TH1D("hist2", "", nBins,tmin,tmax);
	binWidth = (tmax-tmin)/nBins;
	float Energy1, Energy2,  Eta;
	FixX = 0.995;
	Eta = 5;
	cout << "The entries  " << x12 -> GetEntries() << endl;
	x12 -> SetBranchAddress("T0",&Energy1);
	x12 -> SetBranchAddress("T1",&Energy2);
	for (int i=0; i< x12->GetEntries(); i++) {
		x12 -> GetEntry(i);
		hist1 -> Fill(Energy1/1500);
		hist1 -> Fill(Energy2/1500);
		hist2 -> Fill(pow((1.0058-(Energy1/1500)),(1/Eta)));
		hist2 -> Fill(pow((1.0058-(Energy2/1500)),(1/Eta)));
	}*/




// Energy spectrum in variable t	
	double tmax,tmin;
	tmin = 0;
	tmax = 1;
	TH1D * hist2 = new TH1D("hist2", "", nBins,tmin,tmax);
	binWidth = (tmax-tmin)/nBins;
	float Energy1, Energy2,  Eta;
	FixX = 0.995;
	Eta = 5;
	cout << "The entries  " << x12 -> GetEntries() << endl;
	x12 -> SetBranchAddress("T0",&Energy1);
	x12 -> SetBranchAddress("T1",&Energy2);
	for (int i=0; i< x12->GetEntries(); i++) {
		x12 -> GetEntry(i);
		if ((Energy1/1500)>FixX)
			Energy1=1500;
		if ((Energy2/1500)>FixX)
			Energy2=1500;
		hist1 -> Fill(Energy1/1500);
		hist1 -> Fill(Energy2/1500);
		hist2 -> Fill(pow((1-(Energy1/1500)),(1/Eta)));
		hist2 -> Fill(pow((1-(Energy2/1500)),(1/Eta)));
	}


// Normalize, draw and save the first histogram	
	double normFactor;
	normFactor = hist1 -> Integral();
	hist1 -> Scale (1/normFactor);
	hist1 -> GetXaxis() -> SetTitle ("x");
	hist1 -> GetYaxis() -> SetTitle ("1/N dN/dx");
	hist1 -> Draw("Hist");
	cnvs1 -> SetLogy();
	cnvs1 -> SaveAs("EnergySpectra.ee.0.0.eps");

// Normalize, draw and save the second histogram	
	normFactor = hist2 -> Integral();
	hist2 -> Scale (1/normFactor);
	hist2 -> GetXaxis() -> SetTitle ("t");
	hist2 -> GetYaxis() -> SetTitle ("1/N dN/dt");
	hist2 -> Draw("Hist");
	cnvs1 -> SaveAs("TransEnergySpectra.ee.0.0.eps");
                            		 
// Fit the Transformed spectrum
	TF1 * f1 = new TF1("f1",Fit1,tmin,tmax,3);
   	f1-> SetParameters(0.5,12,-0.5);
	f1 -> SetParLimits(2,-0.8,0);
	f1 -> Draw("");
	hist2 -> Fit(f1,"RVB","",tmin,tmax); 
   	cnvs1 -> SaveAs("fit.eps");

return;

}

CiPaBe::~CiPaBe() {

cout << "plot1D done..." <<endl;

}

int main() {

	CiPaBe cipabe;
	return 0;

}


