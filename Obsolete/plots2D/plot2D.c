// my standard includes:
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <assert.h>
#include <map>

// root includes:
#include <TROOT.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TRandom3.h>
#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TTree.h>
#include <TNtuple.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TColor.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TPostScript.h>
#include <TGaxis.h>
#include <TPaveText.h>
#include <TLorentzVector.h>

using namespace std;



class plot2D {
public:
	plot2D();
    	~plot2D();
};

plot2D::plot2D() {

// plot the energy spectrom in 2D histogram

	TNtuple * x12 = new TNtuple("x12","Colliding particles info", "T0:T1:T2:T3:T4:T5:T6:T7:T8:T9:T10:T11:T12:T13:T14:T15:T16");
	int size;
	size = x12 -> ReadFile("/data/sishiahu/LumiFiles/lumi.ee.out.6.0");
	cout << "The size is:" << size << endl;
	TCanvas * cnvs1 = new TCanvas("Beamstrahlung","2D Differential luminosity spectrum - 6.0",712,23,500,500);
	TH2D * histo = new TH2D("histo", "",100,0,0.6,100,0,0.6);
	x12 -> Draw("T1/3000:T0/3000 >> histo","","col");
	histo -> GetXaxis() -> SetTitle ("x2");
	histo -> GetYaxis() -> SetTitle ("x1");
	cnvs1 -> SaveAs("2Dlumi.ee.6.0.eps");		 
return;

}

plot2D::~plot2D() {

cout << "plot1D done..." <<endl;

}
