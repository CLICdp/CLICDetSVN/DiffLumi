// my standard includes:
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <assert.h>
#include <map>

// root includes:
#include <TROOT.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TRandom3.h>
#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TTree.h>
#include <TNtuple.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TColor.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TPostScript.h>
#include <TGaxis.h>
#include <TPaveText.h>
#include <TLorentzVector.h>

using namespace std;

double binWidth, FixX, nBins;

class EnergySpread {
public:
	EnergySpread();
    	~EnergySpread();
};

// The fit function
	double fitFunction (double *x, double *a) { 
	if (x[0]>a[0] && x[0]<a[1]) {
		return a[2]*TMath::CosH(a[3]*(x[0]-a[4]))+a[5];
	}
	else 
		return 0;
}


EnergySpread::EnergySpread() {

	TH1::SetDefaultSumw2(1);
	gROOT -> SetStyle("Plain");
	gStyle -> SetPalette(1);
	gStyle -> SetOptStat(0);

	TNtuple * energySpread = new TNtuple("EnergySpread","Energy Spread info", "T0:T1:T2:T3:T4:T5");
	int size;
	int nBins = 100;

// Read the energy spread files
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.0");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.1");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.2");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.3");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.4");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.5");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.6");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.7");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.8");	
	energySpread -> ReadFile("/home/sishiahu/EnergySpreadFiles/beam_profiles/bds_electron.0.9");	

// Creat the histogram of the energy of the particles before the collision
	TCanvas * cnvs1 = new TCanvas("Energy Spread","Energy Spread - 0.1",712,23,500,500);
	TH1D * hist = new TH1D("hist", "",nBins,0.99,1.01);
	float Energy,X,Y,Z;	
	binWidth = (1.01-0.99)/nBins;
	energySpread -> SetBranchAddress("T0",&Energy);
	energySpread -> SetBranchAddress("T1", &X);
	energySpread -> SetBranchAddress("T2", &Y);
	energySpread -> SetBranchAddress("T3", &Z);
	cout << "entries: " << energySpread -> GetEntries() << endl;

	for (int i=0; i<energySpread -> GetEntries(); i++) {
		energySpread -> GetEntry(i);
		hist -> Fill(Energy/1500);
	}

// Normalize, draw and save the energy spread	
	double normFactor;
	normFactor = hist -> Integral();
	hist -> Scale (1/normFactor);
	hist -> SetTitle("Energy spread");
	hist -> GetXaxis() -> SetTitle ("x");
	hist -> GetYaxis() -> SetTitle ("1/N dN/dx");
	hist -> Draw("");
	cnvs1 -> SaveAs("EnergySpread.ee.0.0.eps");

	
// Fit the energy spread, draw it and save the canvas
	TF1 * fit = new TF1("fitFunction",fitFunction,0.99,1.01,6);
	fit-> SetParameters(0.995,1.004,0.0006,1000,1,0.011);
	//fit -> SetParError(0,0.001);
	//fit -> SetParError(1,0.001);
	hist -> Fit(fit,"RVB","",0.99,1.01); // fit the hist
	hist -> Draw(""); // draw the fitted histogram
	cnvs1 -> SaveAs("fitES.eps");


// Check the correlation between the energy spread and the location of the particle inside the bunch before the scattering
	float NEvents, maxX, maxY, maxZ, minX, minY, minZ;
	NEvents = energySpread -> GetEntries();

	maxX = energySpread -> GetMaximum("T1");
	minx = energySpread -> GetMinimum("T1");
	maxY = energySpread -> GetMaximum("T2");
	minY = energySpread -> GetMinimum("T2");
	maxZ = energySpread -> GetMaximum("T3");
	minZ = energySpread -> GetMinimum("T3");

// X Correlation
	TCanvas * cnvs2 = new TCanvas("E-XCorrel","E-XCorrel - 0.1",712,23,500,500);
	TH2D * hist2 = new TH2D("hist2","",nBins,minX-1,maxX+0.5,nBins,0.99,1.01);
	for (i=0; i<NEvents; i++) {
		energySpread -> GetEntry(i);
		hist2 -> Fill(X, Energy/1500);
	}
	hist2 -> Draw("");
// Y Correlation
	TCanvas * cnvs3 = new TCanvas("E-YCorrel","E-YCorrel - 0.1",712,23,500,500);
	TH2D * hist3 = new TH2D("hist3","",nBins,minY-1,maxY+0.5,nBins,0.99,1.01);
	for (i=0; i<NEvents; i++) {
		energySpread -> GetEntry(i);
		hist3 -> Fill(Y, Energy/1500);
	}
	hist3 -> Draw("");

// Z correlation
	TCanvas * cnvs4 = new TCanvas("E-ZCorrel","E-ZCorrel - 0.1",712,23,500,500);
	TH2D * hist4 = new TH2D("hist4","",nBins,minZ-1,maxZ+0.5,nBins,0.99,1.01);
	for (i=0; i<NEvents; i++) {
		energySpread -> GetEntry(i);
		hist4 -> Fill(Z, Energy/1500);
	}
	hist4 -> Draw("");
/*
// show the correlation with graphs instead of histogram

	TCanvas * cnvs2 = new TCanvas("E-XCorrel","E-XCorrel - 0.1",712,23,500,500);
	TGraph * graph1 = new TGraph;
	graph1 -> SetPoint(graph1 -> GetN(),1,2.3);
	graph1 -> Draw("");
	for (i=0; i<NEvents; i++) {
		energySpread -> GetEntry(i);
		graph1 -> SetPoint(graph1 -> GetN(),1,1);//X,Energy/1500);
	}
	graph1 -> SetMarkerStyle(21);
	graph1 -> GetXaxis() -> SetTitle("X");
	graph1 -> GetYaxis() -> SetTitle("E");
	graph1 -> Draw("");
	graph1 -> SetTitle("The Energy spread vs.the particle position in the bunch before scattering - x axis");
*/

return;

}

EnergySpread::~EnergySpread() {

cout << "plot1D done..." <<endl;

}

int main() {

	EnergySpread energyspread;
	return 0;

}
