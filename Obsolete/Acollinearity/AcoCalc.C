// my standard includes:
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <assert.h>
#include <map>

// root includes:
#include <TROOT.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TRandom3.h>
#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TTree.h>
#include <TNtuple.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TColor.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TPostScript.h>
#include <TGaxis.h>
#include <TPaveText.h>
#include <TLorentzVector.h>
#include <Math/WrappedTF1.h>
#include <Math/GSLIntegrator.h>

using namespace std;

int main() {
	gROOT-> Reset();
	TH1::SetDefaultSumw2(1);
	gROOT -> SetStyle("Plain");
	gStyle -> SetPalette(1);
	gStyle -> SetOptStat(0);
	
//	const Int_t EventsNum = 100;
	

// Create an histogram and a canvas for later
	TCanvas * cnvs1 = new TCanvas("General","General",712,23,500,500);
   	TH1F * Hist1= new TH1F("(sqrtS1-sqrtS2)/sqrtS1","sqrtS1-Generated center of mass energy, sqrtS2- cms energy from acollinearity",1000,-100,100);
   	TH1F * HistS1= new TH1F("S1","S1 - The center of mass energy from the generated bhabha events",50,0,3000);
   	TH1F * HistS2= new TH1F("S2","S2 - The center of mass energy from the acollinearity",50,0,3000);
//   	TH1F * HistS1vsS2= new TH1F("S1vsS2","S1 vs. S2",,0,3000);
   	TH1F * AngleDis= new TH1F("AngleDis","Angular distribuition of the emitted photons",1000,0,TMath::Pi());
   	
// Create a Tree   	
	TTree *bhwideTree;   //!pointer to the analyzed TTree or TChain
   	//Int_t  fCurrent; //!current Tree number in a TChain
   	

	TTree *CalcData = new TTree("CalculatedData","essintial data for creating the histogram");
	  	
	// Declaration 	
   	Float_t Theta1;   
   	Float_t Theta2;   
   	Float_t sqrtS1;   
   	Float_t sqrtS2; 
   	Float_t ThetaPhot[800][4];

   	Float_t sqrtS = 3000;
   	Float_t thetaCrit = 0.5;



// Initialize ThetaPhot array
	for (int l=0; l<800; l++) {
		for (int r=0; r<4; r++){
			ThetaPhot[l][r] = 0;
		}
	}
	
//	CalcData->SetMakeClass(1);
	
	CalcData -> Branch("theta1",&Theta1);
	CalcData -> Branch("theta2",&Theta2);
	CalcData -> Branch("s1",&sqrtS1);
	CalcData -> Branch("s2",&sqrtS2);
	CalcData -> Branch("thetaphot",&ThetaPhot);
	

   // Declaration of leaf types
   	Float_t bpx2;
   	Float_t bpy2;
   	Float_t bpz2;
   	Float_t bpe2;
   	Float_t bqx2;
   	Float_t bqy2;
   	Float_t bqz2;
   	Float_t bqe2;
   	Float_t bxb1;
   	Float_t bxb2;
   	Float_t qrr;
   	Float_t prr;
   	Float_t cxb1;
   	Float_t cxb2;
   	Float_t weight;
   	Int_t   nphotons;
   	Float_t photdata[800][4];   //[nphotons]

   // List of branches
   	TBranch *b_bpx2;   
   	TBranch *b_bpy2;   
   	TBranch *b_bpz2;   
   	TBranch *b_bpe2;   
   	TBranch *b_bqx2;   
   	TBranch *b_bqy2;   
   	TBranch *b_bqz2;   
   	TBranch *b_bqe2;   
   	TBranch *b_bxb1;   
	TBranch *b_bxb2;   
   	TBranch *b_qrr;   
   	TBranch *b_prr;   
   	TBranch *b_cxb1;   
   	TBranch *b_cxb2;   
   	TBranch *b_weight;  
   	TBranch *b_nphotons; 
   	TBranch *b_photdata; 


    TFile *f = (TFile*)gROOT -> GetListOfFiles() -> FindObject("/home/sishiahu/BHWide/demo/bhwideout.root");
    	if (!f) {
        	f = new TFile("/home/sishiahu/BHWide/demo/bhwideout.root");
      	}
      bhwideTree = (TTree*)gDirectory->Get("h998");
   
	//if (!tree) return;
   	bhwideTree->SetMakeClass(1);

   	bhwideTree -> SetBranchAddress("bpx2", &bpx2, &b_bpx2);
   	bhwideTree -> SetBranchAddress("bpy2", &bpy2, &b_bpy2);
   	bhwideTree -> SetBranchAddress("bpz2", &bpz2, &b_bpz2);
   	bhwideTree -> SetBranchAddress("bpe2", &bpe2, &b_bpe2);
   	bhwideTree -> SetBranchAddress("bqx2", &bqx2, &b_bqx2);
   	bhwideTree -> SetBranchAddress("bqy2", &bqy2, &b_bqy2);
   	bhwideTree -> SetBranchAddress("bqz2", &bqz2, &b_bqz2);
   	bhwideTree -> SetBranchAddress("bqe2", &bqe2, &b_bqe2);
   	bhwideTree -> SetBranchAddress("bxb1", &bxb1, &b_bxb1);
   	bhwideTree -> SetBranchAddress("bxb2", &bxb2, &b_bxb2);
   	bhwideTree -> SetBranchAddress("qrr", &qrr, &b_qrr);
   	bhwideTree -> SetBranchAddress("prr", &prr, &b_prr);
   	bhwideTree -> SetBranchAddress("cxb1", &cxb1, &b_cxb1);
   	bhwideTree -> SetBranchAddress("cxb2", &cxb2, &b_cxb2);
   	bhwideTree -> SetBranchAddress("weight", &weight, &b_weight);
   	bhwideTree -> SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   	bhwideTree -> SetBranchAddress("photdata", photdata, &b_photdata);
   		
 	Int_t nevent = bhwideTree -> GetEntries();
 	double thetaG=0;
 	
// Plot the angular distribuition of the photons in order to determine the critical angle for the ISR/FSR photons.  	
 	for (Int_t i=0;i<nevent;i++) {
    	bhwideTree -> GetEntry(i);
    	thetaG =  TMath::ACos(photdata[0][2]/(TMath::Sqrt(pow(photdata[0][0],2)+pow(photdata[0][1],2)+pow(photdata[0][2],2))));
//    	cout << "thetaphot : " << thetaG << endl;
    	AngleDis -> Fill(thetaG);
    }
 	AngleDis -> GetXaxis() -> SetTitle ("#theta"); 
	AngleDis -> GetYaxis() -> SetTitle ("AU"); 
//	AngleDis -> SetTitleSize(0.03,"X");
//	AngleDis -> SetTitleSize(0.03,"Y");
//	AngleDis -> SetTitleOffset(1.3,"X");
//	AngleDis -> SetTitleOffset(1.3,"Y");
//	AngleDis -> SetLabelSize(0.03,"X");
//	AngleDis -> SetLabelSize(0.03,"Y");
//	AngleDis -> SetAxisRange(0,17,"X");
////	AngleDis -> SetAxisRange(0,3,"Y");
	AngleDis -> Draw("AngleDistribution");
	//cnvs1 -> SetLogy();
	cnvs1 -> SaveAs("angleDis.eps");
 	
 	
 	
// Go over all the events and kick out the ISR photons 
// Calculate s1 - the generated cms energy, s2 - the cms energy from the acollinearity 	
// Calculate and Fill the new tree that contain theta1, theta2, s1 and s2 with the calculated data
	int IteNum = 0;
	double fourMomSum[4]={0,0,0,0}; 	
	int nPho;
  	for (int i=0;i<nevent;i++) {
  		cout << " 1 " << endl;
    	bhwideTree -> GetEntry(i);
		cout << "which enetry " << i << endl;
		cout << "asgfsdgjkshgkhsoghwhtowrht " << endl;
		nPho = 0;
		int j;
    	for (j=0; j<nphotons; j++) {
    		cout << " 2 " << endl;
    		cout << TMath::ACos(photdata[j][2]/(TMath::Sqrt(pow(photdata[j][0],2)+pow(photdata[j][1],2)+pow(photdata[j][2],2))));
    		if (TMath::ACos(photdata[j][2]/(TMath::Sqrt(pow(photdata[j][0],2)+pow(photdata[j][1],2)+pow(photdata[j][2],2)))) > thetaCrit) {
    			cout << " a " << endl; 
//    			cout << ThetaPhot[nPho][0] << " = " << photdata[j][0] << endl;
    			cout << "number of photon " << nPho << endl;
    			cout << " number of j " << j << endl;
    			ThetaPhot[nPho][0] = photdata[j][0];
    			cout << " b " << endl;
    			ThetaPhot[nPho][1] = photdata[j][1];
    			cout << " c " << endl;
    			ThetaPhot[nPho][2] = photdata[j][2];
    			cout << " d " << endl;
    			ThetaPhot[nPho][3] = photdata[j][3];
    			cout << " 3 " << endl;
    			nPho=nPho+1;
    		}
    	}
    	nphotons = 0;
    	cout << " 4 " << endl;
//  	   	cout << "thetaphot " << ThetaPhot[nPho] << endl;
       	Theta1 = TMath::ACos(bpz2/(TMath::Sqrt(pow(bpx2,2)+pow(bpy2,2)+pow(bpz2,2))));
//       	cout << "theta1 " << Theta1 << endl;
    	Theta2 = TMath::ACos(bqz2/(TMath::Sqrt(pow(bqx2,2)+pow(bqy2,2)+pow(bqz2,2))));
//    	cout << "theta2 " << Theta2 << endl;
		
		cout << " 5 " << endl;
// Calculating sqrtS1 from the final state excluding the ISR energy 
		fourMomSum[0] = fourMomSum[0] + bpx2 + bqx2; 
		fourMomSum[1] = fourMomSum[1] + bpy2 + bqy2;
		fourMomSum[2] = fourMomSum[2] + bpz2 + bqz2;
		fourMomSum[3] = fourMomSum[3] + bpz2 + bqz2;
		cout << " 6 " << endl;
		
		for (int k=0; k<nPho; k++) {
			fourMomSum[0] = fourMomSum[0] + ThetaPhot[nPho][0];
			fourMomSum[1] = fourMomSum[1] + ThetaPhot[nPho][1];
			fourMomSum[2] = fourMomSum[2] + ThetaPhot[nPho][2];
			fourMomSum[3] = fourMomSum[3] + ThetaPhot[nPho][3];
			cout << " 7 " << endl;
		}  
		sqrtS1 =3000 - sqrt(pow(fourMomSum[0],2)+pow(fourMomSum[1],2)+pow(fourMomSum[2],2)+pow(fourMomSum[3],2));
//		cout << "sqrtS1 " << sqrtS1 << endl;
		cout << " 8 " << endl;
					
					
	   	sqrtS2 = TMath::Abs(sqrtS*sqrt((sin(Theta1)+sin(Theta2)-sin(Theta1+Theta2))/(sin(Theta1)+sin(Theta2)+sin(Theta1+Theta2)))); 
	   	CalcData -> Fill();
	   	cout << " 9 " << endl;
//    	cout << "sqrtS1  in " << i+1 << " is: " << sqrtS1 << endl;
//    	cout << "sqrtS2  in " << i+1 << " is: " << sqrtS2 << endl;
    	HistS1 -> Fill(sqrtS1);
		HistS2 -> Fill(sqrtS2);
    	Hist1 -> Fill((sqrtS1-sqrtS2)/sqrtS1);

		cout << " 10 " << endl;
		// Initializations
		fourMomSum[0] = 0; 
		fourMomSum[1] = 0;
		fourMomSum[2] = 0;
		fourMomSum[3] = 0;
		cout << "Iteration number "<< IteNum << endl;
		
		for (int l=0; l<800; l++) {
			for (int r=0; r<4; r++){
				ThetaPhot[l][r] = 0;
			}
		}
	IteNum = IteNum + 1;
	cout << " 11 " << endl;
	}		

// Normalize, set and draw HistS1 - sqrtS1
	HistS1 -> GetXaxis() -> SetTitle ("S_{1}"); 
	HistS1 -> GetYaxis() -> SetTitle ("AU");
//	cout << "Here " << endl; 
	HistS1 -> SetTitleSize(0.03,"X");
	HistS1 -> SetTitleSize(0.03,"Y");
	HistS1 -> SetTitleOffset(1.3,"X");
	HistS1 -> SetTitleOffset(1.3,"Y");
	HistS1 -> SetLabelSize(0.03,"X");
	HistS1 -> SetLabelSize(0.03,"Y");
	HistS1 -> SetLineColor(kBlack);
//	HistS1 -> SetFillColor(kMagenta+2);
	HistS1 -> Draw("HistS1");
	//cnvs1 -> SetLogy();
	cnvs1 -> SaveAs("s1.eps");




// Normalize, set and draw HistS1 - sqrtS2
	HistS2 -> GetXaxis() -> SetTitle ("S_{2}"); 
	HistS2 -> GetYaxis() -> SetTitle ("AU"); 
	HistS2 -> SetTitleSize(0.03,"X");
	HistS2 -> SetTitleSize(0.03,"Y");
	HistS2 -> SetTitleOffset(1.3,"X");
	HistS2 -> SetTitleOffset(1.3,"Y");
	HistS2 -> SetLabelSize(0.03,"X");
	HistS2 -> SetLabelSize(0.03,"Y");
	HistS2 -> SetLineColor(kBlack);
//	HistS2 -> SetFillColor(kGreen-1);
	HistS2 -> Draw("HistS2");
	//cnvs1 -> SetLogy();
	cnvs1 -> SaveAs("s2.eps");

// Draw s1 and s2 histogram on the same canvas for rought comparison
	HistS1 -> GetXaxis() -> SetTitle ("S_{1},S_{2}"); 
	HistS1 -> GetYaxis() -> SetTitle ("AU"); 
	HistS1 -> SetTitleSize(0.03,"X");
	HistS1 -> SetTitleSize(0.03,"Y");
	HistS1 -> SetTitleOffset(1.3,"X");
	HistS1 -> SetTitleOffset(1.3,"Y");
	HistS1 -> SetLabelSize(0.03,"X");
	HistS1 -> SetLabelSize(0.03,"Y");
	
	HistS1 -> SetLineColor(kMagenta+2);
	HistS2 -> SetLineColor(kGreen-1);
//	HistS1 -> SetFillColor(kMagenta+2);
//	HistS2 -> SetFillColor(kGreen-1);
	
	HistS1 -> Draw("Hist");
	HistS2 -> Draw("sameHist");
	cnvs1 -> SaveAs("s1Vss2.eps");



 	
// Normalize, set and draw Hist1 - (sqrtS1-sqrtS2)/sqrtS1
	double normFactor;
	normFactor = Hist1 -> Integral();
//	Hist1 -> Scale (1/normFactor);
	Hist1 -> GetXaxis() -> SetTitle ("#frac{S_{1}-S_{2}}{S_{1}}"); 
	Hist1 -> GetYaxis() -> SetTitle ("AU"); 
	Hist1 -> SetTitleSize(0.03,"X");
	Hist1 -> SetTitleSize(0.03,"Y");
	Hist1 -> SetTitleOffset(1.3,"X");
	Hist1 -> SetTitleOffset(1.3,"Y");
	Hist1 -> SetLabelSize(0.03,"X");
	Hist1 -> SetLabelSize(0.03,"Y");
	Hist1 -> SetAxisRange(-10,1,"X");
//	Hist1 -> SetAxisRange(0,3,"Y");
	Hist1 -> SetLineColor(kBlack);
	Hist1 -> SetFillColor(kRed+3);
	Hist1 -> Draw("Hist");
	//cnvs1 -> SetLogy();
	cnvs1 -> SaveAs("s1s2comp.eps");
	
	
	return 0;
}
  


