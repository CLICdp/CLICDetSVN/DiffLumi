//Simple Macro to read text file into a root tree
{
TFile *file = TFile::Open("Lumi500_Random.root","RECREATE");
TTree *tree = new TTree("eleTree","eleTree");

tree->ReadFile("Lumi500_Random.ee.dat","E1/D:E2/D");
tree->Write();
file->Close();
  exit();
}
