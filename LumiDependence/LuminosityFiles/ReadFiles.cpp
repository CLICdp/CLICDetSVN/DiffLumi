//Simple Macro to read text file into a root tree
{

TFile *file = TFile::Open("../LuminosityFiles/Lumi.root","RECREATE");
//TTree *tree = new TTree("eleTree","eleTree");
//TFile *file = TFile::Open("Lumi500_Random.root","RECREATE");
//TTree *tree = new TTree("eleTree","eleTree");
//TFile *file = TFile::Open("Lumi1400.root","RECREATE");
//TTree *tree = new TTree("eleTree","eleTree");
//TFile *file = TFile::Open("Lumi3000.root","RECREATE");
//TTree *tree = new TTree("eleTree","eleTree");

//Define var for Tree 
 // Double_t E1, E2;
 //  tree->SetBranchAddress("E1",&E1); //

 TTree *tree1 = new TTree("Lumi350","Lumi350");
 tree1->ReadFile("Lumi350.ee.dat","E1/D:E2/D");
 tree1->Write();


 TTree *tree2 = new TTree("Lumi500","Lumi500");
 tree2->ReadFile("Lumi500_Random.ee.dat","E1/D:E2/D");
 tree2->Write();

 TTree *tree3 = new TTree("Lumi1400","Lumi1400");
 tree3->ReadFile("Lumi1400.ee.dat","E1/D:E2/D");
 tree3->Write();

 TTree *tree4 = new TTree("Lumi3000","Lumi3000");
 tree4->ReadFile("Lumi3000_Random.ee.dat","E1/D:E2/D");
 tree4->Write();


 file->Close();
  exit();
}
