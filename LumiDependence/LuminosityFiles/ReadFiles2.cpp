//Simple Macro to read text file into a root tree
{
TFile *file = TFile::Open("Lumi350.root","RECREATE");
TTree *tree = new TTree("eleTree","eleTree");

tree->ReadFile("Lumi350.ee.dat","E1/D:E2/D");

 tree->Write();
file->Close();
  exit();
}
