//Example Macro to read tree and fill histogram

void LumiDependence() {

  //Open File
  TFile *file = TFile::Open("../LuminosityFiles/Lumi.root");  //Get the Tree
  TTree *tree1;
  TTree *tree2;
  TTree *tree3;
  TTree *tree4;
  file->GetObject("Lumi350",tree1);
  file->GetObject("Lumi500",tree2);
  file->GetObject("Lumi1400",tree3);
  file->GetObject("Lumi3000",tree4);
  //Set the Styles&Options
  //gStyle->SetOptStat(111111111);
  gStyle->SetOptStat(0);
  
  //Define variabels for Tree
  Double_t E1, E2;
  tree1->SetBranchAddress("E1", &E1);
  tree1->SetBranchAddress("E2", &E2);
  tree2->SetBranchAddress("E1", &E1);
  tree2->SetBranchAddress("E2", &E2);
  tree3->SetBranchAddress("E1", &E1);
  tree3->SetBranchAddress("E2", &E2);
  tree4->SetBranchAddress("E1", &E1);
  tree4->SetBranchAddress("E2", &E2);
 

  //Create Histogram
  Int_t bins=6000;
  Double_t xLimit =1.1;
  TH1D *h350 = new TH1D("h350","Luminosity for range of Energies",bins,0, xLimit);
  TH1D *h500 = new TH1D("h500","Luminosity for range of Energies",bins,0, xLimit);
  TH1D *h1400 = new TH1D("h1400","Luminosity for range of Energies",bins,0, xLimit);
  TH1D *h3000 = new TH1D("h3000","Luminosity for range of Energies",bins,0, xLimit);

  TH2D *he350= new TH2D("he350","E_1 : E_2 at 350 GeV",bins,0,175,bins,0,175);
  TH2D *he500= new TH2D("he500","E_1 : E_2 at 500 GeV",bins,0,250,bins,0,250);
  TH2D *he1400 = new TH2D("he1400","E_1 : E_2 at 1400 GeV",bins,0,700,bins,0,700); 
  TH2D *he3000 = new TH2D("he3000","E_1 : E_2 at 3000 GeV",bins,0,1500,bins,0,1500); 

  TH2D *heE350= new TH2D("heE350","E1/E : E2/E at 350 GeV",bins,0,1.1,bins,0,1.1);
  TH2D *heE500= new TH2D("heE500","E1/E : E2/E at 500 GeV",bins,0,1.1,bins,0,1.1);
  TH2D *heE1400 = new TH2D("heE1400","E1/E : E2/E at 1400 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heE3000 = new TH2D("heE3000","E1/E : E2/E at 3000 GeV",bins,0,1.1,bins,0,1.1); 

  //Loop over the tree
  int n_entries =  tree1->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree1->GetEntry(i);
    h350->Fill(2*sqrt(E1*E2)/350);
    he350->Fill(E1,E2);
    heE350->Fill(E1/175,E2/175);
      }

  int n_entries =  tree2->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree2->GetEntry(i);
    h500->Fill(2*sqrt(E1*E2)/500);
    he500->Fill(E1,E2);
    heE500->Fill(E1/250,E2/250);
 }
  int n_entries =  tree3->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree3->GetEntry(i);
    h1400->Fill(2*sqrt(E1*E2)/1400);
    he1400->Fill(E1,E2);
    heE1400->Fill(E1/700,E2/700);
  }
  int n_entries =  tree4->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree4->GetEntry(i);
    h3000-> Fill(2*sqrt(E1*E2)/3000);
    he3000->Fill(E1,E2); 
    heE3000->Fill(E1/1500,E2/1500);
  }
 
  Double_t scalingFactor = 1/(h350->Integral()) ;
  h350->Scale(scalingFactor);
 
  Double_t scalingFactor = 1/(h500->Integral());
  h500->Scale(scalingFactor);
 

  Double_t scalingFactor = 1/(h1400->Integral());
  h1400->Scale(scalingFactor);
 

  Double_t scalingFactor = 1/(h3000->Integral());
  h3000->Scale(scalingFactor);
 


  std::cout << "h350     " <<  h350->Integral() << "     " <<  h350->GetEntries() << std::endl;
  std::cout << "h500     " <<  h500->Integral() << "     " <<  h500->GetEntries() << std::endl;
  std::cout << "h1400    " << h1400->Integral() << "     " << h1400->GetEntries() << std::endl;
  std::cout << "h3000    " << h3000->Integral() << "     " << h3000->GetEntries() << std::endl;
 


  TCanvas *c = new TCanvas("L distr","L distribution");
  h350->Draw();
  h350->SetLineColor(kRed);
  h500->Draw("same");
  h500->SetLineColor(kBlack);
  h1400->Draw("same");
  h1400->SetLineColor(kGreen+2);
  h3000->Draw("same");
  h3000->SetLineColor(kBlue);
  h350->GetXaxis()->SetTitle(" #sqrt{S}/E_{nominal} ");
  h350->GetXaxis()->CenterTitle(true);
  //  h350->GetYaxis()->SetRangeUser(0.1,1);//->SetTitle("Counts[#]");
  h350->GetYaxis()->CenterTitle(true);
 // gPad->SetLeftMargin(0.25);  
  // gPad->SetBottomMargin(0.25);
  leg = new TLegend(0.1,0.7,0.48,0.9);  
  leg->SetHeader("E_{nominal}");
  leg->SetTextSize(0.04);// set size of text
  leg->SetFillColor(0);// Have a white background
  leg->AddEntry(h350, "350 GeV", "l");
  leg->AddEntry(h500, "500 GeV", "l");
  leg->AddEntry(h1400, "1400 Gev", "l");
  leg->AddEntry(h3000, "3 TeV", "l");
  leg->Draw();

  h350->SetAxisRange(0.95, 1.01,"X");
  c->SaveAs("L_distribution-1.pdf");

  c->SetLogy(1);//  c->SetLogy(0);
  h350->SetAxisRange(0.0,1.01,"X");
  c->SaveAs("L_distribution-1.pdf");



   TCanvas *c1 = new TCanvas("E_1 : E_2 to Energies","E_1 : E_2 to Energies",1500,1200);
   c1->Divide(2,2);
   c1->cd(1);
   he350->Draw("colz");   
   c1->cd(2);
   he500->Draw("colz");
   c1->cd(3);
   he1400->Draw("colz");
   c1->cd(4);
   he3000->Draw("colz");
   c1->SaveAs("E1_to_E2-1.pdf");

  TCanvas *c2 = new TCanvas("E1/Enom : E2/nom to Energies","E1/E : E2/E to Energies",1500,1200);
  c2->Divide(2,2);
  c2->cd(1);
  heE350->Draw("colz");   
  heE350->SetAxisRange(0.98,1.01,"X");
  heE350->SetAxisRange(0.98,1.01,"Y");
  c2->cd(2);
  heE500->Draw("colz");
  heE500->SetAxisRange(0.98,1.01,"X");
  heE500->SetAxisRange(0.98,1.01,"Y");
  c2->cd(3);
  heE1400->Draw("colz");
  heE1400->SetAxisRange(0.98,1.01,"X");
  heE1400->SetAxisRange(0.98,1.01,"Y");
  c2->cd(4);
  heE3000->Draw("colz");
  heE3000->SetAxisRange(0.98,1.01,"X");
  heE3000->SetAxisRange(0.98,1.01,"Y");
  c2->SaveAs("E1/E_to_E2/E-1.pdf");


 
  

  return;
}
