

 //Simple Macro to read text file into a root tree
 {
 TFile *file = TFile::Open("Electrons.root","RECREATE");
 TTree *tree = new TTree("eleTree","eleTree");
 tree->ReadFile("bds_electron.0.1","E1/D:x/D:y/D:z/D:xprime/D:yprime/D");

 tree->ReadFile("bds_electron.0.2");

 file->Close();
   exit();
 }
