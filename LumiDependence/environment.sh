#--------------------------------------------------------------------------------
#     CMake
#--------------------------------------------------------------------------------
export PATH="/afs/cern.ch/sw/lcg/external/CMake/2.8.2/x86_64-slc5-gcc43-opt/bin:$PATH"

#--------------------------------------------------------------------------------
#     ROOT
#--------------------------------------------------------------------------------
export ROOTSYS="/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/ROOT/v5-30-00"
export PATH="$ROOTSYS/bin:$PATH"
export LD_LIBRARY_PATH="$ROOTSYS/lib:$LD_LIBRARY_PATH"

#--------------------------------------------------------------------------------
#     GuineaPig
#--------------------------------------------------------------------------------

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/cern.ch/user/s/sailer/afssoftware/sl5/BarbarasVersion/gp/fftwlibs
export PATH=/afs/cern.ch/work/s/sailer/public/software/GuineaPig/guinea-pig-1.2.0/bin:$PATH

