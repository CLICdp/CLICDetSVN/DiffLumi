#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TTree.h>


//Example Compiled Program to read tree and fill histogram

int DrawHistograms() {

  //Open File
  TFile *file = TFile::Open("../ParticleDistributions/bdsElectrons.root");
  
  //Get the Tree
  TTree *tree;
  file->GetObject("eleTree",tree);
  //Define variabels for Tree
  Double_t energy, xPos, yPos, zPos, xPrime, yPrime;
  tree->SetBranchAddress("E1", &energy);
  tree->SetBranchAddress("x", &xPos);
  tree->SetBranchAddress("y", &yPos);
  tree->SetBranchAddress("z", &zPos);
  tree->SetBranchAddress("xprime", &xPrime);
  tree->SetBranchAddress("yprime", &yPrime);


  //Create Histogram
  Int_t bins=100;
  Double_t xLimit = 0.5;
  TH1D *h1X = new TH1D("h1X","X position",bins, -xLimit, xLimit);
  TH1D *h1Y = new TH1D("h1Y","Y position",bins, -xLimit, xLimit);

  //Loop over the tree

  for (int i = 0; i <tree->GetEntries() ;++i) {
    tree->GetEntry(i);
    h1X->Fill(xPos);
    h1Y->Fill(yPos);
  }
 
  //Drawing
  //Create Canvas
  TCanvas *c = new TCanvas("canv","canv");
  h1X->Draw();
  //Draw in the same canvas
  h1Y->Draw("same");
  //Change the line color
  h1Y->SetLineColor(kRed);

  c->SaveAs("Histos.eps");

  return 0;
}


int main ()
{
  return DrawHistograms();
}
