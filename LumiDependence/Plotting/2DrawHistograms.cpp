//Example Macro to read tree and fill histogram

void DrawHistograms() {

  //Open File
  TFile *file = TFile::Open("../ParticleDistributions/bdsElectrons.root");
  
  //Get the Tree
  TTree *tree;
  file->GetObject("eleTree",tree);
  //Define variabels for Tree
  Double_t energy, xPos, yPos, zPos, xPrime, yPrime;
  tree->SetBranchAddress("E1", &energy);
  tree->SetBranchAddress("x", &xPos);
  tree->SetBranchAddress("y", &yPos);
  tree->SetBranchAddress("z", &zPos);
  tree->SetBranchAddress("xprime", &xPrime);
  tree->SetBranchAddress("yprime", &yPrime);


  //Create Histogram
  Int_t bins=100;
  Double_t xLimit = 0.5;
  TH1D *h1X = new TH1D("h1X","X position",bins, -xLimit, xLimit);
  TH1D *h1Y = new TH1D("h1Y","Y position",bins, -xLimit, xLimit);
  TH1D *hEnergy = new TH1D("hEnergy","Energy",bins,1490,1510);


  //Loop over the tree
  int n_entries =  tree->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree->GetEntry(i);
    h1X->Fill(xPos);
    h1Y->Fill(yPos);
    if (energy<1500){
      continue;
    }
    hEnergy->Fill(energy);
  }
 
  //Drawing
  //Create Canvas
  TCanvas *c = new TCanvas("canv","canv");
  h1X->Draw();
  //Draw in the same canvas
  h1Y->Draw("same");
  //Change the line color
  h1Y->SetLineColor(kRed);
  TCanvas *c1 = new TCanvas("Energy","Energy");
  hEnergy->Draw();

  c1->SaveAs("somefile.pdf");

  return;
}
