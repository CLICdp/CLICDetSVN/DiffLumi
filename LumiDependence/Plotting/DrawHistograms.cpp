//Example Macro to read tree and fill histogram
#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <TAxis>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
void DrawHistograms() {
  gStyle->SetOptStat(0);
  //Open File
  TFile *file = TFile::Open("../ParticleDistributions/bdsElectrons.root");  //Get the Tree
  TTree *tree;
  file->GetObject("eleTree",tree);  //Set the Styles&Options
 

  
  //Define variabels for Tree
  Double_t energy, xPos, yPos, zPos, xPrime, yPrime;
  tree->SetBranchAddress("E1", &energy);
  tree->SetBranchAddress("x", &xPos);
  tree->SetBranchAddress("y", &yPos);
  tree->SetBranchAddress("z", &zPos);
  tree->SetBranchAddress("xprime", &xPrime);
  tree->SetBranchAddress("yprime", &yPrime);


  //Create Histogram
  Int_t bins=800;
  Double_t xLimit = 0.5; Double_t yLimit=35;
  //  TH1D *h1Z = new TH1D("h1Z","z micrometer",bins, -200, 200);
  // TH1D *h1X = new TH1D("h1X","X and Y Distribution",bins, 0, 0);
  // TH1D *h1Y = new TH1D("h1Y","X and Y Distribution",bins, 0, 0);
  // TH1D *hEnergy = new TH1D("hEnergy","Energy",1500,1492,1509);
  //TH2D *hEnX= new TH2D("hEnX","En&X",bins,-xLimit,xLimit,1500,1492,1509);
  //TH2D *hEnY= new TH2D("hEnY","En&Y",bins,-xLimit,xLimit,1500,1492,1509);
  TH2D *XY = new TH2D("XY","X to Y",bins,-0.2,0.2,bins,-0.02,0.02);
 
  TH2D *XxPRIME = new TH2D("XxPRIME","X to XPRIME",bins,-1,1,bins,-yLimit,yLimit); 
  TH2D *YyPRIME = new TH2D("YyPRIME","Y to YPRIME",bins,-0.05,0.05,bins,-32,32); 
  // TH2D *EtoZ = new TH2D("EtoZ","Energy to Z",bins,-150,150,1500,1490,1510); 
   // TH3D *XYZ = new TH3D("XzY","X to Y to Z",800,0,0,800,0,0,800,0,0);

  
 


  //Loop over the tree
  int n_entries =  tree->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree->GetEntry(i);
    // h1Y->Fill(yPos);
    // h1X->Fill(xPos);
    // h1Z->Fill(zPos);
    //if (energy>1000000){
    //   continue;
    //   }
    XY->Fill(xPos,yPos);
    // hEnergy->Fill(energy);
    // EtoZ->Fill(zPos,energy);   
    // hEnX->Fill(xPos,energy);
    // hEnY->Fill(yPos,energy);
    XxPRIME->Fill(xPos,xPrime);
    YyPRIME->Fill(yPos,yPrime);
    // XYZ->Fill(xPos,zPos,yPos);
  
  }



   // TCanvas *k = new TCanvas("Energy_to_Z_Pos","Energy to Z Position");
   // EtoZ->GetYaxis()->SetTitleOffset(1.5);
   // EtoZ->GetXaxis()->SetTitle("z [#mum]");
   // EtoZ->GetXaxis()->CenterTitle(true);
   // EtoZ->GetYaxis()->SetTitle("Counts[#]");
   // EtoZ->GetYaxis()->CenterTitle(true);

   // EtoZ->Draw(); 
   // k->SaveAs("EtoZ_Distributions.png");
   // EtoZ->Draw("colz");// unnecessary to write the axis names in every axis if in same
   // k->SaveAs("EtoZ_Distributions_colorful.png");


  // TCanvas *z = new TCanvas("zPos","z");
  // gPad->SetLeftMargin(0.25); // experiment with the best value for you. Depends on
  // gPad->SetBottomMargin(0.25);
  // h1Z->GetYaxis()->SetTitleOffset(1.5);
  // h1Z->GetXaxis()->SetTitle("z [#mum]");
  // h1Z->GetXaxis()->CenterTitle(true);
  // h1Z->GetYaxis()->SetTitle("Counts[#]");
  // h1Z->GetYaxis()->CenterTitle(true);
  // h1Z->Draw();
  // z->SaveAs("Z distribution1.png");

//DrawingCreate Canvas

// TCanvas *c1 = new TCanvas("Energy","Energy");
//  hEnergy->GetYaxis()->SetTitleOffset(1.5);
// //pt = new TPaveText(0.20,0.7,0.5,0.87, "NDC"); // NDC sets coords   // relative to pad dimensions
// //pt->SetFillColor(3); // text is black on white
// //pt->SetTextSize(0.04); 
// //pt->SetTextAlign(12);
// //text = pt->AddText("Whatever I was  want");
//  heOffSet067_21->SetAxisRange(0.98,1.01,"X");
//   heOffSet067_21->SetAxisRange(0.98,1.01,"Y");
//   hEnergy->GetXaxis()->SetTitle("E [GeV]");
//   hEnergy->GetXaxis()->CenterTitle(true);
//   hEnergy->GetYaxis()->SetTitle("Counts[#]");
//   hEnergy->GetYaxis()->CenterTitle(true);
//   hEnergy->Draw();
//   c1->SaveAs("somefile1.png");




  // TH1D *myHist=(TH1D*) gDirectory->Get("Energy");
  // leg = new TLegend(0.1,0.5,0.3,0.1);  //coordinates are fractions of pad dimensions---------->>>>>???
  // leg->AddEntry(Energy, "Energey", "p");// p shows points,other options exist // (Check documentation)
  // leg->SetHeader("The Legend Title");
  // leg->SetTextSize(0.04);// set size of text
  // leg->SetFillColor(3);// Have a white background
  // leg->Draw();
  // gPad->SetLeftMargin(0.15); // experiment with the best value for you.  // Depends on font size:
  // gPad->SetBottomMargin(0.15);


//   TCanvas *c6 = new TCanvas("hEnX","Energy to X");
// hEnX->GetYaxis()->SetTitleOffset(1.5);
//   hEnX->Draw();//("surf1 [cutg]"); //make something like distribution
//   hEnX->GetYaxis()->SetTitleSize(0.05);
//   hEnX->SetYTitle("E[TeV]");
//   hEnX->GetXaxis()->SetTitleSize(0.05);
//   hEnX->SetXTitle("x[nm]");
//   hEnX->Draw();
//   c6->SaveAs("EnergyX.png");


//   TCanvas *c7 = new TCanvas("Energy to Y","Energy to Y");
//   hEnY->Draw();//("surf1 [cutg]"); //make something like distribution
//   hEnY->GetYaxis()->SetTitleSize(0.03);
//   hEnY->SetYTitle("E [TeV]");
//   hEnY->GetXaxis()->SetTitleSize(0.03);
//   hEnY->SetXTitle("y [nm]");
//   hEnY->Draw();
//   c7->SaveAs("EnergyY.pdf");


  TCanvas *c2 = new TCanvas("XY","XtoY");
 gPad->SetLeftMargin(0.25); // experiment with the best value for you. Depends on
 gPad->SetBottomMargin(0.25);
  XY->GetYaxis()->SetTitleOffset(1.2);
  XY->GetXaxis()->SetTitleOffset(1.2);
  XY->GetYaxis()->SetTitleSize(0.05);
  XY->SetYTitle("y[nm]");
  XY->GetXaxis()->SetTitleSize(0.05);
  XY->SetXTitle("x[nm]"); // degree symbol, just like PAW,
  XY->Draw("colz");  
  c2->SaveAs("XtoY-Color.png");

  TCanvas *c4 = new TCanvas("XxPRIME","XtoXPRIME");
 gPad->SetLeftMargin(0.25); // experiment with the best value for you. Depends on
 gPad->SetBottomMargin(0.25);
  XxPRIME->GetYaxis()->SetTitleSize(0.05);
  XxPRIME->GetYaxis()->CenterTitle(true);
  XxPRIME->SetYTitle("x'[#murad]");
  XxPRIME->GetXaxis()->SetTitleSize(0.05);
  XxPRIME->GetXaxis()->CenterTitle(true);
  XxPRIME->SetXTitle("x[nm]"); // degree symbol, just like PAW,
  XxPRIME->Draw("colz"); 
  c4->SaveAs("X_PrimeToX.png");

  // oops we forgot the header (or "title") for the legend
  // leg = new TLegend(0.01,0.15,0.31,0.01);  //coordinates are fractions //of pad
  //  leg->SetHeader("That would be TITLE XY beam");
  //  leg->SetTextSize(0.04);// set size of text
  //  leg->SetFillColor(3);// Have a white background
  //  leg->AddEntry(c2,"nonFirst histo","l");  // "l" means line / use "f" for a box
  //  leg->Draw();  



  TCanvas *c5 = new TCanvas("YyPRIME","Y to yPrime");
  gPad->SetLeftMargin(0.25);  // experiment with the best value for you. Depends on
  gPad->SetBottomMargin(0.25);
  YyPRIME->GetYaxis()->SetTitleSize(0.05);
  YyPRIME->GetYaxis()->CenterTitle(true);
  YyPRIME->SetYTitle("y'[#murad]");
  YyPRIME->GetXaxis()->SetTitleSize(0.05);
  YyPRIME->GetXaxis()->CenterTitle(true);
  YyPRIME->SetXTitle("y[nm]"); // degree symbol, just like PAW,
  YyPRIME->Draw("colz");  // oops we forgot the header (or "title") for the legend
  c5->SaveAs("Y_PrimetoY.png");

  

  //   TCanvas *c3 = new TCanvas("XYZ","XtoYtoZ");
 //  gPad->SetLeftMargin(0.2);
  
 //  XYZ->GetXaxis()->SetTitleOffset(1.2);
 //  XYZ->GetYaxis()->SetTitleOffset(1.2);
 //  XYZ->GetZaxis()->SetTitleOffset(1.2);
  
 //  XYZ->GetZaxis()->SetTitleSize(0.05);
 //  XYZ->GetZaxis()->CenterTitle(true);
 //  XYZ->SetZTitle("y, [nm]");
 //  XYZ->GetYaxis()->SetTitleSize(0.05);
 //  XYZ->GetYaxis()->CenterTitle(true);
 //  XYZ->SetYTitle("z,[nm]");
 // XYZ->GetXaxis()->SetTitleSize(0.05);
 // XYZ->GetXaxis()->CenterTitle(true);
 // XYZ->SetXTitle("x, [nm]"); // degree symbol, just like PAW,
 // XYZ->Draw();   
 // c3->SaveAs("3Dxyz.png");

 return;

}

// ///Legends
//   // legS = new TLegend(0.01,0.01,0.53,0.09);  //of pad dimensions---------->>>>>???
//   // legS->SetTextSize(0.04);// set size of text
//   // legS->SetFillColor(13);// Have a white background
//   // legS->AddEntry(c3, "3D bunch", "l"); // p shows points,// other options exist  // (Check documentati
//   // legS->Draw("colz");
