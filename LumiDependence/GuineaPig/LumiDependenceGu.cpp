#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <cmath>
#include <iostream> 

void FillHistogramsFromTree(TString filename, TH1D* h1D, TH2D* h2D, double nominalCMSEnergy=3000.0, double lumi=1.0, double lumi1=1.0);


int main() {

  gStyle->SetOptStat(0);


   //Create Histogram
  Int_t bins=1500;
  Double_t xLimit =1.1;
  // THE DEFAULT
  ////0.01 0.03 0.05 0.07 0.09 0.11 0.13 0.15 0.17 0.19 0.21 0.23 0.25 0.27 0.29 0.31 0.33 0.35 0.37 0.39 0.41 0.43 0.45 0.47
 TH1D *hOffSet087_1 = new TH1D("hOffSet87_1","hOffSet87_= 0.1",bins,0, xLimit); 
TH1D *hOffSet057_1 = new TH1D("hOffSet57_1","OffSet= 0.1",bins,0, xLimit);
 TH1D *hOffSet027_1 = new TH1D("hOffSet1","OffSet= 0.1",bins,0, xLimit);
 TH1D *hOffSet017_1 = new TH1D("hOffSet1","OffSet= 0.1",bins,0, xLimit);
 TH2D *heOffSet057_1 = new TH2D("heOffSet057-1","OffSet= 0.01  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0);
  TH2D *heOffSet087_1 = new TH2D("heOffSet087-1","OffSet= 0.01  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
TH2D *heOffSet027_1 = new TH2D("heOffSet027-1","OffSet= 0.01 at 027- 3 TeV",bins,0,0,bins,0,0); 
 TH2D *heOffSet017_1 = new TH2D("heOffSet017-1","OffSet= 0.01 at 0017 3 TeV",bins,0,0,bins,0,0); 
 FillHistogramsFromTree("../GuineaPig/0.170-0.01offRun.root",hOffSet017_1, heOffSet017_1,3000,7.28626e+33,7.28626e+33);
 FillHistogramsFromTree("../GuineaPig/0.27-0.01offRun.root",hOffSet027_1, heOffSet027_1,3000,2.07128e+34,2.07128e+34);
 FillHistogramsFromTree("../GuineaPig/0.87-0.01offRun.root",hOffSet087_1, heOffSet087_1,3000,2.94883e+35,2.94883e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.01offRun.root",hOffSet057_1, heOffSet057_1,3000,1.14735e+35,1.14735e+35);
 

  TH1D *hOffSet037_0 = new TH1D("hOffSet037_0",  "OffSet=0.0 ",bins,0, xLimit);
  TH1D *hOffSet037_1 = new TH1D("hOffSet037_1",  "OffSet=0.01",bins,0, xLimit);
  TH1D *hOffSet037_2 = new TH1D("hOffSet037_2",  "OffSet=0.03",bins,0, xLimit);
  TH1D *hOffSet037_3 = new TH1D("hOffSet037_3",  "OffSet=0.05",bins,0, xLimit);
  TH1D *hOffSet037_4 = new TH1D("hOffSet037_4",  "OffSet=0.07",bins,0, xLimit);
  TH1D *hOffSet037_5 = new TH1D("hOffSet037_5",  "OffSet=0.09",bins,0, xLimit);
  TH1D *hOffSet037_6 = new TH1D("hOffSet037_6",  "OffSet=0.11",bins,0, xLimit);
  TH1D *hOffSet037_7 = new TH1D("hOffSet037_7",  "OffSet=0.13",bins,0, xLimit);
  TH1D *hOffSet037_8 = new TH1D("hOffSet037_8",  "OffSet=0.15",bins,0, xLimit);
  TH1D *hOffSet037_9 = new TH1D("hOffSet037_9",  "OffSet=0.17",bins,0, xLimit);
  TH1D *hOffSet037_10 = new TH1D("hOffSet037_10","OffSet=0.19",bins,0, xLimit);
  TH1D *hOffSet037_11 = new TH1D("hOffSet037_11","OffSet=0.21",bins,0, xLimit);
  TH1D *hOffSet037_12 = new TH1D("hOffSet037_12","OffSet=0.23",bins,0, xLimit);
  TH1D *hOffSet037_13 = new TH1D("hOffSet037_13","OffSet=0.27",bins,0, xLimit);
  TH1D *hOffSet037_14 = new TH1D("hOffSet037_14","OffSet=0.25",bins,0, xLimit);
  TH1D *hOffSet037_15 = new TH1D("hOffSet037_15","OffSet=0.29",bins,0, xLimit);
  TH1D *hOffSet037_16 = new TH1D("hOffSet037_16","OffSet=0.31",bins,0, xLimit);
  TH1D *hOffSet037_17 = new TH1D("hOffSet037_17","OffSet=0.33",bins,0, xLimit);
  TH1D *hOffSet037_18 = new TH1D("hOffSet037_18","OffSet=0.35",bins,0, xLimit);
  TH1D *hOffSet037_19 = new TH1D("hOffSet037_19","OffSet=0.37",bins,0, xLimit);
  TH1D *hOffSet037_20 = new TH1D("hOffSet037_20","OffSet=0.39",bins,0, xLimit);
  TH1D *hOffSet037_21 = new TH1D("hOffSet037_21","OffSet=0.41",bins,0, xLimit);
  TH1D *hOffSet037_22 = new TH1D("hOffSet037_22","OffSet=0.43",bins,0, xLimit);
  TH1D *hOffSet037_23 = new TH1D("hOffSet037_23","OffSet=0.45",bins,0, xLimit);

  TH2D *heOffSet037_0 = new TH2D("heOffSet037-0",  "OffSet= 0.0  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_1 = new TH2D("heOffSet037-1", "OffSet= 0.01  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_2 = new TH2D("heOffSet037-2", "OffSet= 0.03  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_3 = new TH2D("heOffSet037-3", "OffSet= 0.05  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_4 = new TH2D("heOffSet037-4", "OffSet= 0.07  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_5 = new TH2D("heOffSet037-5", "OffSet= 0.09  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_6 = new TH2D("heOffSet037-6", "OffSet= 0.11  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_7 = new TH2D("heOffSet037-7", "OffSet= 0.13  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_8 = new TH2D("heOffSet037-8", "OffSet= 0.15  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_9 = new TH2D("heOffSet037-9", "OffSet= 0.17  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_10 = new TH2D("heOffSet037-10","OffSet= 0.19  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_11 = new TH2D("heOffSet037-11","OffSet= 0.21  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_12 = new TH2D("heOffSet037-12","OffSet= 0.23  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_14 = new TH2D("heOffSet037-14","OffSet= 0.27  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_13 = new TH2D("heOffSet037-13","OffSet= 0.25  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_15 = new TH2D("heOffSet037-15","OffSet= 0.29  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_16 = new TH2D("heOffSet037-16","OffSet= 0.31  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_17 = new TH2D("heOffSet037-17","OffSet= 0.33  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_18 = new TH2D("heOffSet037-18","OffSet= 0.35  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_19 = new TH2D("heOffSet037-19","OffSet= 0.37  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_20 = new TH2D("heOffSet037-20","OffSet= 0.39  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_21 = new TH2D("heOffSet037-21","OffSet= 0.41  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_22 = new TH2D("heOffSet037-22","OffSet= 0.43  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_23 = new TH2D("heOffSet037-23","OffSet= 0.45  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  /// 057-

  /////37. 
  FillHistogramsFromTree("../GuineaPig/0.372-0offRun.root",hOffSet037_0, heOffSet037_0,3000,4.29398e+34,4.29398e+34);												    
    FillHistogramsFromTree("../GuineaPig/0.372-0.01offRun.root",hOffSet037_1, heOffSet037_1,3000,4.24827e+34,4.29281e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.03offRun.root",hOffSet037_2, heOffSet037_2,3000,4.14972e+34,4.28517e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.05offRun.root",hOffSet037_3, heOffSet037_3,3000,4.27521e+34,4.2755e+34 ); 
    FillHistogramsFromTree("../GuineaPig/0.372-0.07offRun.root",hOffSet037_4, heOffSet037_4,3000,4.26585e+34,4.26574e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.09offRun.root",hOffSet037_5, heOffSet037_5,3000,4.25117e+34,4.25151e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.11offRun.root",hOffSet037_6, heOffSet037_6,3000,4.24056e+34,4.2383e+34 );
    FillHistogramsFromTree("../GuineaPig/0.372-0.13offRun.root",hOffSet037_7, heOffSet037_7,3000,4.22051e+34,4.22193e+34);
     FillHistogramsFromTree("../GuineaPig/0.372-0.15offRun.root",hOffSet037_8, heOffSet037_8,3000,4.2041e+34,4.20313e+34); 
    FillHistogramsFromTree("../GuineaPig/0.372-0.17offRun.root",hOffSet037_9, heOffSet037_9,3000,4.18504e+34,4.1839e+34 );
  FillHistogramsFromTree("../GuineaPig/0.372-0.19offRun.root",hOffSet037_10, heOffSet037_10,3000,4.15716e+34,4.15923e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.21offRun.root",hOffSet037_11, heOffSet037_11,3000,4.13762e+34,4.13943e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.23offRun.root",hOffSet037_12, heOffSet037_12,3000,4.11268e+34,4.11258e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.25offRun.root",hOffSet037_13, heOffSet037_13,3000,4.08829e+34,4.0881e+34 );
   FillHistogramsFromTree("../GuineaPig/0.372-0.27offRun.root",hOffSet037_14, heOffSet037_14,3000,4.0645e+34,4.06345e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.29offRun.root",hOffSet037_15, heOffSet037_15,3000,4.03541e+34,4.03442e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.31offRun.root",hOffSet037_16, heOffSet037_16,3000,4.00661e+34,4.00366e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.33offRun.root",hOffSet037_17, heOffSet037_17,3000,3.97612e+34,3.97514e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.35offRun.root",hOffSet037_18, heOffSet037_18,3000,3.94799e+34,3.94393e+34); 
  FillHistogramsFromTree("../GuineaPig/0.372-0.37offRun.root",hOffSet037_19, heOffSet037_19,3000,3.91692e+34,3.91175e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.39offRun.root",hOffSet037_20, heOffSet037_20,3000,3.88201e+34,3.88602e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.41offRun.root",hOffSet037_21, heOffSet037_21,3000,3.85228e+34,3.85182e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.45offRun.root",hOffSet037_22, heOffSet037_22,3000,3.81736e+34,3.78869e+34); 
  FillHistogramsFromTree("../GuineaPig/0.372-0.47offRun.root",hOffSet037_23, heOffSet037_23,3000,3.78632e+34,3.74944e+34);
													    

                                                                                                                                                              
  //////////////////////
  ///.170 0.27 0.47 0.57 0.67 0.87 1;

//// 0.47 0.57 0.67 0.87 1;
TCanvas *l37_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  hOffSet037_0->GetYaxis()->CenterTitle(true);
  hOffSet037_0->GetYaxis()->SetTitleSize(0.03);
  hOffSet037_0->GetXaxis()->SetTitle("E_{eff}");
  hOffSet037_0->GetXaxis()->CenterTitle(true);
  hOffSet037_0->GetXaxis()->SetTitleSize(0.03);
  hOffSet037_0->GetYaxis()->SetTitleOffset(1.5);
  hOffSet037_0->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  hOffSet037_0->Draw();
  hOffSet037_0->SetLineColor(kBlack);
  hOffSet037_1->Draw("same");
  hOffSet037_1->SetLineColor(kBlack);
  hOffSet037_5->Draw("same");
  hOffSet037_5->SetLineColor(kBlack);
  hOffSet037_11->Draw("same");
  hOffSet037_11->SetLineColor(kBlack);
  hOffSet037_17->Draw("same");
  hOffSet037_17->SetLineColor(kBlack);
  hOffSet037_23->Draw("same");
  hOffSet037_23->SetLineColor(kBlack);
  TLegend* legC37 = new TLegend(0.8,0.09,0.98,0.9);  
  legC37->SetHeader("N_PART dependence}");
  legC37->SetTextSize(0.02);// set size of text
  // legC4->SetNColumns(2);  
  legC37->SetFillColor(0);// Have a white background 0.193 0.386 0.579 0.965 1.158
  legC37->AddEntry(hOffSet037_1, "Dft OffSet=0.001", "l");
  legC37->AddEntry(hOffSet037_5, "Dft OffSet=0.09", "l");
  legC37->AddEntry(hOffSet037_11, "Dft OffSet=0.21", "l");
  legC37->AddEntry(hOffSet037_16, "Dft OffSet=0.33", "l");
  legC37->AddEntry(hOffSet037_23, "Dft OffSet=0.47", "l");
  legC37->AddEntry(hOffSet037_0, "Dft OffSet=0.0", "l");
  legC37->Draw();
  l37_1->SetGridx(1);
  l37_1->SetGridy(1);
  hOffSet037_0->SetAxisRange(1,5e+33,"Y");
  hOffSet037_0->SetAxisRange(0.98,1.006,"X");
  l37_1->SaveAs("L_distribution_OffSet37.png");
  hOffSet037_0->SetAxisRange(0.9,1.04,"X");  
hOffSet037_0->SetAxisRange(0,2500e+30,"Y"); 
  l37_1->SaveAs("L_distribution_OffSet37_zoom.png");
  hOffSet037_0->SetAxisRange(1e+30,5e+33,"Y");
  l37_1->SetLogy(1);
  hOffSet037_0->SetAxisRange(0.98,1.006,"X");
  l37_1->SaveAs("L_distribution_OffSet37_Logy.png");

  /////////////////////////////
 
  TCanvas *l37 = new TCanvas("E1/Enom : E2/nom n_part=37","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  l37->Divide(6,3);
  l37->cd(1);
  heOffSet037_0->Draw("colz");
  heOffSet037_0->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_0->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(2);
  heOffSet037_1->Draw("colz");
  heOffSet037_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_1->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(3);
  heOffSet037_2->Draw("colz");
  heOffSet037_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_2->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(4);
  heOffSet037_3->Draw("colz");
  heOffSet037_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_3->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(5);
  heOffSet037_4->Draw("colz");
  heOffSet037_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_4->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(6);
  heOffSet037_5->Draw("colz");
  heOffSet037_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_5->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(7);
  heOffSet037_6->Draw("colz");
  heOffSet037_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_6->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(8);
  heOffSet037_7->Draw("colz");
  heOffSet037_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_7->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(9);
  heOffSet037_8->Draw("colz");
  heOffSet037_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_8->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(10);
  heOffSet037_9->Draw("colz");
  heOffSet037_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_9->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(11);
  heOffSet037_10->Draw("colz");
  heOffSet037_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_10->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(12);
  heOffSet037_11->Draw("colz");
  heOffSet037_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_11->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(13);
  heOffSet037_12->Draw("colz");
  heOffSet037_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_12->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(14);
  heOffSet037_13->Draw("colz");
  heOffSet037_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_13->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(15);
  heOffSet037_14->Draw("colz");
  heOffSet037_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_14->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(16);
  heOffSet037_15->Draw("colz");
  heOffSet037_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_15->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(17);
  heOffSet037_16->Draw("colz");
  heOffSet037_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_16->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(18);
  heOffSet037_17->Draw("colz");
  heOffSet037_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_17->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(19);
  heOffSet037_18->Draw("colz");
  heOffSet037_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_18->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(20);
  heOffSet037_19->Draw("colz");
  heOffSet037_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_19->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(21);
  heOffSet037_20->Draw("colz");
  heOffSet037_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_20->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(22);
  heOffSet037_21->Draw("colz");
  heOffSet037_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_21->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(23);
  heOffSet037_22->Draw("colz");
  heOffSet037_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_22->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(24);
  heOffSet037_23->Draw("colz");
  heOffSet037_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_23->SetAxisRange(0.98,1.0063,"Y");
  l37->SaveAs("Lumi2D_OffSet-037.png");
  ////


TCanvas *m0 = new TCanvas("m0","offset = 0",1800,900);
 m0->Divide(2,1);
 m0->cd(1);
 heOffSet037_0->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_0->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_0->SetAxisRange(0,1.2e+32,"Z");
 //heOffSet037_0->SetAxisRange(0.98,1.0063,"Y"); 
heOffSet037_0->Draw("colz");
m0->SetGridx(1);
 m0->SetGridy(1);

 m0->cd(2);
 hOffSet037_0->Draw();
hOffSet037_0->SetAxisRange(1,2.5e+33,"Y");
hOffSet037_0->SetAxisRange(0.98,1.006,"X");
 m0->SaveAs("../offset/37/0.png");



TCanvas *m1 = new TCanvas("1","",1800,900);
 m1->Divide(2,1);
 m1->cd(1);
 heOffSet037_1->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_1->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_1->SetAxisRange(0,1.2e+32,"Z");
heOffSet037_1->Draw("colz");
 m1->cd(2);
 hOffSet037_1->Draw();
hOffSet037_1->SetAxisRange(1,2.5e+33,"Y");
hOffSet037_1->SetAxisRange(0.98,1.006,"X");
m1->SaveAs("../offset/37/1.png");

TCanvas *m2 = new TCanvas("m2","",1800,900);
 m2->Divide(2,1);
 m2->cd(1);
 heOffSet037_2->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_2->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_2->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_2->Draw("colz");
 m2->cd(2);
 hOffSet037_2->Draw();
 hOffSet037_2->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_2->SetAxisRange(0.98,1.006,"X");
 m2->SaveAs("../offset/37/2.png");

TCanvas *m3 = new TCanvas("m3","",1800,900);
 m3->Divide(2,1);
 m3->cd(1);
 heOffSet037_3->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_3->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_3->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_3->Draw("colz");
 m3->cd(2);
 hOffSet037_3->Draw();
 hOffSet037_3->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_3->SetAxisRange(0.98,1.006,"X");
 m3->SaveAs("../offset/37/3.png");



TCanvas *m4 = new TCanvas("m4","",1800,900);
 m4->Divide(2,1);
 m4->cd(1);
 heOffSet037_4->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_4->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_4->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_4->Draw("colz");
 m4->cd(2);
 hOffSet037_4->Draw();
 hOffSet037_4->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_4->SetAxisRange(0.98,1.006,"X");
 m4->SaveAs("../offset/37/4.png");



TCanvas *m5 = new TCanvas("m5","",1800,900);
 m5->Divide(2,1);
 m5->cd(1);
 heOffSet037_5->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_5->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_5->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_5->Draw("colz");
 m5->cd(2);
 hOffSet037_5->Draw();
 hOffSet037_5->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_5->SetAxisRange(0.98,1.006,"X");
 m5->SaveAs("../offset/37/5.png");



TCanvas *m6 = new TCanvas("m6","",1800,900);
 m6->Divide(2,1);
 m6->cd(1);
 heOffSet037_6->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_6->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_6->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_6->Draw("colz");
 m6->cd(2);
 hOffSet037_6->Draw();
 hOffSet037_6->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_6->SetAxisRange(0.98,1.006,"X");
 m6->SaveAs("../offset/37/6.png");


TCanvas *m7= new TCanvas("m7","",1800,900);
 m7->Divide(2,1);
 m7->cd(1);
 heOffSet037_7->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_7->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_7->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_7->Draw("colz");
 m7->cd(2);
 hOffSet037_7->Draw();
 hOffSet037_7->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_7->SetAxisRange(0.98,1.006,"X");
 m7->SaveAs("../offset/37/7.png");



TCanvas *m8 = new TCanvas("m8","",1800,900);
 m8->Divide(2,1);
 m8->cd(1);
 heOffSet037_8->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_8->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_8->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_8->Draw("colz");
 m8->cd(2);
 hOffSet037_8->Draw();
 hOffSet037_8->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_8->SetAxisRange(0.98,1.006,"X");
 m8->SaveAs("../offset/37/8.png");



TCanvas *m9 = new TCanvas("9","",1800,900);
 m9->Divide(2,1);
 m9->cd(1);
 heOffSet037_9->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_9->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_9->SetAxisRange(0,1.2e+32,"Z");
heOffSet037_9->Draw("colz");
 m9->cd(2);
 hOffSet037_9->Draw();
hOffSet037_9->SetAxisRange(1,2.5e+33,"Y");
hOffSet037_9->SetAxisRange(0.98,1.006,"X");
m9->SaveAs("../offset/37/9.png");


TCanvas *m10 = new TCanvas("10","",1800,900);
 m10->Divide(2,1);
 m10->cd(1);
 heOffSet037_10->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_10->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_10->SetAxisRange(0,1.2e+32,"Z");
heOffSet037_10->Draw("colz");
 m10->cd(2);
 hOffSet037_10->Draw();
hOffSet037_10->SetAxisRange(1,2.5e+33,"Y");
hOffSet037_10->SetAxisRange(0.98,1.006,"X");
m10->SaveAs("../offset/37/10.png");



TCanvas *m11 = new TCanvas("11","",1800,900);
 m11->Divide(2,1);
 m11->cd(1);
 heOffSet037_11->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_11->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_11->SetAxisRange(0,1.2e+32,"Z");
heOffSet037_11->Draw("colz");
 m11->cd(2);
 hOffSet037_11->Draw();
hOffSet037_11->SetAxisRange(1,2.5e+33,"Y");
hOffSet037_11->SetAxisRange(0.98,1.006,"X");
m11->SaveAs("../offset/37/11.png");

TCanvas *m12 = new TCanvas("m12","",1800,900);
 m12->Divide(2,1);
 m12->cd(1);
 heOffSet037_12->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_12->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_12->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_12->Draw("colz");
 m12->cd(2);
 hOffSet037_12->Draw();
 hOffSet037_12->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_12->SetAxisRange(0.98,1.006,"X");
 m12->SaveAs("../offset/37/12.png");

TCanvas *m13 = new TCanvas("m13","",1800,900);
 m13->Divide(2,1);
 m13->cd(1);
 heOffSet037_13->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_13->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_13->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_13->Draw("colz");
 m13->cd(2);
 hOffSet037_13->Draw();
 hOffSet037_13->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_13->SetAxisRange(0.98,1.006,"X");
 m13->SaveAs("../offset/37/13.png");



TCanvas *m14 = new TCanvas("m14","",1800,900);
 m14->Divide(2,1);
 m14->cd(1);
 heOffSet037_14->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_14->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_14->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_14->Draw("colz");
 m14->cd(2);
 hOffSet037_14->Draw();
 hOffSet037_14->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_14->SetAxisRange(0.98,1.006,"X");
 m14->SaveAs("../offset/37/14.png");



TCanvas *m15 = new TCanvas("m15","",1800,900);
 m15->Divide(2,1);
 m15->cd(1);
 heOffSet037_15->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_15->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_15->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_15->Draw("colz");
 m15->cd(2);
 hOffSet037_15->Draw();
 hOffSet037_15->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_15->SetAxisRange(0.98,1.006,"X");
 m15->SaveAs("../offset/37/15.png");



TCanvas *m16 = new TCanvas("m16","",1800,900);
 m16->Divide(2,1);
 m16->cd(1);
 heOffSet037_16->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_16->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_16->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_16->Draw("colz");
 m16->cd(2);
 hOffSet037_16->Draw();
 hOffSet037_16->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_16->SetAxisRange(0.98,1.006,"X");
 m16->SaveAs("../offset/37/16.png");


TCanvas *m17= new TCanvas("m17","",1800,900);
 m17->Divide(2,1);
 m17->cd(1);
 heOffSet037_17->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_17->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_17->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_17->Draw("colz");
 m17->cd(2);
 hOffSet037_17->Draw();
 hOffSet037_17->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_17->SetAxisRange(0.98,1.006,"X");
 m17->SaveAs("../offset/37/17.png");



TCanvas *m18 = new TCanvas("m18","",1800,900);
 m18->Divide(2,1);
 m18->cd(1);
 heOffSet037_18->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_18->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_18->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_18->Draw("colz");
 m18->cd(2);
 hOffSet037_18->Draw();
 hOffSet037_18->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_18->SetAxisRange(0.98,1.006,"X");
 m18->SaveAs("../offset/37/18.png");



TCanvas *m19 = new TCanvas("m19","",1800,900);
 m19->Divide(2,1);
 m19->cd(1);
 heOffSet037_19->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_19->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_19->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_19->Draw("colz");
 m19->cd(2);
 hOffSet037_19->Draw();
 hOffSet037_19->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_19->SetAxisRange(0.98,1.006,"X");
 m19->SaveAs("../offset/37/19.png");


TCanvas *m20 = new TCanvas("m20","",1800,900);
 m20->Divide(2,1);
 m20->cd(1);
 heOffSet037_20->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_20->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_20->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_20->Draw("colz");
 m20->cd(2);
 hOffSet037_20->Draw();
 hOffSet037_20->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_20->SetAxisRange(0.98,1.006,"X");
 m20->SaveAs("../offset/37/20.png");


TCanvas *m21 = new TCanvas("m21","",1800,900);
 m21->Divide(2,1);
 m21->cd(1);
 heOffSet037_21->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_21->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_21->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_21->Draw("colz");
 m21->cd(2);
 hOffSet037_21->Draw();
 hOffSet037_21->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_21->SetAxisRange(0.98,1.006,"X");
 m21->SaveAs("../offset/37/21.png");

TCanvas *m22 = new TCanvas("m22","",1800,900);
 m22->Divide(2,1);
 m22->cd(1);
 heOffSet037_22->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_22->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_22->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_22->Draw("colz");
 m22->cd(2);
 hOffSet037_22->Draw();
 hOffSet037_22->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_22->SetAxisRange(0.98,1.006,"X");
 m22->SaveAs("../offset/37/22.png");

TCanvas *m23 = new TCanvas("m23","",1800,900);
 m23->Divide(2,1);
 m23->cd(1);
 heOffSet037_23->SetAxisRange(0.98,1.0063,"X");
 heOffSet037_23->SetAxisRange(0.98,1.0063,"Y");
 heOffSet037_23->SetAxisRange(0,1.2e+32,"Z");
 heOffSet037_23->Draw("colz");
 m23->cd(2);
 hOffSet037_23->Draw();
 hOffSet037_23->SetAxisRange(1,2.5e+33,"Y");
 hOffSet037_23->SetAxisRange(0.98,1.006,"X");
 m23->SaveAs("../offset/37/23.png");


TCanvas *c3 = new TCanvas("L distr part_n","L distribution depends on number of particles");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  hOffSet037_0->GetYaxis()->CenterTitle(true);
  hOffSet037_0->GetYaxis()->SetTitleSize(0.03);
  hOffSet037_0->GetXaxis()->SetTitle("E_{eff}");
  hOffSet037_0->GetXaxis()->CenterTitle(true);
  hOffSet037_0->GetXaxis()->SetTitleSize(0.03);
  hOffSet037_0->GetYaxis()->SetTitleOffset(1.5);
  hOffSet037_0->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
 hOffSet037_0->SetLineColor(kBlack);
 hOffSet037_0->Draw();
    
  hOffSet017_1->SetLineColor(kRed);
  hOffSet017_1->Draw("same");

 hOffSet027_1->SetLineColor(kRed+2);
 hOffSet027_1->Draw("same");

 hOffSet057_1->SetLineColor(kBlue);
 hOffSet057_1->Draw("same");

 hOffSet087_1->SetLineColor(kBlue+2);
 hOffSet087_1->Draw("same");
 hOffSet087_1->SetLineColor(kGreen);
  // legC3->SetNColumns(2);  
  TLegend* legC3 = new TLegend(0.8,0.09,0.98,0.9);  
  legC3->SetHeader("N_part*1e10");
  legC3->SetTextSize(0.05);// set size of text
 legC3->SetFillColor(0);// Have a white background
   legC3->AddEntry(hOffSet037_0, "0.372", "l");
   legC3->AddEntry(hOffSet017_1, "0.17", "l");
   legC3->AddEntry(hOffSet057_1, "0.27", "l");
   legC3->AddEntry(hOffSet057_1, "0.57", "l");
   legC3->AddEntry(hOffSet057_1, "0.87", "l");
   // legC3->AddEntry(hBETA_Y6, "BETA_Y=10.0", "l");
   // legC3->AddEntry(hBETA_Y7, "BETA_Y=25.0", "l");
   //legC3->AddEntry(h3000g, "Dft BETA_Y=0.15", "l");
   legC3->Draw();
  c3->SetGridx(1);
  c3->SetGridy(1);
  
  hOffSet037_0->SetAxisRange(0,8e+33,"Y");
  hOffSet037_0->SetAxisRange(0,100,"X");

  c3->SaveAs("L_distribution_number.png");

  hOffSet037_0->SetAxisRange(0.99,1.006,"X");
  c3->SaveAs("L_distribution_number_zoom.png");

  hOffSet037_0->SetAxisRange(0,100,"X");
  hOffSet037_0->SetAxisRange(1e+28,4e+33,"Y");
  c3->SetLogy(1);//  c->SetLogy(0);
  c3->SaveAs("L_distribution_number_Logy.png");









  //// 0.47 0.57 0.67 0.87 1;
return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////

void FillHistogramsFromTree(TString filename, TH1D* h1D, TH2D* h2D, double nominalCMSEnergy, double lumi, double lumi1) {

  //Open File
  TFile *file = TFile::Open(filename);  //Get the Tree
  TTree *tree1;
  file->GetObject("lumiTree",tree1);

  //Define variabels for Tree
  Double_t E1, E2;
  tree1->SetBranchAddress("E1", &E1);
  tree1->SetBranchAddress("E2", &E2);

 
  //Loop over the tree
  int n_entries =  tree1->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree1->GetEntry(i);
    h1D->Fill(2*sqrt(E1*E2)/nominalCMSEnergy);
    h2D->Fill(E1/(nominalCMSEnergy/2),E2/(nominalCMSEnergy/2));
  }
 
  Double_t scalingFactor = lumi/(h1D->Integral()); 
  Double_t scalingFactor1 = lumi1/(h2D->Integral());
  h1D->Scale(scalingFactor);
  h2D->Scale(scalingFactor1);
 
  std::cout << "1Dhisto         " << h1D->Integral() << "               " << h1D->GetEntries() << std::endl;
  std::cout << "2Dhisto         " << h2D->Integral() << "               " << h2D->GetEntries() << std::endl;

  file->Close();
  delete file;
  return;
}



