#!/bin/bash

ENERGY=1500
N_PART=0.372
BETA_X=8.0
BETA_Y=0.068
EMIT_X=0.68
EMIT_Y=0.02
espread=0.01
offset=0

./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $espread $offset DEFAULT_VALUES


 # for N_PART in 0.170 0.27 0.47 0.57 0.67 0.87; do  
 # for offset in 0.01 0.03 0.05 0.07 0.09 0.11 0.13 0.15 0.17 0.19 0.21 0.23 0.25 0.27 0.29 0.31 0.33 0.35 0.37 0.39 0.41 0.43 0.45 0.47; do
 #      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y  $offset $FILENAME ${N_PART}-${offset}
 # done

# done

# for offset in 0 0.01 0.03 0.05 0.07 0.09 0.11 0.13 0.15 0.17 0.19 0.21 0.23 0.25 0.27 0.29 0.31 0.33 0.35 0.37 0.39 0.41 0.43 0.45 0.47; do
#      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y  $offset $FILENAME ${N_PART}-${offset}
# done

  
for espread in 0.001 0.01 0.05 0.1; do 

 
  for EMIT_X in 0.08 0.28 0.48 0.68  0.88 1.08;  do
      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $espread $offset EMIT_X_${EMIT_X}-${espread}
  done                       
##REVERT VALUE BACK TO ORIGINAL
 EMIT_X=0.68
# ## And NOW THE REST OF THE PARAMETERS 

   

   for EMIT_Y in 0.008 0.014 0.02 0.026 0.032 0.038;  do
      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $espread $offset EMIT_Y_${EMIT_Y}-${espread}
  done
 # ##REVERT VALUE BACK TO ORIGINAL
 EMIT_Y=0.02
 ##And NOW THE REST OF THE PARAMETERS 

#  for N_PART in 0.193 0.386 0.579 0.965 1.158;  do
#        ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $offset $FILENAME N_PART_${N_PART}
#  done
#  N_PART=0.772
 

 for BETA_X in 0.8 3.2 5.6 8 10.4 14.8; do
      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $espread $offset BETA_X${BETA_X}-${espread} 
  done
  BETA_X=8.0

 for BETA_Y in 0.015 0.06 0.068 0.105 0.195 0.24; do
      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $espread $offset BETA_Y_${BETA_Y}-${espread}
 done
  BETA_Y=0.068

done

