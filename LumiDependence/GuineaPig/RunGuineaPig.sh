#!/bin/bash


ENERGY=1500
N_PART=0.772

BETA_X=8.0
BETA_Y=0.15

for EMIT_X in 0.01 0.07 0.1 0.3 0.5 0.75 1.0; do
#  if [ $# -ne  3 ]; then
#      echo "Not Enough Parameters"
#      exit 1
#  fi
EMIT_Y=0.02
FILENAME=${EMIT_X}


cat > acc.dat <<EOF
\$ACCELERATOR:: test
{
energy=${ENERGY};dist_z=0;f_rep=50.0;n_b=312;waist_y=0;
particles=${N_PART};
sigma_z=45;
beta_x=${BETA_X};
beta_y=${BETA_Y};
emitt_x=${EMIT_X};
emitt_y=${EMIT_Y};
espread=0.01;which_espread=3;
}


\$PARAMETERS::default
{
n_x=128;n_y=256;n_z=25;n_t=1;n_m=150000;cut_x=400.0;cut_y=15.0;cut_z=3.0*sigma_z.1;force_symmetric=0;electron_ratio=1;do_photons=1;store_photons=0;ecm_min=2970.0;photon_ratio=1;do_espread=1;do_coherent=0;grids=7;angle_y=0.5*0.0;do_eloss=1;do_pairs=0;track_pairs=0;store_pairs=0;do_compt=0;photon_ratio=1;load_beam=0;store_beam=1;hist_ee_bins=1010;hist_ee_max=2.02*energy.1;charge_sign=-1.0;do_lumi=1;num_lumi=3000000;lumi_p=1e-28;
}

EOF

guinea test default ${FILENAME}R7un

../build/GuineaPig/ReadGPFile ${FILENAME}R7un.root lumi.ee.out

done

for EMIT_X in 0.01 0.07 0.1 0.3 0.5 0.75 1.0; do
#  if [ $# -ne  3 ]; then
#      echo "Not Enough Parameters"
#      exit 1
#  fi
EMIT_Y=0.02
FILENAME=${EMIT_X}


cat > acc.dat <<EOF
\$ACCELERATOR:: test
{
energy=${ENERGY};dist_z=0;f_rep=50.0;n_b=312;waist_y=0;
particles=${N_PART};
sigma_z=45;
beta_x=${BETA_X};
beta_y=${BETA_Y};
emitt_x=${EMIT_X};
emitt_y=${EMIT_Y};
espread=0.01;which_espread=3;
}


\$PARAMETERS::default
{
n_x=128;n_y=256;n_z=25;n_t=1;n_m=150000;cut_x=400.0;cut_y=15.0;cut_z=3.0*sigma_z.1;force_symmetric=0;electron_ratio=1;do_photons=1;store_photons=0;ecm_min=2970.0;photon_ratio=1;do_espread=1;do_coherent=1;grids=7;angle_y=0.5*0.0;do_eloss=1;do_pairs=0;track_pairs=0;store_pairs=0;do_compt=0;photon_ratio=1;load_beam=0;store_beam=1;hist_ee_bins=1010;hist_ee_max=2.02*energy.1;charge_sign=-1.0;do_lumi=1;num_lumi=3000000;lumi_p=1e-28;
}

EOF

guinea test default ${FILENAME}R7un

../build/GuineaPig/ReadGPFile ${FILENAME}R7un.root lumi.ee.out

done

