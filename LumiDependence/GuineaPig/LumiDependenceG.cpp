#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <cmath>
#include <iostream> 

void FillHistogramsFromTree(TString filename, TH1D* h1D, TH2D* h2D, double nominalCMSEnergy=3000.0, double lumi=1.0, double lumi1=1.0);


int main() {

  gStyle->SetOptStat(0);


   //Create Histogram
  Int_t bins=1500;
  Double_t xLimit =1.1;
  // THE DEFAULT
  TH1D *h3000g = new TH1D("h3000g","Luminosity dependence 3 TeV ",bins,0, xLimit);
  TH2D *heE3000g = new TH2D("heE3000g","Default Luminosity at 3 TeV",bins,0,1.1,bins,0,1.1); 
  ////0.01 0.03 0.05 0.07 0.09 0.11 0.13 0.15 0.17 0.19 0.21 0.23 0.25 0.27 0.29 0.31 0.33 0.35 0.37 0.39 0.41 0.43 0.45 0.47

   TH1D *hOffSet017_0 = new TH1D("hOffSet017_0","OffSet= 0",bins,0, xLimit);
   TH1D *hOffSet027_0 = new TH1D("hOffSet027_0","OffSet= 0",bins,0, xLimit);
   TH2D *heOffSet017_0 = new TH2D("heOffSet017-0","OffSet= 0  N_Part=0.17 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet027_0 = new TH2D("heOffSet027-0","OffSet= 0  N_Part=0.27 at 3 TeV",bins,0,0,bins,0,0); 

 
  TH1D *hOffSet037_1 = new TH1D("hOffSet037_1","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_2 = new TH1D("hOffSet037_2","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_3 = new TH1D("hOffSet037_3","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_4 = new TH1D("hOffSet037_4","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_5 = new TH1D("hOffSet037_5","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_6 = new TH1D("hOffSet037_6","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_7 = new TH1D("hOffSet037_7","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_8 = new TH1D("hOffSet037_8","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_9 = new TH1D("hOffSet037_9","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_10 = new TH1D("hOffSet037_10","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_11 = new TH1D("hOffSet037_11","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_12 = new TH1D("hOffSet037_12","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_13 = new TH1D("hOffSet037_13","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_14 = new TH1D("hOffSet037_14","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_15 = new TH1D("hOffSet037_15","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_16 = new TH1D("hOffSet037_16","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_17 = new TH1D("hOffSet037_17","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_18 = new TH1D("hOffSet037_18","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_19 = new TH1D("hOffSet037_19","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_20 = new TH1D("hOffSet037_20","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet037_21 = new TH1D("hOffSet037_21","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet037_22 = new TH1D("hOffSet037_22","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet037_23 = new TH1D("hOffSet037_23","OffSet= 0.4",bins,0, xLimit);

   TH2D *heOffSet037_1 = new TH2D("heOffSet037-1","OffSet= 0.01  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_2 = new TH2D("heOffSet037-2","OffSet= 0.03  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_3 = new TH2D("heOffSet037-3","OffSet= 0.05  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_4 = new TH2D("heOffSet037-4","OffSet= 0.07  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_5 = new TH2D("heOffSet037-5","OffSet= 0.09  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_6 = new TH2D("heOffSet037-6","OffSet= 0.11  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_7 = new TH2D("heOffSet037-7","OffSet= 0.13  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_8 = new TH2D("heOffSet037-8","OffSet= 0.15  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet037_9 = new TH2D("heOffSet037-9","OffSet= 0.17  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_10 = new TH2D("heOffSet037-10","OffSet= 0.19  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_11 = new TH2D("heOffSet037-11","OffSet= 0.21  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_12 = new TH2D("heOffSet037-12","OffSet= 0.23  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_14 = new TH2D("heOffSet037-14","OffSet= 0.27  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_13 = new TH2D("heOffSet037-13","OffSet= 0.25  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_15 = new TH2D("heOffSet037-15","OffSet= 0.29  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_16 = new TH2D("heOffSet037-16","OffSet= 0.31  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_17 = new TH2D("heOffSet037-17","OffSet= 0.33  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_18 = new TH2D("heOffSet037-18","OffSet= 0.35  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_19 = new TH2D("heOffSet037-19","OffSet= 0.37  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_20 = new TH2D("heOffSet037-20","OffSet= 0.39  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_21 = new TH2D("heOffSet037-21","OffSet= 0.41  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_22 = new TH2D("heOffSet037-22","OffSet= 0.43  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet037_23 = new TH2D("heOffSet037-23","OffSet= 0.45  N_Part=0.37 at 3 TeV",bins,0,0,bins,0,0); 
  /// 057-



  //for variation of "OffSet"=   0.01 0.03 0.05 0.07 0.09 0.11 0.13 0.15 0.17 0.19 0.21 0.23 0.25 0.12 0.29 0.31 0.33 0.35 0.37 0.39 0.41 0.43 0.45 0.47
    ///// 0.170 0.27 0.47 0.57 0.67 0.87 1;
   TH1D *hOffSet017_1 = new TH1D("hOffSet1","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_2 = new TH1D("hOffSet2","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_3 = new TH1D("hOffSet3","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_4 = new TH1D("hOffSet4","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_5 = new TH1D("hOffSet5","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_6 = new TH1D("hOffSet6","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_7 = new TH1D("hOffSet7","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_8 = new TH1D("hOffSet8","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_9 = new TH1D("hOffSet9","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_10 = new TH1D("hOffSet10","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_11 = new TH1D("hOffSet11","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_12 = new TH1D("hOffSet12","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_13 = new TH1D("hOffSet13","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_14 = new TH1D("hOffSet14","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_15 = new TH1D("hOffSet15","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_16 = new TH1D("hOffSet16","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_17 = new TH1D("hOffSet17","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_18 = new TH1D("hOffSet18","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_19 = new TH1D("hOffSet19","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_20 = new TH1D("hOffSet20","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet017_21 = new TH1D("hOffSet21","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet017_22 = new TH1D("hOffSet22","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet017_23 = new TH1D("hOffSet23","OffSet= 0.4",bins,0, xLimit);
   /////

  ///// 0.170 0.27 0.47 0.57 0.67 0.87 1;
  TH1D *hOffSet027_1 = new TH1D("hOffSet1","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_2 = new TH1D("hOffSet2","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_3 = new TH1D("hOffSet3","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_4 = new TH1D("hOffSet4","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_5 = new TH1D("hOffSet5","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_6 = new TH1D("hOffSet6","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_7 = new TH1D("hOffSet7","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_8 = new TH1D("hOffSet8","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_9 = new TH1D("hOffSet9","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_10 = new TH1D("hOffSet10","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_11 = new TH1D("hOffSet11","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_12 = new TH1D("hOffSet12","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_13 = new TH1D("hOffSet13","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_14 = new TH1D("hOffSet14","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_15 = new TH1D("hOffSet15","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_16 = new TH1D("hOffSet16","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_17 = new TH1D("hOffSet17","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_18 = new TH1D("hOffSet18","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_19 = new TH1D("hOffSet19","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_20 = new TH1D("hOffSet20","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet027_21 = new TH1D("hOffSet21","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet027_22 = new TH1D("hOffSet22","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet027_23 = new TH1D("hOffSet23","OffSet= 0.4",bins,0, xLimit);
  /////

  /////



  // 0.170 0.27 0.47 0.57 0.67 0.87 1
  TH2D *heOffSet017_1 = new TH2D("heOffSet017-1","OffSet= 0.01 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_2 = new TH2D("heOffSet017-2","OffSet= 0.03 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_3 = new TH2D("heOffSet017-3","OffSet= 0.05 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_4 = new TH2D("heOffSet017-4","OffSet= 0.07 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_5 = new TH2D("heOffSet017-5","OffSet= 0.09 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_6 = new TH2D("heOffSet017-6","OffSet= 0.11 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_7 = new TH2D("heOffSet017-7","OffSet= 0.13 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_8 = new TH2D("heOffSet017-8","OffSet= 0.15 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_9 = new TH2D("heOffSet017-9","OffSet= 0.17 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_10 = new TH2D("heOffSet017-10","OffSet= 0.19 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_11 = new TH2D("heOffSet017-11","OffSet= 0.21 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_12 = new TH2D("heOffSet017-12","OffSet= 0.23 at 0017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_14 = new TH2D("heOffSet017-14","OffSet= 0.27 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_13 = new TH2D("heOffSet017-13","OffSet= 0.25 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_15 = new TH2D("heOffSet017-15","OffSet= 0.29 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_16 = new TH2D("heOffSet017-16","OffSet= 0.31 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_17 = new TH2D("heOffSet017-17","OffSet= 0.33 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_18 = new TH2D("heOffSet017-18","OffSet= 0.35 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_19 = new TH2D("heOffSet017-19","OffSet= 0.37 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_20 = new TH2D("heOffSet017-20","OffSet= 0.39 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_21 = new TH2D("heOffSet017-21","OffSet= 0.41 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_22 = new TH2D("heOffSet017-22","OffSet= 0.43 at 017 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet017_23 = new TH2D("heOffSet017-23","OffSet= 0.45 at 017 3 TeV",bins,0,0,bins,0,0); 
  ///  0.27
  TH2D *heOffSet027_1 = new TH2D("heOffSet027-1","OffSet= 0.01 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_2 = new TH2D("heOffSet027-2","OffSet= 0.03 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_3 = new TH2D("heOffSet027-3","OffSet= 0.05 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_4 = new TH2D("heOffSet027-4","OffSet= 0.07 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_5 = new TH2D("heOffSet027-5","OffSet= 0.09 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_6 = new TH2D("heOffSet027-6","OffSet= 0.11 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_7 = new TH2D("heOffSet027-7","OffSet= 0.13 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_8 = new TH2D("heOffSet027-8","OffSet= 0.15 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_9 = new TH2D("heOffSet027-9","OffSet= 0.17 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_10 = new TH2D("heOffSet027-10","OffSet= 0.19 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_11 = new TH2D("heOffSet027-11","OffSet= 0.21 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_12 = new TH2D("heOffSet027-12","OffSet= 0.23 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_14 = new TH2D("heOffSet027-14","OffSet= 0.27 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_13 = new TH2D("heOffSet027-13","OffSet= 0.25 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_15 = new TH2D("heOffSet027-15","OffSet= 0.29 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_16 = new TH2D("heOffSet027-16","OffSet= 0.31 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_17 = new TH2D("heOffSet027-17","OffSet= 0.33 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_18 = new TH2D("heOffSet027-18","OffSet= 0.35 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_19 = new TH2D("heOffSet027-19","OffSet= 0.37 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_20 = new TH2D("heOffSet027-20","OffSet= 0.39 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_21 = new TH2D("heOffSet027-21","OffSet= 0.41 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_22 = new TH2D("heOffSet027-22","OffSet= 0.43 at 027- 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet027_23 = new TH2D("heOffSet027-23","OffSet= 0.45 at 027- 3 TeV",bins,0,0,bins,0,0); 



  FillHistogramsFromTree("../GuineaPig/DEFAULT_VALUESoffRun.root",h3000g, heE3000g,3000,2.66233e+35,4.29366e+34);
  //TFile*f = TFile::Open("test.root","RECREATE");
  //heE3000g->Write();
  //f->Close();
  //return 0;
  /////// # for offset in 0.01 0.03 0.05 0.07 0.09 0.11 0.13 0.15 0.17 0.19 0.21 0.23 0.25 0.27 0.29 0.31 0.33 0.35 0.37 0.39 0.41 0.43 0.45 0.47; do
  
 //  0.170-  0.47-* 0.57- 0.67- 0.87 1



  
     FillHistogramsFromTree("../GuineaPig/0.170-0.01offRun.root",hOffSet017_1, heOffSet017_1,3000,7.28626e+33,7.28626e+33);
     FillHistogramsFromTree("../GuineaPig/0.170-0.03offRun.root",hOffSet017_2, heOffSet017_2,3000,7.28308e+33,7.28308e+33);
    FillHistogramsFromTree("../GuineaPig/0.170-0.05offRun.root",hOffSet017_3, heOffSet017_3,3000, 7.27727e+33,7.27727e+33);
    FillHistogramsFromTree("../GuineaPig/0.170-0.07offRun.root",hOffSet017_4, heOffSet017_4,3000, 7.27079e+33,7.27079e+33);
    FillHistogramsFromTree("../GuineaPig/0.170-0.09offRun.root",hOffSet017_5, heOffSet017_5,3000, 7.25896e+33,7.25896e+33);
    FillHistogramsFromTree("../GuineaPig/0.170-0.11offRun.root",hOffSet017_6, heOffSet017_6,3000, 7.24858e+33,7.24858e+33);
    FillHistogramsFromTree("../GuineaPig/0.170-0.13offRun.root",hOffSet017_7, heOffSet017_7,3000, 7.22787e+33,7.22787e+33);
    FillHistogramsFromTree("../GuineaPig/0.170-0.15offRun.root",hOffSet017_8, heOffSet017_8,3000, 7.20725e+33,7.20725e+33); 
    FillHistogramsFromTree("../GuineaPig/0.170-0.17offRun.root",hOffSet017_9, heOffSet017_9,3000, 7.18299e+33,7.18299e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.19offRun.root",hOffSet017_10, heOffSet017_10,3000, 7.15681e+33,7.15681e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.21offRun.root",hOffSet017_11, heOffSet017_11,3000, 7.1292e+33,7.1292e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.23offRun.root",hOffSet017_12, heOffSet017_12,3000, 7.10023e+33,7.10023e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.25offRun.root",hOffSet017_13, heOffSet017_13,3000, 7.06654e+33,7.06654e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.27offRun.root",hOffSet017_14, heOffSet017_14,3000, 7.03052e+33,7.03052e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.29offRun.root",hOffSet017_15, heOffSet017_15,3000, 6.99278e+33,6.99278e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.31offRun.root",hOffSet017_16, heOffSet017_16,3000, 6.9561e+33,6.9561e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.33offRun.root",hOffSet017_17, heOffSet017_17,3000, 6.90998e+33,6.90998e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.35offRun.root",hOffSet017_18, heOffSet017_18,3000, 6.86544e+33,6.86544e+33); 
  FillHistogramsFromTree("../GuineaPig/0.170-0.37offRun.root",hOffSet017_19, heOffSet017_19,3000, 6.81551e+33,6.81551e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.39offRun.root",hOffSet017_20, heOffSet017_20,3000, 6.76731e+33,6.76731e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.41offRun.root",hOffSet017_21, heOffSet017_21,3000, 6.71703e+33,6.71703e+33);
  FillHistogramsFromTree("../GuineaPig/0.170-0.45offRun.root",hOffSet017_22, heOffSet017_22,3000, 6.60618e+33,6.60618e+33); 
  FillHistogramsFromTree("../GuineaPig/0.170-0.47offRun.root",hOffSet017_23, heOffSet017_23,3000, 6.54892e+33,6.54892e+33);
  //// 0.27-											  	      ///////////////////////////////////////////////////////


    FillHistogramsFromTree("../GuineaPig/0.27-0.01offRun.root",hOffSet027_1, heOffSet027_1,3000,2.07128e+34,2.07128e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.03offRun.root",hOffSet027_2, heOffSet027_2,3000,2.07035e+34,2.07035e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.05offRun.root",hOffSet027_3, heOffSet027_3,3000,2.06927e+34,2.06927e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.07offRun.root",hOffSet027_4, heOffSet027_4,3000,2.06563e+34,2.06563e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.09offRun.root",hOffSet027_5, heOffSet027_5,3000,2.06192e+34,2.06192e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.11offRun.root",hOffSet027_6, heOffSet027_6,3000,2.05809e+34,2.05809e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.13offRun.root",hOffSet027_7, heOffSet027_7,3000,2.05213e+34,2.05213e+34);
    FillHistogramsFromTree("../GuineaPig/0.27-0.15offRun.root",hOffSet027_8, heOffSet027_8,3000,2.04659e+34,2.04659e+34); 
    FillHistogramsFromTree("../GuineaPig/0.27-0.17offRun.root",hOffSet027_9, heOffSet027_9,3000,2.03959e+34,2.03959e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.19offRun.root",hOffSet027_10, heOffSet027_10,3000,2.03172e+34,2.03172e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.21offRun.root",hOffSet027_11, heOffSet027_11,3000,2.02494e+34,2.02494e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.23offRun.root",hOffSet027_12, heOffSet027_12,3000,2.01541e+34,2.01541e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.25offRun.root",hOffSet027_13, heOffSet027_13,3000,2.00686e+34,2.00686e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.27offRun.root",hOffSet027_14, heOffSet027_14,3000,1.99635e+34,1.99635e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.29offRun.root",hOffSet027_15, heOffSet027_15,3000,1.98666e+34,1.98666e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.31offRun.root",hOffSet027_16, heOffSet027_16,3000,1.97457e+34,1.97457e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.33offRun.root",hOffSet027_17, heOffSet027_17,3000,1.96325e+34,1.96325e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.35offRun.root",hOffSet027_18, heOffSet027_18,3000,1.95179e+34,1.95179e+34); 
  FillHistogramsFromTree("../GuineaPig/0.27-0.37offRun.root",hOffSet027_19, heOffSet027_19,3000,1.93829e+34,1.93829e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.39offRun.root",hOffSet027_20, heOffSet027_20,3000,1.9264e+34 ,1.9264e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.41offRun.root",hOffSet027_21, heOffSet027_21,3000,1.91377e+34,1.91377e+34);
  FillHistogramsFromTree("../GuineaPig/0.27-0.45offRun.root",hOffSet027_22, heOffSet027_22,3000,1.88569e+34,1.88569e+34); 
  FillHistogramsFromTree("../GuineaPig/0.27-0.47offRun.root",hOffSet027_23, heOffSet027_23,3000,1.87161e+34,1.87161e+34);
  /////37.												    
    FillHistogramsFromTree("../GuineaPig/0.372-0.01offRun.root",hOffSet037_1, heOffSet037_1,3000,4.24827e+34,4.29281e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.03offRun.root",hOffSet037_2, heOffSet037_2,3000,4.14972e+34,4.28517e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.05offRun.root",hOffSet037_3, heOffSet037_3,3000,4.27521e+34,4.2755e+34 ); 
    FillHistogramsFromTree("../GuineaPig/0.372-0.07offRun.root",hOffSet037_4, heOffSet037_4,3000,4.26585e+34,4.26574e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.09offRun.root",hOffSet037_5, heOffSet037_5,3000,4.25117e+34,4.25151e+34);
    FillHistogramsFromTree("../GuineaPig/0.372-0.11offRun.root",hOffSet037_6, heOffSet037_6,3000,4.24056e+34,4.2383e+34 );
    FillHistogramsFromTree("../GuineaPig/0.372-0.13offRun.root",hOffSet037_7, heOffSet037_7,3000,4.22051e+34,4.22193e+34);
     FillHistogramsFromTree("../GuineaPig/0.372-0.15offRun.root",hOffSet037_8, heOffSet037_8,3000,4.2041e+34,4.20313e+34); 
    FillHistogramsFromTree("../GuineaPig/0.372-0.17offRun.root",hOffSet037_9, heOffSet037_9,3000,4.18504e+34,4.1839e+34 );
  FillHistogramsFromTree("../GuineaPig/0.372-0.19offRun.root",hOffSet037_10, heOffSet037_10,3000,4.15716e+34,4.15923e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.21offRun.root",hOffSet037_11, heOffSet037_11,3000,4.13762e+34,4.13943e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.23offRun.root",hOffSet037_12, heOffSet037_12,3000,4.11268e+34,4.11258e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.25offRun.root",hOffSet037_13, heOffSet037_13,3000,4.08829e+34,4.0881e+34 );
   FillHistogramsFromTree("../GuineaPig/0.372-0.27offRun.root",hOffSet037_14, heOffSet037_14,3000,4.0645e+34,4.06345e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.29offRun.root",hOffSet037_15, heOffSet037_15,3000,4.03541e+34,4.03442e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.31offRun.root",hOffSet037_16, heOffSet037_16,3000,4.00661e+34,4.00366e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.33offRun.root",hOffSet037_17, heOffSet037_17,3000,3.97612e+34,3.97514e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.35offRun.root",hOffSet037_18, heOffSet037_18,3000,3.94799e+34,3.94393e+34); 
  FillHistogramsFromTree("../GuineaPig/0.372-0.37offRun.root",hOffSet037_19, heOffSet037_19,3000,3.91692e+34,3.91175e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.39offRun.root",hOffSet037_20, heOffSet037_20,3000,3.88201e+34,3.88602e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.41offRun.root",hOffSet037_21, heOffSet037_21,3000,3.85228e+34,3.85182e+34);
  FillHistogramsFromTree("../GuineaPig/0.372-0.45offRun.root",hOffSet037_22, heOffSet037_22,3000,3.81736e+34,3.78869e+34); 
  FillHistogramsFromTree("../GuineaPig/0.372-0.47offRun.root",hOffSet037_23, heOffSet037_23,3000,3.78632e+34,3.74944e+34);
													    

                                                                                                                                                              
  //////////////////////
  ///.170 0.27 0.47 0.57 0.67 0.87 1;
 
  TCanvas *c17 = new TCanvas("E1/Enom : E2/nom  n_part 17","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  c17->Divide(6,3);
  c17->cd(1);
  heOffSet017_0->Draw("colz");
  heOffSet017_0->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_0->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(2);
  heOffSet017_1->Draw("colz");
  heOffSet017_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_1->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(3);
  heOffSet017_2->Draw("colz");
  heOffSet017_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_2->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(4);
  heOffSet017_3->Draw("colz");
  heOffSet017_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_3->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(5);
  heOffSet017_4->Draw("colz");
  heOffSet017_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_4->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(6);
  heOffSet017_5->Draw("colz");
  heOffSet017_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_5->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(7);
  heOffSet017_6->Draw("colz");
  heOffSet017_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_6->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(8);
  heOffSet017_7->Draw("colz");
  heOffSet017_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_7->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(9);
  heOffSet017_8->Draw("colz");
  heOffSet017_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_8->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(10);
  heOffSet017_9->Draw("colz");
  heOffSet017_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_9->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(11);
  heOffSet017_10->Draw("colz");
  heOffSet017_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_10->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(12);
  heOffSet017_11->Draw("colz");
  heOffSet017_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_11->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(13);
  heOffSet017_12->Draw("colz");
  heOffSet017_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_12->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(14);
  heOffSet017_13->Draw("colz");
  heOffSet017_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_13->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(15);
  heOffSet017_14->Draw("colz");
  heOffSet017_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_14->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(16);
  heOffSet017_15->Draw("colz");
  heOffSet017_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_15->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(17);
  heOffSet017_16->Draw("colz");
  heOffSet017_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_16->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(18);
  heOffSet017_17->Draw("colz");
  heOffSet017_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_17->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(19);
  heOffSet017_18->Draw("colz");
  heOffSet017_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_18->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(20);
  heOffSet017_19->Draw("colz");
  heOffSet017_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_19->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(21);
  heOffSet017_20->Draw("colz");
  heOffSet017_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_20->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(22);
  heOffSet017_21->Draw("colz");
  heOffSet017_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_21->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(23);
  heOffSet017_22->Draw("colz");
  heOffSet017_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_22->SetAxisRange(0.98,1.0063,"Y");
  c17->cd(24);
  heOffSet017_23->Draw("colz");
  heOffSet017_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet017_23->SetAxisRange(0.98,1.0063,"Y");
  c17->SaveAs("Lumi2D_OffSet-017.png");
  /////
TCanvas *b0 = new TCanvas("0","offset = 0",900,900);
heOffSet017_0->Draw("colz");
b0->SetGridx(1);
b0->SetGridy(1);
heOffSet017_0->SetAxisRange(0.98,1.0063,"X");
heOffSet017_0->SetAxisRange(0.98,1.0063,"Y");
b0->SaveAs("../offset/17/0.png");
heOffSet017_0->SetAxisRange(0,1.0063,"X");
heOffSet017_0->SetAxisRange(0,1.0063,"Y");
b0->SaveAs("../offset/17/range/0.png");

TCanvas *b1 = new TCanvas("1","offset = 0",900,900);
heOffSet017_1->Draw("colz");
heOffSet017_1->SetAxisRange(0.98,1.0063,"X");
heOffSet017_1->SetAxisRange(0.98,1.0063,"Y");
b1->SaveAs("../offset/17/1.png");
heOffSet017_1->SetAxisRange(0,1.0063,"X");
heOffSet017_1->SetAxisRange(0,1.0063,"Y");
b1->SaveAs("../offset/17/range/1.png");


TCanvas *b2 = new TCanvas("2","offset = 0",900,900);
heOffSet017_2->Draw("colz");
heOffSet017_2->SetAxisRange(0.98,1.0063,"X");
heOffSet017_2->SetAxisRange(0.98,1.0063,"Y");
b2->SaveAs("../offset/17/2.png");
heOffSet017_2->SetAxisRange(0,1.0063,"X");
heOffSet017_2->SetAxisRange(0,1.0063,"Y");
b2->SaveAs("../offset/17/range/2.png");
 

TCanvas *b3 = new TCanvas("3","offset = 0",900,900);
heOffSet017_3->Draw("colz");
heOffSet017_3->SetAxisRange(0.98,1.0063,"X");
heOffSet017_3->SetAxisRange(0.98,1.0063,"Y");
b3->SaveAs("../offset/17/3.png");
heOffSet017_3->SetAxisRange(0,1.0063,"X");
heOffSet017_3->SetAxisRange(0,1.0063,"Y");
b3->SaveAs("../offset/17/range/3.png");

TCanvas *b4 = new TCanvas("4","offset = 0",900,900);
heOffSet017_4->Draw("colz");
heOffSet017_4->SetAxisRange(0.98,1.0063,"X");
heOffSet017_4->SetAxisRange(0.98,1.0063,"Y");
b4->SaveAs("../offset/17/4.png");
heOffSet017_4->SetAxisRange(0,1.0063,"X");
heOffSet017_4->SetAxisRange(0,1.0063,"Y");
b4->SaveAs("../offset/17/range/4.png");
 

TCanvas *b5 = new TCanvas("5","offset = 0",900,900);
heOffSet017_5->Draw("colz");
heOffSet017_5->SetAxisRange(0.98,1.0063,"X");
heOffSet017_5->SetAxisRange(0.98,1.0063,"Y");
b5->SaveAs("../offset/17/5.png");
heOffSet017_5->SetAxisRange(0,1.0063,"X");
heOffSet017_5->SetAxisRange(0,1.0063,"Y");
b5->SaveAs("../offset/17/range/5.png");




TCanvas *b6 = new TCanvas("6","offset = 0",900,900);
heOffSet017_6->Draw("colz");
heOffSet017_6->SetAxisRange(0.98,1.0063,"X");
heOffSet017_6->SetAxisRange(0.98,1.0063,"Y");
b6->SaveAs("../offset/17/6.png");
heOffSet017_6->SetAxisRange(0,1.0063,"X");
heOffSet017_6->SetAxisRange(0,1.0063,"Y");
b6->SaveAs("../offset/17/range/6.png");


TCanvas *b7 = new TCanvas("7","offset = 0",900,900);
heOffSet017_7->Draw("colz");
heOffSet017_7->SetAxisRange(0.98,1.0063,"X");
heOffSet017_7->SetAxisRange(0.98,1.0063,"Y");
b7->SaveAs("../offset/17/7.png");
heOffSet017_7->SetAxisRange(0,1.0063,"X");
heOffSet017_7->SetAxisRange(0,1.0063,"Y");
b7->SaveAs("../offset/17/range/7.png");

TCanvas *b8 = new TCanvas("8","offset = 0",900,900);
heOffSet017_8->Draw("colz");
heOffSet017_8->SetAxisRange(0.98,1.0063,"X");
heOffSet017_8->SetAxisRange(0.98,1.0063,"Y");
b8->SaveAs("../offset/17/8.png");
heOffSet017_8->SetAxisRange(0,1.0063,"X");
heOffSet017_8->SetAxisRange(0,1.0063,"Y");
b8->SaveAs("../offset/17/range/8.png");

TCanvas *b9 = new TCanvas("9","offset = 0",900,900);
heOffSet017_9->Draw("colz");
heOffSet017_9->SetAxisRange(0.98,1.0063,"X");
heOffSet017_9->SetAxisRange(0.98,1.0063,"Y");
b9->SaveAs("../offset/17/9.png");
heOffSet017_9->SetAxisRange(0,1.0063,"X");
heOffSet017_9->SetAxisRange(0,1.0063,"Y");
b9->SaveAs("../offset/17/range/9.png");

TCanvas *b10 = new TCanvas("10","offset = 0",900,900);
heOffSet017_10->Draw("colz");
heOffSet017_10->SetAxisRange(0.98,1.0063,"X");
heOffSet017_10->SetAxisRange(0.98,1.0063,"Y");
b10->SaveAs("../offset/17/10.png");
heOffSet017_10->SetAxisRange(0,1.0063,"X");
heOffSet017_10->SetAxisRange(0,1.0063,"Y");
b10->SaveAs("../offset/17/range/10.png");

TCanvas *b11 = new TCanvas("11","offset = 0",900,900);
heOffSet017_11->Draw("colz");
heOffSet017_11->SetAxisRange(0.98,1.0063,"X");
heOffSet017_11->SetAxisRange(0.98,1.0063,"Y");
b11->SaveAs("../offset/17/11.png");
heOffSet017_11->SetAxisRange(0,1.0063,"X");
heOffSet017_11->SetAxisRange(0,1.0063,"Y");
b11->SaveAs("../offset/17/range/11.png");

TCanvas *b12 = new TCanvas("12","offset = 0",900,900);
heOffSet017_12->Draw("colz");
heOffSet017_12->SetAxisRange(0.98,1.0063,"X");
heOffSet017_12->SetAxisRange(0.98,1.0063,"Y");
b12->SaveAs("../offset/17/12.png");
heOffSet017_12->SetAxisRange(0,1.0063,"X");
heOffSet017_12->SetAxisRange(0,1.0063,"Y");
b12->SaveAs("../offset/17/range/12.png");

TCanvas *b13 = new TCanvas("13","offset = 0",900,900);
heOffSet017_13->Draw("colz");
heOffSet017_13->SetAxisRange(0.98,1.0063,"X");
heOffSet017_13->SetAxisRange(0.98,1.0063,"Y");
b13->SaveAs("../offset/17/13.png");
heOffSet017_13->SetAxisRange(0,1.0063,"X");
heOffSet017_13->SetAxisRange(0,1.0063,"Y");
b13->SaveAs("../offset/17/range/13.png");

TCanvas *b14 = new TCanvas("14","offset = 0",900,900);
heOffSet017_14->Draw("colz");
heOffSet017_14->SetAxisRange(0.98,1.0063,"X");
heOffSet017_14->SetAxisRange(0.98,1.0063,"Y");
b14->SaveAs("../offset/17/14.png");
heOffSet017_14->SetAxisRange(0,1.0063,"X");
heOffSet017_14->SetAxisRange(0,1.0063,"Y");
b14->SaveAs("../offset/17/range/14.png");

TCanvas *b15 = new TCanvas("15","offset = 0",900,900);
heOffSet017_15->Draw("colz");
heOffSet017_15->SetAxisRange(0.98,1.0063,"X");
heOffSet017_15->SetAxisRange(0.98,1.0063,"Y");
b15->SaveAs("../offset/17/15.png");
heOffSet017_15->SetAxisRange(0,1.0063,"X");
heOffSet017_15->SetAxisRange(0,1.0063,"Y");
b15->SaveAs("../offset/17/range/15.png");

TCanvas *b16 = new TCanvas("16","offset = 0",900,900);
heOffSet017_16->Draw("colz");
heOffSet017_16->SetAxisRange(0.98,1.0063,"X");
heOffSet017_16->SetAxisRange(0.98,1.0063,"Y");
b16->SaveAs("../offset/17/16.png");
heOffSet017_16->SetAxisRange(0,1.0063,"X");
heOffSet017_16->SetAxisRange(0,1.0063,"Y");
b16->SaveAs("../offset/17/range/16.png");



TCanvas *b17 = new TCanvas("17","offset = 0",900,900);
heOffSet017_17->Draw("colz");
heOffSet017_17->SetAxisRange(0.98,1.0063,"X");
heOffSet017_17->SetAxisRange(0.98,1.0063,"Y");
b17->SaveAs("../offset/17/17.png");
heOffSet017_17->SetAxisRange(0,1.0063,"X");
heOffSet017_17->SetAxisRange(0,1.0063,"Y");
b17->SaveAs("../offset/17/range/17.png");


TCanvas *b18 = new TCanvas("18","offset = 0",900,900);
heOffSet017_18->Draw("colz");
heOffSet017_18->SetAxisRange(0.98,1.0063,"X");
heOffSet017_18->SetAxisRange(0.98,1.0063,"Y");
b18->SaveAs("../offset/17/18.png");
heOffSet017_18->SetAxisRange(0,1.0063,"X");
heOffSet017_18->SetAxisRange(0,1.0063,"Y");
b18->SaveAs("../offset/17/range/18.png");

TCanvas *b19 = new TCanvas("19","offset = 0",900,900);
heOffSet017_19->Draw("colz");
heOffSet017_19->SetAxisRange(0.98,1.0063,"X");
heOffSet017_19->SetAxisRange(0.98,1.0063,"Y");
b19->SaveAs("../offset/17/19.png");
heOffSet017_19->SetAxisRange(0,1.0063,"X");
heOffSet017_19->SetAxisRange(0,1.0063,"Y");
b19->SaveAs("../offset/17/range/19.png");


TCanvas *b20 = new TCanvas("20","offset = 0",900,900);
heOffSet017_20->Draw("colz");
heOffSet017_20->SetAxisRange(0.98,1.0063,"X");
heOffSet017_20->SetAxisRange(0.98,1.0063,"Y");
b20->SaveAs("../offset/17/20.png");
heOffSet017_20->SetAxisRange(0,1.0063,"X");
heOffSet017_20->SetAxisRange(0,1.0063,"Y");
b20->SaveAs("../offset/17/range/20.png");


  
TCanvas *b21 = new TCanvas("21","offset = 0",900,900);
heOffSet017_21->Draw("colz");
heOffSet017_21->SetAxisRange(0.98,1.0063,"X");
heOffSet017_21->SetAxisRange(0.98,1.0063,"Y");
b21->SaveAs("../offset/17/21.png");
heOffSet017_21->SetAxisRange(0,1.0063,"X");
heOffSet017_21->SetAxisRange(0,1.0063,"Y");
b21->SaveAs("../offset/17/range/21.png");

TCanvas *b22 = new TCanvas("22","offset = 0",900,900);
heOffSet017_22->Draw("colz");
heOffSet017_22->SetAxisRange(0.98,1.0063,"X");
heOffSet017_22->SetAxisRange(0.98,1.0063,"Y");
b22->SaveAs("../offset/17/22.png");
heOffSet017_22->SetAxisRange(0,1.0063,"X");
heOffSet017_22->SetAxisRange(0,1.0063,"Y");
b22->SaveAs("../offset/17/range/22.png");

TCanvas *b23 = new TCanvas("23","offset = 0",900,900);
heOffSet017_23->Draw("colz");
heOffSet017_23->SetAxisRange(0.98,1.0063,"X");
heOffSet017_23->SetAxisRange(0.98,1.0063,"Y");
b23->SaveAs("../offset/17/23.png");
heOffSet017_23->SetAxisRange(0,1.0063,"X");
heOffSet017_23->SetAxisRange(0,1.0063,"Y");
b23->SaveAs("../offset/17/range/23.png");

  /////
TCanvas *c27 = new TCanvas("E1/Enom : E2/nom n_part=27","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  c27->Divide(6,3);
  c27->cd(1);
  heOffSet027_0->Draw("colz");
  heOffSet027_0->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_0->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(2);
  heOffSet027_1->Draw("colz");
  heOffSet027_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_1->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(3);
  heOffSet027_2->Draw("colz");
  heOffSet027_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_2->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(4);
  heOffSet027_3->Draw("colz");
  heOffSet027_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_3->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(5);
  heOffSet027_4->Draw("colz");
  heOffSet027_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_4->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(6);
  heOffSet027_5->Draw("colz");
  heOffSet027_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_5->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(7);
  heOffSet027_6->Draw("colz");
  heOffSet027_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_6->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(8);
  heOffSet027_7->Draw("colz");
  heOffSet027_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_7->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(9);
  heOffSet027_8->Draw("colz");
  heOffSet027_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_8->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(10);
  heOffSet027_9->Draw("colz");
  heOffSet027_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_9->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(11);
  heOffSet027_10->Draw("colz");
  heOffSet027_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_10->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(12);
  heOffSet027_11->Draw("colz");
  heOffSet027_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_11->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(13);
  heOffSet027_12->Draw("colz");
  heOffSet027_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_12->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(14);
  heOffSet027_13->Draw("colz");
  heOffSet027_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_13->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(15);
  heOffSet027_14->Draw("colz");
  heOffSet027_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_14->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(16);
  heOffSet027_15->Draw("colz");
  heOffSet027_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_15->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(17);
  heOffSet027_16->Draw("colz");
  heOffSet027_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_16->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(18);
  heOffSet027_17->Draw("colz");
  heOffSet027_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_17->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(19);
  heOffSet027_18->Draw("colz");
  heOffSet027_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_18->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(20);
  heOffSet027_19->Draw("colz");
  heOffSet027_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_19->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(21);
  heOffSet027_20->Draw("colz");
  heOffSet027_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_20->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(22);
  heOffSet027_21->Draw("colz");
  heOffSet027_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_21->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(23);
  heOffSet027_22->Draw("colz");
  heOffSet027_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_22->SetAxisRange(0.98,1.0063,"Y");
  c27->cd(24);
  heOffSet027_23->Draw("colz");
  heOffSet027_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet027_23->SetAxisRange(0.98,1.0063,"Y");
  c27->SaveAs("Lumi2D_OffSet-027.png");
  ////

TCanvas *d0 = new TCanvas("d0","offset = 0",900,900);
heOffSet027_0->Draw("colz");
d0->SetGridx(1);
d0->SetGridy(1);
heOffSet027_0->SetAxisRange(0.98,1.0063,"X");
heOffSet027_0->SetAxisRange(0.98,1.0063,"Y");
d0->SaveAs("../offset/27/0.png");
heOffSet027_0->SetAxisRange(0,1.0063,"X");
heOffSet027_0->SetAxisRange(0,1.0063,"Y");
d0->SaveAs("../offset/27/range/0.png");

TCanvas *d1 = new TCanvas("1","offset = 0",900,900);
heOffSet027_1->Draw("colz");
heOffSet027_1->SetAxisRange(0.98,1.0063,"X");
heOffSet027_1->SetAxisRange(0.98,1.0063,"Y");
d1->SaveAs("../offset/27/1.png");
heOffSet027_1->SetAxisRange(0,1.0063,"X");
heOffSet027_1->SetAxisRange(0,1.0063,"Y");
d1->SaveAs("../offset/27/range/1.png");


TCanvas *d2 = new TCanvas("2","offset = 0",900,900);
heOffSet027_2->Draw("colz");
heOffSet027_2->SetAxisRange(0.98,1.0063,"X");
heOffSet027_2->SetAxisRange(0.98,1.0063,"Y");
d2->SaveAs("../offset/27/2.png");
heOffSet027_2->SetAxisRange(0,1.0063,"X");
heOffSet027_2->SetAxisRange(0,1.0063,"Y");
d2->SaveAs("../offset/27/range/2.png");
 

TCanvas *d3 = new TCanvas("3","offset = 0",900,900);
heOffSet027_3->Draw("colz");
heOffSet027_3->SetAxisRange(0.98,1.0063,"X");
heOffSet027_3->SetAxisRange(0.98,1.0063,"Y");
d3->SaveAs("../offset/27/3.png");
heOffSet027_3->SetAxisRange(0,1.0063,"X");
heOffSet027_3->SetAxisRange(0,1.0063,"Y");
d3->SaveAs("../offset/27/range/3.png");

TCanvas *d4 = new TCanvas("4","offset = 0",900,900);
heOffSet027_4->Draw("colz");
heOffSet027_4->SetAxisRange(0.98,1.0063,"X");
heOffSet027_4->SetAxisRange(0.98,1.0063,"Y");
d4->SaveAs("../offset/27/4.png");
heOffSet027_4->SetAxisRange(0,1.0063,"X");
heOffSet027_4->SetAxisRange(0,1.0063,"Y");
d4->SaveAs("../offset/27/range/4.png");
 

TCanvas *d5 = new TCanvas("5","offset = 0",900,900);
heOffSet027_5->Draw("colz");
heOffSet027_5->SetAxisRange(0.98,1.0063,"X");
heOffSet027_5->SetAxisRange(0.98,1.0063,"Y");
d5->SaveAs("../offset/27/5.png");
heOffSet027_5->SetAxisRange(0,1.0063,"X");
heOffSet027_5->SetAxisRange(0,1.0063,"Y");
d5->SaveAs("../offset/27/range/5.png");




TCanvas *d6 = new TCanvas("6","offset = 0",900,900);
heOffSet027_6->Draw("colz");
heOffSet027_6->SetAxisRange(0.98,1.0063,"X");
heOffSet027_6->SetAxisRange(0.98,1.0063,"Y");
d6->SaveAs("../offset/27/6.png");
heOffSet027_6->SetAxisRange(0,1.0063,"X");
heOffSet027_6->SetAxisRange(0,1.0063,"Y");
d6->SaveAs("../offset/27/range/6.png");



TCanvas *d7 = new TCanvas("7","offset = 0",900,900);
heOffSet027_7->Draw("colz");
heOffSet027_7->SetAxisRange(0.98,1.0063,"X");
heOffSet027_7->SetAxisRange(0.98,1.0063,"Y");
d7->SaveAs("../offset/27/7.png");
heOffSet027_7->SetAxisRange(0,1.0063,"X");
heOffSet027_7->SetAxisRange(0,1.0063,"Y");
d7->SaveAs("../offset/27/range/7.png");

TCanvas *d8 = new TCanvas("8","offset = 0",900,900);
heOffSet027_8->Draw("colz");
heOffSet027_8->SetAxisRange(0.98,1.0063,"X");
heOffSet027_8->SetAxisRange(0.98,1.0063,"Y");
d8->SaveAs("../offset/27/8.png");
heOffSet027_8->SetAxisRange(0,1.0063,"X");
heOffSet027_8->SetAxisRange(0,1.0063,"Y");
d8->SaveAs("../offset/27/range/8.png");

TCanvas *d9 = new TCanvas("9","offset = 0",900,900);
heOffSet027_9->Draw("colz");
heOffSet027_9->SetAxisRange(0.98,1.0063,"X");
heOffSet027_9->SetAxisRange(0.98,1.0063,"Y");
d9->SaveAs("../offset/27/9.png");
heOffSet027_9->SetAxisRange(0,1.0063,"X");
heOffSet027_9->SetAxisRange(0,1.0063,"Y");
d9->SaveAs("../offset/27/range/9.png");

TCanvas *d10 = new TCanvas("10","offset = 0",900,900);
heOffSet027_10->Draw("colz");
heOffSet027_10->SetAxisRange(0.98,1.0063,"X");
heOffSet027_10->SetAxisRange(0.98,1.0063,"Y");
d10->SaveAs("../offset/27/10.png");
heOffSet027_10->SetAxisRange(0,1.0063,"X");
heOffSet027_10->SetAxisRange(0,1.0063,"Y");
d10->SaveAs("../offset/27/range/10.png");

TCanvas *d11 = new TCanvas("11","offset = 0",900,900);
heOffSet027_11->Draw("colz");
heOffSet027_11->SetAxisRange(0.98,1.0063,"X");
heOffSet027_11->SetAxisRange(0.98,1.0063,"Y");
d11->SaveAs("../offset/27/11.png");
heOffSet027_11->SetAxisRange(0,1.0063,"X");
heOffSet027_11->SetAxisRange(0,1.0063,"Y");
d11->SaveAs("../offset/27/range/11.png");

TCanvas *d12 = new TCanvas("12","offset = 0",900,900);
heOffSet027_12->Draw("colz");
heOffSet027_12->SetAxisRange(0.98,1.0063,"X");
heOffSet027_12->SetAxisRange(0.98,1.0063,"Y");
d12->SaveAs("../offset/27/12.png");
heOffSet027_12->SetAxisRange(0,1.0063,"X");
heOffSet027_12->SetAxisRange(0,1.0063,"Y");
d12->SaveAs("../offset/27/range/12.png");

TCanvas *d13 = new TCanvas("13","offset = 0",900,900);
heOffSet027_13->Draw("colz");
heOffSet027_13->SetAxisRange(0.98,1.0063,"X");
heOffSet027_13->SetAxisRange(0.98,1.0063,"Y");
d13->SaveAs("../offset/27/13.png");
heOffSet027_13->SetAxisRange(0,1.0063,"X");
heOffSet027_13->SetAxisRange(0,1.0063,"Y");
d13->SaveAs("../offset/27/range/13.png");

TCanvas *d14 = new TCanvas("14","offset = 0",900,900);
heOffSet027_14->Draw("colz");
heOffSet027_14->SetAxisRange(0.98,1.0063,"X");
heOffSet027_14->SetAxisRange(0.98,1.0063,"Y");
d14->SaveAs("../offset/27/14.png");
heOffSet027_14->SetAxisRange(0,1.0063,"X");
heOffSet027_14->SetAxisRange(0,1.0063,"Y");
d14->SaveAs("../offset/27/range/14.png");

TCanvas *d15 = new TCanvas("15","offset = 0",900,900);
heOffSet027_15->Draw("colz");
heOffSet027_15->SetAxisRange(0.98,1.0063,"X");
heOffSet027_15->SetAxisRange(0.98,1.0063,"Y");
d15->SaveAs("../offset/27/15.png");
heOffSet027_15->SetAxisRange(0,1.0063,"X");
heOffSet027_15->SetAxisRange(0,1.0063,"Y");
d15->SaveAs("../offset/27/range/15.png");

TCanvas *d16 = new TCanvas("16","offset = 0",900,900);
heOffSet027_16->Draw("colz");
heOffSet027_16->SetAxisRange(0.98,1.0063,"X");
heOffSet027_16->SetAxisRange(0.98,1.0063,"Y");
d16->SaveAs("../offset/27/16.png");
heOffSet027_16->SetAxisRange(0,1.0063,"X");
heOffSet027_16->SetAxisRange(0,1.0063,"Y");
d16->SaveAs("../offset/27/range/16.png");



TCanvas *d17 = new TCanvas("17","offset = 0",900,900);
heOffSet027_17->Draw("colz");
heOffSet027_17->SetAxisRange(0.98,1.0063,"X");
heOffSet027_17->SetAxisRange(0.98,1.0063,"Y");
d17->SaveAs("../offset/27/17.png");
heOffSet027_17->SetAxisRange(0,1.0063,"X");
heOffSet027_17->SetAxisRange(0,1.0063,"Y");
d17->SaveAs("../offset/27/range/17.png");


TCanvas *d18 = new TCanvas("18","offset = 0",900,900);
heOffSet027_18->Draw("colz");
heOffSet027_18->SetAxisRange(0.98,1.0063,"X");
heOffSet027_18->SetAxisRange(0.98,1.0063,"Y");
d18->SaveAs("../offset/27/18.png");
heOffSet027_18->SetAxisRange(0,1.0063,"X");
heOffSet027_18->SetAxisRange(0,1.0063,"Y");
d18->SaveAs("../offset/27/range/18.png");

TCanvas *d19 = new TCanvas("19","offset = 0",900,900);
heOffSet027_19->Draw("colz");
heOffSet027_19->SetAxisRange(0.98,1.0063,"X");
heOffSet027_19->SetAxisRange(0.98,1.0063,"Y");
d19->SaveAs("../offset/27/19.png");
heOffSet027_19->SetAxisRange(0,1.0063,"X");
heOffSet027_19->SetAxisRange(0,1.0063,"Y");
d19->SaveAs("../offset/27/range/19.png");


TCanvas *d20 = new TCanvas("20","offset = 0",900,900);
heOffSet027_20->Draw("colz");
heOffSet027_20->SetAxisRange(0.98,1.0063,"X");
heOffSet027_20->SetAxisRange(0.98,1.0063,"Y");
d20->SaveAs("../offset/27/20.png");
heOffSet027_20->SetAxisRange(0,1.0063,"X");
heOffSet027_20->SetAxisRange(0,1.0063,"Y");
d20->SaveAs("../offset/27/range/20.png");
  
TCanvas *d21 = new TCanvas("21","offset = 0",900,900);
heOffSet027_21->Draw("colz");
heOffSet027_21->SetAxisRange(0.98,1.0063,"X");
heOffSet027_21->SetAxisRange(0.98,1.0063,"Y");
d21->SaveAs("../offset/27/21.png");
heOffSet027_21->SetAxisRange(0,1.0063,"X");
heOffSet027_21->SetAxisRange(0,1.0063,"Y");
d21->SaveAs("../offset/27/range/21.png");

TCanvas *d22 = new TCanvas("22","offset = 0",900,900);
heOffSet027_22->Draw("colz");
heOffSet027_22->SetAxisRange(0.98,1.0063,"X");
heOffSet027_22->SetAxisRange(0.98,1.0063,"Y");
d22->SaveAs("../offset/27/22.png");
heOffSet027_22->SetAxisRange(0,1.0063,"X");
heOffSet027_22->SetAxisRange(0,1.0063,"Y");
d22->SaveAs("../offset/27/range/22.png");

TCanvas *d23 = new TCanvas("23","offset = 0",900,900);
heOffSet027_23->Draw("colz");
heOffSet027_23->SetAxisRange(0.98,1.0063,"X");
heOffSet027_23->SetAxisRange(0.98,1.0063,"Y");
d23->SaveAs("../offset/27/23.png");
heOffSet027_23->SetAxisRange(0,1.0063,"X");
heOffSet027_23->SetAxisRange(0,1.0063,"Y");
d23->SaveAs("../offset/27/range/23.png");
  
////////////////////////////////
TCanvas *l17_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet017_1->Draw("same");
  hOffSet017_1->SetLineColor(kRed);
  hOffSet017_5->Draw("same");
  hOffSet017_5->SetLineColor(kRed+2);
  hOffSet017_11->Draw("same");
  hOffSet017_11->SetLineColor(kBlue);
  hOffSet017_17->Draw("same");
  hOffSet017_17->SetLineColor(kBlue+2);
  hOffSet017_23->Draw("same");
  hOffSet017_23->SetLineColor(kGreen);
  TLegend* legC4 = new TLegend(0.8,0.09,0.98,0.9);  
  legC4->SetHeader("N_PART dependence}");
  legC4->SetTextSize(0.02);// set size of text
  // legC4->SetNColumns(2);  
  legC4->SetFillColor(0);// Have a white background 0.193 0.386 0.579 0.965 1.158
  legC4->AddEntry(hOffSet017_1, "Dft OffSet=0.001", "l");
  legC4->AddEntry(hOffSet017_5, "Dft OffSet=0.09", "l");
  legC4->AddEntry(hOffSet017_11, "Dft OffSet=0.21", "l");
  legC4->AddEntry(hOffSet017_16, "Dft OffSet=0.33", "l");
  legC4->AddEntry(hOffSet017_23, "Dft OffSet=0.47", "l");
  legC4->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC4->Draw();
  l17_1->SetGridx(1);
  l17_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l17_1->SaveAs("L_distribution_OffSet17.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l17_1->SaveAs("L_distribution_OffSet17_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l17_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l17_1->SaveAs("L_distribution_OffSet17_Logy.png");
////////////////////////////////
  //// 0.47 0.57 0.67 0.87 1;
TCanvas *l27_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet027_1->Draw("same");
  hOffSet027_1->SetLineColor(kRed);
  hOffSet027_5->Draw("same");
  hOffSet027_5->SetLineColor(kRed+2);
  hOffSet027_11->Draw("same");
  hOffSet027_11->SetLineColor(kBlue);
  hOffSet027_17->Draw("same");
  hOffSet027_17->SetLineColor(kBlue+2);
  hOffSet027_23->Draw("same");
  hOffSet027_23->SetLineColor(kGreen);
  TLegend* legC27 = new TLegend(0.8,0.09,0.98,0.9);  
  legC27->SetHeader("N_PART dependence}");
  legC27->SetTextSize(0.02);// set size of text
  // legC4->SetNColumns(2);  
  legC27->SetFillColor(0);// Have a white background 0.193 0.386 0.579 0.965 1.158
  legC27->AddEntry(hOffSet027_1, "Dft OffSet=0.001", "l");
  legC27->AddEntry(hOffSet027_5, "Dft OffSet=0.09", "l");
  legC27->AddEntry(hOffSet027_11, "Dft OffSet=0.21", "l");
  legC27->AddEntry(hOffSet027_16, "Dft OffSet=0.33", "l");
  legC27->AddEntry(hOffSet027_23, "Dft OffSet=0.47", "l");
  legC27->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC27->Draw();
  l27_1->SetGridx(1);
  l27_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l27_1->SaveAs("L_distribution_OffSet27.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l27_1->SaveAs("L_distribution_OffSet27_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l27_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l27_1->SaveAs("L_distribution_OffSet27_Logy.png");

//// 0.47 0.57 0.67 0.87 1;
TCanvas *l37_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet037_1->Draw("same");
  hOffSet037_1->SetLineColor(kRed);
  hOffSet037_5->Draw("same");
  hOffSet037_5->SetLineColor(kRed+2);
  hOffSet037_11->Draw("same");
  hOffSet037_11->SetLineColor(kBlue);
  hOffSet037_17->Draw("same");
  hOffSet037_17->SetLineColor(kBlue+2);
  hOffSet037_23->Draw("same");
  hOffSet037_23->SetLineColor(kGreen);
  TLegend* legC37 = new TLegend(0.8,0.09,0.98,0.9);  
  legC37->SetHeader("N_PART dependence}");
  legC37->SetTextSize(0.02);// set size of text
  // legC4->SetNColumns(2);  
  legC37->SetFillColor(0);// Have a white background 0.193 0.386 0.579 0.965 1.158
  legC37->AddEntry(hOffSet037_1, "Dft OffSet=0.001", "l");
  legC37->AddEntry(hOffSet037_5, "Dft OffSet=0.09", "l");
  legC37->AddEntry(hOffSet037_11, "Dft OffSet=0.21", "l");
  legC37->AddEntry(hOffSet037_16, "Dft OffSet=0.33", "l");
  legC37->AddEntry(hOffSet037_23, "Dft OffSet=0.47", "l");
  legC37->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC37->Draw();
  l37_1->SetGridx(1);
  l37_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l37_1->SaveAs("L_distribution_OffSet37.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l37_1->SaveAs("L_distribution_OffSet37_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l37_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l37_1->SaveAs("L_distribution_OffSet37_Logy.png");

  /////////////////////////////
 
  TCanvas *l37 = new TCanvas("E1/Enom : E2/nom n_part=37","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  l37->Divide(6,3);
  l37->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(2);
  heOffSet037_1->Draw("colz");
  heOffSet037_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_1->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(3);
  heOffSet037_2->Draw("colz");
  heOffSet037_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_2->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(4);
  heOffSet037_3->Draw("colz");
  heOffSet037_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_3->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(5);
  heOffSet037_4->Draw("colz");
  heOffSet037_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_4->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(6);
  heOffSet037_5->Draw("colz");
  heOffSet037_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_5->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(7);
  heOffSet037_6->Draw("colz");
  heOffSet037_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_6->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(8);
  heOffSet037_7->Draw("colz");
  heOffSet037_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_7->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(9);
  heOffSet037_8->Draw("colz");
  heOffSet037_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_8->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(10);
  heOffSet037_9->Draw("colz");
  heOffSet037_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_9->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(11);
  heOffSet037_10->Draw("colz");
  heOffSet037_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_10->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(12);
  heOffSet037_11->Draw("colz");
  heOffSet037_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_11->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(13);
  heOffSet037_12->Draw("colz");
  heOffSet037_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_12->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(14);
  heOffSet037_13->Draw("colz");
  heOffSet037_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_13->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(15);
  heOffSet037_14->Draw("colz");
  heOffSet037_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_14->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(16);
  heOffSet037_15->Draw("colz");
  heOffSet037_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_15->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(17);
  heOffSet037_16->Draw("colz");
  heOffSet037_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_16->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(18);
  heOffSet037_17->Draw("colz");
  heOffSet037_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_17->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(19);
  heOffSet037_18->Draw("colz");
  heOffSet037_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_18->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(20);
  heOffSet037_19->Draw("colz");
  heOffSet037_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_19->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(21);
  heOffSet037_20->Draw("colz");
  heOffSet037_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_20->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(22);
  heOffSet037_21->Draw("colz");
  heOffSet037_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_21->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(23);
  heOffSet037_22->Draw("colz");
  heOffSet037_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_22->SetAxisRange(0.98,1.0063,"Y");
  l37->cd(24);
  heOffSet037_23->Draw("colz");
  heOffSet037_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet037_23->SetAxisRange(0.98,1.0063,"Y");
  l37->SaveAs("Lumi2D_OffSet-037.png");
  ////


TCanvas *m0 = new TCanvas("m0","offset = 0",900,900);
heE3000g->Draw("colz");
m0->SetGridx(1);
m0->SetGridy(1);
heE3000g->SetAxisRange(0.98,1.0063,"X");
heE3000g->SetAxisRange(0.98,1.0063,"Y");
m0->SaveAs("../offset/37/0.png");
heE3000g->SetAxisRange(0,1.0063,"X");
heE3000g->SetAxisRange(0,1.0063,"Y");
m0->SaveAs("../offset/37/range/0.png");

TCanvas *m1 = new TCanvas("1","offset = 0",900,900);
heOffSet037_1->Draw("colz");
heOffSet037_1->SetAxisRange(0.98,1.0063,"X");
heOffSet037_1->SetAxisRange(0.98,1.0063,"Y");
m1->SaveAs("../offset/37/1.png");
heOffSet037_1->SetAxisRange(0,1.0063,"X");
heOffSet037_1->SetAxisRange(0,1.0063,"Y");
m1->SaveAs("../offset/37/range/1.png");


TCanvas *m2 = new TCanvas("2","offset = 0",900,900);
heOffSet037_2->Draw("colz"); 
heOffSet037_2->SetAxisRange(0.98,1.0063,"X");
heOffSet037_2->SetAxisRange(0.98,1.0063,"Y");
m2->SaveAs("../offset/37/2.png");
heOffSet037_2->Draw("lego2");
m2->SaveAs("../offset/37/Lego2.png");
heOffSet037_2->Draw("colz");
heOffSet037_2->SetAxisRange(0,1.0063,"X");
heOffSet037_2->SetAxisRange(0,1.0063,"Y");
m2->SaveAs("../offset/37/range/2.png");
heOffSet037_2->Draw("lego");
m2->SaveAs("../offset/37/Lego.png");



TCanvas *m3 = new TCanvas("3","offset = 0",900,900);
heOffSet037_3->Draw("colz");
heOffSet037_3->SetAxisRange(0.98,1.0063,"X");
heOffSet037_3->SetAxisRange(0.98,1.0063,"Y");
m3->SaveAs("../offset/37/3.png");
heOffSet037_3->SetAxisRange(0,1.0063,"X");
heOffSet037_3->SetAxisRange(0,1.0063,"Y");
m3->SaveAs("../offset/37/range/3.png");

TCanvas *m4 = new TCanvas("4","offset = 0",900,900);
heOffSet037_4->Draw("colz");
heOffSet037_4->SetAxisRange(0.98,1.0063,"X");
heOffSet037_4->SetAxisRange(0.98,1.0063,"Y");
m4->SaveAs("../offset/37/4.png");
heOffSet037_4->SetAxisRange(0,1.0063,"X");
heOffSet037_4->SetAxisRange(0,1.0063,"Y");
m4->SaveAs("../offset/37/range/4.png");
 

TCanvas *m5 = new TCanvas("5","offset = 0",900,900);
heOffSet037_5->Draw("colz");
heOffSet037_5->SetAxisRange(0.98,1.0063,"X");
heOffSet037_5->SetAxisRange(0.98,1.0063,"Y");
m5->SaveAs("../offset/37/5.png");
heOffSet037_5->SetAxisRange(0,1.0063,"X");
heOffSet037_5->SetAxisRange(0,1.0063,"Y");
m5->SaveAs("../offset/37/range/5.png");




TCanvas *m6 = new TCanvas("6","offset = 0",900,900);
heOffSet037_6->Draw("colz");
heOffSet037_6->SetAxisRange(0.98,1.0063,"X");
heOffSet037_6->SetAxisRange(0.98,1.0063,"Y");
m6->SaveAs("../offset/37/6.png");
heOffSet037_6->SetAxisRange(0,1.0063,"X");
heOffSet037_6->SetAxisRange(0,1.0063,"Y");
m6->SaveAs("../offset/37/range/6.png");



TCanvas *m7 = new TCanvas("7","offset = 0",900,900);
heOffSet037_7->Draw("colz");
heOffSet037_7->SetAxisRange(0.98,1.0063,"X");
heOffSet037_7->SetAxisRange(0.98,1.0063,"Y");
m7->SaveAs("../offset/37/7.png");
heOffSet037_7->SetAxisRange(0,1.0063,"X");
heOffSet037_7->SetAxisRange(0,1.0063,"Y");
m7->SaveAs("../offset/37/range/7.png");

TCanvas *m8 = new TCanvas("8","offset = 0",900,900);
heOffSet037_8->Draw("colz");
heOffSet037_8->SetAxisRange(0.98,1.0063,"X");
heOffSet037_8->SetAxisRange(0.98,1.0063,"Y");
m8->SaveAs("../offset/37/8.png");
heOffSet037_8->SetAxisRange(0,1.0063,"X");
heOffSet037_8->SetAxisRange(0,1.0063,"Y");
m8->SaveAs("../offset/37/range/8.png");

TCanvas *m9 = new TCanvas("9","offset = 0",900,900);
heOffSet037_9->Draw("colz");
heOffSet037_9->SetAxisRange(0.98,1.0063,"X");
heOffSet037_9->SetAxisRange(0.98,1.0063,"Y");
m9->SaveAs("../offset/37/9.png");
heOffSet037_9->SetAxisRange(0,1.0063,"X");
heOffSet037_9->SetAxisRange(0,1.0063,"Y");
m9->SaveAs("../offset/37/range/9.png");

TCanvas *m10 = new TCanvas("10","offset = 0",900,900);
heOffSet037_10->Draw("colz");
heOffSet037_10->SetAxisRange(0.98,1.0063,"X");
heOffSet037_10->SetAxisRange(0.98,1.0063,"Y");
m10->SaveAs("../offset/37/10.png");
heOffSet037_10->SetAxisRange(0,1.0063,"X");
heOffSet037_10->SetAxisRange(0,1.0063,"Y");
m10->SaveAs("../offset/37/range/10.png");

TCanvas *m11 = new TCanvas("11","offset = 0",900,900);
heOffSet037_11->Draw("colz");
heOffSet037_11->SetAxisRange(0.98,1.0063,"X");
heOffSet037_11->SetAxisRange(0.98,1.0063,"Y");
m11->SaveAs("../offset/37/11.png");
heOffSet037_11->SetAxisRange(0,1.0063,"X");
heOffSet037_11->SetAxisRange(0,1.0063,"Y");
m11->SaveAs("../offset/37/range/11.png");

TCanvas *m12 = new TCanvas("12","offset = 0",900,900);
heOffSet037_12->Draw("colz");
heOffSet037_12->SetAxisRange(0.98,1.0063,"X");
heOffSet037_12->SetAxisRange(0.98,1.0063,"Y");
m12->SaveAs("../offset/37/12.png");
heOffSet037_12->SetAxisRange(0,1.0063,"X");
heOffSet037_12->SetAxisRange(0,1.0063,"Y");
m12->SaveAs("../offset/37/range/12.png");

TCanvas *m13 = new TCanvas("13","offset = 0",900,900);
heOffSet037_13->Draw("colz");
heOffSet037_13->SetAxisRange(0.98,1.0063,"X");
heOffSet037_13->SetAxisRange(0.98,1.0063,"Y");
m13->SaveAs("../offset/37/13.png");
heOffSet037_13->SetAxisRange(0,1.0063,"X");
heOffSet037_13->SetAxisRange(0,1.0063,"Y");
m13->SaveAs("../offset/37/range/13.png");

TCanvas *m14 = new TCanvas("14","offset = 0",900,900);
heOffSet037_14->Draw("colz");
heOffSet037_14->SetAxisRange(0.98,1.0063,"X");
heOffSet037_14->SetAxisRange(0.98,1.0063,"Y");
m14->SaveAs("../offset/37/14.png");
heOffSet037_14->SetAxisRange(0,1.0063,"X");
heOffSet037_14->SetAxisRange(0,1.0063,"Y");
m14->SaveAs("../offset/37/range/14.png");

TCanvas *m15 = new TCanvas("15","offset = 0",900,900);
heOffSet037_15->Draw("colz");
heOffSet037_15->SetAxisRange(0.98,1.0063,"X");
heOffSet037_15->SetAxisRange(0.98,1.0063,"Y");
m15->SaveAs("../offset/37/15.png");
heOffSet037_15->SetAxisRange(0,1.0063,"X");
heOffSet037_15->SetAxisRange(0,1.0063,"Y");
m15->SaveAs("../offset/37/range/15.png");

TCanvas *m16 = new TCanvas("16","offset = 0",900,900);
heOffSet037_16->Draw("colz");
heOffSet037_16->SetAxisRange(0.98,1.0063,"X");
heOffSet037_16->SetAxisRange(0.98,1.0063,"Y");
m16->SaveAs("../offset/37/16.png");
heOffSet037_16->SetAxisRange(0,1.0063,"X");
heOffSet037_16->SetAxisRange(0,1.0063,"Y");
m16->SaveAs("../offset/37/range/16.png");



TCanvas *m17 = new TCanvas("17","offset = 0",900,900);
heOffSet037_17->Draw("colz");
heOffSet037_17->SetAxisRange(0.98,1.0063,"X");
heOffSet037_17->SetAxisRange(0.98,1.0063,"Y");
m17->SaveAs("../offset/37/17.png");
heOffSet037_17->SetAxisRange(0,1.0063,"X");
heOffSet037_17->SetAxisRange(0,1.0063,"Y");
m17->SaveAs("../offset/37/range/17.png");


TCanvas *m18 = new TCanvas("18","offset = 0",900,900);
heOffSet037_18->Draw("colz");
heOffSet037_18->SetAxisRange(0.98,1.0063,"X");
heOffSet037_18->SetAxisRange(0.98,1.0063,"Y");
m18->SaveAs("../offset/37/18.png");
heOffSet037_18->SetAxisRange(0,1.0063,"X");
heOffSet037_18->SetAxisRange(0,1.0063,"Y");
m18->SaveAs("../offset/37/range/18.png");

TCanvas *m19 = new TCanvas("19","offset = 0",900,900);
heOffSet037_19->Draw("colz");
heOffSet037_19->SetAxisRange(0.98,1.0063,"X");
heOffSet037_19->SetAxisRange(0.98,1.0063,"Y");
m19->SaveAs("../offset/37/19.png");
heOffSet037_19->SetAxisRange(0,1.0063,"X");
heOffSet037_19->SetAxisRange(0,1.0063,"Y");
m19->SaveAs("../offset/37/range/19.png");


TCanvas *m20 = new TCanvas("20","offset = 0",900,900);
heOffSet037_20->Draw("colz");
heOffSet037_20->SetAxisRange(0.98,1.0063,"X");
heOffSet037_20->SetAxisRange(0.98,1.0063,"Y");
m20->SaveAs("../offset/37/20.png");
heOffSet037_20->SetAxisRange(0,1.0063,"X");
heOffSet037_20->SetAxisRange(0,1.0063,"Y");
m20->SaveAs("../offset/37/range/20.png");
  
TCanvas *m21 = new TCanvas("21","offset = 0",900,900);
heOffSet037_21->Draw("colz");
heOffSet037_21->SetAxisRange(0.98,1.0063,"X");
heOffSet037_21->SetAxisRange(0.98,1.0063,"Y");
m21->SaveAs("../offset/37/21.png");
heOffSet037_21->SetAxisRange(0,1.0063,"X");
heOffSet037_21->SetAxisRange(0,1.0063,"Y");
m21->SaveAs("../offset/37/range/21.png");

TCanvas *m22 = new TCanvas("22","offset = 0",900,900);
heOffSet037_22->Draw("colz");
heOffSet037_22->SetAxisRange(0.98,1.0063,"X");
heOffSet037_22->SetAxisRange(0.98,1.0063,"Y");
m22->SaveAs("../offset/37/22.png");
heOffSet037_22->SetAxisRange(0,1.0063,"X");
heOffSet037_22->SetAxisRange(0,1.0063,"Y");
m22->SaveAs("../offset/37/range/22.png");

TCanvas *m23 = new TCanvas("23","offset = 0",900,900);
heOffSet037_23->Draw("colz");
heOffSet037_23->SetAxisRange(0.98,1.0063,"X");
heOffSet037_23->SetAxisRange(0.98,1.0063,"Y");
m23->SaveAs("../offset/37/23.png");
heOffSet037_23->SetAxisRange(0,1.0063,"X");
heOffSet037_23->SetAxisRange(0,1.0063,"Y");
m23->SaveAs("../offset/37/range/23.png");



//   //// 0.47 0.57 0.67 0.87 1;

  ///
  
  ///// 0.170 0.27 0.47 0.57 0.67 0.87 1;
  /////
  TH1D *hOffSet047_0 = new TH1D("hOffSet047_0","OffSet= 0",bins,0, xLimit);
  TH1D *hOffSet057_0 = new TH1D("hOffSet057_0","OffSet= 0",bins,0, xLimit);
  TH1D *hOffSet067_0 = new TH1D("hOffSet067_0","OffSet= 0",bins,0, xLimit);
  TH1D *hOffSet087_0 = new TH1D("hOffSet087_0","OffSet= 0",bins,0, xLimit);
  TH1D *hOffSet047_1 = new TH1D("hOffSet47_1","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_2 = new TH1D("hOffSet47_2","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_3 = new TH1D("hOffSet47_3","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_4 = new TH1D("hOffSet47_4","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_5 = new TH1D("hOffSet47_5","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_6 = new TH1D("hOffSet47_6","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_7 = new TH1D("hOffSet47_7","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_8 = new TH1D("hOffSet47_8","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_9 = new TH1D("hOffSet47_9","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_10 = new TH1D("hOffSet47_10","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_11 = new TH1D("hOffSet47_11","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_12 = new TH1D("hOffSet47_12","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_13 = new TH1D("hOffSet47_13","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_14 = new TH1D("hOffSet47_14","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_15 = new TH1D("hOffSet47_15","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_16 = new TH1D("hOffSet47_16","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_17 = new TH1D("hOffSet47_17","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_18 = new TH1D("hOffSet47_18","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_19 = new TH1D("hOffSet47_19","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_20 = new TH1D("hOffSet47_20","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet047_21 = new TH1D("hOffSet47_21","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet047_22 = new TH1D("hOffSet47_22","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet047_23 = new TH1D("hOffSet47_23","OffSet= 0.4",bins,0, xLimit);
  /////
  ////////// 0.170 0.27 0.47 0.57 0.67 0.87 1;
  TH1D *hOffSet057_1 = new TH1D("hOffSet57_1","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_2 = new TH1D("hOffSet57_2","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_3 = new TH1D("hOffSet57_3","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_4 = new TH1D("hOffSet57_4","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_5 = new TH1D("hOffSet57_5","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_6 = new TH1D("hOffSet57_6","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_7 = new TH1D("hOffSet57_7","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_8 = new TH1D("hOffSet57_8","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_9 = new TH1D("hOffSet57_9","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_10 = new TH1D("hOffSet57_10","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_11 = new TH1D("hOffSet57_11","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_12 = new TH1D("hOffSet57_12","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_13 = new TH1D("hOffSet57_13","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_14 = new TH1D("hOffSet57_14","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_15 = new TH1D("hOffSet57_15","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_16 = new TH1D("hOffSet57_16","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_17 = new TH1D("hOffSet57_17","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_18 = new TH1D("hOffSet57_18","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_19 = new TH1D("hOffSet57_19","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_20 = new TH1D("hOffSet57_20","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet057_21 = new TH1D("hOffSet57_21","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet057_22 = new TH1D("hOffSet57_22","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet057_23 = new TH1D("hOffSet57_23","OffSet= 0.4",bins,0, xLimit);
  /////

  ///// 0.170 0.27 0.47 0.57 0.67 0.87 1;
 ///// 0.170 0.27 0.47 0.57 0.67 0.87 1;
   TH1D *hOffSet067_1 = new TH1D("hOffSet67_1","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet067_2 = new TH1D("hOffSet67_2","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet067_3 = new TH1D("hOffSet67_3","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet067_4 = new TH1D("hOffSet67_4","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet067_5 = new TH1D("hOffSet67_5","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet067_6 = new TH1D("hOffSet67_6","OffSet= 0.4",bins,0, xLimit);
   TH1D *hOffSet067_7 = new TH1D("hOffSet67_7","OffSet= 0.1",bins,0, xLimit);
   TH1D *hOffSet067_8 = new TH1D("hOffSet67_8","OffSet= 0.2",bins,0, xLimit);
   TH1D *hOffSet067_9 = new TH1D("hOffSet67_9","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet067_10 = new TH1D("hOffSet67_10","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet067_11 = new TH1D("hOffSet67_11","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet067_12 = new TH1D("hOffSet67_12","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet067_13 = new TH1D("hOffSet67_13","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet067_14 = new TH1D("hOffSet67_14","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet067_15 = new TH1D("hOffSet67_15","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet067_16 = new TH1D("hOffSet67_16","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet067_17 = new TH1D("hOffSet67_17","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet067_18 = new TH1D("hOffSet67_18","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet067_19 = new TH1D("hOffSet67_19","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet067_20 = new TH1D("hOffSet67_20","OffSet= 0.1",bins,0, xLimit);
  TH1D *hOffSet067_21 = new TH1D("hOffSet67_21","OffSet= 0.2",bins,0, xLimit);
  TH1D *hOffSet067_22 = new TH1D("hOffSet67_22","OffSet= 0.4",bins,0, xLimit);
  TH1D *hOffSet067_23 = new TH1D("hOffSet67_23","OffSet= 0.4",bins,0, xLimit);
  /////
  ///// 0.170 0.27 0.47 0.57 0.67 0.87 1;
 
   TH1D *hOffSet087_1 = new TH1D("hOffSet87_1","hOffSet87_= 0.1",bins,0, xLimit); 
   TH1D *hOffSet087_2 = new TH1D("hOffSet87_2","hOffSet87_= 0.2",bins,0, xLimit);	
   TH1D *hOffSet087_3 = new TH1D("hOffSet87_3","hOffSet87_= 0.4",bins,0, xLimit);	
   TH1D *hOffSet087_4 = new TH1D("hOffSet87_4","hOffSet87_= 0.1",bins,0, xLimit);	
   TH1D *hOffSet087_5 = new TH1D("hOffSet87_5","hOffSet87_= 0.2",bins,0, xLimit);
   TH1D *hOffSet087_6 = new TH1D("hOffSet87_6","hOffSet87_= 0.4",bins,0, xLimit);
   TH1D *hOffSet087_7 = new TH1D("hOffSet87_7","hOffSet87_= 0.1",bins,0, xLimit);
   TH1D *hOffSet087_8 = new TH1D("hOffSet87_8","hOffSet87_= 0.2",bins,0, xLimit);
   TH1D *hOffSet087_9 = new TH1D("hOffSet87_9","hOffSet87_= 0.4",bins,0, xLimit);
  TH1D *hOffSet087_10 = new TH1D("hOffSet87_10","hOffSet87_= 0.1",bins,0, xLimit);
  TH1D *hOffSet087_11 = new TH1D("hOffSet87_11","hOffSet87_= 0.2",bins,0, xLimit);
  TH1D *hOffSet087_12 = new TH1D("hOffSet87_12","hOffSet87_= 0.4",bins,0, xLimit);
  TH1D *hOffSet087_13 = new TH1D("hOffSet87_13","hOffSet87_= 0.4",bins,0, xLimit);
  TH1D *hOffSet087_14 = new TH1D("hOffSet87_14","hOffSet87_= 0.1",bins,0, xLimit);
  TH1D *hOffSet087_15 = new TH1D("hOffSet87_15","hOffSet87_= 0.2",bins,0, xLimit);
  TH1D *hOffSet087_16 = new TH1D("hOffSet87_16","hOffSet87_= 0.4",bins,0, xLimit);
  TH1D *hOffSet087_17 = new TH1D("hOffSet87_17","hOffSet87_= 0.1",bins,0, xLimit);
  TH1D *hOffSet087_18 = new TH1D("hOffSet87_18","hOffSet87_= 0.2",bins,0, xLimit);
  TH1D *hOffSet087_19 = new TH1D("hOffSet87_19","hOffSet87_= 0.4",bins,0, xLimit);
  TH1D *hOffSet087_20 = new TH1D("hOffSet87_20","hOffSet87_= 0.1",bins,0, xLimit);
  TH1D *hOffSet087_21 = new TH1D("hOffSet87_21","hOffSet87_= 0.2",bins,0, xLimit);
  TH1D *hOffSet087_22 = new TH1D("hOffSet87_22","hOffSet87_= 0.4",bins,0, xLimit);
  TH1D *hOffSet087_23 = new TH1D("hOffSet87_23","hOffSet87_= 0.4",bins,0, xLimit);

TH2D *heOffSet047_0 = new TH2D("heOffSet047-0","OffSet= 0  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet057_0 = new TH2D("heOffSet057-0","OffSet= 0  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet067_0 = new TH2D("heOffSet067-0","OffSet= 0  N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
   TH2D *heOffSet087_0 = new TH2D("heOffSet087-0","OffSet= 0  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0);
  TH2D *heOffSet047_1 = new TH2D("heOffSet047-1","OffSet= 0.01  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_2 = new TH2D("heOffSet047-2","OffSet= 0.03  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_3 = new TH2D("heOffSet047-3","OffSet= 0.05  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_4 = new TH2D("heOffSet047-4","OffSet= 0.07  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_5 = new TH2D("heOffSet047-5","OffSet= 0.09  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_6 = new TH2D("heOffSet047-6","OffSet= 0.11  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_7 = new TH2D("heOffSet047-7","OffSet= 0.13  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_8 = new TH2D("heOffSet047-8","OffSet= 0.15  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_9 = new TH2D("heOffSet047-9","OffSet= 0.17  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_10 = new TH2D("heOffSet047-10","OffSet= 0.19  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_11 = new TH2D("heOffSet047-11","OffSet= 0.21  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_12 = new TH2D("heOffSet047-12","OffSet= 0.23  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_14 = new TH2D("heOffSet047-14","OffSet= 0.27  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_13 = new TH2D("heOffSet047-13","OffSet= 0.25  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_15 = new TH2D("heOffSet047-15","OffSet= 0.29  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_16 = new TH2D("heOffSet047-16","OffSet= 0.31  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_17 = new TH2D("heOffSet047-17","OffSet= 0.33  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_18 = new TH2D("heOffSet047-18","OffSet= 0.35  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_19 = new TH2D("heOffSet047-19","OffSet= 0.37  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_20 = new TH2D("heOffSet047-20","OffSet= 0.39  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_21 = new TH2D("heOffSet047-21","OffSet= 0.41  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_22 = new TH2D("heOffSet047-22","OffSet= 0.43  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet047_23 = new TH2D("heOffSet047-23","OffSet= 0.45  N_Part=0.47 at 3 TeV",bins,0,0,bins,0,0); 
  /// 057-
  TH2D *heOffSet057_1 = new TH2D("heOffSet057-1","OffSet= 0.01  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_2 = new TH2D("heOffSet057-2","OffSet= 0.03  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_3 = new TH2D("heOffSet057-3","OffSet= 0.05  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_4 = new TH2D("heOffSet057-4","OffSet= 0.07  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_5 = new TH2D("heOffSet057-5","OffSet= 0.09  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_6 = new TH2D("heOffSet057-6","OffSet= 0.11  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_7 = new TH2D("heOffSet057-7","OffSet= 0.13  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_8 = new TH2D("heOffSet057-8","OffSet= 0.15  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_9 = new TH2D("heOffSet057-9","OffSet= 0.17  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_10 = new TH2D("heOffSet057-10","OffSet= 0.19 N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_11 = new TH2D("heOffSet057-11","OffSet= 0.21 N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_12 = new TH2D("heOffSet057-12","OffSet= 0.23 N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_14 = new TH2D("heOffSet057-14","OffSet= 0.27 N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_13 = new TH2D("heOffSet057-13","OffSet= 0.25 N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_15 = new TH2D("heOffSet057-15","OffSet= 0.29  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_16 = new TH2D("heOffSet057-16","OffSet= 0.31  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_17 = new TH2D("heOffSet057-17","OffSet= 0.33  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_18 = new TH2D("heOffSet057-18","OffSet= 0.35  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_19 = new TH2D("heOffSet057-19","OffSet= 0.37  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_20 = new TH2D("heOffSet057-20","OffSet= 0.39  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_21 = new TH2D("heOffSet057-21","OffSet= 0.41  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_22 = new TH2D("heOffSet057-22","OffSet= 0.43  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet057_23 = new TH2D("heOffSet057-23","OffSet= 0.45  N_Part=0.57 at 3 TeV",bins,0,0,bins,0,0); 
  //// 067-
  TH2D *heOffSet067_1 = new TH2D("heOffSet067-1","OffSet= 0.01 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_2 = new TH2D("heOffSet067-2","OffSet= 0.03 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_3 = new TH2D("heOffSet067-3","OffSet= 0.05 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_4 = new TH2D("heOffSet067-4","OffSet= 0.07 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_5 = new TH2D("heOffSet067-5","OffSet= 0.09 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_6 = new TH2D("heOffSet067-6","OffSet= 0.11 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_7 = new TH2D("heOffSet067-7","OffSet= 0.13 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_8 = new TH2D("heOffSet067-8","OffSet= 0.15 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_9 = new TH2D("heOffSet067-9","OffSet= 0.17 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_10 = new TH2D("heOffSet067-10","OffSet= 0.19 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_11 = new TH2D("heOffSet067-11","OffSet= 0.21 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_12 = new TH2D("heOffSet067-12","OffSet= 0.23 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_14 = new TH2D("heOffSet067-14","OffSet= 0.27 N_Part=0.67  at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_13 = new TH2D("heOffSet067-13","OffSet= 0.25 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_15 = new TH2D("heOffSet067-15","OffSet= 0.29 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_16 = new TH2D("heOffSet067-16","OffSet= 0.31 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_17 = new TH2D("heOffSet067-17","OffSet= 0.33 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_18 = new TH2D("heOffSet067-18","OffSet= 0.35 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_19 = new TH2D("heOffSet067-19","OffSet= 0.37 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_20 = new TH2D("heOffSet067-20","OffSet= 0.39 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_21 = new TH2D("heOffSet067-21","OffSet= 0.41 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_22 = new TH2D("heOffSet067-22","OffSet= 0.43 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet067_23 = new TH2D("heOffSet067-23","OffSet= 0.45 N_Part=0.67 at 3 TeV",bins,0,0,bins,0,0); 
  //// 087-
  TH2D *heOffSet087_1 = new TH2D("heOffSet087-1","OffSet= 0.01  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_2 = new TH2D("heOffSet087-2","OffSet= 0.03  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_3 = new TH2D("heOffSet087-3","OffSet= 0.05  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_4 = new TH2D("heOffSet087-4","OffSet= 0.07  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_5 = new TH2D("heOffSet087-5","OffSet= 0.09  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_6 = new TH2D("heOffSet087-6","OffSet= 0.11  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_7 = new TH2D("heOffSet087-7","OffSet= 0.13  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_8 = new TH2D("heOffSet087-8","OffSet= 0.15  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_9 = new TH2D("heOffSet087-9","OffSet= 0.17  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_10 = new TH2D("heOffSet087-10","OffSet= 0.19 N_Part=0.87  at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_11 = new TH2D("heOffSet087-11","OffSet= 0.21  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_12 = new TH2D("heOffSet087-12","OffSet= 0.23  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_14 = new TH2D("heOffSet087-14","OffSet= 0.27  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_13 = new TH2D("heOffSet087-13","OffSet= 0.25  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_15 = new TH2D("heOffSet087-15","OffSet= 0.29  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_16 = new TH2D("heOffSet087-16","OffSet= 0.31  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_17 = new TH2D("heOffSet087-17","OffSet= 0.33  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_18 = new TH2D("heOffSet087-18","OffSet= 0.35  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_19 = new TH2D("heOffSet087-19","OffSet= 0.37  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_20 = new TH2D("heOffSet087-20","OffSet= 0.39 N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_21 = new TH2D("heOffSet087-21","OffSet= 0.41 N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_22 = new TH2D("heOffSet087-22","OffSet= 0.43  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  TH2D *heOffSet087_23 = new TH2D("heOffSet087-23","OffSet= 0.45  N_Part=0.87 at 3 TeV",bins,0,0,bins,0,0); 
  
  ///////////////////////////


  FillHistogramsFromTree("../GuineaPig/0.170-0offRun.root",hOffSet017_0, heOffSet017_0,3000,7.28626e+33,7.28728e+33);
   FillHistogramsFromTree("../GuineaPig/0.27-0offRun.root",hOffSet027_0, heOffSet027_0,3000,7.28308e+33,2.07077e+34);
   FillHistogramsFromTree("../GuineaPig/0.47-0offRun.root",hOffSet047_0, heOffSet047_0,3000, 7.27727e+33,7.35626e+34);
   FillHistogramsFromTree("../GuineaPig/0.57-0offRun.root",hOffSet057_0, heOffSet057_0,3000, 7.27079e+33,1.14795e+35);
   FillHistogramsFromTree("../GuineaPig/0.67-0offRun.root",hOffSet067_0, heOffSet067_0,3000, 7.25896e+33,1.66813e+35);
   FillHistogramsFromTree("../GuineaPig/0.87-0.01offRun.root",hOffSet087_0, heOffSet087_0,3000, 7.24858e+33,2.97033e+35);


    FillHistogramsFromTree("../GuineaPig/0.47-0.01offRun.root",hOffSet047_1, heOffSet047_1,3000,7.35375e+34,7.35375e+34);
    FillHistogramsFromTree("../GuineaPig/0.47-0.03offRun.root",hOffSet047_2, heOffSet047_2,3000,7.33465e+34,7.33465e+34);
    FillHistogramsFromTree("../GuineaPig/0.47-0.05offRun.root",hOffSet047_3, heOffSet047_3,3000,7.30343e+34,7.30343e+34);
    FillHistogramsFromTree("../GuineaPig/0.47-0.07offRun.root",hOffSet047_4, heOffSet047_4,3000,7.27267e+34,7.27267e+34);
    FillHistogramsFromTree("../GuineaPig/0.47-0.09offRun.root",hOffSet047_5, heOffSet047_5,3000,7.22863e+34,7.22863e+34);
    FillHistogramsFromTree("../GuineaPig/0.47-0.11offRun.root",hOffSet047_6, heOffSet047_6,3000,7.1784e+34 ,7.1784e+34 );
    FillHistogramsFromTree("../GuineaPig/0.47-0.13offRun.root",hOffSet047_7, heOffSet047_7,3000,7.13615e+34,7.13615e+34);
    FillHistogramsFromTree("../GuineaPig/0.47-0.15offRun.root",hOffSet047_8, heOffSet047_8,3000,7.08845e+34,7.08845e+34); 
    FillHistogramsFromTree("../GuineaPig/0.47-0.17offRun.root",hOffSet047_9, heOffSet047_9,3000,7.02438e+34,7.02438e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.19offRun.root",hOffSet047_10, heOffSet047_10,3000,6.96968e+34,6.96968e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.21offRun.root",hOffSet047_11, heOffSet047_11,3000,6.90808e+34,6.90808e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.23offRun.root",hOffSet047_12, heOffSet047_12,3000,6.86277e+34,6.86277e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.25offRun.root",hOffSet047_13, heOffSet047_13,3000,6.81247e+34,6.81247e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.27offRun.root",hOffSet047_14, heOffSet047_14,3000,6.74394e+34,6.74394e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.29offRun.root",hOffSet047_15, heOffSet047_15,3000,6.6815e+34 ,6.6815e+34 );
  FillHistogramsFromTree("../GuineaPig/0.47-0.31offRun.root",hOffSet047_16, heOffSet047_16,3000,6.61908e+34,6.61908e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.33offRun.root",hOffSet047_17, heOffSet047_17,3000,6.56189e+34,6.56189e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.35offRun.root",hOffSet047_18, heOffSet047_18,3000,6.50688e+34,6.50688e+34); 
  FillHistogramsFromTree("../GuineaPig/0.47-0.37offRun.root",hOffSet047_19, heOffSet047_19,3000,6.43827e+34,6.43827e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.39offRun.root",hOffSet047_20, heOffSet047_20,3000,6.38428e+34,6.38428e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.41offRun.root",hOffSet047_21, heOffSet047_21,3000,6.32877e+34,6.32877e+34);
  FillHistogramsFromTree("../GuineaPig/0.47-0.45offRun.root",hOffSet047_22, heOffSet047_22,3000,6.18964e+34,6.18964e+34); 
  FillHistogramsFromTree("../GuineaPig/0.47-0.47offRun.root",hOffSet047_23, heOffSet047_23,3000,6.13006e+34,6.13006e+34);
  //// 0.57-												    

    FillHistogramsFromTree("../GuineaPig/0.57-0.01offRun.root",hOffSet057_1, heOffSet057_1,3000,1.14735e+35,1.14735e+35);
    FillHistogramsFromTree("../GuineaPig/0.57-0.03offRun.root",hOffSet057_2, heOffSet057_2,3000,1.14208e+35,1.14208e+35);
    FillHistogramsFromTree("../GuineaPig/0.57-0.05offRun.root",hOffSet057_3, heOffSet057_3,3000,1.13588e+35,1.13588e+35);
    FillHistogramsFromTree("../GuineaPig/0.57-0.07offRun.root",hOffSet057_4, heOffSet057_4,3000,1.12578e+35,1.12578e+35);
    FillHistogramsFromTree("../GuineaPig/0.57-0.09offRun.root",hOffSet057_5, heOffSet057_5,3000,1.11495e+35,1.11495e+35);
    FillHistogramsFromTree("../GuineaPig/0.57-0.11offRun.root",hOffSet057_6, heOffSet057_6,3000,1.10236e+35,1.10236e+35);
    FillHistogramsFromTree("../GuineaPig/0.57-0.13offRun.root",hOffSet057_7, heOffSet057_7,3000,1.0938e+35 ,1.0938e+35 );
    FillHistogramsFromTree("../GuineaPig/0.57-0.15offRun.root",hOffSet057_8, heOffSet057_8,3000,1.0812e+35 ,1.0812e+35 ); 
    FillHistogramsFromTree("../GuineaPig/0.57-0.17offRun.root",hOffSet057_9, heOffSet057_9,3000,1.07017e+35,1.07017e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.19offRun.root",hOffSet057_10, heOffSet057_10,3000,1.05906e+35,1.05906e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.21offRun.root",hOffSet057_11, heOffSet057_11,3000,1.0492e+35 ,1.0492e+35 );
  FillHistogramsFromTree("../GuineaPig/0.57-0.23offRun.root",hOffSet057_12, heOffSet057_12,3000,1.03676e+35,1.03676e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.25offRun.root",hOffSet057_13, heOffSet057_13,3000,1.02543e+35,1.02543e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.27offRun.root",hOffSet057_14, heOffSet057_14,3000,1.01505e+35,1.01505e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.29offRun.root",hOffSet057_15, heOffSet057_15,3000,1.00246e+35,1.00246e+35);
  FillHistogramsFromTree("../GuineaPig/0.57-0.31offRun.root",hOffSet057_16, heOffSet057_16,3000,9.9143e+34 ,9.9143e+34 );
  FillHistogramsFromTree("../GuineaPig/0.57-0.33offRun.root",hOffSet057_17, heOffSet057_17,3000,9.81268e+34,9.81268e+34);
  FillHistogramsFromTree("../GuineaPig/0.57-0.35offRun.root",hOffSet057_18, heOffSet057_18,3000,9.70816e+34,9.70816e+34); 
  FillHistogramsFromTree("../GuineaPig/0.57-0.37offRun.root",hOffSet057_19, heOffSet057_19,3000,9.61067e+34,9.61067e+34);
  FillHistogramsFromTree("../GuineaPig/0.57-0.39offRun.root",hOffSet057_20, heOffSet057_20,3000,9.49151e+34,9.49151e+34);
  FillHistogramsFromTree("../GuineaPig/0.57-0.41offRun.root",hOffSet057_21, heOffSet057_21,3000,9.3837e+34 ,9.3837e+34 );
  FillHistogramsFromTree("../GuineaPig/0.57-0.45offRun.root",hOffSet057_22, heOffSet057_22,3000,9.15815e+34,9.15815e+34); 
  FillHistogramsFromTree("../GuineaPig/0.57-0.47offRun.root",hOffSet057_23, heOffSet057_23,3000,9.05204e+34,9.05204e+34);
  //// 0.67-												    

    FillHistogramsFromTree("../GuineaPig/0.67-0.01offRun.root",hOffSet067_1, heOffSet067_1,3000,1.66704e+35,1.66704e+35);
    FillHistogramsFromTree("../GuineaPig/0.67-0.03offRun.root",hOffSet067_2, heOffSet067_2,3000,1.65271e+35,1.65271e+35);
    FillHistogramsFromTree("../GuineaPig/0.67-0.05offRun.root",hOffSet067_3, heOffSet067_3,3000,1.6345e+35 ,1.6345e+35 );
    FillHistogramsFromTree("../GuineaPig/0.67-0.07offRun.root",hOffSet067_4, heOffSet067_4,3000,1.61627e+35,1.61627e+35);
    FillHistogramsFromTree("../GuineaPig/0.67-0.09offRun.root",hOffSet067_5, heOffSet067_5,3000,1.59927e+35,1.59927e+35);
    FillHistogramsFromTree("../GuineaPig/0.67-0.11offRun.root",hOffSet067_6, heOffSet067_6,3000,1.56789e+35,1.56789e+35);
    FillHistogramsFromTree("../GuineaPig/0.67-0.13offRun.root",hOffSet067_7, heOffSet067_7,3000,1.54478e+35,1.54478e+35);
    FillHistogramsFromTree("../GuineaPig/0.67-0.15offRun.root",hOffSet067_8, heOffSet067_8,3000,1.52363e+35,1.52363e+35); 
    FillHistogramsFromTree("../GuineaPig/0.67-0.17offRun.root",hOffSet067_9, heOffSet067_9,3000,1.51055e+35,1.51055e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.19offRun.root",hOffSet067_10, heOffSet067_10,3000,1.484e+35  ,1.484e+35  );
  FillHistogramsFromTree("../GuineaPig/0.67-0.21offRun.root",hOffSet067_11, heOffSet067_11,3000,1.47017e+35,1.47017e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.23offRun.root",hOffSet067_12, heOffSet067_12,3000,1.44827e+35,1.44827e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.25offRun.root",hOffSet067_13, heOffSet067_13,3000,1.4298e+35 ,1.4298e+35 );
  FillHistogramsFromTree("../GuineaPig/0.67-0.27offRun.root",hOffSet067_14, heOffSet067_14,3000,1.40591e+35,1.40591e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.29offRun.root",hOffSet067_15, heOffSet067_15,3000,1.38753e+35,1.38753e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.31offRun.root",hOffSet067_16, heOffSet067_16,3000,1.3761e+35 ,1.3761e+35 );
  FillHistogramsFromTree("../GuineaPig/0.67-0.33offRun.root",hOffSet067_17, heOffSet067_17,3000,1.35972e+35,1.35972e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.35offRun.root",hOffSet067_18, heOffSet067_18,3000,1.33742e+35,1.33742e+35); 
  FillHistogramsFromTree("../GuineaPig/0.67-0.37offRun.root",hOffSet067_19, heOffSet067_19,3000,1.32317e+35,1.32317e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.39offRun.root",hOffSet067_20, heOffSet067_20,3000,1.30644e+35,1.30644e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.41offRun.root",hOffSet067_21, heOffSet067_21,3000,1.28957e+35,1.28957e+35);
  FillHistogramsFromTree("../GuineaPig/0.67-0.45offRun.root",hOffSet067_22, heOffSet067_22,3000,1.26144e+35,1.26144e+35); 
  FillHistogramsFromTree("../GuineaPig/0.67-0.47offRun.root",hOffSet067_23, heOffSet067_23,3000,1.24561e+35,1.24561e+35);
  /////  0.87-												    

    FillHistogramsFromTree("../GuineaPig/0.87-0.01offRun.root",hOffSet087_1, heOffSet087_1,3000,2.94883e+35,2.94883e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.03offRun.root",hOffSet087_2, heOffSet087_2,3000,2.90133e+35,2.90133e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.05offRun.root",hOffSet087_3, heOffSet087_3,3000,2.84763e+35,2.84763e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.07offRun.root",hOffSet087_4, heOffSet087_4,3000,2.82894e+35,2.82894e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.09offRun.root",hOffSet087_5, heOffSet087_5,3000,2.72582e+35,2.72582e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.11offRun.root",hOffSet087_6, heOffSet087_6,3000,2.68126e+35,2.68126e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.13offRun.root",hOffSet087_7, heOffSet087_7,3000,2.60341e+35,2.60341e+35);
    FillHistogramsFromTree("../GuineaPig/0.87-0.15offRun.root",hOffSet087_8, heOffSet087_8,3000,2.57208e+35,2.57208e+35); 
    FillHistogramsFromTree("../GuineaPig/0.87-0.17offRun.root",hOffSet087_9, heOffSet087_9,3000,2.52713e+35,2.52713e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.19offRun.root",hOffSet087_10, heOffSet087_10,3000,2.48519e+35,2.48519e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.21offRun.root",hOffSet087_11, heOffSet087_11,3000,2.43857e+35,2.43857e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.23offRun.root",hOffSet087_12, heOffSet087_12,3000,2.40031e+35,2.40031e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.25offRun.root",hOffSet087_13, heOffSet087_13,3000,2.3617e+35 ,2.3617e+35 );
  FillHistogramsFromTree("../GuineaPig/0.87-0.27offRun.root",hOffSet087_14, heOffSet087_14,3000,2.32019e+35,2.32019e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.29offRun.root",hOffSet087_15, heOffSet087_15,3000,2.29355e+35,2.29355e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.31offRun.root",hOffSet087_16, heOffSet087_16,3000,2.24905e+35,2.24905e+35); 
  FillHistogramsFromTree("../GuineaPig/0.87-0.33offRun.root",hOffSet087_17, heOffSet087_17,3000,2.22158e+35,2.22158e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.35offRun.root",hOffSet087_18, heOffSet087_18,3000,2.18396e+35,2.18396e+35); 
  FillHistogramsFromTree("../GuineaPig/0.87-0.37offRun.root",hOffSet087_19, heOffSet087_19,3000,2.15886e+35,2.15886e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.39offRun.root",hOffSet087_20, heOffSet087_20,3000,2.132e+35  ,2.132e+35  ); 
  FillHistogramsFromTree("../GuineaPig/0.87-0.41offRun.root",hOffSet087_21, heOffSet087_21,3000,2.10097e+35,2.10097e+35);
  FillHistogramsFromTree("../GuineaPig/0.87-0.45offRun.root",hOffSet087_22, heOffSet087_22,3000,2.03525e+35,2.03525e+35); 
  FillHistogramsFromTree("../GuineaPig/0.87-0.47offRun.root",hOffSet087_23, heOffSet087_23,3000,2.01426e+35,2.01426e+35);


 TCanvas *c47 = new TCanvas("E1/Enom : E2/nom 47","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  c47->Divide(6,3);
  c47->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");


  c47->cd(2);
  heOffSet047_1->Draw("colz");
  heOffSet047_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_1->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(3);
  heOffSet047_2->Draw("colz");
  heOffSet047_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_2->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(4);
  heOffSet047_3->Draw("colz");
  heOffSet047_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_3->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(5);
  heOffSet047_4->Draw("colz");
  heOffSet047_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_4->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(6);
  heOffSet047_5->Draw("colz");
  heOffSet047_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_5->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(7);
  heOffSet047_6->Draw("colz");
  
  heOffSet047_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_6->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(8);
  heOffSet047_7->Draw("colz");
  heOffSet047_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_7->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(9);
  heOffSet047_8->Draw("colz");
  heOffSet047_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_8->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(10);
  heOffSet047_9->Draw("colz");
  heOffSet047_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_9->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(11);
  heOffSet047_10->Draw("colz");
  heOffSet047_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_10->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(12);
  heOffSet047_11->Draw("colz");
  heOffSet047_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_11->SetAxisRange(0.98,1.0063,"Y");

  c47->cd(13);
  heOffSet047_12->Draw("colz");
  heOffSet047_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_12->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(14);
  heOffSet047_13->Draw("colz");
  heOffSet047_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_13->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(15);
  heOffSet047_14->Draw("colz");
  heOffSet047_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_14->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(16);
  heOffSet047_15->Draw("colz");
  heOffSet047_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_15->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(17);
  heOffSet047_16->Draw("colz");
  heOffSet047_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_16->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(18);
  heOffSet047_17->Draw("colz");
  heOffSet047_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_17->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(19);
  heOffSet047_18->Draw("colz");
  heOffSet047_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_18->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(20);
  heOffSet047_19->Draw("colz");
  heOffSet047_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_19->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(21);
  heOffSet047_20->Draw("colz");
  heOffSet047_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_20->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(22);
  heOffSet047_21->Draw("colz");
  heOffSet047_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_21->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(23);
  heOffSet047_22->Draw("colz");
  heOffSet047_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_22->SetAxisRange(0.98,1.0063,"Y");
  c47->cd(24);
  heOffSet047_23->Draw("colz");
  heOffSet047_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet047_23->SetAxisRange(0.98,1.0063,"Y");
  c47->SaveAs("Lumi2D_OffSet-047.png");

TCanvas *f0 = new TCanvas("0","offset = 0",900,900);
heOffSet047_0->Draw("colz");
f0->SetGridx(1);
f0->SetGridy(1);
heOffSet047_0->SetAxisRange(0.98,1.0063,"X");
heOffSet047_0->SetAxisRange(0.98,1.0063,"Y");
f0->SaveAs("../offset/47/0.png");
heOffSet047_0->SetAxisRange(0,1.0063,"X");
heOffSet047_0->SetAxisRange(0,1.0063,"Y");
f0->SaveAs("../offset/47/range/0.png");

TCanvas *f1 = new TCanvas("1","offset = 0",900,900);
f1->SetGridx(1);
f1->SetGridy(1);
heOffSet047_1->Draw("colz");
heOffSet047_1->SetAxisRange(0.98,1.0063,"X");
heOffSet047_1->SetAxisRange(0.98,1.0063,"Y");
f1->SaveAs("../offset/47/1.png");
heOffSet047_1->SetAxisRange(0,1.0063,"X");
heOffSet047_1->SetAxisRange(0,1.0063,"Y");
f1->SaveAs("../offset/47/range/1.png");


TCanvas *f2 = new TCanvas("2","offset = 0",900,900);
f2->SetGridx(1);
f2->SetGridy(1);
heOffSet047_2->Draw("colz");
heOffSet047_2->SetAxisRange(0.98,1.0063,"X");
heOffSet047_2->SetAxisRange(0.98,1.0063,"Y");
f2->SaveAs("../offset/47/2.png");
heOffSet047_2->SetAxisRange(0,1.0063,"X");
heOffSet047_2->SetAxisRange(0,1.0063,"Y");
f2->SaveAs("../offset/47/range/2.png");
 

TCanvas *f3 = new TCanvas("3","offset = 0",900,900);
f3->SetGridx(1);
f3->SetGridy(1);
heOffSet047_3->Draw("colz");
heOffSet047_3->SetAxisRange(0.98,1.0063,"X");
heOffSet047_3->SetAxisRange(0.98,1.0063,"Y");
f3->SaveAs("../offset/47/3.png");
heOffSet047_3->SetAxisRange(0,1.0063,"X");
heOffSet047_3->SetAxisRange(0,1.0063,"Y");
f3->SaveAs("../offset/47/range/3.png");

TCanvas *f4 = new TCanvas("4","offset = 0",900,900);
f4->SetGridx(1);
f4->SetGridy(1);
heOffSet047_4->Draw("colz");
heOffSet047_4->SetAxisRange(0.98,1.0063,"X");
heOffSet047_4->SetAxisRange(0.98,1.0063,"Y");
f4->SaveAs("../offset/47/4.png");
heOffSet047_4->SetAxisRange(0,1.0063,"X");
heOffSet047_4->SetAxisRange(0,1.0063,"Y");
f4->SaveAs("../offset/47/range/4.png");
 

TCanvas *f5 = new TCanvas("5","offset = 0",900,900);
f5->SetGridx(1);
f5->SetGridy(1);
heOffSet047_5->Draw("colz");
heOffSet047_5->SetAxisRange(0.98,1.0063,"X");
heOffSet047_5->SetAxisRange(0.98,1.0063,"Y");
f5->SaveAs("../offset/47/5.png");
heOffSet047_5->SetAxisRange(0,1.0063,"X");
heOffSet047_5->SetAxisRange(0,1.0063,"Y");
f5->SaveAs("../offset/47/range/5.png");




TCanvas *f6 = new TCanvas("6","offset = 0",900,900);
f6->SetGridx(1);
f6->SetGridy(1);
heOffSet047_6->Draw("colz");
heOffSet047_6->SetAxisRange(0.98,1.0063,"X");
heOffSet047_6->SetAxisRange(0.98,1.0063,"Y");
f6->SaveAs("../offset/47/6.png");
heOffSet047_6->SetAxisRange(0,1.0063,"X");
heOffSet047_6->SetAxisRange(0,1.0063,"Y");
f6->SaveAs("../offset/47/range/6.png");



TCanvas *f7 = new TCanvas("7","offset = 0",900,900);
f7->SetGridx(1);
f7->SetGridy(1);
heOffSet047_7->Draw("colz");
heOffSet047_7->SetAxisRange(0.98,1.0063,"X");
heOffSet047_7->SetAxisRange(0.98,1.0063,"Y");
f7->SaveAs("../offset/47/7.png");
heOffSet047_7->SetAxisRange(0,1.0063,"X");
heOffSet047_7->SetAxisRange(0,1.0063,"Y");
f7->SaveAs("../offset/47/range/7.png");

TCanvas *f8 = new TCanvas("8","offset = 0",900,900);
f8->SetGridx(1);
f8->SetGridy(1);
heOffSet047_8->Draw("colz");
heOffSet047_8->SetAxisRange(0.98,1.0063,"X");
heOffSet047_8->SetAxisRange(0.98,1.0063,"Y");
f8->SaveAs("../offset/47/8.png");
heOffSet047_8->SetAxisRange(0,1.0063,"X");
heOffSet047_8->SetAxisRange(0,1.0063,"Y");
f8->SaveAs("../offset/47/range/8.png");

TCanvas *f9 = new TCanvas("9","offset = 0",900,900);
f9->SetGridx(1);
f9->SetGridy(1);
heOffSet047_9->Draw("colz");
heOffSet047_9->SetAxisRange(0.98,1.0063,"X");
heOffSet047_9->SetAxisRange(0.98,1.0063,"Y");
f9->SaveAs("../offset/47/9.png");
heOffSet047_9->SetAxisRange(0,1.0063,"X");
heOffSet047_9->SetAxisRange(0,1.0063,"Y");
f9->SaveAs("../offset/47/range/9.png");


TCanvas *f10 = new TCanvas("10","offset = 0",900,900);
f10->SetGridx(1);
f10->SetGridy(1);
heOffSet047_10->Draw("colz");
heOffSet047_10->SetAxisRange(0.98,1.0063,"X");
heOffSet047_10->SetAxisRange(0.98,1.0063,"Y");
f10->SaveAs("../offset/47/10.png");
heOffSet047_10->SetAxisRange(0,1.0063,"X");
heOffSet047_10->SetAxisRange(0,1.0063,"Y");
f10->SaveAs("../offset/47/range/10.png");

TCanvas *f11 = new TCanvas("11","offset = 0",900,900);
f11->SetGridx(1);
f11->SetGridy(1);
heOffSet047_11->Draw("colz");
heOffSet047_11->SetAxisRange(0.98,1.0063,"X");
heOffSet047_11->SetAxisRange(0.98,1.0063,"Y");
f11->SaveAs("../offset/47/11.png");
heOffSet047_11->SetAxisRange(0,1.0063,"X");
heOffSet047_11->SetAxisRange(0,1.0063,"Y");
f11->SaveAs("../offset/47/range/11.png");

TCanvas *f12 = new TCanvas("12","offset = 0",900,900);
f12->SetGridx(1);
f12->SetGridy(1);
heOffSet047_12->Draw("colz");
heOffSet047_12->SetAxisRange(0.98,1.0063,"X");
heOffSet047_12->SetAxisRange(0.98,1.0063,"Y");
f12->SaveAs("../offset/47/12.png");
heOffSet047_12->SetAxisRange(0,1.0063,"X");
heOffSet047_12->SetAxisRange(0,1.0063,"Y");
f12->SaveAs("../offset/47/range/12.png");

TCanvas *f13 = new TCanvas("13","offset = 0",900,900);
f13->SetGridx(1);
f13->SetGridy(1);
heOffSet047_13->Draw("colz");
heOffSet047_13->SetAxisRange(0.98,1.0063,"X");
heOffSet047_13->SetAxisRange(0.98,1.0063,"Y");
f13->SaveAs("../offset/47/13.png");
heOffSet047_13->SetAxisRange(0,1.0063,"X");
heOffSet047_13->SetAxisRange(0,1.0063,"Y");
f13->SaveAs("../offset/47/range/13.png");

TCanvas *f14 = new TCanvas("14","offset = 0",900,900);
f14->SetGridx(1);
f14->SetGridy(1);
heOffSet047_14->Draw("colz");
heOffSet047_14->SetAxisRange(0.98,1.0063,"X");
heOffSet047_14->SetAxisRange(0.98,1.0063,"Y");
f14->SaveAs("../offset/47/14.png");
heOffSet047_14->SetAxisRange(0,1.0063,"X");
heOffSet047_14->SetAxisRange(0,1.0063,"Y");
f14->SaveAs("../offset/47/range/14.png");

TCanvas *f15 = new TCanvas("15","offset = 0",900,900);
f15->SetGridx(1);
f15->SetGridy(1);
heOffSet047_15->Draw("colz");
heOffSet047_15->SetAxisRange(0.98,1.0063,"X");
heOffSet047_15->SetAxisRange(0.98,1.0063,"Y");
f15->SaveAs("../offset/47/15.png");
heOffSet047_15->SetAxisRange(0,1.0063,"X");
heOffSet047_15->SetAxisRange(0,1.0063,"Y");
f15->SaveAs("../offset/47/range/15.png");

TCanvas *f16 = new TCanvas("16","offset = 0",900,900);
f16->SetGridx(1);
f16->SetGridy(1);
heOffSet047_16->Draw("colz");
heOffSet047_16->SetAxisRange(0.98,1.0063,"X");
heOffSet047_16->SetAxisRange(0.98,1.0063,"Y");
f16->SaveAs("../offset/47/16.png");
heOffSet047_16->SetAxisRange(0,1.0063,"X");
heOffSet047_16->SetAxisRange(0,1.0063,"Y");
f16->SaveAs("../offset/47/range/16.png");



TCanvas *f17 = new TCanvas("17","offset = 0",900,900);
f17->SetGridx(1);
f17->SetGridy(1);
heOffSet047_17->Draw("colz");
heOffSet047_17->SetAxisRange(0.98,1.0063,"X");
heOffSet047_17->SetAxisRange(0.98,1.0063,"Y");
f17->SaveAs("../offset/47/17.png");
heOffSet047_17->SetAxisRange(0,1.0063,"X");
heOffSet047_17->SetAxisRange(0,1.0063,"Y");
f17->SaveAs("../offset/47/range/17.png");


TCanvas *f18 = new TCanvas("18","offset = 0",900,900);
f18->SetGridx(1);
f18->SetGridy(1);
heOffSet047_18->Draw("colz");
heOffSet047_18->SetAxisRange(0.98,1.0063,"X");
heOffSet047_18->SetAxisRange(0.98,1.0063,"Y");
f18->SaveAs("../offset/47/18.png");
heOffSet047_18->SetAxisRange(0,1.0063,"X");
heOffSet047_18->SetAxisRange(0,1.0063,"Y");
f18->SaveAs("../offset/47/range/18.png");

TCanvas *f19 = new TCanvas("19","offset = 0",900,900);
f19->SetGridx(1);
f19->SetGridy(1);
heOffSet047_19->Draw("colz");
heOffSet047_19->SetAxisRange(0.98,1.0063,"X");
heOffSet047_19->SetAxisRange(0.98,1.0063,"Y");
f19->SaveAs("../offset/47/19.png");
heOffSet047_19->SetAxisRange(0,1.0063,"X");
heOffSet047_19->SetAxisRange(0,1.0063,"Y");
f19->SaveAs("../offset/47/range/19.png");


TCanvas *f20 = new TCanvas("20","offset = 0",900,900);
f20->SetGridx(1);
f20->SetGridy(1);
heOffSet047_20->Draw("colz");
heOffSet047_20->SetAxisRange(0.98,1.0063,"X");
heOffSet047_20->SetAxisRange(0.98,1.0063,"Y");
f20->SaveAs("../offset/47/20.png");
heOffSet047_20->SetAxisRange(0,1.0063,"X");
heOffSet047_20->SetAxisRange(0,1.0063,"Y");
f20->SaveAs("../offset/47/range/20.png");
  
TCanvas *f21 = new TCanvas("21","offset = 0",900,900);
f21->SetGridx(1);
f21->SetGridy(1);
heOffSet047_21->Draw("colz");
heOffSet047_21->SetAxisRange(0.98,1.0063,"X");
heOffSet047_21->SetAxisRange(0.98,1.0063,"Y");
f21->SaveAs("../offset/47/21.png");
heOffSet047_21->SetAxisRange(0,1.0063,"X");
heOffSet047_21->SetAxisRange(0,1.0063,"Y");
f21->SaveAs("../offset/47/range/21.png");

TCanvas *f22 = new TCanvas("22","offset = 0",900,900);
f22->SetGridx(1);
f22->SetGridy(1);
heOffSet047_22->Draw("colz");
heOffSet047_22->SetAxisRange(0.98,1.0063,"X");
heOffSet047_22->SetAxisRange(0.98,1.0063,"Y");
f22->SaveAs("../offset/47/22.png");
heOffSet047_22->SetAxisRange(0,1.0063,"X");
heOffSet047_22->SetAxisRange(0,1.0063,"Y");
f22->SaveAs("../offset/47/range/22.png");

TCanvas *f23 = new TCanvas("23","offset = 0",900,900);
f23->SetGridx(1);
f23->SetGridy(1);
heOffSet047_23->Draw("colz");
heOffSet047_23->SetAxisRange(0.98,1.0063,"X");
heOffSet047_23->SetAxisRange(0.98,1.0063,"Y");
f23->SaveAs("../offset/47/23.png");
heOffSet047_23->SetAxisRange(0,1.0063,"X");
heOffSet047_23->SetAxisRange(0,1.0063,"Y");
f23->SaveAs("../offset/47/range/23.png");

  //// 0.47 0.57 0.67 0.87 1;
 //// 0.47 0.57 0.67 0.87 1;
TCanvas *c57 = new TCanvas("E1/Enom : E2/nom 57","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  c57->Divide(6,3);
  c57->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(2);
  heOffSet057_1->Draw("colz");
  heOffSet057_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_1->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(3);
  heOffSet057_2->Draw("colz");
  heOffSet057_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_2->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(4);
  heOffSet057_3->Draw("colz");
  heOffSet057_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_3->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(5);
  heOffSet057_4->Draw("colz");
  heOffSet057_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_4->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(6);
  heOffSet057_5->Draw("colz");
  heOffSet057_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_5->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(7);
  heOffSet057_6->Draw("colz");
  heOffSet057_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_6->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(8);
  heOffSet057_7->Draw("colz");
  heOffSet057_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_7->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(9);
  heOffSet057_8->Draw("colz");
  heOffSet057_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_8->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(10);
  heOffSet057_9->Draw("colz");
  heOffSet057_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_9->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(11);
  heOffSet057_10->Draw("colz");
  heOffSet057_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_10->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(12);
  heOffSet057_11->Draw("colz");
  heOffSet057_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_11->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(13);
  heOffSet057_12->Draw("colz");
  heOffSet057_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_12->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(14);
  heOffSet057_13->Draw("colz");
  heOffSet057_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_13->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(15);
  heOffSet057_14->Draw("colz");
  heOffSet057_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_14->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(16);
  heOffSet057_15->Draw("colz");
  heOffSet057_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_15->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(17);
  heOffSet057_16->Draw("colz");
  heOffSet057_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_16->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(18);
  heOffSet057_17->Draw("colz");
  heOffSet057_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_17->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(19);
  heOffSet057_18->Draw("colz");
  heOffSet057_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_18->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(20);
  heOffSet057_19->Draw("colz");
  heOffSet057_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_19->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(21);
  heOffSet057_20->Draw("colz");
  heOffSet057_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_20->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(22);
  heOffSet057_21->Draw("colz");
  heOffSet057_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_21->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(23);
  heOffSet057_22->Draw("colz");
  heOffSet057_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_22->SetAxisRange(0.98,1.0063,"Y");
  c57->cd(24);
  heOffSet057_23->Draw("colz");
  heOffSet057_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet057_23->SetAxisRange(0.98,1.0063,"Y");
  c57->SaveAs("Lumi2D_OffSet-057.png");
  //// 0.47 0.57 0.67 0.87 1;

TCanvas *g0 = new TCanvas("0","offset = 0",900,900);
heOffSet057_0->Draw("colz");
g0->SetGridx(1);
g0->SetGridy(1);
 heOffSet057_0->SetAxisRange(0.98,1.0063,"X");
heOffSet057_0->SetAxisRange(0.98,1.0063,"Y");
g0->SaveAs("../offset/57/0.png");
heOffSet057_0->SetAxisRange(0,1.0063,"X");
heOffSet057_0->SetAxisRange(0,1.0063,"Y");
g0->SaveAs("../offset/57/range/0.png");

TCanvas *g1 = new TCanvas("1","offset = 0",900,900);
g1->SetGridx(1);
g1->SetGridy(1);
heOffSet057_1->Draw("colz");
heOffSet057_1->SetAxisRange(0.98,1.0063,"X");
heOffSet057_1->SetAxisRange(0.98,1.0063,"Y");
g1->SaveAs("../offset/57/1.png");
heOffSet057_1->SetAxisRange(0,1.0063,"X");
heOffSet057_1->SetAxisRange(0,1.0063,"Y");
g1->SaveAs("../offset/57/range/1.png");


TCanvas *g2 = new TCanvas("2","offset = 0",900,900);
g2->SetGridx(1);
g2->SetGridy(1);
heOffSet057_2->Draw("colz");
heOffSet057_2->SetAxisRange(0.98,1.0063,"X");
heOffSet057_2->SetAxisRange(0.98,1.0063,"Y");
g2->SaveAs("../offset/57/2.png");
heOffSet057_2->SetAxisRange(0,1.0063,"X");
heOffSet057_2->SetAxisRange(0,1.0063,"Y");
g2->SaveAs("../offset/57/range/2.png");
 

TCanvas *g3 = new TCanvas("3","offset = 0",900,900);
g3->SetGridx(1);
g3->SetGridy(1);
heOffSet057_3->Draw("colz");
heOffSet057_3->SetAxisRange(0.98,1.0063,"X");
heOffSet057_3->SetAxisRange(0.98,1.0063,"Y");
g3->SaveAs("../offset/57/3.png");
heOffSet057_3->SetAxisRange(0,1.0063,"X");
heOffSet057_3->SetAxisRange(0,1.0063,"Y");
g3->SaveAs("../offset/57/range/3.png");

TCanvas *g4 = new TCanvas("4","offset = 0",900,900);
g4->SetGridx(1);
g4->SetGridy(1);
heOffSet057_4->Draw("colz");
heOffSet057_4->SetAxisRange(0.98,1.0063,"X");
heOffSet057_4->SetAxisRange(0.98,1.0063,"Y");
g4->SaveAs("../offset/57/4.png");
heOffSet057_4->SetAxisRange(0,1.0063,"X");
heOffSet057_4->SetAxisRange(0,1.0063,"Y");
g4->SaveAs("../offset/57/range/4.png");
 

TCanvas *g5 = new TCanvas("5","offset = 0",900,900);
g5->SetGridx(1);
g5->SetGridy(1);
heOffSet057_5->Draw("colz");
heOffSet057_5->SetAxisRange(0.98,1.0063,"X");
heOffSet057_5->SetAxisRange(0.98,1.0063,"Y");
g5->SaveAs("../offset/57/5.png");
heOffSet057_5->SetAxisRange(0,1.0063,"X");
heOffSet057_5->SetAxisRange(0,1.0063,"Y");
g5->SaveAs("../offset/57/range/5.png");



TCanvas *g6 = new TCanvas("6","offset = 0",900,900);
g6->SetGridx(1);
g6->SetGridy(1);
heOffSet057_6->Draw("colz");
heOffSet057_6->SetAxisRange(0.98,1.0063,"X");
heOffSet057_6->SetAxisRange(0.98,1.0063,"Y");
g6->SaveAs("../offset/57/6.png");
heOffSet057_6->SetAxisRange(0,1.0063,"X");
heOffSet057_6->SetAxisRange(0,1.0063,"Y");
g6->SaveAs("../offset/57/range/6.png");



TCanvas *g7 = new TCanvas("7","offset = 0",900,900);
g7->SetGridx(1);
g7->SetGridy(1);
heOffSet057_7->Draw("colz");
heOffSet057_7->SetAxisRange(0.98,1.0063,"X");
heOffSet057_7->SetAxisRange(0.98,1.0063,"Y");
g7->SaveAs("../offset/57/7.png");
heOffSet057_7->SetAxisRange(0,1.0063,"X");
heOffSet057_7->SetAxisRange(0,1.0063,"Y");
g7->SaveAs("../offset/57/range/7.png");

TCanvas *g8 = new TCanvas("8","offset = 0",900,900);
g8->SetGridx(1);
g8->SetGridy(1);
heOffSet057_8->Draw("colz");
heOffSet057_8->SetAxisRange(0.98,1.0063,"X");
heOffSet057_8->SetAxisRange(0.98,1.0063,"Y");
g8->SaveAs("../offset/57/8.png");
heOffSet057_8->SetAxisRange(0,1.0063,"X");
heOffSet057_8->SetAxisRange(0,1.0063,"Y");
g8->SaveAs("../offset/57/range/8.png");

TCanvas *g9 = new TCanvas("9","offset = 0",900,900);
g9->SetGridx(1);
g9->SetGridy(1);
heOffSet057_9->Draw("colz");
heOffSet057_9->SetAxisRange(0.98,1.0063,"X");
heOffSet057_9->SetAxisRange(0.98,1.0063,"Y");
g9->SaveAs("../offset/57/9.png");
heOffSet057_9->SetAxisRange(0,1.0063,"X");
heOffSet057_9->SetAxisRange(0,1.0063,"Y");
g9->SaveAs("../offset/57/range/9.png");


TCanvas *g10 = new TCanvas("10","offset = 0",900,900);
g10->SetGridx(1);
g10->SetGridy(1);
heOffSet057_10->Draw("colz");
heOffSet057_10->SetAxisRange(0.98,1.0063,"X");
heOffSet057_10->SetAxisRange(0.98,1.0063,"Y");
g10->SaveAs("../offset/57/10.png");
heOffSet057_10->SetAxisRange(0,1.0063,"X");
heOffSet057_10->SetAxisRange(0,1.0063,"Y");
g10->SaveAs("../offset/57/range/10.png");

TCanvas *g11 = new TCanvas("11","offset = 0",900,900);
g11->SetGridx(1);
g11->SetGridy(1);
heOffSet057_11->Draw("colz");
heOffSet057_11->SetAxisRange(0.98,1.0063,"X");
heOffSet057_11->SetAxisRange(0.98,1.0063,"Y");
g11->SaveAs("../offset/57/11.png");
heOffSet057_11->SetAxisRange(0,1.0063,"X");
heOffSet057_11->SetAxisRange(0,1.0063,"Y");
g11->SaveAs("../offset/57/range/11.png");

TCanvas *g12 = new TCanvas("12","offset = 0",900,900);
g12->SetGridx(1);
g12->SetGridy(1);
heOffSet057_12->Draw("colz");
heOffSet057_12->SetAxisRange(0.98,1.0063,"X");
heOffSet057_12->SetAxisRange(0.98,1.0063,"Y");
g12->SaveAs("../offset/57/12.png");
heOffSet057_12->SetAxisRange(0,1.0063,"X");
heOffSet057_12->SetAxisRange(0,1.0063,"Y");
g12->SaveAs("../offset/57/range/12.png");

TCanvas *g13 = new TCanvas("13","offset = 0",900,900);
g13->SetGridx(1);
g13->SetGridy(1);
heOffSet057_13->Draw("colz");
heOffSet057_13->SetAxisRange(0.98,1.0063,"X");
heOffSet057_13->SetAxisRange(0.98,1.0063,"Y");
g13->SaveAs("../offset/57/13.png");
heOffSet057_13->SetAxisRange(0,1.0063,"X");
heOffSet057_13->SetAxisRange(0,1.0063,"Y");
g13->SaveAs("../offset/57/range/13.png");

TCanvas *g14 = new TCanvas("14","offset = 0",900,900);
g14->SetGridx(1);
g14->SetGridy(1);
heOffSet057_14->Draw("colz");
heOffSet057_14->SetAxisRange(0.98,1.0063,"X");
heOffSet057_14->SetAxisRange(0.98,1.0063,"Y");
g14->SaveAs("../offset/57/14.png");
heOffSet057_14->SetAxisRange(0,1.0063,"X");
heOffSet057_14->SetAxisRange(0,1.0063,"Y");
g14->SaveAs("../offset/57/range/14.png");

TCanvas *g15 = new TCanvas("15","offset = 0",900,900);
g15->SetGridx(1);
g15->SetGridy(1);
heOffSet057_15->Draw("colz");
heOffSet057_15->SetAxisRange(0.98,1.0063,"X");
heOffSet057_15->SetAxisRange(0.98,1.0063,"Y");
g15->SaveAs("../offset/57/15.png");
heOffSet057_15->SetAxisRange(0,1.0063,"X");
heOffSet057_15->SetAxisRange(0,1.0063,"Y");
g15->SaveAs("../offset/57/range/15.png");

TCanvas *g16 = new TCanvas("16","offset = 0",900,900);
g16->SetGridx(1);
g16->SetGridy(1);
heOffSet057_16->Draw("colz");
heOffSet057_16->SetAxisRange(0.98,1.0063,"X");
heOffSet057_16->SetAxisRange(0.98,1.0063,"Y");
g16->SaveAs("../offset/57/16.png");
heOffSet057_16->SetAxisRange(0,1.0063,"X");
heOffSet057_16->SetAxisRange(0,1.0063,"Y");
g16->SaveAs("../offset/57/range/16.png");



TCanvas *g17 = new TCanvas("17","offset = 0",900,900);
g17->SetGridx(1);
g17->SetGridy(1);
heOffSet057_17->Draw("colz");
heOffSet057_17->SetAxisRange(0.98,1.0063,"X");
heOffSet057_17->SetAxisRange(0.98,1.0063,"Y");
g17->SaveAs("../offset/57/17.png");
heOffSet057_17->SetAxisRange(0,1.0063,"X");
heOffSet057_17->SetAxisRange(0,1.0063,"Y");
g17->SaveAs("../offset/57/range/17.png");


TCanvas *g18 = new TCanvas("18","offset = 0",900,900);
g18->SetGridx(1);
g18->SetGridy(1);
heOffSet057_18->Draw("colz");
heOffSet057_18->SetAxisRange(0.98,1.0063,"X");
heOffSet057_18->SetAxisRange(0.98,1.0063,"Y");
g18->SaveAs("../offset/57/18.png");
heOffSet057_18->SetAxisRange(0,1.0063,"X");
heOffSet057_18->SetAxisRange(0,1.0063,"Y");
g18->SaveAs("../offset/57/range/18.png");

TCanvas *g19 = new TCanvas("19","offset = 0",900,900);
g19->SetGridx(1);
g19->SetGridy(1);
heOffSet057_19->Draw("colz");
heOffSet057_19->SetAxisRange(0.98,1.0063,"X");
heOffSet057_19->SetAxisRange(0.98,1.0063,"Y");
g19->SaveAs("../offset/57/19.png");
heOffSet057_19->SetAxisRange(0,1.0063,"X");
heOffSet057_19->SetAxisRange(0,1.0063,"Y");
g19->SaveAs("../offset/57/range/19.png");


TCanvas *g20 = new TCanvas("20","offset = 0",900,900);
g20->SetGridx(1);
g20->SetGridy(1);
heOffSet057_20->Draw("colz");
heOffSet057_20->SetAxisRange(0.98,1.0063,"X");
heOffSet057_20->SetAxisRange(0.98,1.0063,"Y");
g20->SaveAs("../offset/57/20.png");
heOffSet057_20->SetAxisRange(0,1.0063,"X");
heOffSet057_20->SetAxisRange(0,1.0063,"Y");
g20->SaveAs("../offset/57/range/20.png");
  
TCanvas *g21 = new TCanvas("21","offset = 0",900,900);
g21->SetGridx(1);
g21->SetGridy(1);
heOffSet057_21->Draw("colz");
heOffSet057_21->SetAxisRange(0.98,1.0063,"X");
heOffSet057_21->SetAxisRange(0.98,1.0063,"Y");
g21->SaveAs("../offset/57/21.png");
heOffSet057_21->SetAxisRange(0,1.0063,"X");
heOffSet057_21->SetAxisRange(0,1.0063,"Y");
g21->SaveAs("../offset/57/range/21.png");

TCanvas *g22 = new TCanvas("22","offset = 0",900,900);
g22->SetGridx(1);
g22->SetGridy(1);
heOffSet057_22->Draw("colz");
heOffSet057_22->SetAxisRange(0.98,1.0063,"X");
heOffSet057_22->SetAxisRange(0.98,1.0063,"Y");
g22->SaveAs("../offset/57/22.png");
heOffSet057_22->SetAxisRange(0,1.0063,"X");
heOffSet057_22->SetAxisRange(0,1.0063,"Y");
g22->SaveAs("../offset/57/range/22.png");

TCanvas *g23 = new TCanvas("23","offset = 0",900,900);
g23->SetGridx(1);
g23->SetGridy(1);
heOffSet057_23->Draw("colz");
heOffSet057_23->SetAxisRange(0.98,1.0063,"X");
heOffSet057_23->SetAxisRange(0.98,1.0063,"Y");
g23->SaveAs("../offset/57/23.png");
heOffSet057_23->SetAxisRange(0,1.0063,"X");
heOffSet057_23->SetAxisRange(0,1.0063,"Y");
g23->SaveAs("../offset/57/range/23.png");

  //// 0.47 0.57 0.67 0.87 1;
 //// 0.47 0.57 0.67 0.87 1;
TCanvas *c67 = new TCanvas("E1/Enom : E2/nom 67","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  c67->Divide(6,3);
  c67->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(2);
  heOffSet067_1->Draw("colz");
  heOffSet067_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_1->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(3);
  heOffSet067_2->Draw("colz");
  heOffSet067_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_2->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(4);
  heOffSet067_3->Draw("colz");
  heOffSet067_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_3->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(5);
  heOffSet067_4->Draw("colz");
  heOffSet067_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_4->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(6);
  heOffSet067_5->Draw("colz");
  heOffSet067_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_5->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(7);
  heOffSet067_6->Draw("colz");
  heOffSet067_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_6->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(8);
  heOffSet067_7->Draw("colz");
  heOffSet067_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_7->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(9);
  heOffSet067_8->Draw("colz");
  heOffSet067_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_8->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(10);
  heOffSet067_9->Draw("colz");
  heOffSet067_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_9->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(11);
  heOffSet067_10->Draw("colz");
  heOffSet067_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_10->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(12);
  heOffSet067_11->Draw("colz");
  heOffSet067_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_11->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(13);
  heOffSet067_12->Draw("colz");
  heOffSet067_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_12->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(14);
  heOffSet067_13->Draw("colz");
  heOffSet067_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_13->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(15);
  heOffSet067_14->Draw("colz");
  heOffSet067_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_14->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(16);
  heOffSet067_15->Draw("colz");
  heOffSet067_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_15->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(17);
  heOffSet067_16->Draw("colz");
  heOffSet067_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_16->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(18);
  heOffSet067_17->Draw("colz");
  heOffSet067_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_17->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(19);
  heOffSet067_18->Draw("colz");
  heOffSet067_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_18->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(20);
  heOffSet067_19->Draw("colz");
  heOffSet067_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_19->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(21);
  heOffSet067_20->Draw("colz");
  heOffSet067_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_20->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(22);
  heOffSet067_21->Draw("colz");
  heOffSet067_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_21->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(23);
  heOffSet067_22->Draw("colz");
  heOffSet067_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_22->SetAxisRange(0.98,1.0063,"Y");
  c67->cd(24);
  heOffSet067_23->Draw("colz");
  heOffSet067_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet067_23->SetAxisRange(0.98,1.0063,"Y");
  c67->SaveAs("Lumi2D_OffSet-067.png");
  //// 0.47 0.57 0.67 0.87 1;
  ////////////////

TCanvas *h0 = new TCanvas("0","offset = 0",900,900);
heOffSet067_0->Draw("colz");
h0->SetGridx(1);
h0->SetGridy(1);
heOffSet067_0->SetAxisRange(0.98,1.0063,"X");
heOffSet067_0->SetAxisRange(0.98,1.0063,"Y");
h0->SaveAs("../offset/67/0.png");
heOffSet067_0->SetAxisRange(0,1.0063,"X");
heOffSet067_0->SetAxisRange(0,1.0063,"Y");
h0->SaveAs("../offset/67/range/0.png");

TCanvas *h1 = new TCanvas("1","offset = 0",900,900);
h1->SetGridx(1);
h1->SetGridy(1);
heOffSet067_1->Draw("colz");
heOffSet067_1->SetAxisRange(0.98,1.0063,"X");
heOffSet067_1->SetAxisRange(0.98,1.0063,"Y");
h1->SaveAs("../offset/67/1.png");
heOffSet067_1->SetAxisRange(0,1.0063,"X");
heOffSet067_1->SetAxisRange(0,1.0063,"Y");
h1->SaveAs("../offset/67/range/1.png");


TCanvas *h2 = new TCanvas("2","offset = 0",900,900);
h2->SetGridx(1);
h2->SetGridy(1);
heOffSet067_2->Draw("colz");
heOffSet067_2->SetAxisRange(0.98,1.0063,"X");
heOffSet067_2->SetAxisRange(0.98,1.0063,"Y");
h2->SaveAs("../offset/67/2.png");
heOffSet067_2->SetAxisRange(0,1.0063,"X");
heOffSet067_2->SetAxisRange(0,1.0063,"Y");
h2->SaveAs("../offset/67/range/2.png");
 

TCanvas *h3 = new TCanvas("3","offset = 0",900,900);
h3->SetGridx(1);
h3->SetGridy(1);
heOffSet067_3->Draw("colz");
heOffSet067_3->SetAxisRange(0.98,1.0063,"X");
heOffSet067_3->SetAxisRange(0.98,1.0063,"Y");
h3->SaveAs("../offset/67/3.png");
heOffSet067_3->SetAxisRange(0,1.0063,"X");
heOffSet067_3->SetAxisRange(0,1.0063,"Y");
h3->SaveAs("../offset/67/range/3.png");

TCanvas *h4 = new TCanvas("4","offset = 0",900,900);
h4->SetGridx(1);
h4->SetGridy(1);
heOffSet067_4->Draw("colz");
heOffSet067_4->SetAxisRange(0.98,1.0063,"X");
heOffSet067_4->SetAxisRange(0.98,1.0063,"Y");
h4->SaveAs("../offset/67/4.png");
heOffSet067_4->SetAxisRange(0,1.0063,"X");
heOffSet067_4->SetAxisRange(0,1.0063,"Y");
h4->SaveAs("../offset/67/range/4.png");
 

TCanvas *h5 = new TCanvas("5","offset = 0",900,900);
h5->SetGridx(1);
h5->SetGridy(1);
heOffSet067_5->Draw("colz");
heOffSet067_5->SetAxisRange(0.98,1.0063,"X");
heOffSet067_5->SetAxisRange(0.98,1.0063,"Y");
h5->SaveAs("../offset/67/5.png");
heOffSet067_5->SetAxisRange(0,1.0063,"X");
heOffSet067_5->SetAxisRange(0,1.0063,"Y");
h5->SaveAs("../offset/67/range/5.png");




TCanvas *h6 = new TCanvas("6","offset = 0",900,900);
h6->SetGridx(1);
h6->SetGridy(1);
heOffSet067_6->Draw("colz");
heOffSet067_6->SetAxisRange(0.98,1.0063,"X");
heOffSet067_6->SetAxisRange(0.98,1.0063,"Y");
h6->SaveAs("../offset/67/6.png");
heOffSet067_6->SetAxisRange(0,1.0063,"X");
heOffSet067_6->SetAxisRange(0,1.0063,"Y");
h6->SaveAs("../offset/67/range/6.png");



TCanvas *h7 = new TCanvas("7","offset = 0",900,900);
h7->SetGridx(1);
h7->SetGridy(1);
heOffSet067_7->Draw("colz");
heOffSet067_7->SetAxisRange(0.98,1.0063,"X");
heOffSet067_7->SetAxisRange(0.98,1.0063,"Y");
h7->SaveAs("../offset/67/7.png");
heOffSet067_7->SetAxisRange(0,1.0063,"X");
heOffSet067_7->SetAxisRange(0,1.0063,"Y");
h7->SaveAs("../offset/67/range/7.png");

TCanvas *h8 = new TCanvas("8","offset = 0",900,900);
h8->SetGridx(1);
h8->SetGridy(1);
heOffSet067_8->Draw("colz");
heOffSet067_8->SetAxisRange(0.98,1.0063,"X");
heOffSet067_8->SetAxisRange(0.98,1.0063,"Y");
h8->SaveAs("../offset/67/8.png");
heOffSet067_8->SetAxisRange(0,1.0063,"X");
heOffSet067_8->SetAxisRange(0,1.0063,"Y");
h8->SaveAs("../offset/67/range/8.png");

TCanvas *h9 = new TCanvas("9","offset = 0",900,900);
h9->SetGridx(1);
h9->SetGridy(1);
heOffSet067_9->Draw("colz");
heOffSet067_9->SetAxisRange(0.98,1.0063,"X");
heOffSet067_9->SetAxisRange(0.98,1.0063,"Y");
h9->SaveAs("../offset/67/9.png");
heOffSet067_9->SetAxisRange(0,1.0063,"X");
heOffSet067_9->SetAxisRange(0,1.0063,"Y");
h9->SaveAs("../offset/67/range/9.png");


TCanvas *h10 = new TCanvas("10","offset = 0",900,900);
h10->SetGridx(1);
h10->SetGridy(1);
heOffSet067_10->Draw("colz");
heOffSet067_10->SetAxisRange(0.98,1.0063,"X");
heOffSet067_10->SetAxisRange(0.98,1.0063,"Y");
h10->SaveAs("../offset/67/10.png");
heOffSet067_10->SetAxisRange(0,1.0063,"X");
heOffSet067_10->SetAxisRange(0,1.0063,"Y");
h10->SaveAs("../offset/67/range/10.png");

TCanvas *h11 = new TCanvas("11","offset = 0",900,900);
h11->SetGridx(1);
h11->SetGridy(1);
heOffSet067_11->Draw("colz");
heOffSet067_11->SetAxisRange(0.98,1.0063,"X");
heOffSet067_11->SetAxisRange(0.98,1.0063,"Y");
h11->SaveAs("../offset/67/11.png");
heOffSet067_11->SetAxisRange(0,1.0063,"X");
heOffSet067_11->SetAxisRange(0,1.0063,"Y");
h11->SaveAs("../offset/67/range/11.png");

TCanvas *h12 = new TCanvas("12","offset = 0",900,900);
h12->SetGridx(1);
h12->SetGridy(1);
heOffSet067_12->Draw("colz");
heOffSet067_12->SetAxisRange(0.98,1.0063,"X");
heOffSet067_12->SetAxisRange(0.98,1.0063,"Y");
h12->SaveAs("../offset/67/12.png");
heOffSet067_12->SetAxisRange(0,1.0063,"X");
heOffSet067_12->SetAxisRange(0,1.0063,"Y");
h12->SaveAs("../offset/67/range/12.png");

TCanvas *h13 = new TCanvas("13","offset = 0",900,900);
h13->SetGridx(1);
h13->SetGridy(1);
heOffSet067_13->Draw("colz");
heOffSet067_13->SetAxisRange(0.98,1.0063,"X");
heOffSet067_13->SetAxisRange(0.98,1.0063,"Y");
h13->SaveAs("../offset/67/13.png");
heOffSet067_13->SetAxisRange(0,1.0063,"X");
heOffSet067_13->SetAxisRange(0,1.0063,"Y");
h13->SaveAs("../offset/67/range/13.png");

TCanvas *h14 = new TCanvas("14","offset = 0",900,900);
h14->SetGridx(1);
h14->SetGridy(1);
heOffSet067_14->Draw("colz");
heOffSet067_14->SetAxisRange(0.98,1.0063,"X");
heOffSet067_14->SetAxisRange(0.98,1.0063,"Y");
h14->SaveAs("../offset/67/14.png");
heOffSet067_14->SetAxisRange(0,1.0063,"X");
heOffSet067_14->SetAxisRange(0,1.0063,"Y");
h14->SaveAs("../offset/67/range/14.png");

TCanvas *h15 = new TCanvas("15","offset = 0",900,900);
h15->SetGridx(1);
h15->SetGridy(1);
heOffSet067_15->Draw("colz");
heOffSet067_15->SetAxisRange(0.98,1.0063,"X");
heOffSet067_15->SetAxisRange(0.98,1.0063,"Y");
h15->SaveAs("../offset/67/15.png");
heOffSet067_15->SetAxisRange(0,1.0063,"X");
heOffSet067_15->SetAxisRange(0,1.0063,"Y");
h15->SaveAs("../offset/67/range/15.png");

TCanvas *h16 = new TCanvas("16","offset = 0",900,900);
h16->SetGridx(1);
h16->SetGridy(1);
heOffSet067_16->Draw("colz");
heOffSet067_16->SetAxisRange(0.98,1.0063,"X");
heOffSet067_16->SetAxisRange(0.98,1.0063,"Y");
h16->SaveAs("../offset/67/16.png");
heOffSet067_16->SetAxisRange(0,1.0063,"X");
heOffSet067_16->SetAxisRange(0,1.0063,"Y");
h16->SaveAs("../offset/67/range/16.png");



TCanvas *h17 = new TCanvas("17","offset = 0",900,900);
h17->SetGridx(1);
h17->SetGridy(1);
heOffSet067_17->Draw("colz");
heOffSet067_17->SetAxisRange(0.98,1.0063,"X");
heOffSet067_17->SetAxisRange(0.98,1.0063,"Y");
h17->SaveAs("../offset/67/17.png");
heOffSet067_17->SetAxisRange(0,1.0063,"X");
heOffSet067_17->SetAxisRange(0,1.0063,"Y");
h17->SaveAs("../offset/67/range/17.png");


TCanvas *h18 = new TCanvas("18","offset = 0",900,900);
h18->SetGridx(1);
h18->SetGridy(1);
heOffSet067_18->Draw("colz");
heOffSet067_18->SetAxisRange(0.98,1.0063,"X");
heOffSet067_18->SetAxisRange(0.98,1.0063,"Y");
h18->SaveAs("../offset/67/18.png");
heOffSet067_18->SetAxisRange(0,1.0063,"X");
heOffSet067_18->SetAxisRange(0,1.0063,"Y");
h18->SaveAs("../offset/67/range/18.png");

TCanvas *h19 = new TCanvas("19","offset = 0",900,900);
h19->SetGridx(1);
h19->SetGridy(1);
heOffSet067_19->Draw("colz");
heOffSet067_19->SetAxisRange(0.98,1.0063,"X");
heOffSet067_19->SetAxisRange(0.98,1.0063,"Y");
h19->SaveAs("../offset/67/19.png");
heOffSet067_19->SetAxisRange(0,1.0063,"X");
heOffSet067_19->SetAxisRange(0,1.0063,"Y");
h19->SaveAs("../offset/67/range/19.png");


TCanvas *h20 = new TCanvas("20","offset = 0",900,900);
h20->SetGridx(1);
h20->SetGridy(1);
heOffSet067_20->Draw("colz");
heOffSet067_20->SetAxisRange(0.98,1.0063,"X");
heOffSet067_20->SetAxisRange(0.98,1.0063,"Y");
h20->SaveAs("../offset/67/20.png");
heOffSet067_20->SetAxisRange(0,1.0063,"X");
heOffSet067_20->SetAxisRange(0,1.0063,"Y");
h20->SaveAs("../offset/67/range/20.png");
  
TCanvas *h21 = new TCanvas("21","offset = 0",900,900);
h21->SetGridx(1);
h21->SetGridy(1);
heOffSet067_21->Draw("colz");
heOffSet067_21->SetAxisRange(0.98,1.0063,"X");
heOffSet067_21->SetAxisRange(0.98,1.0063,"Y");
h21->SaveAs("../offset/67/21.png");
heOffSet067_21->SetAxisRange(0,1.0063,"X");
heOffSet067_21->SetAxisRange(0,1.0063,"Y");
h21->SaveAs("../offset/67/range/21.png");

TCanvas *h22 = new TCanvas("22","offset = 0",900,900);
h22->SetGridx(1);
h22->SetGridy(1);
heOffSet067_22->Draw("colz");
heOffSet067_22->SetAxisRange(0.98,1.0063,"X");
heOffSet067_22->SetAxisRange(0.98,1.0063,"Y");
h22->SaveAs("../offset/67/22.png");
heOffSet067_22->SetAxisRange(0,1.0063,"X");
heOffSet067_22->SetAxisRange(0,1.0063,"Y");
h22->SaveAs("../offset/67/range/22.png");

TCanvas *h23 = new TCanvas("23","offset = 0",900,900);
h23->SetGridx(1);
h23->SetGridy(1);
heOffSet067_23->Draw("colz");
heOffSet067_23->SetAxisRange(0.98,1.0063,"X");
heOffSet067_23->SetAxisRange(0.98,1.0063,"Y");
h23->SaveAs("../offset/67/23.png");
heOffSet067_23->SetAxisRange(0,1.0063,"X");
heOffSet067_23->SetAxisRange(0,1.0063,"Y");
h23->SaveAs("../offset/67/range/23.png");

  //// 0.47 0.57 0.67 0.87 1;///////////
 //// 0.47 0.57 0.67 0.87 1;
TCanvas *c87 = new TCanvas("E1/Enom : E2/nom 87","E1/E : E2/E offset -DEPENDENCE",4100,2300);
  c87->Divide(6,3);
  c87->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(2);
  heOffSet087_1->Draw("colz");
  heOffSet087_1->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_1->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(3);
  heOffSet087_2->Draw("colz");
  heOffSet087_2->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_2->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(4);
  heOffSet087_3->Draw("colz");
  heOffSet087_3->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_3->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(5);
  heOffSet087_4->Draw("colz");
  heOffSet087_4->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_4->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(6);
  heOffSet087_5->Draw("colz");
  heOffSet087_5->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_5->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(7);
  heOffSet087_6->Draw("colz");
  heOffSet087_6->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_6->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(8);
  heOffSet087_7->Draw("colz");
  heOffSet087_7->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_7->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(9);
  heOffSet087_8->Draw("colz");
  heOffSet087_8->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_8->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(10);
  heOffSet087_9->Draw("colz");
  heOffSet087_9->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_9->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(11);
  heOffSet087_10->Draw("colz");
  heOffSet087_10->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_10->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(12);
  heOffSet087_11->Draw("colz");
  heOffSet087_11->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_11->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(13);
  heOffSet087_12->Draw("colz");
  heOffSet087_12->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_12->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(14);
  heOffSet087_13->Draw("colz");
  heOffSet087_13->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_13->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(15);
  heOffSet087_14->Draw("colz");
  heOffSet087_14->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_14->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(16);
  heOffSet087_15->Draw("colz");
  heOffSet087_15->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_15->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(17);
  heOffSet087_16->Draw("colz");
  heOffSet087_16->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_16->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(18);
  heOffSet087_17->Draw("colz");
  heOffSet087_17->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_17->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(19);
  heOffSet087_18->Draw("colz");
  heOffSet087_18->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_18->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(20);
  heOffSet087_19->Draw("colz");
  heOffSet087_19->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_19->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(21);
  heOffSet087_20->Draw("colz");
  heOffSet087_20->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_20->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(22);
  heOffSet087_21->Draw("colz");
  heOffSet087_21->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_21->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(23);
  heOffSet087_22->Draw("colz");
  heOffSet087_22->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_22->SetAxisRange(0.98,1.0063,"Y");
  c87->cd(24);
  heOffSet087_23->Draw("colz");
  heOffSet087_23->SetAxisRange(0.98,1.0063,"X");
  heOffSet087_23->SetAxisRange(0.98,1.0063,"Y");
  c87->SaveAs("Lumi2D_OffSet-087.png");
  //// 0.47 0.57 0.67 0.87 1;
TCanvas *j0 = new TCanvas("0","offset = 0",900,900);
heOffSet087_0->Draw("colz");
j0->SetGridx(1);
j0->SetGridy(1);
heOffSet087_0->SetAxisRange(0.98,1.0063,"X");
heOffSet087_0->SetAxisRange(0.98,1.0063,"Y");
j0->SaveAs("../offset/87/0.png");
heOffSet087_0->SetAxisRange(0,1.0063,"X");
heOffSet087_0->SetAxisRange(0,1.0063,"Y");
j0->SaveAs("../offset/87/range/0.png");

TCanvas *j1 = new TCanvas("1","offset = 0",900,900);
j1->SetGridx(1);
j1->SetGridy(1);
heOffSet087_1->Draw("colz");
heOffSet087_1->SetAxisRange(0.98,1.0063,"X");
heOffSet087_1->SetAxisRange(0.98,1.0063,"Y");
j1->SaveAs("../offset/87/1.png");
heOffSet087_1->SetAxisRange(0,1.0063,"X");
heOffSet087_1->SetAxisRange(0,1.0063,"Y");
j1->SaveAs("../offset/87/range/1.png");


TCanvas *j2 = new TCanvas("2","offset = 0",900,900);
j2->SetGridx(1);
j2->SetGridy(1);
heOffSet087_2->Draw("colz");
heOffSet087_2->SetAxisRange(0.98,1.0063,"X");
heOffSet087_2->SetAxisRange(0.98,1.0063,"Y");
j2->SaveAs("../offset/87/2.png");
heOffSet087_2->SetAxisRange(0,1.0063,"X");
heOffSet087_2->SetAxisRange(0,1.0063,"Y");
j2->SaveAs("../offset/87/range/2.png");
 

TCanvas *j3 = new TCanvas("3","offset = 0",900,900);
j3->SetGridx(1);
j3->SetGridy(1);
heOffSet087_3->Draw("colz");
heOffSet087_3->SetAxisRange(0.98,1.0063,"X");
heOffSet087_3->SetAxisRange(0.98,1.0063,"Y");
j3->SaveAs("../offset/87/3.png");
heOffSet087_3->SetAxisRange(0,1.0063,"X");
heOffSet087_3->SetAxisRange(0,1.0063,"Y");
j3->SaveAs("../offset/87/range/3.png");

TCanvas *j4 = new TCanvas("4","offset = 0",900,900);
j4->SetGridx(1);
j4->SetGridy(1);
heOffSet087_4->Draw("colz");
heOffSet087_4->SetAxisRange(0.98,1.0063,"X");
heOffSet087_4->SetAxisRange(0.98,1.0063,"Y");
j4->SaveAs("../offset/87/4.png");
heOffSet087_4->SetAxisRange(0,1.0063,"X");
heOffSet087_4->SetAxisRange(0,1.0063,"Y");
j4->SaveAs("../offset/87/range/4.png");
 

TCanvas *j5 = new TCanvas("5","offset = 0",900,900);
j5->SetGridx(1);
j5->SetGridy(1);
heOffSet087_5->Draw("colz");
heOffSet087_5->SetAxisRange(0.98,1.0063,"X");
heOffSet087_5->SetAxisRange(0.98,1.0063,"Y");
j5->SaveAs("../offset/87/5.png");
heOffSet087_5->SetAxisRange(0,1.0063,"X");
heOffSet087_5->SetAxisRange(0,1.0063,"Y");
j5->SaveAs("../offset/87/range/5.png");




TCanvas *j6 = new TCanvas("6","offset = 0",900,900);
j6->SetGridx(1);
j6->SetGridy(1);
heOffSet087_6->Draw("colz");
heOffSet087_6->SetAxisRange(0.98,1.0063,"X");
heOffSet087_6->SetAxisRange(0.98,1.0063,"Y");
j6->SaveAs("../offset/87/6.png");
heOffSet087_6->SetAxisRange(0,1.0063,"X");
heOffSet087_6->SetAxisRange(0,1.0063,"Y");
j6->SaveAs("../offset/87/range/6.png");



TCanvas *j7 = new TCanvas("7","offset = 0",900,900);
j7->SetGridx(1);
j7->SetGridy(1);
heOffSet087_7->Draw("colz");
heOffSet087_7->SetAxisRange(0.98,1.0063,"X");
heOffSet087_7->SetAxisRange(0.98,1.0063,"Y");
j7->SaveAs("../offset/87/7.png");
heOffSet087_7->SetAxisRange(0,1.0063,"X");
heOffSet087_7->SetAxisRange(0,1.0063,"Y");
j7->SaveAs("../offset/87/range/7.png");

TCanvas *j8 = new TCanvas("8","offset = 0",900,900);
j8->SetGridx(1);
j8->SetGridy(1);
heOffSet087_8->Draw("colz");
heOffSet087_8->SetAxisRange(0.98,1.0063,"X");
heOffSet087_8->SetAxisRange(0.98,1.0063,"Y");
j8->SaveAs("../offset/87/8.png");
heOffSet087_8->SetAxisRange(0,1.0063,"X");
heOffSet087_8->SetAxisRange(0,1.0063,"Y");
j8->SaveAs("../offset/87/range/8.png");

TCanvas *j9 = new TCanvas("9","offset = 0",900,900);
j9->SetGridx(1);
j9->SetGridy(1);
heOffSet087_9->Draw("colz");
heOffSet087_9->SetAxisRange(0.98,1.0063,"X");
heOffSet087_9->SetAxisRange(0.98,1.0063,"Y");
j9->SaveAs("../offset/87/9.png");
heOffSet087_9->SetAxisRange(0,1.0063,"X");
heOffSet087_9->SetAxisRange(0,1.0063,"Y");
j9->SaveAs("../offset/87/range/9.png");


TCanvas *j10 = new TCanvas("10","offset = 0",900,900);
j10->SetGridx(1);
j10->SetGridy(1);
heOffSet087_10->Draw("colz");
heOffSet087_10->SetAxisRange(0.98,1.0063,"X");
heOffSet087_10->SetAxisRange(0.98,1.0063,"Y");
j10->SaveAs("../offset/87/10.png");
heOffSet087_10->SetAxisRange(0,1.0063,"X");
heOffSet087_10->SetAxisRange(0,1.0063,"Y");
j10->SaveAs("../offset/87/range/10.png");

TCanvas *j11 = new TCanvas("11","offset = 0",900,900);
j11->SetGridx(1);
j11->SetGridy(1);
heOffSet087_11->Draw("colz");
heOffSet087_11->SetAxisRange(0.98,1.0063,"X");
heOffSet087_11->SetAxisRange(0.98,1.0063,"Y");
j11->SaveAs("../offset/87/11.png");
heOffSet087_11->SetAxisRange(0,1.0063,"X");
heOffSet087_11->SetAxisRange(0,1.0063,"Y");
j11->SaveAs("../offset/87/range/11.png");

TCanvas *j12 = new TCanvas("12","offset = 0",900,900);
j12->SetGridx(1);
j12->SetGridy(1);
heOffSet087_12->Draw("colz");
heOffSet087_12->SetAxisRange(0.98,1.0063,"X");
heOffSet087_12->SetAxisRange(0.98,1.0063,"Y");
j12->SaveAs("../offset/87/12.png");
heOffSet087_12->SetAxisRange(0,1.0063,"X");
heOffSet087_12->SetAxisRange(0,1.0063,"Y");
j12->SaveAs("../offset/87/range/12.png");

TCanvas *j13 = new TCanvas("13","offset = 0",900,900);
j13->SetGridx(1);
j13->SetGridy(1);
heOffSet087_13->Draw("colz");
heOffSet087_13->SetAxisRange(0.98,1.0063,"X");
heOffSet087_13->SetAxisRange(0.98,1.0063,"Y");
j13->SaveAs("../offset/87/13.png");
heOffSet087_13->SetAxisRange(0,1.0063,"X");
heOffSet087_13->SetAxisRange(0,1.0063,"Y");
j13->SaveAs("../offset/87/range/13.png");

TCanvas *j14 = new TCanvas("14","offset = 0",900,900);
j14->SetGridx(1);
j14->SetGridy(1);
heOffSet087_14->Draw("colz");
heOffSet087_14->SetAxisRange(0.98,1.0063,"X");
heOffSet087_14->SetAxisRange(0.98,1.0063,"Y");
j14->SaveAs("../offset/87/14.png");
heOffSet087_14->SetAxisRange(0,1.0063,"X");
heOffSet087_14->SetAxisRange(0,1.0063,"Y");
j14->SaveAs("../offset/87/range/14.png");

TCanvas *j15 = new TCanvas("15","offset = 0",900,900);
j15->SetGridx(1);
j15->SetGridy(1);
heOffSet087_15->Draw("colz");
heOffSet087_15->SetAxisRange(0.98,1.0063,"X");
heOffSet087_15->SetAxisRange(0.98,1.0063,"Y");
j15->SaveAs("../offset/87/15.png");
heOffSet087_15->SetAxisRange(0,1.0063,"X");
heOffSet087_15->SetAxisRange(0,1.0063,"Y");
j15->SaveAs("../offset/87/range/15.png");

TCanvas *j16 = new TCanvas("16","offset = 0",900,900);
j16->SetGridx(1);
j16->SetGridy(1);
heOffSet087_16->Draw("colz");
heOffSet087_16->SetAxisRange(0.98,1.0063,"X");
heOffSet087_16->SetAxisRange(0.98,1.0063,"Y");
j16->SaveAs("../offset/87/16.png");
heOffSet087_16->SetAxisRange(0,1.0063,"X");
heOffSet087_16->SetAxisRange(0,1.0063,"Y");
j16->SaveAs("../offset/87/range/16.png");



TCanvas *j17 = new TCanvas("17","offset = 0",900,900);
j17->SetGridx(1);
j17->SetGridy(1);
heOffSet087_17->Draw("colz");
heOffSet087_17->SetAxisRange(0.98,1.0063,"X");
heOffSet087_17->SetAxisRange(0.98,1.0063,"Y");
j17->SaveAs("../offset/87/17.png");
heOffSet087_17->SetAxisRange(0,1.0063,"X");
heOffSet087_17->SetAxisRange(0,1.0063,"Y");
j17->SaveAs("../offset/87/range/17.png");


TCanvas *j18 = new TCanvas("18","offset = 0",900,900);
j18->SetGridx(1);
j18->SetGridy(1);
heOffSet087_18->Draw("colz");
heOffSet087_18->SetAxisRange(0.98,1.0063,"X");
heOffSet087_18->SetAxisRange(0.98,1.0063,"Y");
j18->SaveAs("../offset/87/18.png");
heOffSet087_18->SetAxisRange(0,1.0063,"X");
heOffSet087_18->SetAxisRange(0,1.0063,"Y");
j18->SaveAs("../offset/87/range/18.png");

TCanvas *j19 = new TCanvas("19","offset = 0",900,900);
j19->SetGridx(1);
j19->SetGridy(1);
heOffSet087_19->Draw("colz");
heOffSet087_19->SetAxisRange(0.98,1.0063,"X");
heOffSet087_19->SetAxisRange(0.98,1.0063,"Y");
j19->SaveAs("../offset/87/19.png");
heOffSet087_19->SetAxisRange(0,1.0063,"X");
heOffSet087_19->SetAxisRange(0,1.0063,"Y");
j19->SaveAs("../offset/87/range/19.png");


TCanvas *j20 = new TCanvas("20","offset = 0",900,900);
j20->SetGridx(1);
j20->SetGridy(1);
heOffSet087_20->Draw("colz");
heOffSet087_20->SetAxisRange(0.98,1.0063,"X");
heOffSet087_20->SetAxisRange(0.98,1.0063,"Y");
j20->SaveAs("../offset/87/20.png");
heOffSet087_20->SetAxisRange(0,1.0063,"X");
heOffSet087_20->SetAxisRange(0,1.0063,"Y");
j20->SaveAs("../offset/87/range/20.png");
  
TCanvas *j21 = new TCanvas("21","offset = 0",900,900);
j21->SetGridx(1);
j21->SetGridy(1);
heOffSet087_21->Draw("colz");
heOffSet087_21->SetAxisRange(0.98,1.0063,"X");
heOffSet087_21->SetAxisRange(0.98,1.0063,"Y");
j21->SaveAs("../offset/87/21.png");
heOffSet087_21->SetAxisRange(0,1.0063,"X");
heOffSet087_21->SetAxisRange(0,1.0063,"Y");
j21->SaveAs("../offset/87/range/21.png");

TCanvas *j22 = new TCanvas("22","offset = 0",900,900);
j22->SetGridx(1);
j22->SetGridy(1);
heOffSet087_22->Draw("colz");
heOffSet087_22->SetAxisRange(0.98,1.0063,"X");
heOffSet087_22->SetAxisRange(0.98,1.0063,"Y");
j22->SaveAs("../offset/87/22.png");
heOffSet087_22->SetAxisRange(0,1.0063,"X");
heOffSet087_22->SetAxisRange(0,1.0063,"Y");
j22->SaveAs("../offset/87/range/22.png");

TCanvas *j23 = new TCanvas("23","offset = 0",900,900);
j23->SetGridx(1);
j23->SetGridy(1);
heOffSet087_23->Draw("colz");
heOffSet087_23->SetAxisRange(0.98,1.0063,"X");
heOffSet087_23->SetAxisRange(0.98,1.0063,"Y");
j23->SaveAs("../offset/87/23.png");
heOffSet087_23->SetAxisRange(0,1.0063,"X");
heOffSet087_23->SetAxisRange(0,1.0063,"Y");
j23->SaveAs("../offset/87/range/23.png");

  //// 0.47 0.57 0.67 0.87 1;
TCanvas *l47_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet047_1->Draw("same");
  hOffSet047_1->SetLineColor(kRed);
  hOffSet047_5->Draw("same");
  hOffSet047_5->SetLineColor(kRed+2);
  hOffSet047_11->Draw("same");
  hOffSet047_11->SetLineColor(kBlue);
  hOffSet047_17->Draw("same");
  hOffSet047_17->SetLineColor(kBlue+2);
  hOffSet047_23->Draw("same");
  hOffSet047_23->SetLineColor(kGreen);
  TLegend* legC47 = new TLegend(0.8,0.09,0.98,0.9);  
  legC47->SetHeader("N_PART dependence}");
  legC47->SetTextSize(0.02);// set size of text
  // legC47->SetNColumns(2);  
  legC47->SetFillColor(0);// Have a white background 0.193 0.386 0.579 0.965 1.158
  legC47->AddEntry(hOffSet047_1, "Dft OffSet=0.001", "l");
  legC47->AddEntry(hOffSet047_5, "Dft OffSet=0.09", "l");
  legC47->AddEntry(hOffSet047_11, "Dft OffSet=0.21", "l");
  legC47->AddEntry(hOffSet047_16, "Dft OffSet=0.33", "l");
  legC47->AddEntry(hOffSet047_23, "Dft OffSet=0.47", "l");
  legC47->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC47->Draw();
  l47_1->SetGridx(1);
  l47_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l47_1->SaveAs("L_distribution_OffSet47.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l47_1->SaveAs("L_distribution_OffSet47_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l47_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l47_1->SaveAs("L_distribution_OffSet47_Logy.png");


///////////////////////////////////////////
  //// 0.47 0.57 0.67 0.87 1;
TCanvas *l57_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet057_1->Draw("same");
  hOffSet057_1->SetLineColor(kRed);
  hOffSet057_5->Draw("same");
  hOffSet057_5->SetLineColor(kRed+2);
  hOffSet057_11->Draw("same");
  hOffSet057_11->SetLineColor(kBlue);
  hOffSet057_17->Draw("same");
  hOffSet057_17->SetLineColor(kBlue+2);
  hOffSet057_23->Draw("same");
  hOffSet057_23->SetLineColor(kGreen);
  TLegend* legC57 = new TLegend(0.8,0.09,0.98,0.9);  
  legC57->SetHeader("N_PART dependence}");
  legC57->SetTextSize(0.02);// set size of text
  // legC57->SetNColumns(2);  
  legC57->SetFillColor(0);// Have a white background 0.193 0.386 0.579 0.965 1.158
  legC57->AddEntry(hOffSet057_1, "Dft OffSet=0.001", "l");
  legC57->AddEntry(hOffSet057_5, "Dft OffSet=0.09", "l");
  legC57->AddEntry(hOffSet057_11, "Dft OffSet=0.21", "l");
  legC57->AddEntry(hOffSet057_16, "Dft OffSet=0.33", "l");
  legC57->AddEntry(hOffSet057_23, "Dft OffSet=0.47", "l");
  legC57->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC57->Draw();
  l57_1->SetGridx(1);
  l57_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l57_1->SaveAs("L_distribution_OffSet57.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l57_1->SaveAs("L_distribution_OffSet57_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l57_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l57_1->SaveAs("L_distribution_OffSet57_Logy.png");

////////////////////////////////////////////
  //// 0.47 0.67 0.67 0.87 1;
TCanvas *l67_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet067_1->Draw("same");
  hOffSet067_1->SetLineColor(kRed);
  hOffSet067_5->Draw("same");
  hOffSet067_5->SetLineColor(kRed+2);
  hOffSet067_11->Draw("same");
  hOffSet067_11->SetLineColor(kBlue);
  hOffSet067_17->Draw("same");
  hOffSet067_17->SetLineColor(kBlue+2);
  hOffSet067_23->Draw("same");
  hOffSet067_23->SetLineColor(kGreen);
  TLegend* legC67 = new TLegend(0.8,0.09,0.98,0.9);  
  legC67->SetHeader("N_PART dependence}");
  legC67->SetTextSize(0.02);// set size of text
  // legC67->SetNColumns(2);  
  legC67->SetFillColor(0);// Have a white background 0.193 0.386 0.679 0.965 1.158
  legC67->AddEntry(hOffSet067_1, "Dft OffSet=0.001", "l");
  legC67->AddEntry(hOffSet067_5, "Dft OffSet=0.09", "l");
  legC67->AddEntry(hOffSet067_11, "Dft OffSet=0.21", "l");
  legC67->AddEntry(hOffSet067_16, "Dft OffSet=0.33", "l");
  legC67->AddEntry(hOffSet067_23, "Dft OffSet=0.47", "l");
  legC67->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC67->Draw();
  l67_1->SetGridx(1);
  l67_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l67_1->SaveAs("L_distribution_OffSet67.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l67_1->SaveAs("L_distribution_OffSet67_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l67_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l67_1->SaveAs("L_distribution_OffSet67_Logy.png");
/////////////////////////////////////////


TCanvas *l87_1 = new TCanvas("L distr PARTICLE DEPENDENCE ","L distribution depends on AMOUNT OF PARTICLES");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hOffSet087_1->Draw("same");
  hOffSet087_1->SetLineColor(kRed);
  hOffSet087_5->Draw("same");
  hOffSet087_5->SetLineColor(kRed+2);
  hOffSet087_11->Draw("same");
  hOffSet087_11->SetLineColor(kBlue);
  hOffSet087_17->Draw("same");
  hOffSet087_17->SetLineColor(kBlue+2);
  hOffSet087_23->Draw("same");
  hOffSet087_23->SetLineColor(kGreen);
  TLegend* legC87 = new TLegend(0.8,0.09,0.98,0.9);  
  legC87->SetHeader("N_PART dependence}");
  legC87->SetTextSize(0.02);// set size of text
  // legC87->SetNColumns(2);  
  legC87->SetFillColor(0);// Have a white background 0.193 0.386 0.879 0.965 1.158
  legC87->AddEntry(hOffSet087_1, "Dft OffSet=0.001", "l");
  legC87->AddEntry(hOffSet087_5, "Dft OffSet=0.09", "l");
  legC87->AddEntry(hOffSet087_11, "Dft OffSet=0.21", "l");
  legC87->AddEntry(hOffSet087_16, "Dft OffSet=0.33", "l");
  legC87->AddEntry(hOffSet087_23, "Dft OffSet=0.47", "l");
  legC87->AddEntry(h3000g, "Dft OffSet=0.0", "l");
  legC87->Draw();
  l87_1->SetGridx(1);
  l87_1->SetGridy(1);
  h3000g->SetAxisRange(1,5e+33,"Y");
  h3000g->SetAxisRange(0,1.06,"X");
  l87_1->SaveAs("L_distribution_OffSet87.png");
  h3000g->SetAxisRange(0.9,1.06,"X");  
  l87_1->SaveAs("L_distribution_OffSet87_zoom.png");
  h3000g->SetAxisRange(1e+30,5e+33,"Y");
  l87_1->SetLogy(1);
  h3000g->SetAxisRange(0,1.06,"X");
  l87_1->SaveAs("L_distribution_OffSet87_Logy.png");

  //// 0.47 0.57 0.67 0.87 1;
return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////

void FillHistogramsFromTree(TString filename, TH1D* h1D, TH2D* h2D, double nominalCMSEnergy, double lumi, double lumi1) {

  //Open File
  TFile *file = TFile::Open(filename);  //Get the Tree
  TTree *tree1;
  file->GetObject("lumiTree",tree1);

  //Define variabels for Tree
  Double_t E1, E2;
  tree1->SetBranchAddress("E1", &E1);
  tree1->SetBranchAddress("E2", &E2);

 
  //Loop over the tree
  int n_entries =  tree1->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree1->GetEntry(i);
    h1D->Fill(2*sqrt(E1*E2)/nominalCMSEnergy);
    h2D->Fill(E1/(nominalCMSEnergy/2),E2/(nominalCMSEnergy/2));
  }
 
  Double_t scalingFactor = lumi/(h1D->Integral()); 
  Double_t scalingFactor1 = lumi1/(h2D->Integral());
  h1D->Scale(scalingFactor);
  h2D->Scale(scalingFactor1);
 
  std::cout << "1Dhisto         " << h1D->Integral() << "               " << h1D->GetEntries() << std::endl;
  std::cout << "2Dhisto         " << h2D->Integral() << "               " << h2D->GetEntries() << std::endl;

  file->Close();
  delete file;
  return;
}



