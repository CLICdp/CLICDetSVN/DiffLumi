#!/bin/bash

ENERGY=1500
N_PART=0.372
BETA_X=8.0
BETA_Y=0.068
EMIT_X=0.68
EMIT_Y=0.02
offset=0


./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y $offset  DEFAULT_VALUES


 for ENERGY in 1450; do  
 for BETA_Y in 0.044 0.055 0.068 0.077 0.099 0.1 0.3; do
      ./RunGPandCreateRootFiles.sh $ENERGY $N_PART $BETA_X $BETA_Y $EMIT_X $EMIT_Y  $offset $FILENAME ${ENERGY}-${BETA_Y}
 done

 done

