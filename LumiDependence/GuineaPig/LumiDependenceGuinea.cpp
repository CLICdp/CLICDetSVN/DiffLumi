#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>

#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <cmath>
#include <iostream> 

void FillHistogramsFromTree(TString filename, TH1D* h1D, TH2D* h2D, double nominalCMSEnergy=3000.0, double lumi=1.0, double lumi=1.0);

int main() {

  gStyle->SetOptStat(0);


   //Create Histogram
  Int_t bins=200;
  Double_t xLimit =2.5;
  // THE DEFAULT
  TH1D *h3000g = new TH1D("h3000g","Luminosity dependence 3 TeV ",bins,0, xLimit);
  TH2D *heE3000g = new TH2D("heE3000g","Default Luminosity at 3000 GeV",bins,0,1.1,bins,0,1.1); 

  //for variation of "EMIT_Y" 0.008 0.014 0.026 0.032 0.038
  TH1D *hEMIT_Y1 = new TH1D("hEMIT_Y1","EMIT_Y= 0.008",bins,0, xLimit);
  TH1D *hEMIT_Y2 = new TH1D("hEMIT_Y2","EMIT_Y= 0.014" ,bins,0, xLimit);
  TH1D *hEMIT_Y3 = new TH1D("hEMIT_Y3","EMIT_Y= 0.026" ,bins,0, xLimit);
  TH1D *hEMIT_Y4 = new TH1D("hEMIT_Y4","EMIT_Y= 0.032"  ,bins,0, xLimit);
  TH1D *hEMIT_Y5 = new TH1D("hEMIT_Y5","EMIT_Y= 0.038"  ,bins,0, xLimit);

 
  TH2D *heEMIT_Y1 = new TH2D("heEMIT_Y1","EMIT_Y= 0.008 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_Y2 = new TH2D("heEMIT_Y2","EMIT_Y= 0.014 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_Y3 = new TH2D("heEMIT_Y3","EMIT_Y= 0.026 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_Y4 = new TH2D("heEMIT_Y4","EMIT_Y= 0.32 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_Y5 = new TH2D("heEMIT_Y5","EMIT_Y= 0.38 at 3000 GeV",bins,0,1.1,bins,0,1.1); 



  /// EMIT_X in 0.08 0.28 0.48 0.88 1.08;
   //for variation of "EMIT_X" 
  TH1D *hEMIT_X1 = new TH1D("hEMIT_X1","EMIT_X= 0.08",bins,0, xLimit);
  TH1D *hEMIT_X2 = new TH1D("hEMIT_X2","EMIT_X= 0.28",bins,0, xLimit);
  TH1D *hEMIT_X3 = new TH1D("hEMIT_X3","EMIT_X= 0.48",bins,0, xLimit);
  TH1D *hEMIT_X4 = new TH1D("hEMIT_X4","EMIT_X= 0.88",bins,0, xLimit);
  TH1D *hEMIT_X5 = new TH1D("hEMIT_X5","EMIT_X= 1.08",bins,0, xLimit);


  TH2D *heEMIT_X1 = new TH2D("heEMIT_X1","EMIT_X= 0.08 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_X2 = new TH2D("heEMIT_X2","EMIT_X= 0.28 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_X3 = new TH2D("heEMIT_X3","EMIT_X= 0.48 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_X4 = new TH2D("heEMIT_X4","EMIT_X= 0.88 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heEMIT_X5 = new TH2D("heEMIT_X5","EMIT_X= 1.08 at 3000 GeV",bins,0,1.1,bins,0,1.1); 

   //for variation of "BETA_X" =  0.8 3.2 5.6 10.4 14.8
  TH1D *hBETA_X1 = new TH1D("hBETA_X1","BETA_X= 0.8",bins,0, xLimit);
  TH1D *hBETA_X2 = new TH1D("hBETA_X2","BETA_X= 3.2",bins,0, xLimit);
  TH1D *hBETA_X3 = new TH1D("hBETA_X3","BETA_X= 5.6",bins,0, xLimit);
  TH1D *hBETA_X4 = new TH1D("hBETA_X4","BETA_X= 10.4",bins,0, xLimit);
  TH1D *hBETA_X5 = new TH1D("hBETA_X5","BETA_X= 14.8",bins,0, xLimit);
   
  TH2D *heBETA_X1 = new TH2D("heBETA_X1","BETA_X= 0.08 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_X2 = new TH2D("heBETA_X2","BETA_X= 3.2 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_X3 = new TH2D("heBETA_X3","BETA_X= 5.6 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_X4 = new TH2D("heBETA_X4","BETA_X= 10.4 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_X5 = new TH2D("heBETA_X5","BETA_X= 14.8 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
   

  //for variation of "BETA_Y" =   0.015 0.06 0.105 0.195 0.24;
  TH1D *hBETA_Y1 = new TH1D("hBETA_Y1","BETA_Y= 0.015",bins,0, xLimit);
  TH1D *hBETA_Y2 = new TH1D("hBETA_Y2","BETA_Y= 0.06",bins,0, xLimit);
  TH1D *hBETA_Y3 = new TH1D("hBETA_Y3","BETA_Y= 0.105",bins,0, xLimit);
  TH1D *hBETA_Y4 = new TH1D("hBETA_Y4","BETA_Y= 0.195",bins,0, xLimit);
  TH1D *hBETA_Y5 = new TH1D("hBETA_Y5","BETA_Y= 0.24",bins,0, xLimit);
  TH2D *heBETA_Y1 = new TH2D("heBETA_Y1","BETA_Y= 0.015 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_Y2 = new TH2D("heBETA_Y2","BETA_Y= 0.06 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_Y3 = new TH2D("heBETA_Y3","BETA_Y= 0.105 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_Y4 = new TH2D("heBETA_Y4","BETA_Y= 0.195 at 3000 GeV",bins,0,1.1,bins,0,1.1); 
  TH2D *heBETA_Y5 = new TH2D("heBETA_Y5","BETA_Y= 0.24 at 3000 GeV",bins,0,1.1,bins,0,1.1); 


  FillHistogramsFromTree("../GuineaPig/EMIT_X_0.08offRun.root",hEMIT_X1,heEMIT_X1,3000,1.76616e+35,1.76616e+35);
  FillHistogramsFromTree("../GuineaPig/EMIT_X_0.28offRun.root",hEMIT_X2,heEMIT_X2,3000,8.70944e+34,8.70944e+34);
  FillHistogramsFromTree("../GuineaPig/EMIT_X_0.48offRun.root",hEMIT_X3,heEMIT_X3,3000,6.003e+34,6.003e+34);
  FillHistogramsFromTree("../GuineaPig/EMIT_X_0.88offRun.root",hEMIT_X4,heEMIT_X4,3000,4.06439e+34,4.06439e+34);
  FillHistogramsFromTree("../GuineaPig/EMIT_X_1.08offRun.root",hEMIT_X5,heEMIT_X5,3000,3.55843e+34,3.55843e+34);
 FillHistogramsFromTree("../GuineaPig/EMIT_Y_0.008offRun.root",hEMIT_Y1,heEMIT_Y1,3000,7.90859e+34,7.90859e+34);
FillHistogramsFromTree("../GuineaPig/EMIT_Y_0.014offRun.root",hEMIT_Y2,heEMIT_Y2, 3000,5.80917e+34,5.80917e+34);
FillHistogramsFromTree("../GuineaPig/EMIT_Y_0.026offRun.root",hEMIT_Y3,heEMIT_Y3, 3000,4.12175e+34,4.12175e+34);
 FillHistogramsFromTree("../GuineaPig/EMIT_Y_0.032offRun.root",hEMIT_Y4,heEMIT_Y4,3000,3.65004e+34,3.65004e+34);
 FillHistogramsFromTree("../GuineaPig/EMIT_Y_0.038offRun.root",hEMIT_Y5,heEMIT_Y5,3000,3.31141e+34,3.31141e+34);

 
 FillHistogramsFromTree("../GuineaPig/DEFAULT_VALUESoffRun.root",h3000g, heE3000g,3000,4.77558e+34,4.77558e+34);
 
//BETA FUNCTIONS  0.8 3.2 5.6 10.4 14. 8; do 

    FillHistogramsFromTree("../GuineaPig/BETA_X0.8offRun.root", hBETA_X1,heBETA_X1,3000, 1.70503e+35,1.70503e+35);
    FillHistogramsFromTree("../GuineaPig/BETA_X10.4offRun.root", hBETA_X2,heBETA_X2,3000,4.03455e+34,4.03455e+34);
    FillHistogramsFromTree("../GuineaPig/BETA_X14.8offRun.root", hBETA_X3,heBETA_X3,3000,3.22883e+34,3.22883e+34);
    FillHistogramsFromTree("../GuineaPig/BETA_X3.2offRun.root", hBETA_X4,heBETA_X4,3000, 8.94204e+34,8.94204e+34);
    FillHistogramsFromTree("../GuineaPig/BETA_X5.6offRun.root", hBETA_X5,heBETA_X5,3000, 6.04925e+34,6.04925e+34);
   FillHistogramsFromTree("../GuineaPig/BETA_Y_0.015offRun.root",hBETA_Y1,heBETA_Y1,3000,4.12272e+34,4.12272e+34);
   FillHistogramsFromTree("../GuineaPig/BETA_Y_0.06offRun.root",hBETA_Y2,heBETA_Y2,3000, 4.85994e+34,4.85994e+34);
   FillHistogramsFromTree("../GuineaPig/BETA_Y_0.105offRun.root",hBETA_Y3,heBETA_Y3,3000,4.32067e+34,4.32067e+34);
   FillHistogramsFromTree("../GuineaPig/BETA_Y_0.195offRun.root",hBETA_Y4,heBETA_Y4,3000,3.46012e+34,3.46012e+34);
   FillHistogramsFromTree("../GuineaPig/BETA_Y_0.24offRun.root",hBETA_Y5,heBETA_Y5,3000, 3.17663e+34,3.17663e+34);
 

   /// Emittance dependence
  TCanvas *c = new TCanvas("L distr","L distribution depends on Y-emmitance");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
 h3000g->Draw();
 h3000g->SetAxisRange(0,100,"X");
  c->SaveAs("0_distribution.png");

  h3000g->SetLineColor(kBlack);
  hEMIT_Y1->Draw("same");
  hEMIT_Y1->SetLineColor(kRed);
  hEMIT_Y2->Draw("same");
  hEMIT_Y2->SetLineColor(kRed+2);
  hEMIT_Y3->Draw("same");
  hEMIT_Y3->SetLineColor(kBlue);
  hEMIT_Y4->Draw("same");
  hEMIT_Y4->SetLineColor(kBlue+2);
  hEMIT_Y5->Draw("same");
  hEMIT_Y5->SetLineColor(kGreen);
  TLegend* legC = new TLegend(0.8,0.09,0.98,0.9); 
  legC->SetHeader("EMIT_Y");
  legC->SetTextSize(0.04);
  legC->SetFillColor(0);
  legC->AddEntry(hEMIT_Y1, "0.008", "l");
  legC->AddEntry(hEMIT_Y2, "0.014", "l");
  legC->AddEntry(hEMIT_Y3, "0.026", "l");
  legC->AddEntry(hEMIT_Y4, "0.032", "l");
  legC->AddEntry(hEMIT_Y5, "0.038", "l");
  legC->AddEntry(h3000g, "Dft=0.02", "l");
  legC->Draw();
  c->SetGridx(1);
  c->SetGridy(1);
  h3000g->SetAxisRange(0,2e+33,"Y");
  h3000g->SetAxisRange(0,2.542006,"X");
  c->SaveAs("L_distribution_EMIT_Y.png");
  h3000g->SetAxisRange(0.99,1.006,"X");  
  c->SaveAs("L_distribution_EMIT_Y_zoom.png");
  h3000g->SetAxisRange(1e+20,4e+33,"Y");
  c->SetLogy(1);//  c->SetLogy(0);
  h3000g->SetAxisRange(0,2.542006,"X");
  c->SaveAs("L_distribution_EMIT_Y_Logy.png");


 
  TCanvas *c6 = new TCanvas("E1/Enom : E2/nom to Energies","E1/E : E2/E to Energies -Y EMITTANCE DEPENDENCE",2700,1300);
  c6->Divide(3,2);
  c6->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");

  c6->cd(2);
  heEMIT_Y1->Draw("colz");
  heEMIT_Y1->SetAxisRange(0.98,1.0063,"X");
  heEMIT_Y1->SetAxisRange(0.98,1.0063,"Y");
    

  c6->cd(3);
  heEMIT_Y2->Draw("colz");
  heEMIT_Y2->SetAxisRange(0.98,1.0063,"X");
  heEMIT_Y2->SetAxisRange(0.98,1.0063,"Y");
  
  c6->cd(4);
  heEMIT_Y3->Draw("colz");
  heEMIT_Y3->SetAxisRange(0.98,1.0063,"X");
  heEMIT_Y3->SetAxisRange(0.98,1.0063,"Y");

  c6->cd(5);
  heEMIT_Y4->Draw("colz");
  heEMIT_Y4->SetAxisRange(0.98,1.0063,"X");
  heEMIT_Y4->SetAxisRange(0.98,1.0063,"Y");
  c6->cd(6);
  heEMIT_Y5->Draw("colz");
  heEMIT_Y5->SetAxisRange(0.98,1.0063,"X");
  heEMIT_Y5->SetAxisRange(0.98,1.0063,"Y");
  c6->SaveAs("Lumi2D_EMIT_Y.png");



  TCanvas *c1 = new TCanvas("L distr EMIT_X","L distribution depends on X-emmitance");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hEMIT_X1->Draw("same");
  hEMIT_X1->SetLineColor(kRed);
  hEMIT_X2->Draw("same");
  hEMIT_X2->SetLineColor(kRed+2);
  hEMIT_X3->Draw("same");
  hEMIT_X3->SetLineColor(kBlue);
  hEMIT_X4->Draw("same");
  hEMIT_X4->SetLineColor(kBlue+2);
  hEMIT_X5->Draw("same");
  hEMIT_X5->SetLineColor(kGreen);
 
  TLegend* legC1 = new TLegend(0.8,0.09,0.98,0.9);  
  legC1->SetHeader("EMIT_X dependence}");
  //  legC1->SetNColumns(2);
  legC1->SetTextSize(0.04);// set size of text
  legC1->SetFillColor(0);// Have a white background0,2e+33,"Y"
  legC1->AddEntry(hEMIT_X1, "0.001", "l");
  legC1->AddEntry(hEMIT_X2, "0.05", "l");
  legC1->AddEntry(hEMIT_X3, "0.01", "l");
  legC1->AddEntry(hEMIT_X4, "0.1", "l");
  legC1->AddEntry(hEMIT_X5, "1.0", "l");
  // legC1->AddEntry(hEMIT_X6, "EMIT_X=1.5", "l");
  // legC1->AddEntry(hEMIT_X7, "EMIT_X=2.0", "l");
  legC1->AddEntry(h3000g, "Dft=0.68", "l");
  legC1->Draw();
  c1->SetGridx(1);
  c1->SetGridy(1);
    
  h3000g->SetAxisRange(0,4e+33,"Y");
  h3000g->SetAxisRange(0,2.542006,"X");
  c1->SaveAs("L_distribution_EMIT_X.png");


  h3000g->SetAxisRange(0.99,1.006,"X");  
  c1->SaveAs("L_distribution_EMIT_X_zoom.png");


  h3000g->SetAxisRange(1e+20,2e+33,"Y");
  c1->SetLogy(1);//  c->SetLogy(0);


  h3000g->SetAxisRange(0,2.542006,"X");
  c1->SaveAs("L_distribution_EMIT_X_Logy.png");


  TCanvas *c5 = new TCanvas("E1/Enom : E2/nom to Energies","E1/E : E2/E to Energies",2700,1300);
  c5->Divide(3,2);
  c5->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");

  c5->cd(2);
  heEMIT_X1->Draw("colz");
  heEMIT_X1->SetAxisRange(0.98,1.0063,"X");
  heEMIT_X1->SetAxisRange(0.98,1.0063,"Y");
    

  c5->cd(3);
  heEMIT_X2->Draw("colz");
  heEMIT_X2->SetAxisRange(0.98,1.0063,"X");
  heEMIT_X2->SetAxisRange(0.98,1.0063,"Y");
  
  c5->cd(4);
  heEMIT_X3->Draw("colz");
  heEMIT_X3->SetAxisRange(0.98,1.0063,"X");
  heEMIT_X3->SetAxisRange(0.98,1.0063,"Y");

  c5->cd(5);
  heEMIT_X4->Draw("colz");
  heEMIT_X4->SetAxisRange(0.98,1.0063,"X");
  heEMIT_X4->SetAxisRange(0.98,1.0063,"Y");
  c5->cd(6);
  heEMIT_X5->Draw("colz");
  heEMIT_X5->SetAxisRange(0.98,1.0063,"X");
  heEMIT_X5->SetAxisRange(0.98,1.0063,"Y");
  // c5->cd(7);
  // heEMIT_X6->Draw("colz");
  // heEMIT_X6->SetAxisRange(0.98,1.0063,"X");
  // heEMIT_X6->SetAxisRange(0.98,1.0063,"Y");
  // c5->cd(8);
  // heEMIT_X7->Draw("colz");
  // heEMIT_X7->SetAxisRange(0.98,1.0063,"X");
  // heEMIT_X7->SetAxisRange(0.98,1.0063,"Y");
  c5->SaveAs("Lumi2D_EMIT_X.png");



// BETA dependence 

  TCanvas *c2 = new TCanvas("L distr BETA_X","L distribution depends on X-BETA function");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");
  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hBETA_X1->Draw("same");
  hBETA_X1->SetLineColor(kRed);
  hBETA_X2->Draw("same");
  hBETA_X2->SetLineColor(kRed+2);
  hBETA_X3->Draw("same");
  hBETA_X3->SetLineColor(kBlue);
  hBETA_X4->Draw("same");
  hBETA_X4->SetLineColor(kBlue+2);
  hBETA_X5->Draw("same");
  hBETA_X5->SetLineColor(kGreen);
  // 0.001 0.01 0.1 1.0 5.0 10.0 25.0
  TLegend* legC2 = new TLegend(0.8,0.09,0.98,0.9);  
  legC2->SetHeader("BETA_X ");
  legC2->SetTextSize(0.04);// set size of text
  //legC2->SetNColumns(2);
  legC2->SetFillColor(0);// Have a white background 0.8 3.2 5.6 10.4 14.8;
  legC2->AddEntry(hBETA_X1, "0.8", "l");
  legC2->AddEntry(hBETA_X2, "3.2", "l");
  legC2->AddEntry(hBETA_X3, "5.6", "l");
  legC2->AddEntry(hBETA_X4, "10.4", "l");
  legC2->AddEntry(hBETA_X5, "14.8", "l");
  legC2->AddEntry(h3000g, "Dft=8", "l");
  legC2->Draw();
  c2->SetGridx(1);
  c2->SetGridy(1);
  
  h3000g->SetAxisRange(0,4e+33,"Y");
  h3000g->SetAxisRange(0,2.542006,"X");
  c2->SaveAs("L_distribution_BETA_X.png");
  
  h3000g->SetAxisRange(0.99,1.006,"X");  
  c2->SaveAs("L_distribution_BETA_X_zoom.png");


  h3000g->SetAxisRange(0,2.542006,"X");
  // legC2->SetY1NDC(0.1);
  // legC2->SetY2NDC(0.4);  
  // legC2->SetX1NDC(0.3);
  // legC2->SetX2NDC(0.8);
  h3000g->SetAxisRange(1e+20,4e+33,"Y");
  c2->SetLogy(1);//  c->SetLogy(0);
  h3000g->SetAxisRange(0,2.542006,"X");
  c2->SaveAs("L_distribution_BETA_X_Logy.png");


  TCanvas *c7 = new TCanvas("E1/Enom : E2/nom to Energies","E1/E : E2/E to Energies BETA_X FUNCTION - DEPENDENCE",2700,1300);
  c7->Divide(3,2);
  c7->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");

  c7->cd(2);
  heBETA_X1->Draw("colz");
  heBETA_X1->SetAxisRange(0.98,1.0063,"X");
  heBETA_X1->SetAxisRange(0.98,1.0063,"Y");
    

  c7->cd(3);
  heBETA_X2->Draw("colz");
  heBETA_X2->SetAxisRange(0.98,1.0063,"X");
  heBETA_X2->SetAxisRange(0.98,1.0063,"Y");
  
  c7->cd(4);
  heBETA_X3->Draw("colz");
  heBETA_X3->SetAxisRange(0.98,1.0063,"X");
  heBETA_X3->SetAxisRange(0.98,1.0063,"Y");

  c7->cd(5);
  heBETA_X4->Draw("colz");
  heBETA_X4->SetAxisRange(0.98,1.0063,"X");
  heBETA_X4->SetAxisRange(0.98,1.0063,"Y");
  c7->cd(6);
  heBETA_X5->Draw("colz");
  heBETA_X5->SetAxisRange(0.98,1.0063,"X");
  heBETA_X5->SetAxisRange(0.98,1.0063,"Y");
  c7->SaveAs("Lumi2D_BETA_X.png");


  TCanvas *c3 = new TCanvas("L distr BETA_Y","L distribution depends on Y-BETA function");
  gPad->SetRightMargin(0.18);  
  gPad->SetBottomMargin(0.09);
  h3000g->GetYaxis()->CenterTitle(true);
  h3000g->GetYaxis()->SetTitleSize(0.03);
  h3000g->GetXaxis()->SetTitle("E_{eff}");
  h3000g->GetXaxis()->CenterTitle(true);
  h3000g->GetXaxis()->SetTitleSize(0.03);
  h3000g->GetYaxis()->SetTitleOffset(1.5);
  h3000g->GetYaxis()->SetTitle("L[cm^{-2} s^{-1}]");

  h3000g->Draw();
  h3000g->SetLineColor(kBlack);
  hBETA_Y1->Draw("same");
  hBETA_Y1->SetLineColor(kRed);
  hBETA_Y2->Draw("same");
  hBETA_Y2->SetLineColor(kRed+2);
  hBETA_Y3->Draw("same");
  hBETA_Y3->SetLineColor(kBlue);
  hBETA_Y4->Draw("same");
  hBETA_Y4->SetLineColor(kBlue+2);
  hBETA_Y5->Draw("same");
  hBETA_Y5->SetLineColor(kGreen);
 //  hBETA_Y6->Draw("same");
 //  hBETA_Y6->SetLineColor(kGreen+2);
 //  hBETA_Y7->Draw("same");
 //  hBETA_Y7->SetLineColor(kBlue+3);
 // //0.001 0.01 0.1 1.0 5.0 10.0 25.0
  TLegend* legC3 = new TLegend(0.8,0.09,0.98,0.9);  
  legC3->SetHeader("BETA_Y");
  legC3->SetTextSize(0.04);// set size of text
  // legC3->SetNColumns(2);  
legC3->SetFillColor(0);// Have a white background
  legC3->AddEntry(hBETA_Y1, "0.015", "l");
  legC3->AddEntry(hBETA_Y2, "0.06", "l");
  legC3->AddEntry(hBETA_Y3, "0.105", "l");
  legC3->AddEntry(hBETA_Y4, "0.195", "l");
  legC3->AddEntry(hBETA_Y5, "0.24", "l");
  // legC3->AddEntry(hBETA_Y6, "BETA_Y=10.0", "l");
  // legC3->AddEntry(hBETA_Y7, "BETA_Y=25.0", "l");
  legC3->AddEntry(h3000g, "Dft 0.15", "l");
  legC3->Draw();
  c3->SetGridx(1);
  c3->SetGridy(1);
  
  h3000g->SetAxisRange(0,2e+33,"Y");
  h3000g->SetAxisRange(0,2.542006,"X");
  c3->SaveAs("L_distribution_BETA_Y.png");

  h3000g->SetAxisRange(0.99,1.006,"X");
  c3->SaveAs("L_distribution_BETA_Y_zoom.png");
  h3000g->SetAxisRange(1e+20,4e+33,"Y");
  c3->SetLogy(1);//  c->SetLogy(0);
  h3000g->SetAxisRange(0,2.542006,"X");
  c3->SaveAs("L_distribution_BETA_Y_Logy.png");



  TCanvas *c8 = new TCanvas("E1/Enom : E2/nom to Energies","E1/E : E2/E to Energies BETA_Y_FUNCTION - DEPENDENCE",2700,1300);
  c8->Divide(3,2);
  c8->cd(1);
  heE3000g->Draw("colz");
  heE3000g->SetAxisRange(0.98,1.0063,"X");
  heE3000g->SetAxisRange(0.98,1.0063,"Y");

  c8->cd(2);
  heBETA_Y1->Draw("colz");
  heBETA_Y1->SetAxisRange(0.98,1.0063,"X");
  heBETA_Y1->SetAxisRange(0.98,1.0063,"Y");
    

  c8->cd(3);
  heBETA_Y2->Draw("colz");
  heBETA_Y2->SetAxisRange(0.98,1.0063,"X");
  heBETA_Y2->SetAxisRange(0.98,1.0063,"Y");
  
  c8->cd(4);
  heBETA_Y3->Draw("colz");
  heBETA_Y3->SetAxisRange(0.98,1.0063,"X");
  heBETA_Y3->SetAxisRange(0.98,1.0063,"Y");

  c8->cd(5);
  heBETA_Y4->Draw("colz");
  heBETA_Y4->SetAxisRange(0.98,1.0063,"X");
  heBETA_Y4->SetAxisRange(0.98,1.0063,"Y");
  c8->cd(6);
  heBETA_Y5->Draw("colz");
  heBETA_Y5->SetAxisRange(0.98,1.0063,"X");
  heBETA_Y5->SetAxisRange(0.98,1.0063,"Y");
  c8->SaveAs("Lumi2D_BETA_Y.png");




  return 0;

}

void FillHistogramsFromTree(TString filename, TH1D* h1D, TH2D* h2D, double nominalCMSEnergy, double lumi, double lumi1) {

  //Open File
  TFile *file = TFile::Open(filename);  //Get the Tree
  TTree *tree1;
  file->GetObject("lumiTree",tree1);

  //Define variabels for Tree
  Double_t E1, E2;
  tree1->SetBranchAddress("E1", &E1);
  tree1->SetBranchAddress("E2", &E2);

 
  //Loop over the tree
  int n_entries =  tree1->GetEntries();
  for (int i = 0; i <n_entries ;++i) {
    tree1->GetEntry(i);
    h1D->Fill(2*sqrt(E1*E2)/nominalCMSEnergy);
    h2D->Fill(E1/(nominalCMSEnergy/2),E2/(nominalCMSEnergy/2));
  }
 
  Double_t scalingFactor = lumi/(h1D->Integral());
  Double_t scalingFactor1 = lumi1/(h2D->Integral());
  h1D->Scale(scalingFactor);
  h2D->Scale(scalingFactor1);
  std::cout << "1Dhisto         " << h1D->Integral() << "              " << h1D->GetEntries() << std::endl;
  std::cout << "2Dhisto         " << h2D->Integral() << "              " << h2D->GetEntries() << std::endl;

  file->Close();
  delete file;
  return;
}



