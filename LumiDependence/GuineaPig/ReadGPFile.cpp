#include <TFile.h>
#include <TTree.h>
#include <TString.h>


#include <iostream>

//Simple Macro to read text file into a root tree

int main (int argn, char** argv) {

  if (argn < 3) {
    std::cerr << "not enough parameters! \n ReadGPFiles filename.root lumiGPFile " << std::endl;
    exit(1);
  }
  TString outputfile(argv[1]);
  TString inputfile(argv[2]);

  TFile *file = TFile::Open(outputfile,"RECREATE");


 TTree *tree1 = new TTree("lumiTree","lumiTree");
 tree1->ReadFile(inputfile,"E1/D:E2/D");
 tree1->Write();


 file->Close();
 return 0;
}

