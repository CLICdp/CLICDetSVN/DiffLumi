import os, subprocess, sys

inputfile = sys.argv[1]

inf = file(inputfile,"r")
lcount =0
tempfile = file("temp.in","w")
fpath = os.path.abspath("temp.in")
os.environ['INPUTFILE'] = fpath
for line in inf:
    lcount += 1
    tempfile.write(line)
    if (lcount==1e6):
        tempfile.close()
        subprocess.call(['./demo.exe'])

        if os.path.exists("bhwideout.hbook"):
            subprocess.call(["h2root","bhwideout.hbook"])
            os.remove("bhwideout.hbook")
            if os.path.exists("previous.root"):
              subprocess.call(["hadd","final.root","previous.root","bhwideout.root"])
              os.rename("final.root","previous.root")
              os.remove("bhwideout.root")
            else:
              os.rename("bhwideout.root","previous.root")
        os.remove(fpath)
        tempfile = file("temp.in","w")
        lcount=0
tempfile.close()

subprocess.call(['./demo.exe'])
if os.path.exists("bhwideout.hbook"):
    subprocess.call(["h2root","bhwideout.hbook"])
    subprocess.call(["hadd","final.root","previous.root","bhwideout.root"])
    os.remove("bhwideout.hbook")
    os.remove("bhwideout.root")
os.remove(fpath)
