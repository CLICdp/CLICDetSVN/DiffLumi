// $Id: $
// Include files



// local
#include "SmearEnergy.hh"
#include "TMath.h"
//-----------------------------------------------------------------------------
// Implementation file for class : SmearEnergy
//
// 2011-11-07 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SmearEnergy::SmearEnergy(TChain*ch, std::string fname ):MCTuple(ch) {
  m_fout = TFile::Open(fname.c_str(),"RECREATE");
  m_tree = new TTree("MyTree","MyTree");
  m_tree->Branch("eb1",&m_eb1,"eb1/D");
  m_tree->Branch("eb2",&m_eb2,"eb2/D");
  m_tree->Branch("deb1",&m_deb1,"deb1/D");
  m_tree->Branch("deb2",&m_deb2,"deb2/D");
  m_tree->Branch("px1",&m_px1,"px1/D");
  m_tree->Branch("py1",&m_py1,"py1/D");
  m_tree->Branch("pz1",&m_pz1,"pz1/D");
  m_tree->Branch("e1",&m_e1,"e1/D");
  m_tree->Branch("px2",&m_px2,"px2/D");
  m_tree->Branch("py2",&m_py2,"py2/D");
  m_tree->Branch("pz2",&m_pz2,"pz2/D");
  m_tree->Branch("e2",&m_e2,"e2/D");
  m_tree->Branch("nphotons",&m_nphotons,"nphotons/I");
  m_tree->Branch("photdata",m_photdata,"photdata[nphotons][4]/D");
  m_tree->Branch("RelativeProbability",&m_RelativeProbability,"RelativeProbability/D");

  m_h_smeared = new TH1D("m_h_smeared","E smeared", 300,-0.03,0.03);
  

  m_smearing_ph = 0.0;
  gRandom = new TRandom3(0);

  m_a = new double[4];
  m_b = new double[4];
  m_a[0] = 8.19e-4; m_b[0] = 9.07e-3;
  m_a[1] = 9.86e-5; m_b[1] = 3.83e-3;
  m_a[2] = 3.87e-5; m_b[2] = 1.59e-3;
  m_a[3] = 1.97e-5; m_b[3] = 7.22e-4;

}
//=============================================================================
// Destructor
//=============================================================================
SmearEnergy::~SmearEnergy() {
  delete m_fout;
  delete gRandom;

}

//=============================================================================

//=========================================================================
//
//=========================================================================
void SmearEnergy::Loop ( ) {

  if (!fChain) return;
  std::cout<<"Looping"<<std::endl;

  Long64_t nentries = fChain->GetEntries();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;

    m_eb1 = eb1;
    m_eb2 = eb2;
    m_deb1 = deb1;
    m_deb2 = deb2;
    m_px1 = px1;
    m_py1 = py1;
    m_pz1 = pz1;
    m_px2 = px2;
    m_py2 = py2;
    m_pz2 = pz2;

    m_e1 = e1;
    m_e2 = e2;
    double smear_e1 = 0.0;
    double smear_e2 = 0.0;
    //TLorentzVector part_e1(0,0,0,0);
    //TLorentzVector part_e2(0,0,0,0);
    //part_e1.SetPxPyPzE(m_px1,m_py1,m_pz1,m_e1);
    //part_e2.SetPxPyPzE(m_px2,m_py2,m_pz2,m_e2);

    smear_e1 = TMath::Sqrt(m_e1)*m_smearing_ph;
    smear_e2 = TMath::Sqrt(m_e2)*m_smearing_ph;
    //smear_e1 = deltaE(part_e1);
    //smear_e2 = deltaE(part_e2);

    //now smear the energy;
    if (smear_e1){
      double newe1 = gRandom->Gaus(m_e1,smear_e1);
      double newe2 = gRandom->Gaus(m_e2,smear_e2);
      m_h_smeared->Fill((newe1-m_e1)/m_e1);
      m_e1 = newe1;
      m_e2 = newe2;
    }
    for (int i =0;i<4;++i)
      m_photdata[0][i]=0;
    m_nphotons = nphotons;
    for (int i = 0; i < nphotons; ++i){
      for (int j = 0; j < 3; ++j)
        m_photdata[i][j] = photdata[i][j];
      m_photdata[i][4] = gRandom->Gaus(photdata[i][4],TMath::Sqrt(photdata[i][4])*m_smearing_ph);
    }

    m_RelativeProbability = RelativeProbability;

    m_tree->Fill();

  }
  std::cout<<"Done looping"<<std::endl;

}

//=========================================================================
//
//=========================================================================
void SmearEnergy::Save () {
  m_tree->Write();
  m_h_smeared->Write();
  m_fout->Write();
  m_fout->Close();

}


//=========================================================================
//
//=========================================================================
int  SmearEnergy::getIndex (double theta ) {
  int index =0;
  if (theta > (80*TMath::Pi()/180.))
    index = 3;
  else if (theta > (30*TMath::Pi()/180.))
    index = 2;
  else if (theta > (20*TMath::Pi()/180.))
    index = 1;
  else
    index = 0;
  return index;

}

//=========================================================================
//  
//=========================================================================
double SmearEnergy::deltaE (TLorentzVector vec ) {
  double deltae = 0;
  double sinth = TMath::Sin(vec.Theta());
  double p = vec.P();
  double e = vec.E();
  
  int index = getIndex(vec.Theta());
  deltae = TMath::Sqrt(m_a[index]*m_a[index]+
                       (m_b[index]*m_b[index]/
                        (p*p*sinth*sinth)))*e*e*sinth;
  

  return deltae;
}
