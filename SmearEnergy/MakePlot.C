int MakePlot(  ) {
  gROOT->ProcessLine(".L ../../CLICStyle.C");
  CLICStyle();
  
  TFile*f = TFile::Open("/data/sposs/BHWide_GP_E_Smeared_0.15.root");
  TCanvas*c1 = new TCanvas("c1","c1");
  c1->SetBottomMargin(0.22);
  TH1D*h1 = (TH1D*)f->Get("m_h_smeared");
  h1->SetXTitle("#frac{E_{smeared} - E}{E}");
  h1->SetYTitle("dN/dE");
  h1->GetXaxis()->SetTitleOffset(1.3);
  h1->SetLineColor(kRed);
  
  h1->Draw();
  c1->Print("Smearing.pdf");
  
  
  return 0;
}
