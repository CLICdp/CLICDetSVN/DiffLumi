// $Id: $
#ifndef SMEARENERGY_SMEARENERGY_HH
#define SMEARENERGY_SMEARENERGY_HH 1

// Include files
#include <TChain.h>

#include <TRandom3.h>
#include <string>
#include "MCTuple.h"
#include <TFile.h>
#include "Riostream.h"
#include "TLorentzVector.h"
#include "TH1D.h"
/** @class SmearEnergy SmearEnergy.hh SmearEnergy/SmearEnergy.hh
 *
 *
 *  @author Stephane Poss
 *  @date   2011-11-07
 */
class SmearEnergy: public MCTuple {
public:
  /// Standard constructor
  SmearEnergy(TChain* ch=0, std::string fname="");

  virtual ~SmearEnergy( ); ///< Destructor

  void setSmearing(double smear) { m_smearing_ph = smear; }
  void Loop();
  void Save();
  

protected:

private:

  TTree*m_tree;
  double m_smearing_ph;
  Double_t        m_eb1;
  Double_t        m_eb2;
  Double_t        m_deb1;
  Double_t        m_deb2;
  Double_t        m_px1;
  Double_t        m_py1;
  Double_t        m_pz1;
  Double_t        m_e1;
  Double_t        m_px2;
  Double_t        m_py2;
  Double_t        m_pz2;
  Double_t        m_e2;
  Int_t           m_nphotons;
  Double_t        m_photdata[100][4];   //[nphotons]
  Double_t        m_RelativeProbability;

  TH1D*m_h_smeared;

  TRandom3*gRandom;
  TFile*m_fout;
  double*m_a,*m_b;
  
  int getIndex(double);
  double deltaE (TLorentzVector);
};
#endif // SMEARENERGY_SMEARENERGY_HH
