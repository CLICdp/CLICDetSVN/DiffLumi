// $Id: $
#ifndef LUMI_STUDIES_BDSTOROOT_H 
#define LUMI_STUDIES_BDSTOROOT_H 1

// Include files

#include "TFile.h"
#include "TTree.h"
#include "Riostream.h"
/** @class BdsToRoot BdsToRoot.h Lumi_studies/BdsToRoot.h
 *  
 *
 *  @author Stephane Poss
 *  @date   2011-10-05
 */
class BdsToRoot {
public: 
  /// Standard constructor
  BdsToRoot(std::string ); 

  virtual ~BdsToRoot( ); ///< Destructor

  void readFile(std::string);
  void copyToTree();
  void saveTTree(std::string);
protected:
  ifstream m_file;
  
private:
  TFile*m_rootfile;
  TTree*m_tree;
  Float_t m_eb,m_x,m_y,m_z,m_xp,m_yp;

};
#endif // LUMI_STUDIES_BDSTOROOT_H
