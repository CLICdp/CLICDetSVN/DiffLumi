// $Id: $
// Include files 



// local
#include "LumiToRoot.h"
#include "TMath.h"
//-----------------------------------------------------------------------------
// Implementation file for class : LumiToRoot
//
// 2011-10-05 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LumiToRoot::LumiToRoot( std::string treename ) {
  m_tree = new TTree(treename.c_str(),treename.c_str());
  m_tree->Branch("E1",&m_eb1,"E1/D");
  m_tree->Branch("E2",&m_eb2,"E2/D");
  m_tree->Branch("eb1",&m_eb1,"eb1/D");
  m_tree->Branch("eb2",&m_eb2,"eb2/D");
  m_tree->Branch("deb1",&m_deb1,"deb1/D");
  m_tree->Branch("deb2",&m_deb2,"deb2/D");
  m_tree->Branch("x",&m_x,"x/D");
  m_tree->Branch("y",&m_y,"y/D");
  m_tree->Branch("z",&m_z,"z/D");
  m_tree->Branch("tc",&m_tc,"tc/D");
  m_tree->Branch("px1e1",&m_px1e1,"px1e1/D");
  m_tree->Branch("py1e1",&m_py1e1,"py1e1/D");
  m_tree->Branch("pz1e1",&m_pz1e1,"pz1e1/D");
  m_tree->Branch("px2e2",&m_px1e1,"px2e2/D");
  m_tree->Branch("py2e2",&m_py1e1,"py2e2/D");
  m_tree->Branch("pz2e2",&m_pz1e1,"pz2e2/D");
  m_tree->Branch("px1",&m_px1,"px1/D");
  m_tree->Branch("py1",&m_py1,"py1/D");
  m_tree->Branch("pz1",&m_pz1,"pz1/D");
  m_tree->Branch("px2",&m_px2,"px2/D");
  m_tree->Branch("py2",&m_px2,"py2/D");
  m_tree->Branch("pz2",&m_px2,"pz2/D");
  m_tree->Branch("label",&m_label,"label/I");
  
  h_spectrum = new TH1D("lumi_spectrum","lumi spectrum",1020,-1.5,3058.5);
  h_spectrum_2d = new TH2D("lumi_spectrum_2d","lumi spectrum",510,-0.75,1526.25,510,-0.75,1526.25);
  
}
//=============================================================================
// Destructor
//=============================================================================
LumiToRoot::~LumiToRoot() {
  delete m_tree;
} 

//=============================================================================

//=========================================================================
//  
//=========================================================================
void LumiToRoot::readFile (std::string fname ) {
  m_file.open((char*)fname.c_str() , ifstream::in);
  while (m_file.good()) 
  {
    m_file>>m_eb1>>m_eb2>>m_x>>m_y>>m_z>>m_tc>>m_px1e1>>m_py1e1
          >>m_px2e2>>m_py2e2>>m_px1>>m_py1>>m_pz1>>m_px2>>m_py2>>m_pz2>>m_label;
    m_deb1=0.;
    m_deb2=0.;
    m_tree->Fill();
    h_spectrum->Fill(TMath::Sqrt(4*m_eb1*m_eb2));
    h_spectrum_2d->Fill(m_eb1,m_eb2);
  }
  
}


//=========================================================================
//  
//=========================================================================
void LumiToRoot::saveTTree (std::string fname ) {
  m_rootfile = new TFile(fname.c_str(),"RECREATE");
  m_tree->Write();
  m_rootfile->Close();
  
  TFile fout("GuineaPigSpectrum.root","RECREATE");
  h_spectrum->Write();
  h_spectrum_2d->Write();
  
  fout.Close();
  
}

