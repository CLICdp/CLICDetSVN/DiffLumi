// $Id: $
// Include files 



// local
#include "BdsToRoot.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BdsToRoot
//
// 2011-10-05 : Stephane Poss
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BdsToRoot::BdsToRoot( std::string treename ) {
  m_tree = new TTree(treename.c_str(),treename.c_str());
  m_tree->Branch("E",&m_eb,"E/F");
  m_tree->Branch("x",&m_x,"x/F");
  m_tree->Branch("y",&m_y,"y/F");
  m_tree->Branch("z",&m_z,"z/F");
  m_tree->Branch("xp",&m_xp,"xp/F");
  m_tree->Branch("yp",&m_yp,"yp/F");

  
}
//=============================================================================
// Destructor
//=============================================================================
BdsToRoot::~BdsToRoot() {
  delete m_tree;
} 

//=============================================================================

//=========================================================================
//  
//=========================================================================
void BdsToRoot::readFile (std::string fname ) {
  m_file.open((char*)fname.c_str() , ifstream::in);
  while (m_file.good()) 
  {
    m_file>>m_eb>>m_x>>m_y>>m_z>>m_xp>>m_yp;
    
    m_tree->Fill();
  }
  
}


//=========================================================================
//  
//=========================================================================
void BdsToRoot::saveTTree (std::string fname ) {
  m_rootfile = new TFile(fname.c_str(),"RECREATE");
  m_tree->Write();
  m_rootfile->Close();
  
}

int main()
{
  BdsToRoot* t = new BdsToRoot("MyTree");
  t->readFile("/data/sposs/bds/bds_positron.txt");
  t->saveTTree("file_bds.root");
  
  return 0;
}
